! units.f90 $Id$
! vim: nowrap
!-----------------------------------------------------------------------
!
! For a massive particles ip of species isp the velocity components are
!           sp(isp)%particle(ip)%p = (v/c)*Lorentz factor (p=v*g)
! To get the physical momentum one needs to multiply with the rest mass.
!
! The energy is
!           sp(isp)%particle(ip)%e = kinetic energy = mass*p**2/(g + 1)
! where the Lorentz factor g = sqrt(p**2 + 1)
!
!-----------------------------------------------------------------------
MODULE units
  USE params, only: mtxt, stdin, stdout, out_units, master, trace

  character(len=mtxt) basesystem                                        ! basesystem in module units

  type :: unit
    sequence                                                            ! keep the order
    real :: l                                                           ! length
    real :: t                                                           ! time
    real :: d                                                           ! mass density
    real :: m                                                           ! mass
    real :: n                                                           ! number density
    real :: v                                                           ! velocity
    real :: e                                                           ! energy
    real :: p                                                           ! pressure
    real :: f                                                           ! energy flux
    real :: b                                                           ! magn field strength unit
    real :: q                                                           ! electric charge
    character(len=mtxt) name                                            ! system name
  end type

  type :: constants
    sequence                                                            ! keep the order
    real :: c                                                           ! Speed of light
    real :: c2                                                          ! Speed of light**2
    real :: oc                                                          ! 1./Speed of light
    real :: oc2                                                         ! 1./Speed of light**2
    real :: kb                                                          ! Boltzmann constant
    real :: planck                                                      ! Planck's constant
    real :: pi                                                          ! pi
    real :: twopi                                                       ! 2pi
    real :: sqrtpi                                                      ! sqrt(pi)
    real :: fourpi                                                      ! Four pi
    real :: epsilon0                                                    ! Epsilon0
    real :: mu0                                                         ! Mu0
    real :: evK                                                         ! eV/K
    real :: elm_q                                                       ! Stat Coloumb
    real :: mu                                                          ! atomic mass unit
    real :: me                                                          ! electron mass
    real :: gamma_gas                                                   ! gamma gas
    real :: mc                                                          ! Electron mass*c
    real :: mc2                                                         ! Electron mass*c**2
    real :: omc                                                         ! 1/Electron mass*c
    real :: omc2                                                        ! 1/Electron mass*c**2
    real :: sigma_thom                                                  ! Thompson cross-section
    real :: sigma_comp                                                  ! Compton cross-section
    character(len=mtxt) name                                            ! Name of constant system
  end type

  type :: lorentzmaxwell
    sequence                                                            ! keep the order
    real :: ke,kb,kf                                                    ! Lorentz-Maxwell scaling constants.
    character(len=mtxt) :: name                                         ! name of system used
  end type

  type(unit)              :: u
  type(constants)         :: c
  type(lorentzmaxwell)    :: elm

!-----------------------------------------------------------------------
END MODULE

!-----------------------------------------------------------------------
SUBROUTINE read_units
END SUBROUTINE read_units

!-----------------------------------------------------------------------
SUBROUTINE init_units
  USE params
  USE units
  implicit none
  character(len=mid) :: id = "units.f90 $Id$"
!........................................................................
  call print_id(id)
  
  u%l = 1.; u%t = 1.; u%d = 1.; u%name = 'identity'

  c%name      = 'UNITY'
  basesystem  = c%name

  c%c         = 1.
  c%kb        = 1.
  c%planck    = 1.
  c%pi        = 3.14159265
  c%twopi     = 2.*c%pi
  c%sqrtpi    = sqrt(c%pi)
  c%epsilon0  = 1.
  c%mu0       = 1.
  c%evK       = 1.
  c%elm_q     = 1.
  c%gamma_gas = 1.
  c%mu        = 1.
  c%me        = 1.

  c%fourpi    = 1.
  elm%ke      = 1./c%fourpi
  elm%kb      = 1./(c%c*c%fourpi)
  elm%kf      = 1./c%c
 
  c%c2        = c%c**2
  c%oc        = 1./c%c
  c%oc2       = c%oc**2
        
END SUBROUTINE init_units
