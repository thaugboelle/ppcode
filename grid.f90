! grid.f90 $Id$
! vim: nowrap
!=======================================================================
MODULE grid_m
!
! The scetch below illustrates the meaning of the various elements in the
! grid data structure.  Element names starting with 'g' represent global 
! values while other names represent local values.  The coordinates (g%x
! etc) values are global.  The zero point is not significant but is chosen
! to be such that g%z(g%lb)=0 in the first domain.  
!
! The size of the physical domain is g%gs, from boundary to boundary. The
! size of each computional domain is g%s. The sizes of the computational
! domains are not necessary all the same -- as illustrated, the actual 
! values of for example "lb" may vary between domains, depending on if 
! the domain contains a physical boundary or not.
!................................................................................................
!
! CLOBAL COORDINATES: 
!                     grlb                                                            grub
!              --------<========================== gs =================================>---------
! GLOBAL INDICES:      0                         .                                    gn
!                    iz+offset                                                       iz+offset
!              -----------------------------     .          ------------------------------------
!              |   |   |     ...   |   |   |     .          |   |   |   |   ...    |   |   |   |
!              |   |   |     ...   |   |   |     .          |   |   |   |   ...    |   |   |   |
!              -----------------------------     .          ------------------------------------
!                      lb             ub         .              lb                    ub
!                                  ----------------------------------                 
!                                  |   |   |     .          |   |   |
!                                  |   |   |     .          |   |   |
!                                  ----------------------------------
! LOCAL COORDINATES:              z(1) rlb      z(iz)         rub  z(n)
!                                  ----<=========== s ==========>----
!     LOCAL INDICES:               1   lb                      ub   n
!
!................................................................................................
  type :: grid_type                                                     ! grid properties
    integer :: n(3)                                                     ! dimension
    integer :: lb(3),  ub(3)                                            ! cell bndry
    integer :: ghlb(3),ghub(3)                                          ! ghostzone size 
    real    :: gs(3), s(3), ds(3), ods(3)                               ! size
    integer :: gn(3)                                                    ! global size
    integer :: offset                                                   ! global index = iz+offset
    real    :: rlb(3), rub(3)                                           ! min/max of grid
    real    :: grlb(3),grub(3)                                          ! global limits of grid
    real    :: gV,V                                                     ! Volume domain size
    real    :: goV,oV                                                   ! 1 / (Volume domain size)
    real    :: dV                                                       ! Volume element size
    real    :: odV                                                      ! ...and 1/dV
    real, pointer, dimension(:)   :: x, y, z                            ! mesh functions
  end type

  type :: grid_io                                                       ! grid properties
    sequence                                                            ! keep the order!
    integer :: n(3)                                                     ! dimension
    integer :: lb(3),  ub(3)                                            ! cell bndry for the parts
    real(kind=4) :: gs(3), s(3), ds(3)                                  ! size

    integer :: gn(3)                                                    ! global size
    integer :: offset                                                   ! global index = iz+offset
    real(kind=4) :: rlb(3), rub(3)                                      ! min/max of grid
    real(kind=4) :: grlb(3),grub(3)                                     ! global limits of grid
    real(kind=4) :: gV,V                                                ! Volume domain size
    real(kind=4) :: dV                                                  ! Volume element size
  end type
!
! There are 3 large data structures:
!
!   em              : electric and magnetic fields on the mesh
!   fields          : species fields on the mesh
!   particles       : particle data for all species
!
  type :: mesh_type                                                     ! properties on the mesh
    real bx, by, bz, ex, ey, ez                                         ! fields
  end type

  real fdebye                                                           ! factor in Debye length
  real fskin                                                            ! factor in skin depth
  integer nx, ny, nz, ngrid                                             ! for convenience
  real :: o_ngrid
  type(grid_type) :: g                                                  ! our grid
  type(mesh_type), allocatable, dimension(:,:,:) :: em                  ! EM-fields
  real, allocatable, dimension(:,:,:) :: Bx, By, Bz, Ex, Ey, Ez         ! EM-fields
  real, allocatable, dimension(:,:,:) :: fmhd
  real, dimension(3):: B0, E0                                           ! initial fields

  real(kind=8):: odx, ody, odz, odxq, odyq, odzq                        ! for convenience
  real drmax                                                            ! max index change

!-----------------------------------------------------------------------
CONTAINS
!-----------------------------------------------------------------------
END MODULE grid_m

!=======================================================================
SUBROUTINE read_grid
  USE params, only : mdim,master,stdin,stdout,params_unit,out_namelists
  USE grid_m
  implicit none
  integer, dimension(mdim):: n
  real   , dimension(mdim):: s,ds
  real    :: xmin, ymin, zmin                                           ! for convenience
  namelist /grid/ n, s, ds, xmin, ymin, zmin, b0, e0, fdebye, fskin
!.......................................................................

  s  = g%gs
  ds = g%ds
  n  = g%gn
  xmin=g%grlb(1)
  ymin=g%grlb(2)
  zmin=g%grlb(3)
 
  rewind (stdin); read  (stdin, grid)
  g%gs = s                                                              ! pick up values
  g%ds = ds
  g%gn  = n
  if (maxval(g%gs) > 0) then                                            ! if gs set
    g%ds = g%gs/g%gn                                                    ! base ds on s
  else                                                                  ! otherswise
    g%gs = g%ds*g%gn                                                    ! base s on ds
  end if
  g%grlb = (/xmin,ymin,zmin/)

  s  = g%gs                                                             ! for printout only
  ds = g%ds
  n  = g%gn
  if (master) write(params_unit,grid)
  if (out_namelists) write (stdout,grid)
END SUBROUTINE read_grid

!=======================================================================
SUBROUTINE write_grid
  USE params, ONLY : data_unit, mid, mpi
  USE grid_m, ONLY : g, grid_io
  implicit none
  type(grid_io) :: g_io
  character(len=mid):: &
    id = "grid.f90 $Id$"
  integer, parameter:: io_format=1                                      ! increment when changing
!.......................................................................
  write (data_unit) io_format,mid
  write (data_unit) id
  write (data_unit) 1
  g_io%n      = g%n
  g_io%lb     = g%lb
  g_io%ub     = g%ub
  g_io%gs     = g%gs
  g_io%s      = g%s
  g_io%ds     = g%ds
  g_io%gn     = g%gn
  g_io%offset = mpi%offset(3)
  g_io%rlb    = g%rlb
  g_io%rub    = g%rub
  g_io%grlb   = g%grlb
  g_io%grub   = g%grub
  g_io%gV     = g%gV
  g_io%V      = g%V
  g_io%dV     = g%dV
  !write (data_unit) sizeof(g_io)/sizeof(1.),3
  write (data_unit) 37,3
  write (data_unit) g_io
  write (data_unit) g_io
END SUBROUTINE write_grid

!=======================================================================
SUBROUTINE read_grid_header
  USE params, ONLY : data_unit, mid
  USE grid_m, ONLY : g, grid_io
  USE header
  implicit none
  type(grid_io) :: g_io
  character(len=mid):: &
    id = "grid.f90 $Id$"
  integer           :: io_format=1                                      ! increment when changing
  integer           :: l,t
!.......................................................................
  call check_header_format(io_format, id, "read_grid_header")
  !write (data_unit) sizeof(g_io)/sizeof(1.),3
  read  (data_unit) l,t
  read  (data_unit) g_io
  read  (data_unit) g_io
  if (any(g_io%gn .ne. g%gn))     call error("read_grid_header","global grid sizes differ")
  if (any(g_io%gs .ne. g%gs))     call error("read_grid_header","global sizes differ")
  if (any(g_io%ds .ne. g%ds))     call error("read_grid_header","grid spacing differ")
  if (any(g_io%grlb .ne. g%grlb)) call error("read_grid_header","global lower boundary changed")
  if (any(g_io%grub .ne. g%grub)) call error("read_grid_header","global upper boundary changed")
END SUBROUTINE read_grid_header

!=======================================================================
SUBROUTINE init_grid
!
! Considerations about grids:
!
! 1. On input, either 'ds' and 'n' defines 's', or 's' and 'n' defines 'ds'.
!    's' and 'n' should at this point mean the useful part of the box, NOT
!    including boundary ghost zones.
! 2. Immediately thereafter, the MPI split should be made, with an approximately
!    even number of (physical) zones per domain.  The only remaining memory
!    of the global size should now be 'gs' (which is not directly needed and
!    should not be used in the domains)
! 3. The only remaining MPI information should be offset, which is the 
!    index offset, needed (only) in the random number generator.
! 4. Then ghost zones should be added to internal domains, and boundary zones
!    should be added to boundary domains.
! 5. We do need the (real, not index) physical boundaries for the particles,
!    otherwise we cannot test for being on the boundary
!
  USE params,    only : mid, mdim, mcoord, stdout, out_grid, mpi, master  
  USE grid_m,    only : g, ngrid, o_ngrid, nx, ny, nz, &
                        odx, ody, odz, odxq, odyq, odzq,  &
                        bx, by, bz, ex, ey, ez, b0, e0,   &
                        fdebye, fskin, fmhd, drmax
  implicit none

  integer            :: i, iz, midx
  character(len=mid) :: id = "grid.f90 $Id$"
!.......................................................................

  call print_id(id)

  g%gs = 0.
  g%ds = 1.
  g%gn = 19
  g%grlb = 0.
  drmax = 0.
  fdebye = 0.
  fskin = 0.
  b0 = 0.
  e0 = 0.

  call read_grid

  g%grub = g%grlb + g%gs
  
!-----------------------------------------------------------------------
! Define half zone boundary indices based on lower and upper boundary
!-----------------------------------------------------------------------
  g%ghlb = g%lb - 1
  g%ghub = g%ub + 1

!-----------------------------------------------------------------------
! defined down to here: g%gs, g%ds, g%gn, g%grlb, g%grub
!-----------------------------------------------------------------------
  call setup_mpi_grid                                                   ! Redefine domain size
!-----------------------------------------------------------------------
! additionally defined in setup_mpi_grid: g%lb, g%ub, g%n, g%offset
!-----------------------------------------------------------------------

  ngrid = product(g%n) ! dimensional independent way of writing: nx*ny*nz
  o_ngrid = 1./ngrid

  g%ods = 1./g%ds      ! Needed in sort
  odx = 1./g%ds(1)     ! These pseudonyms are needed in the Maxwell solver 
  ody = 1./g%ds(2)     ! and the interpolation routines.
  odz = 1./g%ds(3)     ! Better not initialise them too many times.
  odxq = odx**2        !
  odyq = ody**2        !
  odzq = odz**2        !

  g%s = g%ds*(g%ub-g%lb)                                                ! local size
!*************** CHECK WHERE g%V IS USED!! **********************
  g%V  = product(g%s)  ; g%oV  = 1./g%V   !Size of local volume & ditto^-1
  g%gV = product(g%gs) ; g%goV = 1./g%gV  !Size of entire volume & ditto^-1
  g%dV = product(g%ds) ; g%odV = 1./g%dV  !Size of diff. element volume & ditto^1

  nx = g%n(1)
  ny = g%n(2)
  nz = g%n(3)
  allocate (g%x(nx), g%y(ny), g%z(nz))

  g%x = g%grlb(1) + g%ds(1)*(/(i+mpi%offset(1), i=1,g%n(1))/)
  g%y = g%grlb(2) + g%ds(2)*(/(i+mpi%offset(2), i=1,g%n(2))/)
  g%z = g%grlb(3) + g%ds(3)*(/(i+mpi%offset(3), i=1,g%n(3))/)

  if (mpi%me(1) .eq. mpi%n(1)-1) g%x(g%ub(1)) = g%grub(1)               ! EXACT btw rub, grub
  if (mpi%me(2) .eq. mpi%n(2)-1) g%y(g%ub(2)) = g%grub(2)
  if (mpi%me(3) .eq. mpi%n(3)-1) g%z(g%ub(3)) = g%grub(3)

  g%rlb = (/g%x(g%lb(1)),g%y(g%lb(2)),g%z(g%lb(3))/)                    ! for clarity
  g%rub = (/g%x(g%ub(1)),g%y(g%ub(2)),g%z(g%ub(3))/)                    ! EXACT upper boundary

!-----------------------------------------------------------------------
! additionally defined above: g%s, g%V, g%dV, g%x, g%y, g%z, g%rlb, g%rub
!-----------------------------------------------------------------------

  if (out_grid .and. stdout.ge.0) then                                  ! write some info
    write(stdout,*)             ' Loading grid...' 
    write(stdout,'(a35,i9)')    '    Number of dimensions,   mdim:     ',mdim 
    write(stdout,'(a35,i9)')    '    Number of coordinates,  mcoord:   ',mcoord
    write(stdout,'(a35,3i9)')   '    Global grid zones,      g%gn:     ',g%gn
    write(stdout,'(a35,3i9)')   '    Local grid zones,       g%n:      ',g%n
    write(stdout,'(a35,3i9)')   '    Grid points,       nx,ny,nz:      ',nx,ny,nz
    write(stdout,'(a35,3f20.3)') '    Grid minimum values,    g%grlb:   ',g%grlb
    write(stdout,'(a35,3f20.3)') '    Grid maximum values,    g%grub:   ',g%grub
    write(stdout,'(a35,3f20.3,1x,3l1)') '    Dom. minimum values,    g%rlb:    ',g%rlb, mpi%lb
    write(stdout,'(a35,3f20.3,1x,3l1)') '    Dom. maximum values,    g%rub:    ',g%rub, mpi%ub
    write(stdout,'(a35,3i9)')   '    Grid offset,            mpi%offset',mpi%offset
    write(stdout,'(a35,3i9)')   '    Grid minimum values,    g%lb:     ',g%lb
    write(stdout,'(a35,3i9)')   '    Grid maximum values,    g%ub:     ',g%ub
    write(stdout,'(a35,3f20.3)')'    Total sizes,            g%gs:     ',g%gs
    write(stdout,'(a35,3f20.3)')'    Domain sizes,           g%s:      ',g%s
    write(stdout,'(a35,3f20.3)')'    Cell sizes,             g%ds:     ',g%ds
  endif
  allocate (Bx(nx,ny,nz), By(nx,ny,nz), Bz(nx,ny,nz))
  allocate (Ex(nx,ny,nz), Ey(nx,ny,nz), Ez(nx,ny,nz))
  allocate (fmhd(nx,ny,nz))
  fmhd = 1.

  do iz=1,g%n(3)                                                        ! explicit loop, for OMPx
    Bx(:,:,iz)=B0(1); By(:,:,iz)=B0(2); Bz(:,:,iz)=B0(3)
    Ex(:,:,iz)=E0(1); Ey(:,:,iz)=E0(2); Ez(:,:,iz)=E0(3)
  end do

  call compute_stagger_prefactors
  if (master) call test_search
END SUBROUTINE init_grid

!=======================================================================
SUBROUTINE overlap(f)
!
! Using MPI or copying, fill the overlap ghost zones
!
  USE grid_m, only: g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f

  call overlap_x(f)
  call overlap_y(f)
  call overlap_z(f)
END SUBROUTINE

!=======================================================================
SUBROUTINE overlap_add(f)
!
! Using MPI or copying, fill the overlap ghost zones
!
  USE grid_m, only: g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f

  call overlap_add_x(f)
  call overlap_add_y(f)
  call overlap_add_z(f)
END SUBROUTINE

!=======================================================================
SUBROUTINE setup_mpi_grid
!
! Defines: g%lb, g%ub, g%n, mpi%offset, interior
!
  USE params, only : rank, master, nodes, stdout, dbg, mpi, periodic
  USE grid_m, only : g
  USE debug
  implicit none

  integer :: k,n,nn,rest,node,i

  do i=1,3
    n = g%gn(i)
    if (mpi%n(i).gt.n-1 .and. mpi%n(i) .gt. 1) then                       ! More nodes than zones?
      if (stdout.gt.0) then
        write (stdout,*) "You have requested too many nodes "// &
          "compared to the number of grid zones!"
        write (stdout,'(a,i7,a,i6,a,i4)') &
          "Number of nodes=",nodes," n=", n, &
          " Maximum number of nodes=", n-1
      endif
      call Finalize_Mpi(.false.)                                          ! doesn't make sense
    endif

    if (periodic(i)) then
      mpi%interior(i) = .true.                                            ! all nodes interior
      mpi%lb(i) = .false.                                                 ! no physical boundary
      mpi%ub(i) = .false.                                                 ! no physical boundary
    end if

    nn = n/mpi%n(i)                                                       ! grids per domain
    rest = n - nn*mpi%n(i)                                                ! overflow

    g%n(i) = nn                                                           ! starting value
    mpi%offset(i) = 0                                                     ! offset zero point  
    
    do node=0,mpi%me(i)                                                   ! add up
      if (node>0 .and. rest>0) then
        nn = n/mpi%n(i)+1                                                 ! one extra point
        rest = rest-1                                                     ! decrement overflow
      else
        nn = n/mpi%n(i)
      end if
      g%n(i) = nn                                                         ! physical sub-domain
      mpi%offset(i) = mpi%offset(i) + nn                                  ! increment offset
    end do
    mpi%offset(i) = mpi%offset(i) - nn                                    ! cancel last increment

    if (verbose > 1) print'(1x,a,l3,6i5)', &
      'grid:',periodic(i),i,rank,mpi%me(i),g%lb(i),g%ub(i),mpi%offset(i)

    !if (mpi%me(i) .ne. 0          .or. periodic(i)) g%lb(i) = 2          ! lower interior
    !if (mpi%me(i) .ne. mpi%n(i)-1 .or. periodic(i)) g%ub(i) = 1          ! upper interior

    g%n(i) = g%n(i) + g%lb(i) + g%ub(i)                                   ! points in domain
    g%ub(i) = g%n(i) - g%ub(i)                                            ! upper bdry index

    mpi%offset(i) = mpi%offset(i) - g%lb(i)                               ! z_local+mpi%offset(i)

    call barrier
    if (debugit(dbg_mpi,1)) write(*,'(a,i4,a,3i7,a,3i7,a,i7,a,2i7,a,2i7)') &
      'Mark1: Rank,  ,mpi%me(i)                             =    ', mpi%me(i),&
      ' Full Size,   ,g%n                                   =    ', g%n, &
      ' Size,        ,g%ub-g%lb                             =    ', g%ub-g%lb, &
      ' Offset,      ,mpi%offset(i)                         =    ', mpi%offset(i),&
      ' Boundaries,  ,g%lb(i)+offset(i), offset(i)+g%ub(i)  =    ', g%lb(i)+mpi%offset(i), &
                                                                    mpi%offset(i)+g%ub(i),&
      ' Bounds,      ,g%lb(i), g%ub(i)                      =    ', g%lb(i),g%ub(i)
      
    call barrier
  end do
  g%offset = mpi%offset(3)

  call test_overlap
  call barrier()
END SUBROUTINE setup_mpi_grid

!=======================================================================
SUBROUTINE fold1 (r, rr)
  USE grid_m, only: g
  USE params , only: periodic, mcoord
  implicit none  
  real, intent(in), dimension(mcoord):: r
  real            , dimension(mcoord):: rr
  integer i
!.......................................................................
  do i=1,mcoord
    rr(i) = r(i)
    if (periodic(i)) then
      rr(i) = mod(rr(i) - g%grlb(i), g%gs(i)) + g%grlb(i)               ! fast initial wrap
      rr(i) = merge(g%grub(i) + (rr(i)-g%grlb(i)), rr(i),  &
              rr(i) <  g%grlb(i))   ! left protection
      rr(i) = merge(g%grlb(i) + (rr(i)-g%grub(i)), rr(i),  &
              rr(i) >= g%grub(i))   ! right protection
    end if
    rr(i) = merge ((rr(i) - g%rlb(i))*g%ods(i) + g%lb(i), &             ! robust float address
                   (rr(i) - g%rub(i))*g%ods(i) + g%ub(i), &
                   rr(i) < 0.5*(g%rlb(i)+g%rub(i)))
  end do
END

!=======================================================================
SUBROUTINE fold2 (r, rr)
  USE grid_m, only: g
  USE params , only: periodic, mcoord
  implicit none  
  real, intent(in), dimension(mcoord):: r
  real            , dimension(mcoord):: rr
  integer i
!.......................................................................
  do i=1,mcoord
    rr(i) = r(i)
    if (periodic(i)) then
      rr(i) = mod(rr(i) - g%grlb(i), g%gs(i)) + g%grlb(i)               ! fast initial wrap
      rr(i) = merge(g%grub(i) + (rr(i)-g%grlb(i)), rr(i), &
              rr(i) <  g%grlb(i))   ! left protection
      rr(i) = merge(g%grlb(i) + (rr(i)-g%grub(i)), rr(i), &
              rr(i) >= g%grub(i))   ! right protection
    end if
  end do
END
