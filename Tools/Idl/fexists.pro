;-----------------------------------------------------------------------

FUNCTION fexists,file,files=f,count=n

  f=findfile(file,count=n)

  if n gt 0 then return,1 else return,0

END