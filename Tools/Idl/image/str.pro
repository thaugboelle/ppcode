FUNCTION str,x,_extra=e
;+
;  $Id: str.pro,v 1.4 2000/07/24 07:37:58 aake Exp $
;-
  return,strtrim(string(x,_extra=e),2)
END

