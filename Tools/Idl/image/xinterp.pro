; $Id: xinterp.pro,v 1.2 2011/04/28 13:58:15 aake Exp $
;***********************************************************************
FUNCTION xinterp,x,xt,f
;
;  Interpolate to a new z-grid.
;
      nx=n_elements(x)
      nt=n_elements(xt)
;-----------------------------------------------------------------------
;
;  Get derivatives of f
;
      d=xder(x,f)
;
;  Depth loop
;
      n=1
      sz=size(f)
      sz[1]=nt
      ft=make_array(size=sz)
      for k=0,nt-1 do begin
;
;  Find n such that  z(n-1) < zt(k) < z(n)
;
        while (xt(k) gt x(n)) do begin
          n=n+1
          if (n gt nx-1) then begin
            n=nx-1
            goto, found
          endif
        end
;
;  Cubic interpolation
;
found:
        dx=(x(n)-x(n-1))
        px=(xt(k)-x(n-1))/dx
        qx=1.-px
;
        pxf=px-(qx*px)*(qx-px)
        pxd=-px*(qx*px)*dx
        qxd=qx*(qx*px)*dx
 
        s=size(f)
        case s(0) of
        2: ft(k,*  )=f(n-1,*  )+pxf*(f(n,*  )-f(n-1,*  )) $
             +qxd*d(n-1,*  )+pxd*d(n,*  )
        3: ft(k,*,*)=f(n-1,*,*)+pxf*(f(n,*,*)-f(n-1,*,*)) $
             +qxd*d(n-1,*,*)+pxd*d(n,*,*)
        end
;
;  End x loop
;
      endfor
;
      return,ft
END
