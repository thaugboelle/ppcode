; $Id: imsz.pro,v 1.4 2010/02/11 12:34:27 troels_h Exp $

pro imsz,xs,ys,get=get
  COMMON cimsz,imx,imy
  if n_params() eq 0 then begin print,'imx=',imx,' imy=',imy & return & endif
  if keyword_set(get) then begin
    xs=imx & ys=imy
  endif else begin
    if n_params() eq 1 then begin
      imx=xs & imy=xs
    endif else begin
      imx=xs & imy=ys
    endelse
  endelse
END
