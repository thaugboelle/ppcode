function haver,f,xvertical=xvertical,yvertical=yvertical,zvertical=zvertical,array=array,cm=cm
;
;  Horizontal average of 3-d scalar
;
    COMMON cvertxy, cxvert, cyvert

    if          keyword_set(xvertical) then begin
      cxvert=1
      cyvert=0
    end else if keyword_set(yvertical) then begin
      cxvert=0
      cyvert=1
    end else if keyword_set(zvertical) then begin
      cxvert=0
      cyvert=0
    end

    s=size(f)
    if s(0) eq 2 then begin
      h=total(f,2)/s(2)
      if keyword_set(array) then $
        h=rebin(reform(h,s(1),1),s(1),s(2))
    end else if keyword_set(cxvert) then begin
      h=aver(f,[2,3])
      if keyword_set(array) then $
        h=rebin(reform(h,s(1),1,1),s(1),s(2),s(3))
    end else if keyword_set(cyvert) then begin
      h=aver(f,[1,3])
      if keyword_set(array) then $
        h=rebin(reform(h,1,s(2),1),s(1),s(2),s(3))
    end else begin
      h=aver(f,[1,2])
      if keyword_set(array) then $
        h=rebin(reform(h,1,1,s(3)),s(1),s(2),s(3))
    end
    if keyword_set(cm) then h=h(4:n_elements(h)-5)
    return,h
end
