; $Id: rp.pro,v 1.55 2012/04/10 23:09:56 aake Exp $
; ----------------------------------------------------------------------
pro rpa,d,n,remains=remains,verbose=verbose
; 
; Advance the particle file pointers corresponding to n particles
;
  for isp=0,d.s.nspecies-1 do begin
    for idim=0,d.s.mdim-1 do begin
      if d.s.format.io eq 12 then $
      d.o.q[idim,isp]=d.o.q[idim,isp]+n*2
      d.o.r[idim,isp]=d.o.r[idim,isp]+n*4
    endfor
    for idim=0,d.s.mcoord-1 do begin
      d.o.p[idim,isp]=d.o.p[idim,isp]+n*4
    endfor
    d.o.e[isp]=d.o.e[isp]+n*4
    d.o.w[isp]=d.o.w[isp]+n*4
    d.o.i[isp]=d.o.i[isp]+n*8
  endfor
  d.o.lnp = d.o.lnp-n
  remains = min(d.o.lnp)
  if keyword_set(verbose) then print,remains,' particles remain'
end

; ----------------------------------------------------------------------
 function rp2,d,n,remains=remains,verbose=verbose, $
   noindices=noindices,noweight=noweight, $
   swap_if_big_endian=swap_if_big_endian, swap_if_little_endian=swap_if_little_endian
;
; Read particle data piece by piece, in chunks of length n, until the snapshot is exhausted:
;
; n=10000L                                      ; number of particles per chunk (should be larger than species difference)
; p=rp(0,/partial,remains=remains)              ; returns a set of file offsets instead of actual data
; while remains ge 0 do begin                   ; continue while there are more particles
;   p=rp2(p,n,remains=remains)                  ; read a chunk with n particles per species
;   process,p                                   ; do something with the particles
; end
;
  if keyword_set(verbose) then t0=systime(1)
  np     = d.o.lnp
  nsp    = d.s.nspecies
  ndim   = d.s.mdim
  ncrd   = d.s.mcoord
  n_max  = 2L^20
  np_max = max(d.o.lnp,min=np_min)
  np_min = np_min
  n_min  = np_max-np_min+1
  if n_elements(n) eq 0 then begin
    if np_min gt n_max then begin               ; if not set, choose 1M particles
      n = n_max
      print,'number of particles to read set to '+string(n)
    endif else begin
      n = np_min
    endelse
  endif
  if n_elements(n) gt 0 then begin
    if np_max gt n then begin
      if np_min le 0 then begin                   ; check if any particles remain
        print,'all particles have been read'
        return, d
      endif
        if n ge np_min then begin                   ; check if too many requested
        print,'n = '+string(n)+' was too large, set = '+string(np_min)
        n = np_min
      endif
      if n lt n_min then begin                    ; check if too few requested
        print,'n = '+string(n)+' was too small, set = '+string(n_min)
        n = n_min
      endif
      if np_max gt n then np = (np < n)           ; read all particles the last time
    endif
  endif
  fmt = d.s.format.io
  if fmt eq 12 then $
  q =   intarr(max(np),ndim,nsp)
  r =   fltarr(max(np),ndim,nsp)
  p =   fltarr(max(np),ncrd,nsp)
  e =   fltarr(max(np),nsp)
  w =   fltarr(max(np),nsp)
  i = lon64arr(max(np),nsp)
  o = d.o
  for isp=0,nsp-1 do begin
    m=np[isp]
    for idim=0,ndim-1 do begin
      if fmt eq 12 then $
      a=assoc(o.lun,intarr(m),o.q[idim,isp]) & q[0:m-1,idim,isp]=a[0] & o.q[idim,isp]=(fstat(o.lun)).cur_ptr
      a=assoc(o.lun,fltarr(m),o.r[idim,isp]) & r[0:m-1,idim,isp]=a[0] & o.r[idim,isp]=(fstat(o.lun)).cur_ptr
    endfor
    for idim=0,ncrd-1 do begin
      a=assoc(o.lun,fltarr(m),o.p[idim,isp]) & p[0:m-1,idim,isp]=a[0] & o.p[idim,isp]=(fstat(o.lun)).cur_ptr
    endfor
    a=assoc(o.lun,  fltarr(m),o.e[isp]) & e[0:m-1,isp]=a[0] & o.e[isp]=(fstat(o.lun)).cur_ptr
    if not keyword_set(noweight) then $
    a=assoc(o.lun,  fltarr(m),o.w[isp]) & w[0:m-1,isp]=a[0] & o.w[isp]=(fstat(o.lun)).cur_ptr
    if not keyword_set(noindices) then $
    a=assoc(o.lun,lon64arr(m),o.i[isp]) & i[0:m-1,isp]=a[0] & o.i[isp]=(fstat(o.lun)).cur_ptr
  endfor
  if keyword_set(swap_if_big_endian) or keyword_set(swap_if_little_endian) then begin
    if fmt eq 12 then $
    swap_endian_inplace, q, swap_if_big_endian=swap_if_big_endian, swap_if_little_endian=swap_if_little_endian
    swap_endian_inplace, r, swap_if_big_endian=swap_if_big_endian, swap_if_little_endian=swap_if_little_endian
    swap_endian_inplace, p, swap_if_big_endian=swap_if_big_endian, swap_if_little_endian=swap_if_little_endian
    swap_endian_inplace, e, swap_if_big_endian=swap_if_big_endian, swap_if_little_endian=swap_if_little_endian
    if not keyword_set(noweight) then $
    swap_endian_inplace, w, swap_if_big_endian=swap_if_big_endian, swap_if_little_endian=swap_if_little_endian
    if not keyword_set(noindices) then $
    swap_endian_inplace, i, swap_if_big_endian=swap_if_big_endian, swap_if_little_endian=swap_if_little_endian
  endif
  if fmt eq 12 then begin
    ds   = d.s.ds
    grlb = d.s.grlb
    for idim=0,ndim-1 do begin
      r[*,idim,*] = r[*,idim,*]*ds[idim]+grlb[idim] + q[*,idim,*]*ds[idim]
    endfor
  endif
  o.np = np
  o.lnp = o.lnp - np
  remains = min(o.lnp)
  if remains eq 0 then free_lun,o.lun
  if keyword_set(verbose) then begin
    print,string(float(remains),format='(g12.3)')+' particles remain'
    print,string(float(max(np)),format='(g12.3)')+' particles read in ', $
	      string(systime(1)-t0,format='(f6.1)')+' sec'
  endif
  ;d = {r:r,p:p,q:q,e:e,i:i,w:w,s:d.s,o:o}
  d = {r:r,p:p,e:e,i:i,w:w,s:d.s,o:o}
  return, d
end

;+
; $Id: rp.pro,v 1.55 2012/04/10 23:09:56 aake Exp $
; Keywords:
;   default     : read data into a structure
;   commonb     : store data in common block (save mem)
;   datadir     : dir where data resides
;   thread      : only get for a specific thread (maybe not supported?)
;   sort        : sort particles with respect to their indices
;   params      : all the parameters; can also be accessed as strcuture.s
;   verbose     : verbosity level, for debug
;   wrap        : wrap particles into the 'real' domain given by coordinates
;   noweight    : do not read the weights of the particles to save mem
;   noenergy    : do not read the energies of the particles to save mem
;   nopositions : do not read the positions of the particles to save mem
;   novelocities: do not read the velocities of the particles to save mem
;   noindices   : do not read the indices to save mem
;   only_params : only pass the structure from open_data with the header, and return
;   partial_read: prepare for reading partial snapshot data, as per
;                 p=rp(t,/part,rem=rem) & while rem gt 0 do begin process,rp2(p,1000L,rem=rem)
;   _extra      : keywords passed to open_data
; -------------------------------------------------------------------------
function rp,snapnr,datadir=datadir,thread=thread,merge=merge, $
            sort=sort,params=params,verbose=verbose,wrap=wrap, $
            reduce=reduce,commonb=commonb,noweight=noweight,time=time, $
            noenergy=noenergy,noindices=noindices,nopositions=nopositions,$
            novelocities=novelocities, delete=delete,only_params=only_params,$
	    velocities=velocities,positions=positions,weights=weights,$
	    indices=indices,energies=energies,$
            freq=freq,partial_read=partial_read,remains=remains,$
            get_positionu=get_position,_extra=_extra
common particles,np,tnp,r,p,w,e,ind,part
common cparams,s
default, verbose, 0
;-
default, reduce, 1
default, freq, 0
if (n_params() lt 1) then begin
  doc_library, 'rp'
  return, 0
endif

if keyword_set(velocities) $
or keyword_set(positions) $
or keyword_set(weights) $
or keyword_set(indices) $
or keyword_set(energies) then begin
  if keyword_set(verbose) then print,'using inclusive choice'
  novelocities = 1-keyword_set(velocities)
  nopositions  = 1-keyword_set(positions)
  noenergy     = 1-keyword_set(energies)
  noweight     = 1-keyword_set(weights)
  noindices    = 1-keyword_set(indices)
end

if keyword_set(time) then t0=systime(1)

names = get_snap_files('particles',snapnr,datadir=datadir,merge=merge,thread=thread,verbose=verbose)
if (names[0] eq '') then begin
  print,'WARNING: trying datadir=Data/ after failure to find file in ',datadir
  datadir = 'Data'
  names = get_snap_files('particles',snapnr,datadir=datadir,merge=merge,thread=thread,verbose=verbose)
endif
if keyword_set(time) then print,'time (get_snap_files): ',systime(1)-t0,format='(a,f6.1)'

if (verbose gt 1) then $
  for n=0,n_elements(names)-1 do print,'names: ',names(n)
nchunks = n_elements(names)                                                     ; #chunks from file names
if (verbose gt 0) then print,'nchunks: ',nchunks

if (names[0] eq '') then begin
  if (n_elements(datadir) gt 0) then $
    print, "Cannot find data! snapnr="+string(snapnr)+" dir="+datadir $
  else $
    print, "Cannot find data! snapnr="+string(snapnr)+" dir=<default dirname>"
  return, -1
endif

open_data,names[0],lun,/F77_UNFORMATTED,struct=s,datadir=datadir, $
  verbose=verbose,_extra=_extra
ptime=s.ptime & mdim=s.mdim & mcoord=s.mcoord
nspecies=s.nspecies & indices=s.indices
free_lun, lun

                                                                                ; define arrays to hold species particle numbers
 np = lonarr(nspecies)                                                          ; scratch # of particles
if (s.format.io ge 9) then np = lon64arr(nspecies)                              ; kind=8 format?
nnp = lon64arr(nchunks,nspecies)                                                ; # of parts in mpi chunks
tnp = lon64arr(nspecies)                                                        ; total # of parts in snap

; loop over names to find out how big a particle array we need
for ch=0,nchunks-1L do begin
  open_data,names[ch],lun,/F77_UNFORMATTED,struct=s,datadir=datadir, $
    verbose=verbose,_extra=_extra
  ptime=s.ptime & mdim=s.mdim & nspecies=s.nspecies & indices=s.indices
  if (s.format.io ge 4) then begin                                      ; old format?
    readu, lun,np                                                       ; get actual #parts
  end else begin
    np=s.np                                                             ; take #parts from header
  end
  nnp[ch,*] = np                                                        ; #parts on this node
  tnp = tnp + np                                                        ; total #parts over nodes
  free_lun, lun
endfor
s = create_struct(s,'lnp',tnp)                                          ; nr of _loaded_ particles

params=s

if keyword_set(only_params) then begin
  return, {s:s}
endif

; sanity check only valid when writefac=1 and reading whole array:
if (s.format.io lt 4) then $
if (not array_equal(tnp, s.tnp) and (n_elements(thread) eq 0)) then $
  print, "Datafile is corrupted. Total nr of particles, as reported by"+ $
  " last chunk, is not equal to the number of particles we find by simply"+ $
  " summing up particles from all chunks. Chunks missing?"

maxnp = max(tnp)
totnp = total(tnp,/int)

if (verbose gt 0) then print,'max(tnp)  : ',max(tnp)                     ; maximum number of particles in any single node
if (verbose gt 1) then begin
  print,'      nnp : ',nnp         ; total number of particles from any node
  print,'      tnp : ',tnp         ; total number of particles from all nodes
  print,'total(tnp): ',totnp       ; total number of particles in this snapshot
endif
if (verbose gt 0) then print,'time = ',s.time

if (maxnp eq 0) then begin
  print, "No particles in snapshot"
  return, -1
endif

; Allocate data arrays for all chunks and all species. Only allocate what is neccesary.
; For allocating the structure, we want to do it without spending any more memory than
; neccesary, therefore we construct the execute string, and execute it. It could be done with
; a lot of nested if's, but this code is clearer and easier to maintain.

itnp=lon64arr(nspecies)
if (keyword_set(commonb)) then begin
  if (not keyword_set(nopositions)) then r = fltarr(maxnp,mdim,nspecies) ; overkill by choosing maxnp for species, but OK...
  if (not keyword_set(novelocities)) then p = fltarr(maxnp,mcoord,nspecies) ; ...for now to avoid use of pointers
  if (not keyword_set(noweight)) then w = fltarr(maxnp,nspecies)
  if (not keyword_set(noenergy)) then e = fltarr(maxnp,nspecies)
  if (indices and (not keyword_set(noindices))) then ind=lon64arr(maxnp,nspecies)
endif else begin
  L1=lon64arr(nspecies)
  L2=lon64arr(mdim,nspecies)
  ;o={lun:0,p:L2,q:L2,r:L2,e:L1,w:L1,i:L1,np:itnp,lnp:itnp}
  o={lun:0,p:L2,r:L2,e:L1,w:L1,i:L1,np:itnp,lnp:itnp}
  exestring = 'd={'
  if (    keyword_set(partial_read)) then begin
    exestring = exestring + 'o:o,'
  endif else begin
    if (not keyword_set(novelocities)) then exestring = exestring + 'p:fltarr(maxnp,mcoord,nspecies),'
    if (not keyword_set(nopositions))  then exestring = exestring + 'r:fltarr(maxnp,mdim,nspecies),'
    ;if (not keyword_set(nopositions))  then exestring = exestring + 'q:intarr(maxnp,mdim,nspecies),'
    if (not keyword_set(noweight))     then exestring = exestring + 'w:fltarr(maxnp,nspecies),'
    if (not keyword_set(noenergy))     then exestring = exestring + 'e:fltarr(maxnp,nspecies),'
    if (indices and (not keyword_set(noindices))) then exestring = exestring + 'i:lon64arr(maxnp,nspecies),'
  endelse
  exestring = exestring + 's:s}'
  if not keyword_set(get_position) then void = execute(exestring)
endelse

for ch=0, nchunks-1l do begin
  open_data,names[ch],lun,/F77_UNFORMATTED,struct=s,datadir=datadir, $
    delete=delete,verbose=verbose,_extra=_extra
  if keyword_set(partial_read) then begin
    openr,lun1,/get,datadir+'/'+names[ch],/F77_UNFORMATTED
  endif
  ptime=s.ptime & mdim=s.mdim & nspecies=s.nspecies & indices=s.indices
  grlb =s.grlb  & ds  =s.ds

  if (s.format.io ge 4) then begin                                      ; old format?
    readu, lun,np                                                       ; get actual #parts
  end else begin
    np=s.np                                                             ; take #parts from header
  end

  if keyword_set(partial_read) then begin
    o.lun = lun1
    o.np  = np
  endif

; loop over species reading the data, and copying the blocks into the arrays
  skip=0L
  p=(fstat(lun)).cur_ptr
  if (keyword_set(get_position) or keyword_set(verbose)) then print,'position = '+strtrim(string(p),2)
  if keyword_set(get_position) then return,p
  for isp=0,nspecies-1 do begin
      npi =   np(isp)
    itnpi = itnp(isp)
    if (npi eq 0) then continue
    if not keyword_set(partial_read) then temp=fltarr(npi,/NoZero)
    for idim=0,mdim-1 do begin
      if (keyword_set(nopositions)) then begin
        readu,lun,skip
      endif else begin
        if keyword_set(partial_read) then begin
          o.r[idim,isp]=(fstat(lun)).cur_ptr+8
          readu,lun,skip
        endif else begin
          readu,lun,temp
          if (s.format.io ge 12) then $
            temp = temporary(temp) * ds[idim] + grlb[idim]
          if (keyword_set(commonb)) then $                                        ; positions
            r(itnpi:itnpi+npi-1,idim,isp)=temp $
          else $
            d.r(itnpi:itnpi+npi-1,idim,isp)=temp
        endelse
      endelse
      if (s.format.io eq 1) then begin                                          ; check for old (wrong) fmt
        if (keyword_set(novelocities)) then begin
          readu,lun,skip
        endif else begin
          if keyword_set(partial_read) then begin
            o.p[idim,isp]=(fstat(lun)).cur_ptr+8
            readu,lun,skip
          endif else begin
            readu,lun,temp
            if (keyword_set(commonb)) then $                                      ; momenta
              p(itnpi:itnpi+npi-1,idim,isp)=temp $
            else $
              d.p(itnpi:itnpi+npi-1,idim,isp)=temp
          endelse
        endelse
      endif
    endfor
    if (s.format.io ge 12)  then begin                                          ; cell positions
      if (not keyword_set(nopositions)) then temp2 = intarr(npi,/NoZero)        ; scratch array
      for idim=0,mdim-1 do begin
        if (keyword_set(nopositions)) then begin
          readu,lun,skip
        endif else begin
          if keyword_set(partial_read) then begin
            o.q[idim,isp]=(fstat(lun)).cur_ptr+8
            readu,lun,skip
          endif else begin
            readu,lun,temp2
            temp = temp2*ds[idim]
            if (keyword_set(commonb)) then begin                                  ; positions
              r(itnpi:itnpi+npi-1,idim,isp) += temp
            endif else begin
              d.r(itnpi:itnpi+npi-1,idim,isp) += temp
            endelse
          endelse
        endelse
      endfor
      if (not keyword_set(nopositions)) then temp2 = 0                          ; save memory
    endif
    if (s.format.io gt 1) then begin
      for idim=0,mcoord-1 do begin
        if (keyword_set(novelocities)) then begin
          readu,lun,skip
        endif else begin
          if keyword_set(partial_read) then begin
            o.p[idim,isp]=(fstat(lun)).cur_ptr+8
            readu,lun,skip
          endif else begin
            readu,lun,temp
            if (keyword_set(commonb)) then $                                        ; momenta
              p(itnpi:itnpi+npi-1,idim,isp)=temp $
            else $
              d.p(itnpi:itnpi+npi-1,idim,isp)=temp
          endelse
        endelse
      endfor
    endif
    if (keyword_set(noenergy)) then begin
      readu,lun,skip
    endif else begin
      if keyword_set(partial_read) then begin
        o.e[isp]=(fstat(lun)).cur_ptr+8
        readu,lun,skip
      endif else begin
        readu,lun,temp
        if (keyword_set(commonb)) then  $                                         ; energies
          e(itnpi:itnpi+npi-1,isp)=temp $
        else $
          d.e(itnpi:itnpi+npi-1,isp)=temp
      endelse
    endelse
    if (keyword_set(noweight)) then begin
      readu,lun,skip
    endif else begin
      if keyword_set(partial_read) then begin
        o.w[isp]=(fstat(lun)).cur_ptr+8
        readu,lun,skip
      endif else begin
        readu,lun,temp
        if (keyword_set(commonb)) then  $                                         ; weights
          w(itnpi:itnpi+npi-1,isp)=temp $
        else $
          d.w(itnpi:itnpi+npi-1,isp)=temp
      endelse
    endelse
    if (indices) then begin
      if (keyword_set(noindices)) then begin
        readu,lun,skip
      endif else begin
        if keyword_set(partial_read) then begin
          o.i[isp]=(fstat(lun)).cur_ptr+8
          readu,lun,skip
        endif else begin
          temp = lon64arr(npi,/NoZero)
          readu,lun,temp
          if (keyword_set(commonb)) then  $                                       ; indices
            ind(itnpi:itnpi+npi-1,isp)=temp $
          else $
            d.i(itnpi:itnpi+npi-1,isp)=temp
        endelse
      endelse
    endif
  endfor
  if verbose gt 1 then print,names[ch],ch,itnp
  itnp = itnp + np
  if not keyword_set(partial_read) then free_lun,lun
endfor
if (keyword_set(sort)) then begin                                               ; sort ids
  if (not keyword_set(indices) or keyword_set(noindices)) then begin            ; check if we _can_ sort
    if (not keyword_set(indices)) then print, $
        "Cannot sort particles, since indices are not stored in snapshot"
    if (keyword_set(noindices))   then print, $
        "Cannot sort particles, since indices are not loaded fom the snapshot (noindices set)"
  endif else begin
    if (keyword_set(commonb)) then begin                                        ; sort on structure
      for isp=0,nspecies-1 do begin
        I = sort(ind[0:tnp[isp]-1,isp] and (not 2ll^63))                        ; sort masking the synth bit
        if (not keyword_set(nopositions))  then for id=0,mdim-1 do r[0:tnp[isp]-1,id,isp] = r[I,id,isp]
        if (not keyword_set(novelocities)) then for id=0,mcoord-1 do p[0:tnp[isp]-1,id,isp] = p[I,id,isp]
        if (not keyword_set(noweight)) then w[0:tnp[isp]-1,isp] = w[I,isp]
        if (not keyword_set(noenergy)) then e[0:tnp[isp]-1,isp] = e[I,isp]
      ind[0:tnp[isp]-1,isp]=ind[i,isp]
      endfor
    endif else begin                                                            ; sort on common data
      for isp=0,nspecies-1 do begin
        I = sort(d.i[0:tnp[isp]-1,isp] and (not 2ll^63))                        ; sort masking the synth bit
        if (not keyword_set(nopositions)) then for id=0,mdim-1 do d.r[0:tnp[isp]-1,id,isp] = d.r[I,id,isp]
        if (not keyword_set(novelocities)) then for id=0,mcoord-1 do d.p[0:tnp[isp]-1,id,isp] = d.p[I,id,isp]
        if (not keyword_set(noweight)) then d.w[0:tnp[isp]-1,isp] = d.w[I,isp]
        if (not keyword_set(noenergy)) then d.e[0:tnp[isp]-1,isp] = d.e[I,isp]
      d.i[0:tnp[isp]-1,isp]=d.i[i,isp]
      endfor
    endelse
    I = 0                                                                       ; save mem
  endelse
endif
s = create_struct(s,'lnp',tnp)                                                  ; nr of _loaded_ particles

if (keyword_set(wrap) and (not keyword_set(nopositions))) then begin            ; wrap particle positions into principal domain
  ;print,'Wrapping particle positions... '
  for iisp = 0,nspecies-1 do begin
   for iidim = 0,idim-1 do begin
     rmax = d.s.grub[iidim]                                                     ; make some prefactors
     rmin = d.s.grlb[iidim]
     ipmax = s.lnp[iisp]-1                                                      ; particle loop index
     mn = min(d.r[0:ipmax,iidim,iisp], max=mx)                                  ; get min and max of positions
       if (mn ge rmin and mx le rmax) then continue                               ; cycle if none outside box
     L  = rmax-rmin
     oL = 1./L
     ;print,'iisp, iidim = ',iisp,iidim
     dis = (d.r[0:ipmax,iidim,iisp]-rmin)*oL
     d.r[0:ipmax,iidim,iisp] += rmin - floor(dis)*L
   endfor
  endfor
endif

if keyword_set(time) then print,'time (total): ',systime(1)-t0,format='(a,f6.1)'; time the loading procedure

remains = min(d.s.lnp)
if keyword_set(partial_read) then begin
  o.lnp = d.s.lnp
  d.o = o
  if keyword_set(verbose) then print,string(remains)+' particles available for reading with rp2(p,n)'
endif

if (keyword_set(commonb)) then begin
  return, 1
end else begin
  d.s.np = tnp
  return, d
endelse
end
