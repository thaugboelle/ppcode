! Patch/hybrid/ohms_law.f90 $Id$
! vim: nowrap
!=======================================================================
SUBROUTINE ohms_law(Bx,By,Bz,nUx,nUy,nUz,n,Ex,Ey,Ez)
  USE params,   only : mid
  USE units,    only : c
  USE grid_m,   only : g
  USE hybrid_m, only : T_electron, q_electron, eta, do_electron_density_floor
  USE species,  only : sp, nspecies
  USE pic_stagger
  implicit none
  real, dimension(g%n(1), g%n(2), g%n(3)), intent(in) :: Bx, By, Bz
  real, dimension(g%n(1), g%n(2), g%n(3)), target     :: Ex, Ey, Ez
  real, dimension(g%n(1), g%n(2), g%n(3), nspecies), target :: n, nUx, nUy, nUz
  !
  real, parameter    :: tny = 1e4 * tiny(1.0)
  real               :: imu0, q, alpha_p
  integer            :: isp, iz
  character(len=mid) :: id='Patch/hybrid/ohms_law.f90 $Id$'

  ! Local scratch fields
  real, dimension(g%n(1), g%n(2), g%n(3)) :: ene, Jex, Jey, Jez, Jx, Jy, Jz
  real, dimension(g%n(1), g%n(2), g%n(3)), target :: tmp
  real, dimension(:,:,:), pointer :: ene1, ln_ene
  !
  call print_id(id)
                                   call timer('ohms law','start')
  !
  ! The electric field is face staggered. Therefore we have to
  ! interpolate to keep things face staggered
  !
  ! Let the electron charge be q_e = -e with e > 0. The charge-neutrality
  ! condition is then
  !
  ! e n_e = Sum(q_i n_i).
  !
  ! The electron current density is given by
  !
  ! J_e = -e n_e U_e = J - Sum(q_i n_i U_i),
  !
  ! where the total current density is given through Ampere's law
  !
  ! mu0 J = curl(B).
  !
  ! Given the electron number density and the electron velocity
  ! (the latter via the electron current), Ohm's law for
  ! isothermal electrons (p_e = n_e k_B T_e with T_e = const) may be
  ! written as
  !
  ! E = -U_e x B - grad (p_e) / (e n_e)
  !   = -U_e x B + alpha * grad (ln(e n_e))
  !
  ! where we have introduced the short hand
  !
  ! alpha_p = k_B T_e / q_e.


  ! Obtain total current density from Ampere's law.
  imu0 = 1. / c%mu0
  Jx = imu0 * (ddydn(Bz) - ddzdn(By))
  Jy = imu0 * (ddzdn(Bx) - ddxdn(Bz))
  Jz = imu0 * (ddxdn(By) - ddydn(Bx))

  ! Compute centered electron number density (times e)
  ! and face staggered current density.
  isp=1
  q = sp(isp)%charge
  ene =      q * n  (:,:,:,isp)
  Jex = Jx - q * nUx(:,:,:,isp)
  Jey = Jy - q * nUy(:,:,:,isp)
  Jez = Jz - q * nUz(:,:,:,isp)

  do isp=2,nspecies
    q = sp(isp)%charge
    ene = ene + q * n  (:,:,:,isp)
    Jex = Jex - q * nUx(:,:,:,isp)
    Jey = Jey - q * nUy(:,:,:,isp)
    Jez = Jez - q * nUz(:,:,:,isp)
  enddo

  ! Compute cell centered current density
  tmp = xdn(Jex); Jex = tmp
  tmp = ydn(Jey); Jey = tmp
  tmp = zdn(Jez); Jez = tmp

  if (do_electron_density_floor) then
    ! Prevent electron number density from getting too low.
    ene = max(ene, tny)
  endif

  ! Inverse electron number density
  ene1 => tmp
  if (do_electron_density_floor) then
    ! Guard against divide-by-zero when there are cells without ions
    ene1 = merge(1./ene, 0., ene > tny)
  else
    ene1 = 1./ene
  endif

  ! Negative electron velocity -- cell centered
  Jex = Jex * ene1
  Jey = Jey * ene1
  Jez = Jez * ene1

  ! Electric field -- face staggered
  Ex = xup(Jey) * ydn(Bz) - xup(Jez) * zdn(By)
  Ey = yup(Jez) * zdn(Bx) - yup(Jex) * xdn(Bz)
  Ez = zup(Jex) * xdn(By) - zup(Jey) * ydn(Bx)

  ! Add electron pressure contribution
  if (T_electron > 0.) then
    alpha_p = c%kb * T_electron / q_electron
    ! Log of charge density
    ln_ene => tmp
    if (do_electron_density_floor) then
      ! Guard against log-of-zero when there are cells without ions
      ln_ene = merge(alog(ene), 0., ene > tny)
    else
      ln_ene = alog(ene)
    endif
    Ex = Ex + alpha_p * ddxup(ln_ene)
    Ey = Ey + alpha_p * ddyup(ln_ene)
    Ez = Ez + alpha_p * ddzup(ln_ene)
  endif

  ! Add Ohmic resistivity
  if (eta > 0.) then
    Ex = Ex + eta*Jx
    Ey = Ey + eta*Jy
    Ez = Ez + eta*Jz
  endif

  !!!!!!!!!!!
  !! FIXME !!
  !!!!!!!!!!!
  ! if (do_shear) then
  !   Transform to fluctuating E-field
  ! endif

  ! Update boundary zones of electric field
  !---------------------------------------------------------------------
  call e_boundaries(Ex, Ey, Ez)
END SUBROUTINE ohms_law
