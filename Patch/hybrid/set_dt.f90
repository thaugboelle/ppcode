! Patch/hybrid/set_dt.f90 $Id$
! vim: nowrap
!=======================================================================
SUBROUTINE set_dt
!
! Set the time step, using Courant conditions based on three vmax values:
!
! vmax(1) = max particle speed
! vmax(2) = max cyclotron frequency
! vmax(3) = max Alfven speed
!-----------------------------------------------------------------------
  USE params,   only: time, dt, dtp, Cdtwce, nspecies, mpi, &
                      master, stdout, dt_factor, do_vth, Cdt
  USE grid_m,   only: Bx,By, Bz, g
  USE species,  only: sp, fields
  USE hybrid_m, only: nvmax, vmax
  implicit none
  integer           :: isp, ix, iy, iz, lb(3), ub(3)
  real(kind=8)      :: dtnew
  real, parameter   :: pi=acos(-1.)
  real              :: B,wci,dsmin,rho,v_A,v_c,v_ph,v_wh
  logical, save     :: first_call=.true.
!.......................................................................
  if (Cdt <= 0.0) return                                               ! for testing
  call trace_enter('SET_DT')
                                            call timer('set_dt','start')
  lb = merge(g%lb, g%lb  , mpi%lb)
  ub = merge(g%ub, g%ub-1, mpi%ub)
  dsmin = minval(g%ds)
  vmax(2:nvmax) = 0.                                                    ! vmax(1) is set
!-----------------------------------------------------------------------
! Evaluate local cyclotron frequencies and Alfven speeds
!-----------------------------------------------------------------------
  do iz=lb(3),ub(3); do iy=lb(2),ub(2); do ix=lb(1),ub(1)
    B= sqrt(Bx(ix,iy,iz)**2 + By(ix,iy,iz)**2 + Bz(ix,iy,iz)**2)        ! FIXME: center(?)
    rho = 0.
    wci = 0.
    do isp=1,nspecies
      wci = max (wci, B*sp(isp)%charge/sp(isp)%mass)                    ! cyclotron freq
      rho = rho+fields(ix,iy,iz,isp)%d * sp(isp)%mass                   ! mass density
    end do
    v_c = dsmin*wci/(2.*pi)                                             ! cyclotron speed
    vmax(2) = max(vmax(2), v_c)                                         ! cyclotron limit
    if (rho > 0.) then
      v_A = B/sqrt(rho)                                                 ! Alfven speed
      if (v_A > 0.) then
        v_ph = v_A*(sqrt(1. + (.5*v_A/v_c)**2) + .5*v_A/v_c)            ! phase speed
      else
        v_ph = 0.
      endif
    else
      v_A = 0.
      v_ph = 0.
    endif
    vmax(3) = max(vmax(3), v_ph)                                        ! max phase sp
  end do; end do; end do
  vmax = vmax + 1e-30                                                   ! zero B protect
!-----------------------------------------------------------------------
! Compute conditions globally over MPI-domains
!-----------------------------------------------------------------------
  call mpi_max_reals (nvmax, vmax)                                      ! max over domains 
  dtnew = Cdt*dsmin/maxval(vmax)
!-----------------------------------------------------------------------
! Possibly change dt, but in discrete steps, to preserve accuracy in
! most time steps.
!-----------------------------------------------------------------------
  if (first_call) then
    dtp = dtnew
    first_call=.false.
  else
    if (dtnew > dt*dt_factor) then                                        ! need larger dt
      dtnew = dt*dt_factor                                                ! discrete step
      if (master) write(stdout,1) &
      'Increased dtnew, Cdt_{vcAw} =', dtnew, vmax*dtnew/dsmin
  1   format(1x,a,1p,g12.3,1x,0p,4f6.3)
    else if (dtnew < dt/dt_factor) then                                   ! need smaller dt
      dtnew = dt/dt_factor
      if (master) write(stdout,1) &
      'Decreased dtnew, Cdt_{vcAw} =', dtnew, vmax*dtnew/dsmin
    else
      dtnew = dt                                                          ! use the same dt
    end if
    dtp = dt                                                              ! set previous dt
  endif
  dt = dtnew                                                            ! set new dt

  call trace_exit('SET_DT')
END SUBROUTINE set_dt
