! Patch/hybrid/periodic.f90 $Id$
! vim: nowrap
! Module for field boundary conditions

SUBROUTINE orbital_advection_B(Bx,By,Bz,delta_t)
  USE grid_m,      only : g
  implicit none
  !
  real, dimension(g%n(1),g%n(2),g%n(3)), target :: Bx, By, Bz
  real         :: delta_t
  logical      :: dagger
  !
  dagger=.false.
  call orbital_advection(Bx,By,Bz,dagger,delta_t)
END SUBROUTINE orbital_advection_B

SUBROUTINE orbital_advection_E(Ex,Ey,Ez,Dx,Dy,Dz,delta_t)
  USE grid_m,      only : g
  implicit none
  !
  real, dimension(g%n(1),g%n(2),g%n(3)), target :: Ex, Ey, Ez
  real, dimension(g%n(1),g%n(2),g%n(3)), target :: Dx, Dy, Dz
  real         :: delta_t
  logical      :: dagger
  !
  dagger=.true.
  Dx=Ex; Dy=Ey; Dz=Ez
  call orbital_advection(Dx,Dy,Dz,dagger,delta_t)
END SUBROUTINE orbital_advection_E

! Shear Transform inside the box
SUBROUTINE orbital_advection(fx,fy,fz,dagger,delta_t)
  USE params,      only : mpi, shear
  USE grid_m,      only : g
  USE pic_stagger, only : ytranslate
  implicit none
  !
  real, dimension(g%n(1),g%n(2),g%n(3)), target :: fx, fy, fz
  real, dimension(:,:,:),               pointer :: slice
  real         :: delta_t
  integer      :: ix
  logical      :: dagger
  real(kind=8) :: trans, shear8
  !
  ! First
  !
  do ix=g%lb(1),g%ub(1)

  enddo
END SUBROUTINE orbital_advection

! Shear Transform in ghostzones
!=====================================================================!
SUBROUTINE shear_translate(f)
  USE params, ONLY : time, shear, mpi
  USE grid_m, ONLY : g
  USE pic_stagger, ONLY : ytranslate
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f
  real(kind=8) :: trans, shear8
  !
  shear8 = real(shear, kind=8)                                          ! counteract round-off
  ! Transform in lower boundary region (cells sent from the upper boundary)
  trans = -shear8 * time * g%gs(1)                                       ! Distance to be translated
  if (mpi%me(1)==0) call ytranslate(f(1:g%lb(1)-1,:,:),trans,g%lb(1)-1) ! Lower x-domain. Translate
  !
  ! Transform in upper boundary region (cells sent from lower boundary)
  trans = shear8 * time * g%gs(1)                                      ! Distance to be translated
  if (mpi%me(1)==mpi%n(1)-1) call ytranslate(f(g%ub(1):g%n(1),:,:),trans,g%n(1)-g%ub(1)+1) ! Upper x-domain. Translate
END SUBROUTINE shear_translate
! Boundary conditions for electric fields
!=====================================================================!
SUBROUTINE e_boundaries(ex,ey,ez)
  USE grid_m, ONLY : g
  USE params, ONLY : do_shear, shear
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: ex,ey,ez

  call overlap(ex)
  call overlap(ey)
  call overlap(ez)
  if (do_shear .and. shear .ne. 0. .and. g%n(2) > 1) then
     call shear_translate(ex)
     call shear_translate(ey)
     call shear_translate(ez)
  endif
END SUBROUTINE e_boundaries
! Boundary conditions for magnetic fields
!=====================================================================!
SUBROUTINE b_boundaries(bx,by,bz)
  USE grid_m, ONLY : g
  USE params, ONLY : do_shear, shear
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: bx, by, bz

  call overlap(bx)
  call overlap(by)
  call overlap(bz)
  if (do_shear .and. shear .ne. 0. .and. g%n(2) > 1) then
     call shear_translate(bx)
     call shear_translate(by)
     call shear_translate(bz)
  endif
END SUBROUTINE b_boundaries
! Boundary condition for electric potential
!=====================================================================!
SUBROUTINE phi_boundaries(phi)
  USE grid_m, ONLY : g
  USE params, ONLY : do_shear, shear
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: phi

  call overlap(phi)
  if (do_shear .and. shear .ne. 0. .and. g%n(2) > 1) call shear_translate(phi)
END SUBROUTINE phi_boundaries
! Boundary condition for magnetic potential
!=====================================================================!
SUBROUTINE phib_boundaries(phi)
  USE grid_m, ONLY : g
  USE params, ONLY : do_shear, shear
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: phi

  call overlap(phi)
  if (do_shear .and. shear .ne. 0. .and. g%n(2) > 1) call shear_translate(phi)
END SUBROUTINE phib_boundaries
! Initialization for source (number density and number-density-flux) boundaries
!=======================================================================
SUBROUTINE init_source_boundaries
END SUBROUTINE init_source_boundaries
! Boundaries for ion-flux, including summation in ghostzones
! If add is false, we should not do summation in boundaries
!=======================================================================
SUBROUTINE impose_source_boundaries(n,nUx,nUy,nUz,add)
  USE grid_m,  ONLY : g
  USE species, ONLY : nspecies
  USE params,  ONLY : do_shear, shear
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3),nspecies) :: n, nUx, nUy, nUz
  real, dimension(g%n(1)) :: x_shear
  integer :: isp, ix, iy, iz
  logical :: add

  if (.not. add) return

  ! We have flux in boundaries; nU[xyz] is the full field.
  if (do_shear .and. shear .ne. 0.) then
    ! Transform to fluctuating field
    ! FIXME Take in to account it should only be done at lower and upper X-MPI domains
    !x_shear = g%x - (g%gs(1) / 2. + g%grlb(1))
    !do isp=1,nspecies; do iz=1, g%n(3); do iy=1, g%n(2); do ix=1, g%n(1)
    !  nUy(ix,iy,iz,isp) = nUy(ix,iy,iz,isp) - shear * x_shear(ix)
    !enddo; enddo; enddo; enddo
    if (g%n(2) > 1) then
       shear = -shear
       call shear_translate(  n)
       call shear_translate(nUx)
       call shear_translate(nUy)
       call shear_translate(nUz)
       shear = -shear
    endif
  endif 

  call overlap_add(nUx)
  call overlap_add(nUy)
  call overlap_add(nUz)
  call overlap_add(n)

  if (do_shear .and. shear .ne. 0. .and. g%n(2) > 1) then
     call shear_translate(  n)
     call shear_translate(nUx)
     call shear_translate(nUy)
     call shear_translate(nUz)
  endif
END SUBROUTINE impose_source_boundaries
! Particle boundary conditions
!=====================================================================!
SUBROUTINE impose_particle_boundaries
  USE params,  ONLY : nspecies, time, do_shear, shear, mpi
  USE grid_m,  ONLY : g
  USE species, ONLY : sp, particle
  implicit none
  type(particle), pointer, dimension(:) :: pa
  type(particle), pointer               :: p
  integer :: isp, ip, dq
  real(kind=8) :: boost_y, boost_vy
  ! Do shear periodic transformation in velocity and position if particle crosses x-boundary
  if (mpi%me(1)==0 .or. mpi%me(1)==mpi%n(1)-1) then
    if (do_shear .and. shear .ne. 0.) then
      boost_vy = shear * g%gs(1)              ! Boost in y-velocity
      boost_y  = shear * g%gn(1) * time       ! Boost in y-position in units of grid cells
      do isp=1, nspecies
        pa => sp(isp)%particle
        do ip=1, sp(isp)%np
          p => pa(ip)
          if (p%q(1) < 0) then                ! Particle has exited lower boundary
            p%q(1) = p%q(1) + g%gn(1)         ! Translate a box length
            p%p(2) = p%p(2) + boost_vy        ! Corresponding change in velocity
 
            dq = floor(p%r(2) + boost_y)      ! Delta grid cells corresponding to shear * time * gsx 
            p%r(2) = p%r(2) + boost_y - dq    ! Fractional change inside the grid cell
            p%q(2) = p%q(2) + dq              ! Integer change in grid cells
          endif
          if (p%q(1) >= g%gn(1)) then         ! Particle has exited upper boundary
            p%q(1) = p%q(1) - g%gn(1)         ! Translate a box length
            p%p(2) = p%p(2) - boost_vy        ! Corresponding change in velocity
 
            dq = floor(p%r(2) - boost_y)
            p%r(2) = p%r(2) - boost_y - dq
            p%q(2) = p%q(2) + dq
          endif
        enddo
      enddo
    endif
  endif
END SUBROUTINE impose_particle_boundaries
!=======================================================================
