! id="Patch/hybrid/mesh_sources.f90 $ Date: 2013-08-30 00:43:49 +0200 $"
! vim: nowrap
!-----------------------------------------------------------------------
SUBROUTINE mesh_sources
  USE params,        only : nspecies, mdim, mcoord, mid, stdall, rank, &
                            do_vth, do_local_patch
  USE species,       only : cellvector, particle, sp, fields, vthb
  USE grid_m,        only : g
  implicit none
  type(particle), pointer, dimension(:) :: pa
  type(particle), pointer :: pp
  integer                 :: isp
  real, dimension(mcoord) :: p, vel
  real, dimension(mdim)   :: r
  real                    :: p2, og, vv, v, w, ww, wa, wb, wc, wd, s, &
                             sm1, sm2
  real, parameter         :: one_sixth = 1./6., two_third = 2./3., &
                             four_third = 4./3.
  integer                 :: ip, np, off
  integer                 :: ix, iy, iz, jx, jy, jz, kx, ky, kz         ! grid counters
  logical                 :: up, err
  integer, save           :: itm=0
  integer, parameter      :: verbose=0
  integer                 :: lastcellindex                              ! index to last cell
  integer, parameter      :: sdb=-2, slb=-1, sub=2
  real, dimension(sdb:sub,sdb:sub,sdb:sub) :: vx, vy, vz, d, vth        ! local field patch
  real, dimension(:), allocatable :: wxm,wx0,wx1,wx2,wym,wy0,wy1,wy2, &
    wzm,wz0,wz1,wz2,dxn,dxm,dx0,dx1,dx2,dyn,dym,dy0,dy1,dy2,dzn,dzm, &
    dz0,dz1,dz2,v2
  real(kind=8), external :: wallclock
  character(len=mid):: id = &
    'Patch/hybrid/mesh_sources.f90 $ Date: 2013-09-02 18:22:45 +0200 $'
!.......................................................................
  call print_id(id)
  itm = itm + 1
  np = maxval(sp%nmaxcell)
  if (np > 0) allocate(wxm(np),wx0(np),wx1(np),wx2(np), &
                       wym(np),wy0(np),wy1(np),wy2(np), &
                       wzm(np),wz0(np),wz1(np),wz2(np),&
                       dxn(np),dxm(np),dx0(np),dx1(np),dx2(np), &
                       dyn(np),dym(np),dy0(np),dy1(np),dy2(np), &
                       dzn(np),dzm(np),dz0(np),dz1(np),dz2(np),v2(np))
  do isp=1,nspecies                                                     ! loop over species
    do iz=1,g%n(3)                                                      ! zero fields
    do iy=1,g%n(2)
    do ix=1,g%n(1)
      fields(ix,iy,iz,isp)%d = 0.                                       ! zap charges
      fields(ix,iy,iz,isp)%v = 0.                                       ! zap velocities
    enddo
    if (do_vth) then
      do ix=1,g%n(1)
        fields(ix,iy,iz,isp)%vth = 0.                                   ! zap thermal vel
      enddo
    end if
    enddo
    enddo
    if (sp(isp)%np .eq. 0) cycle                                        ! if no particles

    pa => sp(isp)%particle                                              ! shortcut 
    lastcellindex = 1
    do iz=1,g%n(3)                                                      ! loop over cells
    do iy=1,g%n(2)
    do ix=1,g%n(1)
      if (fields(ix,iy,iz,isp)%i .ge. lastcellindex) then               ! avoid empty cell
!-----------------------------------------------------------------------
! In the loop below, the velocities are correct only for particles with 
! mass, but that is unimportant, as long as those field components are 
! not used for anything.  We do want the densities of all species to be
! computed, and the directions of the velocities of massless particles 
! are nice to have too.
!-----------------------------------------------------------------------
        np = fields(ix,iy,iz,isp)%i - lastcellindex + 1                 ! nr of parts in cell
        if (np > 0) then
          vx=0.; vy=0.; vz=0.; d=0.; if (do_vth) vth=0.                 ! zero fluxes for cell
          off = lastcellindex-1
          do ip=1,np                                                    ! parts in cell
            ! ---------------------------------------------------------
            ! Find signed distance to center points in all directions.
            ! Project index into allowed range for interpolation.
            r = pa(ip+off)%r
            w = pa(ip+off)%w
            !-------------
            ! X_COORD CENTERED
            s = r(1); sm1 = 1. - s; sm2 = 2. - s 
            wxm(ip) = one_sixth * w * sm1*sm1*sm1
            wx0(ip) = w * (two_third - 0.5*s*s*sm2)
            wx1(ip) = w * (two_third - 0.5*sm1*sm1*(1.+ s)) 
            wx2(ip) = w - (wxm(ip) + wx0(ip) + wx1(ip))
            !-------------
            ! Y_COORD CENTERED
            s = r(2); sm1 = 1. - s; sm2 = 2. - s 
            wym(ip) = one_sixth * sm1*sm1*sm1
            wy0(ip) = two_third - 0.5*s*s*sm2
            wy1(ip) = two_third - 0.5*sm1*sm1*(1.+ s) 
            wy2(ip) = 1. - (wym(ip) + wy0(ip) + wy1(ip))
            !-------------
            ! Z_COORD CENTERED
            s = r(3); sm1 = 1. - s; sm2 = 2. - s 
            wzm(ip) = one_sixth * sm1*sm1*sm1
            wz0(ip) = two_third - 0.5*s*s*sm2
            wz1(ip) = two_third - 0.5*sm1*sm1*(1.+ s) 
            wz2(ip) = 1. - (wzm(ip) + wz0(ip) + wz1(ip))
            !-------------
            ! X_COORD SHIFTED IN X
            v = pa(ip+off)%p(1)                                         ! velocity
            v2(ip) = v*v
            v = v*w   
            s = r(1) - 0.5; up = s >= 0    
            s = s + merge(0.,1.,up); sm1 = 1. - s; sm2 = 2. - s
            wa = one_sixth * v * sm1*sm1*sm1
            wb = v * (two_third - 0.5*s*s*sm2)
            wc = v * (two_third - 0.5*sm1*sm1*(1.+ s)) 
            wd = v - (wa + wb + wc)
            dxn(ip) = merge(0., wa, up)
            dxm(ip) = merge(wa, wb, up)
            dx0(ip) = merge(wb, wc, up)
            dx1(ip) = merge(wc, wd, up)
            dx2(ip) = merge(wd, 0., up)
            !-------------
            ! Y_COORD SHIFTED IN Y
            v = pa(ip+off)%p(2)                                         ! velocity
            v2(ip) = v2(ip) + v*v
            s = r(2) - 0.5; up = s >= 0
            s = s + merge(0.,1.,up); sm1 = 1. - s; sm2 = 2. - s
            wa = one_sixth * v * sm1*sm1*sm1
            wb = v * (two_third - 0.5*s*s*sm2)
            wc = v * (two_third - 0.5*sm1*sm1*(1.+ s)) 
            wd = v - (wa + wb + wc)
            dyn(ip) = merge(0., wa, up)
            dym(ip) = merge(wa, wb, up)
            dy0(ip) = merge(wb, wc, up)
            dy1(ip) = merge(wc, wd, up)
            dy2(ip) = merge(wd, 0., up)
            !-------------
            ! Z_COORD SHIFTED IN Z
            v = pa(ip+off)%p(3)                                         ! velocity
            v2(ip) = v2(ip) + v*v
            s = r(3) - 0.5; up = s >= 0
            s = s + merge(0.,1.,up); sm1 = 1. - s; sm2 = 2. - s
            wa = one_sixth * v * sm1*sm1*sm1
            wb = v * (two_third - 0.5*s*s*sm2)
            wc = v * (two_third - 0.5*sm1*sm1*(1.+ s)) 
            wd = v - (wa + wb + wc)
            dzn(ip) = merge(0., wa, up)
            dzm(ip) = merge(wa, wb, up)
            dz0(ip) = merge(wb, wc, up)
            dz1(ip) = merge(wc, wd, up)
            dz2(ip) = merge(wd, 0., up)
          enddo
          !
          ! Loop over np and do the full 64 + 3*36 = 172 interpolations
          !!$DEC LOOP COUNT=12
          do ip=1,np
            !-----------------------------------------------------------------------
            ! density and vx block
            ww = wym(ip)*wzm(ip)
             d(-1,-1,-1) =  d(-1,-1,-1) + wxm(ip)*ww
             d( 0,-1,-1) =  d( 0,-1,-1) + wx0(ip)*ww
             d( 1,-1,-1) =  d( 1,-1,-1) + wx1(ip)*ww
             d( 2,-1,-1) =  d( 2,-1,-1) + wx2(ip)*ww
            vx(-2,-1,-1) = vx(-2,-1,-1) + dxn(ip)*ww
            vx(-1,-1,-1) = vx(-1,-1,-1) + dxm(ip)*ww
            vx( 0,-1,-1) = vx( 0,-1,-1) + dx0(ip)*ww
            vx( 1,-1,-1) = vx( 1,-1,-1) + dx1(ip)*ww
            vx( 2,-1,-1) = vx( 2,-1,-1) + dx2(ip)*ww
            ww = wy0(ip)*wzm(ip)
             d(-1, 0,-1) =  d(-1, 0,-1) + wxm(ip)*ww
             d( 0, 0,-1) =  d( 0, 0,-1) + wx0(ip)*ww
             d( 1, 0,-1) =  d( 1, 0,-1) + wx1(ip)*ww
             d( 2, 0,-1) =  d( 2, 0,-1) + wx2(ip)*ww
            vx(-2, 0,-1) = vx(-2, 0,-1) + dxn(ip)*ww
            vx(-1, 0,-1) = vx(-1, 0,-1) + dxm(ip)*ww
            vx( 0, 0,-1) = vx( 0, 0,-1) + dx0(ip)*ww
            vx( 1, 0,-1) = vx( 1, 0,-1) + dx1(ip)*ww
            vx( 2, 0,-1) = vx( 2, 0,-1) + dx2(ip)*ww
            ww = wy1(ip)*wzm(ip)
             d(-1, 1,-1) =  d(-1, 1,-1) + wxm(ip)*ww
             d( 0, 1,-1) =  d( 0, 1,-1) + wx0(ip)*ww
             d( 1, 1,-1) =  d( 1, 1,-1) + wx1(ip)*ww
             d( 2, 1,-1) =  d( 2, 1,-1) + wx2(ip)*ww
            vx(-2, 1,-1) = vx(-2, 1,-1) + dxn(ip)*ww
            vx(-1, 1,-1) = vx(-1, 1,-1) + dxm(ip)*ww
            vx( 0, 1,-1) = vx( 0, 1,-1) + dx0(ip)*ww
            vx( 1, 1,-1) = vx( 1, 1,-1) + dx1(ip)*ww
            vx( 2, 1,-1) = vx( 2, 1,-1) + dx2(ip)*ww
            ww = wy2(ip)*wzm(ip)
             d(-1, 2,-1) =  d(-1, 2,-1) + wxm(ip)*ww
             d( 0, 2,-1) =  d( 0, 2,-1) + wx0(ip)*ww
             d( 1, 2,-1) =  d( 1, 2,-1) + wx1(ip)*ww
             d( 2, 2,-1) =  d( 2, 2,-1) + wx2(ip)*ww
            vx(-2, 2,-1) = vx(-2, 2,-1) + dxn(ip)*ww
            vx(-1, 2,-1) = vx(-1, 2,-1) + dxm(ip)*ww
            vx( 0, 2,-1) = vx( 0, 2,-1) + dx0(ip)*ww
            vx( 1, 2,-1) = vx( 1, 2,-1) + dx1(ip)*ww
            vx( 2, 2,-1) = vx( 2, 2,-1) + dx2(ip)*ww
            ww = wym(ip)*wz0(ip)
             d(-1,-1, 0) =  d(-1,-1, 0) + wxm(ip)*ww
             d( 0,-1, 0) =  d( 0,-1, 0) + wx0(ip)*ww
             d( 1,-1, 0) =  d( 1,-1, 0) + wx1(ip)*ww
             d( 2,-1, 0) =  d( 2,-1, 0) + wx2(ip)*ww
            vx(-2,-1, 0) = vx(-2,-1, 0) + dxn(ip)*ww
            vx(-1,-1, 0) = vx(-1,-1, 0) + dxm(ip)*ww
            vx( 0,-1, 0) = vx( 0,-1, 0) + dx0(ip)*ww
            vx( 1,-1, 0) = vx( 1,-1, 0) + dx1(ip)*ww
            vx( 2,-1, 0) = vx( 2,-1, 0) + dx2(ip)*ww
            ww = wy0(ip)*wz0(ip)
             d(-1, 0, 0) =  d(-1, 0, 0) + wxm(ip)*ww
             d( 0, 0, 0) =  d( 0, 0, 0) + wx0(ip)*ww
             d( 1, 0, 0) =  d( 1, 0, 0) + wx1(ip)*ww
             d( 2, 0, 0) =  d( 2, 0, 0) + wx2(ip)*ww
            vx(-2, 0, 0) = vx(-2, 0, 0) + dxn(ip)*ww
            vx(-1, 0, 0) = vx(-1, 0, 0) + dxm(ip)*ww
            vx( 0, 0, 0) = vx( 0, 0, 0) + dx0(ip)*ww
            vx( 1, 0, 0) = vx( 1, 0, 0) + dx1(ip)*ww
            vx( 2, 0, 0) = vx( 2, 0, 0) + dx2(ip)*ww
            ww = wy1(ip)*wz0(ip)
             d(-1, 1, 0) =  d(-1, 1, 0) + wxm(ip)*ww
             d( 0, 1, 0) =  d( 0, 1, 0) + wx0(ip)*ww
             d( 1, 1, 0) =  d( 1, 1, 0) + wx1(ip)*ww
             d( 2, 1, 0) =  d( 2, 1, 0) + wx2(ip)*ww
            vx(-2, 1, 0) = vx(-2, 1, 0) + dxn(ip)*ww
            vx(-1, 1, 0) = vx(-1, 1, 0) + dxm(ip)*ww
            vx( 0, 1, 0) = vx( 0, 1, 0) + dx0(ip)*ww
            vx( 1, 1, 0) = vx( 1, 1, 0) + dx1(ip)*ww
            vx( 2, 1, 0) = vx( 2, 1, 0) + dx2(ip)*ww
            ww = wy2(ip)*wz0(ip)
             d(-1, 2, 0) =  d(-1, 2, 0) + wxm(ip)*ww
             d( 0, 2, 0) =  d( 0, 2, 0) + wx0(ip)*ww
             d( 1, 2, 0) =  d( 1, 2, 0) + wx1(ip)*ww
             d( 2, 2, 0) =  d( 2, 2, 0) + wx2(ip)*ww
            vx(-2, 2, 0) = vx(-2, 2, 0) + dxn(ip)*ww
            vx(-1, 2, 0) = vx(-1, 2, 0) + dxm(ip)*ww
            vx( 0, 2, 0) = vx( 0, 2, 0) + dx0(ip)*ww
            vx( 1, 2, 0) = vx( 1, 2, 0) + dx1(ip)*ww
            vx( 2, 2, 0) = vx( 2, 2, 0) + dx2(ip)*ww
            ww = wym(ip)*wz1(ip)
             d(-1,-1, 1) =  d(-1,-1, 1) + wxm(ip)*ww
             d( 0,-1, 1) =  d( 0,-1, 1) + wx0(ip)*ww
             d( 1,-1, 1) =  d( 1,-1, 1) + wx1(ip)*ww
             d( 2,-1, 1) =  d( 2,-1, 1) + wx2(ip)*ww
            vx(-2,-1, 1) = vx(-2,-1, 1) + dxn(ip)*ww
            vx(-1,-1, 1) = vx(-1,-1, 1) + dxm(ip)*ww
            vx( 0,-1, 1) = vx( 0,-1, 1) + dx0(ip)*ww
            vx( 1,-1, 1) = vx( 1,-1, 1) + dx1(ip)*ww
            vx( 2,-1, 1) = vx( 2,-1, 1) + dx2(ip)*ww
            ww = wy0(ip)*wz1(ip)
             d(-1, 0, 1) =  d(-1, 0, 1) + wxm(ip)*ww
             d( 0, 0, 1) =  d( 0, 0, 1) + wx0(ip)*ww
             d( 1, 0, 1) =  d( 1, 0, 1) + wx1(ip)*ww
             d( 2, 0, 1) =  d( 2, 0, 1) + wx2(ip)*ww
            vx(-2, 0, 1) = vx(-2, 0, 1) + dxn(ip)*ww
            vx(-1, 0, 1) = vx(-1, 0, 1) + dxm(ip)*ww
            vx( 0, 0, 1) = vx( 0, 0, 1) + dx0(ip)*ww
            vx( 1, 0, 1) = vx( 1, 0, 1) + dx1(ip)*ww
            vx( 2, 0, 1) = vx( 2, 0, 1) + dx2(ip)*ww
            ww = wy1(ip)*wz1(ip)
             d(-1, 1, 1) =  d(-1, 1, 1) + wxm(ip)*ww
             d( 0, 1, 1) =  d( 0, 1, 1) + wx0(ip)*ww
             d( 1, 1, 1) =  d( 1, 1, 1) + wx1(ip)*ww
             d( 2, 1, 1) =  d( 2, 1, 1) + wx2(ip)*ww
            vx(-2, 1, 1) = vx(-2, 1, 1) + dxn(ip)*ww
            vx(-1, 1, 1) = vx(-1, 1, 1) + dxm(ip)*ww
            vx( 0, 1, 1) = vx( 0, 1, 1) + dx0(ip)*ww
            vx( 1, 1, 1) = vx( 1, 1, 1) + dx1(ip)*ww
            vx( 2, 1, 1) = vx( 2, 1, 1) + dx2(ip)*ww
            ww = wy2(ip)*wz1(ip)
             d(-1, 2, 1) =  d(-1, 2, 1) + wxm(ip)*ww
             d( 0, 2, 1) =  d( 0, 2, 1) + wx0(ip)*ww
             d( 1, 2, 1) =  d( 1, 2, 1) + wx1(ip)*ww
             d( 2, 2, 1) =  d( 2, 2, 1) + wx2(ip)*ww
            vx(-2, 2, 1) = vx(-2, 2, 1) + dxn(ip)*ww
            vx(-1, 2, 1) = vx(-1, 2, 1) + dxm(ip)*ww
            vx( 0, 2, 1) = vx( 0, 2, 1) + dx0(ip)*ww
            vx( 1, 2, 1) = vx( 1, 2, 1) + dx1(ip)*ww
            vx( 2, 2, 1) = vx( 2, 2, 1) + dx2(ip)*ww
            ww = wym(ip)*wz2(ip)
             d(-1,-1, 2) =  d(-1,-1, 2) + wxm(ip)*ww
             d( 0,-1, 2) =  d( 0,-1, 2) + wx0(ip)*ww
             d( 1,-1, 2) =  d( 1,-1, 2) + wx1(ip)*ww
             d( 2,-1, 2) =  d( 2,-1, 2) + wx2(ip)*ww
            vx(-2,-1, 2) = vx(-2,-1, 2) + dxn(ip)*ww
            vx(-1,-1, 2) = vx(-1,-1, 2) + dxm(ip)*ww
            vx( 0,-1, 2) = vx( 0,-1, 2) + dx0(ip)*ww
            vx( 1,-1, 2) = vx( 1,-1, 2) + dx1(ip)*ww
            vx( 2,-1, 2) = vx( 2,-1, 2) + dx2(ip)*ww
            ww = wy0(ip)*wz2(ip)
             d(-1, 0, 2) =  d(-1, 0, 2) + wxm(ip)*ww
             d( 0, 0, 2) =  d( 0, 0, 2) + wx0(ip)*ww
             d( 1, 0, 2) =  d( 1, 0, 2) + wx1(ip)*ww
             d( 2, 0, 2) =  d( 2, 0, 2) + wx2(ip)*ww
            vx(-2, 0, 2) = vx(-2, 0, 2) + dxn(ip)*ww
            vx(-1, 0, 2) = vx(-1, 0, 2) + dxm(ip)*ww
            vx( 0, 0, 2) = vx( 0, 0, 2) + dx0(ip)*ww
            vx( 1, 0, 2) = vx( 1, 0, 2) + dx1(ip)*ww
            vx( 2, 0, 2) = vx( 2, 0, 2) + dx2(ip)*ww
            ww = wy1(ip)*wz2(ip)
             d(-1, 1, 2) =  d(-1, 1, 2) + wxm(ip)*ww
             d( 0, 1, 2) =  d( 0, 1, 2) + wx0(ip)*ww
             d( 1, 1, 2) =  d( 1, 1, 2) + wx1(ip)*ww
             d( 2, 1, 2) =  d( 2, 1, 2) + wx2(ip)*ww
            vx(-2, 1, 2) = vx(-2, 1, 2) + dxn(ip)*ww
            vx(-1, 1, 2) = vx(-1, 1, 2) + dxm(ip)*ww
            vx( 0, 1, 2) = vx( 0, 1, 2) + dx0(ip)*ww
            vx( 1, 1, 2) = vx( 1, 1, 2) + dx1(ip)*ww
            vx( 2, 1, 2) = vx( 2, 1, 2) + dx2(ip)*ww
            ww = wy2(ip)*wz2(ip)
             d(-1, 2, 2) =  d(-1, 2, 2) + wxm(ip)*ww
             d( 0, 2, 2) =  d( 0, 2, 2) + wx0(ip)*ww
             d( 1, 2, 2) =  d( 1, 2, 2) + wx1(ip)*ww
             d( 2, 2, 2) =  d( 2, 2, 2) + wx2(ip)*ww
            vx(-2, 2, 2) = vx(-2, 2, 2) + dxn(ip)*ww
            vx(-1, 2, 2) = vx(-1, 2, 2) + dxm(ip)*ww
            vx( 0, 2, 2) = vx( 0, 2, 2) + dx0(ip)*ww
            vx( 1, 2, 2) = vx( 1, 2, 2) + dx1(ip)*ww
            vx( 2, 2, 2) = vx( 2, 2, 2) + dx2(ip)*ww
            !-----------------------------------------------------------
            ! vy block
            ww = dyn(ip)*wzm(ip)
            vy(-1,-2,-1) = vy(-1,-2,-1) + wxm(ip)*ww
            vy( 0,-2,-1) = vy( 0,-2,-1) + wx0(ip)*ww
            vy( 1,-2,-1) = vy( 1,-2,-1) + wx1(ip)*ww
            vy( 2,-2,-1) = vy( 2,-2,-1) + wx2(ip)*ww
            ww = dym(ip)*wzm(ip)
            vy(-1,-1,-1) = vy(-1,-1,-1) + wxm(ip)*ww
            vy( 0,-1,-1) = vy( 0,-1,-1) + wx0(ip)*ww
            vy( 1,-1,-1) = vy( 1,-1,-1) + wx1(ip)*ww
            vy( 2,-1,-1) = vy( 2,-1,-1) + wx2(ip)*ww
            ww = dy0(ip)*wzm(ip)
            vy(-1, 0,-1) = vy(-1, 0,-1) + wxm(ip)*ww
            vy( 0, 0,-1) = vy( 0, 0,-1) + wx0(ip)*ww
            vy( 1, 0,-1) = vy( 1, 0,-1) + wx1(ip)*ww
            vy( 2, 0,-1) = vy( 2, 0,-1) + wx2(ip)*ww
            ww = dy1(ip)*wzm(ip)
            vy(-1, 1,-1) = vy(-1, 1,-1) + wxm(ip)*ww
            vy( 0, 1,-1) = vy( 0, 1,-1) + wx0(ip)*ww
            vy( 1, 1,-1) = vy( 1, 1,-1) + wx1(ip)*ww
            vy( 2, 1,-1) = vy( 2, 1,-1) + wx2(ip)*ww
            ww = dy2(ip)*wzm(ip)
            vy(-1, 2,-1) = vy(-1, 2,-1) + wxm(ip)*ww
            vy( 0, 2,-1) = vy( 0, 2,-1) + wx0(ip)*ww
            vy( 1, 2,-1) = vy( 1, 2,-1) + wx1(ip)*ww
            vy( 2, 2,-1) = vy( 2, 2,-1) + wx2(ip)*ww
            ww = dyn(ip)*wz0(ip)
            vy(-1,-2, 0) = vy(-1,-2, 0) + wxm(ip)*ww
            vy( 0,-2, 0) = vy( 0,-2, 0) + wx0(ip)*ww
            vy( 1,-2, 0) = vy( 1,-2, 0) + wx1(ip)*ww
            vy( 2,-2, 0) = vy( 2,-2, 0) + wx2(ip)*ww
            ww = dym(ip)*wz0(ip)
            vy(-1,-1, 0) = vy(-1,-1, 0) + wxm(ip)*ww
            vy( 0,-1, 0) = vy( 0,-1, 0) + wx0(ip)*ww
            vy( 1,-1, 0) = vy( 1,-1, 0) + wx1(ip)*ww
            vy( 2,-1, 0) = vy( 2,-1, 0) + wx2(ip)*ww
            ww = dy0(ip)*wz0(ip)
            vy(-1, 0, 0) = vy(-1, 0, 0) + wxm(ip)*ww
            vy( 0, 0, 0) = vy( 0, 0, 0) + wx0(ip)*ww
            vy( 1, 0, 0) = vy( 1, 0, 0) + wx1(ip)*ww
            vy( 2, 0, 0) = vy( 2, 0, 0) + wx2(ip)*ww
            ww = dy1(ip)*wz0(ip)
            vy(-1, 1, 0) = vy(-1, 1, 0) + wxm(ip)*ww
            vy( 0, 1, 0) = vy( 0, 1, 0) + wx0(ip)*ww
            vy( 1, 1, 0) = vy( 1, 1, 0) + wx1(ip)*ww
            vy( 2, 1, 0) = vy( 2, 1, 0) + wx2(ip)*ww
            ww = dy2(ip)*wz0(ip)
            vy(-1, 2, 0) = vy(-1, 2, 0) + wxm(ip)*ww
            vy( 0, 2, 0) = vy( 0, 2, 0) + wx0(ip)*ww
            vy( 1, 2, 0) = vy( 1, 2, 0) + wx1(ip)*ww
            vy( 2, 2, 0) = vy( 2, 2, 0) + wx2(ip)*ww
            ww = dyn(ip)*wz1(ip)
            vy(-1,-2, 1) = vy(-1,-2, 1) + wxm(ip)*ww
            vy( 0,-2, 1) = vy( 0,-2, 1) + wx0(ip)*ww
            vy( 1,-2, 1) = vy( 1,-2, 1) + wx1(ip)*ww
            vy( 2,-2, 1) = vy( 2,-2, 1) + wx2(ip)*ww
            ww = dym(ip)*wz1(ip)
            vy(-1,-1, 1) = vy(-1,-1, 1) + wxm(ip)*ww
            vy( 0,-1, 1) = vy( 0,-1, 1) + wx0(ip)*ww
            vy( 1,-1, 1) = vy( 1,-1, 1) + wx1(ip)*ww
            vy( 2,-1, 1) = vy( 2,-1, 1) + wx2(ip)*ww
            ww = dy0(ip)*wz1(ip)
            vy(-1, 0, 1) = vy(-1, 0, 1) + wxm(ip)*ww
            vy( 0, 0, 1) = vy( 0, 0, 1) + wx0(ip)*ww
            vy( 1, 0, 1) = vy( 1, 0, 1) + wx1(ip)*ww
            vy( 2, 0, 1) = vy( 2, 0, 1) + wx2(ip)*ww
            ww = dy1(ip)*wz1(ip)
            vy(-1, 1, 1) = vy(-1, 1, 1) + wxm(ip)*ww
            vy( 0, 1, 1) = vy( 0, 1, 1) + wx0(ip)*ww
            vy( 1, 1, 1) = vy( 1, 1, 1) + wx1(ip)*ww
            vy( 2, 1, 1) = vy( 2, 1, 1) + wx2(ip)*ww
            ww = dy2(ip)*wz1(ip)
            vy(-1, 2, 1) = vy(-1, 2, 1) + wxm(ip)*ww
            vy( 0, 2, 1) = vy( 0, 2, 1) + wx0(ip)*ww
            vy( 1, 2, 1) = vy( 1, 2, 1) + wx1(ip)*ww
            vy( 2, 2, 1) = vy( 2, 2, 1) + wx2(ip)*ww
            ww = dyn(ip)*wz2(ip)
            vy(-1,-2, 2) = vy(-1,-2, 2) + wxm(ip)*ww
            vy( 0,-2, 2) = vy( 0,-2, 2) + wx0(ip)*ww
            vy( 1,-2, 2) = vy( 1,-2, 2) + wx1(ip)*ww
            vy( 2,-2, 2) = vy( 2,-2, 2) + wx2(ip)*ww
            ww = dym(ip)*wz2(ip)
            vy(-1,-1, 2) = vy(-1,-1, 2) + wxm(ip)*ww
            vy( 0,-1, 2) = vy( 0,-1, 2) + wx0(ip)*ww
            vy( 1,-1, 2) = vy( 1,-1, 2) + wx1(ip)*ww
            vy( 2,-1, 2) = vy( 2,-1, 2) + wx2(ip)*ww
            ww = dy0(ip)*wz2(ip)
            vy(-1, 0, 2) = vy(-1, 0, 2) + wxm(ip)*ww
            vy( 0, 0, 2) = vy( 0, 0, 2) + wx0(ip)*ww
            vy( 1, 0, 2) = vy( 1, 0, 2) + wx1(ip)*ww
            vy( 2, 0, 2) = vy( 2, 0, 2) + wx2(ip)*ww
            ww = dy1(ip)*wz2(ip)
            vy(-1, 1, 2) = vy(-1, 1, 2) + wxm(ip)*ww
            vy( 0, 1, 2) = vy( 0, 1, 2) + wx0(ip)*ww
            vy( 1, 1, 2) = vy( 1, 1, 2) + wx1(ip)*ww
            vy( 2, 1, 2) = vy( 2, 1, 2) + wx2(ip)*ww
            ww = dy2(ip)*wz2(ip)
            vy(-1, 2, 2) = vy(-1, 2, 2) + wxm(ip)*ww
            vy( 0, 2, 2) = vy( 0, 2, 2) + wx0(ip)*ww
            vy( 1, 2, 2) = vy( 1, 2, 2) + wx1(ip)*ww
            vy( 2, 2, 2) = vy( 2, 2, 2) + wx2(ip)*ww
            !-----------------------------------------------------------
            ! vz block
            ww = wym(ip)*dzn(ip)
            vz(-1,-1,-2) = vz(-1,-1,-2) + wxm(ip)*ww
            vz( 0,-1,-2) = vz( 0,-1,-2) + wx0(ip)*ww
            vz( 1,-1,-2) = vz( 1,-1,-2) + wx1(ip)*ww
            vz( 2,-1,-2) = vz( 2,-1,-2) + wx2(ip)*ww
            ww = wy0(ip)*dzn(ip)
            vz(-1, 0,-2) = vz(-1, 0,-2) + wxm(ip)*ww
            vz( 0, 0,-2) = vz( 0, 0,-2) + wx0(ip)*ww
            vz( 1, 0,-2) = vz( 1, 0,-2) + wx1(ip)*ww
            vz( 2, 0,-2) = vz( 2, 0,-2) + wx2(ip)*ww
            ww = wy1(ip)*dzn(ip)
            vz(-1, 1,-2) = vz(-1, 1,-2) + wxm(ip)*ww
            vz( 0, 1,-2) = vz( 0, 1,-2) + wx0(ip)*ww
            vz( 1, 1,-2) = vz( 1, 1,-2) + wx1(ip)*ww
            vz( 2, 1,-2) = vz( 2, 1,-2) + wx2(ip)*ww
            ww = wy2(ip)*dzn(ip)
            vz(-1, 2,-2) = vz(-1, 2,-2) + wxm(ip)*ww
            vz( 0, 2,-2) = vz( 0, 2,-2) + wx0(ip)*ww
            vz( 1, 2,-2) = vz( 1, 2,-2) + wx1(ip)*ww
            vz( 2, 2,-2) = vz( 2, 2,-2) + wx2(ip)*ww
            ww = wym(ip)*dzm(ip)
            vz(-1,-1,-1) = vz(-1,-1,-1) + wxm(ip)*ww
            vz( 0,-1,-1) = vz( 0,-1,-1) + wx0(ip)*ww
            vz( 1,-1,-1) = vz( 1,-1,-1) + wx1(ip)*ww
            vz( 2,-1,-1) = vz( 2,-1,-1) + wx2(ip)*ww
            ww = wy0(ip)*dzm(ip)
            vz(-1, 0,-1) = vz(-1, 0,-1) + wxm(ip)*ww
            vz( 0, 0,-1) = vz( 0, 0,-1) + wx0(ip)*ww
            vz( 1, 0,-1) = vz( 1, 0,-1) + wx1(ip)*ww
            vz( 2, 0,-1) = vz( 2, 0,-1) + wx2(ip)*ww
            ww = wy1(ip)*dzm(ip)
            vz(-1, 1,-1) = vz(-1, 1,-1) + wxm(ip)*ww
            vz( 0, 1,-1) = vz( 0, 1,-1) + wx0(ip)*ww
            vz( 1, 1,-1) = vz( 1, 1,-1) + wx1(ip)*ww
            vz( 2, 1,-1) = vz( 2, 1,-1) + wx2(ip)*ww
            ww = wy2(ip)*dzm(ip)
            vz(-1, 2,-1) = vz(-1, 2,-1) + wxm(ip)*ww
            vz( 0, 2,-1) = vz( 0, 2,-1) + wx0(ip)*ww
            vz( 1, 2,-1) = vz( 1, 2,-1) + wx1(ip)*ww
            vz( 2, 2,-1) = vz( 2, 2,-1) + wx2(ip)*ww
            ww = wym(ip)*dz0(ip)
            vz(-1,-1, 0) = vz(-1,-1, 0) + wxm(ip)*ww
            vz( 0,-1, 0) = vz( 0,-1, 0) + wx0(ip)*ww
            vz( 1,-1, 0) = vz( 1,-1, 0) + wx1(ip)*ww
            vz( 2,-1, 0) = vz( 2,-1, 0) + wx2(ip)*ww
            ww = wy0(ip)*dz0(ip)
            vz(-1, 0, 0) = vz(-1, 0, 0) + wxm(ip)*ww
            vz( 0, 0, 0) = vz( 0, 0, 0) + wx0(ip)*ww
            vz( 1, 0, 0) = vz( 1, 0, 0) + wx1(ip)*ww
            vz( 2, 0, 0) = vz( 2, 0, 0) + wx2(ip)*ww
            ww = wy1(ip)*dz0(ip)
            vz(-1, 1, 0) = vz(-1, 1, 0) + wxm(ip)*ww
            vz( 0, 1, 0) = vz( 0, 1, 0) + wx0(ip)*ww
            vz( 1, 1, 0) = vz( 1, 1, 0) + wx1(ip)*ww
            vz( 2, 1, 0) = vz( 2, 1, 0) + wx2(ip)*ww
            ww = wy2(ip)*dz0(ip)
            vz(-1, 2, 0) = vz(-1, 2, 0) + wxm(ip)*ww
            vz( 0, 2, 0) = vz( 0, 2, 0) + wx0(ip)*ww
            vz( 1, 2, 0) = vz( 1, 2, 0) + wx1(ip)*ww
            vz( 2, 2, 0) = vz( 2, 2, 0) + wx2(ip)*ww
            ww = wym(ip)*dz1(ip)
            vz(-1,-1, 1) = vz(-1,-1, 1) + wxm(ip)*ww
            vz( 0,-1, 1) = vz( 0,-1, 1) + wx0(ip)*ww
            vz( 1,-1, 1) = vz( 1,-1, 1) + wx1(ip)*ww
            vz( 2,-1, 1) = vz( 2,-1, 1) + wx2(ip)*ww
            ww = wy0(ip)*dz1(ip)
            vz(-1, 0, 1) = vz(-1, 0, 1) + wxm(ip)*ww
            vz( 0, 0, 1) = vz( 0, 0, 1) + wx0(ip)*ww
            vz( 1, 0, 1) = vz( 1, 0, 1) + wx1(ip)*ww
            vz( 2, 0, 1) = vz( 2, 0, 1) + wx2(ip)*ww
            ww = wy1(ip)*dz1(ip)
            vz(-1, 1, 1) = vz(-1, 1, 1) + wxm(ip)*ww
            vz( 0, 1, 1) = vz( 0, 1, 1) + wx0(ip)*ww
            vz( 1, 1, 1) = vz( 1, 1, 1) + wx1(ip)*ww
            vz( 2, 1, 1) = vz( 2, 1, 1) + wx2(ip)*ww
            ww = wy2(ip)*dz1(ip)
            vz(-1, 2, 1) = vz(-1, 2, 1) + wxm(ip)*ww
            vz( 0, 2, 1) = vz( 0, 2, 1) + wx0(ip)*ww
            vz( 1, 2, 1) = vz( 1, 2, 1) + wx1(ip)*ww
            vz( 2, 2, 1) = vz( 2, 2, 1) + wx2(ip)*ww
            ww = wym(ip)*dz2(ip)
            vz(-1,-1, 2) = vz(-1,-1, 2) + wxm(ip)*ww
            vz( 0,-1, 2) = vz( 0,-1, 2) + wx0(ip)*ww
            vz( 1,-1, 2) = vz( 1,-1, 2) + wx1(ip)*ww
            vz( 2,-1, 2) = vz( 2,-1, 2) + wx2(ip)*ww
            ww = wy0(ip)*dz2(ip)
            vz(-1, 0, 2) = vz(-1, 0, 2) + wxm(ip)*ww
            vz( 0, 0, 2) = vz( 0, 0, 2) + wx0(ip)*ww
            vz( 1, 0, 2) = vz( 1, 0, 2) + wx1(ip)*ww
            vz( 2, 0, 2) = vz( 2, 0, 2) + wx2(ip)*ww
            ww = wy1(ip)*dz2(ip)
            vz(-1, 1, 2) = vz(-1, 1, 2) + wxm(ip)*ww
            vz( 0, 1, 2) = vz( 0, 1, 2) + wx0(ip)*ww
            vz( 1, 1, 2) = vz( 1, 1, 2) + wx1(ip)*ww
            vz( 2, 1, 2) = vz( 2, 1, 2) + wx2(ip)*ww
            ww = wy2(ip)*dz2(ip)
            vz(-1, 2, 2) = vz(-1, 2, 2) + wxm(ip)*ww
            vz( 0, 2, 2) = vz( 0, 2, 2) + wx0(ip)*ww
            vz( 1, 2, 2) = vz( 1, 2, 2) + wx1(ip)*ww
            vz( 2, 2, 2) = vz( 2, 2, 2) + wx2(ip)*ww
          enddo
          do kz=sdb,sub                                                 ! copy fluxes to fields
            jz=kz+iz
            if (jz <= 0 .or. jz > g%n(3)) cycle
            do ky=sdb,sub
              jy=ky+iy
              if (jy <= 0 .or. jy > g%n(2)) cycle
              do kx=sdb,sub
                jx=kx+ix
                if (jx <= 0 .or. jx > g%n(1)) cycle
                if (kx > -2 .and. ky > -2 .and. kz > -2) &
                  fields(jx,jy,jz,isp)%d = &
                  fields(jx,jy,jz,isp)%d + d (kx,ky,kz)
                if (ky > -2 .and. kz > -2) &
                  fields(jx,jy,jz,isp)%v(1) = &
                  fields(jx,jy,jz,isp)%v(1) + vx(kx,ky,kz)
                if (kx > -2 .and. kz > -2) &
                  fields(jx,jy,jz,isp)%v(2) = &
                  fields(jx,jy,jz,isp)%v(2) + vy(kx,ky,kz)
                if (kx > -2 .and. ky > -2) &
                  fields(jx,jy,jz,isp)%v(3) = &
                  fields(jx,jy,jz,isp)%v(3) + vz(kx,ky,kz)
              enddo
            enddo
          enddo
	  if (do_vth) then
	    do ip=1,np
	      !-----------------------------------------------------------------------
	      ! thermal velocity block 
	      ww = wym(ip)*wzm(ip)*v2(ip)
	      vth(-1,-1,-1) =  vth(-1,-1,-1) + wxm(ip)*ww
	      vth( 0,-1,-1) =  vth( 0,-1,-1) + wx0(ip)*ww
	      vth( 1,-1,-1) =  vth( 1,-1,-1) + wx1(ip)*ww
	      vth( 2,-1,-1) =  vth( 2,-1,-1) + wx2(ip)*ww
	      ww = wy0(ip)*wzm(ip)*v2(ip)
	      vth(-1, 0,-1) =  vth(-1, 0,-1) + wxm(ip)*ww
	      vth( 0, 0,-1) =  vth( 0, 0,-1) + wx0(ip)*ww
	      vth( 1, 0,-1) =  vth( 1, 0,-1) + wx1(ip)*ww
	      vth( 2, 0,-1) =  vth( 2, 0,-1) + wx2(ip)*ww
	      ww = wy1(ip)*wzm(ip)*v2(ip)
	      vth(-1, 1,-1) =  vth(-1, 1,-1) + wxm(ip)*ww
	      vth( 0, 1,-1) =  vth( 0, 1,-1) + wx0(ip)*ww
	      vth( 1, 1,-1) =  vth( 1, 1,-1) + wx1(ip)*ww
	      vth( 2, 1,-1) =  vth( 2, 1,-1) + wx2(ip)*ww
	      ww = wy2(ip)*wzm(ip)*v2(ip)
	      vth(-1, 2,-1) =  vth(-1, 2,-1) + wxm(ip)*ww
	      vth( 0, 2,-1) =  vth( 0, 2,-1) + wx0(ip)*ww
	      vth( 1, 2,-1) =  vth( 1, 2,-1) + wx1(ip)*ww
	      vth( 2, 2,-1) =  vth( 2, 2,-1) + wx2(ip)*ww
	      ww = wym(ip)*wz0(ip)*v2(ip)
	      vth(-1,-1, 0) =  vth(-1,-1, 0) + wxm(ip)*ww
	      vth( 0,-1, 0) =  vth( 0,-1, 0) + wx0(ip)*ww
	      vth( 1,-1, 0) =  vth( 1,-1, 0) + wx1(ip)*ww
	      vth( 2,-1, 0) =  vth( 2,-1, 0) + wx2(ip)*ww
	      ww = wy0(ip)*wz0(ip)*v2(ip)
	      vth(-1, 0, 0) =  vth(-1, 0, 0) + wxm(ip)*ww
	      vth( 0, 0, 0) =  vth( 0, 0, 0) + wx0(ip)*ww
	      vth( 1, 0, 0) =  vth( 1, 0, 0) + wx1(ip)*ww
	      vth( 2, 0, 0) =  vth( 2, 0, 0) + wx2(ip)*ww
	      ww = wy1(ip)*wz0(ip)*v2(ip)
	      vth(-1, 1, 0) =  vth(-1, 1, 0) + wxm(ip)*ww
	      vth( 0, 1, 0) =  vth( 0, 1, 0) + wx0(ip)*ww
	      vth( 1, 1, 0) =  vth( 1, 1, 0) + wx1(ip)*ww
	      vth( 2, 1, 0) =  vth( 2, 1, 0) + wx2(ip)*ww
	      ww = wy2(ip)*wz0(ip)*v2(ip)
	      vth(-1, 2, 0) =  vth(-1, 2, 0) + wxm(ip)*ww
	      vth( 0, 2, 0) =  vth( 0, 2, 0) + wx0(ip)*ww
	      vth( 1, 2, 0) =  vth( 1, 2, 0) + wx1(ip)*ww
	      vth( 2, 2, 0) =  vth( 2, 2, 0) + wx2(ip)*ww
	      ww = wym(ip)*wz1(ip)*v2(ip)
	      vth(-1,-1, 1) =  vth(-1,-1, 1) + wxm(ip)*ww
	      vth( 0,-1, 1) =  vth( 0,-1, 1) + wx0(ip)*ww
	      vth( 1,-1, 1) =  vth( 1,-1, 1) + wx1(ip)*ww
	      vth( 2,-1, 1) =  vth( 2,-1, 1) + wx2(ip)*ww
	      ww = wy0(ip)*wz1(ip)*v2(ip)
	      vth(-1, 0, 1) =  vth(-1, 0, 1) + wxm(ip)*ww
	      vth( 0, 0, 1) =  vth( 0, 0, 1) + wx0(ip)*ww
	      vth( 1, 0, 1) =  vth( 1, 0, 1) + wx1(ip)*ww
	      vth( 2, 0, 1) =  vth( 2, 0, 1) + wx2(ip)*ww
	      ww = wy1(ip)*wz1(ip)*v2(ip)
	      vth(-1, 1, 1) =  vth(-1, 1, 1) + wxm(ip)*ww
	      vth( 0, 1, 1) =  vth( 0, 1, 1) + wx0(ip)*ww
	      vth( 1, 1, 1) =  vth( 1, 1, 1) + wx1(ip)*ww
	      vth( 2, 1, 1) =  vth( 2, 1, 1) + wx2(ip)*ww
	      ww = wy2(ip)*wz1(ip)*v2(ip)
	      vth(-1, 2, 1) =  vth(-1, 2, 1) + wxm(ip)*ww
	      vth( 0, 2, 1) =  vth( 0, 2, 1) + wx0(ip)*ww
	      vth( 1, 2, 1) =  vth( 1, 2, 1) + wx1(ip)*ww
	      vth( 2, 2, 1) =  vth( 2, 2, 1) + wx2(ip)*ww
	      ww = wym(ip)*wz2(ip)*v2(ip)
	      vth(-1,-1, 2) =  vth(-1,-1, 2) + wxm(ip)*ww
	      vth( 0,-1, 2) =  vth( 0,-1, 2) + wx0(ip)*ww
	      vth( 1,-1, 2) =  vth( 1,-1, 2) + wx1(ip)*ww
	      vth( 2,-1, 2) =  vth( 2,-1, 2) + wx2(ip)*ww
	      ww = wy0(ip)*wz2(ip)*v2(ip)
	      vth(-1, 0, 2) =  vth(-1, 0, 2) + wxm(ip)*ww
	      vth( 0, 0, 2) =  vth( 0, 0, 2) + wx0(ip)*ww
	      vth( 1, 0, 2) =  vth( 1, 0, 2) + wx1(ip)*ww
	      vth( 2, 0, 2) =  vth( 2, 0, 2) + wx2(ip)*ww
	      ww = wy1(ip)*wz2(ip)*v2(ip)
	      vth(-1, 1, 2) =  vth(-1, 1, 2) + wxm(ip)*ww
	      vth( 0, 1, 2) =  vth( 0, 1, 2) + wx0(ip)*ww
	      vth( 1, 1, 2) =  vth( 1, 1, 2) + wx1(ip)*ww
	      vth( 2, 1, 2) =  vth( 2, 1, 2) + wx2(ip)*ww
	      ww = wy2(ip)*wz2(ip)*v2(ip)
	      vth(-1, 2, 2) =  vth(-1, 2, 2) + wxm(ip)*ww
	      vth( 0, 2, 2) =  vth( 0, 2, 2) + wx0(ip)*ww
	      vth( 1, 2, 2) =  vth( 1, 2, 2) + wx1(ip)*ww
	      vth( 2, 2, 2) =  vth( 2, 2, 2) + wx2(ip)*ww
	     enddo
	    do kz=slb,sub						                                          ! copy fluxes to fields
	      jz=kz+iz
	      if (jz <= 0 .or. jz > g%n(3)) cycle
	      do ky=slb,sub
		jy=ky+iy
		if (jy <= 0 .or. jy > g%n(2)) cycle
		do kx=slb,sub
		  jx=kx+ix
		  if (jx <= 0 .or. jx > g%n(1)) cycle
		  fields(jx,jy,jz,isp)%vth = fields(jx,jy,jz,isp)%vth + vth(kx,ky,kz)
		enddo
	      enddo
	    enddo
	  endif
        endif
      endif
      lastcellindex = fields(ix,iy,iz,isp)%i + 1                        ! update last cell idx
    enddo
    enddo
    enddo
  enddo
  
  if (do_vth) then
    do isp=1, nspecies; do iz=1,g%n(3); do iy=1,g%n(2); do ix=1,g%n(1)
      !vel  = fields(ix,iy,iz,isp)%v/fields(ix,iy,iz,isp)%d               
      ! FIXME: Should be recentered, but this is not a big problem
      vv = fields(ix,iy,iz,isp)%vth/max(fields(ix,iy,iz,isp)%d,1e-30) !-sqrt(sum(vel**2)) ! v^2 relative to average v
      fields(ix,iy,iz,isp)%vth = sqrt(max(0.,vv))                        
      ! guard against centering error influence
    end do; end do; end do; end do
  end if

!-----------------------------------------------------------------------
! FIXME: MUST CONTAIN BOTH VTHB_PERP, VTHB_PAR -- POSSIBLY ALSO BULK 
! VELOCITY (WHICH IS SUM OF ALL SPECIES VELOs) [MHD FLOWS W/O CURRENTS]
!-----------------------------------------------------------------------
  if (do_vth) then
   do isp=1, nspecies
    vthb(:,:,1,1,isp) = fields(:,:,g%lb(3),isp)%vth  ! lower boundary layer == 1, vth == 1
    vthb(:,:,2,1,isp) = fields(:,:,g%ub(3),isp)%vth  ! upper boundary layer == 2, vth == 1
   enddo
  endif

!  do isp=1, nspecies
!   call overlap(vthb(:,:,:,isp))
!  enddo

  if (allocated(wxm)) &
    deallocate(wxm,wx0,wx1,wx2,wym,wy0,wy1,wy2,wzm,wz0,wz1,wz2,&
    dxn,dxm,dx0,dx1,dx2,dyn,dym,dy0,dy1,dy2,dzn,dzm,dz0,dz1,dz2,v2)
 
  if (verbose > 0) write(*,*) "MESH_SOURCES: UB I, D, PZ, VZ:", itm, &
    sum(fields(:,:,g%ub(3),1)%d+fields(:,:,g%ub(3),3)%d), &
    sum(fields(:,:,g%ub(3),1)%v(3)+fields(:,:,g%ub(3),3)%v(2)), &
    sum(fields(:,:,g%ub(3),1)%v(3)+fields(:,:,g%ub(3),3)%v(2))/ &
    sum(fields(:,:,g%ub(3),1)%d+fields(:,:,g%ub(3),3)%d)
END SUBROUTINE mesh_sources
!-----------------------------------------------------------------------

SUBROUTINE mesh_sources_thermal
  USE params,        only : nspecies, mdim, mcoord, mid, stdall, rank, &
                            do_vth, do_local_patch, mpi
  USE species,       only : cellvector, particle, sp, fields
  USE grid_m,        only : g, nx, ny, nz
#if defined (_OPENMP)
  USE omp_lib, only : omp_get_thread_num, omp_get_num_threads
#endif
  implicit none
  type(particle), pointer, dimension(:) :: pa
  real, dimension(mcoord) :: p, vel
  real, dimension(mdim)   :: r
  real                    :: p2, og, vv, v, w, ww, wa, wb, wc, wd, s, &
                             sm1, sm2
  real, parameter         :: one_sixth = 1./6., two_third = 2./3., &
                             four_third = 4./3.
  integer                 :: ip, np, off, isp, jsp
  integer                 :: izy, ix, iy, iz, jx, jy, jz, kx, ky, kz    ! grid counters
  logical                 :: up, err
  integer, save           :: itm=0
  integer, parameter      :: verbose=0
  integer                 :: lastcellindex                              ! index to last cell
  integer, parameter      :: sdb=-2, slb=-1, sub=2
  real, dimension(sdb:sub,sdb:sub,sdb:sub) :: vth                       ! local field patch
  real, dimension(:), allocatable :: wxm,wx0,wx1,wx2, &
                                     wym,wy0,wy1,wy2, &
                                     wzm,wz0,wz1,wz2,v2
  real, external          :: wallclock
  logical                 :: first_call=.true.
  integer, save           :: omp_nthreads
  integer                 :: romp, iomp
  real, allocatable, dimension(:,:,:,:) :: lvth
#if defined (_OPENMP)
  common /omp/ romp
  if (first_call) then
    omp_nthreads = 0
    romp       = omp_get_thread_num() + 1
    omp_nthreads = omp_nthreads + 1
  endif
#else
  romp         = 1
  omp_nthreads = 1
#endif
  first_call = .false.

  if (.not. do_vth) return

  itm = itm + 1

  if (omp_nthreads > 1) allocate(lvth(g%n(1),g%n(2),g%n(3),2:omp_nthreads))

  if (romp==1) then
    do isp=1,nspecies                                                           ! loop over species
      do iz=1,g%n(3)                                                            ! zero fields
      do iy=1,g%n(2)
      do ix=1,g%n(1)
        fields(ix,iy,iz,isp)%vth = 0.                                           ! zap thermal vel
      enddo
      enddo
      enddo
    enddo
  else
    do iz=1,g%n(3)                                                              ! zero fields
    do iy=1,g%n(2)
    do ix=1,g%n(1)
      lvth(ix,iy,iz,romp) = 0.                                                  ! zap thermal vel
    enddo
    enddo
    enddo
  endif

  do jsp=1,nspecies                                                             ! loop over species
    if (sp(jsp)%np .eq. 0) cycle                                                ! cycle if no particles
    pa => sp(jsp)%particle                                                      ! shortcut to the part data
    isp = jsp
    np = sp(isp)%nmaxcell
    if (np > 0) allocate(wxm(np),wx0(np),wx1(np),wx2(np), &
                         wym(np),wy0(np),wy1(np),wy2(np), &
                         wzm(np),wz0(np),wz1(np),wz2(np),v2(np))
    do izy=1,g%n(2)*g%n(3)                                                      ! collapse loop manually. Xlf on Jugene doesnt support collapse directive
      iz = (izy-1) / g%n(2) + 1
      iy = izy - (iz-1)*g%n(2) 
      if (iy==1) then
        if (iz==1) then
          lastcellindex = 1                                                       
        else
          lastcellindex = fields(g%n(1),g%n(2),iz-1,isp)%i + 1
        endif
      else
        lastcellindex = fields(g%n(1),iy-1,iz,isp)%i + 1 
      endif
      do ix=1,g%n(1)
        if (fields(ix,iy,iz,isp)%i .ge. lastcellindex) then                     ! avoid if empty cell
 
        !-----------------------------------------------------------------------
        ! In this special version of mesh_sources, we calculate only the thermal
        ! stuff.
        !-----------------------------------------------------------------------
 
          np = fields(ix,iy,iz,isp)%i - lastcellindex + 1                       ! nr of parts in cell
          if (np > 0) then
            vth  = 0.                                                           ! zero fluxes for cell
            off = lastcellindex-1
            do ip=1,np                                                          ! loop over parts in cell
              ! ---------------------------------------------------------
              ! Find signed distance to center points in all directions.
              ! Project index into allowed range for interpolation.
              r = pa(ip+off)%r
              w = pa(ip+off)%w
              !-------------
              ! X_COORD CENTERED
              s = r(1); sm1 = 1. - s; sm2 = 2. - s 
              wxm(ip) = one_sixth * w * sm1*sm1*sm1
              wx0(ip) = w * (two_third - 0.5*s*s*sm2)
              wx1(ip) = w * (two_third - 0.5*sm1*sm1*(1.+ s)) 
              wx2(ip) = w - (wxm(ip) + wx0(ip) + wx1(ip))
              !-------------
              ! Y_COORD CENTERED
              s = r(2); sm1 = 1. - s; sm2 = 2. - s 
              wym(ip) = one_sixth * sm1*sm1*sm1
              wy0(ip) = two_third - 0.5*s*s*sm2
              wy1(ip) = two_third - 0.5*sm1*sm1*(1.+ s) 
              wy2(ip) = 1. - (wym(ip) + wy0(ip) + wy1(ip))
              !-------------
              ! Z_COORD CENTERED
              s = r(3); sm1 = 1. - s; sm2 = 2. - s 
              wzm(ip) = one_sixth * sm1*sm1*sm1
              wz0(ip) = two_third - 0.5*s*s*sm2
              wz1(ip) = two_third - 0.5*sm1*sm1*(1.+ s) 
              wz2(ip) = 1. - (wzm(ip) + wz0(ip) + wz1(ip))
              !-------------
              ! Get velocity
              p2= pa(ip+off)%p(1)*pa(ip+off)%p(1) + &
                  pa(ip+off)%p(2)*pa(ip+off)%p(2) + &
                  pa(ip+off)%p(3)*pa(ip+off)%p(3)                               ! momentum squared
              v = pa(ip+off)%p(1)                                               ! velocity
              v2(ip) = v*v
              v = pa(ip+off)%p(2)                                               ! velocity
              v2(ip) = v2(ip) + v*v
              v = pa(ip+off)%p(3)                                               ! velocity
              v2(ip)   = v2(ip) + v*v
            enddo
 
            do ip=1,np
              ww  = wym(ip)*wzm(ip)*v2(ip)
              vth(-1,-1,-1) =  vth(-1,-1,-1) + wxm(ip)*ww
              vth( 0,-1,-1) =  vth( 0,-1,-1) + wx0(ip)*ww
              vth( 1,-1,-1) =  vth( 1,-1,-1) + wx1(ip)*ww
              vth( 2,-1,-1) =  vth( 2,-1,-1) + wx2(ip)*ww
              ww  = wy0(ip)*wzm(ip)*v2(ip)
              vth(-1, 0,-1) =  vth(-1, 0,-1) + wxm(ip)*ww
              vth( 0, 0,-1) =  vth( 0, 0,-1) + wx0(ip)*ww
              vth( 1, 0,-1) =  vth( 1, 0,-1) + wx1(ip)*ww
              vth( 2, 0,-1) =  vth( 2, 0,-1) + wx2(ip)*ww
              ww  = wy1(ip)*wzm(ip)*v2(ip)
              vth(-1, 1,-1) =  vth(-1, 1,-1) + wxm(ip)*ww
              vth( 0, 1,-1) =  vth( 0, 1,-1) + wx0(ip)*ww
              vth( 1, 1,-1) =  vth( 1, 1,-1) + wx1(ip)*ww
              vth( 2, 1,-1) =  vth( 2, 1,-1) + wx2(ip)*ww
              ww  = wy2(ip)*wzm(ip)*v2(ip)
              vth(-1, 2,-1) =  vth(-1, 2,-1) + wxm(ip)*ww
              vth( 0, 2,-1) =  vth( 0, 2,-1) + wx0(ip)*ww
              vth( 1, 2,-1) =  vth( 1, 2,-1) + wx1(ip)*ww
              vth( 2, 2,-1) =  vth( 2, 2,-1) + wx2(ip)*ww
              ww  = wym(ip)*wz0(ip)*v2(ip)
              vth(-1,-1, 0) =  vth(-1,-1, 0) + wxm(ip)*ww
              vth( 0,-1, 0) =  vth( 0,-1, 0) + wx0(ip)*ww
              vth( 1,-1, 0) =  vth( 1,-1, 0) + wx1(ip)*ww
              vth( 2,-1, 0) =  vth( 2,-1, 0) + wx2(ip)*ww
              ww  = wy0(ip)*wz0(ip)*v2(ip)
              vth(-1, 0, 0) =  vth(-1, 0, 0) + wxm(ip)*ww
              vth( 0, 0, 0) =  vth( 0, 0, 0) + wx0(ip)*ww
              vth( 1, 0, 0) =  vth( 1, 0, 0) + wx1(ip)*ww
              vth( 2, 0, 0) =  vth( 2, 0, 0) + wx2(ip)*ww
              ww  = wy1(ip)*wz0(ip)*v2(ip)
              vth(-1, 1, 0) =  vth(-1, 1, 0) + wxm(ip)*ww
              vth( 0, 1, 0) =  vth( 0, 1, 0) + wx0(ip)*ww
              vth( 1, 1, 0) =  vth( 1, 1, 0) + wx1(ip)*ww
              vth( 2, 1, 0) =  vth( 2, 1, 0) + wx2(ip)*ww
              ww  = wy2(ip)*wz0(ip)*v2(ip)
              vth(-1, 2, 0) =  vth(-1, 2, 0) + wxm(ip)*ww
              vth( 0, 2, 0) =  vth( 0, 2, 0) + wx0(ip)*ww
              vth( 1, 2, 0) =  vth( 1, 2, 0) + wx1(ip)*ww
              vth( 2, 2, 0) =  vth( 2, 2, 0) + wx2(ip)*ww
              ww  = wym(ip)*wz1(ip)*v2(ip)
              vth(-1,-1, 1) =  vth(-1,-1, 1) + wxm(ip)*ww
              vth( 0,-1, 1) =  vth( 0,-1, 1) + wx0(ip)*ww
              vth( 1,-1, 1) =  vth( 1,-1, 1) + wx1(ip)*ww
              vth( 2,-1, 1) =  vth( 2,-1, 1) + wx2(ip)*ww
              ww  = wy0(ip)*wz1(ip)*v2(ip)
              vth(-1, 0, 1) =  vth(-1, 0, 1) + wxm(ip)*ww
              vth( 0, 0, 1) =  vth( 0, 0, 1) + wx0(ip)*ww
              vth( 1, 0, 1) =  vth( 1, 0, 1) + wx1(ip)*ww
              vth( 2, 0, 1) =  vth( 2, 0, 1) + wx2(ip)*ww
              ww  = wy1(ip)*wz1(ip)*v2(ip)
              vth(-1, 1, 1) =  vth(-1, 1, 1) + wxm(ip)*ww
              vth( 0, 1, 1) =  vth( 0, 1, 1) + wx0(ip)*ww
              vth( 1, 1, 1) =  vth( 1, 1, 1) + wx1(ip)*ww
              vth( 2, 1, 1) =  vth( 2, 1, 1) + wx2(ip)*ww
              ww  = wy2(ip)*wz1(ip)*v2(ip)
              vth(-1, 2, 1) =  vth(-1, 2, 1) + wxm(ip)*ww
              vth( 0, 2, 1) =  vth( 0, 2, 1) + wx0(ip)*ww
              vth( 1, 2, 1) =  vth( 1, 2, 1) + wx1(ip)*ww
              vth( 2, 2, 1) =  vth( 2, 2, 1) + wx2(ip)*ww
              ww  = wym(ip)*wz2(ip)*v2(ip)
              vth(-1,-1, 2) =  vth(-1,-1, 2) + wxm(ip)*ww
              vth( 0,-1, 2) =  vth( 0,-1, 2) + wx0(ip)*ww
              vth( 1,-1, 2) =  vth( 1,-1, 2) + wx1(ip)*ww
              vth( 2,-1, 2) =  vth( 2,-1, 2) + wx2(ip)*ww
              ww  = wy0(ip)*wz2(ip)*v2(ip)
              vth(-1, 0, 2) =  vth(-1, 0, 2) + wxm(ip)*ww
              vth( 0, 0, 2) =  vth( 0, 0, 2) + wx0(ip)*ww
              vth( 1, 0, 2) =  vth( 1, 0, 2) + wx1(ip)*ww
              vth( 2, 0, 2) =  vth( 2, 0, 2) + wx2(ip)*ww
              ww  = wy1(ip)*wz2(ip)*v2(ip)
              vth(-1, 1, 2) =  vth(-1, 1, 2) + wxm(ip)*ww
              vth( 0, 1, 2) =  vth( 0, 1, 2) + wx0(ip)*ww
              vth( 1, 1, 2) =  vth( 1, 1, 2) + wx1(ip)*ww
              vth( 2, 1, 2) =  vth( 2, 1, 2) + wx2(ip)*ww
              ww  = wy2(ip)*wz2(ip)*v2(ip)
              vth(-1, 2, 2) =  vth(-1, 2, 2) + wxm(ip)*ww
              vth( 0, 2, 2) =  vth( 0, 2, 2) + wx0(ip)*ww
              vth( 1, 2, 2) =  vth( 1, 2, 2) + wx1(ip)*ww
              vth( 2, 2, 2) =  vth( 2, 2, 2) + wx2(ip)*ww
            enddo
            if (romp==1) then
              do kz=slb,sub                                             ! copy fluxes to fields
                jz=kz+iz
                if (jz <= 0 .or. jz > g%n(3)) cycle
                do ky=slb,sub
                  jy=ky+iy
                  if (jy <= 0 .or. jy > g%n(2)) cycle
                  do kx=slb,sub
                    jx=kx+ix
                    if (jx <= 0 .or. jx > g%n(1)) cycle
                    fields(jx,jy,jz,isp)%vth = fields(jx,jy,jz,isp)%vth +  vth(kx,ky,kz)
                  enddo
                enddo
              enddo
            else
              do kz=slb,sub                                             ! copy fluxes for cell to fields
                jz=kz+iz
                if (jz <= 0 .or. jz > g%n(3)) cycle
                do ky=slb,sub
                  jy=ky+iy
                  if (jy <= 0 .or. jy > g%n(2)) cycle
                  do kx=slb,sub
                    jx=kx+ix
                    if (jx <= 0 .or. jx > g%n(1)) cycle
                    lvth(jx,jy,jz,romp) = lvth(jx,jy,jz,romp) +  vth(kx,ky,kz)
                  enddo
                enddo
              enddo
            endif
          endif
        endif
        lastcellindex = fields(ix,iy,iz,isp)%i + 1                      ! update last cell idx
      enddo
    enddo

    if (omp_nthreads > 1) then
      do izy=1,g%n(2)*g%n(3)                                            ! collapse loop over iz, and iy
        iz  = (izy-1)/g%n(2) + 1
        iy  = izy - (iz-1)*g%n(2)
        do iomp=2,omp_nthreads
        do ix=1,g%n(1)
          fields(ix,iy,iz,isp)%vth = &
          fields(ix,iy,iz,isp)%vth + lvth(ix,iy,iz,iomp)
          lvth(ix,iy,iz,iomp) = 0
        enddo
        enddo
      enddo
    endif
    if (allocated(wxm)) deallocate(wxm,wx0,wx1,wx2, &
                                   wym,wy0,wy1,wy2, &
                                   wzm,wz0,wz1,wz2,v2)
    !enddo
  enddo

  if (do_vth) then
    do isp=1, nspecies; do iz=1,g%n(3); do iy=1,g%n(2); do ix=1,g%n(1)
      vv = fields(ix,iy,iz,isp)%vth/max(fields(ix,iy,iz,isp)%d,1e-30)   !-sqrt(sum(vel**2)) ! v^2 relative to average v
      fields(ix,iy,iz,isp)%vth = sqrt(max(0.,vv))                       ! centering error guard
    end do; end do; end do; end do
  end if


END SUBROUTINE mesh_sources_thermal
!-----------------------------------------------------------------------
