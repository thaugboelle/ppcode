! Patch/hybrid/stagger_6th.f90 $Id$
! vim: nowrap
!----------------------------------------------------------------------
MODULE pic_stagger
  implicit none

  real    :: aa, bb, cc                                                 ! stagger operator prefactors
  real    :: a1
  real    :: ax1, ay1, az1                                              ! do.
  real    :: ax3, ay3, az3                                              ! do.
  real    :: bx3, by3, bz3                                              ! do.
  real    :: cx3, cy3, cz3                                              ! do.
  real    :: lp0, lpm1, lpm2, lpm3, lpp1, lpp2, lpp3                    ! LaPlace prefactors

  integer                  :: order=5                                  ! first order for now - default

CONTAINS
!----------------------------------------------------------------------
! Translate an arbitrary amount in y-direction. mpi%n(2)=1 is required.
SUBROUTINE ytranslate(f,trans,nx)
  USE params, only : mpi
  USE grid_m, only : g
  implicit none
  integer,      intent(in)          :: nx
  real(kind=8), intent(in)          :: trans
  real, dimension(nx,g%n(2),g%n(3)) :: f, h
  !
  real         :: w00, wp1
  real(kind=8) :: ntrans, ftrans
  integer      :: ix, iy, iz, jy, itrans
  !
  if (mpi%n(1) .ne. 1) call error('ytranslate',  &
    'Translation only works with one MPI domain in the y-direction.')
  !
  ntrans = trans * g%ods(2)                                             ! translation distance in units of y-cells
  itrans = floor(ntrans)                                                ! integer distance to translate
  ftrans = ntrans - itrans                                              ! fractional distance to translate
  ! Integer translation
  !---------------------------------------------------------------------
  do iz=1,g%n(3)
  do iy=g%lb(2), g%ub(2)-1
    jy = modulo((iy + itrans), g%gn(2)) + g%lb(2)
    h(:,jy,iz) = f(:,iy,iz)
  enddo
  enddo
  ! Update ghost zones of g in y-direction
  !---------------------------------------------------------------------
  do iz=1,g%n(3)
    do iy=1,g%lb(2)-1
      h(:,iy,iz) = h(:,iy + g%gn(2),iz)
    enddo
    do iy=g%ub(2),g%n(2)
      h(:,iy,iz) = h(:,iy - g%gn(2),iz)
    enddo
  enddo
  ! Fractional translation using 5th order polynomial interpolation
  !---------------------------------------------------------------------
  ! 1) Calculate weights
  !-------------------------------------
  w00 = -1. * (ftrans - 1.)
  wp1 = +1. * ftrans
  ! 2) Do fifth order fractional interpolation
  !-------------------------------------------
  do iz=1,g%n(3)
  do iy=g%lb(2), g%ub(2)-1
    f(:,iy,iz) = w00*h(:,iy,iz) + wp1*h(:,iy+1,iz)
  enddo
  enddo
  ! Update ghost zones of f in y-direction
  !---------------------------------------------------------------------
  do iz=1,g%n(3)
    do iy=1,g%lb(2)-1
      f(:,iy,iz) = f(:,iy + g%gn(2),iz)
    enddo
    do iy=g%ub(2),g%n(2)
      f(:,iy,iz) = f(:,iy - g%gn(2),iz)
    enddo
  enddo
END SUBROUTINE ytranslate
!-----------------------------------------------------------------------
FUNCTION xups (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: xups
  integer                                            :: i, j, k

     xups = (  aa*(f(i+1,j,k)+f(i  ,j,k)) )
END FUNCTION xups
!----------------------------------------------------------------------
FUNCTION yups (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: yups
  integer                                            :: i, j, k

     yups = (  aa*(f(i,j+1,k)+f(i,j  ,k)) )
END FUNCTION yups
!----------------------------------------------------------------------
FUNCTION zups (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: zups
  integer                                            :: i, j, k

     zups = (  aa*(f(i,j,k+1)+f(i,j,k  )) )
END FUNCTION zups
!----------------------------------------------------------------------
FUNCTION xdns (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: xdns
  integer                                            :: i, j, k

     xdns = (  aa*(f(i-1,j,k)+f(i  ,j,k)) )
END FUNCTION xdns
!----------------------------------------------------------------------
FUNCTION ydns (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ydns
  integer                                            :: i, j, k

     ydns = (  aa*(f(i,j-1,k)+f(i,j  ,k)) )
END FUNCTION ydns
!----------------------------------------------------------------------
FUNCTION zdns (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: zdns
  integer                                            :: i, j, k

     zdns = (  aa*(f(i,j,k-1)+f(i,j,k  )) )
END FUNCTION zdns
!----------------------------------------------------------------------
FUNCTION ddxups (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddxups
  integer                                            :: i, j, k

     ddxups = (  ax3*(f(i+1 ,j,k)-f(i   ,j,k)) )
END FUNCTION ddxups
!-----------------------------------------------------------------------
FUNCTION ddxdns(f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddxdns
  integer                                            :: mx, my, mz
  integer                                            :: i, j, k

      ddxdns = (  ax3*(f(i   ,j,k)-f(i-1 ,j,k)) )
END FUNCTION ddxdns
!-----------------------------------------------------------------------
FUNCTION ddyups (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddyups
  integer                                            :: i, j, k

    ddyups = (  ay3*(f(i,j+1 ,k)-f(i,j   ,k)) )
END FUNCTION ddyups
!-----------------------------------------------------------------------
FUNCTION ddydns (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddydns
  integer                                            :: i, j, k

      ddydns = (  ay3*(f(i,j   ,k)-f(i,j-1 ,k)) )
END FUNCTION ddydns
!-----------------------------------------------------------------------
FUNCTION ddzups (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddzups
  integer                                            :: i, j, k

    ddzups  = (  az3*(f(i,j,k+1 )-f(i,j,k   )) )
END FUNCTION ddzups
!-----------------------------------------------------------------------
FUNCTION ddzdns (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddzdns
  integer                                            :: i, j, k

    ddzdns  = (  az3*(f(i,j,k   )-f(i,j,k-1 )) )
END FUNCTION ddzdns
!-----------------------------------------------------------------------
FUNCTION xdn(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  if (g%gn(1)==1) then; h = f; return; endif
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(1) = max(lb(1),4)
  ub(1) = min(ub(1),g%n(1)-2)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = xdns(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION xdn
!-----------------------------------------------------------------------
FUNCTION ddxdn(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  if (g%gn(1)==1) then; h = 0.; return; endif
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(1) = max(lb(1),2)
  ub(1) = min(ub(1),g%n(1))
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddxdns(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddxdn
!-----------------------------------------------------------------------
FUNCTION xup(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  if (g%gn(1)==1) then; h = f; return; endif
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(1) = max(lb(1),2)
  ub(1) = min(ub(1),g%n(1)-1)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = xups(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION xup
!-----------------------------------------------------------------------
FUNCTION ddxup(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  if (g%gn(1)==1) then; h = 0.; return; endif
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(1) = max(lb(1),2)
  ub(1) = min(ub(1),g%n(1)-1)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddxups(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddxup
!-----------------------------------------------------------------------
FUNCTION ydn(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  if (g%gn(2)==1) then; h = f; return; endif
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(2) = max(lb(2),2)
  ub(2) = min(ub(2),g%n(2))
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ydns(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ydn
!-----------------------------------------------------------------------
FUNCTION ddydn(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  if (g%gn(2)==1) then; h = 0.; return; endif
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(2) = max(lb(2),2)
  ub(2) = min(ub(2),g%n(2))
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddydns(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddydn
!-----------------------------------------------------------------------
FUNCTION yup(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  if (g%gn(2)==1) then; h = f; return; endif
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(2) = max(lb(2),1)
  ub(2) = min(ub(2),g%n(2)-1)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = yups(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION yup
!-----------------------------------------------------------------------
FUNCTION ddyup(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  if (g%gn(2)==1) then; h = 0.; return; endif
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(2) = max(lb(2),1)
  ub(2) = min(ub(2),g%n(2)-1)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddyups(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddyup
!-----------------------------------------------------------------------
FUNCTION zdn(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  if (g%gn(3)==1) then; h = f; return; endif
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(3) = max(lb(3),2)
  ub(3) = min(ub(3),g%n(3))
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = zdns(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION zdn
!-----------------------------------------------------------------------
FUNCTION ddzdn(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  if (g%gn(3)==1) then; h = 0.; return; endif
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(3) = max(lb(3),2)
  ub(3) = min(ub(3),g%n(3))
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddzdns(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddzdn
!-----------------------------------------------------------------------
FUNCTION zup(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  if (g%gn(3)==1) then; h = f; return; endif
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(3) = max(lb(3),1)
  ub(3) = min(ub(3),g%n(3)-1)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = zups(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION zup
!-----------------------------------------------------------------------
FUNCTION ddzup(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  if (g%gn(3)==1) then; h = 0.; return; endif
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(3) = max(lb(3),1)
  ub(3) = min(ub(3),g%n(3)-1)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddzups(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddzup
!-----------------------------------------------------------------------
FUNCTION d2dx2cen(f,i,j,k)
  USE grid_m,           only : g, odxq
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                     :: d2dx2cen
  integer                                  :: i,j,k

    d2dx2cen = (  lpm1 * f(i-1,j,k) &
                +  lp0 * f(i  ,j,k) &
                + lpp1 * f(i+1,j,k) ) * odxq
END FUNCTION d2dx2cen
!-----------------------------------------------------------------------
FUNCTION d2dy2cen(f,i,j,k)
  USE grid_m,           only : g, odyq
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                     :: d2dy2cen
  integer                                  :: i,j,k

    d2dy2cen = (  lpm1 * f(i,j-1,k) &
                +  lp0 * f(i,j  ,k) &
                + lpp1 * f(i,j+1,k) ) * odyq
END FUNCTION d2dy2cen
!------------------------
FUNCTION d2dz2cen(f,i,j,k)
  USE grid_m,           only : g, odzq
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                     :: d2dz2cen
  integer                                  :: i,j,k

    d2dz2cen = (  lpm1 * f(i,j,k-1) &
                +  lp0 * f(i,j,k  ) &
                + lpp1 * f(i,j,k+1) ) * odzq
END FUNCTION d2dz2cen
!------------------------
FUNCTION d2dx2cena(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,ub(3)
  ub = merge(g%ub,g%ub-1,mpi%ub)
  do k=g%lb(3),ub(3); do j=g%lb(2),ub(2); do i=g%lb(1),ub(1)
    h(i,j,k) = d2dx2cen(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION d2dx2cena
!------------------------
FUNCTION d2dy2cena(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,ub(3)
  ub = merge(g%ub,g%ub-1,mpi%ub)
  do k=g%lb(3),ub(3); do j=g%lb(2),ub(2); do i=g%lb(1),ub(1)
    h(i,j,k) = d2dy2cen(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION d2dy2cena
!------------------------
FUNCTION d2dz2cena(f) result(h)
  USE grid_m, only : g
  USE params, only : mpi
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,ub(3)
  ub = merge(g%ub,g%ub-1,mpi%ub)
  do k=g%lb(3),ub(3); do j=g%lb(2),ub(2); do i=g%lb(1),ub(1)
    h(i,j,k) = d2dz2cen(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION d2dz2cena
!------------------------
FUNCTION lpx(f,i,j,k)
  USE grid_m,           only : g
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3))              :: f
  integer                                               :: i, j, k  
  real                                                  :: lpx
    lpx = (  lpm1 * f(i-1,j,k) &
           + lpp1 * f(i+1,j,k) )
END FUNCTION lpx
!------------------------
FUNCTION lpy(f,i,j,k)
  USE grid_m,           only : g
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3))              :: f
  integer                                               :: i, j, k  
  real                                                  :: lpy
      lpy = (  lpm1 * f(i,j-1,k) &
             + lpp1 * f(i,j+1,k) )
END FUNCTION lpy
!------------------------
FUNCTION lpz(f,i,j,k)
  USE grid_m,           only : g
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3))              :: f
  integer                                               :: i, j, k  
  real                                                  :: lpz
      lpz = (  lpm1 * f(i,j,k-1) &
             + lpp1 * f(i,j,k+1) )
END FUNCTION lpz
!------------------------




END MODULE pic_stagger
!-------------------------------------------------------------------------------
  subroutine test_stagger
!
!  Check the correctness of the stagger operators
!
  USE params, only: mpi, periodic, rank, master, mid
  USE grid_m, only : g, bx, by, bz, ex, ey, ez
  use pic_stagger, only: ddxdn, ddydn, ddzdn, &
                         ddxup, ddyup, ddzup, &
                         d2dx2cena, d2dy2cena, d2dz2cena
  use units, only : c
  implicit none
  integer i,j,k, jx,jy,jz, mx, my, mz
  real epsx, epsy, epsz, eps1, eps2, eps3, fx, fy, fz, dx, dy, dz, pi
  real, dimension(:,:,:), allocatable :: scr
  logical ok, all_mpi

  character(len=mid):: id='Patch/hybrid/stagger_6th.f90 $Id$'

  call print_id(id)

  mx=g%gn(1); my=g%gn(2); mz=g%gn(3); dx=g%ds(1); dy=g%ds(2); dz=g%ds(3)
  allocate(scr(g%n(1),g%n(2),g%n(3)))

  pi = c%pi
  fx = 2.*pi/(mx*dx)
  fy = 2.*pi/(my*dy)
  fz = 2.*pi/(mz*dz)
  ok = .true.

  do k=1,g%n(3)
  do j=1,g%n(2)
  do i=1,g%n(1)
    jx = i-g%lb(1)+mpi%offset(1); jy = j-g%lb(2)+mpi%offset(2); jz = k-g%lb(3)+mpi%offset(3)
    bx(i,j,k)=sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)=sin(2.*pi*real(jy)/real(my))
    bz(i,j,k)=sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=cos(2.*pi*real(jx-0.5)/real(mx))*fx
    ey(i,j,k)=cos(2.*pi*real(jy-0.5)/real(my))*fy
    ez(i,j,k)=cos(2.*pi*real(jz-0.5)/real(mz))*fz
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(bz)
  call overlap(ex); call overlap(ey); call overlap(ez)
  scr=ddxdn(bx); bx=scr
  bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ex(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsx=maxval(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(1).eq.1) epsx=0.
  scr = ddydn(by); by=scr
  by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ey(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsy=maxval(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(2).eq.1) epsy=0.
  scr = ddzdn(bz); bz=scr
  bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ez(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsz=maxval(bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(3).eq.1) epsz=0.
  call overlap(bx); call overlap(by); call overlap(bz)
  eps1 = max(0.5*fx**3*dx**2/(3.*2.*1.),1.e-6/dx)
  eps2 = max(0.5*fy**3*dy**2/(3.*2.*1.),1.e-6/dy)
  eps3 = max(0.5*fz**3*dz**2/(3.*2.*1.),1.e-6/dz)
  ok = ok .and. (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  if (.not. ok) print'(a,i5,1p,6g12.4,l4)',' ddn     :', rank, epsx, eps1, epsy, eps2, epsz, eps3, ok

  do k=1,g%n(3)
  do j=1,g%n(2)
  do i=1,g%n(1)
    jx = i-g%lb(1)+mpi%offset(1); jy = j-g%lb(2)+mpi%offset(2); jz = k-g%lb(3)+mpi%offset(3)
    bx(i,j,k)=sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)=sin(2.*pi*real(jy)/real(my))
    bz(i,j,k)=sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=cos(2.*pi*real(jx+0.5)/real(mx))*fx
    ey(i,j,k)=cos(2.*pi*real(jy+0.5)/real(my))*fy
    ez(i,j,k)=cos(2.*pi*real(jz+0.5)/real(mz))*fz
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(bz)
  call overlap(ex); call overlap(ey); call overlap(ez)
  scr = ddxup(bx); bx=scr
  bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ex(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsx=maxval(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(1).eq.1) epsx=0.
  scr = ddyup(by); by=scr
  by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ey(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsy=maxval(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(2).eq.1) epsy=0.
  scr = ddzup(bz); bz=scr
  bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ez(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsz=maxval(bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(3).eq.1) epsz=0.
  eps1 = max(0.5*fx**3*dx**2/(3.*2.*1.),1.e-6/dx)
  eps2 = max(0.5*fy**3*dy**2/(3.*2.*1.),1.e-6/dy)
  eps3 = max(0.5*fz**3*dz**2/(3.*2.*1.),1.e-6/dz)
  ok = ok .and. (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  if (.not. ok) print'(a,i5,1p,6g12.4,l4)',' dup     :', rank, epsx, eps1, epsy, eps2, epsz, eps3, ok

  do k=1,g%n(3)
  do j=1,g%n(2)
  do i=1,g%n(1)
    jx = i-g%lb(1)+mpi%offset(1); jy = j-g%lb(2)+mpi%offset(2); jz = k-g%lb(3)+mpi%offset(3)
    bx(i,j,k)=sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)=sin(2.*pi*real(jy)/real(my))
    bz(i,j,k)=sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=-sin(2.*pi*real(jx)/real(mx))*fx**2
    ey(i,j,k)=-sin(2.*pi*real(jy)/real(my))*fy**2
    ez(i,j,k)=-sin(2.*pi*real(jz)/real(mz))*fz**2
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(bz)
  call overlap(ex); call overlap(ey); call overlap(ez)
  scr = d2dx2cena(bx); bx=scr
  bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ex(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsx=maxval(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(1).eq.1) epsx=0.
  scr = d2dy2cena(by); by=scr
  by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ey(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsy=maxval(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(2).eq.1) epsy=0.
  scr = d2dz2cena(bz); bz=scr
  bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ez(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsz=maxval(bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(3).eq.1) epsz=0.
  eps1 = 2.*max(fx**4*dx**2/(3.*2.*1.),2.e-6*fx/dx)
  eps2 = 2.*max(fy**4*dy**2/(3.*2.*1.),2.e-6*fy/dy)
  eps3 = 2.*max(fz**4*dz**2/(3.*2.*1.),2.e-6*fz/dz)
  ok = ok .and. (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  if (.not. ok .and. rank < 10) print'(a,i5,1p,6g12.4,l4)',' d2d[xyz]:', rank, epsx, eps1, epsy, eps2, epsz, eps3, ok

  ok = all_mpi(ok)
  if (ok) then
    if (master) print *,'the stagger routines appear to be working correctly'
  else
    call warning('stagger_test','THERE APPEARS TO BE A PROBLEM WITH THE STAGGER ROUTINES')
  end if

  deallocate(scr)

  end subroutine
!----------------------------------------------------------------------

SUBROUTINE compute_stagger_prefactors                                  ! compute prefactors for the stagger operators, they depend on order in finite diff.
USE grid_m,           only : g
USE pic_stagger,    only : aa,                    &
                           ax3,  ay3,  az3,       &
                           lp0, lpm1, lpp1
  implicit none
  integer :: i
  real    :: dx, dy, dz

  dx = g%ds(1); dy = g%ds(2); dz = g%ds(3)
  aa =.5; ax3=1./dx; ay3=1./dy; az3=1./dz    !stagger

  lp0  = - 2.                                !LaPlace
  lpm1 = + 1.
  lpp1 = + 1.

  call test_stagger
  
END SUBROUTINE compute_stagger_prefactors
!----------------------------------------------------------------------


!==========================================================================================================================
!CONVENTIONS:
!
!
!
!
!GRID:	i-3		i-2		i-1		i		i+1		i+2		i+3		i+1
!
!							ddxdn[i,j,k](i-3,i-2,i-1,i,i+1,i+2) / (i-2,i-1,i,i+1) / (i-1,i)
!							|
!		----------------------------------------|----------------------------------------
!		|		|		|		|		|		|
!X	O	X	O	X	O	X	O	X	O	X	O	X	O	X	O
!			|		|		|		|		|		|
!			----------------------------------------|----------------------------------------
!								|
!								ddxup[i,j,k](i-2,i-1,i,i+1,i+2,i+3) / (i-1,i,i+1,i+2) / (i,i+1)
!
!==========================================================================================================================


!From the coefficients: 
!    bz3=-1./24.
!    az3=(1. - 3.*bz3)/dz
!    bz3=bz3/dz
!
! we get for example that 
!    ddzup  = (  az3*(f(i,j,k+1 )-f(i,j,k   )) &
!              + bz3*(f(i,j,k+2 )-f(i,j,k-1 ))  )
!
! can be rewritten as
!    ddzup  = ( -f[+3/2] + 27*f[+1/2] - 27*f[-1/2]  + 1*f[-3/2] )/(24*dz).
!
! which is the correct representation on HALF-CENTERED grids. 
!
!We should, however, use the corresponding CENTERED LaPlace 
! operator to keep things in line, since we're finding d^2/d[Q]^2
! at it's 'own' point.
