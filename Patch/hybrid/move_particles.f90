! $ Patch/hybrid/move_particles_simple.f90  $
! vim: nowrap
!-----------------------------------------------------------------------
MODULE move_particles_m
  USE params,  only : mcoord
  USE species, only : particle, field_type_deposit
  USE grid_m,  only : g
  USE interpolation_cached
  implicit none
  real    :: ds(3), grlb(3), ods(3), hE, hB, dt4, dtp4

  PROCEDURE(boris_pusher_nonrel), pointer :: boris_pusher => NULL()     ! points to particle pusher

CONTAINS
! Non-relativistic Boris Pusher
!    - input  : pav -- particle data, do_update flag
!               p[EB][xyz] -- electromagnetic fields at particle position
!    - output : r, o[xyz], particle position; pav_[xyz], updated momentum
!=======================================================================
SUBROUTINE boris_pusher_nonrel(pa,mass,do_update,E,B,r,p,ox,oy,oz)
  USE params,   only : do_shear, omega_r, shear
  USE hybrid_m, only : vmax
  implicit none
  type(particle)   :: pa
  logical          :: do_update
  real, dimension(3) :: r, p, E, B
  integer          :: ox, oy, oz
  real             :: mass
  !
  real             :: vmx, vmy, vmz, vpx, vpy, vpz, &
                      ex, ey, ez, bx, by, bz, fac, x
  !
  ! Particle velocity update
  !=======================================================================
  ex = hE*E(1)                                                          ! rescale electric field
  ey = hE*E(2)
  ez = hE*E(3)

  bx = hB*B(1)                                                          ! rescale magnetic field
  by = hB*B(2)
  bz = hB*B(3)

  if (do_shear) then
    x = (pa%q(1) + pa%r(1))*ds(1) + grlb(1)                             ! global x-position

    bz = bz + omega_r*dtp4                                              ! ammend magnetic field; dt comes from hB rescaling
    ex = ex - shear*x*bz                                                ! ammend electric field
    ez = ez + shear*x*bx
  endif

  vmx = pa%p(1) + ex                                                    ! Boost: p = v + h_E E
  vmy = pa%p(2) + ey
  vmz = pa%p(3) + ez

  vpx = vmx + (vmy*bz - vmz*by)                                         ! Rotation: vp = vm + vm x b
  vpy = vmy + (vmz*bx - vmx*bz)
  vpz = vmz + (vmx*by - vmy*bx)

  fac = 2./(1. + bx*bx + by*by + bz*bz)                                 ! Rescale factor: s = 2 / (1 + h^2)

  p(1) = vmx + fac*(vpy*bz - vpz*by) + ex                               ! Second boost, rescale and rotation
  p(2) = vmy + fac*(vpz*bx - vpx*bz) + ey                               ! p = vm + h_E E + s *(vp x b)
  p(3) = vmz + fac*(vpx*by - vpy*bx) + ez

! Particle position update: r/ds = r/ds + u_new * dt, o[xyz] tracks cell crossing
!=======================================================================
  r(1) = pa%r(1) + p(1)*dt4*ods(1)
  r(2) = pa%r(2) + p(2)*dt4*ods(2)
  r(3) = pa%r(3) + p(3)*dt4*ods(3)
  ox = floor(r(1))
  oy = floor(r(2))
  oz = floor(r(3))
  r(1) = r(1) - ox
  r(2) = r(2) - oy
  r(3) = r(3) - oz
! Store new position and velocity
!=======================================================================
  if (do_update) then
    pa%q(1) = pa%q(1) + ox
    pa%q(2) = pa%q(2) + oy
    pa%q(3) = pa%q(3) + oz
    pa%r(1) = r(1)
    pa%r(2) = r(2)
    pa%r(3) = r(3)
    pa%p(1) = p(1)
    pa%p(2) = p(2)
    pa%p(3) = p(3)
    pa%e = 0.5*mass*(p(1)*p(1) + p(2)*p(2) + p(3)*p(3))
    vmax(1) = max(vmax(1), p(1), p(2), p(3))                            ! max linear particle speed
  endif
END SUBROUTINE boris_pusher_nonrel
!=======================================================================
END MODULE move_particles_m
!-----------------------------------------------------------------------
SUBROUTINE particle_position_update
  USE params,   only : nspecies, dt
  USE species,  only : sp
  USE units,    only : elm
  USE move_particles_m
  implicit none
  type(particle), pointer, dimension(:)   :: pa
  integer :: isp, ip, ox, oy, oz
!-----------------------------------------------------------------------
  call trace_enter('move_particles')
  call check_particle_ranges ('before move')
                                         call timer('particles','start')
  dt4 = dt; ods=g%ods
  do isp=1,nspecies
    pa => sp(isp)%particle
    do ip=1,sp(isp)%np
      pa(ip)%r(1) = pa(ip)%r(1) + pa(ip)%p(1)*dt4*ods(1)
      pa(ip)%r(2) = pa(ip)%r(2) + pa(ip)%p(2)*dt4*ods(2)
      pa(ip)%r(3) = pa(ip)%r(3) + pa(ip)%p(3)*dt4*ods(3)
      ox = floor(pa(ip)%r(1))
      oy = floor(pa(ip)%r(2))
      oz = floor(pa(ip)%r(3))
      pa(ip)%r(1) = pa(ip)%r(1) - ox
      pa(ip)%r(2) = pa(ip)%r(2) - oy
      pa(ip)%r(3) = pa(ip)%r(3) - oz
      pa%q(1) = pa%q(1) + ox
      pa%q(2) = pa%q(2) + oy
      pa%q(3) = pa%q(3) + oz
    enddo
  enddo

  ! Impose boundaries, exchange particles, and sort them
  !-------------------------------------------------------------------
                                       call timer('particle boundaries','start')
  call impose_particle_boundaries
                                       call timer('send particles','start')
  call send_particles
                                       call timer('sort','start')
  call sort

  call trace_exit('particle_position_update')
END SUBROUTINE particle_position_update
!-----------------------------------------------------------------------
SUBROUTINE particle_velocity_update(Bx,By,Bz,Ex,Ey,Ez)
!
! This version of the particle mover is optimized for speed, but all operations
! been logically split in to different subroutines to aid in readability.
!
  USE params,   only : nspecies, dtp
  USE species,  only : sp, fields
  USE units,    only : elm
  USE move_particles_m
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in) :: Bx, BY, Bz, Ex, Ey, Ez
  !
  integer,        dimension(nspecies)     :: firstcellindex                     ! index to first part in cell
  type(particle), pointer, dimension(:)   :: pa
  real, dimension(slc:suc,3)              :: w, ws                              ! Interpolation weights
  real    :: mass, s, r(3), E(3), B(3), p(3), rold(3)
  integer :: ix, iy, iz, isp, ip, np, ox, oy, oz
  integer(kind=2) :: qold(3)
  logical :: touched_row
!-----------------------------------------------------------------------
  call trace_enter('move_particles')
  call check_particle_ranges ('before move')
                                         call timer('particles','start')
  grlb = g%grlb; ds = g%ds; ods = g%ods; dt4 = 0.; dtp4 = real(dtp)
  w=0.; ws=0.
  do iz=1,g%n(3)
  do iy=1,g%n(2)
    if (iy==1) then
      if (iz==1) then
        firstcellindex = 1                                                    
      else
        firstcellindex = fields(g%n(1),g%n(2),iz-1,:)%i + 1
      endif
    else
      firstcellindex = fields(g%n(1),iy-1,iz,:)%i + 1
    endif
    touched_row = .false.                                             
    do ix=1,g%n(1)
      if (any(fields(ix,iy,iz,:)%i >= firstcellindex)) then             ! avoid if empty cell for all species
        call cache_em_fields(ix,iy,iz,touched_row,Ex,Ey,Ez,Bx,By,Bz)    ! Cache E-M fields
        ! Loop over species and then particles in the cell
        do isp=1,nspecies
          np  = fields(ix,iy,iz,isp)%i - firstcellindex(isp) + 1        ! #parts in cell
          if (np > 0) then
            mass = sp(isp)%mass
            hE = 0.5*dtp4*sp(isp)%charge/mass                           ! Boris pusher
            hB = hE*elm%kf                                              ! prefctors
            v=0.; d=0.                                                  ! zero fluxes for cell
            pa => sp(isp)%particle                                      ! for convenience

            do ip=firstcellindex(isp),fields(ix,iy,iz,isp)%i
              w=0.; ws=0.
              r = pa(ip)%r
              rold = r; qold = pa(ip)%q
              call make_centered_weights (r,w)                          ! Compute inteprolation weights
              call make_staggered_weights(r,ws)
              call interpolate_fields(w,ws,E,B)                         ! Interpolation of fields to particle position
              call boris_pusher(pa(ip),mass,.true.,E,B,r,p,ox,oy,oz)    ! Non-relativistic Boris Pusher and particle position update
              if (max(abs(ox),abs(oy),abs(oz)) > 1) then                ! Sanity check: Has particle moved more than a cell ?
                print '(a,i8,1p,15e9.1)',' W1 ip,r,r,v,dt/ds:',ip,pa(ip)%r,r,p,ods*dt4
                print '(a,i8,1p,3i5,6e9.1)', 'W1 ip, ix, iy, iz, E, B :',&
                  ip,ix,iy,iz,Ex(ix,iy,iz),Ey(ix,iy,iz),Ez(ix,iy,iz),Bx(ix,iy,iz),By(ix,iy,iz),Bz(ix,iy,iz)
                print *, 'Problems with Courant condition or crazy and rapidly changing fields.'
              end if
              pa(ip)%r = rold; pa(ip)%q = qold                          ! Use old position; only velocity is updated. 
            enddo 
          endif ! if particles in this species 
        enddo
      else
        touched_row = .false.                                           ! No particles, so cache_em_fields cannot roll values in next step
      endif     ! if any particles in cell
      firstcellindex = fields(ix,iy,iz,:)%i + 1
    enddo
  enddo
  enddo
  call trace_exit('particle_velocity_update')
END SUBROUTINE particle_velocity_update
!-----------------------------------------------------------------------
SUBROUTINE move_particles(nion,nUx,nUy,nUz,Bx,By,Bz,Ex,Ey,Ez,do_update)
!
! This version of the particle mover is optimized for speed, but all operations
! been logically split in to different subroutines to aid in readability.
!
  USE params,   only : nspecies, dt, dtp, particleupdates, &
                       nodes, do_vth, nomp, romp
  USE species,  only : sp, fields, lfields
  USE units,    only : elm
  USE hybrid_m, only : do_mass_conservation
  USE move_particles_m
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3),nspecies), target :: nion, nUx, nUy, nUz
  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in) :: Bx, BY, Bz, Ex, Ey, Ez
  logical, intent(in)                     :: do_update
  !
  integer,        dimension(nspecies)     :: firstcellindex                     ! index to first part in cell
  type(particle), pointer, dimension(:)   :: pa
  real    :: mass, s, r(3), E(3), B(3), p(3), dxdt, dydt, dzdt
  integer :: iomp, ix, iy, iz, isp, ip, np, ox, oy, oz
  logical :: touched_row, add
!-----------------------------------------------------------------------

  call trace_enter('move_particles')
  call check_particle_ranges ('before move')

                                         call timer('particles','start')
  if (nomp>1) allocate(lfields(g%n(1),g%n(2),g%n(3),nspecies,2:nomp))


  ! Thread private scratch vars
  ods = g%ods; dt4 = real(dt); dtp4 = real(dtp)
  dxdt = g%ds(1) / dt4; dydt = g%ds(2) / dt4; dzdt = g%ds(3) / dt4
  grlb = g%grlb; ds = g%ds

  ! Initialize global (flux-)densities to zero
  if (romp==1) then
    do isp=1,nspecies
      do iz=1,g%n(3)
      do iy=1,g%n(2)
      do ix=1,g%n(1)
        nion(ix,iy,iz,isp) = 0.
         nUx(ix,iy,iz,isp) = 0.
         nUy(ix,iy,iz,isp) = 0.
         nUz(ix,iy,iz,isp) = 0.
      enddo
      enddo
      enddo
    enddo
  else
    do isp=1,nspecies
      do iz=1,g%n(3)
      do iy=1,g%n(2)
      do ix=1,g%n(1)
        lfields(ix,iy,iz,isp,romp)%d = 0.
        lfields(ix,iy,iz,isp,romp)%v = 0.
      enddo
      enddo
      enddo
    enddo
  endif

  do iz=1,g%n(3)
  do iy=1,g%n(2)
    if (iy==1) then
      if (iz==1) then
        firstcellindex = 1                                                    
      else
        firstcellindex = fields(g%n(1),g%n(2),iz-1,:)%i + 1
      endif
    else
      firstcellindex = fields(g%n(1),iy-1,iz,:)%i + 1
    endif
    touched_row = .false.                                             
    do ix=1,g%n(1)
      if (any(fields(ix,iy,iz,:)%i >= firstcellindex)) then             ! avoid if empty cell for all species
        call cache_em_fields(ix,iy,iz,touched_row,Ex,Ey,Ez,Bx,By,Bz)    ! Cache E-M fields
        ! Loop over species and then particles in the cell
        do isp=1,nspecies
          np  = fields(ix,iy,iz,isp)%i - firstcellindex(isp) + 1        ! #parts in cell
          if (np > 0) then
            mass = sp(isp)%mass
            hE = 0.5*dtp4*sp(isp)%charge/mass                           ! Boris pusher
            hB = hE*elm%kf                                              ! prefctors
            v=0.; d=0.                                                  ! zero fluxes for cell
            pa => sp(isp)%particle                                      ! for convenience

            do ip=firstcellindex(isp),fields(ix,iy,iz,isp)%i
              r = pa(ip)%r
              w_old=0; ws_old=0.; w_new=0.; ws_new=0.
              call make_centered_weights (r,w_old)                      ! Compute inteprolation weights
              call make_staggered_weights(r,ws_old)
              call interpolate_fields(w_old,ws_old,E,B)                 ! Interpolation of fields to particle position
              call boris_pusher(pa(ip),mass,do_update,E,B,r,p,ox,oy,oz) ! Non-relativistic Boris Pusher and particle position update

              if (max(abs(ox),abs(oy),abs(oz)) > 1) then                ! Sanity check: Has particle moved more than a cell ?
                print '(a,i8,1p,15e9.1)',' W1 ip,r,r,v,dt/ds:',ip,pa(ip)%r,r,p,ods*dt4
                print '(a,i8,1p,3i5,6e9.1)', 'W1 ip, ix, iy, iz, E, B :',&
                  ip,ix,iy,iz,Ex(ix,iy,iz),Ey(ix,iy,iz),Ez(ix,iy,iz),Bx(ix,iy,iz),By(ix,iy,iz),Bz(ix,iy,iz)
                print *, 'Problems with Courant condition or crazy and rapidly changing fields.'
              end if

              if (do_mass_conservation) then
                call make_centered_weights_updated(r,ox,oy,oz,w_new)      ! Calculate new interpolation weights
                call mass_conserving_deposition(w_old,w_new,pa(ip)%w,p,ox,oy,oz,dxdt,dydt,dzdt) ! Number density and fluxes for species 
              else
                call make_centered_weights_updated(r,ox,oy,oz,w_new)      ! Calculate new interpolation weights
                call make_staggered_weights_updated(r,ox,oy,oz,ws_new)    ! Calculate new interpolation weights
                call energy_conserving_deposition(w_new,ws_new,w_old,ws_old,pa(ip)%w,p,ox,oy,oz)! Number densities and fluxes for species
              endif
            enddo 

            call deposit_cached_fields(ix, iy, iz, isp, nion, nUx, nUy, nUz) ! Copy d,v back to fields
            particleupdates = particleupdates + np
          endif ! if particles in this species 
        enddo
      else
        touched_row = .false.                                           ! No particles, so cache_em_fields cannot roll values in next step
      endif     ! if any particles in cell
      firstcellindex = fields(ix,iy,iz,:)%i + 1
    enddo
  enddo
  enddo

  ! Add up the contributions
  if (nomp > 1) then
    do isp=1,nspecies
    do iz=1,g%n(3)
    do iy=1,g%n(3)
      do iomp=2,nomp
      do ix=1,g%n(1)
        nion(ix,iy,iz,isp) = nion(ix,iy,iz,isp) + lfields(ix,iy,iz,isp,iomp)%d
         nUx(ix,iy,iz,isp) =  nUx(ix,iy,iz,isp) + lfields(ix,iy,iz,isp,iomp)%v(1)
         nUy(ix,iy,iz,isp) =  nUy(ix,iy,iz,isp) + lfields(ix,iy,iz,isp,iomp)%v(2)
         nUz(ix,iy,iz,isp) =  nUz(ix,iy,iz,isp) + lfields(ix,iy,iz,isp,iomp)%v(3)
      enddo
      enddo
    enddo
    enddo
    enddo
  endif
  if (allocated(lfields)) deallocate(lfields)
  
  if (do_mass_conservation) then
                                         call timer('penta solve','start')
    call compute_flux(nUx,nUy,nUz)                                      ! Solve dnU_i/ds(i) = nU[xyz] iff g%gn(i)>1
  endif

                                         call timer('source boundaries','start')
  add = .true.; call impose_source_boundaries(nion,nUx,nUy,nUz,add)     ! Boundaries for (flux-)densities

  if (do_update) then
    ! Impose boundaries, exchange particles, and sort them
    !-------------------------------------------------------------------
                                         call timer('particle boundaries','start')
    call impose_particle_boundaries
                                         call timer('send particles','start')
    call send_particles
                                         call timer('sort','start')
    call sort

    ! Find time averaged number densities and update fields
    !-------------------------------------------------------------------
    do isp=1,nspecies
    do iz=1,g%n(3)
    do iy=1,g%n(2)
      do ix=1,g%n(1)                                                              
        !!!!!!!!!!!!!!!!
        ! FIXME! We need to add orbital advection to s and fields%d to properly
        ! FIXME! construct average in a shear-periodic box
        !!!!!!!!!!!!!!!!
        s = nion(ix,iy,iz,isp); nion(ix,iy,iz,isp) = 0.5*(fields(ix,iy,iz,isp)%d + s)
        fields(ix,iy,iz,isp)%d = s
        fields(ix,iy,iz,isp)%v(1) = nUx(ix,iy,iz,isp)
        fields(ix,iy,iz,isp)%v(2) = nUy(ix,iy,iz,isp)
        fields(ix,iy,iz,isp)%v(3) = nUz(ix,iy,iz,isp)
      enddo
    enddo
    enddo
    enddo
                                         call timer('source boundaries','start')
    add = .false.; call impose_source_boundaries(nion,nUx,nUy,nUz,add)  ! Boundaries for (flux-)densities
  else
    ! Find time averaged number densities without updating fields
    !-------------------------------------------------------------------
    do isp=1,nspecies
    do iz=1,g%n(3)
    do iy=1,g%n(2)
      do ix=1,g%n(1)
        nion(ix,iy,iz,isp) = 0.5*(nion(ix,iy,iz,isp) + fields(ix,iy,iz,isp)%d)
      enddo
    enddo  
    enddo
    enddo
                                         call timer('source boundaries','start')
    add = .false.; call impose_source_boundaries(nion,nUx,nUy,nUz,add)  ! Boundaries for (flux-)densities
  endif

  call trace_exit('move_particles')
END SUBROUTINE move_particles
!=======================================================================
SUBROUTINE init_particle_pusher
  USE move_particles_m
  implicit none
  boris_pusher => boris_pusher_nonrel
END SUBROUTINE
!=======================================================================
