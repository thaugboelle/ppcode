! Patch/hybrid/periodic.f90 $Id$
! vim: nowrap
! Module for field boundary conditions

! Boundary conditions for electric fields
!=====================================================================!
SUBROUTINE e_boundaries(ex,ey,ez)
  USE grid_m, ONLY : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: ex,ey,ez

  call overlap(ex)
  call overlap(ey)
  call overlap(ez)
END SUBROUTINE e_boundaries
! Boundary conditions for magnetic fields
!=====================================================================!
SUBROUTINE b_boundaries(bx,by,bz)
  USE grid_m, ONLY : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: bx, by, bz

  call overlap(bx)
  call overlap(by)
  call overlap(bz)
END SUBROUTINE b_boundaries
! Boundary condition for electric potential
!=====================================================================!
SUBROUTINE phi_boundaries(phi)
  USE grid_m, ONLY : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: phi

  call overlap(phi)
END SUBROUTINE phi_boundaries
! Boundary condition for magnetic potential
!=====================================================================!
SUBROUTINE phib_boundaries(phi)
  USE grid_m, ONLY : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: phi

  call overlap(phi)
END SUBROUTINE phib_boundaries
! Initialization for source (number density and number-density-flux) boundaries
!=======================================================================
SUBROUTINE init_source_boundaries
  USE params, only : do_shear, shear
  if (do_shear .and. shear .ne. 0.) &
    call error('init_source_boundaries','do_shear true and shear > 0 is incompatible'// &
               ' with a periodic box. Use BOUNDARY=shear in your options.mkf and recompile.')
END SUBROUTINE init_source_boundaries
! Boundaries for ion-flux, including summation in ghostzones
! If add is false, we should not do summation in boundaries
!=======================================================================
SUBROUTINE impose_source_boundaries(n,nUx,nUy,nUz,add)
  USE species, ONLY : nspecies
  USE grid_m,  ONLY : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3),nspecies) :: n, nUx, nUy, nUz
  logical :: add

  if (.not. add) return

  call overlap_add(nUx)
  call overlap_add(nUy)
  call overlap_add(nUz)
  call overlap_add(n)
END SUBROUTINE impose_source_boundaries
! Particle boundary conditions
!=====================================================================!
SUBROUTINE impose_particle_boundaries
  implicit none
  ! No special boundaries. SendParticles takes care of sending between mpi-threads
END SUBROUTINE impose_particle_boundaries
!=======================================================================
