! Particles/move_particles.f90 Patch/vectorize/move_particles.f90 $Id$
! vim: nowrap
!=======================================================================
! This routine handles OpenMP parallelization, by creating a "cell" 
! instance for each thread, passing it on to a particle mover, and then
! storing the data back into the permanent particle structure.
!=======================================================================
SUBROUTINE move_particles
  USE species, only: cellvector, nspecies, mid
  USE grid_m, only: nx, ny, nz
  implicit none
  type(cellvector), dimension(:), allocatable:: cell
  integer:: ix, iy, iz
  character(len=mid), save:: id = &
    'Patch/vectorize/move_particles.f90 $Id$'
!.......................................................................
  call print_id (id)
  call trace_enter ('MOVE_PARTICLES')
                                    call timer('move_particles-1','x')
  
  allocate (cell(nspecies))
  call create_cell(cell)                                                ! create cell vector
  do iz=1,nz                                                            ! loop over cells
    do iy=1,ny
      do ix=1,nx
        call get_cell (ix,iy,iz,cell)                                   ! get cell data
        call move_particles_in (cell)                                   ! move particles
        call put_cell (ix,iy,iz,cell)                                   ! put cell data
      enddo
    enddo
  enddo
  call kill_cell(cell)                                                  ! kill on each thread
  !$ omp end parallel                                                   ! end OMP parallel region
  deallocate (cell)
  call trace_exit ('MOVE_PARTICLES')
END SUBROUTINE move_particles

! Move the particles that are inside the given cell
!-----------------------------------------------------------------------
SUBROUTINE move_particles_in (cell)
 USE params  , only : nsubcycle, dt, grav_acc, periodic, &
                      particleupdates, time, stdout, nspectra
 USE grid_m,   only : g, drmax, ex, ey, ez, bx, by, bz
 USE units   , only : c, elm
 USE species , only : cellvector, particle, sp, nspecies, mcoord, mdim

 implicit none
 
 type(cellvector), dimension(nspecies)  :: cell
 type(particle), pointer                :: pa
 type(particle), pointer, dimension(:)  :: pav
 integer, dimension(3) :: q,ir
 real, dimension(3)    :: r,dr,dp,dps,p,v,v0,v1,dvdt,dvsdts,b,bdot, &
                          ods,EE,BB,t,p1,t1
 real                  :: hE,hB,o,odt,t2,s,ut,og,gg,g0,g1,p0,p2,pp, &
                          gi,denom,mass,w,csub,ct,cts,dts,odts,ts
 integer               :: i,isp,isub,nsubvar,tnp,np
 integer(kind=8)       :: ip
 logical               :: isset_i8_flag, gammasteps, realmove, do_grav
 integer, save         :: mprint=5
 real, parameter       :: sqrt2 = sqrt(2.)
 real, allocatable, dimension(:,:):: p_r, p_q, p_p, p_d, p_EE, p_BB
 real, allocatable, dimension(:)  :: p_e
!.......................................................................
!do_varsub  = .true.
 gammasteps = .false.
 realmove   = .false.

 ods = g%ods
 drmax = 0.
 do isp=1,nspecies                                                      ! loop over species
   if (cell(isp)%np == 0) cycle                                         ! short cut
   np = cell(isp)%np
   allocate (p_r (np,3), p_p (np,3), p_d(np,3), &
             p_q (np,3), p_EE(np,3), p_BB(np,3), p_e(np  ))
  pav => cell(isp)%particle                                             ! for convenience
  if (sp(isp)%charge /= 0.0) then
     odt = 1./dt                                                        ! one over dt
     hE = 0.5*dt*sp(isp)%charge/sp(isp)%mass                            ! perfectly time centered
     hB = hE*elm%kf                                                     ! B prefactor
                                               call timer('scatter','x')
     do ip=1,cell(isp)%np                                               ! loop over parts in cell
       !----------------------------------------------------------------
       ! Field interpolation
       !----------------------------------------------------------------
       pa => cell(isp)%particle(ip)                                     ! particle data
       r = pa%r                                                         ! fractional coordinate
       q = pa%q                                                         ! integer coordinate
       p = pa%p                                                         ! gamma*v
       if (any((.not. periodic) .and. &                                 ! non-periodic case
               (q < 0 .or. q >= g%n))) then                             ! outside box
           EE = 0.                                                      ! assume zero fields     
           BB = 0.                                                           
       else
         call scatter_fields (r,q,Ex,Ey,Ez,EE,Bx,By,Bz,BB)              ! interpolate to EE,BB
       endif
       p_EE(ip,:) = EE
       p_BB(ip,:) = BB
       p_r(ip,:) = r
       p_q(ip,:) = q
       p_p(ip,:) = p
       p_d(ip,:) = pa%p
     end do
                                                   call timer('Vay','x')
     do ip=1,np
       !----------------------------------------------------------------
       ! Lorentz Force - Vay pusher.  This loop vectorizes with the ifort
       ! compiler, but only after introducing locally allocated variables
       ! (names p_*), and using the t1 scratch variable to avois t=t*o.
       !----------------------------------------------------------------
       o = 1./sqrt(1.+(p_p(ip,1)*p_p(ip,1)+ &
                       p_p(ip,2)*p_p(ip,2)+ &
                       p_p(ip,3)*p_p(ip,3))*c%oc2) 
       t(1) = hB * p_BB(ip,1)
       t(2) = hB * p_BB(ip,2)
       t(3) = hB * p_BB(ip,3)
       t2 = t(1)*t(1)+t(2)*t(2)+t(3)*t(3)
       p1(1) = p_p(ip,1) + 2.*he*p_EE(ip,1) + o*(p_p(ip,2)*t(3)-p_p(ip,3)*t(2))   
       p1(2) = p_p(ip,2) + 2.*he*p_EE(ip,2) + o*(p_p(ip,3)*t(1)-p_p(ip,1)*t(3))   
       p1(3) = p_p(ip,3) + 2.*he*p_EE(ip,3) + o*(p_p(ip,1)*t(2)-p_p(ip,2)*t(1))   
       s = 1. + (p1(1)*p1(1)+p1(2)*p1(2)+p1(3)*p1(3))*c%oc2 - t2
       pp = (p1(1)*t(1) + p1(2)*t(2) + p1(3)*t(3))*c%oc
       o = sqrt2 * 1./sqrt(s + sqrt(s*s + 4.*(t2 + pp*pp))) 
       t1(1) = t(1)*o                                                   ! t1 needed to vectorize
       t1(2) = t(2)*o
       t1(3) = t(3)*o
       s = 1./(1 + t2*o*o)
       ut = p1(1)*t1(1) + p1(2)*t1(2) + p1(3)*t1(3)
       p_p(ip,1) = s * (p1(1) + ut*t1(1) + p1(2)*t1(3) - p1(3)*t1(2))
       p_p(ip,2) = s * (p1(2) + ut*t1(2) + p1(3)*t1(1) - p1(1)*t1(3))
       p_p(ip,3) = s * (p1(3) + ut*t1(3) + p1(1)*t1(2) - p1(2)*t1(1))

       !----------------------------------------------------------------- 
       ! store total momentum change for particle
       !----------------------------------------------------------------
       p_d(ip,1) = p(1) - p_d(ip,1)
       p_d(ip,2) = p(2) - p_d(ip,2)
       p_d(ip,3) = p(3) - p_d(ip,3)   
     end do                                                               
   end if
   if (sp(isp)%mass == 0.) then                                         ! mass?
     do ip=1,cell(isp)%np                                               ! loop over particles
       !----------------------------------------------------------------
       ! Massless particles
       !----------------------------------------------------------------
       p(1) = p_p(ip,1) + p_d(ip,1)                                     ! scratch momentum array
       p(2) = p_p(ip,1) + p_d(ip,2)                                     ! scratch momentum array
       p(3) = p_p(ip,2) + p_d(ip,3)                                     ! scratch momentum array
       p_e(ip) = sqrt((p(1)**2+p(2)**2+p(3)**2)*c%oc2)                  ! photon dimensionless energy
       v(1) = p(1)/p_e(ip)                                              ! velocity
       v(2) = p(2)/p_e(ip)                                              ! velocity
       v(3) = p(3)/p_e(ip)                                              ! velocity
       p_e(ip) = p_e(ip)*c%mc                                           ! photon energy
       dr(1) = v(1)*dt*ods(1)                                           ! position upd, normalized
       dr(2) = v(2)*dt*ods(2)                                           ! position upd, normalized
       dr(3) = v(3)*dt*ods(3)                                           ! position upd, normalized
       drmax = max(drmax,dr(1),dr(2),dr(3))                             ! for set_dt
       r(1) = p_r(ip,1) + dr(1)                                         ! position upd, normalized
       r(2) = p_r(ip,2) + dr(2)                                         ! position upd, normalized
       r(3) = p_r(ip,3) + dr(3)                                         ! position upd, normalized
       q(1) = floor(r(1))                                               ! cell changes
       q(2) = floor(r(2))                                               ! cell changes
       q(3) = floor(r(3))                                               ! cell changes
       p_q(ip,1) = p_q(ip,1) + q(1)                                     ! integer coord update
       p_q(ip,2) = p_q(ip,2) + q(2)                                     ! integer coord update
       p_q(ip,3) = p_q(ip,3) + q(3)                                     ! integer coord update
       p_r(ip,1) = r(1) - q(1)                                          ! rescale to [0:1)
       p_r(ip,2) = r(2) - q(2)                                          ! rescale to [0:1)
       p_r(ip,3) = r(3) - q(3)                                          ! rescale to [0:1)
       p_p(ip,1) = p(1)                                                 ! commit momentum
       p_p(ip,2) = p(2)                                                 ! commit momentum
       p_p(ip,3) = p(3)                                                 ! commit momentum
     end do
   else
                                                  call timer('mass','x')
     mass = sp(isp)%mass                                                ! mass
     do ip=1,np                                                         ! loop over part in cell
       !---------------------------------------------------------------- 
       ! Particles with mass
       !----------------------------------------------------------------
       p(1) = p_p(ip,1) + p_d(ip,1)                        
       p(2) = p_p(ip,2) + p_d(ip,2)                                     ! momentum upd
       p(3) = p_p(ip,3) + p_d(ip,3)                                     ! momentum upd
       p2 = p(1)**2+p(2)**2+p(3)**2                                     ! momentum squared
       og = 1./sqrt(1.+p2*c%oc2)                                        ! gamma
       p_e(ip) = mass*p2*og/(1.+og)                                     ! kinetic energy
       og = og*dt
       dr(1) = p(1)*og*ods(1)
       dr(2) = p(2)*og*ods(2)
       dr(3) = p(3)*og*ods(3)
       drmax = max(drmax,dr(1),dr(2),dr(3))
       r(1) = p_r(ip,1) + dr(1)
       r(2) = p_r(ip,2) + dr(2)
       r(3) = p_r(ip,3) + dr(3)
       q(1) = floor(r(1))
       q(2) = floor(r(2))
       q(3) = floor(r(3))
       p_q(ip,1) = p_q(ip,1) + q(1)
       p_q(ip,2) = p_q(ip,2) + q(2)
       p_q(ip,3) = p_q(ip,3) + q(3)
       p_r(ip,1) = r(1) - q(1)
       p_r(ip,2) = r(2) - q(2)
       p_r(ip,3) = r(3) - q(3)
       p_p(ip,1) = p(1)                                                 ! commit momentum
       p_p(ip,2) = p(2)                                                 ! commit momentum
       p_p(ip,3) = p(3)                                                 ! commit momentum
     end do
     do i=1,3; do ip=1,np
       pav(ip)%r (i) = p_r(ip,i)
       pav(ip)%q (i) = p_q(ip,i)
       pav(ip)%p (i) = p_p(ip,i)
     end do; end do
   endif
   deallocate (p_r, p_q, p_p, p_d, p_e, p_BB, p_EE)
   particleupdates = particleupdates + cell(isp)%np
 end do
END SUBROUTINE
