! Patch/lean/flux_solver.f90 $Id$
! vim: nowrap
!===============================================================================
! Module with a solver that inverts exactly a 6th order stagger operator
! We still need to transpose the fields to make the solve along the full ray on
! a single core. This is done in the mpi module.
!===============================================================================
MODULE flux_solver
  implicit none
#if PREC==4
  integer, parameter :: wp_fl=4                                                 ! kind for penta solve oprations
#else
#if PREC==8
  integer, parameter :: wp_fl=8                                                 ! kind for penta solve oprations
#else
  integer, parameter :: wp_fl=16                                                ! kind for penta solve oprations
#endif
#endif
  integer, parameter :: order=6                                                 ! order of stagger ops
  real(kind=wp_fl), parameter :: dd(3)=(/1067./960.,-29./480.,3./640./)         ! coefs for a+b+c,b+c,c
  integer            :: max_n=-1
  real(kind=wp_fl), allocatable :: alpha(:), gamma(:), delta(:)
!===============================================================================
CONTAINS
! Solve a symmetric penta-diagonal linear system with fixed coefficients and
! diagonal a, diagonal+1 b, diagonal+2 c, and rhs r
! The solution is left in r on return
! Based on algorithm from : Numerical algorithms with C by Gisela Engeln-M�llges, Frank Uhlig
! This version has the input array transposed compared to the version below
!===============================================================================
SUBROUTINE penta_solve_transposed(n,ny,a,b,c,r)
  implicit none
  integer, intent(in)                :: n, ny
  real(kind=wp_fl),    intent(in)    :: a, b, c
  real(kind=wp_fl),  dimension(ny,n) :: r
  real(kind=wp_fl)                   :: inv
  integer                            :: i,j

  if (n > max_n) then
    if (allocated(alpha)) deallocate(alpha,gamma,delta)
    allocate(alpha(n),gamma(n-1),delta(n-2))
    max_n=n
  endif
  alpha(1) = a
  inv      = 1./a
  gamma(1) = b*inv
  delta(1) = c*inv

  alpha(2) = a-b*gamma(1)
  inv      = 1./alpha(2)
  gamma(2) = (b-c*gamma(1))*inv
  delta(2) = c*inv

  do i=3,n-2
    alpha(i) = a-c*delta(i-2)-alpha(i-1)*gamma(i-1)*gamma(i-1)
    inv      = 1./alpha(i)
    gamma(i) = (b-c*gamma(i-1))*inv
    delta(i) = c*inv
  enddo

  alpha(n-1) = a-c*delta(n-3)-alpha(n-2)*gamma(n-2)*gamma(n-2)
  gamma(n-1) = (b-c*gamma(n-2))/alpha(n-1)
  alpha(n)   = a-c*delta(n-2)-alpha(n-1)*gamma(n-1)*gamma(n-1)
    
  do j=1,ny
    r(j,2)=r(j,2)-gamma(1)*r(j,1)
  enddo
  do i=3,n
    do j=1,ny
      r(j,i)   = r(j,i)-gamma(i-1)*r(j,i-1)-delta(i-2)*r(j,i-2)
      r(j,i-2) = r(j,i-2)/alpha(i-2)
    enddo
  enddo
  
  do j=1,ny
    r(j,n)   = r(j,n)/alpha(n)
    r(j,n-1) = r(j,n-1)/alpha(n-1)-gamma(n-1)*r(j,n)
  enddo
  do i=n-2,1,-1
    do j=1,ny
      r(j,i) = r(j,i)-gamma(i)*r(j,i+1)-delta(i)*r(j,i+2)
    enddo
  enddo
END SUBROUTINE penta_solve_transposed
! Solve a symmetric penta-diagonal linear system with fixed coefficients and
! diagonal a, diagonal+1 b, diagonal+2 c, and rhs r
! The solution is left in r on return
! Based on algorithm from : Numerical algorithms with C by Gisela Engeln-M�llges, Frank Uhlig
!===============================================================================
SUBROUTINE penta_solve(n,ny,a,b,c,r)
  implicit none
  integer, intent(in)                :: n, ny
  real(kind=wp_fl),    intent(in)    :: a, b, c
  real(kind=wp_fl),  dimension(n,ny) :: r
  real(kind=wp_fl)                   :: inv
  integer                            :: i, j

  if (n > max_n) then
    if (allocated(alpha)) deallocate(alpha,gamma,delta)
    allocate(alpha(n),gamma(n-1),delta(n-2))
    max_n=n
  endif
  alpha(1) = a
  inv      = 1.0_wp_fl/a
  gamma(1) = b*inv
  delta(1) = c*inv

  alpha(2) = a-b*gamma(1)
  inv      = 1.0_wp_fl/alpha(2)
  gamma(2) = (b-c*gamma(1))*inv
  delta(2) = c*inv

  do i=3,n-2
    alpha(i) = a-c*delta(i-2)-alpha(i-1)*gamma(i-1)*gamma(i-1)
    inv      = 1.0_wp_fl/alpha(i)
    gamma(i) = (b-c*gamma(i-1))*inv
    delta(i) = c*inv
  enddo

  alpha(n-1) = a-c*delta(n-3)-alpha(n-2)*gamma(n-2)*gamma(n-2)
  gamma(n-1) = (b-c*gamma(n-2))/alpha(n-1)
  alpha(n)   = a-c*delta(n-2)-alpha(n-1)*gamma(n-1)*gamma(n-1)
    
  do j=1,ny
    r(2,j)=r(2,j)-gamma(1)*r(1,j)
    do i=3,n
      r(i,j)   = r(i,j)-gamma(i-1)*r(i-1,j)-delta(i-2)*r(i-2,j)
      r(i-2,j) = r(i-2,j)/alpha(i-2)
    enddo
    
    r(n,j)   = r(n,j)/alpha(n)
    r(n-1,j) = r(n-1,j)/alpha(n-1)-gamma(n-1)*r(n,j)
    do i=n-2,1,-1
      r(i,j) = r(i,j)-gamma(i)*r(i+1,j)-delta(i)*r(i+2,j)
    enddo
  enddo
END SUBROUTINE penta_solve
! EXAMPLE BOUNDARY CONDITIONS:
! boundary conditions for a periodic box
!===============================================================================
SUBROUTINE ddj_boundaries_periodic(n,u,v,q,dd,dir,isp)
  implicit none
  integer, intent(in) :: n, dir, isp
  real,    intent(in) :: dd(3)
  real                :: v(n-2,2), q(2,2)
  real, target        :: u(n-2,2)
  ! We have to get the stagger operators from the "other side" of the matrix
  u(1,1) = dd(3);   u(1,2) = dd(2);   u(2,2) = dd(3)
  v(1,1) = dd(3);   v(1,2) = dd(2);   v(2,2) = dd(3)
END SUBROUTINE ddj_boundaries_periodic 
! boundary conditions for a reflecting boundary
!===============================================================================
SUBROUTINE ddj_boundaries_reflect(n,u,v,q,dd,dir,isp)
  implicit none
  integer, intent(in) :: n, dir, isp
  real,    intent(in) :: dd(3)
  real                :: v(n-2,2), q(2,2)
  real, target        :: u(n-2,2)
  q(2,2) = q(2,2) + dd(3) ! we have to add a 'c' element, because the difference is symmetric
                          ! across the boundary, and zero on the boundary
END SUBROUTINE ddj_boundaries_reflect
!===============================================================================
END MODULE flux_solver
!===============================================================================
! Find flux by solving along ray for v(i)-v(i-1), which is numerically stable
! Get the boundary elements from call to boundary routine (periodic, reflecting, etc)
! Use a solver for a simple penta-diagonal matrix and some linear algebra to get
! the solution for dv = v(i)-v(i-1), and then make simple prefix-sum update to get v(i)
!
! We solve the equation system by splitting it up into four sub-matrices :
! P[n-2,n-2]: penta diagonal, symmetric, positiv definite, with a very fast solution
! Q[2,2]: The remaining 4 corner elements [[a,b],[b,a]]
! U, V: The right and lower 2x(n-2) matrices with arbitrary boundary conditions
! This gives the system of eqs : [ [P, U], [V, Q] ] X = [B]
! That can be solved as follows using only 3 solutions with P, and a couple of matrix multiplications :
! B(1:n-2) = P^-1 B(1:n-2); U = P^-1 U
! Q = Q - V U; B(n-1:n) = B(n-1:n) - V B(1:n-2);
! Solve eqs for Q X = B(n-1:n) and store in B(n-1:n)
! Finally update: B(1:n-2) = B(1:n-2) - U B(n-1:n)
!
! For generality we allow the boundary conditions to do anything they like to the
! last two coloumns and rows. This means we can only have something special
! on the _upper_ boundary, or periodic boundary conditions.
!
! This is maybe not optimal, but solving a general boundary condition on both
! upper and lower boundary is in fact trivial. Then we would just have to solve
! a system of four equations with four unknowns explicitly in point 6) below.
! Ie. we would have the systems :
! [ Q11 | V1 | Q12 ] [ X1 ]   [ B1 ]
! [ U1  | P  | U2  ] [ XP ] = [ BP ]
! [ Q21 | V2 | Q22 ] [ X2 ]   [ B2 ]
! where P = [n-4, n-4] and penta-diagonal, Qij is [2,2], Ui, Vi are [n-2,2], and
! X = [ X1(2), XP(n-4), X2(2) ], B = [ B1(2), BP(n-4), B2(2) ] are the solution and
! right hand side. This can be solved completely analogously to the system above.
! 
!===============================================================================
SUBROUTINE vflux_solve(n,ny,b,dir,isp)
  USE flux_solver
  implicit none
  integer, intent(in) :: n, ny, dir, isp
  real                :: b(n,ny),q(2,2)
  integer             :: i,j,k
  real                :: b1, b2, idetQ
  real(kind=8)        :: flux
  real, allocatable   :: u(:,:)
  real, allocatable   :: v(:,:)
  ! 1) Call the solver for the band diagonal system (B(1:n-2) contains now the solution)
  call penta_solve(n-2,ny,dd(1),dd(2),dd(3),real(b(1:n-2,:),kind=wp_fl))

  ! 2) Make the boundary and lower corner matrices U, V, Q
  allocate(u(n-2,2),v(n-2,2))
  do i=1,n-3
   u(i,1) = 0.; u(i,2) = 0.
   v(i,1) = 0.; v(i,2) = 0.
  enddo
  ! Non-Zero elements for U, V, Q : 
  q(1,1)   = dd(1); q(2,1)   = dd(2); q(1,2)   = dd(2); q(2,2) = dd(1)
  u(n-3,1) = dd(3); u(n-2,1) = dd(2); u(n-2,2) = dd(3)                          ! Terms from penta-diagonal 
  v(n-3,1) = dd(3); v(n-2,1) = dd(2); v(n-2,2) = dd(3)
  call ddj_boundaries(n,u,v,q,dd,dir,isp)                                       ! Update with off-diag terms

  ! 3) Solve for the 2 boundary vectors (U afterwards contains the solution)
  call penta_solve(n-2,2,dd(1),dd(2),dd(3),real(u,kind=wp_fl))

  ! 4) Update Q and calc inverse determinant :
  do i=1,2; do j=1,2; do k=1,n-2
    q(i,j) = q(i,j) - v(k,i)*u(k,j)
  enddo; enddo; enddo
  idetQ = 1. / ( q(1,1)*q(2,2) - q(1,2)*q(2,1) )

  do j=1,ny
    ! 5) Update the two last entries in the rhs vector B
    do i=1,n-2
      b(n-1,j) = b(n-1,j) - v(i,1)*b(i,j)
      b(n,  j) = b(n,  j) - v(i,2)*b(i,j)
    enddo

    ! 6) Solve the equation system Q X = B(n-1:n), and store solution in b(n-1:n) :
    b1 = b(n-1,j)
    b(n-1,j) = idetQ * ( q(2,2)*b1   - q(1,2)*b(n,j) )
    b(n,  j) = idetQ * ( q(1,1)*b(n,j) - q(2,1)*b1   ) 

    ! 7) Update solution for b(1:n-2) :
    do i=1,n-2
      b(i,j) = b(i,j) - u(i,1)*b(n-1,j) - u(i,2)*b(n,j)
    enddo

    ! 8) Now do the prefix sum for getting the flux into b (supposing we are ok on boundaries) :
    ! Notice that overall normalization is arbitrary, and should be determined by f.x. physics
    flux = 0.0
    do i=1,n
      flux = flux + b(i,j)
      b(i,j) = flux
    enddo
  enddo
END SUBROUTINE vflux_solve
! Solve the flux under the assumption of zero current in the boundary. This is normally ok
! Notice how the x-direction is treated transposed compared to the y- and z-, for optimal memory layout
!===============================================================================
SUBROUTINE compute_flux_new
  USE grid_m,    only : g
  USE species, only : fields, nspecies
  USE flux_solver
  implicit none
  real(kind=wp_fl),  allocatable, dimension(:,:) :: out
  real(kind=8), allocatable, dimension(:)   :: fluxv
  real(kind=8)                              :: flux
  integer                                   :: dir,n,area,i,j,ix,iy,iz,isp

  ! Loop of the three components Jx, Jy, and Jz
  !-----------------------------------------------------------------------------
  do dir=1,3
    if (g%gn(dir)==1) cycle                                                     ! cycle if only one point in this direction
    n    = g%n(dir)
    area = product(g%n) / n
    if (allocated(out)) deallocate(out)
    if (dir==1) then
      allocate(out(n,area))
    else
      if (allocated(fluxv)) deallocate(fluxv)
      allocate(fluxv(area))
      allocate(out(area,n))
    endif

    do isp=1, nspecies

      ! Copy fields data to contigious memory 
      !-------------------------------------------------------------------------
      select case (dir)
      case(1)
        do iz=1,g%n(3)
        do iy=1,g%n(2)
          i = iy + (iz - 1)*g%n(2)
          do ix=1,g%n(1)
            j = ix
            out(j,i) = fields(ix,iy,iz,isp)%v(dir)
          enddo
        enddo
        enddo
      case(2)
        do iz=1,g%n(3)
        do iy=1,g%n(2)
          j = iy
          do ix=1,g%n(1)
            i = ix + (iz - 1)*g%n(1)
            out(i,j) = fields(ix,iy,iz,isp)%v(dir)
          enddo
        enddo
        enddo
      case(3)
        do iz=1,g%n(3)
        do iy=1,g%n(2)
          j = iz
          do ix=1,g%n(1)
            i = ix + (iy - 1)*g%n(1)
            out(i,j) = fields(ix,iy,iz,isp)%v(dir)
          enddo
          enddo
        enddo
      endselect
 
      ! Solve the penta-diagonal equation system along the coordinate direction
      !-------------------------------------------------------------------------
      if (dir==1) then
        call penta_solve(n,area,dd(1),dd(2),dd(3),out)
        do i=1,area
          flux = out(1,i)
          do j=2,n
            flux = flux + out(j,i)
            out(j,i) = flux
          enddo
        enddo
      else
        call penta_solve_transposed(n,area,dd(1),dd(2),dd(3),out)
        do i=1,area
          fluxv(i) = out(i,1)
        enddo
        do j=2,n
          do i=1,area
            fluxv(i) = fluxv(i) + out(i,j)
            out(i,j) = fluxv(i)
          enddo
        enddo
      endif
 
      ! Copy data back to fields array -- now J is stored in the array
      !-------------------------------------------------------------------------
      select case (dir)
      case(1)
        do iz=1,g%n(3)
        do iy=1,g%n(2)
          i = iy + (iz - 1)*g%n(2)
          do ix=1,g%n(1)
            j = ix
            fields(ix,iy,iz,isp)%v(dir) = out(j,i)
          enddo
        enddo
        enddo
      case(2)
        do iz=1,g%n(3)
        do iy=1,g%n(2)
          j = iy
          do ix=1,g%n(1)
            i = ix + (iz - 1)*g%n(1)
            fields(ix,iy,iz,isp)%v(dir) = out(i,j)
          enddo
        enddo
        enddo
      case(3)
        do iz=1,g%n(3)
        do iy=1,g%n(2)
          j = iz
          do ix=1,g%n(1)
            i = ix + (iy - 1)*g%n(1)
            fields(ix,iy,iz,isp)%v(dir) = out(i,j)
          enddo
          enddo
        enddo
      endselect

    enddo
  enddo

  if (allocated(fluxv)) deallocate(fluxv)
  deallocate(out)

END SUBROUTINE compute_flux_new
! Solve the flux under the assumption of zero current in the boundary. This is normally ok
! Notice how the x-direction is treated transposed compared to the z-, for optimal memory layout
!===============================================================================
SUBROUTINE compute_flux_new_2d
  USE grid_m,    only : g
  USE species, only : fields, nspecies
  USE flux_solver
  implicit none
  real(kind=wp_fl),  allocatable, dimension(:,:) :: out
  real(kind=8), allocatable, dimension(:)   :: fluxv
  real(kind=8)                              :: flux
  integer                                   :: dir,ix,iy,iz,isp

  ! Loop of the three components Jx, Jy, and Jz
  !-----------------------------------------------------------------------------
  iy = g%lb(2)
  allocate(out(g%n(1),g%n(3)),fluxv(g%n(1)))
  do dir=1,3,2

    do isp=1,nspecies

      ! Copy fields data to contigious memory 
      !-------------------------------------------------------------------------
      do iz=1,g%n(3)
      do ix=1,g%n(1)
        out(ix,iz) = fields(ix,iy,iz,isp)%v(dir)
      enddo
      enddo

      ! Solve the penta-diagonal equation system along the coordinate direction
      !-------------------------------------------------------------------------
      if (dir==1) then
        call penta_solve(g%n(1),g%n(3),dd(1),dd(2),dd(3),out)
        do iz=1,g%n(3)
          flux = 0.0_8
          do ix=1,g%n(1)
            flux = flux + out(ix,iz)
            out(ix,iz) = flux
          enddo
        enddo
      else
        call penta_solve_transposed(g%n(3),g%n(1),dd(1),dd(2),dd(3),out)
        fluxv = 0.0_8
        do iz=1,g%n(3)
          fluxv = fluxv + out(:,iz)
          out(:,iz) = fluxv
        enddo
      endif
 
      ! Copy data back to fields array -- now J is stored in the array
      !-------------------------------------------------------------------------
      do iz=1,g%n(3)
      do ix=1,g%n(1)
        fields(ix,iy,iz,isp)%v(dir) = out(ix,iz)
      enddo
      enddo
    enddo
  enddo
  deallocate(out,fluxv)

END SUBROUTINE compute_flux_new_2d
!===============================================================================
