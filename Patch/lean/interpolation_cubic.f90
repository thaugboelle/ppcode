! Patch/lean/interpolation_cubic.f90 $Id$
! vim: nowrap
!-------------------------------------------------------------------------------
MODULE interpolation
  implicit none
  ! Shorthand variables that doesn't change in the
  ! big particle loop
  logical :: floormethod  

  ! Scratch variables needed for interpolation
  real    :: wxm,wx0,wx1,wx2,wym,wy0,wy1,wy2,wzm,wz0,wz1,wz2                    ! centered weights
  real    :: dxm,dx0,dx1,dx2,dym,dy0,dy1,dy2,dzm,dz0,dz1,dz2                    ! staggered weights
  integer :: ixc,iyc,izc,ixs,iys,izs                                            ! indices for centred and staggered weights
  integer, save :: nprint=0
  integer, parameter :: sdb=-2, slb=-1, sub=2, suc=3                            ! parameters for stencil for local patch
!-------------------------------------------------------------------------------
END MODULE Interpolation
!-------------------------------------------------------------------------------
! The boundaries zones needed by this method
!-------------------------------------------------------------------------------
SUBROUTINE interpolation_boundaries
  USE grid_m, only : g
  implicit none
  g%lb = 3                                                                      ! def lower particle bndry
  g%ub = 2                                                                      ! def upper particle bndry (n-ub)
END SUBROUTINE
! Routine for cutting out the cached fields used for interpolation
!-------------------------------------------------------------------------------
SUBROUTINE cache_fields(touched_row,ix,iy,iz,sEx,sEy,sEz,sBx,sBy,sBz)
  USE interpolation, only : sdb,slb,sub,suc
  USE grid_m,          only : g,Ex,Ey,Ez,Bx,By,Bz
  implicit none
  logical, intent(in) :: touched_row
  integer, intent(in) :: ix,iy,iz
  real, dimension(sdb:sub,slb:sub,slb:sub) :: sEx
  real, dimension(slb:sub,sdb:sub,slb:sub) :: sEy
  real, dimension(slb:sub,slb:sub,sdb:sub) :: sEz
  real, dimension(slb:sub,sdb:sub,sdb:sub) :: sBx
  real, dimension(sdb:sub,slb:sub,sdb:sub) :: sBy
  real, dimension(sdb:sub,sdb:sub,slb:sub) :: sBz
  integer             :: jx,jy,jz,kx,ky,kz
  ! copy fields over, taking care of boundaries and reusing slices where possible
  do jz=sdb,sub
    kz = jz + iz
    if (kz > 0 .and. kz <= g%n(3)) then
      do jy=sdb,sub
        ky = jy + iy
        if (ky > 0 .and. ky <= g%n(2)) then
          do jx=sdb,sub
            kx = jx + ix
            if (kx > 0 .and. kx <= g%n(1)) then
              if (touched_row) then
                if (jx==sdb) then
                  if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = sEx(jx+1,jy,jz)
                  if (jy > sdb) sBy(jx,jy,jz) = sBy(jx+1,jy,jz)
                  if (jz > sdb) sBz(jx,jy,jz) = sBz(jx+1,jy,jz)
                else if (jx < sub) then
                  if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = sEx(jx+1,jy,jz)
                  if (jz > sdb) sEy(jx,jy,jz) = sEy(jx+1,jy,jz)
                  if (jy > sdb) sEz(jx,jy,jz) = sEz(jx+1,jy,jz)
                  sBx(jx,jy,jz) = sBx(jx+1,jy,jz)
                  if (jy > sdb) sBy(jx,jy,jz) = sBy(jx+1,jy,jz)
                  if (jz > sdb) sBz(jx,jy,jz) = sBz(jx+1,jy,jz)
                else !if (jx==sub) then ! "if" not needed, this is the last option
                  if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = Ex(kx,ky,kz)
                  if (jz > sdb) sEy(jx,jy,jz) = Ey(kx,ky,kz)
                  if (jy > sdb) sEz(jx,jy,jz) = Ez(kx,ky,kz)
                  sBx(jx,jy,jz) = Bx(kx,ky,kz)
                  if (jy > sdb) sBy(jx,jy,jz) = By(kx,ky,kz)
                  if (jz > sdb) sBz(jx,jy,jz) = Bz(kx,ky,kz)
                endif
              else
                if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = Ex(kx,ky,kz)
                if (jx > sdb .and. jz > sdb) sEy(jx,jy,jz) = Ey(kx,ky,kz)
                if (jx > sdb .and. jy > sdb) sEz(jx,jy,jz) = Ez(kx,ky,kz)
                if (jx > sdb) sBx(jx,jy,jz) = Bx(kx,ky,kz)
                if (jy > sdb) sBy(jx,jy,jz) = By(kx,ky,kz)
                if (jz > sdb) sBz(jx,jy,jz) = Bz(kx,ky,kz)
              endif
            else
              if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = 0.
              if (jx > sdb .and. jz > sdb) sEy(jx,jy,jz) = 0.
              if (jx > sdb .and. jy > sdb) sEz(jx,jy,jz) = 0.
              if (jx > sdb) sBx(jx,jy,jz) = 0.
              if (jy > sdb) sBy(jx,jy,jz) = 0.
              if (jz > sdb) sBz(jx,jy,jz) = 0.
            endif
          enddo
        else
          do jx=sdb,sub
            if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = 0.
            if (jx > sdb .and. jz > sdb) sEy(jx,jy,jz) = 0.
            if (jx > sdb .and. jy > sdb) sEz(jx,jy,jz) = 0.
            if (jx > sdb) sBx(jx,jy,jz) = 0.
            if (jy > sdb) sBy(jx,jy,jz) = 0.
            if (jz > sdb) sBz(jx,jy,jz) = 0.
          enddo
        endif
      enddo
    else
      do jy=sdb,sub
        do jx=sdb,sub
          if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = 0.
          if (jx > sdb .and. jz > sdb) sEy(jx,jy,jz) = 0.
          if (jx > sdb .and. jy > sdb) sEz(jx,jy,jz) = 0.
          if (jx > sdb) sBx(jx,jy,jz) = 0.
          if (jy > sdb) sBy(jx,jy,jz) = 0.
          if (jz > sdb) sBz(jx,jy,jz) = 0.
        enddo
      enddo
    endif
  enddo
END SUBROUTINE cache_fields
! Compute weights for use in interpolating fields to particles, and for 1. step in current computation
!-------------------------------------------------------------------------------
SUBROUTINE makeweights_cell(np,mmp,pav,wx,wy,wz,dxn,dxm,dx0,dx1,dx2,dyn,dym,dy0,dy1,dy2,dzn,dzm,dz0,dz1,dz2)
  USE interpolation, only : sdb,slb,sub,suc
  USE grid_m,          only : g
  USE species,       only : particle
  implicit none
  integer, intent(in)  :: np, mmp
  type(particle), dimension(np) :: pav
  real, dimension(mmp) :: dxn,dxm,dx0,dx1,dx2,dyn,dym,dy0,dy1,dy2,dzn,dzm,dz0,dz1,dz2
  real, dimension(sdb:suc,mmp) :: wx,wy,wz
  real, parameter      :: one_sixth = 1./6., two_third = 2./3.
  real                 :: wa,wb,wc,wd,s,sm1,sm2, r(3)
  integer              :: ip
  logical              :: up
  do ip=1,np                                              ! loop over parts in cell
    ! ---------------------------------------------------------
    ! Find signed distance to center points in all directions.
    ! Project index into allowed range for interpolation.
    r = pav(ip)%r
    !-------------
    ! X_COORD CENTERED
    s = r(1); sm1 = 1. - s; sm2 = 2. - s 
    wx(-1,ip) = one_sixth * sm1*sm1*sm1
    wx( 0,ip) = two_third - 0.5*s*s*sm2
    wx( 1,ip) = two_third - 0.5*sm1*sm1*(1.+ s) 
    wx( 2,ip) = 1. - (wx(-1,ip) + wx(0,ip) + wx(1,ip))
    !-------------
    ! Y_COORD CENTERED
    s = r(2); sm1 = 1. - s; sm2 = 2. - s 
    wy(-1,ip) = one_sixth * sm1*sm1*sm1
    wy( 0,ip) = two_third - 0.5*s*s*sm2
    wy( 1,ip) = two_third - 0.5*sm1*sm1*(1.+ s) 
    wy( 2,ip) = 1. - (wy(-1,ip) + wy(0,ip) + wy(1,ip))
    !-------------
    ! Z_COORD CENTERED
    s = r(3); sm1 = 1. - s; sm2 = 2. - s 
    wz(-1,ip) = one_sixth * sm1*sm1*sm1
    wz( 0,ip) = two_third - 0.5*s*s*sm2
    wz( 1,ip) = two_third - 0.5*sm1*sm1*(1.+ s) 
    wz( 2,ip) = 1. - (wz(-1,ip) + wz(0,ip) + wz(1,ip))
    !-------------
    ! X_COORD SHIFTED IN X
    s = r(1) - 0.5; up = s >= 0
    s = s + merge(0.,1.,up); sm1 = 1. - s; sm2 = 2. - s
    wa = one_sixth * sm1*sm1*sm1
    wb = two_third - 0.5*s*s*sm2
    wc = two_third - 0.5*sm1*sm1*(1.+ s) 
    wd = 1. - (wa + wb + wc)
    dxn(ip) = merge(0., wa, up)
    dxm(ip) = merge(wa, wb, up)
    dx0(ip) = merge(wb, wc, up)
    dx1(ip) = merge(wc, wd, up)
    dx2(ip) = merge(wd, 0., up)
    !-------------
    ! Y_COORD SHIFTED IN Y
    s = r(2) - 0.5; up = s >= 0
    s = s + merge(0.,1.,up); sm1 = 1. - s; sm2 = 2. - s
    wa = one_sixth * sm1*sm1*sm1
    wb = two_third - 0.5*s*s*sm2
    wc = two_third - 0.5*sm1*sm1*(1.+ s) 
    wd = 1. - (wa + wb + wc)
    dyn(ip) = merge(0., wa, up)
    dym(ip) = merge(wa, wb, up)
    dy0(ip) = merge(wb, wc, up)
    dy1(ip) = merge(wc, wd, up)
    dy2(ip) = merge(wd, 0., up)
    !-------------
    ! Z_COORD SHIFTED IN Z
    s = r(3) - 0.5; up = s >= 0
    s = s + merge(0.,1.,up); sm1 = 1. - s; sm2 = 2. - s
    wa = one_sixth * sm1*sm1*sm1
    wb = two_third - 0.5*s*s*sm2
    wc = two_third - 0.5*sm1*sm1*(1.+ s) 
    wd = 1. - (wa + wb + wc)
    dzn(ip) = merge(0., wa, up)
    dzm(ip) = merge(wa, wb, up)
    dz0(ip) = merge(wb, wc, up)
    dz1(ip) = merge(wc, wd, up)
    dz2(ip) = merge(wd, 0., up)
  enddo
END SUBROUTINE
!-------------------------------------------------------------------------------
SUBROUTINE interpolate_EB(np,mmp,sEx,sEy,sEz,sBx,sBy,sBz,pEx,pEy,pEz,pBx,pBy,pBz, &
                          wx,wy,wz,dxn,dxm,dx0,dx1,dx2,dyn,dym,dy0,dy1,dy2,dzn,dzm,dz0,dz1,dz2)
  USE interpolation, only : sdb,slb,sub,suc
  USE grid_m,          only : g
  implicit none
  integer, intent(in)  :: np, mmp
  real, intent(in), dimension(sdb:sub,slb:sub,slb:sub) :: sEx
  real, intent(in), dimension(slb:sub,sdb:sub,slb:sub) :: sEy
  real, intent(in), dimension(slb:sub,slb:sub,sdb:sub) :: sEz
  real, intent(in), dimension(slb:sub,sdb:sub,sdb:sub) :: sBx
  real, intent(in), dimension(sdb:sub,slb:sub,sdb:sub) :: sBy
  real, intent(in), dimension(sdb:sub,sdb:sub,slb:sub) :: sBz
  real, intent(in), dimension(mmp) :: dxn,dxm,dx0,dx1,dx2,dyn,dym,dy0,dy1,dy2,dzn,dzm,dz0,dz1,dz2
  real, intent(in), dimension(sdb:suc,mmp) :: wx,wy,wz
  real,             dimension(mmp) :: pEx,pEy,pEz,pBx,pBy,pBz
  integer                          :: ip
  do ip=1,np
    !-----------------------------------
    ! X SHIFTED WEIGHTS
    pEx(ip) = &
         ((sEx(-2,-1,-1)*dxn(ip) + &
           sEx(-1,-1,-1)*dxm(ip) + &
           sEx( 0,-1,-1)*dx0(ip) + &
           sEx(+1,-1,-1)*dx1(ip) + &
           sEx(+2,-1,-1)*dx2(ip))*wy(-1,ip) + &
          (sEx(-2, 0,-1)*dxn(ip) + &
           sEx(-1, 0,-1)*dxm(ip) + &
           sEx( 0, 0,-1)*dx0(ip) + &
           sEx(+1, 0,-1)*dx1(ip) + &
           sEx(+2, 0,-1)*dx2(ip))*wy( 0,ip) + &
          (sEx(-2,+1,-1)*dxn(ip) + &
           sEx(-1,+1,-1)*dxm(ip) + &
           sEx( 0,+1,-1)*dx0(ip) + &
           sEx(+1,+1,-1)*dx1(ip) + &
           sEx(+2,+1,-1)*dx2(ip))*wy( 1,ip) + &
          (sEx(-2,+2,-1)*dxn(ip) + &
           sEx(-1,+2,-1)*dxm(ip) + &
           sEx( 0,+2,-1)*dx0(ip) + &
           sEx(+1,+2,-1)*dx1(ip) + &
           sEx(+2,+2,-1)*dx2(ip))*wy( 2,ip))*wz(-1,ip) + &
         ((sEx(-2,-1, 0)*dxn(ip) + &
           sEx(-1,-1, 0)*dxm(ip) + &
           sEx( 0,-1, 0)*dx0(ip) + &
           sEx(+1,-1, 0)*dx1(ip) + &
           sEx(+2,-1, 0)*dx2(ip))*wy(-1,ip) + &
          (sEx(-2, 0, 0)*dxn(ip) + &
           sEx(-1, 0, 0)*dxm(ip) + &
           sEx( 0, 0, 0)*dx0(ip) + &
           sEx(+1, 0, 0)*dx1(ip) + &
           sEx(+2, 0, 0)*dx2(ip))*wy( 0,ip) + &
          (sEx(-2,+1, 0)*dxn(ip) + &
           sEx(-1,+1, 0)*dxm(ip) + &
           sEx( 0,+1, 0)*dx0(ip) + &
           sEx(+1,+1, 0)*dx1(ip) + &
           sEx(+2,+1, 0)*dx2(ip))*wy( 1,ip) + &
          (sEx(-2,+2, 0)*dxn(ip) + &
           sEx(-1,+2, 0)*dxm(ip) + &
           sEx( 0,+2, 0)*dx0(ip) + &
           sEx(+1,+2, 0)*dx1(ip) + &
           sEx(+2,+2, 0)*dx2(ip))*wy( 2,ip))*wz( 0,ip) + &
         ((sEx(-2,-1,+1)*dxn(ip) + &
           sEx(-1,-1,+1)*dxm(ip) + &
           sEx( 0,-1,+1)*dx0(ip) + &
           sEx(+1,-1,+1)*dx1(ip) + &
           sEx(+2,-1,+1)*dx2(ip))*wy(-1,ip) + &
          (sEx(-2, 0,+1)*dxn(ip) + &
           sEx(-1, 0,+1)*dxm(ip) + &
           sEx( 0, 0,+1)*dx0(ip) + &
           sEx(+1, 0,+1)*dx1(ip) + &
           sEx(+2, 0,+1)*dx2(ip))*wy( 0,ip) + &
          (sEx(-2,+1,+1)*dxn(ip) + &
           sEx(-1,+1,+1)*dxm(ip) + &
           sEx( 0,+1,+1)*dx0(ip) + &
           sEx(+1,+1,+1)*dx1(ip) + &
           sEx(+2,+1,+1)*dx2(ip))*wy( 1,ip) + &
          (sEx(-2,+2,+1)*dxn(ip) + &
           sEx(-1,+2,+1)*dxm(ip) + &
           sEx( 0,+2,+1)*dx0(ip) + &
           sEx(+1,+2,+1)*dx1(ip) + &
           sEx(+2,+2,+1)*dx2(ip))*wy( 2,ip))*wz( 1,ip) + &
         ((sEx(-2,-1,+2)*dxn(ip) + &
           sEx(-1,-1,+2)*dxm(ip) + &
           sEx( 0,-1,+2)*dx0(ip) + &
           sEx(+1,-1,+2)*dx1(ip) + &
           sEx(+2,-1,+2)*dx2(ip))*wy(-1,ip) + &
          (sEx(-2, 0,+2)*dxn(ip) + &
           sEx(-1, 0,+2)*dxm(ip) + &
           sEx( 0, 0,+2)*dx0(ip) + &
           sEx(+1, 0,+2)*dx1(ip) + &
           sEx(+2, 0,+2)*dx2(ip))*wy( 0,ip) + &
          (sEx(-2,+1,+2)*dxn(ip) + &
           sEx(-1,+1,+2)*dxm(ip) + &
           sEx( 0,+1,+2)*dx0(ip) + &
           sEx(+1,+1,+2)*dx1(ip) + &
           sEx(+2,+1,+2)*dx2(ip))*wy( 1,ip) + &
          (sEx(-2,+2,+2)*dxn(ip) + &
           sEx(-1,+2,+2)*dxm(ip) + &
           sEx( 0,+2,+2)*dx0(ip) + &
           sEx(+1,+2,+2)*dx1(ip) + &
           sEx(+2,+2,+2)*dx2(ip))*wy( 2,ip))*wz( 2,ip)
    !-----------------------------------
    ! Y SHIFTED WEIGHTS
    pEy(ip) = &
        ((sEy(-1,-2,-1)*wx(-1,ip) + &
          sEy( 0,-2,-1)*wx( 0,ip) + &
          sEy(+1,-2,-1)*wx( 1,ip) + &
          sEy(+2,-2,-1)*wx( 2,ip))*dyn(ip) + &
         (sEy(-1,-1,-1)*wx(-1,ip) + &
          sEy( 0,-1,-1)*wx( 0,ip) + &
          sEy(+1,-1,-1)*wx( 1,ip) + &
          sEy(+2,-1,-1)*wx( 2,ip))*dym(ip) + &
         (sEy(-1, 0,-1)*wx(-1,ip) + &
          sEy( 0, 0,-1)*wx( 0,ip) + &
          sEy(+1, 0,-1)*wx( 1,ip) + &
          sEy(+2, 0,-1)*wx( 2,ip))*dy0(ip) + &
         (sEy(-1,+1,-1)*wx(-1,ip) + &
          sEy( 0,+1,-1)*wx( 0,ip) + &
          sEy(+1,+1,-1)*wx( 1,ip) + &
          sEy(+2,+1,-1)*wx( 2,ip))*dy1(ip) + &
         (sEy(-1,+2,-1)*wx(-1,ip) + &
          sEy( 0,+2,-1)*wx( 0,ip) + &
          sEy(+1,+2,-1)*wx( 1,ip) + &
          sEy(+2,+2,-1)*wx( 2,ip))*dy2(ip))*wz(-1,ip) + &
        ((sEy(-1,-2, 0)*wx(-1,ip) + &
          sEy( 0,-2, 0)*wx( 0,ip) + &
          sEy(+1,-2, 0)*wx( 1,ip) + &
          sEy(+2,-2, 0)*wx( 2,ip))*dyn(ip) + &
         (sEy(-1,-1, 0)*wx(-1,ip) + &
          sEy( 0,-1, 0)*wx( 0,ip) + &
          sEy(+1,-1, 0)*wx( 1,ip) + &
          sEy(+2,-1, 0)*wx( 2,ip))*dym(ip) + &
         (sEy(-1, 0, 0)*wx(-1,ip) + &
          sEy( 0, 0, 0)*wx( 0,ip) + &
          sEy(+1, 0, 0)*wx( 1,ip) + &
          sEy(+2, 0, 0)*wx( 2,ip))*dy0(ip) + &
         (sEy(-1,+1, 0)*wx(-1,ip) + &
          sEy( 0,+1, 0)*wx( 0,ip) + &
          sEy(+1,+1, 0)*wx( 1,ip) + &
          sEy(+2,+1, 0)*wx( 2,ip))*dy1(ip) + &
         (sEy(-1,+2, 0)*wx(-1,ip) + &
          sEy( 0,+2, 0)*wx( 0,ip) + &
          sEy(+1,+2, 0)*wx( 1,ip) + &
          sEy(+2,+2, 0)*wx( 2,ip))*dy2(ip))*wz( 0,ip) + &
        ((sEy(-1,-2,+1)*wx(-1,ip) + &
          sEy( 0,-2,+1)*wx( 0,ip) + &
          sEy(+1,-2,+1)*wx( 1,ip) + &
          sEy(+2,-2,+1)*wx( 2,ip))*dyn(ip) + &
         (sEy(-1,-1,+1)*wx(-1,ip) + &
          sEy( 0,-1,+1)*wx( 0,ip) + &
          sEy(+1,-1,+1)*wx( 1,ip) + &
          sEy(+2,-1,+1)*wx( 2,ip))*dym(ip) + &
         (sEy(-1, 0,+1)*wx(-1,ip) + &
          sEy( 0, 0,+1)*wx( 0,ip) + &
          sEy(+1, 0,+1)*wx( 1,ip) + &
          sEy(+2, 0,+1)*wx( 2,ip))*dy0(ip) + &
         (sEy(-1,+1,+1)*wx(-1,ip) + &
          sEy( 0,+1,+1)*wx( 0,ip) + &
          sEy(+1,+1,+1)*wx( 1,ip) + &
          sEy(+2,+1,+1)*wx( 2,ip))*dy1(ip) + &
         (sEy(-1,+2,+1)*wx(-1,ip) + &
          sEy( 0,+2,+1)*wx( 0,ip) + &
          sEy(+1,+2,+1)*wx( 1,ip) + &
          sEy(+2,+2,+1)*wx( 2,ip))*dy2(ip))*wz( 1,ip) + &
        ((sEy(-1,-2,+2)*wx(-1,ip) + &
          sEy( 0,-2,+2)*wx( 0,ip) + &
          sEy(+1,-2,+2)*wx( 1,ip) + &
          sEy(+2,-2,+2)*wx( 2,ip))*dyn(ip) + &
         (sEy(-1,-1,+2)*wx(-1,ip) + &
          sEy( 0,-1,+2)*wx( 0,ip) + &
          sEy(+1,-1,+2)*wx( 1,ip) + &
          sEy(+2,-1,+2)*wx( 2,ip))*dym(ip) + &
         (sEy(-1, 0,+2)*wx(-1,ip) + &
          sEy( 0, 0,+2)*wx( 0,ip) + &
          sEy(+1, 0,+2)*wx( 1,ip) + &
          sEy(+2, 0,+2)*wx( 2,ip))*dy0(ip) + &
         (sEy(-1,+1,+2)*wx(-1,ip) + &
          sEy( 0,+1,+2)*wx( 0,ip) + &
          sEy(+1,+1,+2)*wx( 1,ip) + &
          sEy(+2,+1,+2)*wx( 2,ip))*dy1(ip) + &
         (sEy(-1,+2,+2)*wx(-1,ip) + &
          sEy( 0,+2,+2)*wx( 0,ip) + &
          sEy(+1,+2,+2)*wx( 1,ip) + &
          sEy(+2,+2,+2)*wx( 2,ip))*dy2(ip))*wz( 2,ip)
  !-----------------------------------
  ! Z SHIFTED WEIGHTS
    pEz(ip) = &
       ((sEz(-1,-1,-2)*wx(-1,ip) + &      
         sEz( 0,-1,-2)*wx( 0,ip) + &      
         sEz(+1,-1,-2)*wx( 1,ip) + &      
         sEz(+2,-1,-2)*wx( 2,ip))*wy(-1,ip) + &
        (sEz(-1, 0,-2)*wx(-1,ip) + &      
         sEz( 0, 0,-2)*wx( 0,ip) + &      
         sEz(+1, 0,-2)*wx( 1,ip) + &      
         sEz(+2, 0,-2)*wx( 2,ip))*wy( 0,ip) + &
        (sEz(-1,+1,-2)*wx(-1,ip) + &      
         sEz( 0,+1,-2)*wx( 0,ip) + &      
         sEz(+1,+1,-2)*wx( 1,ip) + &      
         sEz(+2,+1,-2)*wx( 2,ip))*wy( 1,ip) + &
        (sEz(-1,+2,-2)*wx(-1,ip) + &
         sEz( 0,+2,-2)*wx( 0,ip) + &      
         sEz(+1,+2,-2)*wx( 1,ip) + &      
         sEz(+2,+2,-2)*wx( 2,ip))*wy( 2,ip))*dzn(ip) + &
       ((sEz(-1,-1,-1)*wx(-1,ip) + &      
         sEz( 0,-1,-1)*wx( 0,ip) + &      
         sEz(+1,-1,-1)*wx( 1,ip) + &      
         sEz(+2,-1,-1)*wx( 2,ip))*wy(-1,ip) + &
        (sEz(-1, 0,-1)*wx(-1,ip) + &      
         sEz( 0, 0,-1)*wx( 0,ip) + &      
         sEz(+1, 0,-1)*wx( 1,ip) + &      
         sEz(+2, 0,-1)*wx( 2,ip))*wy( 0,ip) + &
        (sEz(-1,+1,-1)*wx(-1,ip) + &      
         sEz( 0,+1,-1)*wx( 0,ip) + &      
         sEz(+1,+1,-1)*wx( 1,ip) + &      
         sEz(+2,+1,-1)*wx( 2,ip))*wy( 1,ip) + &
        (sEz(-1,+2,-1)*wx(-1,ip) + &
         sEz( 0,+2,-1)*wx( 0,ip) + &      
         sEz(+1,+2,-1)*wx( 1,ip) + &      
         sEz(+2,+2,-1)*wx( 2,ip))*wy( 2,ip))*dzm(ip) + &
       ((sEz(-1,-1, 0)*wx(-1,ip) + &      
         sEz( 0,-1, 0)*wx( 0,ip) + &      
         sEz(+1,-1, 0)*wx( 1,ip) + &      
         sEz(+2,-1, 0)*wx( 2,ip))*wy(-1,ip) + &
        (sEz(-1, 0, 0)*wx(-1,ip) + &      
         sEz( 0, 0, 0)*wx( 0,ip) + &      
         sEz(+1, 0, 0)*wx( 1,ip) + &      
         sEz(+2, 0, 0)*wx( 2,ip))*wy( 0,ip) + &
        (sEz(-1,+1, 0)*wx(-1,ip) + &      
         sEz( 0,+1, 0)*wx( 0,ip) + &      
         sEz(+1,+1, 0)*wx( 1,ip) + &      
         sEz(+2,+1, 0)*wx( 2,ip))*wy( 1,ip) + &
        (sEz(-1,+2, 0)*wx(-1,ip) + &      
         sEz( 0,+2, 0)*wx( 0,ip) + &      
         sEz(+1,+2, 0)*wx( 1,ip) + &      
         sEz(+2,+2, 0)*wx( 2,ip))*wy( 2,ip))*dz0(ip) + &
       ((sEz(-1,-1,+1)*wx(-1,ip) + &      
         sEz( 0,-1,+1)*wx( 0,ip) + &      
         sEz(+1,-1,+1)*wx( 1,ip) + &      
         sEz(+2,-1,+1)*wx( 2,ip))*wy(-1,ip) + &
        (sEz(-1, 0,+1)*wx(-1,ip) + &      
         sEz( 0, 0,+1)*wx( 0,ip) + &      
         sEz(+1, 0,+1)*wx( 1,ip) + &      
         sEz(+2, 0,+1)*wx( 2,ip))*wy( 0,ip) + &
        (sEz(-1,+1,+1)*wx(-1,ip) + &      
         sEz( 0,+1,+1)*wx( 0,ip) + &      
         sEz(+1,+1,+1)*wx( 1,ip) + &      
         sEz(+2,+1,+1)*wx( 2,ip))*wy( 1,ip) + &
        (sEz(-1,+2,+1)*wx(-1,ip) + &      
         sEz( 0,+2,+1)*wx( 0,ip) + &      
         sEz(+1,+2,+1)*wx( 1,ip) + &      
         sEz(+2,+2,+1)*wx( 2,ip))*wy( 2,ip))*dz1(ip) + &
       ((sEz(-1,-1,+2)*wx(-1,ip) + &      
         sEz( 0,-1,+2)*wx( 0,ip) + &      
         sEz(+1,-1,+2)*wx( 1,ip) + &      
         sEz(+2,-1,+2)*wx( 2,ip))*wy(-1,ip) + &
        (sEz(-1, 0,+2)*wx(-1,ip) + &      
         sEz( 0, 0,+2)*wx( 0,ip) + &      
         sEz(+1, 0,+2)*wx( 1,ip) + &      
         sEz(+2, 0,+2)*wx( 2,ip))*wy( 0,ip) + &
        (sEz(-1,+1,+2)*wx(-1,ip) + &      
         sEz( 0,+1,+2)*wx( 0,ip) + &      
         sEz(+1,+1,+2)*wx( 1,ip) + &      
         sEz(+2,+1,+2)*wx( 2,ip))*wy( 1,ip) + &
        (sEz(-1,+2,+2)*wx(-1,ip) + &      
         sEz( 0,+2,+2)*wx( 0,ip) + &      
         sEz(+1,+2,+2)*wx( 1,ip) + &      
         sEz(+2,+2,+2)*wx( 2,ip))*wy( 2,ip))*dz2(ip)
  !-----------------------------------
  ! YZ SHIFTED WEIGHTS
    pBx(ip) = &
       ((sBx(-1,-2,-2)*wx(-1,ip) + &      
         sBx( 0,-2,-2)*wx( 0,ip) + &      
         sBx(+1,-2,-2)*wx( 1,ip) + &      
         sBx(+2,-2,-2)*wx( 2,ip))*dyn(ip) + &
        (sBx(-1,-1,-2)*wx(-1,ip) + &      
         sBx( 0,-1,-2)*wx( 0,ip) + &      
         sBx(+1,-1,-2)*wx( 1,ip) + &      
         sBx(+2,-1,-2)*wx( 2,ip))*dym(ip) + &
        (sBx(-1, 0,-2)*wx(-1,ip) + &      
         sBx( 0, 0,-2)*wx( 0,ip) + &      
         sBx(+1, 0,-2)*wx( 1,ip) + &      
         sBx(+2, 0,-2)*wx( 2,ip))*dy0(ip) + &
        (sBx(-1,+1,-2)*wx(-1,ip) + &      
         sBx( 0,+1,-2)*wx( 0,ip) + &      
         sBx(+1,+1,-2)*wx( 1,ip) + &      
         sBx(+2,+1,-2)*wx( 2,ip))*dy1(ip) + &
        (sBx(-1,+2,-2)*wx(-1,ip) + &      
         sBx( 0,+2,-2)*wx( 0,ip) + &      
         sBx(+1,+2,-2)*wx( 1,ip) + &      
         sBx(+2,+2,-2)*wx( 2,ip))*dy2(ip))*dzn(ip) + &
       ((sBx(-1,-2,-1)*wx(-1,ip) + &      
         sBx( 0,-2,-1)*wx( 0,ip) + &      
         sBx(+1,-2,-1)*wx( 1,ip) + &      
         sBx(+2,-2,-1)*wx( 2,ip))*dyn(ip) + &
        (sBx(-1,-1,-1)*wx(-1,ip) + &      
         sBx( 0,-1,-1)*wx( 0,ip) + &      
         sBx(+1,-1,-1)*wx( 1,ip) + &      
         sBx(+2,-1,-1)*wx( 2,ip))*dym(ip) + &
        (sBx(-1, 0,-1)*wx(-1,ip) + &      
         sBx( 0, 0,-1)*wx( 0,ip) + &      
         sBx(+1, 0,-1)*wx( 1,ip) + &      
         sBx(+2, 0,-1)*wx( 2,ip))*dy0(ip) + &
        (sBx(-1,+1,-1)*wx(-1,ip) + &      
         sBx( 0,+1,-1)*wx( 0,ip) + &      
         sBx(+1,+1,-1)*wx( 1,ip) + &      
         sBx(+2,+1,-1)*wx( 2,ip))*dy1(ip) + &
        (sBx(-1,+2,-1)*wx(-1,ip) + &      
         sBx( 0,+2,-1)*wx( 0,ip) + &      
         sBx(+1,+2,-1)*wx( 1,ip) + &      
         sBx(+2,+2,-1)*wx( 2,ip))*dy2(ip))*dzm(ip) + &
       ((sBx(-1,-2, 0)*wx(-1,ip) + &      
         sBx( 0,-2, 0)*wx( 0,ip) + &      
         sBx(+1,-2, 0)*wx( 1,ip) + &      
         sBx(+2,-2, 0)*wx( 2,ip))*dyn(ip) + &
        (sBx(-1,-1, 0)*wx(-1,ip) + &      
         sBx( 0,-1, 0)*wx( 0,ip) + &      
         sBx(+1,-1, 0)*wx( 1,ip) + &      
         sBx(+2,-1, 0)*wx( 2,ip))*dym(ip) + &
        (sBx(-1, 0, 0)*wx(-1,ip) + &      
         sBx( 0, 0, 0)*wx( 0,ip) + &      
         sBx(+1, 0, 0)*wx( 1,ip) + &      
         sBx(+2, 0, 0)*wx( 2,ip))*dy0(ip) + &
        (sBx(-1,+1, 0)*wx(-1,ip) + &      
         sBx( 0,+1, 0)*wx( 0,ip) + &      
         sBx(+1,+1, 0)*wx( 1,ip) + &      
         sBx(+2,+1, 0)*wx( 2,ip))*dy1(ip) + &
        (sBx(-1,+2, 0)*wx(-1,ip) + &      
         sBx( 0,+2, 0)*wx( 0,ip) + &      
         sBx(+1,+2, 0)*wx( 1,ip) + &      
         sBx(+2,+2, 0)*wx( 2,ip))*dy2(ip))*dz0(ip) + &
       ((sBx(-1,-2,+1)*wx(-1,ip) + &      
         sBx( 0,-2,+1)*wx( 0,ip) + &      
         sBx(+1,-2,+1)*wx( 1,ip) + &      
         sBx(+2,-2,+1)*wx( 2,ip))*dyn(ip) + &
        (sBx(-1,-1,+1)*wx(-1,ip) + &      
         sBx( 0,-1,+1)*wx( 0,ip) + &      
         sBx(+1,-1,+1)*wx( 1,ip) + &      
         sBx(+2,-1,+1)*wx( 2,ip))*dym(ip) + &
        (sBx(-1, 0,+1)*wx(-1,ip) + &      
         sBx( 0, 0,+1)*wx( 0,ip) + &      
         sBx(+1, 0,+1)*wx( 1,ip) + &      
         sBx(+2, 0,+1)*wx( 2,ip))*dy0(ip) + &
        (sBx(-1,+1,+1)*wx(-1,ip) + &      
         sBx( 0,+1,+1)*wx( 0,ip) + &      
         sBx(+1,+1,+1)*wx( 1,ip) + &      
         sBx(+2,+1,+1)*wx( 2,ip))*dy1(ip) + &
        (sBx(-1,+2,+1)*wx(-1,ip) + &      
         sBx( 0,+2,+1)*wx( 0,ip) + &      
         sBx(+1,+2,+1)*wx( 1,ip) + &      
         sBx(+2,+2,+1)*wx( 2,ip))*dy2(ip))*dz1(ip) + &
       ((sBx(-1,-2,+2)*wx(-1,ip) + &      
         sBx( 0,-2,+2)*wx( 0,ip) + &      
         sBx(+1,-2,+2)*wx( 1,ip) + &      
         sBx(+2,-2,+2)*wx( 2,ip))*dyn(ip) + &
        (sBx(-1,-1,+2)*wx(-1,ip) + &      
         sBx( 0,-1,+2)*wx( 0,ip) + &      
         sBx(+1,-1,+2)*wx( 1,ip) + &      
         sBx(+2,-1,+2)*wx( 2,ip))*dym(ip) + &
        (sBx(-1, 0,+2)*wx(-1,ip) + &      
         sBx( 0, 0,+2)*wx( 0,ip) + &      
         sBx(+1, 0,+2)*wx( 1,ip) + &      
         sBx(+2, 0,+2)*wx( 2,ip))*dy0(ip) + &
        (sBx(-1,+1,+2)*wx(-1,ip) + &      
         sBx( 0,+1,+2)*wx( 0,ip) + &      
         sBx(+1,+1,+2)*wx( 1,ip) + &      
         sBx(+2,+1,+2)*wx( 2,ip))*dy1(ip) + &
        (sBx(-1,+2,+2)*wx(-1,ip) + &      
         sBx( 0,+2,+2)*wx( 0,ip) + &      
         sBx(+1,+2,+2)*wx( 1,ip) + &      
         sBx(+2,+2,+2)*wx( 2,ip))*dy2(ip))*dz2(ip)
  !-----------------------------------
  ! ZX SHIFTED WEIGHTS
    pBy(ip) = &
       ((sBy(-2,-1,-2)*dxn(ip) + &
         sBy(-1,-1,-2)*dxm(ip) + &
         sBy( 0,-1,-2)*dx0(ip) + &
         sBy(+1,-1,-2)*dx1(ip) + &
         sBy(+2,-1,-2)*dx2(ip))*wy(-1,ip) + &
        (sBy(-2, 0,-2)*dxn(ip) + &
         sBy(-1, 0,-2)*dxm(ip) + &
         sBy( 0, 0,-2)*dx0(ip) + &
         sBy(+1, 0,-2)*dx1(ip) + &
         sBy(+2, 0,-2)*dx2(ip))*wy( 0,ip) + &
        (sBy(-2,+1,-2)*dxn(ip) + &
         sBy(-1,+1,-2)*dxm(ip) + &
         sBy( 0,+1,-2)*dx0(ip) + &
         sBy(+1,+1,-2)*dx1(ip) + &
         sBy(+2,+1,-2)*dx2(ip))*wy( 1,ip) + &
        (sBy(-2,+2,-2)*dxn(ip) + &
         sBy(-1,+2,-2)*dxm(ip) + &
         sBy( 0,+2,-2)*dx0(ip) + &
         sBy(+1,+2,-2)*dx1(ip) + &
         sBy(+2,+2,-2)*dx2(ip))*wy( 2,ip))*dzn(ip) + &
       ((sBy(-2,-1,-1)*dxn(ip) + &
         sBy(-1,-1,-1)*dxm(ip) + &
         sBy( 0,-1,-1)*dx0(ip) + &
         sBy(+1,-1,-1)*dx1(ip) + &
         sBy(+2,-1,-1)*dx2(ip))*wy(-1,ip) + &
        (sBy(-2, 0,-1)*dxn(ip) + &
         sBy(-1, 0,-1)*dxm(ip) + &
         sBy( 0, 0,-1)*dx0(ip) + &
         sBy(+1, 0,-1)*dx1(ip) + &
         sBy(+2, 0,-1)*dx2(ip))*wy( 0,ip) + &
        (sBy(-2,+1,-1)*dxn(ip) + &
         sBy(-1,+1,-1)*dxm(ip) + &
         sBy( 0,+1,-1)*dx0(ip) + &
         sBy(+1,+1,-1)*dx1(ip) + &
         sBy(+2,+1,-1)*dx2(ip))*wy( 1,ip) + &
        (sBy(-2,+2,-1)*dxn(ip) + &
         sBy(-1,+2,-1)*dxm(ip) + &
         sBy( 0,+2,-1)*dx0(ip) + &
         sBy(+1,+2,-1)*dx1(ip) + &
         sBy(+2,+2,-1)*dx2(ip))*wy( 2,ip))*dzm(ip) + &
       ((sBy(-2,-1, 0)*dxn(ip) + &
         sBy(-1,-1, 0)*dxm(ip) + &
         sBy( 0,-1, 0)*dx0(ip) + &
         sBy(+1,-1, 0)*dx1(ip) + &
         sBy(+2,-1, 0)*dx2(ip))*wy(-1,ip) + &
        (sBy(-2, 0, 0)*dxn(ip) + &
         sBy(-1, 0, 0)*dxm(ip) + &
         sBy( 0, 0, 0)*dx0(ip) + &
         sBy(+1, 0, 0)*dx1(ip) + &
         sBy(+2, 0, 0)*dx2(ip))*wy( 0,ip) + &
        (sBy(-2,+1, 0)*dxn(ip) + &
         sBy(-1,+1, 0)*dxm(ip) + &
         sBy( 0,+1, 0)*dx0(ip) + &
         sBy(+1,+1, 0)*dx1(ip) + &
         sBy(+2,+1, 0)*dx2(ip))*wy( 1,ip) + &
        (sBy(-2,+2, 0)*dxn(ip) + &
         sBy(-1,+2, 0)*dxm(ip) + &
         sBy( 0,+2, 0)*dx0(ip) + &
         sBy(+1,+2, 0)*dx1(ip) + &
         sBy(+2,+2, 0)*dx2(ip))*wy( 2,ip))*dz0(ip) + &
       ((sBy(-2,-1,+1)*dxn(ip) + &
         sBy(-1,-1,+1)*dxm(ip) + &
         sBy( 0,-1,+1)*dx0(ip) + &
         sBy(+1,-1,+1)*dx1(ip) + &
         sBy(+2,-1,+1)*dx2(ip))*wy(-1,ip) + &
        (sBy(-2, 0,+1)*dxn(ip) + &
         sBy(-1, 0,+1)*dxm(ip) + &
         sBy( 0, 0,+1)*dx0(ip) + &
         sBy(+1, 0,+1)*dx1(ip) + &
         sBy(+2, 0,+1)*dx2(ip))*wy( 0,ip) + &
        (sBy(-2,+1,+1)*dxn(ip) + &
         sBy(-1,+1,+1)*dxm(ip) + &
         sBy( 0,+1,+1)*dx0(ip) + &
         sBy(+1,+1,+1)*dx1(ip) + &
         sBy(+2,+1,+1)*dx2(ip))*wy( 1,ip) + &
        (sBy(-2,+2,+1)*dxn(ip) + &
         sBy(-1,+2,+1)*dxm(ip) + &
         sBy( 0,+2,+1)*dx0(ip) + &
         sBy(+1,+2,+1)*dx1(ip) + &
         sBy(+2,+2,+1)*dx2(ip))*wy( 2,ip))*dz1(ip) + &
       ((sBy(-2,-1,+2)*dxn(ip) + &
         sBy(-1,-1,+2)*dxm(ip) + &
         sBy( 0,-1,+2)*dx0(ip) + &
         sBy(+1,-1,+2)*dx1(ip) + &
         sBy(+2,-1,+2)*dx2(ip))*wy(-1,ip) + &
        (sBy(-2, 0,+2)*dxn(ip) + &
         sBy(-1, 0,+2)*dxm(ip) + &
         sBy( 0, 0,+2)*dx0(ip) + &
         sBy(+1, 0,+2)*dx1(ip) + &
         sBy(+2, 0,+2)*dx2(ip))*wy( 0,ip) + &
        (sBy(-2,+1,+2)*dxn(ip) + &
         sBy(-1,+1,+2)*dxm(ip) + &
         sBy( 0,+1,+2)*dx0(ip) + &
         sBy(+1,+1,+2)*dx1(ip) + &
         sBy(+2,+1,+2)*dx2(ip))*wy( 1,ip) + &
        (sBy(-2,+2,+2)*dxn(ip) + &
         sBy(-1,+2,+2)*dxm(ip) + &
         sBy( 0,+2,+2)*dx0(ip) + &
         sBy(+1,+2,+2)*dx1(ip) + &
         sBy(+2,+2,+2)*dx2(ip))*wy( 2,ip))*dz2(ip)
  !-----------------------------------
  ! XY SHIFTED WEIGHTS
    pBz(ip) = &
       ((sBz(-2,-2,-1)*dxn(ip) + &
         sBz(-1,-2,-1)*dxm(ip) + &
         sBz( 0,-2,-1)*dx0(ip) + &
         sBz(+1,-2,-1)*dx1(ip) + &
         sBz(+2,-2,-1)*dx2(ip))*dyn(ip) + &
        (sBz(-2,-1,-1)*dxn(ip) + &
         sBz(-1,-1,-1)*dxm(ip) + &
         sBz( 0,-1,-1)*dx0(ip) + &
         sBz(+1,-1,-1)*dx1(ip) + &
         sBz(+2,-1,-1)*dx2(ip))*dym(ip) + &
        (sBz(-2, 0,-1)*dxn(ip) + &
         sBz(-1, 0,-1)*dxm(ip) + &
         sBz( 0, 0,-1)*dx0(ip) + &
         sBz(+1, 0,-1)*dx1(ip) + &
         sBz(+2, 0,-1)*dx2(ip))*dy0(ip) + &
        (sBz(-2,+1,-1)*dxn(ip) + &
         sBz(-1,+1,-1)*dxm(ip) + &
         sBz( 0,+1,-1)*dx0(ip) + &
         sBz(+1,+1,-1)*dx1(ip) + &
         sBz(+2,+1,-1)*dx2(ip))*dy1(ip) + &
        (sBz(-2,+2,-1)*dxn(ip) + &
         sBz(-1,+2,-1)*dxm(ip) + &
         sBz( 0,+2,-1)*dx0(ip) + &
         sBz(+1,+2,-1)*dx1(ip) + &
         sBz(+2,+2,-1)*dx2(ip))*dy2(ip))*wz(-1,ip) + &
       ((sBz(-2,-2, 0)*dxn(ip) + &
         sBz(-1,-2, 0)*dxm(ip) + &
         sBz( 0,-2, 0)*dx0(ip) + &
         sBz(+1,-2, 0)*dx1(ip) + &
         sBz(+2,-2, 0)*dx2(ip))*dyn(ip) + &
        (sBz(-2,-1, 0)*dxn(ip) + &
         sBz(-1,-1, 0)*dxm(ip) + &
         sBz( 0,-1, 0)*dx0(ip) + &
         sBz(+1,-1, 0)*dx1(ip) + &
         sBz(+2,-1, 0)*dx2(ip))*dym(ip) + &
        (sBz(-2, 0, 0)*dxn(ip) + &
         sBz(-1, 0, 0)*dxm(ip) + &
         sBz( 0, 0, 0)*dx0(ip) + &
         sBz(+1, 0, 0)*dx1(ip) + &
         sBz(+2, 0, 0)*dx2(ip))*dy0(ip) + &
        (sBz(-2,+1, 0)*dxn(ip) + &
         sBz(-1,+1, 0)*dxm(ip) + &
         sBz( 0,+1, 0)*dx0(ip) + &
         sBz(+1,+1, 0)*dx1(ip) + &
         sBz(+2,+1, 0)*dx2(ip))*dy1(ip) + &
        (sBz(-2,+2, 0)*dxn(ip) + &
         sBz(-1,+2, 0)*dxm(ip) + &
         sBz( 0,+2, 0)*dx0(ip) + &
         sBz(+1,+2, 0)*dx1(ip) + &
         sBz(+2,+2, 0)*dx2(ip))*dy2(ip))*wz( 0,ip) + &
       ((sBz(-2,-2,+1)*dxn(ip) + &
         sBz(-1,-2,+1)*dxm(ip) + &
         sBz( 0,-2,+1)*dx0(ip) + &
         sBz(+1,-2,+1)*dx1(ip) + &
         sBz(+2,-2,+1)*dx2(ip))*dyn(ip) + &
        (sBz(-2,-1,+1)*dxn(ip) + &
         sBz(-1,-1,+1)*dxm(ip) + &
         sBz( 0,-1,+1)*dx0(ip) + &
         sBz(+1,-1,+1)*dx1(ip) + &
         sBz(+2,-1,+1)*dx2(ip))*dym(ip) + &
        (sBz(-2, 0,+1)*dxn(ip) + &
         sBz(-1, 0,+1)*dxm(ip) + &
         sBz( 0, 0,+1)*dx0(ip) + &
         sBz(+1, 0,+1)*dx1(ip) + &
         sBz(+2, 0,+1)*dx2(ip))*dy0(ip) + &
        (sBz(-2,+1,+1)*dxn(ip) + &
         sBz(-1,+1,+1)*dxm(ip) + &
         sBz( 0,+1,+1)*dx0(ip) + &
         sBz(+1,+1,+1)*dx1(ip) + &
         sBz(+2,+1,+1)*dx2(ip))*dy1(ip) + &
        (sBz(-2,+2,+1)*dxn(ip) + &
         sBz(-1,+2,+1)*dxm(ip) + &
         sBz( 0,+2,+1)*dx0(ip) + &
         sBz(+1,+2,+1)*dx1(ip) + &
         sBz(+2,+2,+1)*dx2(ip))*dy2(ip))*wz( 1,ip) + &
       ((sBz(-2,-2,+2)*dxn(ip) + &
         sBz(-1,-2,+2)*dxm(ip) + &
         sBz( 0,-2,+2)*dx0(ip) + &
         sBz(+1,-2,+2)*dx1(ip) + &
         sBz(+2,-2,+2)*dx2(ip))*dyn(ip) + &
        (sBz(-2,-1,+2)*dxn(ip) + &
         sBz(-1,-1,+2)*dxm(ip) + &
         sBz( 0,-1,+2)*dx0(ip) + &
         sBz(+1,-1,+2)*dx1(ip) + &
         sBz(+2,-1,+2)*dx2(ip))*dym(ip) + &
        (sBz(-2, 0,+2)*dxn(ip) + &
         sBz(-1, 0,+2)*dxm(ip) + &
         sBz( 0, 0,+2)*dx0(ip) + &
         sBz(+1, 0,+2)*dx1(ip) + &
         sBz(+2, 0,+2)*dx2(ip))*dy0(ip) + &
        (sBz(-2,+1,+2)*dxn(ip) + &
         sBz(-1,+1,+2)*dxm(ip) + &
         sBz( 0,+1,+2)*dx0(ip) + &
         sBz(+1,+1,+2)*dx1(ip) + &
         sBz(+2,+1,+2)*dx2(ip))*dy1(ip) + &
        (sBz(-2,+2,+2)*dxn(ip) + &
         sBz(-1,+2,+2)*dxm(ip) + &
         sBz( 0,+2,+2)*dx0(ip) + &
         sBz(+1,+2,+2)*dx1(ip) + &
         sBz(+2,+2,+2)*dx2(ip))*dy2(ip))*wz( 2,ip)
  enddo
END SUBROUTINE interpolate_EB
!-------------------------------------------------------------------------------
SUBROUTINE makeweights (r,err)
  USE params, only : mdim, stdout, stdall, master, mpi, do_check_interpolate
  USE grid_m,   only : g
  USE interpolation
  implicit none
  logical          :: err
  real, intent(in) :: r(mdim)
  real             :: rr(mdim)
  real             :: s, sm1, sm2
  real, parameter  :: one_sixth=1./6., two_third=2./3.
  logical          :: up
  integer          :: lb, ub
!===============================================================================
! Construct cubic weights for DIRECT interpolation of fields to particle
!===============================================================================

! ------------------------------------------------------------------------------
! Project index into allowed range for interpolation.  For
! reasons of continuity in synthetic spectra we do NOT want
! to wrap the actual coordinates of particles!

! rr is a float grid index wrapped properly. ie. rr = "wrap"[(r-g%grlb)/g%ds]
  call fold1 (r, rr)
  
! ------------------------------------------------------------------------------
! Find signed distance to centred and staggered points in all directions.
  
  !-------------
  ! X COORDINATE CENTERED
  ixc = floor(rr(1)); s = rr(1) - ixc; sm1 = 1. - s; sm2 = 2. - s 
  wxm = one_sixth * sm1*sm1*sm1
  wx0 = two_third - 0.5*s*s*sm2
  wx1 = two_third - 0.5*sm1*sm1*(1.+ s) 
  wx2 = 1. - (wxm + wx0 + wx1)
  ixc = merge(max(ixc,2),ixc,mpi%lb(1))                                         ! last point where part can contribute
  ixc = merge(min(ixc,g%n(1)-2),ixc,mpi%ub(1))                                  ! last point where part can contribute

  !-------------
  ! Y COORDINATE CENTERED
  iyc= floor(rr(2)); s = rr(2) - iyc; sm1 = 1. - s; sm2 = 2. - s 
  wym = one_sixth * sm1*sm1*sm1
  wy0 = two_third - 0.5*s*s*sm2
  wy1 = two_third - 0.5*sm1*sm1*(1.+ s) 
  wy2 = 1. - (wym + wy0 + wy1)
  iyc = merge(max(iyc,2),iyc,mpi%lb(2))                                         ! last point where part can contribute
  iyc = merge(min(iyc,g%n(2)-2),iyc,mpi%ub(2))

  !-------------
  ! Z COORDINATE CENTERED
  izc = floor(rr(3)); s = rr(3) - izc; sm1 = 1. - s; sm2 = 2. - s 
  wzm = one_sixth * sm1*sm1*sm1
  wz0 = two_third - 0.5*s*s*sm2
  wz1 = two_third - 0.5*sm1*sm1*(1.+ s) 
  wz2 = 1. - (wzm + wz0 + wz1)
  izc = merge(max(izc,2),izc,mpi%lb(3))                                         ! last point where part can contribute
  izc = merge(min(izc,g%n(3)-2),izc,mpi%ub(3))

  !-------------
  ! X COORDINATE SHIFTED IN X
  ixs = floor(rr(1) - 0.5); s = rr(1) - 0.5 - ixs; sm1 = 1. - s; sm2 = 2. - s
  dxm = one_sixth * sm1*sm1*sm1
  dx0 = two_third - 0.5*s*s*sm2
  dx1 = two_third - 0.5*sm1*sm1*(1.+ s) 
  dx2 = 1. - (dxm + dx0 + dx1)
  ixs = merge(max(ixs,2),ixs,mpi%lb(1))                                         ! last point where part can contribute
  ixs = merge(min(ixs,g%n(1)-2),ixs,mpi%ub(1))

  !-------------
  ! Y COORDINATE SHIFTED IN Y
  iys = floor(rr(2) - 0.5); s = rr(2) - 0.5 - iys; sm1 = 1. - s; sm2 = 2. - s
  dym = one_sixth * sm1*sm1*sm1
  dy0 = two_third - 0.5*s*s*sm2
  dy1 = two_third - 0.5*sm1*sm1*(1.+ s) 
  dy2 = 1. - (dym + dy0 + dy1)
  iys = merge(max(iys,2),iys,mpi%lb(2))                                         ! last point where part can contribute
  iys = merge(min(iys,g%n(2)-2),iys,mpi%ub(2))

  !-------------
  ! Z COORDINATE SHIFTED IN Z
  izs = floor(rr(3) - 0.5); s = rr(3) - 0.5 - izs; sm1 = 1. - s; sm2 = 2. - s
  dzm = one_sixth * sm1*sm1*sm1
  dz0 = two_third - 0.5*s*s*sm2
  dz1 = two_third - 0.5*sm1*sm1*(1.+ s) 
  dz2 = 1. - (dzm + dz0 + dz1)
  izs = merge(max(izs,2),izs,mpi%lb(3))                                         ! last point where part can contribute
  izs = merge(min(izs,g%n(3)-2),izs,mpi%ub(3))

  err = .false.
  if (do_check_interpolate) then
    if (nprint > 0 .and. master) then
      nprint = nprint - 1
      print '(1x,a,1x,i3,2(3f8.3,3x),6i5)', &
        'interp: mpi%me(3),r,ic,is',mpi%me(3),r,rr,ixc,iyc,izc,ixs,iys,izs
      print '(1x,a,1x,6(4f8.5,3x))', 'interp: weights', &
        wxm,wx0,wx1,wx2, &
        wym,wy0,wy1,wy2, &
        wzm,wz0,wz1,wz2, &
        dxm,dx0,dx1,dx2, &
        dym,dy0,dy1,dy2, &
        dzm,dz0,dz1,dz2
    end if
    if ( any((/ixc,iyc,izc,ixs,iys,izs/) < 2) .or. &
         any((/ixc,iyc,izc/) > g%n-2)         .or. &
         any((/ixs,iys,izs/) > g%n-2) ) then
      err = .true.

      if (stdout>0) then
        write(stdout,*) ' '
        write(stdout,*) 'mpi%me(3)        : ',mpi%me(3)
        write(stdout,*) 'g%z(g%lb(3)      : ',g%z(g%lb(3))
        write(stdout,*) '(1.50001*g%ds(3)): ',(1.50001*g%ds(3))
        write(stdout,*) ' '
      endif

      if (stdall>0) then
       write(stdall,*) 'makeweights: out of bounds result, thread', mpi%me(3)
       write(stdall,*) 'lb             =', g%lb
       write(stdall,*) 'ixc, iyc, izc  =', ixc,iyc,izc
       write(stdall,*) 'ub             =', g%ub
       write(stdall,*) 'ixs, iys, izs  =', ixs, iys, izs
       write(stdall,*) 'g%n            =', g%n
       write(stdall,'(a,3f16.10)') 'g%grlb         =', g%grlb
       write(stdall,'(a,3f16.10)') 'g%rlb          =', g%rlb
       write(stdall,'(a,3f16.10)') 'r              =', r
       write(stdall,'(a,3f16.10)') 'rr             =', rr
       write(stdall,'(a,3f16.10)') 'g%grub         =', g%grub
       write(stdall,'(a,3f16.10)') 'g%rub          =', g%rub
       write(stdall,'(a,3f16.10)') 'g%r(ub)-pa%r   =', g%rub-r
       write(stdall,*) 's  =', g%s
      endif
    endif
    call warning_any(err,'interpolation','index out of bounds')
  endif

  ! make sure that we dont go out-of-bounds. This can happen in approx 1 out of 10^7 cases
  lb = 2; ub = g%n(1)-2
  ixc = min(max(ixc,lb),ub)
  ixs = min(max(ixs,lb),ub)
  ub = g%n(2)-2
  iyc = min(max(iyc,lb),ub)
  iys = min(max(iys,lb),ub)
  ub = g%n(3)-2
  izc = min(max(izc,lb),ub)
  izs = min(max(izs,lb),ub)

END SUBROUTINE
!-------------------------------------------------------------------------------
SUBROUTINE scatter_fields (fx,fy,fz,fp1,fyz,fzx,fxy,fp2)
  USE params, only : mcoord
  USE grid_m,   only : nx,ny,nz
  USE interpolation
  implicit none
  real, dimension(nx,ny,nz), intent(in)  :: fx,fy,fz,fxy,fyz,fzx                ! input fields
  real, dimension(mcoord)                :: fp1,fp2                             ! output vectors

  !-----------------------------------
  ! X SHIFTED WEIGHTS
  fp1(1) = &
       ((fx(ixs-1,iyc-1,izc-1)*dxm + &
         fx(ixs  ,iyc-1,izc-1)*dx0 + &
         fx(ixs+1,iyc-1,izc-1)*dx1 + &
         fx(ixs+2,iyc-1,izc-1)*dx2)*wym + &
        (fx(ixs-1,iyc  ,izc-1)*dxm + &
         fx(ixs  ,iyc  ,izc-1)*dx0 + &
         fx(ixs+1,iyc  ,izc-1)*dx1 + &
         fx(ixs+2,iyc  ,izc-1)*dx2)*wy0 + &
        (fx(ixs-1,iyc+1,izc-1)*dxm + &
         fx(ixs  ,iyc+1,izc-1)*dx0 + &
         fx(ixs+1,iyc+1,izc-1)*dx1 + &
         fx(ixs+2,iyc+1,izc-1)*dx2)*wy1 + &
        (fx(ixs-1,iyc+2,izc-1)*dxm + &
         fx(ixs  ,iyc+2,izc-1)*dx0 + &
         fx(ixs+1,iyc+2,izc-1)*dx1 + &
         fx(ixs+2,iyc+2,izc-1)*dx2)*wy2)*wzm + &
       ((fx(ixs-1,iyc-1,izc  )*dxm + &
         fx(ixs  ,iyc-1,izc  )*dx0 + &
         fx(ixs+1,iyc-1,izc  )*dx1 + &
         fx(ixs+2,iyc-1,izc  )*dx2)*wym + &
        (fx(ixs-1,iyc  ,izc  )*dxm + &
         fx(ixs  ,iyc  ,izc  )*dx0 + &
         fx(ixs+1,iyc  ,izc  )*dx1 + &
         fx(ixs+2,iyc  ,izc  )*dx2)*wy0 + &
        (fx(ixs-1,iyc+1,izc  )*dxm + &
         fx(ixs  ,iyc+1,izc  )*dx0 + &
         fx(ixs+1,iyc+1,izc  )*dx1 + &
         fx(ixs+2,iyc+1,izc  )*dx2)*wy1 + &
        (fx(ixs-1,iyc+2,izc  )*dxm + &
         fx(ixs  ,iyc+2,izc  )*dx0 + &
         fx(ixs+1,iyc+2,izc  )*dx1 + &
         fx(ixs+2,iyc+2,izc  )*dx2)*wy2)*wz0 + &
       ((fx(ixs-1,iyc-1,izc+1)*dxm + &
         fx(ixs  ,iyc-1,izc+1)*dx0 + &
         fx(ixs+1,iyc-1,izc+1)*dx1 + &
         fx(ixs+2,iyc-1,izc+1)*dx2)*wym + &
        (fx(ixs-1,iyc  ,izc+1)*dxm + &
         fx(ixs  ,iyc  ,izc+1)*dx0 + &
         fx(ixs+1,iyc  ,izc+1)*dx1 + &
         fx(ixs+2,iyc  ,izc+1)*dx2)*wy0 + &
        (fx(ixs-1,iyc+1,izc+1)*dxm + &
         fx(ixs  ,iyc+1,izc+1)*dx0 + &
         fx(ixs+1,iyc+1,izc+1)*dx1 + &
         fx(ixs+2,iyc+1,izc+1)*dx2)*wy1 + &
        (fx(ixs-1,iyc+2,izc+1)*dxm + &
         fx(ixs  ,iyc+2,izc+1)*dx0 + &
         fx(ixs+1,iyc+2,izc+1)*dx1 + &
         fx(ixs+2,iyc+2,izc+1)*dx2)*wy2)*wz1 + &
       ((fx(ixs-1,iyc-1,izc+2)*dxm + &
         fx(ixs  ,iyc-1,izc+2)*dx0 + &
         fx(ixs+1,iyc-1,izc+2)*dx1 + &
         fx(ixs+2,iyc-1,izc+2)*dx2)*wym + &
        (fx(ixs-1,iyc  ,izc+2)*dxm + &
         fx(ixs  ,iyc  ,izc+2)*dx0 + &
         fx(ixs+1,iyc  ,izc+2)*dx1 + &
         fx(ixs+2,iyc  ,izc+2)*dx2)*wy0 + &
        (fx(ixs-1,iyc+1,izc+2)*dxm + &
         fx(ixs  ,iyc+1,izc+2)*dx0 + &
         fx(ixs+1,iyc+1,izc+2)*dx1 + &
         fx(ixs+2,iyc+1,izc+2)*dx2)*wy1 + &
        (fx(ixs-1,iyc+2,izc+2)*dxm + &
         fx(ixs  ,iyc+2,izc+2)*dx0 + &
         fx(ixs+1,iyc+2,izc+2)*dx1 + &
         fx(ixs+2,iyc+2,izc+2)*dx2)*wy2)*wz2

  !-----------------------------------
  ! Y SHIFTED WEIGHTS
  fp1(2) = &
       ((fy(ixc-1,iys-1,izc-1)*wxm + &
         fy(ixc  ,iys-1,izc-1)*wx0 + &
         fy(ixc+1,iys-1,izc-1)*wx1 + &
         fy(ixc+2,iys-1,izc-1)*wx2)*dym + &
        (fy(ixc-1,iys  ,izc-1)*wxm + &
         fy(ixc  ,iys  ,izc-1)*wx0 + &
         fy(ixc+1,iys  ,izc-1)*wx1 + &
         fy(ixc+2,iys  ,izc-1)*wx2)*dy0 + &
        (fy(ixc-1,iys+1,izc-1)*wxm + &
         fy(ixc  ,iys+1,izc-1)*wx0 + &
         fy(ixc+1,iys+1,izc-1)*wx1 + &
         fy(ixc+2,iys+1,izc-1)*wx2)*dy1 + &
        (fy(ixc-1,iys+2,izc-1)*wxm + &
         fy(ixc  ,iys+2,izc-1)*wx0 + &
         fy(ixc+1,iys+2,izc-1)*wx1 + &
         fy(ixc+2,iys+2,izc-1)*wx2)*dy2)*wzm + &
       ((fy(ixc-1,iys-1,izc  )*wxm + &
         fy(ixc  ,iys-1,izc  )*wx0 + &
         fy(ixc+1,iys-1,izc  )*wx1 + &
         fy(ixc+2,iys-1,izc  )*wx2)*dym + &
        (fy(ixc-1,iys  ,izc  )*wxm + &
         fy(ixc  ,iys  ,izc  )*wx0 + &
         fy(ixc+1,iys  ,izc  )*wx1 + &
         fy(ixc+2,iys  ,izc  )*wx2)*dy0 + &
        (fy(ixc-1,iys+1,izc  )*wxm + &
         fy(ixc  ,iys+1,izc  )*wx0 + &
         fy(ixc+1,iys+1,izc  )*wx1 + &
         fy(ixc+2,iys+1,izc  )*wx2)*dy1 + &
        (fy(ixc-1,iys+2,izc  )*wxm + &
         fy(ixc  ,iys+2,izc  )*wx0 + &
         fy(ixc+1,iys+2,izc  )*wx1 + &
         fy(ixc+2,iys+2,izc  )*wx2)*dy2)*wz0 + &
       ((fy(ixc-1,iys-1,izc+1)*wxm + &
         fy(ixc  ,iys-1,izc+1)*wx0 + &
         fy(ixc+1,iys-1,izc+1)*wx1 + &
         fy(ixc+2,iys-1,izc+1)*wx2)*dym + &
        (fy(ixc-1,iys  ,izc+1)*wxm + &
         fy(ixc  ,iys  ,izc+1)*wx0 + &
         fy(ixc+1,iys  ,izc+1)*wx1 + &
         fy(ixc+2,iys  ,izc+1)*wx2)*dy0 + &
        (fy(ixc-1,iys+1,izc+1)*wxm + &
         fy(ixc  ,iys+1,izc+1)*wx0 + &
         fy(ixc+1,iys+1,izc+1)*wx1 + &
         fy(ixc+2,iys+1,izc+1)*wx2)*dy1 + &
        (fy(ixc-1,iys+2,izc+1)*wxm + &
         fy(ixc  ,iys+2,izc+1)*wx0 + &
         fy(ixc+1,iys+2,izc+1)*wx1 + &
         fy(ixc+2,iys+2,izc+1)*wx2)*dy2)*wz1 + &
       ((fy(ixc-1,iys-1,izc+2)*wxm + &
         fy(ixc  ,iys-1,izc+2)*wx0 + &
         fy(ixc+1,iys-1,izc+2)*wx1 + &
         fy(ixc+2,iys-1,izc+2)*wx2)*dym + &
        (fy(ixc-1,iys  ,izc+2)*wxm + &
         fy(ixc  ,iys  ,izc+2)*wx0 + &
         fy(ixc+1,iys  ,izc+2)*wx1 + &
         fy(ixc+2,iys  ,izc+2)*wx2)*dy0 + &
        (fy(ixc-1,iys+1,izc+2)*wxm + &
         fy(ixc  ,iys+1,izc+2)*wx0 + &
         fy(ixc+1,iys+1,izc+2)*wx1 + &
         fy(ixc+2,iys+1,izc+2)*wx2)*dy1 + &
        (fy(ixc-1,iys+2,izc+2)*wxm + &
         fy(ixc  ,iys+2,izc+2)*wx0 + &
         fy(ixc+1,iys+2,izc+2)*wx1 + &
         fy(ixc+2,iys+2,izc+2)*wx2)*dy2)*wz2

  !-----------------------------------
  ! Z SHIFTED WEIGHTS
  fp1(3) = &
       ((fz(ixc-1,iyc-1,izs-1)*wxm + &
         fz(ixc  ,iyc-1,izs-1)*wx0 + &
         fz(ixc+1,iyc-1,izs-1)*wx1 + &
         fz(ixc+2,iyc-1,izs-1)*wx2)*wym + &
        (fz(ixc-1,iyc  ,izs-1)*wxm + &
         fz(ixc  ,iyc  ,izs-1)*wx0 + &
         fz(ixc+1,iyc  ,izs-1)*wx1 + &
         fz(ixc+2,iyc  ,izs-1)*wx2)*wy0 + &
        (fz(ixc-1,iyc+1,izs-1)*wxm + &
         fz(ixc  ,iyc+1,izs-1)*wx0 + &
         fz(ixc+1,iyc+1,izs-1)*wx1 + &
         fz(ixc+2,iyc+1,izs-1)*wx2)*wy1 + &
        (fz(ixc-1,iyc+2,izs-1)*wxm + &
         fz(ixc  ,iyc+2,izs-1)*wx0 + &
         fz(ixc+1,iyc+2,izs-1)*wx1 + &
         fz(ixc+2,iyc+2,izs-1)*wx2)*wy2)*dzm + &
       ((fz(ixc-1,iyc-1,izs  )*wxm + &
         fz(ixc  ,iyc-1,izs  )*wx0 + &
         fz(ixc+1,iyc-1,izs  )*wx1 + &
         fz(ixc+2,iyc-1,izs  )*wx2)*wym + &
        (fz(ixc-1,iyc  ,izs  )*wxm + &
         fz(ixc  ,iyc  ,izs  )*wx0 + &
         fz(ixc+1,iyc  ,izs  )*wx1 + &
         fz(ixc+2,iyc  ,izs  )*wx2)*wy0 + &
        (fz(ixc-1,iyc+1,izs  )*wxm + &
         fz(ixc  ,iyc+1,izs  )*wx0 + &
         fz(ixc+1,iyc+1,izs  )*wx1 + &
         fz(ixc+2,iyc+1,izs  )*wx2)*wy1 + &
        (fz(ixc-1,iyc+2,izs  )*wxm + &
         fz(ixc  ,iyc+2,izs  )*wx0 + &
         fz(ixc+1,iyc+2,izs  )*wx1 + &
         fz(ixc+2,iyc+2,izs  )*wx2)*wy2)*dz0 + &
       ((fz(ixc-1,iyc-1,izs+1)*wxm + &
         fz(ixc  ,iyc-1,izs+1)*wx0 + &
         fz(ixc+1,iyc-1,izs+1)*wx1 + &
         fz(ixc+2,iyc-1,izs+1)*wx2)*wym + &
        (fz(ixc-1,iyc  ,izs+1)*wxm + &
         fz(ixc  ,iyc  ,izs+1)*wx0 + &
         fz(ixc+1,iyc  ,izs+1)*wx1 + &
         fz(ixc+2,iyc  ,izs+1)*wx2)*wy0 + &
        (fz(ixc-1,iyc+1,izs+1)*wxm + &
         fz(ixc  ,iyc+1,izs+1)*wx0 + &
         fz(ixc+1,iyc+1,izs+1)*wx1 + &
         fz(ixc+2,iyc+1,izs+1)*wx2)*wy1 + &
        (fz(ixc-1,iyc+2,izs+1)*wxm + &
         fz(ixc  ,iyc+2,izs+1)*wx0 + &
         fz(ixc+1,iyc+2,izs+1)*wx1 + &
         fz(ixc+2,iyc+2,izs+1)*wx2)*wy2)*dz1 + &
       ((fz(ixc-1,iyc-1,izs+2)*wxm + &
         fz(ixc  ,iyc-1,izs+2)*wx0 + &
         fz(ixc+1,iyc-1,izs+2)*wx1 + &
         fz(ixc+2,iyc-1,izs+2)*wx2)*wym + &
        (fz(ixc-1,iyc  ,izs+2)*wxm + &
         fz(ixc  ,iyc  ,izs+2)*wx0 + &
         fz(ixc+1,iyc  ,izs+2)*wx1 + &
         fz(ixc+2,iyc  ,izs+2)*wx2)*wy0 + &
        (fz(ixc-1,iyc+1,izs+2)*wxm + &
         fz(ixc  ,iyc+1,izs+2)*wx0 + &
         fz(ixc+1,iyc+1,izs+2)*wx1 + &
         fz(ixc+2,iyc+1,izs+2)*wx2)*wy1 + &
        (fz(ixc-1,iyc+2,izs+2)*wxm + &
         fz(ixc  ,iyc+2,izs+2)*wx0 + &
         fz(ixc+1,iyc+2,izs+2)*wx1 + &
         fz(ixc+2,iyc+2,izs+2)*wx2)*wy2)*dz2

  !-----------------------------------
  ! YZ SHIFTED WEIGHTS
  fp2(1) = &
       ((fyz(ixc-1,iys-1,izs-1)*wxm + &
         fyz(ixc  ,iys-1,izs-1)*wx0 + &
         fyz(ixc+1,iys-1,izs-1)*wx1 + &
         fyz(ixc+2,iys-1,izs-1)*wx2)*dym + &
        (fyz(ixc-1,iys  ,izs-1)*wxm + &
         fyz(ixc  ,iys  ,izs-1)*wx0 + &
         fyz(ixc+1,iys  ,izs-1)*wx1 + &
         fyz(ixc+2,iys  ,izs-1)*wx2)*dy0 + &
        (fyz(ixc-1,iys+1,izs-1)*wxm + &
         fyz(ixc  ,iys+1,izs-1)*wx0 + &
         fyz(ixc+1,iys+1,izs-1)*wx1 + &
         fyz(ixc+2,iys+1,izs-1)*wx2)*dy1 + &
        (fyz(ixc-1,iys+2,izs-1)*wxm + &
         fyz(ixc  ,iys+2,izs-1)*wx0 + &
         fyz(ixc+1,iys+2,izs-1)*wx1 + &
         fyz(ixc+2,iys+2,izs-1)*wx2)*dy2)*dzm + &
       ((fyz(ixc-1,iys-1,izs  )*wxm + &
         fyz(ixc  ,iys-1,izs  )*wx0 + &
         fyz(ixc+1,iys-1,izs  )*wx1 + &
         fyz(ixc+2,iys-1,izs  )*wx2)*dym + &
        (fyz(ixc-1,iys  ,izs  )*wxm + &
         fyz(ixc  ,iys  ,izs  )*wx0 + &
         fyz(ixc+1,iys  ,izs  )*wx1 + &
         fyz(ixc+2,iys  ,izs  )*wx2)*dy0 + &
        (fyz(ixc-1,iys+1,izs  )*wxm + &
         fyz(ixc  ,iys+1,izs  )*wx0 + &
         fyz(ixc+1,iys+1,izs  )*wx1 + &
         fyz(ixc+2,iys+1,izs  )*wx2)*dy1 + &
        (fyz(ixc-1,iys+2,izs  )*wxm + &
         fyz(ixc  ,iys+2,izs  )*wx0 + &
         fyz(ixc+1,iys+2,izs  )*wx1 + &
         fyz(ixc+2,iys+2,izs  )*wx2)*dy2)*dz0 + &
       ((fyz(ixc-1,iys-1,izs+1)*wxm + &
         fyz(ixc  ,iys-1,izs+1)*wx0 + &
         fyz(ixc+1,iys-1,izs+1)*wx1 + &
         fyz(ixc+2,iys-1,izs+1)*wx2)*dym + &
        (fyz(ixc-1,iys  ,izs+1)*wxm + &
         fyz(ixc  ,iys  ,izs+1)*wx0 + &
         fyz(ixc+1,iys  ,izs+1)*wx1 + &
         fyz(ixc+2,iys  ,izs+1)*wx2)*dy0 + &
        (fyz(ixc-1,iys+1,izs+1)*wxm + &
         fyz(ixc  ,iys+1,izs+1)*wx0 + &
         fyz(ixc+1,iys+1,izs+1)*wx1 + &
         fyz(ixc+2,iys+1,izs+1)*wx2)*dy1 + &
        (fyz(ixc-1,iys+2,izs+1)*wxm + &
         fyz(ixc  ,iys+2,izs+1)*wx0 + &
         fyz(ixc+1,iys+2,izs+1)*wx1 + &
         fyz(ixc+2,iys+2,izs+1)*wx2)*dy2)*dz1 + &
       ((fyz(ixc-1,iys-1,izs+2)*wxm + &
         fyz(ixc  ,iys-1,izs+2)*wx0 + &
         fyz(ixc+1,iys-1,izs+2)*wx1 + &
         fyz(ixc+2,iys-1,izs+2)*wx2)*dym + &
        (fyz(ixc-1,iys  ,izs+2)*wxm + &
         fyz(ixc  ,iys  ,izs+2)*wx0 + &
         fyz(ixc+1,iys  ,izs+2)*wx1 + &
         fyz(ixc+2,iys  ,izs+2)*wx2)*dy0 + &
        (fyz(ixc-1,iys+1,izs+2)*wxm + &
         fyz(ixc  ,iys+1,izs+2)*wx0 + &
         fyz(ixc+1,iys+1,izs+2)*wx1 + &
         fyz(ixc+2,iys+1,izs+2)*wx2)*dy1 + &
        (fyz(ixc-1,iys+2,izs+2)*wxm + &
         fyz(ixc  ,iys+2,izs+2)*wx0 + &
         fyz(ixc+1,iys+2,izs+2)*wx1 + &
         fyz(ixc+2,iys+2,izs+2)*wx2)*dy2)*dz2

  !-----------------------------------
  ! ZX SHIFTED WEIGHTS
  fp2(2) = &
       ((fzx(ixs-1,iyc-1,izs-1)*dxm + &
         fzx(ixs  ,iyc-1,izs-1)*dx0 + &
         fzx(ixs+1,iyc-1,izs-1)*dx1 + &
         fzx(ixs+2,iyc-1,izs-1)*dx2)*wym + &
        (fzx(ixs-1,iyc  ,izs-1)*dxm + &
         fzx(ixs  ,iyc  ,izs-1)*dx0 + &
         fzx(ixs+1,iyc  ,izs-1)*dx1 + &
         fzx(ixs+2,iyc  ,izs-1)*dx2)*wy0 + &
        (fzx(ixs-1,iyc+1,izs-1)*dxm + &
         fzx(ixs  ,iyc+1,izs-1)*dx0 + &
         fzx(ixs+1,iyc+1,izs-1)*dx1 + &
         fzx(ixs+2,iyc+1,izs-1)*dx2)*wy1 + &
        (fzx(ixs-1,iyc+2,izs-1)*dxm + &
         fzx(ixs  ,iyc+2,izs-1)*dx0 + &
         fzx(ixs+1,iyc+2,izs-1)*dx1 + &
         fzx(ixs+2,iyc+2,izs-1)*dx2)*wy2)*dzm + &
       ((fzx(ixs-1,iyc-1,izs  )*dxm + &
         fzx(ixs  ,iyc-1,izs  )*dx0 + &
         fzx(ixs+1,iyc-1,izs  )*dx1 + &
         fzx(ixs+2,iyc-1,izs  )*dx2)*wym + &
        (fzx(ixs-1,iyc  ,izs  )*dxm + &
         fzx(ixs  ,iyc  ,izs  )*dx0 + &
         fzx(ixs+1,iyc  ,izs  )*dx1 + &
         fzx(ixs+2,iyc  ,izs  )*dx2)*wy0 + &
        (fzx(ixs-1,iyc+1,izs  )*dxm + &
         fzx(ixs  ,iyc+1,izs  )*dx0 + &
         fzx(ixs+1,iyc+1,izs  )*dx1 + &
         fzx(ixs+2,iyc+1,izs  )*dx2)*wy1 + &
        (fzx(ixs-1,iyc+2,izs  )*dxm + &
         fzx(ixs  ,iyc+2,izs  )*dx0 + &
         fzx(ixs+1,iyc+2,izs  )*dx1 + &
         fzx(ixs+2,iyc+2,izs  )*dx2)*wy2)*dz0 + &
       ((fzx(ixs-1,iyc-1,izs+1)*dxm + &
         fzx(ixs  ,iyc-1,izs+1)*dx0 + &
         fzx(ixs+1,iyc-1,izs+1)*dx1 + &
         fzx(ixs+2,iyc-1,izs+1)*dx2)*wym + &
        (fzx(ixs-1,iyc  ,izs+1)*dxm + &
         fzx(ixs  ,iyc  ,izs+1)*dx0 + &
         fzx(ixs+1,iyc  ,izs+1)*dx1 + &
         fzx(ixs+2,iyc  ,izs+1)*dx2)*wy0 + &
        (fzx(ixs-1,iyc+1,izs+1)*dxm + &
         fzx(ixs  ,iyc+1,izs+1)*dx0 + &
         fzx(ixs+1,iyc+1,izs+1)*dx1 + &
         fzx(ixs+2,iyc+1,izs+1)*dx2)*wy1 + &
        (fzx(ixs-1,iyc+2,izs+1)*dxm + &
         fzx(ixs  ,iyc+2,izs+1)*dx0 + &
         fzx(ixs+1,iyc+2,izs+1)*dx1 + &
         fzx(ixs+2,iyc+2,izs+1)*dx2)*wy2)*dz1 + &
       ((fzx(ixs-1,iyc-1,izs+2)*dxm + &
         fzx(ixs  ,iyc-1,izs+2)*dx0 + &
         fzx(ixs+1,iyc-1,izs+2)*dx1 + &
         fzx(ixs+2,iyc-1,izs+2)*dx2)*wym + &
        (fzx(ixs-1,iyc  ,izs+2)*dxm + &
         fzx(ixs  ,iyc  ,izs+2)*dx0 + &
         fzx(ixs+1,iyc  ,izs+2)*dx1 + &
         fzx(ixs+2,iyc  ,izs+2)*dx2)*wy0 + &
        (fzx(ixs-1,iyc+1,izs+2)*dxm + &
         fzx(ixs  ,iyc+1,izs+2)*dx0 + &
         fzx(ixs+1,iyc+1,izs+2)*dx1 + &
         fzx(ixs+2,iyc+1,izs+2)*dx2)*wy1 + &
        (fzx(ixs-1,iyc+2,izs+2)*dxm + &
         fzx(ixs  ,iyc+2,izs+2)*dx0 + &
         fzx(ixs+1,iyc+2,izs+2)*dx1 + &
         fzx(ixs+2,iyc+2,izs+2)*dx2)*wy2)*dz2

  !-----------------------------------
  ! XY SHIFTED WEIGHTS
  fp2(3) = &
       ((fxy(ixs-1,iys-1,izc-1)*dxm + &
         fxy(ixs  ,iys-1,izc-1)*dx0 + &
         fxy(ixs+1,iys-1,izc-1)*dx1 + &
         fxy(ixs+2,iys-1,izc-1)*dx2)*dym + &
        (fxy(ixs-1,iys  ,izc-1)*dxm + &
         fxy(ixs  ,iys  ,izc-1)*dx0 + &
         fxy(ixs+1,iys  ,izc-1)*dx1 + &
         fxy(ixs+2,iys  ,izc-1)*dx2)*dy0 + &
        (fxy(ixs-1,iys+1,izc-1)*dxm + &
         fxy(ixs  ,iys+1,izc-1)*dx0 + &
         fxy(ixs+1,iys+1,izc-1)*dx1 + &
         fxy(ixs+2,iys+1,izc-1)*dx2)*dy1 + &
        (fxy(ixs-1,iys+2,izc-1)*dxm + &
         fxy(ixs  ,iys+2,izc-1)*dx0 + &
         fxy(ixs+1,iys+2,izc-1)*dx1 + &
         fxy(ixs+2,iys+2,izc-1)*dx2)*dy2)*wzm + &
       ((fxy(ixs-1,iys-1,izc  )*dxm + &
         fxy(ixs  ,iys-1,izc  )*dx0 + &
         fxy(ixs+1,iys-1,izc  )*dx1 + &
         fxy(ixs+2,iys-1,izc  )*dx2)*dym + &
        (fxy(ixs-1,iys  ,izc  )*dxm + &
         fxy(ixs  ,iys  ,izc  )*dx0 + &
         fxy(ixs+1,iys  ,izc  )*dx1 + &
         fxy(ixs+2,iys  ,izc  )*dx2)*dy0 + &
        (fxy(ixs-1,iys+1,izc  )*dxm + &
         fxy(ixs  ,iys+1,izc  )*dx0 + &
         fxy(ixs+1,iys+1,izc  )*dx1 + &
         fxy(ixs+2,iys+1,izc  )*dx2)*dy1 + &
        (fxy(ixs-1,iys+2,izc  )*dxm + &
         fxy(ixs  ,iys+2,izc  )*dx0 + &
         fxy(ixs+1,iys+2,izc  )*dx1 + &
         fxy(ixs+2,iys+2,izc  )*dx2)*dy2)*wz0 + &
       ((fxy(ixs-1,iys-1,izc+1)*dxm + &
         fxy(ixs  ,iys-1,izc+1)*dx0 + &
         fxy(ixs+1,iys-1,izc+1)*dx1 + &
         fxy(ixs+2,iys-1,izc+1)*dx2)*dym + &
        (fxy(ixs-1,iys  ,izc+1)*dxm + &
         fxy(ixs  ,iys  ,izc+1)*dx0 + &
         fxy(ixs+1,iys  ,izc+1)*dx1 + &
         fxy(ixs+2,iys  ,izc+1)*dx2)*dy0 + &
        (fxy(ixs-1,iys+1,izc+1)*dxm + &
         fxy(ixs  ,iys+1,izc+1)*dx0 + &
         fxy(ixs+1,iys+1,izc+1)*dx1 + &
         fxy(ixs+2,iys+1,izc+1)*dx2)*dy1 + &
        (fxy(ixs-1,iys+2,izc+1)*dxm + &
         fxy(ixs  ,iys+2,izc+1)*dx0 + &
         fxy(ixs+1,iys+2,izc+1)*dx1 + &
         fxy(ixs+2,iys+2,izc+1)*dx2)*dy2)*wz1 + &
       ((fxy(ixs-1,iys-1,izc+2)*dxm + &
         fxy(ixs  ,iys-1,izc+2)*dx0 + &
         fxy(ixs+1,iys-1,izc+2)*dx1 + &
         fxy(ixs+2,iys-1,izc+2)*dx2)*dym + &
        (fxy(ixs-1,iys  ,izc+2)*dxm + &
         fxy(ixs  ,iys  ,izc+2)*dx0 + &
         fxy(ixs+1,iys  ,izc+2)*dx1 + &
         fxy(ixs+2,iys  ,izc+2)*dx2)*dy0 + &
        (fxy(ixs-1,iys+1,izc+2)*dxm + &
         fxy(ixs  ,iys+1,izc+2)*dx0 + &
         fxy(ixs+1,iys+1,izc+2)*dx1 + &
         fxy(ixs+2,iys+1,izc+2)*dx2)*dy1 + &
        (fxy(ixs-1,iys+2,izc+2)*dxm + &
         fxy(ixs  ,iys+2,izc+2)*dx0 + &
         fxy(ixs+1,iys+2,izc+2)*dx1 + &
         fxy(ixs+2,iys+2,izc+2)*dx2)*dy2)*wz2

END SUBROUTINE
!--------------------------------------------------------------------------------------
SUBROUTINE gather(f,fp,k,w)
  USE params,  only : mcoord, nspecies, do_vth
  USE species, only : field
  USE grid_m,    only : nx,ny,nz
  USE interpolation
  implicit none
  type(field), dimension(nx,ny,nz,nspecies)  :: f                               ! receiving fields
  real,        intent(in), dimension(mcoord) :: fp                              ! velocity
  integer,     intent(in)                    :: k                               ! species index
  real,        intent(in)                    :: w                               ! weight
  ! Local variables
  real :: ww,www,fp2
!...............................................................................

  !-----------------------------------------------------------------------------
  ! particle density
  www = w*wzm
  ww = wym*www
  f(ixc-1,iyc-1,izc-1,k)%d = f(ixc-1,iyc-1,izc-1,k)%d + wxm*ww
  f(ixc  ,iyc-1,izc-1,k)%d = f(ixc  ,iyc-1,izc-1,k)%d + wx0*ww
  f(ixc+1,iyc-1,izc-1,k)%d = f(ixc+1,iyc-1,izc-1,k)%d + wx1*ww
  f(ixc+2,iyc-1,izc-1,k)%d = f(ixc+2,iyc-1,izc-1,k)%d + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izc-1,k)%d = f(ixc-1,iyc  ,izc-1,k)%d + wxm*ww
  f(ixc  ,iyc  ,izc-1,k)%d = f(ixc  ,iyc  ,izc-1,k)%d + wx0*ww
  f(ixc+1,iyc  ,izc-1,k)%d = f(ixc+1,iyc  ,izc-1,k)%d + wx1*ww
  f(ixc+2,iyc  ,izc-1,k)%d = f(ixc+2,iyc  ,izc-1,k)%d + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izc-1,k)%d = f(ixc-1,iyc+1,izc-1,k)%d + wxm*ww
  f(ixc  ,iyc+1,izc-1,k)%d = f(ixc  ,iyc+1,izc-1,k)%d + wx0*ww
  f(ixc+1,iyc+1,izc-1,k)%d = f(ixc+1,iyc+1,izc-1,k)%d + wx1*ww
  f(ixc+2,iyc+1,izc-1,k)%d = f(ixc+2,iyc+1,izc-1,k)%d + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izc-1,k)%d = f(ixc-1,iyc+2,izc-1,k)%d + wxm*ww
  f(ixc  ,iyc+2,izc-1,k)%d = f(ixc  ,iyc+2,izc-1,k)%d + wx0*ww
  f(ixc+1,iyc+2,izc-1,k)%d = f(ixc+1,iyc+2,izc-1,k)%d + wx1*ww
  f(ixc+2,iyc+2,izc-1,k)%d = f(ixc+2,iyc+2,izc-1,k)%d + wx2*ww
  www = w*wz0
  ww = wym*www
  f(ixc-1,iyc-1,izc  ,k)%d = f(ixc-1,iyc-1,izc  ,k)%d + wxm*ww
  f(ixc  ,iyc-1,izc  ,k)%d = f(ixc  ,iyc-1,izc  ,k)%d + wx0*ww
  f(ixc+1,iyc-1,izc  ,k)%d = f(ixc+1,iyc-1,izc  ,k)%d + wx1*ww
  f(ixc+2,iyc-1,izc  ,k)%d = f(ixc+2,iyc-1,izc  ,k)%d + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izc  ,k)%d = f(ixc-1,iyc  ,izc  ,k)%d + wxm*ww
  f(ixc  ,iyc  ,izc  ,k)%d = f(ixc  ,iyc  ,izc  ,k)%d + wx0*ww
  f(ixc+1,iyc  ,izc  ,k)%d = f(ixc+1,iyc  ,izc  ,k)%d + wx1*ww
  f(ixc+2,iyc  ,izc  ,k)%d = f(ixc+2,iyc  ,izc  ,k)%d + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izc  ,k)%d = f(ixc-1,iyc+1,izc  ,k)%d + wxm*ww
  f(ixc  ,iyc+1,izc  ,k)%d = f(ixc  ,iyc+1,izc  ,k)%d + wx0*ww
  f(ixc+1,iyc+1,izc  ,k)%d = f(ixc+1,iyc+1,izc  ,k)%d + wx1*ww
  f(ixc+2,iyc+1,izc  ,k)%d = f(ixc+2,iyc+1,izc  ,k)%d + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izc  ,k)%d = f(ixc-1,iyc+2,izc  ,k)%d + wxm*ww
  f(ixc  ,iyc+2,izc  ,k)%d = f(ixc  ,iyc+2,izc  ,k)%d + wx0*ww
  f(ixc+1,iyc+2,izc  ,k)%d = f(ixc+1,iyc+2,izc  ,k)%d + wx1*ww
  f(ixc+2,iyc+2,izc  ,k)%d = f(ixc+2,iyc+2,izc  ,k)%d + wx2*ww
  www = w*wz1
  ww = wym*www
  f(ixc-1,iyc-1,izc+1,k)%d = f(ixc-1,iyc-1,izc+1,k)%d + wxm*ww
  f(ixc  ,iyc-1,izc+1,k)%d = f(ixc  ,iyc-1,izc+1,k)%d + wx0*ww
  f(ixc+1,iyc-1,izc+1,k)%d = f(ixc+1,iyc-1,izc+1,k)%d + wx1*ww
  f(ixc+2,iyc-1,izc+1,k)%d = f(ixc+2,iyc-1,izc+1,k)%d + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izc+1,k)%d = f(ixc-1,iyc  ,izc+1,k)%d + wxm*ww
  f(ixc  ,iyc  ,izc+1,k)%d = f(ixc  ,iyc  ,izc+1,k)%d + wx0*ww
  f(ixc+1,iyc  ,izc+1,k)%d = f(ixc+1,iyc  ,izc+1,k)%d + wx1*ww
  f(ixc+2,iyc  ,izc+1,k)%d = f(ixc+2,iyc  ,izc+1,k)%d + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izc+1,k)%d = f(ixc-1,iyc+1,izc+1,k)%d + wxm*ww
  f(ixc  ,iyc+1,izc+1,k)%d = f(ixc  ,iyc+1,izc+1,k)%d + wx0*ww
  f(ixc+1,iyc+1,izc+1,k)%d = f(ixc+1,iyc+1,izc+1,k)%d + wx1*ww
  f(ixc+2,iyc+1,izc+1,k)%d = f(ixc+2,iyc+1,izc+1,k)%d + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izc+1,k)%d = f(ixc-1,iyc+2,izc+1,k)%d + wxm*ww
  f(ixc  ,iyc+2,izc+1,k)%d = f(ixc  ,iyc+2,izc+1,k)%d + wx0*ww
  f(ixc+1,iyc+2,izc+1,k)%d = f(ixc+1,iyc+2,izc+1,k)%d + wx1*ww
  f(ixc+2,iyc+2,izc+1,k)%d = f(ixc+2,iyc+2,izc+1,k)%d + wx2*ww
  www = w*wz2
  ww = wym*www
  f(ixc-1,iyc-1,izc+2,k)%d = f(ixc-1,iyc-1,izc+2,k)%d + wxm*ww
  f(ixc  ,iyc-1,izc+2,k)%d = f(ixc  ,iyc-1,izc+2,k)%d + wx0*ww
  f(ixc+1,iyc-1,izc+2,k)%d = f(ixc+1,iyc-1,izc+2,k)%d + wx1*ww
  f(ixc+2,iyc-1,izc+2,k)%d = f(ixc+2,iyc-1,izc+2,k)%d + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izc+2,k)%d = f(ixc-1,iyc  ,izc+2,k)%d + wxm*ww
  f(ixc  ,iyc  ,izc+2,k)%d = f(ixc  ,iyc  ,izc+2,k)%d + wx0*ww
  f(ixc+1,iyc  ,izc+2,k)%d = f(ixc+1,iyc  ,izc+2,k)%d + wx1*ww
  f(ixc+2,iyc  ,izc+2,k)%d = f(ixc+2,iyc  ,izc+2,k)%d + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izc+2,k)%d = f(ixc-1,iyc+1,izc+2,k)%d + wxm*ww
  f(ixc  ,iyc+1,izc+2,k)%d = f(ixc  ,iyc+1,izc+2,k)%d + wx0*ww
  f(ixc+1,iyc+1,izc+2,k)%d = f(ixc+1,iyc+1,izc+2,k)%d + wx1*ww
  f(ixc+2,iyc+1,izc+2,k)%d = f(ixc+2,iyc+1,izc+2,k)%d + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izc+2,k)%d = f(ixc-1,iyc+2,izc+2,k)%d + wxm*ww
  f(ixc  ,iyc+2,izc+2,k)%d = f(ixc  ,iyc+2,izc+2,k)%d + wx0*ww
  f(ixc+1,iyc+2,izc+2,k)%d = f(ixc+1,iyc+2,izc+2,k)%d + wx1*ww
  f(ixc+2,iyc+2,izc+2,k)%d = f(ixc+2,iyc+2,izc+2,k)%d + wx2*ww

  !-----------------------------------------------------------------------------
  ! particle thermal velocity
  if (do_vth) then
    fp2 = (fp(1)**2+fp(2)**2+fp(3)**2)*w
    www = fp2*wzm
    ww = wym*www
    f(ixc-1,iyc-1,izc-1,k)%vth = f(ixc-1,iyc-1,izc-1,k)%vth + wxm*ww
    f(ixc  ,iyc-1,izc-1,k)%vth = f(ixc  ,iyc-1,izc-1,k)%vth + wx0*ww
    f(ixc+1,iyc-1,izc-1,k)%vth = f(ixc+1,iyc-1,izc-1,k)%vth + wx1*ww
    f(ixc+2,iyc-1,izc-1,k)%vth = f(ixc+2,iyc-1,izc-1,k)%vth + wx2*ww
    ww = wy0*www
    f(ixc-1,iyc  ,izc-1,k)%vth = f(ixc-1,iyc  ,izc-1,k)%vth + wxm*ww
    f(ixc  ,iyc  ,izc-1,k)%vth = f(ixc  ,iyc  ,izc-1,k)%vth + wx0*ww
    f(ixc+1,iyc  ,izc-1,k)%vth = f(ixc+1,iyc  ,izc-1,k)%vth + wx1*ww
    f(ixc+2,iyc  ,izc-1,k)%vth = f(ixc+2,iyc  ,izc-1,k)%vth + wx2*ww
    ww = wy1*www
    f(ixc-1,iyc+1,izc-1,k)%vth = f(ixc-1,iyc+1,izc-1,k)%vth + wxm*ww
    f(ixc  ,iyc+1,izc-1,k)%vth = f(ixc  ,iyc+1,izc-1,k)%vth + wx0*ww
    f(ixc+1,iyc+1,izc-1,k)%vth = f(ixc+1,iyc+1,izc-1,k)%vth + wx1*ww
    f(ixc+2,iyc+1,izc-1,k)%vth = f(ixc+2,iyc+1,izc-1,k)%vth + wx2*ww
    ww = wy2*www
    f(ixc-1,iyc+2,izc-1,k)%vth = f(ixc-1,iyc+2,izc-1,k)%vth + wxm*ww
    f(ixc  ,iyc+2,izc-1,k)%vth = f(ixc  ,iyc+2,izc-1,k)%vth + wx0*ww
    f(ixc+1,iyc+2,izc-1,k)%vth = f(ixc+1,iyc+2,izc-1,k)%vth + wx1*ww
    f(ixc+2,iyc+2,izc-1,k)%vth = f(ixc+2,iyc+2,izc-1,k)%vth + wx2*ww
    www = fp2*wz0
    ww = wym*www
    f(ixc-1,iyc-1,izc  ,k)%vth = f(ixc-1,iyc-1,izc  ,k)%vth + wxm*ww
    f(ixc  ,iyc-1,izc  ,k)%vth = f(ixc  ,iyc-1,izc  ,k)%vth + wx0*ww
    f(ixc+1,iyc-1,izc  ,k)%vth = f(ixc+1,iyc-1,izc  ,k)%vth + wx1*ww
    f(ixc+2,iyc-1,izc  ,k)%vth = f(ixc+2,iyc-1,izc  ,k)%vth + wx2*ww
    ww = wy0*www
    f(ixc-1,iyc  ,izc  ,k)%vth = f(ixc-1,iyc  ,izc  ,k)%vth + wxm*ww
    f(ixc  ,iyc  ,izc  ,k)%vth = f(ixc  ,iyc  ,izc  ,k)%vth + wx0*ww
    f(ixc+1,iyc  ,izc  ,k)%vth = f(ixc+1,iyc  ,izc  ,k)%vth + wx1*ww
    f(ixc+2,iyc  ,izc  ,k)%vth = f(ixc+2,iyc  ,izc  ,k)%vth + wx2*ww
    ww = wy1*www
    f(ixc-1,iyc+1,izc  ,k)%vth = f(ixc-1,iyc+1,izc  ,k)%vth + wxm*ww
    f(ixc  ,iyc+1,izc  ,k)%vth = f(ixc  ,iyc+1,izc  ,k)%vth + wx0*ww
    f(ixc+1,iyc+1,izc  ,k)%vth = f(ixc+1,iyc+1,izc  ,k)%vth + wx1*ww
    f(ixc+2,iyc+1,izc  ,k)%vth = f(ixc+2,iyc+1,izc  ,k)%vth + wx2*ww
    ww = wy2*www
    f(ixc-1,iyc+2,izc  ,k)%vth = f(ixc-1,iyc+2,izc  ,k)%vth + wxm*ww
    f(ixc  ,iyc+2,izc  ,k)%vth = f(ixc  ,iyc+2,izc  ,k)%vth + wx0*ww
    f(ixc+1,iyc+2,izc  ,k)%vth = f(ixc+1,iyc+2,izc  ,k)%vth + wx1*ww
    f(ixc+2,iyc+2,izc  ,k)%vth = f(ixc+2,iyc+2,izc  ,k)%vth + wx2*ww
    www = fp2*wz1
    ww = wym*www
    f(ixc-1,iyc-1,izc+1,k)%vth = f(ixc-1,iyc-1,izc+1,k)%vth + wxm*ww
    f(ixc  ,iyc-1,izc+1,k)%vth = f(ixc  ,iyc-1,izc+1,k)%vth + wx0*ww
    f(ixc+1,iyc-1,izc+1,k)%vth = f(ixc+1,iyc-1,izc+1,k)%vth + wx1*ww
    f(ixc+2,iyc-1,izc+1,k)%vth = f(ixc+2,iyc-1,izc+1,k)%vth + wx2*ww
    ww = wy0*www
    f(ixc-1,iyc  ,izc+1,k)%vth = f(ixc-1,iyc  ,izc+1,k)%vth + wxm*ww
    f(ixc  ,iyc  ,izc+1,k)%vth = f(ixc  ,iyc  ,izc+1,k)%vth + wx0*ww
    f(ixc+1,iyc  ,izc+1,k)%vth = f(ixc+1,iyc  ,izc+1,k)%vth + wx1*ww
    f(ixc+2,iyc  ,izc+1,k)%vth = f(ixc+2,iyc  ,izc+1,k)%vth + wx2*ww
    ww = wy1*www
    f(ixc-1,iyc+1,izc+1,k)%vth = f(ixc-1,iyc+1,izc+1,k)%vth + wxm*ww
    f(ixc  ,iyc+1,izc+1,k)%vth = f(ixc  ,iyc+1,izc+1,k)%vth + wx0*ww
    f(ixc+1,iyc+1,izc+1,k)%vth = f(ixc+1,iyc+1,izc+1,k)%vth + wx1*ww
    f(ixc+2,iyc+1,izc+1,k)%vth = f(ixc+2,iyc+1,izc+1,k)%vth + wx2*ww
    ww = wy2*www
    f(ixc-1,iyc+2,izc+1,k)%vth = f(ixc-1,iyc+2,izc+1,k)%vth + wxm*ww
    f(ixc  ,iyc+2,izc+1,k)%vth = f(ixc  ,iyc+2,izc+1,k)%vth + wx0*ww
    f(ixc+1,iyc+2,izc+1,k)%vth = f(ixc+1,iyc+2,izc+1,k)%vth + wx1*ww
    f(ixc+2,iyc+2,izc+1,k)%vth = f(ixc+2,iyc+2,izc+1,k)%vth + wx2*ww
    www = fp2*wz2
    ww = wym*www
    f(ixc-1,iyc-1,izc+2,k)%vth = f(ixc-1,iyc-1,izc+2,k)%vth + wxm*ww
    f(ixc  ,iyc-1,izc+2,k)%vth = f(ixc  ,iyc-1,izc+2,k)%vth + wx0*ww
    f(ixc+1,iyc-1,izc+2,k)%vth = f(ixc+1,iyc-1,izc+2,k)%vth + wx1*ww
    f(ixc+2,iyc-1,izc+2,k)%vth = f(ixc+2,iyc-1,izc+2,k)%vth + wx2*ww
    ww = wy0*www
    f(ixc-1,iyc  ,izc+2,k)%vth = f(ixc-1,iyc  ,izc+2,k)%vth + wxm*ww
    f(ixc  ,iyc  ,izc+2,k)%vth = f(ixc  ,iyc  ,izc+2,k)%vth + wx0*ww
    f(ixc+1,iyc  ,izc+2,k)%vth = f(ixc+1,iyc  ,izc+2,k)%vth + wx1*ww
    f(ixc+2,iyc  ,izc+2,k)%vth = f(ixc+2,iyc  ,izc+2,k)%vth + wx2*ww
    ww = wy1*www
    f(ixc-1,iyc+1,izc+2,k)%vth = f(ixc-1,iyc+1,izc+2,k)%vth + wxm*ww
    f(ixc  ,iyc+1,izc+2,k)%vth = f(ixc  ,iyc+1,izc+2,k)%vth + wx0*ww
    f(ixc+1,iyc+1,izc+2,k)%vth = f(ixc+1,iyc+1,izc+2,k)%vth + wx1*ww
    f(ixc+2,iyc+1,izc+2,k)%vth = f(ixc+2,iyc+1,izc+2,k)%vth + wx2*ww
    ww = wy2*www
    f(ixc-1,iyc+2,izc+2,k)%vth = f(ixc-1,iyc+2,izc+2,k)%vth + wxm*ww
    f(ixc  ,iyc+2,izc+2,k)%vth = f(ixc  ,iyc+2,izc+2,k)%vth + wx0*ww
    f(ixc+1,iyc+2,izc+2,k)%vth = f(ixc+1,iyc+2,izc+2,k)%vth + wx1*ww
    f(ixc+2,iyc+2,izc+2,k)%vth = f(ixc+2,iyc+2,izc+2,k)%vth + wx2*ww
  end if 

  !-----------------------------------------------------------------------------
  ! Particle x-flux
  www = fp(1)*w*wzm
  ww = wym*www
  f(ixs-1,iyc-1,izc-1,k)%v(1) = f(ixs-1,iyc-1,izc-1,k)%v(1) + dxm*ww
  f(ixs  ,iyc-1,izc-1,k)%v(1) = f(ixs  ,iyc-1,izc-1,k)%v(1) + dx0*ww
  f(ixs+1,iyc-1,izc-1,k)%v(1) = f(ixs+1,iyc-1,izc-1,k)%v(1) + dx1*ww
  f(ixs+2,iyc-1,izc-1,k)%v(1) = f(ixs+2,iyc-1,izc-1,k)%v(1) + dx2*ww
  ww = wy0*www
  f(ixs-1,iyc  ,izc-1,k)%v(1) = f(ixs-1,iyc  ,izc-1,k)%v(1) + dxm*ww
  f(ixs  ,iyc  ,izc-1,k)%v(1) = f(ixs  ,iyc  ,izc-1,k)%v(1) + dx0*ww
  f(ixs+1,iyc  ,izc-1,k)%v(1) = f(ixs+1,iyc  ,izc-1,k)%v(1) + dx1*ww
  f(ixs+2,iyc  ,izc-1,k)%v(1) = f(ixs+2,iyc  ,izc-1,k)%v(1) + dx2*ww
  ww = wy1*www
  f(ixs-1,iyc+1,izc-1,k)%v(1) = f(ixs-1,iyc+1,izc-1,k)%v(1) + dxm*ww
  f(ixs  ,iyc+1,izc-1,k)%v(1) = f(ixs  ,iyc+1,izc-1,k)%v(1) + dx0*ww
  f(ixs+1,iyc+1,izc-1,k)%v(1) = f(ixs+1,iyc+1,izc-1,k)%v(1) + dx1*ww
  f(ixs+2,iyc+1,izc-1,k)%v(1) = f(ixs+2,iyc+1,izc-1,k)%v(1) + dx2*ww
  ww = wy2*www
  f(ixs-1,iyc+2,izc-1,k)%v(1) = f(ixs-1,iyc+2,izc-1,k)%v(1) + dxm*ww
  f(ixs  ,iyc+2,izc-1,k)%v(1) = f(ixs  ,iyc+2,izc-1,k)%v(1) + dx0*ww
  f(ixs+1,iyc+2,izc-1,k)%v(1) = f(ixs+1,iyc+2,izc-1,k)%v(1) + dx1*ww
  f(ixs+2,iyc+2,izc-1,k)%v(1) = f(ixs+2,iyc+2,izc-1,k)%v(1) + dx2*ww
  www = fp(1)*w*wz0
  ww = wym*www
  f(ixs-1,iyc-1,izc  ,k)%v(1) = f(ixs-1,iyc-1,izc  ,k)%v(1) + dxm*ww
  f(ixs  ,iyc-1,izc  ,k)%v(1) = f(ixs  ,iyc-1,izc  ,k)%v(1) + dx0*ww
  f(ixs+1,iyc-1,izc  ,k)%v(1) = f(ixs+1,iyc-1,izc  ,k)%v(1) + dx1*ww
  f(ixs+2,iyc-1,izc  ,k)%v(1) = f(ixs+2,iyc-1,izc  ,k)%v(1) + dx2*ww
  ww = wy0*www
  f(ixs-1,iyc  ,izc  ,k)%v(1) = f(ixs-1,iyc  ,izc  ,k)%v(1) + dxm*ww
  f(ixs  ,iyc  ,izc  ,k)%v(1) = f(ixs  ,iyc  ,izc  ,k)%v(1) + dx0*ww
  f(ixs+1,iyc  ,izc  ,k)%v(1) = f(ixs+1,iyc  ,izc  ,k)%v(1) + dx1*ww
  f(ixs+2,iyc  ,izc  ,k)%v(1) = f(ixs+2,iyc  ,izc  ,k)%v(1) + dx2*ww
  ww = wy1*www
  f(ixs-1,iyc+1,izc  ,k)%v(1) = f(ixs-1,iyc+1,izc  ,k)%v(1) + dxm*ww
  f(ixs  ,iyc+1,izc  ,k)%v(1) = f(ixs  ,iyc+1,izc  ,k)%v(1) + dx0*ww
  f(ixs+1,iyc+1,izc  ,k)%v(1) = f(ixs+1,iyc+1,izc  ,k)%v(1) + dx1*ww
  f(ixs+2,iyc+1,izc  ,k)%v(1) = f(ixs+2,iyc+1,izc  ,k)%v(1) + dx2*ww
  ww = wy2*www
  f(ixs-1,iyc+2,izc  ,k)%v(1) = f(ixs-1,iyc+2,izc  ,k)%v(1) + dxm*ww
  f(ixs  ,iyc+2,izc  ,k)%v(1) = f(ixs  ,iyc+2,izc  ,k)%v(1) + dx0*ww
  f(ixs+1,iyc+2,izc  ,k)%v(1) = f(ixs+1,iyc+2,izc  ,k)%v(1) + dx1*ww
  f(ixs+2,iyc+2,izc  ,k)%v(1) = f(ixs+2,iyc+2,izc  ,k)%v(1) + dx2*ww
  www = fp(1)*w*wz1
  ww = wym*www
  f(ixs-1,iyc-1,izc+1,k)%v(1) = f(ixs-1,iyc-1,izc+1,k)%v(1) + dxm*ww
  f(ixs  ,iyc-1,izc+1,k)%v(1) = f(ixs  ,iyc-1,izc+1,k)%v(1) + dx0*ww
  f(ixs+1,iyc-1,izc+1,k)%v(1) = f(ixs+1,iyc-1,izc+1,k)%v(1) + dx1*ww
  f(ixs+2,iyc-1,izc+1,k)%v(1) = f(ixs+2,iyc-1,izc+1,k)%v(1) + dx2*ww
  ww = wy0*www
  f(ixs-1,iyc  ,izc+1,k)%v(1) = f(ixs-1,iyc  ,izc+1,k)%v(1) + dxm*ww
  f(ixs  ,iyc  ,izc+1,k)%v(1) = f(ixs  ,iyc  ,izc+1,k)%v(1) + dx0*ww
  f(ixs+1,iyc  ,izc+1,k)%v(1) = f(ixs+1,iyc  ,izc+1,k)%v(1) + dx1*ww
  f(ixs+2,iyc  ,izc+1,k)%v(1) = f(ixs+2,iyc  ,izc+1,k)%v(1) + dx2*ww
  ww = wy1*www
  f(ixs-1,iyc+1,izc+1,k)%v(1) = f(ixs-1,iyc+1,izc+1,k)%v(1) + dxm*ww
  f(ixs  ,iyc+1,izc+1,k)%v(1) = f(ixs  ,iyc+1,izc+1,k)%v(1) + dx0*ww
  f(ixs+1,iyc+1,izc+1,k)%v(1) = f(ixs+1,iyc+1,izc+1,k)%v(1) + dx1*ww
  f(ixs+2,iyc+1,izc+1,k)%v(1) = f(ixs+2,iyc+1,izc+1,k)%v(1) + dx2*ww
  ww = wy2*www
  f(ixs-1,iyc+2,izc+1,k)%v(1) = f(ixs-1,iyc+2,izc+1,k)%v(1) + dxm*ww
  f(ixs  ,iyc+2,izc+1,k)%v(1) = f(ixs  ,iyc+2,izc+1,k)%v(1) + dx0*ww
  f(ixs+1,iyc+2,izc+1,k)%v(1) = f(ixs+1,iyc+2,izc+1,k)%v(1) + dx1*ww
  f(ixs+2,iyc+2,izc+1,k)%v(1) = f(ixs+2,iyc+2,izc+1,k)%v(1) + dx2*ww
  www = fp(1)*w*wz2
  ww = wym*www
  f(ixs-1,iyc-1,izc+2,k)%v(1) = f(ixs-1,iyc-1,izc+2,k)%v(1) + dxm*ww
  f(ixs  ,iyc-1,izc+2,k)%v(1) = f(ixs  ,iyc-1,izc+2,k)%v(1) + dx0*ww
  f(ixs+1,iyc-1,izc+2,k)%v(1) = f(ixs+1,iyc-1,izc+2,k)%v(1) + dx1*ww
  f(ixs+2,iyc-1,izc+2,k)%v(1) = f(ixs+2,iyc-1,izc+2,k)%v(1) + dx2*ww
  ww = wy0*www
  f(ixs-1,iyc  ,izc+2,k)%v(1) = f(ixs-1,iyc  ,izc+2,k)%v(1) + dxm*ww
  f(ixs  ,iyc  ,izc+2,k)%v(1) = f(ixs  ,iyc  ,izc+2,k)%v(1) + dx0*ww
  f(ixs+1,iyc  ,izc+2,k)%v(1) = f(ixs+1,iyc  ,izc+2,k)%v(1) + dx1*ww
  f(ixs+2,iyc  ,izc+2,k)%v(1) = f(ixs+2,iyc  ,izc+2,k)%v(1) + dx2*ww
  ww = wy1*www
  f(ixs-1,iyc+1,izc+2,k)%v(1) = f(ixs-1,iyc+1,izc+2,k)%v(1) + dxm*ww
  f(ixs  ,iyc+1,izc+2,k)%v(1) = f(ixs  ,iyc+1,izc+2,k)%v(1) + dx0*ww
  f(ixs+1,iyc+1,izc+2,k)%v(1) = f(ixs+1,iyc+1,izc+2,k)%v(1) + dx1*ww
  f(ixs+2,iyc+1,izc+2,k)%v(1) = f(ixs+2,iyc+1,izc+2,k)%v(1) + dx2*ww
  ww = wy2*www
  f(ixs-1,iyc+2,izc+2,k)%v(1) = f(ixs-1,iyc+2,izc+2,k)%v(1) + dxm*ww
  f(ixs  ,iyc+2,izc+2,k)%v(1) = f(ixs  ,iyc+2,izc+2,k)%v(1) + dx0*ww
  f(ixs+1,iyc+2,izc+2,k)%v(1) = f(ixs+1,iyc+2,izc+2,k)%v(1) + dx1*ww
  f(ixs+2,iyc+2,izc+2,k)%v(1) = f(ixs+2,iyc+2,izc+2,k)%v(1) + dx2*ww

  !-----------------------------------------------------------------------------
  ! Particle y-flux
  www = fp(2)*w*wzm
  ww = dym*www
  f(ixc-1,iys-1,izc-1,k)%v(2) = f(ixc-1,iys-1,izc-1,k)%v(2) + wxm*ww
  f(ixc  ,iys-1,izc-1,k)%v(2) = f(ixc  ,iys-1,izc-1,k)%v(2) + wx0*ww
  f(ixc+1,iys-1,izc-1,k)%v(2) = f(ixc+1,iys-1,izc-1,k)%v(2) + wx1*ww
  f(ixc+2,iys-1,izc-1,k)%v(2) = f(ixc+2,iys-1,izc-1,k)%v(2) + wx2*ww
  ww = dy0*www
  f(ixc-1,iys  ,izc-1,k)%v(2) = f(ixc-1,iys  ,izc-1,k)%v(2) + wxm*ww
  f(ixc  ,iys  ,izc-1,k)%v(2) = f(ixc  ,iys  ,izc-1,k)%v(2) + wx0*ww
  f(ixc+1,iys  ,izc-1,k)%v(2) = f(ixc+1,iys  ,izc-1,k)%v(2) + wx1*ww
  f(ixc+2,iys  ,izc-1,k)%v(2) = f(ixc+2,iys  ,izc-1,k)%v(2) + wx2*ww
  ww = dy1*www
  f(ixc-1,iys+1,izc-1,k)%v(2) = f(ixc-1,iys+1,izc-1,k)%v(2) + wxm*ww
  f(ixc  ,iys+1,izc-1,k)%v(2) = f(ixc  ,iys+1,izc-1,k)%v(2) + wx0*ww
  f(ixc+1,iys+1,izc-1,k)%v(2) = f(ixc+1,iys+1,izc-1,k)%v(2) + wx1*ww
  f(ixc+2,iys+1,izc-1,k)%v(2) = f(ixc+2,iys+1,izc-1,k)%v(2) + wx2*ww
  ww = dy2*www
  f(ixc-1,iys+2,izc-1,k)%v(2) = f(ixc-1,iys+2,izc-1,k)%v(2) + wxm*ww
  f(ixc  ,iys+2,izc-1,k)%v(2) = f(ixc  ,iys+2,izc-1,k)%v(2) + wx0*ww
  f(ixc+1,iys+2,izc-1,k)%v(2) = f(ixc+1,iys+2,izc-1,k)%v(2) + wx1*ww
  f(ixc+2,iys+2,izc-1,k)%v(2) = f(ixc+2,iys+2,izc-1,k)%v(2) + wx2*ww
  www = fp(2)*w*wz0
  ww = dym*www
  f(ixc-1,iys-1,izc  ,k)%v(2) = f(ixc-1,iys-1,izc  ,k)%v(2) + wxm*ww
  f(ixc  ,iys-1,izc  ,k)%v(2) = f(ixc  ,iys-1,izc  ,k)%v(2) + wx0*ww
  f(ixc+1,iys-1,izc  ,k)%v(2) = f(ixc+1,iys-1,izc  ,k)%v(2) + wx1*ww
  f(ixc+2,iys-1,izc  ,k)%v(2) = f(ixc+2,iys-1,izc  ,k)%v(2) + wx2*ww
  ww = dy0*www
  f(ixc-1,iys  ,izc  ,k)%v(2) = f(ixc-1,iys  ,izc  ,k)%v(2) + wxm*ww
  f(ixc  ,iys  ,izc  ,k)%v(2) = f(ixc  ,iys  ,izc  ,k)%v(2) + wx0*ww
  f(ixc+1,iys  ,izc  ,k)%v(2) = f(ixc+1,iys  ,izc  ,k)%v(2) + wx1*ww
  f(ixc+2,iys  ,izc  ,k)%v(2) = f(ixc+2,iys  ,izc  ,k)%v(2) + wx2*ww
  ww = dy1*www
  f(ixc-1,iys+1,izc  ,k)%v(2) = f(ixc-1,iys+1,izc  ,k)%v(2) + wxm*ww
  f(ixc  ,iys+1,izc  ,k)%v(2) = f(ixc  ,iys+1,izc  ,k)%v(2) + wx0*ww
  f(ixc+1,iys+1,izc  ,k)%v(2) = f(ixc+1,iys+1,izc  ,k)%v(2) + wx1*ww
  f(ixc+2,iys+1,izc  ,k)%v(2) = f(ixc+2,iys+1,izc  ,k)%v(2) + wx2*ww
  ww = dy2*www
  f(ixc-1,iys+2,izc  ,k)%v(2) = f(ixc-1,iys+2,izc  ,k)%v(2) + wxm*ww
  f(ixc  ,iys+2,izc  ,k)%v(2) = f(ixc  ,iys+2,izc  ,k)%v(2) + wx0*ww
  f(ixc+1,iys+2,izc  ,k)%v(2) = f(ixc+1,iys+2,izc  ,k)%v(2) + wx1*ww
  f(ixc+2,iys+2,izc  ,k)%v(2) = f(ixc+2,iys+2,izc  ,k)%v(2) + wx2*ww
  www = fp(2)*w*wz1
  ww = dym*www
  f(ixc-1,iys-1,izc+1,k)%v(2) = f(ixc-1,iys-1,izc+1,k)%v(2) + wxm*ww
  f(ixc  ,iys-1,izc+1,k)%v(2) = f(ixc  ,iys-1,izc+1,k)%v(2) + wx0*ww
  f(ixc+1,iys-1,izc+1,k)%v(2) = f(ixc+1,iys-1,izc+1,k)%v(2) + wx1*ww
  f(ixc+2,iys-1,izc+1,k)%v(2) = f(ixc+2,iys-1,izc+1,k)%v(2) + wx2*ww
  ww = dy0*www
  f(ixc-1,iys  ,izc+1,k)%v(2) = f(ixc-1,iys  ,izc+1,k)%v(2) + wxm*ww
  f(ixc  ,iys  ,izc+1,k)%v(2) = f(ixc  ,iys  ,izc+1,k)%v(2) + wx0*ww
  f(ixc+1,iys  ,izc+1,k)%v(2) = f(ixc+1,iys  ,izc+1,k)%v(2) + wx1*ww
  f(ixc+2,iys  ,izc+1,k)%v(2) = f(ixc+2,iys  ,izc+1,k)%v(2) + wx2*ww
  ww = dy1*www
  f(ixc-1,iys+1,izc+1,k)%v(2) = f(ixc-1,iys+1,izc+1,k)%v(2) + wxm*ww
  f(ixc  ,iys+1,izc+1,k)%v(2) = f(ixc  ,iys+1,izc+1,k)%v(2) + wx0*ww
  f(ixc+1,iys+1,izc+1,k)%v(2) = f(ixc+1,iys+1,izc+1,k)%v(2) + wx1*ww
  f(ixc+2,iys+1,izc+1,k)%v(2) = f(ixc+2,iys+1,izc+1,k)%v(2) + wx2*ww
  ww = dy2*www
  f(ixc-1,iys+2,izc+1,k)%v(2) = f(ixc-1,iys+2,izc+1,k)%v(2) + wxm*ww
  f(ixc  ,iys+2,izc+1,k)%v(2) = f(ixc  ,iys+2,izc+1,k)%v(2) + wx0*ww
  f(ixc+1,iys+2,izc+1,k)%v(2) = f(ixc+1,iys+2,izc+1,k)%v(2) + wx1*ww
  f(ixc+2,iys+2,izc+1,k)%v(2) = f(ixc+2,iys+2,izc+1,k)%v(2) + wx2*ww
  www = fp(2)*w*wz2
  ww = dym*www
  f(ixc-1,iys-1,izc+2,k)%v(2) = f(ixc-1,iys-1,izc+2,k)%v(2) + wxm*ww
  f(ixc  ,iys-1,izc+2,k)%v(2) = f(ixc  ,iys-1,izc+2,k)%v(2) + wx0*ww
  f(ixc+1,iys-1,izc+2,k)%v(2) = f(ixc+1,iys-1,izc+2,k)%v(2) + wx1*ww
  f(ixc+2,iys-1,izc+2,k)%v(2) = f(ixc+2,iys-1,izc+2,k)%v(2) + wx2*ww
  ww = dy0*www
  f(ixc-1,iys  ,izc+2,k)%v(2) = f(ixc-1,iys  ,izc+2,k)%v(2) + wxm*ww
  f(ixc  ,iys  ,izc+2,k)%v(2) = f(ixc  ,iys  ,izc+2,k)%v(2) + wx0*ww
  f(ixc+1,iys  ,izc+2,k)%v(2) = f(ixc+1,iys  ,izc+2,k)%v(2) + wx1*ww
  f(ixc+2,iys  ,izc+2,k)%v(2) = f(ixc+2,iys  ,izc+2,k)%v(2) + wx2*ww
  ww = dy1*www
  f(ixc-1,iys+1,izc+2,k)%v(2) = f(ixc-1,iys+1,izc+2,k)%v(2) + wxm*ww
  f(ixc  ,iys+1,izc+2,k)%v(2) = f(ixc  ,iys+1,izc+2,k)%v(2) + wx0*ww
  f(ixc+1,iys+1,izc+2,k)%v(2) = f(ixc+1,iys+1,izc+2,k)%v(2) + wx1*ww
  f(ixc+2,iys+1,izc+2,k)%v(2) = f(ixc+2,iys+1,izc+2,k)%v(2) + wx2*ww
  ww = dy2*www
  f(ixc-1,iys+2,izc+2,k)%v(2) = f(ixc-1,iys+2,izc+2,k)%v(2) + wxm*ww
  f(ixc  ,iys+2,izc+2,k)%v(2) = f(ixc  ,iys+2,izc+2,k)%v(2) + wx0*ww
  f(ixc+1,iys+2,izc+2,k)%v(2) = f(ixc+1,iys+2,izc+2,k)%v(2) + wx1*ww
  f(ixc+2,iys+2,izc+2,k)%v(2) = f(ixc+2,iys+2,izc+2,k)%v(2) + wx2*ww


  !-----------------------------------------------------------------------------
  ! Particle y-flux
  www = fp(3)*w*dzm
  ww = wym*www
  f(ixc-1,iyc-1,izs-1,k)%v(3) = f(ixc-1,iyc-1,izs-1,k)%v(3) + wxm*ww
  f(ixc  ,iyc-1,izs-1,k)%v(3) = f(ixc  ,iyc-1,izs-1,k)%v(3) + wx0*ww
  f(ixc+1,iyc-1,izs-1,k)%v(3) = f(ixc+1,iyc-1,izs-1,k)%v(3) + wx1*ww
  f(ixc+2,iyc-1,izs-1,k)%v(3) = f(ixc+2,iyc-1,izs-1,k)%v(3) + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izs-1,k)%v(3) = f(ixc-1,iyc  ,izs-1,k)%v(3) + wxm*ww
  f(ixc  ,iyc  ,izs-1,k)%v(3) = f(ixc  ,iyc  ,izs-1,k)%v(3) + wx0*ww
  f(ixc+1,iyc  ,izs-1,k)%v(3) = f(ixc+1,iyc  ,izs-1,k)%v(3) + wx1*ww
  f(ixc+2,iyc  ,izs-1,k)%v(3) = f(ixc+2,iyc  ,izs-1,k)%v(3) + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izs-1,k)%v(3) = f(ixc-1,iyc+1,izs-1,k)%v(3) + wxm*ww
  f(ixc  ,iyc+1,izs-1,k)%v(3) = f(ixc  ,iyc+1,izs-1,k)%v(3) + wx0*ww
  f(ixc+1,iyc+1,izs-1,k)%v(3) = f(ixc+1,iyc+1,izs-1,k)%v(3) + wx1*ww
  f(ixc+2,iyc+1,izs-1,k)%v(3) = f(ixc+2,iyc+1,izs-1,k)%v(3) + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izs-1,k)%v(3) = f(ixc-1,iyc+2,izs-1,k)%v(3) + wxm*ww
  f(ixc  ,iyc+2,izs-1,k)%v(3) = f(ixc  ,iyc+2,izs-1,k)%v(3) + wx0*ww
  f(ixc+1,iyc+2,izs-1,k)%v(3) = f(ixc+1,iyc+2,izs-1,k)%v(3) + wx1*ww
  f(ixc+2,iyc+2,izs-1,k)%v(3) = f(ixc+2,iyc+2,izs-1,k)%v(3) + wx2*ww
  www = fp(3)*w*dz0
  ww = wym*www
  f(ixc-1,iyc-1,izs  ,k)%v(3) = f(ixc-1,iyc-1,izs  ,k)%v(3) + wxm*ww
  f(ixc  ,iyc-1,izs  ,k)%v(3) = f(ixc  ,iyc-1,izs  ,k)%v(3) + wx0*ww
  f(ixc+1,iyc-1,izs  ,k)%v(3) = f(ixc+1,iyc-1,izs  ,k)%v(3) + wx1*ww
  f(ixc+2,iyc-1,izs  ,k)%v(3) = f(ixc+2,iyc-1,izs  ,k)%v(3) + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izs  ,k)%v(3) = f(ixc-1,iyc  ,izs  ,k)%v(3) + wxm*ww
  f(ixc  ,iyc  ,izs  ,k)%v(3) = f(ixc  ,iyc  ,izs  ,k)%v(3) + wx0*ww
  f(ixc+1,iyc  ,izs  ,k)%v(3) = f(ixc+1,iyc  ,izs  ,k)%v(3) + wx1*ww
  f(ixc+2,iyc  ,izs  ,k)%v(3) = f(ixc+2,iyc  ,izs  ,k)%v(3) + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izs  ,k)%v(3) = f(ixc-1,iyc+1,izs  ,k)%v(3) + wxm*ww
  f(ixc  ,iyc+1,izs  ,k)%v(3) = f(ixc  ,iyc+1,izs  ,k)%v(3) + wx0*ww
  f(ixc+1,iyc+1,izs  ,k)%v(3) = f(ixc+1,iyc+1,izs  ,k)%v(3) + wx1*ww
  f(ixc+2,iyc+1,izs  ,k)%v(3) = f(ixc+2,iyc+1,izs  ,k)%v(3) + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izs  ,k)%v(3) = f(ixc-1,iyc+2,izs  ,k)%v(3) + wxm*ww
  f(ixc  ,iyc+2,izs  ,k)%v(3) = f(ixc  ,iyc+2,izs  ,k)%v(3) + wx0*ww
  f(ixc+1,iyc+2,izs  ,k)%v(3) = f(ixc+1,iyc+2,izs  ,k)%v(3) + wx1*ww
  f(ixc+2,iyc+2,izs  ,k)%v(3) = f(ixc+2,iyc+2,izs  ,k)%v(3) + wx2*ww
  www = fp(3)*w*dz1
  ww = wym*www
  f(ixc-1,iyc-1,izs+1,k)%v(3) = f(ixc-1,iyc-1,izs+1,k)%v(3) + wxm*ww
  f(ixc  ,iyc-1,izs+1,k)%v(3) = f(ixc  ,iyc-1,izs+1,k)%v(3) + wx0*ww
  f(ixc+1,iyc-1,izs+1,k)%v(3) = f(ixc+1,iyc-1,izs+1,k)%v(3) + wx1*ww
  f(ixc+2,iyc-1,izs+1,k)%v(3) = f(ixc+2,iyc-1,izs+1,k)%v(3) + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izs+1,k)%v(3) = f(ixc-1,iyc  ,izs+1,k)%v(3) + wxm*ww
  f(ixc  ,iyc  ,izs+1,k)%v(3) = f(ixc  ,iyc  ,izs+1,k)%v(3) + wx0*ww
  f(ixc+1,iyc  ,izs+1,k)%v(3) = f(ixc+1,iyc  ,izs+1,k)%v(3) + wx1*ww
  f(ixc+2,iyc  ,izs+1,k)%v(3) = f(ixc+2,iyc  ,izs+1,k)%v(3) + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izs+1,k)%v(3) = f(ixc-1,iyc+1,izs+1,k)%v(3) + wxm*ww
  f(ixc  ,iyc+1,izs+1,k)%v(3) = f(ixc  ,iyc+1,izs+1,k)%v(3) + wx0*ww
  f(ixc+1,iyc+1,izs+1,k)%v(3) = f(ixc+1,iyc+1,izs+1,k)%v(3) + wx1*ww
  f(ixc+2,iyc+1,izs+1,k)%v(3) = f(ixc+2,iyc+1,izs+1,k)%v(3) + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izs+1,k)%v(3) = f(ixc-1,iyc+2,izs+1,k)%v(3) + wxm*ww
  f(ixc  ,iyc+2,izs+1,k)%v(3) = f(ixc  ,iyc+2,izs+1,k)%v(3) + wx0*ww
  f(ixc+1,iyc+2,izs+1,k)%v(3) = f(ixc+1,iyc+2,izs+1,k)%v(3) + wx1*ww
  f(ixc+2,iyc+2,izs+1,k)%v(3) = f(ixc+2,iyc+2,izs+1,k)%v(3) + wx2*ww
  www = fp(3)*w*dz2
  ww = wym*www
  f(ixc-1,iyc-1,izs+2,k)%v(3) = f(ixc-1,iyc-1,izs+2,k)%v(3) + wxm*ww
  f(ixc  ,iyc-1,izs+2,k)%v(3) = f(ixc  ,iyc-1,izs+2,k)%v(3) + wx0*ww
  f(ixc+1,iyc-1,izs+2,k)%v(3) = f(ixc+1,iyc-1,izs+2,k)%v(3) + wx1*ww
  f(ixc+2,iyc-1,izs+2,k)%v(3) = f(ixc+2,iyc-1,izs+2,k)%v(3) + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izs+2,k)%v(3) = f(ixc-1,iyc  ,izs+2,k)%v(3) + wxm*ww
  f(ixc  ,iyc  ,izs+2,k)%v(3) = f(ixc  ,iyc  ,izs+2,k)%v(3) + wx0*ww
  f(ixc+1,iyc  ,izs+2,k)%v(3) = f(ixc+1,iyc  ,izs+2,k)%v(3) + wx1*ww
  f(ixc+2,iyc  ,izs+2,k)%v(3) = f(ixc+2,iyc  ,izs+2,k)%v(3) + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izs+2,k)%v(3) = f(ixc-1,iyc+1,izs+2,k)%v(3) + wxm*ww
  f(ixc  ,iyc+1,izs+2,k)%v(3) = f(ixc  ,iyc+1,izs+2,k)%v(3) + wx0*ww
  f(ixc+1,iyc+1,izs+2,k)%v(3) = f(ixc+1,iyc+1,izs+2,k)%v(3) + wx1*ww
  f(ixc+2,iyc+1,izs+2,k)%v(3) = f(ixc+2,iyc+1,izs+2,k)%v(3) + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izs+2,k)%v(3) = f(ixc-1,iyc+2,izs+2,k)%v(3) + wxm*ww
  f(ixc  ,iyc+2,izs+2,k)%v(3) = f(ixc  ,iyc+2,izs+2,k)%v(3) + wx0*ww
  f(ixc+1,iyc+2,izs+2,k)%v(3) = f(ixc+1,iyc+2,izs+2,k)%v(3) + wx1*ww
  f(ixc+2,iyc+2,izs+2,k)%v(3) = f(ixc+2,iyc+2,izs+2,k)%v(3) + wx2*ww
END SUBROUTINE
!-------------------------------------------------------------------------------
SUBROUTINE init_interpolation
  USE params, only : mid, dbg, mpi, stdout
  USE grid_m,   only : g
  USE interpolation
  implicit none
  character(len=mid) :: id = "Patch/lean/interpolation_cubic.f90 $Id$"

  ! toggle btw 'floor' and 'int' index calc - depending on z-coordinates on my node
  floormethod = (g%z(g%lb(3)).le.(1.50001*g%ds(3)))

  if (dbg>1 .and. stdout>0) then
    write(stdout,*) ' '
    write(stdout,*) 'mpi%me(3)        : ',mpi%me(3)
    write(stdout,*) 'floormethod?     : ',floormethod
    write(stdout,*) 'g%z(g%lb(3)      : ',g%z(g%lb(3))
    write(stdout,*) '(1.50001*g%ds(3)): ',(1.50001*g%ds(3))
    write(stdout,*) ' '
  endif
END SUBROUTINE
!-------------------------------------------------------------------------------
SUBROUTINE test_interpolation
  USE params, only : stdout, mpi
  USE grid_m,   only : g
  USE interpolation
  implicit none
  logical err
  integer iz
  real r(3)
!...............................................................................
  if (mpi%me(3) > 0) return
  write(stdout,*) 'TESTING INTERPOLATION:'
  write(stdout,*) '      r  iz1 ize1   wz0   wz1   wz2  wsz0  wsz1  wsz2'
  do iz=1,40
    r = (/0.1,0.1,g%rlb(3)+(iz-30)*0.1/)
    call makeweights (r, err)
    write(stdout,'(f8.2,2(i5,3f6.2))') r(3),izc,wz0,wz1,wz2,izs,dz0,dz1,dz2
  end do
END SUBROUTINE test_interpolation
!-------------------------------------------------------------------------------
