! Patch/lean/maxwell_solver.f90 $Id$
! vim: nowrap
!-----------------------------------------------------------------------!
MODULE maxwell_solver
  USE grid_m,          ONLY : g, o_ngrid                                ! "The Grid"
  USE units,           ONLY : c, elm                                    ! unit system constants.
  USE grid_m,          ONLY : odx, ody, odz, odxq, odyq, odzq,bx,by,bz  ! grid props
  USE params,          ONLY : nspecies, &                               ! src arrs, nr of species
                              mid, dt, &                                ! other params
                              out_namelists, trace, dbg, mdim, periodic ! debug levels
  implicit none
  real    :: tol_b, tol_e, tol_divb                                     ! tolerance limits
  integer :: maxiter_b, maxiter_e, miniter_e, iter_b, iter_e, &         ! tolerance controls
             miniter_divb, maxiter_divb, iter_divb, every_e, every_b

 !These are prefactors and variables needed for the integration sceheme - common to all finit diff orders.
  real    :: emf, omemf                                                 ! time de-centering params
  real    :: filter, sor, f_2nd, f_4th                                  ! checkerboard filter and SORelaxation parameter
  real    :: dd1e, dd1b                                                 ! prefactor for e_fix and b_fix
  real    :: ce1, ce2, ce3                                              ! e_push prefactors
  real    :: cb1, cb2, cb3                                              ! b_push prefactors
  real    :: c4bx, c4by, c4bz, denom                                    ! b_push prefactors

 !This is ported from the pic_stagger module.
  real    :: aa, bb, cc                                                 ! stagger operator prefactors
  real    :: a1
  real    :: ax1, ay1, az1                                              ! do.
  real    :: ax3, ay3, az3                                              ! do.
  real    :: bx3, by3, bz3                                              ! do.
  real    :: cx3, cy3, cz3                                              ! do.
  real    :: lp0, lpm1, lpm2, lpm3, lpp1, lpp2, lpp3                    ! LaPlace prefactors
  integer :: order=6                                                    ! sixth order
  integer :: lb(3), ub(3)                                               ! boundary indices
  logical :: do_explicit  
CONTAINS
!-----------------------------------------------------------------------!
SUBROUTINE maxwell_stagger_prefactors                                   ! compute prefactors for the stagger operators, they depend on order in finite diff.
USE grid_m,           only : g
  implicit none

  integer :: i
  real    :: dx, dy, dz

   !----------------
    dx = g%ds(1)
    dy = g%ds(2)
    dz = g%ds(3)

    cc=3./256.   ; cx3=(-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3.))
    bb=-25./256. ; cy3=(-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3.))
    aa=.5-bb-cc    ; cz3=(-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3.))
    bx3=(-1.-120.*cx3)/24.    ; by3=(-1.-120.*cy3)/24.    ; bz3=(-1.-120.*cz3)/24.
    ax3=(1.-3.*bx3-5.*cx3)/dx ; ay3=(1.-3.*by3-5.*cy3)/dy ; az3=(1.-3.*bz3-5.*cz3)/dz
    bx3=bx3/dx                ; by3=by3/dy                ; bz3=bz3/dz
    cx3=cx3/dx                ; cy3=cy3/dy                ; cz3=cz3/dz

    lpm3 = +   2./180.
    lpm2 = -  27./180.
    lpm1 = + 270./180.
    lp0  = - 490./180.
    lpp1 = + 270./180.
    lpp2 = -  27./180.
    lpp3 = +   2./180.
   !----------------
  
END SUBROUTINE maxwell_stagger_prefactors
!-----------------------------------------------------------------------!
FUNCTION ddxup (f,i,j,k)
  USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddxup
  integer                                            :: i, j, k

     ddxup = (  ax3*(f(i+1 ,j,k)-f(i   ,j,k)) &
              + bx3*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
              + cx3*(f(i+3 ,j,k)-f(i-2 ,j,k))  )
END FUNCTION ddxup
!-----------------------------------------------------------------------!
FUNCTION ddxdn(f,i,j,k)
  USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddxdn
  integer                                            :: mx, my, mz
  integer                                            :: i, j, k

      ddxdn = (  ax3*(f(i   ,j,k)-f(i-1 ,j,k)) &
               + bx3*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
               + cx3*(f(i+2 ,j,k)-f(i-3 ,j,k))  )
END FUNCTION ddxdn
!-----------------------------------------------------------------------!
FUNCTION ddyup (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddyup
  integer                                            :: i, j, k

    ddyup = (  ay3*(f(i,j+1 ,k)-f(i,j   ,k)) &
             + by3*(f(i,j+2 ,k)-f(i,j-1 ,k)) &
             + cy3*(f(i,j+3 ,k)-f(i,j-2 ,k))  )
END FUNCTION ddyup
!-----------------------------------------------------------------------!
FUNCTION ddydn (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddydn
  integer                                            :: i, j, k

      ddydn = (  ay3*(f(i,j   ,k)-f(i,j-1 ,k)) &
               + by3*(f(i,j+1 ,k)-f(i,j-2 ,k)) &
               + cy3*(f(i,j+2 ,k)-f(i,j-3 ,k)))
END FUNCTION ddydn
!-----------------------------------------------------------------------!
FUNCTION ddzup (f,i,j,k)
  USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddzup
  integer                                            :: i, j, k

    ddzup  = (  az3*(f(i,j,k+1 )-f(i,j,k   )) &
              + bz3*(f(i,j,k+2 )-f(i,j,k-1 )) &
              + cz3*(f(i,j,k+3 )-f(i,j,k-2 ))  )
END FUNCTION ddzup
!-----------------------------------------------------------------------!
FUNCTION ddzdn (f,i,j,k)
  USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddzdn
  integer                                            :: i, j, k

    ddzdn  = (  az3*(f(i,j,k   )-f(i,j,k-1 )) &
              + bz3*(f(i,j,k+1 )-f(i,j,k-2 )) &
              + cz3*(f(i,j,k+2 )-f(i,j,k-3 ))  )
END FUNCTION ddzdn

!=======================================================================!
SUBROUTINE b_push
  USE params,          ONLY: stdout, master, time, stdall, mpi, &
                             do_b_fix, it, dt
  USE grid_m,          ONLY: ex, ey, ez    ! fields needed
  USE grid_m,          ONLY: bx, by, bz    ! more fields needed
  USE maxwell_sources, ONLY: verbose
  USE maxwell_sources, ONLY: bxa, bya, bza, bxm, bym, bzm
  USE dumps,           ONLY: dump
  USE math,            ONLY: divstat
  USE pic_stagger,     ONLY: curlxup, curlyup, curlzup
  implicit none
  logical, save :: calldivb=.true.
  integer ::  iz
  real maxerrp
!.......................................................................!
  call trace_enter('B_PUSH')
!-----------------------------------------------------------------------!
! Step the B-field
!-----------------------------------------------------------------------!
  lb =       g%lb               
  ub = merge(g%ub, g%ub-1, mpi%ub)                                      ! proper boundaries

  if (do_explicit) then
    bxm = curlxup (ez, ey)
    bym = curlyup (ex, ez)
    bzm = curlzup (ey, ex)
    do iz=lb(3),ub(3)
           bx(lb(1):ub(1),lb(2):ub(2),iz) = & 
           bx(lb(1):ub(1),lb(2):ub(2),iz) - &
      cb1*bxm(lb(1):ub(1),lb(2):ub(2),iz)

           by(lb(1):ub(1),lb(2):ub(2),iz) = & 
           by(lb(1):ub(1),lb(2):ub(2),iz) - &
      cb1*bym(lb(1):ub(1),lb(2):ub(2),iz)

           bz(lb(1):ub(1),lb(2):ub(2),iz) = & 
           bz(lb(1):ub(1),lb(2):ub(2),iz) - &
      cb1*bzm(lb(1):ub(1),lb(2):ub(2),iz)
    end do
  else
    call b_push_implicit
  endif
!-----------------------------------------------------------------------!
! Set boundary components of b-field
!-----------------------------------------------------------------------!
  call b_boundaries(bx,by,bz)
                                                      call dump(bx,'Bx')
                                                      call dump(by,'By')
                                                      call dump(bz,'Bz')
!-----------------------------------------------------------------------!
! Possibly check div(B) for roundoff error build-up
!-----------------------------------------------------------------------!
  iter_divb = 0
  if (calldivb) call divstat ('B', bx, by, bz)
  if (do_b_fix .and. mod(it,every_b)==0) then 
    call b_fix (bx, by, bz)
    if (calldivb) call divstat ('B(fixed)', bx, by, bz)
  endif
  calldivb = .false.

  call trace_exit('B_PUSH')
END SUBROUTINE b_push

!=======================================================================!
SUBROUTINE e_push
  USE params,          ONLY: stdout, master, time, stdall, mpi, &
                             do_e_fix, it, dt
  USE grid_m,          ONLY: ex, ey, ez    ! fields needed
  USE grid_m,          ONLY: bx, by, bz    ! more fields needed
  USE maxwell_sources, ONLY: verbose
  USE maxwell_sources, ONLY: sx, sy, sz, jx, jy, jz, charge
  USE maxwell_sources, only: exa=>bxm, eya=>bym, eza=>bzm
  USE dumps,           ONLY: dump
  USE math,            ONLY: divstat
  USE pic_stagger,     ONLY: curlxdn, curlydn, curlzdn
  USE stat,            ONLY: energy_e_box, prefe
  implicit none
  logical, save :: calldivb=.true.
  integer ::  iz
  real(kind=8) :: total3
  real maxerrp
!.......................................................................!
  call trace_enter('E_PUSH')
!-----------------------------------------------------------------------!
! Step the E-field
!-----------------------------------------------------------------------!
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)

  do iz=lb(3),ub(3)
    exa(:,:,iz) = ex(:,:,iz)                                            ! store current ..
    eya(:,:,iz) = ey(:,:,iz)                                            ! .. values for ..
    eza(:,:,iz) = ez(:,:,iz)                                            ! .. Io/stat.f90
  end do

  if (do_explicit) then
    sx = curlxdn (bz, by)
    sy = curlydn (bx, bz)
    sz = curlzdn (by, bx)
    do iz=lb(3),ub(3)
           ex(lb(1):ub(1),lb(2):ub(2),iz) = & 
           ex(lb(1):ub(1),lb(2):ub(2),iz) + &
       ce1*sx(lb(1):ub(1),lb(2):ub(2),iz) - &
       ce2*jx(lb(1):ub(1),lb(2):ub(2),iz)
        
           ey(lb(1):ub(1),lb(2):ub(2),iz) = & 
           ey(lb(1):ub(1),lb(2):ub(2),iz) + &
       ce1*sy(lb(1):ub(1),lb(2):ub(2),iz) - &
       ce2*jy(lb(1):ub(1),lb(2):ub(2),iz)
      
           ez(lb(1):ub(1),lb(2):ub(2),iz) = & 
           ez(lb(1):ub(1),lb(2):ub(2),iz) + &
       ce1*sz(lb(1):ub(1),lb(2):ub(2),iz) - &
       ce2*jz(lb(1):ub(1),lb(2):ub(2),iz)
    end do
  else
    call e_push_implicit
  endif

  do iz=lb(3),ub(3)
    sx(:,:,iz) = (ex(:,:,iz)+exa(:,:,iz))**2 &
               + (ey(:,:,iz)+eya(:,:,iz))**2 &
               + (ez(:,:,iz)+eza(:,:,iz))**2
  end do
  energy_e_box = 0.25*prefe*total3(sx)*g%dV

!-----------------------------------------------------------------------!
! Set boundary components of E-field
!-----------------------------------------------------------------------!
  call e_boundaries (ex, ey, ez)
                                                      call dump(ex,'Ex')
                                                      call dump(ey,'Ey')
                                                      call dump(ez,'Ez')
!-----------------------------------------------------------------------!
! Possibly check div(E) for roundoff error build-up
!-----------------------------------------------------------------------!
  if (do_e_fix .and. mod(it,every_e)==0) then 
    call e_fix (ex, ey, ez, charge)
  endif

  call trace_exit('E_PUSH')
END SUBROUTINE e_push

!=======================================================================!
SUBROUTINE b_push_implicit
  USE params,              ONLY : mpi, rank, stdout, master, do_b_fix
  USE grid_m,                ONLY : g, ex, ey, ez                       ! grid needed, fields needed
  USE maxwell_sources,     ONLY : jx,jy,jz,bxm,bym,bzm
  USE maxwell_sources,     ONLY : bxa,bya,bza,sx,sy,sz                  ! help fields B{t-1} and S -> 'solve'
  USE maxwell_sources,     ONLY : bmax, verbose
  USE dumps,               ONLY : dump
  USE math,                ONLY : divstat

  implicit none

  real, dimension(:,:,:), allocatable   :: bsqr

  logical, save :: calldivb=.true.

  integer       :: iter
  real          :: errp, aver3, maxerr, b0e, errpp, maxerrp
  real          :: max_scalar
  integer       :: ix, iy, iz
 
  real, external :: max4

  call trace_enter('B_PUSH')

!----------------------------------------------------------------------!
!Reset - then construct - current density on interior.
  ub = merge(g%ub, g%ub-1, mpi%ub)

!----------------------------------------------------------------------!
!Now calculate Sx,Sy,Sz on interior only - for the relaxation solve. 

  do iz = g%lb(3),ub(3)
  do iy = g%lb(2),ub(2)
  do ix = g%lb(1),ub(1)
    sx(ix,iy,iz) = bx(ix,iy,iz)                                                        &
        - cb1 * (                                       &
            (  ay3*(ez(ix,iy+1 ,iz)-ez(ix,iy   ,iz))    & ! ddyup(ez)
             + by3*(ez(ix,iy+2 ,iz)-ez(ix,iy-1 ,iz))    &
             + cy3*(ez(ix,iy+3 ,iz)-ez(ix,iy-2 ,iz))  ) &
          - (  az3*(ey(ix,iy,iz+1 )-ey(ix,iy,iz   ))    & ! ddzup(ey)
             + bz3*(ey(ix,iy,iz+2 )-ey(ix,iy,iz-1 ))    &
             + cz3*(ey(ix,iy,iz+3 )-ey(ix,iy,iz-2 ))  ))&
        + cb2 * (                                       &
            (  ay3*(jz(ix,iy+1 ,iz)-jz(ix,iy   ,iz))    & ! ddyup(jz)
             + by3*(jz(ix,iy+2 ,iz)-jz(ix,iy-1 ,iz))    &
             + cy3*(jz(ix,iy+3 ,iz)-jz(ix,iy-2 ,iz))  ) &
          - (  az3*(jy(ix,iy,iz+1 )-jy(ix,iy,iz   ))    & ! ddzup(jy)
             + bz3*(jy(ix,iy,iz+2 )-jy(ix,iy,iz-1 ))    &
             + cz3*(jy(ix,iy,iz+3 )-jy(ix,iy,iz-2 ))  ))&
        + cb3 * (                       &
               (  lpm3 * bx(ix-3,iy,iz) &                 ! d2dx2cen(bx) 
                + lpm2 * bx(ix-2,iy,iz) &
                + lpm1 * bx(ix-1,iy,iz) &
                +  lp0 * bx(ix  ,iy,iz) &
                + lpp1 * bx(ix+1,iy,iz) &
                + lpp2 * bx(ix+2,iy,iz) &
                + lpp3 * bx(ix+3,iy,iz)  ) * odxq &
             + (  lpm3 * bx(ix,iy-3,iz) &                 ! d2dy2cen(bx) 
                + lpm2 * bx(ix,iy-2,iz) &
                + lpm1 * bx(ix,iy-1,iz) &
                +  lp0 * bx(ix,iy  ,iz) &
                + lpp1 * bx(ix,iy+1,iz) &
                + lpp2 * bx(ix,iy+2,iz) &
                + lpp3 * bx(ix,iy+3,iz)  ) * odyq &
             + (  lpm3 * bx(ix,iy,iz-3) &                 ! d2dz2cen(bx) 
                + lpm2 * bx(ix,iy,iz-2) &
                + lpm1 * bx(ix,iy,iz-1) &
                +  lp0 * bx(ix,iy,iz  ) &
                + lpp1 * bx(ix,iy,iz+1) &
                + lpp2 * bx(ix,iy,iz+2) &
                + lpp3 * bx(ix,iy,iz+3)  ) * odzq )
  enddo
  enddo
  enddo 

  do iz = g%lb(3),ub(3)
  do iy = g%lb(2),ub(2)
  do ix = g%lb(1),ub(1)
    sy(ix,iy,iz) = by(ix,iy,iz)                          &
        - cb1 * (                                        &
             (  az3*(ex(ix,iy,iz+1 )-ex(ix,iy,iz   ))    & ! ddzup(ex)
              + bz3*(ex(ix,iy,iz+2 )-ex(ix,iy,iz-1 ))    &
              + cz3*(ex(ix,iy,iz+3 )-ex(ix,iy,iz-2 ))  ) &
           - (  ax3*(ez(ix+1 ,iy,iz)-ez(ix   ,iy,iz))    & ! ddxup(ez)
              + bx3*(ez(ix+2 ,iy,iz)-ez(ix-1 ,iy,iz))    &
              + cx3*(ez(ix+3 ,iy,iz)-ez(ix-2 ,iy,iz))  ))&
        + cb2 * (                                        &
             (  az3*(jx(ix,iy,iz+1 )-jx(ix,iy,iz   ))    & ! ddzup(jx)
              + bz3*(jx(ix,iy,iz+2 )-jx(ix,iy,iz-1 ))    &
              + cz3*(jx(ix,iy,iz+3 )-jx(ix,iy,iz-2 ))  ) &
           - (  ax3*(jz(ix+1 ,iy,iz)-jz(ix   ,iy,iz))    & ! ddxup(jz)
              + bx3*(jz(ix+2 ,iy,iz)-jz(ix-1 ,iy,iz))    &
              + cx3*(jz(ix+3 ,iy,iz)-jz(ix-2 ,iy,iz))  ))&
        + cb3 * (                       &
               (  lpm3 * by(ix-3,iy,iz) &                 ! d2dx2cen(by) 
                + lpm2 * by(ix-2,iy,iz) &
                + lpm1 * by(ix-1,iy,iz) &
                +  lp0 * by(ix  ,iy,iz) &
                + lpp1 * by(ix+1,iy,iz) &
                + lpp2 * by(ix+2,iy,iz) &
                + lpp3 * by(ix+3,iy,iz)  ) * odxq &
             + (  lpm3 * by(ix,iy-3,iz) &                 ! d2dy2cen(by) 
                + lpm2 * by(ix,iy-2,iz) &
                + lpm1 * by(ix,iy-1,iz) &
                +  lp0 * by(ix,iy  ,iz) &
                + lpp1 * by(ix,iy+1,iz) &
                + lpp2 * by(ix,iy+2,iz) &
                + lpp3 * by(ix,iy+3,iz)  ) * odyq &
             + (  lpm3 * by(ix,iy,iz-3) &                 ! d2dz2cen(by) 
                + lpm2 * by(ix,iy,iz-2) &
                + lpm1 * by(ix,iy,iz-1) &
                +  lp0 * by(ix,iy,iz  ) &
                + lpp1 * by(ix,iy,iz+1) &
                + lpp2 * by(ix,iy,iz+2) &
                + lpp3 * by(ix,iy,iz+3)  ) * odzq )
  enddo
  enddo
  enddo 

  do iz = g%lb(3),ub(3)
  do iy = g%lb(2),ub(2)
  do ix = g%lb(1),ub(1)
    sz(ix,iy,iz) = bz(ix,iy,iz)                                                        &
        - cb1 * (                                       &
             (  ax3*(ey(ix+1 ,iy,iz)-ey(ix   ,iy,iz))   & ! ddxdup(ey)
              + bx3*(ey(ix+2 ,iy,iz)-ey(ix-1 ,iy,iz))   &
              + cx3*(ey(ix+3 ,iy,iz)-ey(ix-2 ,iy,iz)) ) &
          -  (  ay3*(ex(ix,iy+1 ,iz)-ex(ix,iy   ,iz))   & ! ddyup(ex)
              + by3*(ex(ix,iy+2 ,iz)-ex(ix,iy-1 ,iz))   &
              + cy3*(ex(ix,iy+3 ,iz)-ex(ix,iy-2 ,iz)) ))&
        + cb2 * (                                       &
             (  ax3*(jy(ix+1 ,iy,iz)-jy(ix   ,iy,iz))   & ! ddxdup(jy)
              + bx3*(jy(ix+2 ,iy,iz)-jy(ix-1 ,iy,iz))   &
              + cx3*(jy(ix+3 ,iy,iz)-jy(ix-2 ,iy,iz)) ) &
          -  (  ay3*(jx(ix,iy+1 ,iz)-jx(ix,iy   ,iz))   & ! ddyup(jx)
              + by3*(jx(ix,iy+2 ,iz)-jx(ix,iy-1 ,iz))   &
              + cy3*(jx(ix,iy+3 ,iz)-jx(ix,iy-2 ,iz)) ))&
        + cb3 * (                       &
               (  lpm3 * bz(ix-3,iy,iz) &                 ! d2dx2cen(bz) 
                + lpm2 * bz(ix-2,iy,iz) &
                + lpm1 * bz(ix-1,iy,iz) &
                +  lp0 * bz(ix  ,iy,iz) &
                + lpp1 * bz(ix+1,iy,iz) &
                + lpp2 * bz(ix+2,iy,iz) &
                + lpp3 * bz(ix+3,iy,iz)  ) * odxq &
             + (  lpm3 * bz(ix,iy-3,iz) &                 ! d2dy2cen(bz) 
                + lpm2 * bz(ix,iy-2,iz) &
                + lpm1 * bz(ix,iy-1,iz) &
                +  lp0 * bz(ix,iy  ,iz) &
                + lpp1 * bz(ix,iy+1,iz) &
                + lpp2 * bz(ix,iy+2,iz) &
                + lpp3 * bz(ix,iy+3,iz)  ) * odyq &
             + (  lpm3 * bz(ix,iy,iz-3) &                 ! d2dz2cen(bz) 
                + lpm2 * bz(ix,iy,iz-2) &
                + lpm1 * bz(ix,iy,iz-1) &
                +  lp0 * bz(ix,iy,iz  ) &
                + lpp1 * bz(ix,iy,iz+1) &
                + lpp2 * bz(ix,iy,iz+2) &
                + lpp3 * bz(ix,iy,iz+3)  ) * odzq )
  enddo
  enddo
  enddo 
!-----------------------------------------------------------------------!
!Find, compute and print information about error level
  do iz=1,g%n(3)
    bxm(:,:,iz) = bx(:,:,iz)**2+by(:,:,iz)**2+bz(:,:,iz)**2
  end do

  b0e   = 0.
  call b_boundaries (sx, sy, sz)
  bmax = sqrt(max4(bxm))
                               if (verbose>1) then
                                 allocate(bsqr(g%n(1),g%n(2),g%n(3)))
                                 bsqr = sqrt(bxm)
                                 b0e  = aver3(bsqr)
                                 write(stdout,*) rank,bmax,b0e,' xxx'
                                 deallocate(bsqr)
                               endif
                                                      call dump(sx,'sx')
                                                      call dump(sy,'sy')
                                                      call dump(sz,'sz')

!-----------------------------------------------------------------------!
!Memorize 0'th iteration Ba for the loop and error check.
  do iz=1,g%n(3)
  do iy=1,g%n(2)
  do ix=1,g%n(1)
    bxa(ix,iy,iz) = bx(ix,iy,iz)
    bya(ix,iy,iz) = by(ix,iy,iz)
    bza(ix,iy,iz) = bz(ix,iy,iz)
  end do
  end do
  end do
                                                     call dump(bx,'Bxm')
                                                     call dump(by,'Bym')
                                                     call dump(bz,'Bzm')
!-----------------------------------------------------------------------!
!Now move, NB!  Bza -- e.g. -- is defined on {lb-1:ub},{lb-1:ub},{lb-1:ub+1}

  iter = 0                                                             ! reset number of iterations for this timestep

SOLVE_LOOP : do while(iter.le.maxiter_b)                               ! loop only maxiter_b times.

  do iz = g%lb(3),ub(3)
  do iy = g%lb(2),ub(2)
  do ix = g%lb(1),ub(1)
      bxm(ix,iy,iz)=(sx(ix,iy,iz)                                       &
                                  + c4bx * (  lpm3 * bxa(ix-3,iy  ,iz  )&
                                            + lpm2 * bxa(ix-2,iy  ,iz  )&
                                            + lpm1 * bxa(ix-1,iy  ,iz  )&
                                            + lpp1 * bxa(ix+1,iy  ,iz  )&
                                            + lpp2 * bxa(ix+2,iy  ,iz  )&
                                            + lpp3 * bxa(ix+3,iy  ,iz  ))&
                                  + c4by * (  lpm3 * bxa(ix  ,iy-3,iz  )&
                                            + lpm2 * bxa(ix  ,iy-2,iz  )&
                                            + lpm1 * bxa(ix  ,iy-1,iz  )&
                                            + lpp1 * bxa(ix  ,iy+1,iz  )&
                                            + lpp2 * bxa(ix  ,iy+2,iz  )&
                                            + lpp3 * bxa(ix  ,iy+3,iz  ))&
                                  + c4bz * (  lpm3 * bxa(ix  ,iy  ,iz-3)&
                                            + lpm2 * bxa(ix  ,iy  ,iz-2)&
                                            + lpm1 * bxa(ix  ,iy  ,iz-1)&
                                            + lpp1 * bxa(ix  ,iy  ,iz+1)&
                                            + lpp2 * bxa(ix  ,iy  ,iz+2)&
                                            + lpp3 * bxa(ix  ,iy  ,iz+3))&
                                                                          )*denom
  enddo
  enddo
  enddo

  do iz = g%lb(3),ub(3)
  do iy = g%lb(2),ub(2)
  do ix = g%lb(1),ub(1)
      bym(ix,iy,iz)=(sy(ix,iy,iz)                                       &
                                  + c4bx * (  lpm3 * bya(ix-3,iy  ,iz  )&
                                            + lpm2 * bya(ix-2,iy  ,iz  )&
                                            + lpm1 * bya(ix-1,iy  ,iz  )&
                                            + lpp1 * bya(ix+1,iy  ,iz  )&
                                            + lpp2 * bya(ix+2,iy  ,iz  )&
                                            + lpp3 * bya(ix+3,iy  ,iz  ))&
                                  + c4by * (  lpm3 * bya(ix  ,iy-3,iz  )&
                                            + lpm2 * bya(ix  ,iy-2,iz  )&
                                            + lpm1 * bya(ix  ,iy-1,iz  )&
                                            + lpp1 * bya(ix  ,iy+1,iz  )&
                                            + lpp2 * bya(ix  ,iy+2,iz  )&
                                            + lpp3 * bya(ix  ,iy+3,iz  ))&
                                  + c4bz * (  lpm3 * bya(ix  ,iy  ,iz-3)&
                                            + lpm2 * bya(ix  ,iy  ,iz-2)&
                                            + lpm1 * bya(ix  ,iy  ,iz-1)&
                                            + lpp1 * bya(ix  ,iy  ,iz+1)&
                                            + lpp2 * bya(ix  ,iy  ,iz+2)&
                                            + lpp3 * bya(ix  ,iy  ,iz+3))&
                                                                          )*denom
  enddo
  enddo
  enddo

  do iz = g%lb(3),ub(3)
  do iy = g%lb(2),ub(2)
  do ix = g%lb(1),ub(1)
      bzm(ix,iy,iz)=(sz(ix,iy,iz)                                       &
                                  + c4bx * (  lpm3 * bza(ix-3,iy  ,iz  )&
                                            + lpm2 * bza(ix-2,iy  ,iz  )&
                                            + lpm1 * bza(ix-1,iy  ,iz  )&
                                            + lpp1 * bza(ix+1,iy  ,iz  )&
                                            + lpp2 * bza(ix+2,iy  ,iz  )&
                                            + lpp3 * bza(ix+3,iy  ,iz  ))&
                                  + c4by * (  lpm3 * bza(ix  ,iy-3,iz  )&
                                            + lpm2 * bza(ix  ,iy-2,iz  )&
                                            + lpm1 * bza(ix  ,iy-1,iz  )&
                                            + lpp1 * bza(ix  ,iy+1,iz  )&
                                            + lpp2 * bza(ix  ,iy+2,iz  )&
                                            + lpp3 * bza(ix  ,iy+3,iz  ))&
                                  + c4bz * (  lpm3 * bza(ix  ,iy  ,iz-3)&
                                            + lpm2 * bza(ix  ,iy  ,iz-2)&
                                            + lpm1 * bza(ix  ,iy  ,iz-1)&
                                            + lpp1 * bza(ix  ,iy  ,iz+1)&
                                            + lpp2 * bza(ix  ,iy  ,iz+2)&
                                            + lpp3 * bza(ix  ,iy  ,iz+3))&
                                                                          )*denom
  enddo
  enddo
  enddo

!-----------------------------------------------------------------------!
!Supply boundary conditions for B{xyz}m and for B{xyz} - same as above.
  call b_boundaries( bxm, bym, bzm)
                                                    call dump(bxm,'Bxh')
                                                    call dump(bym,'Byh')
                                                    call dump(bzm,'Bzh')
!-----------------------------------------------------------------------!
! Produce max error on grid on interior.
  errp = 0. ; maxerr = 0.
  errpp = 0. ; maxerrp = 0.

  do iz = g%lb(3),g%ub(3)-1
  do iy = g%lb(2),g%ub(2)-1
  do ix = g%lb(1),g%ub(1)-1
      errpp = max(errpp,& 
                 abs(bxm(ix,iy,iz)-bxa(ix,iy,iz)),&
                 abs(bym(ix,iy,iz)-bya(ix,iy,iz)),&
                 abs(bzm(ix,iy,iz)-bza(ix,iy,iz))) 
    maxerrp = max(maxerrp,&                                                    
                 abs(bxm(ix,iy,iz)),&
                 abs(bym(ix,iy,iz)),&
                 abs(bzm(ix,iy,iz))) 
  enddo
  enddo
  enddo
  maxerr = max(maxerr,maxerrp)
  errp   = max(errp,errpp)

  maxerr = max(maxerr,1e-34)
  errp = max_scalar(errp)
  maxerr = max_scalar(maxerr)
  errp = errp/maxerr

!----------------------------------------------------------------------!
!Convergence check to not necessarily do max allowed # iterations.
  if (master .and. verbose > 1) then 
    write(stdout,*) 'errp,maxerr,iter_b =',rank,errp,maxerr,iter       ! write some diagnostics - on demand
  endif

  iter_b = iter

  if (errp.lt.tol_b) then                                              ! we're done perhaps?
    if (master .and. verbose > 0) then                                 ! write-it ...
      write (stdout,*) 'Exiting; b_push CONVERGED - step ok.'
      write (stdout,*) ' errp     = ',errp
      write (stdout,*) ' tol_b    = ',tol_b
      write (stdout,*) ' iter     = ',iter
    end if
   EXIT SOLVE_LOOP                                                     ! ... exit solve loop and continue program.
  endif

!----------------------------------------------------------------------!
!Memorize last move iteration - same as above.
!We must know Bmem field on: Bxa: {lb-1:ub+1}{lb-1:ub}{lb-1:ub}
!                            Bya: {lb-1:ub}{lb-1:ub+1}{lb-1:ub}
!                            Bza: {lb-1:ub}{lb-1:ub}{lb-1:ub+1}
!to be able to form the operator Nabla**2(Bxa,Bya,Bza).
  do iz=1,g%n(3)
  do iy=1,g%n(2)
  do ix=1,g%n(1)
    bxa(ix,iy,iz) = bxm(ix,iy,iz)
    bya(ix,iy,iz) = bym(ix,iy,iz)
    bza(ix,iy,iz) = bzm(ix,iy,iz)
  end do
  end do
  end do

  iter=iter+1                                                          ! Next iteration if not done yet.

ENDDO SOLVE_LOOP                                                       ! end of loop for solving for LaPlace(B).

  if(errp.ge.tol_b) then                                               ! Warn us if we're not below error tolerance after max iterations.
    call warning('b_push','Max iterations reached, no convergence')
  endif

!----------------------------------------------------------------------!
!Here we are hopefully done; Reassign the magnetic field on INTERIOR
  if (calldivb) then
    call divstat ('B', bx, by, bz)
  endif   
  if (do_b_fix) then
    call b_fix (bxm, bym, bzm)
  endif
  if (calldivb) then 
    call divstat ('B(fixed)', bx, by, bz)
    calldivb=.false.
  endif   

!----------------------------------------------------------------------!
!Save the B-field before updating B to B(i+1);
! needed in 'e_push' for time-averaging B(i+1)-B(i).

  do iz=1,g%n(3)
  do iy=1,g%n(2)
  do ix=1,g%n(1)
    bxa(ix,iy,iz) = bx(ix,iy,iz)
    bx(ix,iy,iz) = bxm(ix,iy,iz)
    bya(ix,iy,iz) = by(ix,iy,iz)
    by(ix,iy,iz) = bym(ix,iy,iz)
    bza(ix,iy,iz) = bz(ix,iy,iz)
    bz(ix,iy,iz) = bzm(ix,iy,iz)
  end do
  end do
  end do

  call trace_exit('B_PUSH')
END SUBROUTINE b_push_implicit

!=======================================================================!
SUBROUTINE e_push_implicit
  USE params,               ONLY : stdout, master, time, stdall, mpi, do_e_fix, it
  USE grid_m,               ONLY : ex, ey, ez    ! fields needed
  USE grid_m,               ONLY : bx, by, bz    ! more fields needed
  USE grid_m,               ONLY : fskin, fmhd   ! increase dt
  USE maxwell_sources,      ONLY : jx, jy, jz    ! current density needed
  USE maxwell_sources,      ONLY : bxa,bya,bza   ! last B{t-1} needed
  USE maxwell_sources,      ONLY : calc_charge, charge, verbose
  USE dumps,                ONLY : dump

  implicit none
  logical :: convergence=.false.                                          ! e_fix convergence flag
  integer ::  ix, iy, iz, ii
  real    :: de(3)
  real    :: dphi, phitemp, chgmax, a, norm, emax
  real    :: max_scalar, max_dphi

  call trace_enter('E_PUSH')

!-----------------------------------------------------------------------
!Now push the whole interior grid.
  ub = merge(g%ub, g%ub-1, mpi%ub)

  do iz=g%lb(3),ub(3)
  do iy=g%lb(2),ub(2)
  do ix=g%lb(1),ub(1)
    ex(ix,iy,iz) = ex(ix,iy,iz)                                           &
                 + ce1 * (emf*(                                           &
                            (  ay3*(bz (ix,iy   ,iz)-bz (ix,iy-1 ,iz))    & ! ddydn(bz)
                             + by3*(bz (ix,iy+1 ,iz)-bz (ix,iy-2 ,iz))    &
                             + cy3*(bz (ix,iy+2 ,iz)-bz (ix,iy-3 ,iz)) )  &
                          - (  az3*(by (ix,iy,iz   )-by (ix,iy,iz-1 ))    & ! ddzdn(by)
                             + bz3*(by (ix,iy,iz+1 )-by (ix,iy,iz-2 ))    &
                             + cz3*(by (ix,iy,iz+2 )-by (ix,iy,iz-3 )) )) &
                      + omemf*(                                           &
                            (  ay3*(bza(ix,iy   ,iz)-bza(ix,iy-1 ,iz))    & ! ddydn(bza)
                             + by3*(bza(ix,iy+1 ,iz)-bza(ix,iy-2 ,iz))    &
                             + cy3*(bza(ix,iy+2 ,iz)-bza(ix,iy-3 ,iz)) )  &
                          - (  az3*(bya(ix,iy,iz   )-bya(ix,iy,iz-1 ))    & ! ddzdn(bya)
                             + bz3*(bya(ix,iy,iz+1 )-bya(ix,iy,iz-2 ))    &
                             + cz3*(bya(ix,iy,iz+2 )-bya(ix,iy,iz-3 )) )) &
                 - ce2 * (jx(ix,iy,iz)))
  enddo
  enddo
  enddo

  do iz=g%lb(3),ub(3)
  do iy=g%lb(2),ub(2)
  do ix=g%lb(1),ub(1)
    ey(ix,iy,iz) = ey(ix,iy,iz)                                           &
                 + ce1 * (emf*(                                           &
                            (  az3*(bx (ix,iy,iz   )-bx (ix,iy,iz-1 ))    & ! ddzdn(bx)
                             + bz3*(bx (ix,iy,iz+1 )-bx (ix,iy,iz-2 ))    &
                             + cz3*(bx (ix,iy,iz+2 )-bx (ix,iy,iz-3 )) )  &
                          - (  ax3*(bz (ix   ,iy,iz)-bz (ix-1 ,iy,iz))    & ! ddxdn(bz)
                             + bx3*(bz (ix+1 ,iy,iz)-bz (ix-2 ,iy,iz))    &
                             + cx3*(bz (ix+2 ,iy,iz)-bz (ix-3 ,iy,iz)) )) &
                      + omemf*(                                           &
                            (  az3*(bxa(ix,iy,iz   )-bxa(ix,iy,iz-1 ))    & ! ddzdn(bxa)
                             + bz3*(bxa(ix,iy,iz+1 )-bxa(ix,iy,iz-2 ))    &
                             + cz3*(bxa(ix,iy,iz+2 )-bxa(ix,iy,iz-3 )) )  &
                          - (  ax3*(bza(ix   ,iy,iz)-bza(ix-1 ,iy,iz))    & ! ddxdn(bza)
                             + bx3*(bza(ix+1 ,iy,iz)-bza(ix-2 ,iy,iz))    &
                             + cx3*(bza(ix+2 ,iy,iz)-bza(ix-3 ,iy,iz)) )) &
                 - ce2 * (jy(ix,iy,iz)))
  enddo
  enddo
  enddo

  do iz=g%lb(3),ub(3)
  do iy=g%lb(2),ub(2)
  do ix=g%lb(1),ub(1)
    ez(ix,iy,iz) = ez(ix,iy,iz)                                           &
                 + ce1 * (emf*(                                           &
                            (  ax3*(by (ix   ,iy,iz)-by (ix-1 ,iy,iz))    & ! ddxdn(by)
                             + bx3*(by (ix+1 ,iy,iz)-by (ix-2 ,iy,iz))    &
                             + cx3*(by (ix+2 ,iy,iz)-by (ix-3 ,iy,iz)) )  &
                          - (  ay3*(bx (ix,iy   ,iz)-bx (ix,iy-1 ,iz))    & ! ddydn(bx)
                             + by3*(bx (ix,iy+1 ,iz)-bx (ix,iy-2 ,iz))    &
                             + cy3*(bx (ix,iy+2 ,iz)-bx (ix,iy-3 ,iz)) )) &
                      + omemf*(                                           &
                            (  ax3*(bya(ix   ,iy,iz)-bya(ix-1 ,iy,iz))    & ! ddxdn(bya)
                             + bx3*(bya(ix+1 ,iy,iz)-bya(ix-2 ,iy,iz))    &
                             + cx3*(bya(ix+2 ,iy,iz)-bya(ix-3 ,iy,iz)) )  &
                          - (  ay3*(bxa(ix,iy   ,iz)-bxa(ix,iy-1 ,iz))    & ! ddydn(bxa)
                             + by3*(bxa(ix,iy+1 ,iz)-bxa(ix,iy-2 ,iz))    &
                             + cy3*(bxa(ix,iy+2 ,iz)-bxa(ix,iy-3 ,iz)) )) &
                 - ce2 * (jz(ix,iy,iz)))
  enddo
  enddo
  enddo
  if (fskin > 0.0) then
    do iz=g%lb(3),ub(3); do iy=g%lb(2),ub(2); do ix=g%lb(1),ub(1)
      ex(ix,iy,iz) = ex(ix,iy,iz)*fmhd(ix,iy,iz)
      ey(ix,iy,iz) = ey(ix,iy,iz)*fmhd(ix,iy,iz)
      ez(ix,iy,iz) = ez(ix,iy,iz)*fmhd(ix,iy,iz)
    enddo; enddo; enddo
  endif

!-----------------------------------------------------------------------!
!Set boundary components of e-field (compare with 'e_fix')
  call e_boundaries(ex,ey,ez)
!-----------------------------------------------------------------------!
! find new charge density and align charges and electric field
  if (do_e_fix .and. mod(it,every_e)==0) then
    call calc_charge
    call e_fix (ex, ey, ez, charge)
  else
    iter_e = 0
  endif
  call trace_exit('E_PUSH')

END SUBROUTINE e_push_implicit                                          ! done with the electric field.
!-----------------------------------------------------------------------!

SUBROUTINE e_fix (ex, ey, ez, charge)
  USE params,               ONLY : stdout, rank, time, stdall, master, mpi
  USE maxwell_sources,      ONLY : phi=>bxa, phif=>bya, verbose
  USE dumps,                ONLY : dump, dump_set
  USE grid_m,               ONLY : fmhd
#ifdef _OPENMP
  USE omp_lib
#endif
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: ex, ey, ez, charge
  logical :: convergence,inside                                         ! e_fix convergence flag
  integer ::  ix, iy, iz, ii, lb(3)
  real    :: dex, dey, dez, dexp, deyp, dezp, detemp
  real    :: dphi, phitemp, chgmax, a, norm, emax, dphip, chgmaxp, emaxp
  real    :: max_scalar, max_dphi

  call trace_enter('E_FIX')
  call dump_set ('e_fix')

  convergence=.false.; ii=0; dex=0.; dey=0.; dez=0.; detemp=0.         ! zap loop variables

!----------------------------------------------------------------------!
!AT THE LOWER PHYSICAL BOUNDARY WE NEED TO START HALF A ZONE INSIDE
  lb = merge(g%lb+1, g%lb, mpi%lb)                                     ! proper boundaries

!----------------------------------------------------------------------!
!NORMALIZE FILTER SO 3-D CHECKERBOARD IS THE SAME AFTER FILTERING
  a = filter/3.
  norm = 1./(1.+(3*lp0-3*lpp1-3*lpm1+3*lpp2+3*lpm2-3*lpp3-3*lpm3 - 12.*f_2nd)*a)
  if (norm .lt. 0. .and. master) then
    print *, 'Normalisation :', norm
    print *, 'Filter        :', filter
    print *, '2nd order part:', f_2nd
    print *, '4th order part:', f_4th
    print *, 'Max filter    :', abs(3. / (3*lp0-3*lpp1-3*lpm1+3*lpp2+3*lpm2-3*lpp3-3*lpm3 - 12*f_2nd))
    call error('e_fix','Your filter value is too high. Normalisation becoming negative')
  endif

  dphi = 0. ; emax = 0. ; chgmax = 0.
MAIN_LOOP : do while(ii .le. maxiter_e)                                ! loop only maxiter_e times.

!----------------------------------------------------------------------!
! Find 'source' term; PHI = div(E) - rho/epsilon0 on INTERIOR.
  dphip = 0. ; emaxp = 0. ; chgmaxp = 0.

  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    phitemp =      (  (  ax3*(ex(ix   ,iy,iz)-ex(ix-1 ,iy,iz))&
                       + bx3*(ex(ix+1 ,iy,iz)-ex(ix-2 ,iy,iz))&
                       + cx3*(ex(ix+2 ,iy,iz)-ex(ix-3 ,iy,iz))) &                            
                    + (  ay3*(ey(ix,iy   ,iz)-ey(ix,iy-1 ,iz))&
                       + by3*(ey(ix,iy+1 ,iz)-ey(ix,iy-2 ,iz))&
                       + cy3*(ey(ix,iy+2 ,iz)-ey(ix,iy-3 ,iz))) &                            
                    + (  az3*(ez(ix,iy,iz   )-ez(ix,iy,iz-1 ))&
                       + bz3*(ez(ix,iy,iz+1 )-ez(ix,iy,iz-2 ))&
                       + cz3*(ez(ix,iy,iz+2 )-ez(ix,iy,iz-3 ))) &                            
                    -   (ce3*fmhd(ix,iy,iz)*charge(ix,iy,iz))   )
    inside  = ix < g%ub(1) .and. iy < g%ub(2) .and. iz < g%ub(3)
    emaxp    = merge(max(emaxp,abs(ex(ix,iy,iz)),abs(ey(ix,iy,iz)),abs(ez(ix,iy,iz))),emaxp,inside)
    chgmaxp  = merge(max(chgmaxp,abs(ce3*charge(ix,iy,iz))),chgmaxp,inside)
    dphip    = merge(max(dphip,abs(phitemp)),dphip,inside)
    phi(ix,iy,iz) = phitemp*dd1e
  enddo
  enddo
  enddo
  emax = max(emaxp,emax)
  chgmax = max(chgmaxp,chgmax)
  dphi = max(dphip,dphi)

                                              call dump(charge,'charge')
                                              call dump(phi,   'phi1'  )
  chgmax = max(chgmax,emax/minval(g%ds))
  chgmax = max_scalar(chgmax)
  if (chgmax > 0.) dphi = dphi/chgmax

  if (maxiter_e==miniter_e) then
    max_dphi = max_scalar(dphi)
    if (master) write (stdout,*) ' dphi, tol_e, dd1e : ', max_dphi, tol_e, dd1e, dphi*dd1e
    convergence=.true.
    exit MAIN_LOOP
  endif

  max_dphi = max_scalar(dphi)
  call phi_boundaries(phi)
                                                   call dump(phi,'phi2')

!-----------------------------------------------------------------------!
!FILTER POISSON RESIDUAL (using bya as buffer - phif => bya above). 
if (filter > 0.) then
  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    phif(ix,iy,iz) = (1.+(3.*lp0 - 6.*f_2nd)*a) * phi(ix,iy,iz)                   &
                                               + a * (  lpm3 * phi(ix-3,iy  ,iz  )&
                                                      + lpm2 * phi(ix-2,iy  ,iz  )&
                                              + (lpm1+f_2nd) * phi(ix-1,iy  ,iz  )&
                                              + (lpp1+f_2nd) * phi(ix+1,iy  ,iz  )&
                                                      + lpp2 * phi(ix+2,iy  ,iz  )&
                                                      + lpp3 * phi(ix+3,iy  ,iz  ))&
                                               + a * (  lpm3 * phi(ix  ,iy-3,iz  )&
                                                      + lpm2 * phi(ix  ,iy-2,iz  )&
                                              + (lpm1+f_2nd) * phi(ix  ,iy-1,iz  )&
                                              + (lpp1+f_2nd) * phi(ix  ,iy+1,iz  )&
                                                      + lpp2 * phi(ix  ,iy+2,iz  )&
                                                      + lpp3 * phi(ix  ,iy+3,iz  ))&
                                               + a * (  lpm3 * phi(ix  ,iy  ,iz-3)&
                                                      + lpm2 * phi(ix  ,iy  ,iz-2)&
                                              + (lpm1+f_2nd) * phi(ix  ,iy  ,iz-1)&
                                              + (lpp1+f_2nd) * phi(ix  ,iy  ,iz+1)&
                                                      + lpp2 * phi(ix  ,iy  ,iz+2)&
                                                      + lpp3 * phi(ix  ,iy  ,iz+3))
  enddo
  enddo
  enddo
  
  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    phi(ix,iy,iz) = phif(ix,iy,iz)*norm
  enddo
  enddo
  enddo

  !---------------------------------------------------------------------!
  !SET BOUNDARY COMPONENTS OF PHI AGAIN
  call phi_boundaries(phi)
                                                   call dump(phi,'phi3')

end if

!-----------------------------------------------------------------------!
! NOW CORRECT E-FIELD. The electric field is half-centered in its own
! direction while grid-centered in the two perpendicular directions.  
! Because of the way half-points are indexed, the first point, half a 
! zone outside the lower boundary, has the same index as the first cell-
! centered point, so all three loops therefore run from g%lb to g%ub-1,
! except at physical lower boundaries.
  dex=0.; dey=0.; dez=0.
  dexp=0.; deyp=0.; dezp=0.

  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    detemp = (  ax3*(phi(ix+1 ,iy,iz)-phi(ix   ,iy,iz)) &  ! ddxdup(phi)
              + bx3*(phi(ix+2 ,iy,iz)-phi(ix-1 ,iy,iz)) &
              + cx3*(phi(ix+3 ,iy,iz)-phi(ix-2 ,iy,iz))  )
    dexp = max(dexp,abs(detemp))
    ex(ix,iy,iz) = ex(ix,iy,iz) + detemp
  enddo
  enddo
  enddo
  dex = max(dex,dexp)

  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    detemp = (  ay3*(phi(ix,iy+1 ,iz)-phi(ix,iy   ,iz))     & ! ddyup(phi)
              + by3*(phi(ix,iy+2 ,iz)-phi(ix,iy-1 ,iz))     &
              + cy3*(phi(ix,iy+3 ,iz)-phi(ix,iy-2 ,iz))  )
    deyp = max(deyp,abs(detemp))
    ey(ix,iy,iz) = ey(ix,iy,iz) + detemp
  enddo
  enddo
  enddo
  dey = max(dey,deyp)

  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    detemp = (  az3*(phi(ix,iy,iz+1 )-phi(ix,iy,iz   ))    & ! ddzup(phi)
              + bz3*(phi(ix,iy,iz+2 )-phi(ix,iy,iz-1 ))    &
              + cz3*(phi(ix,iy,iz+3 )-phi(ix,iy,iz-2 ))  )
    dezp = max(dezp,abs(detemp))
    ez(ix,iy,iz) = ez(ix,iy,iz) + detemp
  enddo
  enddo
  enddo
  dez = max(dez,dezp)

!-----------------------------------------------------------------------!
!SET BOUNDARY COMPONENTS OF E-FIELD
  call e_boundaries(ex,ey,ez)
                                                      call dump(ex,'ex')
                                                      call dump(ey,'ey')
                                                      call dump(ez,'ez')

!-----------------------------------------------------------------------!
! Output some stats
  iter_e = ii
  if (master .and. verbose > 1) write(stdout,*) &
                                'max_dphi,iter_e =',max_dphi,iter_e
  if (master .and. verbose > 2) write(stdall,*) &
                                'rank=',    rank,    &
                                'tol_e=',   tol_e,   &
                                'max_dphi=',max_dphi,&
                                'dphi=',    dphi,    &
                                'de(:)=',   dex,dey,dez, &
                                'iter=',    ii

!-----------------------------------------------------------------------!
!Error check part #1
  if((max_dphi >= tol_e).and.(ii >= maxiter_e).and.(stdout >= 0)) then  
    ! Something is rotten - no/too slow convergence; bail out.
    write (stdout,*) 'Exiting e_fix w/NO CONVERGENCE maxiter reached.'
    write (stdout,*) ' dphi, de, max(de(:)) : ', dphi, dex, dey, dez &
                                               , maxval((/dex,dey,dez/))
    write (stdout,*) ' max_dphi, tol_e      : ', max_dphi, tol_e
    write (stdout,*) ' abs(dphi)            : ', abs(dphi)
    write (stdout,*) ' '
    convergence=.false.
    exit MAIN_LOOP
  else if((max_dphi.le.tol_e).and.(ii.ge.miniter_e)) then               
    ! Everything is OK - convergence in due time; set flag and exit.
    convergence=.true.
    exit MAIN_LOOP
  endif
  dphi = 0. ; emax = 0. ; chgmax = 0.

  ii=ii+1                                                               ! Next iteration.
!-----------------------------------------------------------------------
ENDDO MAIN_LOOP

!-----------------------------------------------------------------------
!Error check part #2 
  if (convergence .and. verbose>0 .and. stdout>= 0) then              
    ! Print stats for 'good solve' conditions.
    write (stdout,*) 'Exiting; e_fix CONVERGED - step ok.'
    write (stdout,*) ' iterations         : ', ii
    write (stdout,*) ' dphi, max(de(:))   : ', dphi, maxval((/dex,dey,dez/))
    write (stdout,*) ' max_dphi, tol_e    : ', max_dphi, tol_e
    write (stdout,*) ' abs(dphi)          : ', abs(dphi)
    write (stdout,*) ' '
  endif

  if (verbose>1) write(stdall,*) 'exit',rank,max_dphi,iter_e
  call trace_exit('E_FIX')
!-----------------------------------------------------------------------
END SUBROUTINE e_fix

!-----------------------------------------------------------------------
SUBROUTINE b_fix (bx, by, bz)
  USE params,               ONLY : stdout, rank, time, stdall, mpi, master
  USE maxwell_sources,      ONLY : phi=>bxa, phif=>bya 
  USE maxwell_sources,      ONLY : verbose
  USE dumps,                ONLY : dump
  USE grid_m,               ONLY : g

  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)):: bx, by, bz

  logical :: convergence=.false.
  integer :: ix, iy, iz, ii, lb(3)
  real    :: dex,dey,dez, detemp
  real    :: dphi, phitemp, a, norm, bmax, bmaxp, dphip
  real    :: max_scalar, max_dphi

  call trace_enter('B_FIX')

!----------------------------------------------------------------------!
  convergence=.false.; ii=0; dex=0.; dey=0.; dez=0.                    ! zap variables
  a = filter/3.
  norm = 1./(1.-12.*a)
  lb = merge(g%lb+1, g%lb, mpi%lb)
  dphi = 0. ; bmax = 0.


!----------------------------------------------------------------------!
!Loop only maxiter_b times.
  MAIN_LOOP : do while(ii < maxiter_b)                                   

!----------------------------------------------------------------------!
!Find 'source' term; PHI = div(B) 
  dphip = 0. ; bmaxp = 0.
  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    phitemp = dd1b*(  (  ax3*(bx(ix+1 ,iy,iz)-bx(ix   ,iy,iz))&
                       + bx3*(bx(ix+2 ,iy,iz)-bx(ix-1 ,iy,iz))&
                       + cx3*(bx(ix+3 ,iy,iz)-bx(ix-2 ,iy,iz)))&
                    + (  ay3*(by(ix,iy+1 ,iz)-by(ix,iy   ,iz))&
                       + by3*(by(ix,iy+2 ,iz)-by(ix,iy-1 ,iz))&
                       + cy3*(by(ix,iy+3 ,iz)-by(ix,iy-2 ,iz)))&
                    + (  az3*(bz(ix,iy,iz+1 )-bz(ix,iy,iz   ))&
                       + bz3*(bz(ix,iy,iz+2 )-bz(ix,iy,iz-1 ))&
                       + cz3*(bz(ix,iy,iz+3 )-bz(ix,iy,iz-2 ))) )
    bmaxp = max(bmaxp,abs(bx(ix,iy,iz)),abs(by(ix,iy,iz)),abs(bz(ix,iy,iz)))
    dphip = max(dphip,abs(phitemp))
    phi(ix,iy,iz) = phitemp
  enddo
  enddo
  enddo
  bmax = max(bmax,bmaxp)
  dphi = max(dphi,dphip)

  call phib_boundaries(phi)

  bmax = bmax/minval(g%ds)
  bmax = max_scalar(bmax)

  if (bmax > 0) dphi = dphi/bmax
  max_dphi = max_scalar(dphi)

!----------------------------------------------------------------------!
!FILTER POISSON RESIDUAL (using bxh as buffer). 
if (filter > 0.) then
  do iz=lb(3), g%ub(3)-1
  do iy=lb(2), g%ub(2)-1
  do ix=lb(1), g%ub(1)-1
    phif(ix,iy,iz) = (1.-6.*a) * phi(ix,iy,iz) + a * (  lpm3 * phi(ix-3,iy,iz) &
                                                      + lpm2 * phi(ix-2,iy,iz) &
                                                      + lpm1 * phi(ix-1,iy,iz) &
                                                      + lpp1 * phi(ix+1,iy,iz) &
                                                      + lpp2 * phi(ix+2,iy,iz) &
                                                      + lpp3 * phi(ix+3,iy,iz))&
                                               + a * (  lpm3 * phi(ix,iy-3,iz) &
                                                      + lpm2 * phi(ix,iy-2,iz) &
                                                      + lpm1 * phi(ix,iy-1,iz) &
                                                      + lpp1 * phi(ix,iy+1,iz) &
                                                      + lpp2 * phi(ix,iy+2,iz) &
                                                      + lpp3 * phi(ix,iy+3,iz))&
                                               + a * (  lpm3 * phi(ix,iy,iz-3) &
                                                      + lpm2 * phi(ix,iy,iz-2) &
                                                      + lpm1 * phi(ix,iy,iz-1) &
                                                      + lpp1 * phi(ix,iy,iz+1) &
                                                      + lpp2 * phi(ix,iy,iz+2) &
                                                      + lpp3 * phi(ix,iy,iz+3))
  enddo
  enddo
  enddo

!----------------------------------------------------------------------!
!NORMALIZE FILTER SO 3-D CHECKERBOARD IS THE SAME AFTER FILTERING
  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    phi(ix,iy,iz) = phif(ix,iy,iz)*norm
  enddo
  enddo
  enddo
!-----------------------------------------------------------------------!
!SET BOUNDARY COMPONENTS OF PHI AGAIN
  call phib_boundaries(phi)
                                                    call dump(phi,'phi')

end if

!-----------------------------------------------------------------------!
!NOW CORRECT B-FIELD 

  dex=0.; dey=0.; dez=0.

  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    ! ddxdn(phi)
    detemp = ax3*(phi(ix   ,iy,iz)-phi(ix-1 ,iy,iz))  &
           + bx3*(phi(ix+1 ,iy,iz)-phi(ix-2 ,iy,iz))  &
           + cx3*(phi(ix+2 ,iy,iz)-phi(ix-3 ,iy,iz))
    dex = max(dex,abs(detemp))
    bx(ix,iy,iz) = bx(ix,iy,iz) + detemp
    ! ddydn(phi)
    detemp = ay3*(phi(ix,iy   ,iz)-phi(ix,iy-1 ,iz))  &
           + by3*(phi(ix,iy+1 ,iz)-phi(ix,iy-2 ,iz))  &
           + cy3*(phi(ix,iy+2 ,iz)-phi(ix,iy-3 ,iz))
    dey = max(dey,abs(detemp))
    by(ix,iy,iz) = by(ix,iy,iz) + detemp
    ! ddzdn(phi)
    detemp = az3*(phi(ix,iy,iz   )-phi(ix,iy,iz-1 ))  &
           + bz3*(phi(ix,iy,iz+1 )-phi(ix,iy,iz-2 ))  &
           + cz3*(phi(ix,iy,iz+2 )-phi(ix,iy,iz-3 ))
    dez = max(dez,abs(detemp))
    bz(ix,iy,iz) = bz(ix,iy,iz) + detemp
  enddo
  enddo
  enddo

!-----------------------------------------------------------------------!
!SET BOUNDARY COMPONENTS OF B-FIELD
  call b_boundaries(bx,by,bz)

  iter_divb = ii                                                        ! increase iteration stats counter, for stats only.
  if (master .and. verbose > 1) write(stdout,*) 'max_dphi,iter_divb =',max_dphi,iter_divb
  if (master .and. verbose > 2) write(stdall,*) 'rank=',    rank,    &
                                'tol_divb=',tol_divb,&
                                'max_dphi=',max_dphi,&
                                'dphi=',    dphi,    &
                                'de(:)=',   dex,dey,dez,  &
                                'iter=',    ii

!-----------------------------------------------------------------------!
!Error check part #1
  if((max_dphi >= tol_divb).and.(ii >= maxiter_divb).and.(stdout >= 0)) then  ! Something is rotten - no/too slow convergence; bail out.
    write (stdout,*) 'Exiting b_fix w/NO CONVERGENCE maxiter reached.'
    write (stdout,*) ' dphi, max(de(:))   : ', dphi, maxval((/dex,dey,dez/))
    write (stdout,*) ' max_dphi, tol_divb : ', max_dphi, tol_divb
    write (stdout,*) ' abs(dphi)          : ', abs(dphi)
    write (stdout,*) ' '
    convergence=.false.
    exit MAIN_LOOP
  else if((max_dphi.le.tol_divb).and.(ii.ge.miniter_divb)) then         ! Everything is OK - convergence in due time; set flag and exit.
    convergence=.true.
    exit MAIN_LOOP
  endif
  dphi = 0. ; bmax = 0.

  ii=ii+1                                                               ! Next iteration.
!-----------------------------------------------------------------------!
enddo MAIN_LOOP

!-----------------------------------------------------------------------!
!Error check part #2 
   if (convergence .and. verbose>0 .and. stdout>= 0) then               ! Print stats for 'good solve' conditions.
    write (stdout,*) 'Exiting; b_fix CONVERGED - step ok.'
    write (stdout,*) ' iterations         : ', ii
    write (stdout,*) ' dphi, max(de(:))   : ', dphi, maxval((/dex,dey,dez/))
    write (stdout,*) ' max_dphi, tol_divb : ', max_dphi, tol_divb
    write (stdout,*) ' abs(dphi)          : ', abs(dphi)
    write (stdout,*) ' '
   endif

!-----------------------------------------------------------------------
  if (verbose>1) write(stdall,*) 'exit',rank,max_dphi,iter_b
  call trace_exit('B_FIX')

!-----------------------------------------------------------------------
END SUBROUTINE b_fix

!-----------------------------------------------------------------------
END MODULE maxwell_solver

!-----------------------------------------------------------------------
SUBROUTINE read_maxwell_solver
  USE params,          only: master, trace, stdin, stdout, params_unit, &
                             out_namelists, do_e_fix, do_b_fix
  USE grid_m,          only: g
  USE units,           only: c
  USE maxwell_sources, only: charge, verbose
  USE energy_filter_m, only: e_filter
  USE maxwell_solver
  implicit none
  namelist /maxwell/ sor, filter, f_2nd, f_4th, emf, maxiter_b, tol_b, &
    miniter_e, maxiter_e, tol_e, every_e, miniter_divb, maxiter_divb, &
    tol_divb, verbose, do_b_fix, do_e_fix, e_filter, do_explicit
  rewind (stdin);    read  (stdin,maxwell)                              ! read params
  if (master)        write (params_unit,maxwell)
  if (sor == 0.) sor = 2./(1.+c%pi/maxval(g%n))
  if (out_namelists) write (stdout,maxwell)
END SUBROUTINE read_maxwell_solver

!-----------------------------------------------------------------------
SUBROUTINE init_maxwell
  USE params,     only : master, trace, stdin, stdout, mid, do_b_fix, &
                         do_e_fix, mpi
  USE maxwell_sources
  USE dumps,      only : dump, dump_set, dump_prefix
  USE maxwell_solver
  USE grid_m,     only : nx,ny,nz,ex,ey,ez,bx,by,bz
  USE math,       only : divstat
  implicit none  

  character(len=mid) :: id="Patch/lean/maxwell_solver.f90 $Id$"
  real           :: dtsave
  real, external :: max4
  real, dimension(nx,ny,nz) :: scr
  integer :: ix, iy, iz
  logical :: problems
  integer :: maxprint
!.......................................................................
  call print_id (id)
  call trace_enter ('INIT_MAXWELL_SOLVER')
  call dump_prefix('lean')
  call dump_set ('init_maxwell')

!-----------------------------------------------------------------------
! As per the IDL test, use no over-relaxation when using filter
  emf          = 0.55
  filter       = 0.105
  f_2nd        = 0.6
  f_4th        = 0.    ! FIXME, f_4th is not implemented in e_fix yet
  sor          = 0.65
  miniter_divb = 1
  miniter_e    = 1
  every_e      = 1
  every_b      = 100
  maxiter_b    = 400
  maxiter_divb = 400
  maxiter_e    = 400
  tol_b        = 1.e-5
  tol_e        = 1.e-4                                                   ! FIXME: are tol_e, tol_b affected by non-trivial unit scaling?
  tol_divb     = 1.e-5                                                   ! FIXME: are tol_e, tol_b affected by non-trivial unit scaling?
  verbose      = 0
  do_b_fix     = .true.
  do_e_fix     = .true.
  do_explicit  = .false.

  call read_maxwell_solver

  !---------------------------------------------------------------------
  ! use over-relaxation if not using filtering
  if (filter==0.) then
    sor = 2./(1.+c%pi/maxval(g%n)) 
  endif

  !---------------------------------------------------------------------
  ! Allocate arrays, and call sort to count particles per cell
  call allocate_sources
  call sort

  !---------------------------------------------------------------------
  ! Call the solver once, to possibly clean E and B fields, and to
  ! compute statistics.  The charge-conserving method needs dt to be
  ! non-zero, so we use a very small value instead of zero.
  dtsave = dt
  dt = 1e-30
  call solve_maxwell
  dt = dtsave

  call trace_exit('INIT_MAXWELL_SOLVER')
END SUBROUTINE init_maxwell
!-----------------------------------------------------------------------
SUBROUTINE init_maxwell_solver_restart
  USE params,          only : master, trace, stdin, stdout 
  USE dumps,           only : dump, dump_set
  USE grid_m,          only : ex, ey, ez, bx, by, bz
  USE maxwell_sources, only : bxa, bya, bza, bmax
  USE maxwell_solver,  only : order
  implicit none  

  real,            external :: max4
  real                      :: dtsave

  call trace_enter('INIT_MAXWELL_SOLVER_RESTART')

  !---------------------------------------------------------------------
  call dump_set('init_maxwell.dmp')
                                                      call dump(bx,'bx')
                                                      call dump(by,'by')
                                                      call dump(bz,'bz')
  call overlap(bx)                                                      ! Make sure overlaps are ok
  call overlap(by)
  call overlap(bz)
  call overlap(ex)
  call overlap(ey)
  call overlap(ez)
  
  bxa = bx**2+by**2+bz**2
  bmax = sqrt(max4(bxa))                                                ! Get max B-field for set_dt

  call compute_prefactors()                                             ! compute prefactors here -- they might exhibit time variation
  
  call trace_exit('INIT_MAXWELL_SOLVER_RESTART')
END SUBROUTINE init_maxwell_solver_restart

!-----------------------------------------------------------------------
SUBROUTINE solve_maxwell
  USE params,          only: do_maxwell, do_vth, dt
  USE grid_m,          only: g, bx, by, bz, ex, ey, ez
  USE dumps,           only: dump, dump_set
  USE units,           only: c
  USE maxwell_solver,  only: e_push, b_push
  USE maxwell_sources, only: calc_current
  USE energy_filter_m
  implicit none
  real a
!........................................................................
  if (.not. do_maxwell) return
  call trace_enter('SOLVE_MAXWELL')
                               call timer('thermal velocities','start')
  if (do_vth) call mesh_sources_thermal                                 ! thermal velocities
                               call timer('maxwell solver','start')
  call dump_set('maxwell.dmp')
  call compute_prefactors()                                             ! might vary w time
  call calc_current
  call b_push
                                                      call dump(bx,'bx')
                                                      call dump(by,'by')
                                                      call dump(bz,'bz')
  call e_push
                                                      call dump(ex,'ex')
                                                      call dump(ey,'ey')
                                                      call dump(ez,'ez') 
  if (e_filter > 0.0) then
    a = e_filter*c%c*dt/maxval(g%ds)
    call energy_filter (bx, by, bz, ex, ey, ez, a)
  end if

  call trace_exit('SOLVE_MAXWELL')
END SUBROUTINE solve_maxwell

!-----------------------------------------------------------------------!
SUBROUTINE compute_prefactors                                           ! 
  USE grid_m,         only : g, odxq, odyq, odzq
  USE params,         only : dt, it, mdim
  USE units,          only : c, elm
  USE maxwell_solver, only : ce1,  ce2,  ce3,                    &
                             cb1,  cb2,  cb3,                    &
                             c4bx, c4by, c4bz,                   &
                             emf, omemf, dd1e, dd1b, denom, sor, &
                             lp0, order, f_2nd, f_4th,                &
                             maxwell_stagger_prefactors
  implicit none
  integer :: order_save, test
!......................................................................!
  call maxwell_stagger_prefactors

  omemf = 1.-emf

  ce1   = (elm%ke/elm%kb)*dt                                           ! for forming dE/dt, rotB & J terms
  ce2   = c%fourpi*elm%kb                                              !         do.      -- J term only
  ce3   = c%fourpi*elm%ke                                              ! for phi in MAIN_LOOP

  cb1   = (1./elm%kf)*dt                                               ! for rot(E)
  cb2   = c%fourpi*emf*(elm%ke/elm%kf)*dt**2                           ! for rot(J), note 4pi
  cb3   = emf*omemf*(elm%ke/(elm%kf*elm%kb))*dt**2                     ! for laplace(B^{n})

  c4bx  = emf**2*dt**2*odxq*c%c2                                       ! for laplace(B^{n+1})
  c4by  = emf**2*dt**2*odyq*c%c2                                       ! for laplace(B^{n+1})
  c4bz  = emf**2*dt**2*odzq*c%c2                                       ! for laplace(B^{n+1})

  dd1e  = (1./(6. + 4.*abs(f_2nd)))*(1./(odxq+odyq+odzq))*sor          ! prefactor for e_fix
  dd1b  = (1./4.)*(1./(3.*max(odxq,odyq,odzq)))*sor                    ! prefactor for b_fix 

  denom = 1./(1.-(lp0*(c4bx + c4by + c4bz)))                           ! for laplace, 6th order

END SUBROUTINE compute_prefactors

!=======================================================================
! The boundaries zones needed by this field method
!=======================================================================
SUBROUTINE field_solver_boundaries
  USE grid_m,      only : g
  implicit none
  g%lb = max(g%lb, 4)                                                   ! def lower field bndry
  g%ub = max(g%ub, 2)                                                   ! def upper field bndry (n-ub)
END SUBROUTINE

!=======================================================================
! Return iterations used (to avoid a module dependency)
!=======================================================================
SUBROUTINE maxwell_iterations (n_b, n_e)
  USE maxwell_solver, only: iter_b, iter_e
  implicit none
  integer n_b, n_e
  n_b = iter_b
  n_e = iter_e
END SUBROUTINE
