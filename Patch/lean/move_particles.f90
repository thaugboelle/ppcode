! Patch/lean/move_particles.f90 $Id$
! vim: nowrap
!-----------------------------------------------------------------------
SUBROUTINE move_particles
!
! This is a version of integrate without any interactions, optimised for speed
! and with all the cell stuff, lorentz_force, and move_particles inlined.
!
! It takes half the time per particle update than the original version.
!
  USE params,  only : nspecies, dt, dtp, particleupdates, do_lorentz, do_maxwell, &
                      nodes, do_synthetic_spectra, do_vth, grav_acc, mcoord, &
                      do_dump, do_compare, nomp, romp, do_cool
  USE grid_m,  only : g, Bx, By, Bz, Ex, Ey, Ez
  USE species, only : particle, sp, fields
  USE units,   only : c, elm
  USE dumps,   only : dump, dump_set, dump_prefix, verbose
  implicit none

  integer,        parameter               :: sdb=-2, slb=-1, sub=2, suc=3
  integer,        dimension(nspecies)     :: firstcellindex                     ! index to first part in cell
  type(particle), pointer, dimension(:)   :: pav
  real    :: ods(3), oc, oc2, mass, hE, hB, dt4, dtp4, dxdt, dydt, dzdt, r(3), o, &
             wa, wb, wc, wd, s, sm1, pEx, pEy, pEz, pBx, pBy, pBz, we(8), &
             rfac, cx, cy, cz, p(3), dp(3), p2, og
  real, dimension(sdb:suc,3)               :: w, dw
  real, dimension(sdb:sub,slb:sub,slb:sub) :: sEx
  real, dimension(slb:sub,sdb:sub,slb:sub) :: sEy
  real, dimension(slb:sub,slb:sub,sdb:sub) :: sEz
  real, dimension(slb:sub,sdb:sub,sdb:sub) :: sBx
  real, dimension(sdb:sub,slb:sub,sdb:sub) :: sBy
  real, dimension(sdb:sub,sdb:sub,slb:sub) :: sBz
  real, dimension(sdb:suc,sdb:suc,sdb:suc) :: d
  real, dimension(sdb:suc,sdb:suc,sdb:suc,3) :: v
  real, parameter :: zero=0., one_sixth=1./6., two_third=2./3., sqrt2=1.4142135623730951454746219
  integer :: ix, iy, iz, izy, jx, jy, jz, jxu, jyu, jzu, kx, ky, kz, isp, ip, np, ox, oy, oz
  logical :: up, touched_row, do_cool_local
  type field_type_local
    real d, v(mcoord)
  end type
  integer               :: iomp
  type(field_type_local), allocatable, dimension(:,:,:,:,:) :: lfields
  real, allocatable, dimension(:,:,:) :: scr
!-----------------------------------------------------------------------

  if (.not. do_lorentz) call error('integrate', 'integrate_lean is for runs without collisions' &
    //' and only with fields. A run without collisions and without fields does not make sense.')

  call trace_enter('main integration step')
  call check_particle_ranges ('before move')
  call dump_prefix('lean')
  call dump_set('maxwell')
                                                call timer('particles','start')

  ods = g%ods
  oc2 = c%oc2
  oc  = c%oc
  dt4 = real(dt)                                                                ! convert dt to real default kind
  dtp4 = real(dtp)                                                              ! convert dt to real default kind
  dxdt = g%ds(1) / dt4; dydt = g%ds(2) / dt4; dzdt = g%ds(3) / dt4              

  if (nomp > 1) allocate(lfields(g%n(1),g%n(2),g%n(3),nspecies,2:nomp))

  if (romp==1) then
    do isp=1,nspecies                                                           ! loop over species
      do iz=1,g%n(3)                                                            ! zero fields
      do iy=1,g%n(2)                                                              
      do ix=1,g%n(1)                                                              
        fields(ix,iy,iz,isp)%d = 0.                                             ! zap charges
        fields(ix,iy,iz,isp)%v = 0.                                             ! zap velocities
      enddo
      enddo
      enddo
    enddo
  else
    do isp=1,nspecies                                                           ! loop over species
      do iz=1,g%n(3)                                                            ! zero fields
      do iy=1,g%n(2)                                                              
      do ix=1,g%n(1)                                                              
        lfields(ix,iy,iz,isp,romp)%d = 0.                                       ! zap charges
        lfields(ix,iy,iz,isp,romp)%v = 0.                                       ! zap velocities
      enddo
      enddo
      enddo
    enddo
  endif

  w=0.; dw=0.; we = 0.                                                          ! zero weights

  do izy=1,g%n(2)*g%n(3)                                                        ! collapse loop manually. Xlf on Jugene doesnt support collapse directive
    iz = (izy-1) / g%n(2) + 1
    iy = izy - (iz-1)*g%n(2)
      if (iy==1) then
        if (iz==1) then
          firstcellindex = 1                                                            
        else
          firstcellindex = fields(g%n(1),g%n(2),iz-1,:)%i + 1
        endif
      else
        firstcellindex = fields(g%n(1),iy-1,iz,:)%i + 1
      endif        
      touched_row = .false.                                                     
      do ix=1,g%n(1)                                                            ! loop over cells
        if (any(fields(ix,iy,iz,:)%i >= firstcellindex)) then                   ! avoid if empty cell
!===============================================================================
! Cache E-M fields
!===============================================================================
          ! copy fields over, taking care of boundaries and reusing slices where possible
          do jz=sdb,sub
            kz = jz + iz
            if (kz > 0 .and. kz <= g%n(3)) then
              do jy=sdb,sub
                ky = jy + iy
                if (ky > 0 .and. ky <= g%n(2)) then
                  do jx=sdb,sub
                    kx = jx + ix
                    if (kx > 0 .and. kx <= g%n(1)) then
                      if (touched_row) then
                        if (jx==sdb) then
                          if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = sEx(jx+1,jy,jz)
                          if (jy > sdb) sBy(jx,jy,jz) = sBy(jx+1,jy,jz)
                          if (jz > sdb) sBz(jx,jy,jz) = sBz(jx+1,jy,jz)
                        else if (jx < sub) then
                          if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = sEx(jx+1,jy,jz)
                          if (jz > sdb) sEy(jx,jy,jz) = sEy(jx+1,jy,jz)
                          if (jy > sdb) sEz(jx,jy,jz) = sEz(jx+1,jy,jz)
                          sBx(jx,jy,jz) = sBx(jx+1,jy,jz)
                          if (jy > sdb) sBy(jx,jy,jz) = sBy(jx+1,jy,jz)
                          if (jz > sdb) sBz(jx,jy,jz) = sBz(jx+1,jy,jz)
                        else !if (jx==sub) then ! "if" not needed, this is the last option
                          if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = Ex(kx,ky,kz)
                          if (jz > sdb) sEy(jx,jy,jz) = Ey(kx,ky,kz)
                          if (jy > sdb) sEz(jx,jy,jz) = Ez(kx,ky,kz)
                          sBx(jx,jy,jz) = Bx(kx,ky,kz)
                          if (jy > sdb) sBy(jx,jy,jz) = By(kx,ky,kz)
                          if (jz > sdb) sBz(jx,jy,jz) = Bz(kx,ky,kz)
                        endif
                      else
                        if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = Ex(kx,ky,kz)
                        if (jx > sdb .and. jz > sdb) sEy(jx,jy,jz) = Ey(kx,ky,kz)
                        if (jx > sdb .and. jy > sdb) sEz(jx,jy,jz) = Ez(kx,ky,kz)
                        if (jx > sdb) sBx(jx,jy,jz) = Bx(kx,ky,kz)
                        if (jy > sdb) sBy(jx,jy,jz) = By(kx,ky,kz)
                        if (jz > sdb) sBz(jx,jy,jz) = Bz(kx,ky,kz)
                      endif
                    else
                      if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = 0.
                      if (jx > sdb .and. jz > sdb) sEy(jx,jy,jz) = 0.
                      if (jx > sdb .and. jy > sdb) sEz(jx,jy,jz) = 0.
                      if (jx > sdb) sBx(jx,jy,jz) = 0.
                      if (jy > sdb) sBy(jx,jy,jz) = 0.
                      if (jz > sdb) sBz(jx,jy,jz) = 0.
                    endif
                  enddo
                else
                  do jx=sdb,sub
                    if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = 0.
                    if (jx > sdb .and. jz > sdb) sEy(jx,jy,jz) = 0.
                    if (jx > sdb .and. jy > sdb) sEz(jx,jy,jz) = 0.
                    if (jx > sdb) sBx(jx,jy,jz) = 0.
                    if (jy > sdb) sBy(jx,jy,jz) = 0.
                    if (jz > sdb) sBz(jx,jy,jz) = 0.
                  enddo
                endif
              enddo
            else
              do jy=sdb,sub
                do jx=sdb,sub
                  if (jy > sdb .and. jz > sdb) sEx(jx,jy,jz) = 0.
                  if (jx > sdb .and. jz > sdb) sEy(jx,jy,jz) = 0.
                  if (jx > sdb .and. jy > sdb) sEz(jx,jy,jz) = 0.
                  if (jx > sdb) sBx(jx,jy,jz) = 0.
                  if (jy > sdb) sBy(jx,jy,jz) = 0.
                  if (jz > sdb) sBz(jx,jy,jz) = 0.
                enddo
              enddo
            endif
          enddo
          touched_row = .true.

!===============================================================================
! Loop over species and then particles in the cell
!===============================================================================
          do isp=1,nspecies                                                     ! loop over species
            np  = fields(ix,iy,iz,isp)%i - firstcellindex(isp) + 1              ! #parts in cell
            do_cool_local = do_cool .and. &                                     ! only cool electrons and positrons
              ( trim(sp(isp)%name) .eq. 'electron' .or. &
                trim(sp(isp)%name) .eq. 'positron' )
            if (np > 0) then                                                    ! short cut, should not call if no particles
              mass = sp(isp)%mass                                               ! mass
              hE = dtp4*sp(isp)%charge/mass                                     ! prefactor, '0.5' => perfectly time centered E-splitting
              hB = 0.5*hE*elm%kf                                                ! B prefactor
              v=0.; d=0.                                                        ! zero fluxes for cell
              pav => sp(isp)%particle                                           ! for convenience
              rfac = -2.*dtp4*elm%ke*sp(isp)%charge**2/(3.*c%c)/(mass*c%c2)     ! prefactor for radiative cooling 
              do ip=firstcellindex(isp),fields(ix,iy,iz,isp)%i
!===============================================================================
! Make weights
!===============================================================================
                ! ---------------------------------------------------------
                ! Find signed distance to center points in all directions.
                ! Project index into allowed range for interpolation.
                r = pav(ip)%r
                !-------------
                ! X_COORD CENTERED
                s = r(1); sm1 = 1. - s; o = 2. - s 
                w(-1,1) = one_sixth * sm1*sm1*sm1
                w( 0,1) = two_third - 0.5*s*s*o
                w( 1,1) = two_third - 0.5*sm1*sm1*(1.+ s) 
                w( 2,1) = 1. - (w(-1,1) + w(0,1) + w(1,1))
                !-------------
                ! Y_COORD CENTERED
                s = r(2); sm1 = 1. - s; o = 2. - s 
                w(-1,2) = one_sixth * sm1*sm1*sm1
                w( 0,2) = two_third - 0.5*s*s*o
                w( 1,2) = two_third - 0.5*sm1*sm1*(1.+ s) 
                w( 2,2) = 1. - (w(-1,2) + w(0,2) + w(1,2))
                !-------------
                ! Z_COORD CENTERED
                s = r(3); sm1 = 1. - s; o = 2. - s 
                w(-1,3) = one_sixth * sm1*sm1*sm1
                w( 0,3) = two_third - 0.5*s*s*o
                w( 1,3) = two_third - 0.5*sm1*sm1*(1.+ s) 
                w( 2,3) = 1. - (w(-1,3) + w(0,3) + w(1,3))
                !-------------
                ! X_COORD SHIFTED IN X
                s = r(1) - 0.5; up = s >= 0
                s = s + merge(0.,1.,up); sm1 = 1. - s; o = 2. - s
                wa = one_sixth * sm1*sm1*sm1
                wb = two_third - 0.5*s*s*o
                wc = two_third - 0.5*sm1*sm1*(1.+ s) 
                wd = 1. - (wa + wb + wc)
                dw(-2,1) = merge(zero, wa, up)
                dw(-1,1) = merge(wa, wb, up)
                dw( 0,1) = merge(wb, wc, up)
                dw( 1,1) = merge(wc, wd, up)
                dw( 2,1) = merge(wd, zero, up)
                !-------------
                ! Y_COORD SHIFTED IN Y
                s = r(2) - 0.5; up = s >= 0
                s = s + merge(0.,1.,up); sm1 = 1. - s; o = 2. - s
                wa = one_sixth * sm1*sm1*sm1
                wb = two_third - 0.5*s*s*o
                wc = two_third - 0.5*sm1*sm1*(1.+ s) 
                wd = 1. - (wa + wb + wc)
                dw(-2,2) = merge(zero, wa, up)
                dw(-1,2) = merge(wa, wb, up)
                dw( 0,2) = merge(wb, wc, up)
                dw( 1,2) = merge(wc, wd, up)
                dw( 2,2) = merge(wd, zero, up)
                !-------------
                ! Z_COORD SHIFTED IN Z
                s = r(3) - 0.5; up = s >= 0
                s = s + merge(0.,1.,up); sm1 = 1. - s; o = 2. - s
                wa = one_sixth * sm1*sm1*sm1
                wb = two_third - 0.5*s*s*o
                wc = two_third - 0.5*sm1*sm1*(1.+ s) 
                wd = 1. - (wa + wb + wc)
                dw(-2,3) = merge(zero, wa, up)
                dw(-1,3) = merge(wa, wb, up)
                dw( 0,3) = merge(wb, wc, up)
                dw( 1,3) = merge(wc, wd, up)
                dw( 2,3) = merge(wd, zero, up)
!===============================================================================
! Interpolation
!===============================================================================
                !-----------------------------------
                ! X SHIFTED WEIGHTS
                pEx = &
                     ((sEx(-2,-1,-1)*dw(-2,1) + &
                       sEx(-1,-1,-1)*dw(-1,1) + &
                       sEx( 0,-1,-1)*dw( 0,1) + &
                       sEx(+1,-1,-1)*dw( 1,1) + &
                       sEx(+2,-1,-1)*dw( 2,1))*w(-1,2) + &
                      (sEx(-2, 0,-1)*dw(-2,1) + &
                       sEx(-1, 0,-1)*dw(-1,1) + &
                       sEx( 0, 0,-1)*dw( 0,1) + &
                       sEx(+1, 0,-1)*dw( 1,1) + &
                       sEx(+2, 0,-1)*dw( 2,1))*w( 0,2) + &
                      (sEx(-2,+1,-1)*dw(-2,1) + &
                       sEx(-1,+1,-1)*dw(-1,1) + &
                       sEx( 0,+1,-1)*dw( 0,1) + &
                       sEx(+1,+1,-1)*dw( 1,1) + &
                       sEx(+2,+1,-1)*dw( 2,1))*w( 1,2) + &
                      (sEx(-2,+2,-1)*dw(-2,1) + &
                       sEx(-1,+2,-1)*dw(-1,1) + &
                       sEx( 0,+2,-1)*dw( 0,1) + &
                       sEx(+1,+2,-1)*dw( 1,1) + &
                       sEx(+2,+2,-1)*dw( 2,1))*w( 2,2))*w(-1,3) + &
                     ((sEx(-2,-1, 0)*dw(-2,1) + &
                       sEx(-1,-1, 0)*dw(-1,1) + &
                       sEx( 0,-1, 0)*dw( 0,1) + &
                       sEx(+1,-1, 0)*dw( 1,1) + &
                       sEx(+2,-1, 0)*dw( 2,1))*w(-1,2) + &
                      (sEx(-2, 0, 0)*dw(-2,1) + &
                       sEx(-1, 0, 0)*dw(-1,1) + &
                       sEx( 0, 0, 0)*dw( 0,1) + &
                       sEx(+1, 0, 0)*dw( 1,1) + &
                       sEx(+2, 0, 0)*dw( 2,1))*w( 0,2) + &
                      (sEx(-2,+1, 0)*dw(-2,1) + &
                       sEx(-1,+1, 0)*dw(-1,1) + &
                       sEx( 0,+1, 0)*dw( 0,1) + &
                       sEx(+1,+1, 0)*dw( 1,1) + &
                       sEx(+2,+1, 0)*dw( 2,1))*w( 1,2) + &
                      (sEx(-2,+2, 0)*dw(-2,1) + &
                       sEx(-1,+2, 0)*dw(-1,1) + &
                       sEx( 0,+2, 0)*dw( 0,1) + &
                       sEx(+1,+2, 0)*dw( 1,1) + &
                       sEx(+2,+2, 0)*dw( 2,1))*w( 2,2))*w( 0,3) + &
                     ((sEx(-2,-1,+1)*dw(-2,1) + &
                       sEx(-1,-1,+1)*dw(-1,1) + &
                       sEx( 0,-1,+1)*dw( 0,1) + &
                       sEx(+1,-1,+1)*dw( 1,1) + &
                       sEx(+2,-1,+1)*dw( 2,1))*w(-1,2) + &
                      (sEx(-2, 0,+1)*dw(-2,1) + &
                       sEx(-1, 0,+1)*dw(-1,1) + &
                       sEx( 0, 0,+1)*dw( 0,1) + &
                       sEx(+1, 0,+1)*dw( 1,1) + &
                       sEx(+2, 0,+1)*dw( 2,1))*w( 0,2) + &
                      (sEx(-2,+1,+1)*dw(-2,1) + &
                       sEx(-1,+1,+1)*dw(-1,1) + &
                       sEx( 0,+1,+1)*dw( 0,1) + &
                       sEx(+1,+1,+1)*dw( 1,1) + &
                       sEx(+2,+1,+1)*dw( 2,1))*w( 1,2) + &
                      (sEx(-2,+2,+1)*dw(-2,1) + &
                       sEx(-1,+2,+1)*dw(-1,1) + &
                       sEx( 0,+2,+1)*dw( 0,1) + &
                       sEx(+1,+2,+1)*dw( 1,1) + &
                       sEx(+2,+2,+1)*dw( 2,1))*w( 2,2))*w( 1,3) + &
                     ((sEx(-2,-1,+2)*dw(-2,1) + &
                       sEx(-1,-1,+2)*dw(-1,1) + &
                       sEx( 0,-1,+2)*dw( 0,1) + &
                       sEx(+1,-1,+2)*dw( 1,1) + &
                       sEx(+2,-1,+2)*dw( 2,1))*w(-1,2) + &
                      (sEx(-2, 0,+2)*dw(-2,1) + &
                       sEx(-1, 0,+2)*dw(-1,1) + &
                       sEx( 0, 0,+2)*dw( 0,1) + &
                       sEx(+1, 0,+2)*dw( 1,1) + &
                       sEx(+2, 0,+2)*dw( 2,1))*w( 0,2) + &
                      (sEx(-2,+1,+2)*dw(-2,1) + &
                       sEx(-1,+1,+2)*dw(-1,1) + &
                       sEx( 0,+1,+2)*dw( 0,1) + &
                       sEx(+1,+1,+2)*dw( 1,1) + &
                       sEx(+2,+1,+2)*dw( 2,1))*w( 1,2) + &
                      (sEx(-2,+2,+2)*dw(-2,1) + &
                       sEx(-1,+2,+2)*dw(-1,1) + &
                       sEx( 0,+2,+2)*dw( 0,1) + &
                       sEx(+1,+2,+2)*dw( 1,1) + &
                       sEx(+2,+2,+2)*dw( 2,1))*w( 2,2))*w( 2,3)
                !-----------------------------------
                ! Y SHIFTED WEIGHTS
                pEy = &
                    ((sEy(-1,-2,-1)*w(-1,1) + &
                      sEy( 0,-2,-1)*w( 0,1) + &
                      sEy(+1,-2,-1)*w( 1,1) + &
                      sEy(+2,-2,-1)*w( 2,1))*dw(-2,2) + &
                     (sEy(-1,-1,-1)*w(-1,1) + &
                      sEy( 0,-1,-1)*w( 0,1) + &
                      sEy(+1,-1,-1)*w( 1,1) + &
                      sEy(+2,-1,-1)*w( 2,1))*dw(-1,2) + &
                     (sEy(-1, 0,-1)*w(-1,1) + &
                      sEy( 0, 0,-1)*w( 0,1) + &
                      sEy(+1, 0,-1)*w( 1,1) + &
                      sEy(+2, 0,-1)*w( 2,1))*dw( 0,2) + &
                     (sEy(-1,+1,-1)*w(-1,1) + &
                      sEy( 0,+1,-1)*w( 0,1) + &
                      sEy(+1,+1,-1)*w( 1,1) + &
                      sEy(+2,+1,-1)*w( 2,1))*dw( 1,2) + &
                     (sEy(-1,+2,-1)*w(-1,1) + &
                      sEy( 0,+2,-1)*w( 0,1) + &
                      sEy(+1,+2,-1)*w( 1,1) + &
                      sEy(+2,+2,-1)*w( 2,1))*dw( 2,2))*w(-1,3) + &
                    ((sEy(-1,-2, 0)*w(-1,1) + &
                      sEy( 0,-2, 0)*w( 0,1) + &
                      sEy(+1,-2, 0)*w( 1,1) + &
                      sEy(+2,-2, 0)*w( 2,1))*dw(-2,2) + &
                     (sEy(-1,-1, 0)*w(-1,1) + &
                      sEy( 0,-1, 0)*w( 0,1) + &
                      sEy(+1,-1, 0)*w( 1,1) + &
                      sEy(+2,-1, 0)*w( 2,1))*dw(-1,2) + &
                     (sEy(-1, 0, 0)*w(-1,1) + &
                      sEy( 0, 0, 0)*w( 0,1) + &
                      sEy(+1, 0, 0)*w( 1,1) + &
                      sEy(+2, 0, 0)*w( 2,1))*dw( 0,2) + &
                     (sEy(-1,+1, 0)*w(-1,1) + &
                      sEy( 0,+1, 0)*w( 0,1) + &
                      sEy(+1,+1, 0)*w( 1,1) + &
                      sEy(+2,+1, 0)*w( 2,1))*dw( 1,2) + &
                     (sEy(-1,+2, 0)*w(-1,1) + &
                      sEy( 0,+2, 0)*w( 0,1) + &
                      sEy(+1,+2, 0)*w( 1,1) + &
                      sEy(+2,+2, 0)*w( 2,1))*dw( 2,2))*w( 0,3) + &
                    ((sEy(-1,-2,+1)*w(-1,1) + &
                      sEy( 0,-2,+1)*w( 0,1) + &
                      sEy(+1,-2,+1)*w( 1,1) + &
                      sEy(+2,-2,+1)*w( 2,1))*dw(-2,2) + &
                     (sEy(-1,-1,+1)*w(-1,1) + &
                      sEy( 0,-1,+1)*w( 0,1) + &
                      sEy(+1,-1,+1)*w( 1,1) + &
                      sEy(+2,-1,+1)*w( 2,1))*dw(-1,2) + &
                     (sEy(-1, 0,+1)*w(-1,1) + &
                      sEy( 0, 0,+1)*w( 0,1) + &
                      sEy(+1, 0,+1)*w( 1,1) + &
                      sEy(+2, 0,+1)*w( 2,1))*dw( 0,2) + &
                     (sEy(-1,+1,+1)*w(-1,1) + &
                      sEy( 0,+1,+1)*w( 0,1) + &
                      sEy(+1,+1,+1)*w( 1,1) + &
                      sEy(+2,+1,+1)*w( 2,1))*dw( 1,2) + &
                     (sEy(-1,+2,+1)*w(-1,1) + &
                      sEy( 0,+2,+1)*w( 0,1) + &
                      sEy(+1,+2,+1)*w( 1,1) + &
                      sEy(+2,+2,+1)*w( 2,1))*dw( 2,2))*w( 1,3) + &
                    ((sEy(-1,-2,+2)*w(-1,1) + &
                      sEy( 0,-2,+2)*w( 0,1) + &
                      sEy(+1,-2,+2)*w( 1,1) + &
                      sEy(+2,-2,+2)*w( 2,1))*dw(-2,2) + &
                     (sEy(-1,-1,+2)*w(-1,1) + &
                      sEy( 0,-1,+2)*w( 0,1) + &
                      sEy(+1,-1,+2)*w( 1,1) + &
                      sEy(+2,-1,+2)*w( 2,1))*dw(-1,2) + &
                     (sEy(-1, 0,+2)*w(-1,1) + &
                      sEy( 0, 0,+2)*w( 0,1) + &
                      sEy(+1, 0,+2)*w( 1,1) + &
                      sEy(+2, 0,+2)*w( 2,1))*dw( 0,2) + &
                     (sEy(-1,+1,+2)*w(-1,1) + &
                      sEy( 0,+1,+2)*w( 0,1) + &
                      sEy(+1,+1,+2)*w( 1,1) + &
                      sEy(+2,+1,+2)*w( 2,1))*dw( 1,2) + &
                     (sEy(-1,+2,+2)*w(-1,1) + &
                      sEy( 0,+2,+2)*w( 0,1) + &
                      sEy(+1,+2,+2)*w( 1,1) + &
                      sEy(+2,+2,+2)*w( 2,1))*dw( 2,2))*w( 2,3)
              !-----------------------------------
              ! Z SHIFTED WEIGHTS
                pEz = &
                   ((sEz(-1,-1,-2)*w(-1,1) + &      
                     sEz( 0,-1,-2)*w( 0,1) + &      
                     sEz(+1,-1,-2)*w( 1,1) + &      
                     sEz(+2,-1,-2)*w( 2,1))*w(-1,2) + &
                    (sEz(-1, 0,-2)*w(-1,1) + &      
                     sEz( 0, 0,-2)*w( 0,1) + &      
                     sEz(+1, 0,-2)*w( 1,1) + &      
                     sEz(+2, 0,-2)*w( 2,1))*w( 0,2) + &
                    (sEz(-1,+1,-2)*w(-1,1) + &      
                     sEz( 0,+1,-2)*w( 0,1) + &      
                     sEz(+1,+1,-2)*w( 1,1) + &      
                     sEz(+2,+1,-2)*w( 2,1))*w( 1,2) + &
                    (sEz(-1,+2,-2)*w(-1,1) + &
                     sEz( 0,+2,-2)*w( 0,1) + &      
                     sEz(+1,+2,-2)*w( 1,1) + &      
                     sEz(+2,+2,-2)*w( 2,1))*w( 2,2))*dw(-2,3) + &
                   ((sEz(-1,-1,-1)*w(-1,1) + &      
                     sEz( 0,-1,-1)*w( 0,1) + &      
                     sEz(+1,-1,-1)*w( 1,1) + &      
                     sEz(+2,-1,-1)*w( 2,1))*w(-1,2) + &
                    (sEz(-1, 0,-1)*w(-1,1) + &      
                     sEz( 0, 0,-1)*w( 0,1) + &      
                     sEz(+1, 0,-1)*w( 1,1) + &      
                     sEz(+2, 0,-1)*w( 2,1))*w( 0,2) + &
                    (sEz(-1,+1,-1)*w(-1,1) + &      
                     sEz( 0,+1,-1)*w( 0,1) + &      
                     sEz(+1,+1,-1)*w( 1,1) + &      
                     sEz(+2,+1,-1)*w( 2,1))*w( 1,2) + &
                    (sEz(-1,+2,-1)*w(-1,1) + &
                     sEz( 0,+2,-1)*w( 0,1) + &      
                     sEz(+1,+2,-1)*w( 1,1) + &      
                     sEz(+2,+2,-1)*w( 2,1))*w( 2,2))*dw(-1,3) + &
                   ((sEz(-1,-1, 0)*w(-1,1) + &      
                     sEz( 0,-1, 0)*w( 0,1) + &      
                     sEz(+1,-1, 0)*w( 1,1) + &      
                     sEz(+2,-1, 0)*w( 2,1))*w(-1,2) + &
                    (sEz(-1, 0, 0)*w(-1,1) + &      
                     sEz( 0, 0, 0)*w( 0,1) + &      
                     sEz(+1, 0, 0)*w( 1,1) + &      
                     sEz(+2, 0, 0)*w( 2,1))*w( 0,2) + &
                    (sEz(-1,+1, 0)*w(-1,1) + &      
                     sEz( 0,+1, 0)*w( 0,1) + &      
                     sEz(+1,+1, 0)*w( 1,1) + &      
                     sEz(+2,+1, 0)*w( 2,1))*w( 1,2) + &
                    (sEz(-1,+2, 0)*w(-1,1) + &      
                     sEz( 0,+2, 0)*w( 0,1) + &      
                     sEz(+1,+2, 0)*w( 1,1) + &      
                     sEz(+2,+2, 0)*w( 2,1))*w( 2,2))*dw( 0,3) + &
                   ((sEz(-1,-1,+1)*w(-1,1) + &      
                     sEz( 0,-1,+1)*w( 0,1) + &      
                     sEz(+1,-1,+1)*w( 1,1) + &      
                     sEz(+2,-1,+1)*w( 2,1))*w(-1,2) + &
                    (sEz(-1, 0,+1)*w(-1,1) + &      
                     sEz( 0, 0,+1)*w( 0,1) + &      
                     sEz(+1, 0,+1)*w( 1,1) + &      
                     sEz(+2, 0,+1)*w( 2,1))*w( 0,2) + &
                    (sEz(-1,+1,+1)*w(-1,1) + &      
                     sEz( 0,+1,+1)*w( 0,1) + &      
                     sEz(+1,+1,+1)*w( 1,1) + &      
                     sEz(+2,+1,+1)*w( 2,1))*w( 1,2) + &
                    (sEz(-1,+2,+1)*w(-1,1) + &      
                     sEz( 0,+2,+1)*w( 0,1) + &      
                     sEz(+1,+2,+1)*w( 1,1) + &      
                     sEz(+2,+2,+1)*w( 2,1))*w( 2,2))*dw( 1,3) + &
                   ((sEz(-1,-1,+2)*w(-1,1) + &      
                     sEz( 0,-1,+2)*w( 0,1) + &      
                     sEz(+1,-1,+2)*w( 1,1) + &      
                     sEz(+2,-1,+2)*w( 2,1))*w(-1,2) + &
                    (sEz(-1, 0,+2)*w(-1,1) + &      
                     sEz( 0, 0,+2)*w( 0,1) + &      
                     sEz(+1, 0,+2)*w( 1,1) + &      
                     sEz(+2, 0,+2)*w( 2,1))*w( 0,2) + &
                    (sEz(-1,+1,+2)*w(-1,1) + &      
                     sEz( 0,+1,+2)*w( 0,1) + &      
                     sEz(+1,+1,+2)*w( 1,1) + &      
                     sEz(+2,+1,+2)*w( 2,1))*w( 1,2) + &
                    (sEz(-1,+2,+2)*w(-1,1) + &      
                     sEz( 0,+2,+2)*w( 0,1) + &      
                     sEz(+1,+2,+2)*w( 1,1) + &      
                     sEz(+2,+2,+2)*w( 2,1))*w( 2,2))*dw( 2,3)
              !-----------------------------------
              ! YZ SHIFTED WEIGHTS
                pBx = &
                   ((sBx(-1,-2,-2)*w(-1,1) + &      
                     sBx( 0,-2,-2)*w( 0,1) + &      
                     sBx(+1,-2,-2)*w( 1,1) + &      
                     sBx(+2,-2,-2)*w( 2,1))*dw(-2,2) + &
                    (sBx(-1,-1,-2)*w(-1,1) + &      
                     sBx( 0,-1,-2)*w( 0,1) + &      
                     sBx(+1,-1,-2)*w( 1,1) + &      
                     sBx(+2,-1,-2)*w( 2,1))*dw(-1,2) + &
                    (sBx(-1, 0,-2)*w(-1,1) + &      
                     sBx( 0, 0,-2)*w( 0,1) + &      
                     sBx(+1, 0,-2)*w( 1,1) + &      
                     sBx(+2, 0,-2)*w( 2,1))*dw( 0,2) + &
                    (sBx(-1,+1,-2)*w(-1,1) + &      
                     sBx( 0,+1,-2)*w( 0,1) + &      
                     sBx(+1,+1,-2)*w( 1,1) + &      
                     sBx(+2,+1,-2)*w( 2,1))*dw( 1,2) + &
                    (sBx(-1,+2,-2)*w(-1,1) + &      
                     sBx( 0,+2,-2)*w( 0,1) + &      
                     sBx(+1,+2,-2)*w( 1,1) + &      
                     sBx(+2,+2,-2)*w( 2,1))*dw( 2,2))*dw(-2,3) + &
                   ((sBx(-1,-2,-1)*w(-1,1) + &      
                     sBx( 0,-2,-1)*w( 0,1) + &      
                     sBx(+1,-2,-1)*w( 1,1) + &      
                     sBx(+2,-2,-1)*w( 2,1))*dw(-2,2) + &
                    (sBx(-1,-1,-1)*w(-1,1) + &      
                     sBx( 0,-1,-1)*w( 0,1) + &      
                     sBx(+1,-1,-1)*w( 1,1) + &      
                     sBx(+2,-1,-1)*w( 2,1))*dw(-1,2) + &
                    (sBx(-1, 0,-1)*w(-1,1) + &      
                     sBx( 0, 0,-1)*w( 0,1) + &      
                     sBx(+1, 0,-1)*w( 1,1) + &      
                     sBx(+2, 0,-1)*w( 2,1))*dw( 0,2) + &
                    (sBx(-1,+1,-1)*w(-1,1) + &      
                     sBx( 0,+1,-1)*w( 0,1) + &      
                     sBx(+1,+1,-1)*w( 1,1) + &      
                     sBx(+2,+1,-1)*w( 2,1))*dw( 1,2) + &
                    (sBx(-1,+2,-1)*w(-1,1) + &      
                     sBx( 0,+2,-1)*w( 0,1) + &      
                     sBx(+1,+2,-1)*w( 1,1) + &      
                     sBx(+2,+2,-1)*w( 2,1))*dw( 2,2))*dw(-1,3) + &
                   ((sBx(-1,-2, 0)*w(-1,1) + &      
                     sBx( 0,-2, 0)*w( 0,1) + &      
                     sBx(+1,-2, 0)*w( 1,1) + &      
                     sBx(+2,-2, 0)*w( 2,1))*dw(-2,2) + &
                    (sBx(-1,-1, 0)*w(-1,1) + &      
                     sBx( 0,-1, 0)*w( 0,1) + &      
                     sBx(+1,-1, 0)*w( 1,1) + &      
                     sBx(+2,-1, 0)*w( 2,1))*dw(-1,2) + &
                    (sBx(-1, 0, 0)*w(-1,1) + &      
                     sBx( 0, 0, 0)*w( 0,1) + &      
                     sBx(+1, 0, 0)*w( 1,1) + &      
                     sBx(+2, 0, 0)*w( 2,1))*dw( 0,2) + &
                    (sBx(-1,+1, 0)*w(-1,1) + &      
                     sBx( 0,+1, 0)*w( 0,1) + &      
                     sBx(+1,+1, 0)*w( 1,1) + &      
                     sBx(+2,+1, 0)*w( 2,1))*dw( 1,2) + &
                    (sBx(-1,+2, 0)*w(-1,1) + &      
                     sBx( 0,+2, 0)*w( 0,1) + &      
                     sBx(+1,+2, 0)*w( 1,1) + &      
                     sBx(+2,+2, 0)*w( 2,1))*dw( 2,2))*dw( 0,3) + &
                   ((sBx(-1,-2,+1)*w(-1,1) + &      
                     sBx( 0,-2,+1)*w( 0,1) + &      
                     sBx(+1,-2,+1)*w( 1,1) + &      
                     sBx(+2,-2,+1)*w( 2,1))*dw(-2,2) + &
                    (sBx(-1,-1,+1)*w(-1,1) + &      
                     sBx( 0,-1,+1)*w( 0,1) + &      
                     sBx(+1,-1,+1)*w( 1,1) + &      
                     sBx(+2,-1,+1)*w( 2,1))*dw(-1,2) + &
                    (sBx(-1, 0,+1)*w(-1,1) + &      
                     sBx( 0, 0,+1)*w( 0,1) + &      
                     sBx(+1, 0,+1)*w( 1,1) + &      
                     sBx(+2, 0,+1)*w( 2,1))*dw( 0,2) + &
                    (sBx(-1,+1,+1)*w(-1,1) + &      
                     sBx( 0,+1,+1)*w( 0,1) + &      
                     sBx(+1,+1,+1)*w( 1,1) + &      
                     sBx(+2,+1,+1)*w( 2,1))*dw( 1,2) + &
                    (sBx(-1,+2,+1)*w(-1,1) + &      
                     sBx( 0,+2,+1)*w( 0,1) + &      
                     sBx(+1,+2,+1)*w( 1,1) + &      
                     sBx(+2,+2,+1)*w( 2,1))*dw( 2,2))*dw( 1,3) + &
                   ((sBx(-1,-2,+2)*w(-1,1) + &      
                     sBx( 0,-2,+2)*w( 0,1) + &      
                     sBx(+1,-2,+2)*w( 1,1) + &      
                     sBx(+2,-2,+2)*w( 2,1))*dw(-2,2) + &
                    (sBx(-1,-1,+2)*w(-1,1) + &      
                     sBx( 0,-1,+2)*w( 0,1) + &      
                     sBx(+1,-1,+2)*w( 1,1) + &      
                     sBx(+2,-1,+2)*w( 2,1))*dw(-1,2) + &
                    (sBx(-1, 0,+2)*w(-1,1) + &      
                     sBx( 0, 0,+2)*w( 0,1) + &      
                     sBx(+1, 0,+2)*w( 1,1) + &      
                     sBx(+2, 0,+2)*w( 2,1))*dw( 0,2) + &
                    (sBx(-1,+1,+2)*w(-1,1) + &      
                     sBx( 0,+1,+2)*w( 0,1) + &      
                     sBx(+1,+1,+2)*w( 1,1) + &      
                     sBx(+2,+1,+2)*w( 2,1))*dw( 1,2) + &
                    (sBx(-1,+2,+2)*w(-1,1) + &      
                     sBx( 0,+2,+2)*w( 0,1) + &      
                     sBx(+1,+2,+2)*w( 1,1) + &      
                     sBx(+2,+2,+2)*w( 2,1))*dw( 2,2))*dw( 2,3)
              !-----------------------------------
              ! ZX SHIFTED WEIGHTS
                pBy = &
                   ((sBy(-2,-1,-2)*dw(-2,1) + &
                     sBy(-1,-1,-2)*dw(-1,1) + &
                     sBy( 0,-1,-2)*dw( 0,1) + &
                     sBy(+1,-1,-2)*dw( 1,1) + &
                     sBy(+2,-1,-2)*dw( 2,1))*w(-1,2) + &
                    (sBy(-2, 0,-2)*dw(-2,1) + &
                     sBy(-1, 0,-2)*dw(-1,1) + &
                     sBy( 0, 0,-2)*dw( 0,1) + &
                     sBy(+1, 0,-2)*dw( 1,1) + &
                     sBy(+2, 0,-2)*dw( 2,1))*w( 0,2) + &
                    (sBy(-2,+1,-2)*dw(-2,1) + &
                     sBy(-1,+1,-2)*dw(-1,1) + &
                     sBy( 0,+1,-2)*dw( 0,1) + &
                     sBy(+1,+1,-2)*dw( 1,1) + &
                     sBy(+2,+1,-2)*dw( 2,1))*w( 1,2) + &
                    (sBy(-2,+2,-2)*dw(-2,1) + &
                     sBy(-1,+2,-2)*dw(-1,1) + &
                     sBy( 0,+2,-2)*dw( 0,1) + &
                     sBy(+1,+2,-2)*dw( 1,1) + &
                     sBy(+2,+2,-2)*dw( 2,1))*w( 2,2))*dw(-2,3) + &
                   ((sBy(-2,-1,-1)*dw(-2,1) + &
                     sBy(-1,-1,-1)*dw(-1,1) + &
                     sBy( 0,-1,-1)*dw( 0,1) + &
                     sBy(+1,-1,-1)*dw( 1,1) + &
                     sBy(+2,-1,-1)*dw( 2,1))*w(-1,2) + &
                    (sBy(-2, 0,-1)*dw(-2,1) + &
                     sBy(-1, 0,-1)*dw(-1,1) + &
                     sBy( 0, 0,-1)*dw( 0,1) + &
                     sBy(+1, 0,-1)*dw( 1,1) + &
                     sBy(+2, 0,-1)*dw( 2,1))*w( 0,2) + &
                    (sBy(-2,+1,-1)*dw(-2,1) + &
                     sBy(-1,+1,-1)*dw(-1,1) + &
                     sBy( 0,+1,-1)*dw( 0,1) + &
                     sBy(+1,+1,-1)*dw( 1,1) + &
                     sBy(+2,+1,-1)*dw( 2,1))*w( 1,2) + &
                    (sBy(-2,+2,-1)*dw(-2,1) + &
                     sBy(-1,+2,-1)*dw(-1,1) + &
                     sBy( 0,+2,-1)*dw( 0,1) + &
                     sBy(+1,+2,-1)*dw( 1,1) + &
                     sBy(+2,+2,-1)*dw( 2,1))*w( 2,2))*dw(-1,3) + &
                   ((sBy(-2,-1, 0)*dw(-2,1) + &
                     sBy(-1,-1, 0)*dw(-1,1) + &
                     sBy( 0,-1, 0)*dw( 0,1) + &
                     sBy(+1,-1, 0)*dw( 1,1) + &
                     sBy(+2,-1, 0)*dw( 2,1))*w(-1,2) + &
                    (sBy(-2, 0, 0)*dw(-2,1) + &
                     sBy(-1, 0, 0)*dw(-1,1) + &
                     sBy( 0, 0, 0)*dw( 0,1) + &
                     sBy(+1, 0, 0)*dw( 1,1) + &
                     sBy(+2, 0, 0)*dw( 2,1))*w( 0,2) + &
                    (sBy(-2,+1, 0)*dw(-2,1) + &
                     sBy(-1,+1, 0)*dw(-1,1) + &
                     sBy( 0,+1, 0)*dw( 0,1) + &
                     sBy(+1,+1, 0)*dw( 1,1) + &
                     sBy(+2,+1, 0)*dw( 2,1))*w( 1,2) + &
                    (sBy(-2,+2, 0)*dw(-2,1) + &
                     sBy(-1,+2, 0)*dw(-1,1) + &
                     sBy( 0,+2, 0)*dw( 0,1) + &
                     sBy(+1,+2, 0)*dw( 1,1) + &
                     sBy(+2,+2, 0)*dw( 2,1))*w( 2,2))*dw( 0,3) + &
                   ((sBy(-2,-1,+1)*dw(-2,1) + &
                     sBy(-1,-1,+1)*dw(-1,1) + &
                     sBy( 0,-1,+1)*dw( 0,1) + &
                     sBy(+1,-1,+1)*dw( 1,1) + &
                     sBy(+2,-1,+1)*dw( 2,1))*w(-1,2) + &
                    (sBy(-2, 0,+1)*dw(-2,1) + &
                     sBy(-1, 0,+1)*dw(-1,1) + &
                     sBy( 0, 0,+1)*dw( 0,1) + &
                     sBy(+1, 0,+1)*dw( 1,1) + &
                     sBy(+2, 0,+1)*dw( 2,1))*w( 0,2) + &
                    (sBy(-2,+1,+1)*dw(-2,1) + &
                     sBy(-1,+1,+1)*dw(-1,1) + &
                     sBy( 0,+1,+1)*dw( 0,1) + &
                     sBy(+1,+1,+1)*dw( 1,1) + &
                     sBy(+2,+1,+1)*dw( 2,1))*w( 1,2) + &
                    (sBy(-2,+2,+1)*dw(-2,1) + &
                     sBy(-1,+2,+1)*dw(-1,1) + &
                     sBy( 0,+2,+1)*dw( 0,1) + &
                     sBy(+1,+2,+1)*dw( 1,1) + &
                     sBy(+2,+2,+1)*dw( 2,1))*w( 2,2))*dw( 1,3) + &
                   ((sBy(-2,-1,+2)*dw(-2,1) + &
                     sBy(-1,-1,+2)*dw(-1,1) + &
                     sBy( 0,-1,+2)*dw( 0,1) + &
                     sBy(+1,-1,+2)*dw( 1,1) + &
                     sBy(+2,-1,+2)*dw( 2,1))*w(-1,2) + &
                    (sBy(-2, 0,+2)*dw(-2,1) + &
                     sBy(-1, 0,+2)*dw(-1,1) + &
                     sBy( 0, 0,+2)*dw( 0,1) + &
                     sBy(+1, 0,+2)*dw( 1,1) + &
                     sBy(+2, 0,+2)*dw( 2,1))*w( 0,2) + &
                    (sBy(-2,+1,+2)*dw(-2,1) + &
                     sBy(-1,+1,+2)*dw(-1,1) + &
                     sBy( 0,+1,+2)*dw( 0,1) + &
                     sBy(+1,+1,+2)*dw( 1,1) + &
                     sBy(+2,+1,+2)*dw( 2,1))*w( 1,2) + &
                    (sBy(-2,+2,+2)*dw(-2,1) + &
                     sBy(-1,+2,+2)*dw(-1,1) + &
                     sBy( 0,+2,+2)*dw( 0,1) + &
                     sBy(+1,+2,+2)*dw( 1,1) + &
                     sBy(+2,+2,+2)*dw( 2,1))*w( 2,2))*dw( 2,3)
              !-----------------------------------
              ! XY SHIFTED WEIGHTS
                pBz = &
                   ((sBz(-2,-2,-1)*dw(-2,1) + &
                     sBz(-1,-2,-1)*dw(-1,1) + &
                     sBz( 0,-2,-1)*dw( 0,1) + &
                     sBz(+1,-2,-1)*dw( 1,1) + &
                     sBz(+2,-2,-1)*dw( 2,1))*dw(-2,2) + &
                    (sBz(-2,-1,-1)*dw(-2,1) + &
                     sBz(-1,-1,-1)*dw(-1,1) + &
                     sBz( 0,-1,-1)*dw( 0,1) + &
                     sBz(+1,-1,-1)*dw( 1,1) + &
                     sBz(+2,-1,-1)*dw( 2,1))*dw(-1,2) + &
                    (sBz(-2, 0,-1)*dw(-2,1) + &
                     sBz(-1, 0,-1)*dw(-1,1) + &
                     sBz( 0, 0,-1)*dw( 0,1) + &
                     sBz(+1, 0,-1)*dw( 1,1) + &
                     sBz(+2, 0,-1)*dw( 2,1))*dw( 0,2) + &
                    (sBz(-2,+1,-1)*dw(-2,1) + &
                     sBz(-1,+1,-1)*dw(-1,1) + &
                     sBz( 0,+1,-1)*dw( 0,1) + &
                     sBz(+1,+1,-1)*dw( 1,1) + &
                     sBz(+2,+1,-1)*dw( 2,1))*dw( 1,2) + &
                    (sBz(-2,+2,-1)*dw(-2,1) + &
                     sBz(-1,+2,-1)*dw(-1,1) + &
                     sBz( 0,+2,-1)*dw( 0,1) + &
                     sBz(+1,+2,-1)*dw( 1,1) + &
                     sBz(+2,+2,-1)*dw( 2,1))*dw( 2,2))*w(-1,3) + &
                   ((sBz(-2,-2, 0)*dw(-2,1) + &
                     sBz(-1,-2, 0)*dw(-1,1) + &
                     sBz( 0,-2, 0)*dw( 0,1) + &
                     sBz(+1,-2, 0)*dw( 1,1) + &
                     sBz(+2,-2, 0)*dw( 2,1))*dw(-2,2) + &
                    (sBz(-2,-1, 0)*dw(-2,1) + &
                     sBz(-1,-1, 0)*dw(-1,1) + &
                     sBz( 0,-1, 0)*dw( 0,1) + &
                     sBz(+1,-1, 0)*dw( 1,1) + &
                     sBz(+2,-1, 0)*dw( 2,1))*dw(-1,2) + &
                    (sBz(-2, 0, 0)*dw(-2,1) + &
                     sBz(-1, 0, 0)*dw(-1,1) + &
                     sBz( 0, 0, 0)*dw( 0,1) + &
                     sBz(+1, 0, 0)*dw( 1,1) + &
                     sBz(+2, 0, 0)*dw( 2,1))*dw( 0,2) + &
                    (sBz(-2,+1, 0)*dw(-2,1) + &
                     sBz(-1,+1, 0)*dw(-1,1) + &
                     sBz( 0,+1, 0)*dw( 0,1) + &
                     sBz(+1,+1, 0)*dw( 1,1) + &
                     sBz(+2,+1, 0)*dw( 2,1))*dw( 1,2) + &
                    (sBz(-2,+2, 0)*dw(-2,1) + &
                     sBz(-1,+2, 0)*dw(-1,1) + &
                     sBz( 0,+2, 0)*dw( 0,1) + &
                     sBz(+1,+2, 0)*dw( 1,1) + &
                     sBz(+2,+2, 0)*dw( 2,1))*dw( 2,2))*w( 0,3) + &
                   ((sBz(-2,-2,+1)*dw(-2,1) + &
                     sBz(-1,-2,+1)*dw(-1,1) + &
                     sBz( 0,-2,+1)*dw( 0,1) + &
                     sBz(+1,-2,+1)*dw( 1,1) + &
                     sBz(+2,-2,+1)*dw( 2,1))*dw(-2,2) + &
                    (sBz(-2,-1,+1)*dw(-2,1) + &
                     sBz(-1,-1,+1)*dw(-1,1) + &
                     sBz( 0,-1,+1)*dw( 0,1) + &
                     sBz(+1,-1,+1)*dw( 1,1) + &
                     sBz(+2,-1,+1)*dw( 2,1))*dw(-1,2) + &
                    (sBz(-2, 0,+1)*dw(-2,1) + &
                     sBz(-1, 0,+1)*dw(-1,1) + &
                     sBz( 0, 0,+1)*dw( 0,1) + &
                     sBz(+1, 0,+1)*dw( 1,1) + &
                     sBz(+2, 0,+1)*dw( 2,1))*dw( 0,2) + &
                    (sBz(-2,+1,+1)*dw(-2,1) + &
                     sBz(-1,+1,+1)*dw(-1,1) + &
                     sBz( 0,+1,+1)*dw( 0,1) + &
                     sBz(+1,+1,+1)*dw( 1,1) + &
                     sBz(+2,+1,+1)*dw( 2,1))*dw( 1,2) + &
                    (sBz(-2,+2,+1)*dw(-2,1) + &
                     sBz(-1,+2,+1)*dw(-1,1) + &
                     sBz( 0,+2,+1)*dw( 0,1) + &
                     sBz(+1,+2,+1)*dw( 1,1) + &
                     sBz(+2,+2,+1)*dw( 2,1))*dw( 2,2))*w( 1,3) + &
                   ((sBz(-2,-2,+2)*dw(-2,1) + &
                     sBz(-1,-2,+2)*dw(-1,1) + &
                     sBz( 0,-2,+2)*dw( 0,1) + &
                     sBz(+1,-2,+2)*dw( 1,1) + &
                     sBz(+2,-2,+2)*dw( 2,1))*dw(-2,2) + &
                    (sBz(-2,-1,+2)*dw(-2,1) + &
                     sBz(-1,-1,+2)*dw(-1,1) + &
                     sBz( 0,-1,+2)*dw( 0,1) + &
                     sBz(+1,-1,+2)*dw( 1,1) + &
                     sBz(+2,-1,+2)*dw( 2,1))*dw(-1,2) + &
                    (sBz(-2, 0,+2)*dw(-2,1) + &
                     sBz(-1, 0,+2)*dw(-1,1) + &
                     sBz( 0, 0,+2)*dw( 0,1) + &
                     sBz(+1, 0,+2)*dw( 1,1) + &
                     sBz(+2, 0,+2)*dw( 2,1))*dw( 0,2) + &
                    (sBz(-2,+1,+2)*dw(-2,1) + &
                     sBz(-1,+1,+2)*dw(-1,1) + &
                     sBz( 0,+1,+2)*dw( 0,1) + &
                     sBz(+1,+1,+2)*dw( 1,1) + &
                     sBz(+2,+1,+2)*dw( 2,1))*dw( 1,2) + &
                    (sBz(-2,+2,+2)*dw(-2,1) + &
                     sBz(-1,+2,+2)*dw(-1,1) + &
                     sBz( 0,+2,+2)*dw( 0,1) + &
                     sBz(+1,+2,+2)*dw( 1,1) + &
                     sBz(+2,+2,+2)*dw( 2,1))*dw( 2,2))*w( 2,3)
!===============================================================================
! Particle Mover
!===============================================================================
                wa = pav(ip)%p(1)
                wb = pav(ip)%p(2)
                wc = pav(ip)%p(3)
                ! --------------------------------------------------------------
                ! Lorentz Force - Vay pusher.
                r(1) = hB * pBx
                r(2) = hB * pBy
                r(3) = hB * pBz
                sm1 = r(1)*r(1)+r(2)*r(2)+r(3)*r(3)
                o = 1./sqrt(1.+(wa*wa+wb*wb+wc*wc)*oc2)
                pBx = wa + hE*pEx + o*(wb*r(3)-wc*r(2))
                pBy = wb + hE*pEy + o*(wc*r(1)-wa*r(3))
                pBz = wc + hE*pEz + o*(wa*r(2)-wb*r(1))
                s = 1. + (pBx*pBx+pBy*pBy+pBz*pBz)*oc2 - sm1
                wd = (pBx*r(1) + pBy*r(2) + pBz*r(3))*oc
                o = sqrt2 * 1./sqrt(s + sqrt(s*s + 4.*(sm1 + wd*wd)))
                r(1) = r(1)*o
                r(2) = r(2)*o
                r(3) = r(3)*o
                s = 1./(1 + sm1*o*o)
                sm1 = pBx*r(1) + pBy*r(2) + pBz*r(3)
!===============================================================================
! Cooling
!===============================================================================
                if (do_cool_local) then
                  wa = s * (pBx + sm1*r(1) + pBy*r(3) - pBz*r(2))
                  wb = s * (pBy + sm1*r(2) + pBz*r(1) - pBx*r(3))
                  wc = s * (pBz + sm1*r(3) + pBx*r(2) - pBy*r(1))
                  p(1) = 0.5*(wa+pav(ip)%p(1))                                  ! time centered momentum
                  p(2) = 0.5*(wb+pav(ip)%p(2))
                  p(3) = 0.5*(wc+pav(ip)%p(3))
                  dp(1) = (wa - pav(ip)%p(1))/dtp4                               ! reconstruct total four acceleration
                  dp(2) = (wb - pav(ip)%p(2))/dtp4
                  dp(3) = (wc - pav(ip)%p(3))/dtp4
                  p2   = p(1)*p(1) + p(2)*p(2) + p(3)*p(3)                      ! u^2
                  cx   = dp(2)*p(3) - dp(3)*p(2)                                ! udot x u
                  cy   = dp(3)*p(1) - dp(1)*p(3)
                  cz   = dp(1)*p(2) - dp(2)*p(1)
                  p2   = p2 / sqrt(1. + p2*c%oc2)                               ! u^2 / gamma
                  og   = rfac*(dp(1)*dp(1)+dp(2)*dp(2)+dp(3)*dp(3) &
                       + (cx*cx+cy*cy+cz*cz)*c%oc2)/p2                          ! -Prad/mc^2 * gamma / u^2
                  wa = wa + p(1)*og + grav_acc(1)*dtp4                           ! update momentum with rad cool contribution
                  wb = wb + p(2)*og + grav_acc(2)*dtp4
                  wc = wc + p(3)*og + grav_acc(3)*dtp4
                else
                  wa = s * (pBx + sm1*r(1) + pBy*r(3) - pBz*r(2)) + grav_acc(1)*dtp4
                  wb = s * (pBy + sm1*r(2) + pBz*r(1) - pBx*r(3)) + grav_acc(2)*dtp4
                  wc = s * (pBz + sm1*r(3) + pBx*r(2) - pBy*r(1)) + grav_acc(3)*dtp4
                endif
                wd = wa*wa + wb*wb + wc*wc                                      ! momentum squared
                o  = 1./sqrt(1.+wd*oc2)                                         ! 1/gamma - 1/sqrt is fast
                pav(ip)%e = mass*wd*o/(1.+o)                                    ! kinetic energy
                o  = o*dt4
                r(1) = pav(ip)%r(1) + wa*o*ods(1)                               ! position upd
                r(2) = pav(ip)%r(2) + wb*o*ods(2)                               ! position upd
                r(3) = pav(ip)%r(3) + wc*o*ods(3)                               ! position upd
                ox = floor(r(1))
                oy = floor(r(2))
                oz = floor(r(3))
                if (max(abs(ox),abs(oy),abs(oz)) > 1) then
                  print'(a,i8,1p,16e9.1)',' W1:',ip,pav(ip)%r,r,wa,wb,wc,o,ods,dt4
                end if
                pav(ip)%q(1) = pav(ip)%q(1) + ox
                pav(ip)%q(2) = pav(ip)%q(2) + oy
                pav(ip)%q(3) = pav(ip)%q(3) + oz
                pav(ip)%r(1) = r(1) - ox
                pav(ip)%r(2) = r(2) - oy
                pav(ip)%r(3) = r(3) - oz
                pav(ip)%p(1) = wa                                               ! cowb back upd momentum
                pav(ip)%p(2) = wb
                pav(ip)%p(3) = wc
!===============================================================================
! Charge and current deposits
!===============================================================================
                ! ---------------------------------------------------------
                ! Find signed distance to center points in all directions.
                ! Project index into allowed range for interpolation.
                r = pav(ip)%r
                !-------------
                ! X_COORD CENTERED
                s = r(1); sm1 = 1. - s; o = 2. - s 
                we(3) = one_sixth * sm1*sm1*sm1
                we(4) = two_third - 0.5*s*s*o
                we(5) = two_third - 0.5*sm1*sm1*(1.+ s)
                we(6) = 1.0 - (we(3)+we(4)+we(5))
                if (1 > (2-ox) .or. 8 < (2-ox)) then
                  print'(a,2i8,1p,16e9.1)',' W2:',ip,ox,pav(ip)%r,r,wa,wb,wc,o,ods,dt4
                end if
                dw(-2,1) = we(2-ox)
                dw(-1,1) = we(3-ox)
                dw( 0,1) = we(4-ox)
                dw(+1,1) = we(5-ox)
                dw(+2,1) = we(6-ox)
                dw(+3,1) = we(7-ox)
                !-------------
                ! Y_COORD CENTERED
                s = r(2); sm1 = 1. - s; o = 2. - s 
                we(3) = one_sixth * sm1*sm1*sm1
                we(4) = two_third - 0.5*s*s*o
                we(5) = two_third - 0.5*sm1*sm1*(1.+ s)
                we(6) = 1.0 - (we(3)+we(4)+we(5))
                dw(-2,2) = we(2-oy)
                dw(-1,2) = we(3-oy)
                dw( 0,2) = we(4-oy)
                dw(+1,2) = we(5-oy)
                dw(+2,2) = we(6-oy)
                dw(+3,2) = we(7-oy)
                !-----  --------
                ! Z_COORD CENTERED
                s = r(3); sm1 = 1. - s; o = 2. - s 
                we(3) = one_sixth * sm1*sm1*sm1
                we(4) = two_third - 0.5*s*s*o
                we(5) = two_third - 0.5*sm1*sm1*(1.+ s)
                we(6) = 1.0 - (we(3)+we(4)+we(5))
                dw(-2,3) = we(2-oz)
                dw(-1,3) = we(3-oz)
                dw( 0,3) = we(4-oz)
                dw(+1,3) = we(5-oz)
                dw(+2,3) = we(6-oz)
                dw(+3,3) = we(7-oz)
                !---------------------------------------------------------------
                ! density
                wc = pav(ip)%w
                do jz=-1+oz,2+oz
                  wa = wc*dw(jz,3)
                  do jy=-1+oy,2+oy
                    s = dw(jy,2)*wa
                    d(-1+ox,jy,jz) = d(-1+ox,jy,jz) + dw(-1+ox,1)*s
                    d(   ox,jy,jz) = d( 0+ox,jy,jz) + dw(   ox,1)*s
                    d( 1+ox,jy,jz) = d( 1+ox,jy,jz) + dw( 1+ox,1)*s
                    d( 2+ox,jy,jz) = d( 2+ox,jy,jz) + dw( 2+ox,1)*s
                  enddo
                enddo
                !---------------------------------------------------------------
                ! Currents
                kx = min(-1+ox,-1); jxu=max(2+ox,2)
                ky = min(-1+oy,-1); jyu=max(2+oy,2)
                kz = min(-1+oz,-1); jzu=max(2+oz,2)
                !---------------------------------------------------------------
                ! x-current
                wc = wc * one_sixth
                wd = wc * dxdt
                do jz=kz,jzu
                  wa = wd * (2*w(jz,3) +   dw(jz,3))
                  wb = wd * (  w(jz,3) + 2*dw(jz,3))
                  do jy=ky,jyu
                    sm1 = wa*w(jy,2) + wb*dw(jy,2)
                    do jx=kx,jxu
                      v(jx,jy,jz,1)=v(jx,jy,jz,1) - (dw(jx,1) - w(jx,1))*sm1
                    enddo
                  enddo
                enddo
                !---------------------------------------------------------------
                ! y-current
                wd = wc * dydt
                do jx=kx,jxu
                  wa = wd * (2*w(jx,1) +   dw(jx,1))
                  wb = wd * (  w(jx,1) + 2*dw(jx,1))
                  do jz=kz,jzu
                    sm1 = wa*w(jz,3) + wb*dw(jz,3)
                    do jy=ky,jyu
                      v(jx,jy,jz,2)=v(jx,jy,jz,2) - (dw(jy,2) - w(jy,2))*sm1
                    enddo
                  enddo
                enddo
                !---------------------------------------------------------------
                ! z-current
                wd = wc * dzdt
                do jy=ky,jyu
                  wa = wd * (2*w(jy,2) +   dw(jy,2))
                  wb = wd * (  w(jy,2) + 2*dw(jy,2))
                  do jx=kx,jxu
                    sm1 = wa*w(jx,1) + wb*dw(jx,1)
                    do jz=kz,jzu
                      v(jx,jy,jz,3)=v(jx,jy,jz,3) - (dw(jz,3) - w(jz,3))*sm1
                    enddo
                  enddo
                enddo
              !-----------------------------------------------------------------
              ! End loop over particles
              enddo 
              !-----------------------------------------------------------------
              if (romp==1) then
                do kz=sdb,suc                                                   ! copy cached fluxes to fields
                  jz=kz+iz
                  if (jz <= 0 .or. jz > g%n(3)) cycle
                  do ky=sdb,suc
                    jy=ky+iy
                    if (jy <= 0 .or. jy > g%n(2)) cycle
                    do kx=sdb,suc
                      jx=kx+ix
                      if (jx <= 0 .or. jx > g%n(1)) cycle
                      fields(jx,jy,jz,isp)%d    = fields(jx,jy,jz,isp)%d    + d (kx,ky,kz)
                      fields(jx,jy,jz,isp)%v(1) = fields(jx,jy,jz,isp)%v(1) + v(kx,ky,kz,1)
                      fields(jx,jy,jz,isp)%v(2) = fields(jx,jy,jz,isp)%v(2) + v(kx,ky,kz,2)
                      fields(jx,jy,jz,isp)%v(3) = fields(jx,jy,jz,isp)%v(3) + v(kx,ky,kz,3)
                    enddo
                  enddo
                enddo
              else
                do kz=sdb,suc                                                   ! copy cached fluxes to fields
                  jz=kz+iz
                  if (jz <= 0 .or. jz > g%n(3)) cycle
                  do ky=sdb,suc
                    jy=ky+iy
                    if (jy <= 0 .or. jy > g%n(2)) cycle
                    do kx=sdb,suc
                      jx=kx+ix
                      if (jx <= 0 .or. jx > g%n(1)) cycle
                      lfields(jx,jy,jz,isp,romp)%d    = lfields(jx,jy,jz,isp,romp)%d    + d (kx,ky,kz)
                      lfields(jx,jy,jz,isp,romp)%v(1) = lfields(jx,jy,jz,isp,romp)%v(1) + v(kx,ky,kz,1)
                      lfields(jx,jy,jz,isp,romp)%v(2) = lfields(jx,jy,jz,isp,romp)%v(2) + v(kx,ky,kz,2)
                      lfields(jx,jy,jz,isp,romp)%v(3) = lfields(jx,jy,jz,isp,romp)%v(3) + v(kx,ky,kz,3)
                    enddo
                  enddo
                enddo
              endif
              particleupdates = particleupdates + np
            endif ! if particles in this species 
          enddo   ! loop over species
        else
          touched_row = .false.
        endif     ! if any particles in cell
        firstcellindex = fields(ix,iy,iz,:)%i + 1
      enddo       ! loop over x in cells
  enddo           ! collapsed loop over y and z in cells

  if (nomp > 1) then
    do izy=1,nspecies*g%n(2)*g%n(3)                                             ! collapse loop over species, iz, and iy
      isp = (izy-1) / (g%n(2)*g%n(3)) + 1
      iz  = (izy-1-(isp-1)*(g%n(2)*g%n(3)))/g%n(2) + 1
      iy  = izy - ((isp-1)*g%n(3) + (iz-1))*g%n(2)
      do iomp=2,nomp
      do ix=1,g%n(1)                                                              
        fields(ix,iy,iz,isp)%d    = fields(ix,iy,iz,isp)%d    + lfields(ix,iy,iz,isp,iomp)%d
        fields(ix,iy,iz,isp)%v(1) = fields(ix,iy,iz,isp)%v(1) + lfields(ix,iy,iz,isp,iomp)%v(1)
        fields(ix,iy,iz,isp)%v(2) = fields(ix,iy,iz,isp)%v(2) + lfields(ix,iy,iz,isp,iomp)%v(2)
        fields(ix,iy,iz,isp)%v(3) = fields(ix,iy,iz,isp)%v(3) + lfields(ix,iy,iz,isp,iomp)%v(3)
      enddo
      enddo
    enddo  
    deallocate(lfields)
  endif

  if (do_dump .or. do_compare .or. verbose>1) then
    allocate (scr(g%n(1),g%n(2),g%n(3)))
    do isp=1,nspecies
      scr=fields(:,:,:,isp)%d;    call dump (scr, 'd' )
      scr=fields(:,:,:,isp)%v(1); call dump (scr, 'vx')
      scr=fields(:,:,:,isp)%v(2); call dump (scr, 'vy')
      scr=fields(:,:,:,isp)%v(3); call dump (scr, 'vz')
      if (do_vth) then
        scr=fields(:,:,:,isp)%vth; call dump (scr, 'vth')
      end if
    end do
    deallocate (scr)
  end if
  
  call ParticleTotal                                                            ! Upd total nr of parts
                                         call timer('penta solve','start')
  call compute_flux_new                                                         ! Solve dJi/ds(i) = fields(...)%v(i)
                                         call timer('particle boundaries','start')
  call trace_exit('move_particles')
END SUBROUTINE move_particles
!-------------------------------------------------------------------------------
