! Patch/simple/stagger_6th.f90 $Id$
! vim: nowrap
!=======================================================================
MODULE stagger
  USE grid_m, ONLY : g
  USE params, ONLY : mpi
  implicit none
  real    :: aa, bb, cc, a1                                             ! stagger operator prefactors
  real    :: ax1, ay1, az1                                              ! do.
  real    :: ax3, ay3, az3                                              ! do.
  real    :: bx3, by3, bz3                                              ! do.
  real    :: cx3, cy3, cz3                                              ! do.
  real    :: lp0, lpm1, lpm2, lpm3, lpp1, lpp2, lpp3                    ! Laplace prefactors
  integer :: lb(3),ub(3)
  integer :: order=5
CONTAINS
!-----------------------------------------------------------------------
SUBROUTINE ddxdn_set (f, h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = (  ax3*(f(i   ,j,k)-f(i-1 ,j,k)) &
                + bx3*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
                + cx3*(f(i+2 ,j,k)-f(i-3 ,j,k))  )
  enddo
  enddo
  enddo
  call overlap(h)
END SUBROUTINE ddxdn_set
!----------------------------------------------------------------------
FUNCTION ddxdnf (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddxdnf
  integer                                            :: i, j, k
!.......................................................................
    ddxdnf = (  ax3*(f(i+1 ,j,k)-f(i   ,j,k)) &
              + bx3*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
              + cx3*(f(i+3 ,j,k)-f(i-2 ,j,k))  )
END FUNCTION ddxdnf
!-----------------------------------------------------------------------
FUNCTION ddxdna(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = ddxdnf(f,i,j,k)
  enddo
  enddo
  enddo
END FUNCTION ddxdna
!-----------------------------------------------------------------------
FUNCTION ddxdn(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cx3*(f(i+2 ,j,k)-f(i-3 ,j,k)) &
             + bx3*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
             + ax3*(f(i   ,j,k)-f(i-1 ,j,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION ddxdn
!-----------------------------------------------------------------------
FUNCTION ddxup(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cx3*(f(i+3 ,j,k)-f(i-2 ,j,k)) &
             + bx3*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
             + ax3*(f(i+1 ,j,k)-f(i   ,j,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION ddxup
!-----------------------------------------------------------------------
FUNCTION ddydn(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cy3*(f(i,j+2 ,k)-f(i,j-3 ,k)) &
             + by3*(f(i,j+1 ,k)-f(i,j-2 ,k)) &
             + ay3*(f(i,j   ,k)-f(i,j-1 ,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION ddydn
!-----------------------------------------------------------------------
FUNCTION ddyup(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cy3*(f(i,j+3 ,k)-f(i,j-2 ,k)) &
             + by3*(f(i,j+2 ,k)-f(i,j-1 ,k)) &
             + ay3*(f(i,j+1 ,k)-f(i,j   ,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION ddyup
!-----------------------------------------------------------------------
FUNCTION ddzdn(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cz3*(f(i,j,k+2 )-f(i,j,k-3 )) &
             + bz3*(f(i,j,k+1 )-f(i,j,k-2 )) &
             + az3*(f(i,j,k   )-f(i,j,k-1 ))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION ddzdn
!-----------------------------------------------------------------------
FUNCTION ddzup(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cz3*(f(i,j,k+3 )-f(i,j,k-2 )) &
             + bz3*(f(i,j,k+2 )-f(i,j,k-1 )) &
             + az3*(f(i,j,k+1 )-f(i,j,k   ))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION ddzup
!-----------------------------------------------------------------------
FUNCTION curlxdn(e,f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cy3*(e(i,j+2,k)-e(i,j-3,k)) &
             + by3*(e(i,j+1,k)-e(i,j-2,k)) &
             + ay3*(e(i,j  ,k)-e(i,j-1,k)) &
             - cz3*(f(i,j,k+2)-f(i,j,k-3)) &
             - bz3*(f(i,j,k+1)-f(i,j,k-2)) &
             - az3*(f(i,j,k  )-f(i,j,k-1))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlxdn
!-----------------------------------------------------------------------
FUNCTION curlydn(e,f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cz3*(e(i,j,k+2)-e(i,j,k-3)) &
             + bz3*(e(i,j,k+1)-e(i,j,k-2)) &
             + az3*(e(i,j,k  )-e(i,j,k-1)) &
             - cx3*(f(i+2,j,k)-f(i-3,j,k)) &
             - bx3*(f(i+1,j,k)-f(i-2,j,k)) &
             - ax3*(f(i  ,j,k)-f(i-1,j,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlydn
!-----------------------------------------------------------------------
FUNCTION curlzdn(e,f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cx3*(e(i+2,j,k)-e(i-3,j,k)) &
             + bx3*(e(i+1,j,k)-e(i-2,j,k)) &
             + ax3*(e(i  ,j,k)-e(i-1,j,k)) &
             - cy3*(f(i,j+2,k)-f(i,j-3,k)) &
             - by3*(f(i,j+1,k)-f(i,j-2,k)) &
             - ay3*(f(i,j,  k)-f(i,j-1,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlzdn
!-----------------------------------------------------------------------
FUNCTION curlxup(e,f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cy3*(e(i,j+3,k)-e(i,j-2,k)) &
             + by3*(e(i,j+2,k)-e(i,j-1,k)) &
             + ay3*(e(i,j+1,k)-e(i,j  ,k)) &
             - cz3*(f(i,j,k+3)-f(i,j,k-2)) &
             - bz3*(f(i,j,k+2)-f(i,j,k-1)) &
             - az3*(f(i,j,k+1)-f(i,j,k  ))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlxup
!-----------------------------------------------------------------------
FUNCTION curlyup(e,f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cz3*(e(i,j,k+3)-e(i,j,k-2)) &
             + bz3*(e(i,j,k+2)-e(i,j,k-1)) &
             + az3*(e(i,j,k+1)-e(i,j,k  )) &
             - cx3*(f(i+3,j,k)-f(i-2,j,k)) &
             - bx3*(f(i+2,j,k)-f(i-1,j,k)) &
             - ax3*(f(i+1,j,k)-f(i  ,j,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlyup
!-----------------------------------------------------------------------
FUNCTION curlzup(e,f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cx3*(e(i+3,j,k)-e(i-2,j,k)) &
             + bx3*(e(i+2,j,k)-e(i-1,j,k)) &
             + ax3*(e(i+1,j,k)-e(i  ,j,k)) &
             - cy3*(f(i,j+3,k)-f(i,j-2,k)) &
             - by3*(f(i,j+2,k)-f(i,j-1,k)) &
             - ay3*(f(i,j+1,k)-f(i,j  ,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlzup
!=======================================================================
FUNCTION laplace (f) result(h)
  USE grid_m, ONLY: odxq, odyq, odzq
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f, h
  integer                               :: ix, iy, iz 
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(ix,iy,iz)
  do iz=lb(3),ub(3)
  do iy=lb(2),ub(2)
  do ix=lb(1),ub(1)
    h(ix,iy,iz) = (  lpm3 * f(ix-3,iy,iz) &
                   + lpm2 * f(ix-2,iy,iz) &
                   + lpm1 * f(ix-1,iy,iz) &
                   +  lp0 * f(ix  ,iy,iz) &
                   + lpp1 * f(ix+1,iy,iz) &
                   + lpp2 * f(ix+2,iy,iz) &
                   + lpp3 * f(ix+3,iy,iz)  ) * odxq &
                + (  lpm3 * f(ix,iy-3,iz) &
                   + lpm2 * f(ix,iy-2,iz) &
                   + lpm1 * f(ix,iy-1,iz) &
                   +  lp0 * f(ix,iy  ,iz) &
                   + lpp1 * f(ix,iy+1,iz) &
                   + lpp2 * f(ix,iy+2,iz) &
                   + lpp3 * f(ix,iy+3,iz)  ) * odyq &
                + (  lpm3 * f(ix,iy,iz-3) &
                   + lpm2 * f(ix,iy,iz-2) &
                   + lpm1 * f(ix,iy,iz-1) &
                   +  lp0 * f(ix,iy,iz  ) &
                   + lpp1 * f(ix,iy,iz+1) &
                   + lpp2 * f(ix,iy,iz+2) &
                   + lpp3 * f(ix,iy,iz+3)  ) * odzq
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION laplace
!=======================================================================
FUNCTION laplace_nl (f,a,b,f1) result(h)
  USE grid_m, ONLY: odxq, odyq, odzq
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f, f1, h
  real                                  :: a, b
  integer                               :: ix, iy, iz, lb(3), ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(ix,iy,iz)
  do iz=lb(3),ub(3)
  do iy=lb(2),ub(2)
  do ix=lb(1),ub(1)
    h(ix,iy,iz) = (f1(ix,iy,iz) &
                + (  lpm3 * f(ix-3,iy,iz) &
                   + lpm2 * f(ix-2,iy,iz) &
                   + lpm1 * f(ix-1,iy,iz) &
                   + lpp1 * f(ix+1,iy,iz) &
                   + lpp2 * f(ix+2,iy,iz) &
                   + lpp3 * f(ix+3,iy,iz)  ) * (odxq*a) &
                + (  lpm3 * f(ix,iy-3,iz) &
                   + lpm2 * f(ix,iy-2,iz) &
                   + lpm1 * f(ix,iy-1,iz) &
                   + lpp1 * f(ix,iy+1,iz) &
                   + lpp2 * f(ix,iy+2,iz) &
                   + lpp3 * f(ix,iy+3,iz)  ) * (odyq*a) &
                + (  lpm3 * f(ix,iy,iz-3) &
                   + lpm2 * f(ix,iy,iz-2) &
                   + lpm1 * f(ix,iy,iz-1) &
                   + lpp1 * f(ix,iy,iz+1) &
                   + lpp2 * f(ix,iy,iz+2) &
                   + lpp3 * f(ix,iy,iz+3)  ) * (odzq*a)) * b
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION laplace_nl

!=======================================================================
FUNCTION poisson_filter(f,filter,f_2nd) result(h)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f, h
  real                                  :: filter, f_2nd, a, norm
  integer                               :: ix, iy, iz, lb(3), ub(3)
!.......................................................................
  a = filter/3.
  norm = 1./(1.+(3*lp0-3*lpp1-3*lpm1+3*lpp2+3*lpm2-3*lpp3-3*lpm3 - 12.*f_2nd)*a)
  if (norm .lt. 0.) then
    print *, 'Normalisation :', norm
    print *, 'Filter        :', filter
    print *, '2nd order part:', f_2nd
    print *, 'Max filter    :', abs(3. / (3*lp0-3*lpp1-3*lpm1+3*lpp2+3*lpm2-3*lpp3-3*lpm3 - 12*f_2nd))
    call error('e_fix','Your filter value is too high. Normalisation becoming negative')
  endif

  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb = max(lb,4)
  ub = min(ub,g%n-3)
  !$do
  do iz=lb(3),ub(3)
  do iy=lb(2),ub(2)
  do ix=lb(1),ub(1)
    h(ix,iy,iz) = norm*((1.+(3.*lp0 - 6.*f_2nd)*a) * f(ix,iy,iz) &
                + a * (  lpm3 * f(ix-3,iy  ,iz  )&
                       + lpm2 * f(ix-2,iy  ,iz  )&
               + (lpm1+f_2nd) * f(ix-1,iy  ,iz  )&
               + (lpp1+f_2nd) * f(ix+1,iy  ,iz  )&
                       + lpp2 * f(ix+2,iy  ,iz  )&
                       + lpp3 * f(ix+3,iy  ,iz  ))&
                + a * (  lpm3 * f(ix  ,iy-3,iz  )&
                       + lpm2 * f(ix  ,iy-2,iz  )&
               + (lpm1+f_2nd) * f(ix  ,iy-1,iz  )&
               + (lpp1+f_2nd) * f(ix  ,iy+1,iz  )&
                       + lpp2 * f(ix  ,iy+2,iz  )&
                       + lpp3 * f(ix  ,iy+3,iz  ))&
                + a * (  lpm3 * f(ix  ,iy  ,iz-3)&
                       + lpm2 * f(ix  ,iy  ,iz-2)&
               + (lpm1+f_2nd) * f(ix  ,iy  ,iz-1)&
               + (lpp1+f_2nd) * f(ix  ,iy  ,iz+1)&
                       + lpp2 * f(ix  ,iy  ,iz+2)&
                       + lpp3 * f(ix  ,iy  ,iz+3)))
  enddo
  enddo
  enddo
  call overlap(h)
END FUNCTION poisson_filter

!-----------------------------------------------------------------------
END MODULE stagger
!=======================================================================
SUBROUTINE test_stagger
!
!  Check the correctness of the stagger operators
!
  USE params,  ONLY: mpi, periodic, rank, master, mid, do_validate
  USE grid_m,  ONLY: g, bx, by, ex, ey, ez
  use units,   ONLY: c
  use stagger, ONLY: ddxdn, ddydn, ddzdn, &
                     ddxup, ddyup, ddzup, &
                     ddxdn_set, ddxdna, &
                     laplace
  implicit none
  integer i,j,k, jx,jy,jz, mx, my, mz
  real epsx, epsy, epsz, eps1, eps2, eps3, fx, fy, fz, dx, dy, dz, pi
  real, dimension(:,:,:), allocatable :: scr, f
  logical ok, all_mpi
  real(kind=8):: wc, wallclock, test_time=0.5
  integer niter, maxiter

  character(len=mid):: id = &
    'Patch/simple/stagger_6th.f90 $Id$'

  call print_id(id)

  mx=g%gn(1); my=g%gn(2); mz=g%gn(3); dx=g%ds(1); dy=g%ds(2); dz=g%ds(3)
  allocate(scr(g%n(1),g%n(2),g%n(3)),f(g%n(1),g%n(2),g%n(3)))

  pi = c%pi
  fx = 2.*pi/(mx*dx)
  fy = 2.*pi/(my*dy)
  fz = 2.*pi/(mz*dz)
  ok = .true.

  do k=1,g%n(3)
  do j=1,g%n(2)
  do i=1,g%n(1)
    jx = i-g%lb(1)+mpi%offset(1); jy = j-g%lb(2)+mpi%offset(2); jz = k-g%lb(3)+mpi%offset(3)
    bx(i,j,k)=sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)=sin(2.*pi*real(jy)/real(my))
    f (i,j,k)=sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=cos(2.*pi*real(jx-0.5)/real(mx))*fx
    ey(i,j,k)=cos(2.*pi*real(jy-0.5)/real(my))*fy
    ez(i,j,k)=cos(2.*pi*real(jz-0.5)/real(mz))*fz
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(f)
  call overlap(ex); call overlap(ey); call overlap(ez)

  
  maxiter = 1e8/(mx*my*mz)
  niter = 0
  wc = wallclock()
  do while (wallclock()-wc < test_time)
    do i=1,maxiter/30
      call ddxdn_set (bx, scr)
    enddo
    niter = niter+maxiter/30
  end do
  wc = wallclock()-wc
  if (.not. do_validate) &
    print 1, 'ddxdn subroutine :', (wc*1e9*test_time/niter)/(mx*my*mz), ' ns/p'
1 format (1x,a,f7.2,a)
  niter = 0
  wc = wallclock()
  do while (wallclock()-wc < test_time)
    do i=1,maxiter/30
      scr = ddxdna(bx)
    enddo
    niter = niter+maxiter/30
  enddo
  wc = wallclock()-wc
  if (.not. do_validate) &
    print 1, 'ddxdna function  :', (wc*1e9*test_time/niter)/(mx*my*mz), ' ns/p'

  niter = 0
  wc = wallclock()
  do while (wallclock()-wc < test_time)
    do i=1,maxiter/30
      scr = ddxdn(bx)
    enddo
    niter = niter+maxiter/30
  enddo
  wc = wallclock()-wc
  if (.not. do_validate) &
    print 1, 'ddxdn function   :', (wc*1e9*test_time/niter)/(mx*my*mz), ' ns/p'

  scr=ddxdn(bx); bx=scr
  bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ex(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsx=maxval(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(1).eq.1) epsx=0.
  scr = ddydn(by); by=scr
  by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ey(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsy=maxval(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(2).eq.1) epsy=0.
  scr = ddzdn(f); f=scr
  f(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(f(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ez(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsz=maxval(f(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(3).eq.1) epsz=0.
  call overlap(bx); call overlap(by); call overlap(f)
  eps1 = max(0.5*fx**3*dx**2/(3.*2.*1.),1.e-6/dx)
  eps2 = max(0.5*fy**3*dy**2/(3.*2.*1.),1.e-6/dy)
  eps3 = max(0.5*fz**3*dz**2/(3.*2.*1.),1.e-6/dz)
  ok = ok .and. (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  if (.not. ok) print'(a,i5,1p,6g12.4,l4)',' ddn     :', rank, epsx, eps1, epsy, eps2, epsz, eps3, ok

  do k=1,g%n(3)
  do j=1,g%n(2)
  do i=1,g%n(1)
    jx = i-g%lb(1)+mpi%offset(1); jy = j-g%lb(2)+mpi%offset(2); jz = k-g%lb(3)+mpi%offset(3)
    bx(i,j,k)=sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)=sin(2.*pi*real(jy)/real(my))
    f(i,j,k)=sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=cos(2.*pi*real(jx+0.5)/real(mx))*fx
    ey(i,j,k)=cos(2.*pi*real(jy+0.5)/real(my))*fy
    ez(i,j,k)=cos(2.*pi*real(jz+0.5)/real(mz))*fz
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(f)
  call overlap(ex); call overlap(ey); call overlap(ez)
  scr = ddxup(bx); bx=scr
  bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ex(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsx=maxval(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(1).eq.1) epsx=0.
  scr = ddyup(by); by=scr
  by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ey(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsy=maxval(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(2).eq.1) epsy=0.
  scr = ddzup(f); f=scr
  f(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=abs(f(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
                                                                ez(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsz=maxval(f(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(3).eq.1) epsz=0.
  eps1 = max(0.5*fx**3*dx**2/(3.*2.*1.),1.e-6/dx)
  eps2 = max(0.5*fy**3*dy**2/(3.*2.*1.),1.e-6/dy)
  eps3 = max(0.5*fz**3*dz**2/(3.*2.*1.),1.e-6/dz)
  ok = ok .and. (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  if (.not. ok) print'(a,i5,1p,6g12.4,l4)',' dup     :', rank, epsx, eps1, epsy, eps2, epsz, eps3, ok

  do k=1,g%n(3); do j=1,g%n(2); do i=1,g%n(1)
    jx = i-g%lb(1)+mpi%offset(1); jy = j-g%lb(2)+mpi%offset(2); jz = k-g%lb(3)+mpi%offset(3)
    bx(i,j,k) = sin(2.*pi*real(jx)/real(mx)) &
              + sin(2.*pi*real(jy)/real(my)) &
              + sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k) =-sin(2.*pi*real(jx)/real(mx))*fx**2 &
              - sin(2.*pi*real(jy)/real(my))*fy**2 &
              - sin(2.*pi*real(jz)/real(mz))*fz**2
  end do; end do; end do
  call overlap(bx); call overlap(by); call overlap(f)
  call overlap(ex); call overlap(ey); call overlap(ez)
  scr = laplace(bx); bx=scr
  bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) = &
    abs(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) &
  -     ex(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsx = maxval(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)); if (g%gn(1).eq.1) epsx=0.
  eps1 = 2.*max(fx**4*dx**2/(3.*2.*1.),2.e-6*fx/dx)
  ok = ok .and. (epsx < eps1)
  if (.not. ok .and. rank < 10) print'(a,i5,1p,6g12.4,l4)',' d2d[xyz]:', rank, epsx, eps1, epsy, eps2, epsz, eps3, ok

  ok = all_mpi(ok)
  if (ok) then
    if (master) print *,'the stagger routines appear to be working correctly'
  else
    call warning('stagger_test','THERE APPEARS TO BE A PROBLEM WITH THE STAGGER ROUTINES')
  end if

  deallocate (scr, f)
END SUBROUTINE test_stagger
!----------------------------------------------------------------------

SUBROUTINE compute_stagger_prefactors                                  ! compute prefactors for the stagger operators, they depend on order in finite diff.
USE grid_m,  ONLY: g
USE stagger, ONLY: aa,  bb,  cc, a1,     &
                   ax1, ay1, az1,      &
                   ax3, ay3, az3,      &
                   bx3, by3, bz3,      &
                   cx3, cy3, cz3,      &
                   lp0, lpm1, lpm2,    &
                   lpm3, lpp1, lpp2,   &
                   lpp3, order
  implicit none

  integer :: i
  real    :: dx, dy, dz

   !----------------
    dx = g%ds(1)
    dy = g%ds(2)
    dz = g%ds(3)

    cc=3./256.   ; cx3=(-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3.))
    bb=-25./256. ; cy3=(-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3.))
    aa=.5-bb-cc    ; cz3=(-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3.))
    bx3=(-1.-120.*cx3)/24.    ; by3=(-1.-120.*cy3)/24.    ; bz3=(-1.-120.*cz3)/24.
    ax3=(1.-3.*bx3-5.*cx3)/dx ; ay3=(1.-3.*by3-5.*cy3)/dy ; az3=(1.-3.*bz3-5.*cz3)/dz
    bx3=bx3/dx                ; by3=by3/dy                ; bz3=bz3/dz
    cx3=cx3/dx                ; cy3=cy3/dy                ; cz3=cz3/dz

    lpm3 = +   2./180.
    lpm2 = -  27./180.
    lpm1 = + 270./180.
    lp0  = - 490./180.
    lpp1 = + 270./180.
    lpp2 = -  27./180.
    lpp3 = +   2./180.
   !----------------

  call test_stagger
  
END SUBROUTINE compute_stagger_prefactors
!-----------------------------------------------------------------------


!=======================================================================
!CONVENTIONS:
!
!
!
!
!GRID:	i-3		i-2		i-1		i		i+1		i+2		i+3		i+1
!
!				ddxdn[i,j,k](i-3,i-2,i-1,i,i+1,i+2) / (i-2,i-1,i,i+1) / (i-1,i)
!							|
!		----------------------------------------|---------------------------
!		|		|		|		|		|		|
!X	O	X	O	X	O	X	O	X	O	X	O	X	O	X	O
!			|		|		|		|		|		|
!			----------------------------------------|-------------------------
!								|
!				ddxup[i,j,k](i-2,i-1,i,i+1,i+2,i+3) / (i-1,i,i+1,i+2) / (i,i+1)
!
!=======================================================================


!From the coefficients: 
!    bz3=-1./24.
!    az3=(1. - 3.*bz3)/dz
!    bz3=bz3/dz
!
! we get for example that 
!    ddzup  = (  az3*(f(i,j,k+1 )-f(i,j,k   )) &
!              + bz3*(f(i,j,k+2 )-f(i,j,k-1 ))  )
!
! can be rewritten as
!    ddzup  = ( -f[+3/2] + 27*f[+1/2] - 27*f[-1/2]  + 1*f[-3/2] )/(24*dz).
!
! which is the correct representation on HALF-CENTERED grids. 
!
!We should, however, use the corresponding CENTERED LaPlace 
! operator to keep things in line, since we're finding d^2/d[Q]^2
! at it's 'own' point.
