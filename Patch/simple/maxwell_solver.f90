! $Id$
! vim: nowrap
!=======================================================================!
MODULE maxwell_solver
  USE grid_m,          ONLY: g, o_ngrid                                 ! "The Grid"
  USE units,           ONLY: c, elm                                     ! unit system constants.
  USE grid_m,          ONLY: odx, ody, odz, odxq, odyq, odzq,bx,by,bz   ! grid props
  USE params,          ONLY: nspecies, &                                ! src arrs, nr of species
                              mid, dt, &                                ! other params
                              out_namelists, trace, dbg, mdim, periodic ! debug levels
  implicit none
  real    :: tol_b, tol_e, tol_divb                                     ! tolerance limits
  integer :: maxiter_b, maxiter_e, miniter_e, iter_b, iter_e, &         ! tolerance controls
             miniter_divb, maxiter_divb, iter_divb, every_b, every_e, &
             lb(3), ub(3)
!-----------------------------------------------------------------------! 
! Prefactors and variables needed for the integration sceheme
!  - common to all finit diff orders.
!-----------------------------------------------------------------------! 
  real    :: emf, omemf                                                 ! time de-centering
  real    :: filter, sor, f_2nd, f_6th                                  ! poisson filter
  real    :: dd1e, dd1b                                                 ! e_fix and b_fix
  real    :: ce1, ce2, ce3                                              ! e_push
  real    :: cb1, cb2, cb3, cb4, c4bx, c4by, c4bz, denom                ! b_push
  logical :: do_explicit                                                ! simple b_push
CONTAINS

!=======================================================================!
SUBROUTINE b_push
  USE params,          ONLY: stdout, master, time, stdall, mpi, &
                             do_b_fix, it, dt
  USE grid_m,          ONLY: ex, ey, ez    ! fields needed
  USE grid_m,          ONLY: bx, by, bz    ! more fields needed
  USE maxwell_sources, ONLY: verbose
  USE maxwell_sources, ONLY: bxm, bym, bzm
  USE dumps,           ONLY: dump
  USE math,            ONLY: divstat
  USE stagger,         ONLY: curlxup, curlyup, curlzup
  implicit none
  logical, save :: calldivb=.true.
  integer ::  iz
  real maxerrp
!.......................................................................!
  call trace_enter('B_PUSH')
!-----------------------------------------------------------------------!
! Step the B-field
!-----------------------------------------------------------------------!
  lb =       g%lb               
  ub = merge(g%ub, g%ub-1, mpi%ub)                                      ! proper boundaries

  if (do_explicit) then
    bxm = curlxup (ez, ey)
    bym = curlyup (ex, ez)
    bzm = curlzup (ey, ex)
    do iz=lb(3),ub(3)
           bx(lb(1):ub(1),lb(2):ub(2),iz) = & 
           bx(lb(1):ub(1),lb(2):ub(2),iz) - &
      cb1*bxm(lb(1):ub(1),lb(2):ub(2),iz)

           by(lb(1):ub(1),lb(2):ub(2),iz) = & 
           by(lb(1):ub(1),lb(2):ub(2),iz) - &
      cb1*bym(lb(1):ub(1),lb(2):ub(2),iz)

           bz(lb(1):ub(1),lb(2):ub(2),iz) = & 
           bz(lb(1):ub(1),lb(2):ub(2),iz) - &
      cb1*bzm(lb(1):ub(1),lb(2):ub(2),iz)
    end do
  else
    call b_push_implicit
  endif
!-----------------------------------------------------------------------!
! Set boundary components of b-field
!-----------------------------------------------------------------------!
  call b_boundaries(bx,by,bz)
                                                      call dump(bx,'Bx')
                                                      call dump(by,'By')
                                                      call dump(bz,'Bz')
!-----------------------------------------------------------------------!
! Possibly check div(B) for roundoff error build-up
!-----------------------------------------------------------------------!
  iter_divb = 0
  if (calldivb) call divstat ('B', bx, by, bz)
  if (do_b_fix .and. mod(it,every_b)==0) then 
    call b_fix (bx, by, bz)
    if (calldivb) call divstat ('B(fixed)', bx, by, bz)
  endif
  calldivb = .false.

  call trace_exit('B_PUSH')
END SUBROUTINE b_push

!=======================================================================!
SUBROUTINE e_push
  USE params,          ONLY: stdout, master, time, stdall, mpi, &
                             do_e_fix, it, dt
  USE grid_m,          ONLY: ex, ey, ez    ! fields needed
  USE grid_m,          ONLY: bx, by, bz    ! more fields needed
  USE maxwell_sources, ONLY: verbose
  USE maxwell_sources, ONLY: sx, sy, sz, jx, jy, jz, charge
  USE maxwell_sources, only: exa=>bxm, eya=>bym, eza=>bzm
  USE dumps,           ONLY: dump
  USE math,            ONLY: divstat
  USE stagger,         ONLY: curlxdn, curlydn, curlzdn
  USE stat,            ONLY: energy_e_box, prefe
  implicit none
  logical, save :: calldivb=.true.
  integer ::  iz, lb(3), ub(3)
  real(kind=8) :: total3
  real maxerrp
!.......................................................................!
  call trace_enter('E_PUSH')
!-----------------------------------------------------------------------!
! Step the E-field
!-----------------------------------------------------------------------!
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)

  do iz=lb(3),ub(3)
    exa(:,:,iz) = ex(:,:,iz)                                            ! store current ..
    eya(:,:,iz) = ey(:,:,iz)                                            ! .. values for ..
    eza(:,:,iz) = ez(:,:,iz)                                            ! .. Io/stat.f90
  end do

  if (do_explicit) then
    sx = curlxdn (bz, by)
    sy = curlydn (bx, bz)
    sz = curlzdn (by, bx)
    do iz=lb(3),ub(3)
           ex(lb(1):ub(1),lb(2):ub(2),iz) = & 
           ex(lb(1):ub(1),lb(2):ub(2),iz) + &
      ce1*(sx(lb(1):ub(1),lb(2):ub(2),iz) - &
       ce2*jx(lb(1):ub(1),lb(2):ub(2),iz))
        
           ey(lb(1):ub(1),lb(2):ub(2),iz) = & 
           ey(lb(1):ub(1),lb(2):ub(2),iz) + &
      ce1*(sy(lb(1):ub(1),lb(2):ub(2),iz) - &
       ce2*jy(lb(1):ub(1),lb(2):ub(2),iz))
      
           ez(lb(1):ub(1),lb(2):ub(2),iz) = & 
           ez(lb(1):ub(1),lb(2):ub(2),iz) + &
      ce1*(sz(lb(1):ub(1),lb(2):ub(2),iz) - &
       ce2*jz(lb(1):ub(1),lb(2):ub(2),iz))
    end do
  else
    call e_push_implicit
  endif

  do iz=lb(3),ub(3)
    sx(:,:,iz) = (ex(:,:,iz)+exa(:,:,iz))**2 &
               + (ey(:,:,iz)+eya(:,:,iz))**2 &
               + (ez(:,:,iz)+eza(:,:,iz))**2
  end do
  energy_e_box = 0.25*prefe*total3(sx)*g%dV

!-----------------------------------------------------------------------!
! Set boundary components of E-field
!-----------------------------------------------------------------------!
  call e_boundaries (ex, ey, ez)
                                                      call dump(ex,'Ex')
                                                      call dump(ey,'Ey')
                                                      call dump(ez,'Ez')
!-----------------------------------------------------------------------!
! Possibly check div(E) for roundoff error build-up
!-----------------------------------------------------------------------!
  if (do_e_fix .and. mod(it,every_e)==0) then 
    call e_fix (ex, ey, ez, charge)
  endif

  call trace_exit('E_PUSH')
END SUBROUTINE e_push

!=======================================================================!
SUBROUTINE b_push_implicit
  USE params,          ONLY: stdout, master, time, stdall, mpi, &
                             do_b_fix, it, dt, rank
  USE grid_m,          ONLY: ex, ey, ez    ! fields needed
  USE grid_m,          ONLY: bx, by, bz    ! more fields needed
  USE maxwell_sources, ONLY: jx, jy, jz    ! more fields needed
  USE maxwell_sources, ONLY: verbose, bmax
  USE maxwell_sources, ONLY: bxa, bya, bza
  USE maxwell_sources, ONLY: bxm, bym, bzm
  USE maxwell_sources, ONLY: sx,  sy,  sz
  USE dumps,           ONLY: dump
  USE math,            ONLY: divstat
  USE stagger,         ONLY: ddxup, ddyup, ddzup, laplace, laplace_nl
  implicit none
  integer        :: ix, iy, iz
  logical, save  :: calldivb = .true.
  real           :: max4, errp, maxerr, max_scalar
!.......................................................................!
  call trace_enter('B_PUSH')

  iter_b = 0                                                            ! new timestep
!----------------------------------------------------------------------!
! Reset - then construct - current density on interior.
!----------------------------------------------------------------------!
                                                    call dump(ez,'ex0')
                                                    call dump(ey,'ey0')
                                                    call dump(ez,'ez0')
!-----------------------------------------------------------------------!
! S = B - cb1 curl(E) + cb2 durl(J) + laplace(B)
!-----------------------------------------------------------------------!
  sx = cb2*(ddyup(jz)-ddzup(jy)) &
     - cb1*(ddyup(ez)-ddzup(ey)) &
     + cb3*laplace(bx)
  sy = cb2*(ddzup(jx)-ddxup(jz)) &
     - cb1*(ddzup(ex)-ddxup(ez)) &
     + cb3*laplace(by)
  sz = cb2*(ddxup(jy)-ddyup(jx)) &
     - cb1*(ddxup(ey)-ddyup(ex)) &
     + cb3*laplace(bz)
  do iz = lb(3),ub(3)
  do iy = lb(2),ub(2)
  do ix = lb(1),ub(1)
    sx (ix,iy,iz) = sx(ix,iy,iz) + bx(ix,iy,iz)
    sy (ix,iy,iz) = sy(ix,iy,iz) + by(ix,iy,iz)
    sz (ix,iy,iz) = sz(ix,iy,iz) + bz(ix,iy,iz)
    bxa(ix,iy,iz) = bx(ix,iy,iz)
    bya(ix,iy,iz) = by(ix,iy,iz)
    bza(ix,iy,iz) = bz(ix,iy,iz)
    bzm(ix,iy,iz) = bx(ix,iy,iz)**2+by(ix,iy,iz)**2+bz(ix,iy,iz)**2
  end do
  end do
  end do
  call b_boundaries(sx,sy,sz)
  bmax = sqrt(max4(bxm))
                                                      call dump(sx,'sx')
                                                      call dump(sy,'sy')
                                                      call dump(sz,'sz')
                                                     call dump(bx,'bxa')
                                                     call dump(by,'bya')
                                                     call dump(bz,'bza')
!-----------------------------------------------------------------------!
! Now move, NB!  Bza is defined on {lb-1:ub},{lb-1:ub},{lb-1:ub+1}
!-----------------------------------------------------------------------!
SOLVE_LOOP : do while (iter_b <= maxiter_b)                             ! max maxiter_b 

  bxm = laplace_nl(bxa,cb4,denom,sx)
  bym = laplace_nl(bya,cb4,denom,sy)
  bzm = laplace_nl(bza,cb4,denom,sz)
  call b_boundaries(bxm,bym,bzm)
                                                    call dump(bxm,'bxm')
                                                    call dump(bym,'bym')
                                                    call dump(bzm,'bzm')
!-----------------------------------------------------------------------!
! Produce max error on grid on interior.
!-----------------------------------------------------------------------!
  errp = 0. ; maxerr = 0.

  do iz = lb(3),ub(3)
  do iy = lb(2),ub(2)
  do ix = lb(1),ub(1)
      errp = max(errp,& 
                 abs(bxm(ix,iy,iz)-bxa(ix,iy,iz)),&
                 abs(bym(ix,iy,iz)-bya(ix,iy,iz)),&
                 abs(bzm(ix,iy,iz)-bza(ix,iy,iz))) 
     maxerr = max(maxerr,&                                                    
                 abs(bxm(ix,iy,iz)),&
                 abs(bym(ix,iy,iz)),&
                 abs(bzm(ix,iy,iz))) 
  enddo
  enddo
  enddo
!-----------------------------------------------------------------------!
! Convergence check to not necessarily do max allowed # iterations.
!-----------------------------------------------------------------------!
  errp   = max_scalar(errp)
  maxerr = max_scalar(maxerr)
  errp = errp/maxerr
  if (master .and. verbose>1) then 
    write(stdout,*) 'errp,maxerr,iter_b =',rank,errp,maxerr,iter_b
  endif
  iter_b = iter_b + 1

  if (errp < tol_b) then                                             ! done?
    EXIT SOLVE_LOOP                                                  ! exit and continue
  end if

!-----------------------------------------------------------------------!
! Memorize last move iteration - same as above.
!-----------------------------------------------------------------------!
  do iz=1,g%n(3)
     bxa(:,:,iz) = bxm(:,:,iz)
     bya(:,:,iz) = bym(:,:,iz)
     bza(:,:,iz) = bzm(:,:,iz)
  end do
  
!-----------------------------------------------------------------------!
! end of loop for solving for LaPlace(B).
!-----------------------------------------------------------------------!
ENDDO SOLVE_LOOP                                                       

  if (verbose>0) &
    write (stdout,*) 'Exiting; b_push CONVERGED - step ok.'

!-----------------------------------------------------------------------!
! Warn us if we're not below error tolerance after max iterations.
!-----------------------------------------------------------------------!
  if(errp.ge.tol_b) then                                               
    call warning('b_push','Max iterations reached, no convergence')
  endif

!-----------------------------------------------------------------------!
! Save the B-field before updating B to B(i+1);
! needed in 'e_push' for time-averaging B(i+1)-B(i).
!-----------------------------------------------------------------------!
  do iz=1,g%n(3)
    bxa(:,:,iz) = bx (:,:,iz); bx(:,:,iz) = bxm(:,:,iz)
    bya(:,:,iz) = by (:,:,iz); by(:,:,iz) = bym(:,:,iz)
    bza(:,:,iz) = bz (:,:,iz); bz(:,:,iz) = bzm(:,:,iz)
  end do
                                                      call dump(bx,'bx')
                                                      call dump(by,'by')
                                                      call dump(bz,'bz')

END SUBROUTINE b_push_implicit

!=======================================================================!
SUBROUTINE e_push_implicit
  USE params,          ONLY: mpi, do_e_fix, it
  USE grid_m,          ONLY: ex, ey, ez, nx, ny, nz                     ! fields
  USE grid_m,          ONLY: bx, by, bz                                 ! more fields
  USE maxwell_sources, ONLY: jx, jy, jz                                 ! current density
  USE maxwell_sources, ONLY: calc_charge, charge
  USE maxwell_sources, ONLY: bxa, bya, bza
  USE maxwell_sources, ONLY: e1=>sx, e2=>sy
  USE dumps,           ONLY: dump
  USE stagger
  implicit none
  integer ix, iy, iz
!.......................................................................!
  call trace_enter('E_PUSH')

!-----------------------------------------------------------------------!
!Now push the whole interior grid.
!-----------------------------------------------------------------------!
  e1 = curlxdn (bz , by )
  e2 = curlxdn (bza, bya) 
  do iz=lb(3),ub(3)
             ex(lb(1):ub(1),lb(2):ub(2),iz) = &
             ex(lb(1):ub(1),lb(2):ub(2),iz) + &
    ce1*(emf*e1(lb(1):ub(1),lb(2):ub(2),iz) + &
       omemf*e2(lb(1):ub(1),lb(2):ub(2),iz) - &
         ce2*jx(lb(1):ub(1),lb(2):ub(2),iz))
  enddo
!-----------------------------------------------------------------------!
  e1 = curlydn (bx , bz )
  e2 = curlydn (bxa, bza) 
  do iz=lb(3),ub(3)
             ey(lb(1):ub(1),lb(2):ub(2),iz) = &
             ey(lb(1):ub(1),lb(2):ub(2),iz) + &
    ce1*(emf*e1(lb(1):ub(1),lb(2):ub(2),iz) + &
       omemf*e2(lb(1):ub(1),lb(2):ub(2),iz) - &
         ce2*jy(lb(1):ub(1),lb(2):ub(2),iz))
  enddo
!-----------------------------------------------------------------------!
  e1 = curlzdn (by , bx )
  e2 = curlzdn (bya, bxa) 
  do iz=lb(3),ub(3)
             ez(lb(1):ub(1),lb(2):ub(2),iz) = &
             ez(lb(1):ub(1),lb(2):ub(2),iz) + &
    ce1*(emf*e1(lb(1):ub(1),lb(2):ub(2),iz) + &
       omemf*e2(lb(1):ub(1),lb(2):ub(2),iz) - &
         ce2*jz(lb(1):ub(1),lb(2):ub(2),iz))
  enddo
!-----------------------------------------------------------------------!

!-----------------------------------------------------------------------!
!Set boundary components of e-field (compare with 'e_fix')
  call e_boundaries(ex,ey,ez)
                                                      call dump(ex,'ex')
                                                      call dump(ey,'ey')
                                                      call dump(ez,'ez')
!-----------------------------------------------------------------------!
! find new charge density and align charges and electric field
  if (do_e_fix .and. mod(it,every_e)==0) then
    call calc_charge
    call e_fix (ex, ey, ez, charge)
  else
    iter_e = 0
  endif
  call trace_exit('E_PUSH')

END SUBROUTINE e_push_implicit

!=======================================================================!
SUBROUTINE e_fix (ex, ey, ez, charge)
  USE params,          ONLY: stdout, rank, time, stdall, master, mpi
  USE maxwell_sources, ONLY: phi=>bxa, phif=>bya, dei=> bza, verbose
  USE maxwell_sources, ONLY: dex=>bxm, dey=>bym, dez=>bzm
  USE dumps,           ONLY: dump
  USE stagger
#ifdef _OPENMP
  USE omp_lib
#endif

  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: ex, ey, ez, charge
  logical :: convergence,inside                                         ! e_fix convergence flag
  integer ::  ix, iy, iz
  real    :: dexm, deym, dezm, dexp, deyp, dezp
  real    :: dphi, phitemp, cmax, emax, dphip, cmaxp, emaxp
  real    :: max_scalar, max_dphi
!.......................................................................!
  call trace_enter('E_FIX')

  convergence=.false.; dex=0.; dey=0.; dez=0.                           ! zap loop variables

  dphi = 0. ; emax = 0. ; cmax = 0.
MAIN_LOOP : do while(iter_e .le. maxiter_e)                             ! loop ONLY maxiter_e times.

!-----------------------------------------------------------------------!
! Find 'source' term; PHI = div(E) - rho/epsilon0 on INTERIOR.
  dphip = 0. ; emaxp = 0. ; cmaxp = 0.

  phi = ddxdn(ex) + ddydn(ey) + ddzdn(ez) - ce3*charge

  do iz=lb(3),ub(3); do iy=lb(2),ub(2); do ix=lb(1),ub(1)
    emaxp   = max(emaxp,abs(ex (ix,iy,iz)), &
                        abs(ey (ix,iy,iz)), &
                        abs(ez (ix,iy,iz)))
    dphip   = max(dphip,abs(phi(ix,iy,iz)))
    cmaxp   = max(cmaxp,abs(ce3*charge(ix,iy,iz)))
    phi(ix,iy,iz) = phi(ix,iy,iz)*dd1e
  enddo; enddo; enddo

  emax = max(emaxp,emax)
  cmax = max(cmaxp,cmax)
  dphi = max(dphip,dphi)

  cmax = max(cmax,emax/minval(g%ds))
  cmax = max_scalar(cmax)
  if (cmax > 0.) dphi = dphi/cmax
  max_dphi = max_scalar(dphi)

  if (maxiter_e==miniter_e) then
    write (stdout,*) ' dphi, tol_e, dd1e : ', dphi, tol_e, dd1e, dphi*dd1e
    convergence=.true.
    exit MAIN_LOOP
  endif

  call phi_boundaries(phi)

!-----------------------------------------------------------------------!
! FILTER POISSON RESIDUAL (using bya as buffer - phif => bya above). 
!-----------------------------------------------------------------------!
if (filter > 0.) then
  phi = poisson_filter (phi, filter, f_2nd)
  call phi_boundaries(phi)
              call dump(phi,'phi')
end if

!-----------------------------------------------------------------------!
! APPLY CORRECTIONS
!-----------------------------------------------------------------------!
  dexm=0.; deym=0.; dezm=0.

  dex = ddxdn(phi)
  dey = ddydn(phi)
  dez = ddydn(phi)

  do iz=lb(3),ub(3)
  do iy=lb(2),ub(2)
  do ix=lb(1),ub(1)
    dexm = max(dexm,abs(dex(ix,iy,iz)))
    deym = max(deym,abs(dey(ix,iy,iz)))
    dezm = max(dezm,abs(dez(ix,iy,iz)))
  enddo
  enddo
  enddo

!------------------------------------------------------------------------!
! SET BOUNDARY COMPONENTS OF E-FIELD
!------------------------------------------------------------------------!
  call e_boundaries(ex,ey,ez)
                                                       call dump(ex,'ex')
                                                       call dump(ey,'ey')
                                                       call dump(ez,'ez')
!------------------------------------------------------------------------!
! Output some stats
!------------------------------------------------------------------------!
  if (master .and. verbose > 1) write(stdout,*) 'max_dphi,iter_e =', &
                                                 max_dphi,iter_e
  if (master .and. verbose > 2) write(stdall,*) 'rank=',    rank,    &
                                                'tol_e=',   tol_e,   &
                                                'max_dphi=',max_dphi,&
                                                'dphi=',    dphi,    &
                                                'de(:)=',   dex,dey,dez, &
                                                'iter=',    iter_e

!-----------------------------------------------------------------------!
! Error check part #1
!-----------------------------------------------------------------------!
  if((max_dphi >= tol_e).and.(iter_e >= maxiter_e).and.(stdout >= 0)) then  
    ! Something is rotten - no/too slow convergence; bail out.
    write (stdout,*) 'Exiting e_fix w/NO CONVERGENCE maxiter reached.'
    write (stdout,*) ' dphi, de, max(de(:))   : ', &
                       dphi, dex, dey, dez, maxval((/dex,dey,dez/))
    write (stdout,*) ' max_dphi, tol_e    : ', max_dphi, tol_e
    write (stdout,*) ' abs(dphi)          : ', abs(dphi)
    write (stdout,*) ' '
    convergence=.false.
    exit MAIN_LOOP
  else if((max_dphi.le.tol_e).and.(iter_e.ge.miniter_e)) then               
    ! Everything is OK - convergence in due time; set flag and exit.
    convergence=.true.
    exit MAIN_LOOP
  endif

  dphi = 0. ; emax = 0. ; cmax = 0.
  iter_e = iter_e + 1 
!-----------------------------------------------------------------------!
ENDDO MAIN_LOOP

!-----------------------------------------------------------------------!
! Error check part #2, print stats for 'good solve' conditions.
!-----------------------------------------------------------------------!
   if (convergence .and. verbose>0 .and. stdout>= 0) then               
    write (stdout,*) 'Exiting; e_fix CONVERGED - step ok.'
    write (stdout,*) ' iterations         : ', iter_e
    write (stdout,*) ' dphi, max(de(:))   : ', dphi, maxval((/dex,dey,dez/))
    write (stdout,*) ' max_dphi, tol_e    : ', max_dphi, tol_e
    write (stdout,*) ' abs(dphi)          : ', abs(dphi)
    write (stdout,*) ' '
   endif

  if (verbose>1) write(stdall,*) 'exit',rank,max_dphi,iter_e
  call trace_exit('E_FIX')
!-----------------------------------------------------------------------!
END SUBROUTINE e_fix

!=======================================================================!
SUBROUTINE b_fix (bx, by, bz)
  USE params,          ONLY: stdout, rank, time, stdall, mpi, master
  USE maxwell_sources, ONLY: phi=>sx, phif=>sy 
  USE maxwell_sources, ONLY: dbx=>bxm, dby=>bym, dbz=>bzm
  USE maxwell_sources, ONLY: verbose
  USE dumps,           ONLY: dump
  USE grid_m,          ONLY: g
  USE stagger
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: bx, by, bz
  logical :: convergence=.false.
  integer :: ix, iy, iz
  real    :: dex,dey,dez
  real    :: dphi, phitemp, bmax, bmaxp, dphip
  real    :: max_scalar, max_dphi
!......................................................................!
  call trace_enter('B_FIX')

!----------------------------------------------------------------------!
! zap various loop variables
  convergence=.false.; dex=0.; dey=0.; dez=0.                    
!----------------------------------------------------------------------!

  dphi = 0. ; bmax = 0.


MAIN_LOOP : do while(iter_b < maxiter_b) 

!----------------------------------------------------------------------!
!Find 'source' term; PHI = div(B) 
  dphip = 0. ; bmaxp = 0.
  
  phi = ddxup(bx) + ddyup(by) + ddzup(bz)

  do iz=lb(3),ub(3)
  do iy=lb(2),ub(2)
  do ix=lb(1),ub(1)
    bmaxp = max(bmaxp,abs(bx(ix,iy,iz)), &
                      abs(by(ix,iy,iz)), &
                      abs(bz(ix,iy,iz)))
    dphip = max(dphip,abs(phi(ix,iy,iz)))
  enddo
  enddo
  enddo

  bmax = max(bmax,bmaxp)
  dphi = max(dphi,dphip)

  call phib_boundaries(phi)

  bmax = bmax/minval(g%ds)
  bmax = max_scalar(bmax)

  if (bmax > 0) dphi = dphi/bmax
  max_dphi = max_scalar(dphi)

!----------------------------------------------------------------------!
! FILTER POISSON RESIDUAL
! FILTER IS NORMALIZED SO 3-D CHECKERBOARD IS THE SAME AFTER FILTERING
!----------------------------------------------------------------------!
if (filter > 0.) then
  phi = poisson_filter (phi, filter, f_2nd)

!----------------------------------------------------------------------!
! SET BOUNDARY COMPONENTS OF PHI AGAIN
!----------------------------------------------------------------------!
  call phib_boundaries(phi)
              call dump(phi,'phi')

end if

!----------------------------------------------------------------------!
! NOW CORRECT B-FIELD 
!----------------------------------------------------------------------!

  dex=0.; dey=0.; dez=0.

  dbx = ddxdn(phi)
  dby = ddydn(phi)
  dbz = ddydn(phi)

  do iz=lb(3),ub(3)
  do iy=lb(2),ub(2)
  do ix=lb(1),ub(1)
    dex = max(dex,abs(dbx(ix,iy,iz)))
    dey = max(dey,abs(dby(ix,iy,iz)))
    dez = max(dez,abs(dbz(ix,iy,iz)))
  enddo
  enddo
  enddo

!-----------------------------------------------------------------------!
! SET BOUNDARY COMPONENTS OF B-FIELD
!-----------------------------------------------------------------------!
  call b_boundaries(bx,by,bz)

  if (master .and. verbose > 1) write(stdout,*) 'max_dphi,iter_divb =',max_dphi,iter_divb
  if (master .and. verbose > 2) write(stdall,*) 'rank=',    rank,    &
                                'tol_divb=',tol_divb,&
                                'max_dphi=',max_dphi,&
                                'dphi=',    dphi,    &
                                'de(:)=',   dex,dey,dez,  &
                                'iter=',    iter_b

!-----------------------------------------------------------------------!
! Error check part #1
!-----------------------------------------------------------------------!
  if ((max_dphi >= tol_divb)   .and. & 
      (iter_b >= maxiter_divb) .and. &
      (stdout >= 0)) then  
    ! Something is rotten - no/too slow convergence; bail out.
    write (stdout,*) 'Exiting b_fix w/NO CONVERGENCE maxiter reached.'
    write (stdout,*) ' dphi, max(de(:))   : ', dphi, maxval((/dex,dey,dez/))
    write (stdout,*) ' max_dphi, tol_divb : ', max_dphi, tol_divb
    write (stdout,*) ' '
    convergence=.false.
    exit MAIN_LOOP
  else if((max_dphi.le.tol_divb).and.(iter_b.ge.miniter_divb)) then         
  ! Everything is OK - convergence in due time; set flag and exit.
    convergence=.true.
    exit MAIN_LOOP
  endif
  dphi = 0. ; bmax = 0.
  iter_b = iter_b + 1

!-----------------------------------------------------------------------!
! Next iteration
!-----------------------------------------------------------------------!
enddo MAIN_LOOP

!-----------------------------------------------------------------------!
! Error check part #2 
!-----------------------------------------------------------------------!
  if (convergence .and. verbose>0 .and. stdout>= 0) then                ! Print stats for 'good solve' conditions.
    write (stdout,*) 'Exiting; b_fix CONVERGED - step ok.'
    write (stdout,*) ' iterations         : ', iter_b
    write (stdout,*) ' dphi, max(de(:))   : ', dphi, maxval((/dex,dey,dez/))
    write (stdout,*) ' max_dphi, tol_divb : ', max_dphi, tol_divb
    write (stdout,*) ' abs(dphi)          : ', abs(dphi)
    write (stdout,*) ' '
  endif

  if (verbose>1) write(stdall,*) 'exit',rank,max_dphi,iter_b
  call trace_exit('B_FIX')
!-----------------------------------------------------------------------!
END SUBROUTINE b_fix

!-----------------------------------------------------------------------!
END MODULE maxwell_solver
!=======================================================================!

!=======================================================================!
SUBROUTINE read_maxwell_solver
  USE params,          ONLY: master, trace, stdin, stdout, params_unit, &
                              out_namelists, do_e_fix, do_b_fix, do_b_push
  USE grid_m,          ONLY: g
  USE units,           ONLY: c
  USE maxwell_sources, ONLY: verbose
  USE energy_filter_m, ONLY: e_filter
  USE maxwell_solver
  implicit none
  namelist /maxwell/ sor, filter, f_2nd, f_6th, emf, maxiter_b, tol_b, &
    miniter_e, maxiter_e, tol_e, every_e, miniter_divb, maxiter_divb, &
    tol_divb, verbose, do_b_fix, do_e_fix, do_explicit, e_filter
!.......................................................................
  rewind (stdin);     read  (stdin,maxwell)
  if (master)   write (params_unit,maxwell)
  if (sor == 0.) sor = 2./(1.+c%pi/maxval(g%n))
  if (out_namelists) write (stdout,maxwell)
END SUBROUTINE read_maxwell_solver

!=======================================================================!
SUBROUTINE init_maxwell
  USE params, ONLY: master, trace, stdin, stdout, mid, do_b_fix, &
                    do_e_fix, do_b_push, mpi
  USE grid_m, ONLY: nx,ny,nz,ex,ey,ez,bx,by,bz
  USE dumps,  ONLY: dump, dump_set, dump_prefix
  USE maxwell_sources
  USE maxwell_solver
  implicit none  
  character(len=mid) :: id = &
    'Fields/maxwell_solver.f90 $Id$'
  real           :: dtsave
  real, external :: max4
  real, dimension(nx,ny,nz) :: scr
  integer :: ix,iy,iz
  logical :: problems
  integer :: maxprint
!.......................................................................!
  call print_id(id)
  call trace_enter('INIT_MAXWELL')
  call dump_prefix('simple')
  call dump_set('init_maxwell')

  !---------------------------------------------------------------------!
  ! As per the IDL test, use no over-relaxation when using filter
  emf          = 0.55
  filter       = 0.105
  f_2nd        = 0.6
  f_6th        = 0.    ! FIXME, f_6th is not implemented in e_fix yet
  sor          = 0.65
  miniter_divb = 1
  miniter_e    = 1
  every_b      = 100
  every_e      = 1
  maxiter_b    = 400
  maxiter_divb = 400
  maxiter_e    = 400
  tol_b        = 1.e-5
  tol_e        = 1.e-4                                                   
  ! FIXME: are tol_e, tol_b affected by non-trivial unit scaling?
  tol_divb     = 1.e-5
  verbose      = 0
  do_b_fix     = .true.
  do_e_fix     = .true.
  do_explicit  = .false.

  call read_maxwell_solver

  !---------------------------------------------------------------------!
  ! use over-relaxation if not using filtering
  if (filter==0.) then
    sor = 2./(1.+c%pi/maxval(g%n)) 
  endif

  !---------------------------------------------------------------------!
  ! Allocate arrays, and call sort to count particles per cell
  call allocate_sources
  call sort

  !---------------------------------------------------------------------
  ! Make sure initial state has proper boundary zones
  call e_boundaries (ex, ey, ez)
  call b_boundaries (bx, by, bz)

  !---------------------------------------------------------------------!
  ! Call Maxwell with dt=0, to get bmax, %d, %v + possibly clean E and B
  dtsave = dt
  dt = 0.0
  call solve_maxwell
  dt = dtsave
  
  call trace_exit('INIT_MAXWELL_SOLVER')
END SUBROUTINE init_maxwell

!=======================================================================!
SUBROUTINE init_maxwell_solver_restart
  USE params,          ONLY: master, trace, stdin, stdout 
  USE dumps,           ONLY: dump, dump_set
  USE grid_m,          ONLY: ex, ey, ez, bx, by, bz
  USE maxwell_sources, ONLY: bxa, bya, bza, bmax
  implicit none  

  real,            external :: max4
  real                      :: dtsave

  call trace_enter('INIT_MAXWELL_SOLVER_RESTART')

!-----------------------------------------------------------------------!
  call dump_set('init_maxwell_restart')
              call dump(bx,'Bx')
              call dump(by,'By')
              call dump(bz,'Bz')

! Make sure overlaps are ok
  call overlap(bx)                                                      
  call overlap(by)
  call overlap(bz)
  call overlap(ex)
  call overlap(ey)
  call overlap(ez)
  
! Get max B-field for set_dt
  bxa = bx**2+by**2+bz**2
  bmax = sqrt(max4(bxa))                                                

! compute prefactors here -- they might exhibit time variation
  call compute_prefactors()                                             
  
  call trace_exit('INIT_MAXWELL_SOLVER_RESTART')
END SUBROUTINE init_maxwell_solver_restart

!=======================================================================!
SUBROUTINE solve_maxwell
  USE energy_filter_m
  USE params,          only: do_maxwell, dt
  USE grid_m,          only: g, bx, by, bz, ex, ey, ez
  USE dumps,           only: dump, dump_set
  USE units,           only: c
  USE maxwell_solver,  only: e_push, b_push
  USE maxwell_sources, only: calc_current
  USE energy_filter_m
  implicit none
  real a
!.......................................................................
  if (.not. do_maxwell) return
  call trace_enter('SOLVE_MAXWELL')
  if (dt > 0.0) call dump_set('maxwell')

  call compute_prefactors
  call mesh_sources
  call calc_current
  call b_push
  call e_push
 !call damping                                                          ! apply damping 
 
  if (e_filter > 0.0) then
    a = e_filter*c%c*dt/maxval(g%ds)
    call energy_filter (bx, by, bz, ex, ey, ez, a)
  end if
  call trace_exit('SOLVE_MAXWELL')
END SUBROUTINE solve_maxwell

!=======================================================================!
! Compute prefactors, might depend on order in finite diff.
!=======================================================================!
SUBROUTINE compute_prefactors
  USE grid_m,         ONLY: g, odxq, odyq, odzq
  USE params,         ONLY: dt, it, mdim
  USE units,          ONLY: c, elm
  USE stagger,        ONLY: lp0
  USE maxwell_solver, ONLY: ce1, ce2, ce3,      &
                            cb1, cb2, cb3, cb4, c4bx, c4by, c4bz, &
                            dd1e, dd1b, sor, emf, omemf, &
                            f_2nd, f_6th, denom
  implicit none
  integer :: test
!.......................................................................
  omemf = 1.-emf
  ce1 = (elm%ke/elm%kb)*dt                                              ! for curl(B) & J
  ce2 = c%fourpi*elm%kb                                                 ! for J only
  ce3 = c%fourpi*elm%ke                                                 ! for phi 

  cb1 = (1./elm%kf)*dt                                                  ! for curl(E)
  cb2 = c%fourpi*emf*(elm%ke/elm%kf)*dt**2                              ! for curl(J)
  cb3 = emf*omemf*(elm%ke/(elm%kf*elm%kb))*dt**2                        ! for laplace(B)
  cb4 = emf**2*dt**2*c%c2                                               ! laplace(B^{n+1})
  c4bx  = emf**2*dt**2*odxq*c%c2
  c4by  = emf**2*dt**2*odyq*c%c2
  c4bz  = emf**2*dt**2*odzq*c%c2
  denom = 1./(1.-(lp0*(c4bx + c4by + c4bz)))                            ! for laplace

  dd1e  = (1./(6. + 4.*abs(f_2nd)))*(1./(odxq+odyq+odzq))*sor           ! for e_fix 
  dd1b  = (1./4.)*(1./(3.*max(odxq,odyq,odzq)))*sor                     ! for b_fix 
END SUBROUTINE compute_prefactors

!=======================================================================!
! The boundary zones needed by this field method
!=======================================================================!
SUBROUTINE field_solver_boundaries    
  USE grid_m,      ONLY: g
  implicit none
  g%lb = max(g%lb, 5)                                                   ! lower field bndry
  g%ub = max(g%ub, 4)                                                   ! upper field bndry
END SUBROUTINE

!=======================================================================
! Return iterations used (to avoid a module dependency)
!=======================================================================
SUBROUTINE maxwell_iterations (n_b, n_e)
  USE maxwell_solver, only: iter_b, iter_e
  implicit none
  integer n_b, n_e
  n_b = iter_b
  n_e = iter_e
END SUBROUTINE
