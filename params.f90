! vim: nowrap
MODULE params
  implicit none
!.......................................................................
! Basic MPI parameters
  type :: comm_type                                                     !> type for defining a communicator
    integer          :: comm                                            !> communicator id
    logical          :: master                                          !> master thread in communicator ?
    integer          :: rank                                            !> rank in communicator
    integer          :: nodes                                           !> number of threads in communicator
  end type
  type :: mpi_type
    integer          :: n(3)                                            !> mpi space dimensions
    integer          :: me(3)                                           !> 3D thread index
    integer          :: dn(3)                                           !> _rank_ of next node down
    integer          :: up(3)                                           !> _rank_ of next node up
    integer          :: offset(3)                                       !> integer offset (0-based)
    logical          :: lb(3), ub(3)                                    !> does the node contain a _physical_ boundary ?
    logical          :: interior(3)                                     !> Equivalent to .not. (mpi%lb .or. mpi%ub)
    integer          :: pnodes                                          !> smallest 2^n number such that nodes <= pnodes=2^n 
    type(comm_type)  :: x,  y,  z                                       !> comms for talking in x-, y-, and z-direction 
    type(comm_type)  :: xy, yz, zx                                      !> comms for talking in the xy-, yz-, and zx-plane 
    type(comm_type)  :: pset_s, pset_d                                  !> comms for Blue-Gene runs specifying IO nodes
    type(comm_type), allocatable :: sp(:)                               !> comms that are private for each species
  end type
  type (mpi_type)    :: mpi
  integer            :: rank                                            !> 1-D Rank of cpu
  integer            :: nodes                                           !> Nr of nodes == product(mpi%n)
  integer            :: real_size                                       !> size of reals
  logical            :: periodic(3)                                     !> Periodic mesh direction ?
  integer            :: comm                                            !> Communicator id
  integer            :: mpi_err                                         !> Error code
  logical            :: master                                          !> true on master thread
  integer            :: gpu_dev                                         !> GPU device to use for this rank
  integer            :: romp, nomp                                      !> omp rank (1-based) and nr of omp threads
#if defined (_OPENMP)
#endif
!.......................................................................
  integer, parameter :: mdim=3                                          !> number of dimensions
  integer, parameter :: mcoord=3                                        !> number of coordinates
  integer, parameter :: mid=120                                         !> size of Id strings
  integer, parameter :: mfile=120                                       !> size of file names
  integer, parameter :: minf=120                                        !> size of info bar
  integer, parameter :: mtxt=16                                         !> size of text strings
  integer            :: run_id                                          !> Unique id of the run
  logical            :: production                                      !> production mode?
!.......................................................................
  integer            :: nt                                              !> number of time steps
  integer            :: it=0                                            !> current time step
  integer            :: nspecies                                        !> number of species
  integer            :: ninteractions                                   !> number of interactions
  integer            :: nspectra                                        !> nr of synthetic spectra
  integer            :: nsubcycle                                       !> subcycle @move_particles
  integer            :: mprint=1                                        !> printout modulo this
  integer(kind=8)    :: writefac_global                                 !> global writefac setting
  integer(kind=8), allocatable, dimension(:) :: writefac                !> Frac of parts to write
  real(kind=8)       :: time=0.0,dt,dtp,tend,time_limit,time_bet_restart!> time info
  real               :: Cdt,Cdtwce,Cdtwpe                               !> Courant numbers
  real(kind=8)       :: walltime, walltime0, walltime_step              !> wallclock time used in a run
  real               :: dt_factor                                       !> Ignore smaller changes of dt
  real               :: grav_acc(mcoord)                                !> gravitational acceleration in the box
  real               :: q_shear, shear, omega_r                         ! Shearing sheet variables
!.......................................................................
  logical            :: do_2d2v                                         !> make velocity distributions 2V, nothing in y

  logical            :: do_lorentz,do_compton,do_benchmark,        &    !> APPEND any new switches
                        do_e_fix, do_b_push, do_candy_rozmus,      &    !> at the end of the list,
                        do_renormalize,do_cool,do_maxwell,do_dead, &    !> and copy the new section
                        do_Weightchange,do_dump,do_compare,        &    !> to the read_params, and
                        do_timer,die_in_sort, do_vay, do_b_fix,    &    !> write_params routines!
                        do_simple_renormalize,do_courant,          & 
                        do_synthetic_spectra,do_coulomb,do_vth,    &
                        do_check_ranges,do_any_trace,do_parallel_io, &
                        do_check_bounds,do_compression, do_local_patch, &
                        do_validate,do_overlap_add_at_io,do_picmhd,do_shear
  logical            :: out_e_bal,out_e_tot,out_e_em,out_e_pa,     &
                        out_namelists,out_units,out_grid,          &
                        out_particle,out_bound,out_ranks,          &
                        out_slice,out_id
  logical            :: restarting                                      !> Are we restarting?
  logical            :: re_read_init_namelists=.false.                  !> Can we safely reread the experiments namelists ?
  logical            :: using_old_particle_format=.true.                !> Are we using the old particle format in the init file ?
!.......................................................................
  logical            :: priomaxmem                                      !> prioritize maxing out on memory comsumption
  real               :: GBpercore                                       !> GBs (1GB == 1024^2 bytes) per core
!.......................................................................
  integer(kind=8)    :: particleupdates=0_8                             !> total updates for timing
  real               :: imbalance                                       !> trigger for sending slices between nodes
  real, dimension(2) :: load_balance_range                              !> lrange for load balance
  real, dimension(2) :: buffer_change                                   !> percent change on LB
  integer            :: dbg                                             !> debug level
  integer            :: trace_level=0                                   !> trace level (cf trace_mpi)
  logical            :: trace                                           !> trace calls flag
  integer            :: seed                                            !> main seed
  integer            :: Tsize                                           !> Size of lookup tables
  character(len=mid) :: hl='-----------------------------------------------------------------------'
  character(len=minf):: main_info,main_info_bar,main_info_line
  character(len=mfile):: datadir
  logical            :: do_check_interpolate
  integer            :: stdin=10                                        !> input file
  integer            :: stdout                                          !> output file
  integer            :: stderr                                          !> terminal / log
  integer            :: stdall                                          !> I/O needed for MPI
!.......................................................................
! Reserve your favorite unit number for a specific IO task here; then we are sure
! to get no unit-number-collisions in the future.
  integer, save      :: dbg_unit=50                                     !> track problem locations by changing unit number
  integer, parameter :: data_unit=30, mpi_unit=31, dump_unit=32,      &
                        random_unit=33, params_unit=34, touch_unit=35,&
                        copy_unit1=36, copy_unit2=37, stat_unit=38,   &
                        trace_unit=39, flag_unit=40, mpi_stats_unit=41  !> I/O unit numbers
!.......................................................................
  namelist /run_info/ out_e_tot, out_e_bal, out_e_em, out_e_pa, &
    out_namelists, out_units, out_grid, out_particle, out_bound, &
    out_ranks, out_slice, out_id, do_validate
!=======================================================================
END MODULE params


!=======================================================================
SUBROUTINE init_run_info                                                !> called from init_mpi
  USE params
  implicit none
!........................................................................
  out_e_tot     = .false.
  out_e_bal     = .false.
  out_e_em      = .false.
  out_e_pa      = .false.
  out_namelists = .false.
  out_units     = .false.
  out_grid      = .false.
  out_particle  = .false.
  out_bound     = .false.
  out_id        = .false.
  out_ranks     = .false.                                               ! info from all ranks ?
  out_slice     = .false.                                               ! slice namelists ?

  call read_run_info
END SUBROUTINE init_run_info

!=======================================================================
SUBROUTINE read_run_info
  USE params
  implicit none
!........................................................................
  rewind (stdin); read (stdin,run_info)
  if (out_ranks) stdout = stdall

  if (stdout < 0) then                                                   ! No output if stdout<0
    out_e_tot     = .false.
    out_e_bal     = .false.
    out_e_em      = .false.
    out_e_pa      = .false.
    out_namelists = .false.
    out_units     = .false.
    out_grid      = .false.
    out_particle  = .false.
    out_bound     = .false.
    out_ranks     = .false.                                             ! info from all ranks ?
    out_slice     = .false.                                             ! slice namelists ?
    do_validate   = .false.
  endif
  if (out_namelists) write (stdout,run_info)
END SUBROUTINE read_run_info

!=======================================================================
SUBROUTINE read_params
  USE params
  implicit none
  namelist /main/ nspecies, ninteractions, nspectra, tend, dt, &
    time_limit, grav_acc, time_bet_restart, Cdt, Cdtwce, Cdtwpe, seed, &
    Tsize, GBpercore, imbalance, load_balance_range, buffer_change, &
    dt_factor, priomaxmem
!.......................................................................
  rewind (stdin); read (stdin,main)                                     ! read main params
  if (master)        write (params_unit,main)
  if (out_namelists) write (stdout,main)
END SUBROUTINE read_params

!=======================================================================
SUBROUTINE init_params
  USE params
  implicit none
  integer :: gpu_err1, gpu_err2
  character(len=mid) :: &
    id = "params.f90 $Id$"

  if (master) write (stdout,'(a,i7)') ' Number of OMP threads:', nomp

  call print_id (id)

  run_id = -1

  time = 0.  ; tend = 1. ; dt = 0. ; it = 0
  Cdt  = 0.5 ; Cdtwce=2. ; Cdtwpe=.5                                    ! Courant number defaults
  seed = 21641                                                          ! default seed from mtran
  dt_factor = 1.2                                                       ! Ignore smaller dt changes
  ninteractions = 0
  nspecies = 2
  nspectra = 1
  nsubcycle = 100
  periodic = .true.
  ! empirically quick sort is faster, and does not require any storage!
  Tsize    = 10000
  time_limit = 2678400                                                  ! def to one month
  time_bet_restart = 86400                                              ! min one snapshot per day
  imbalance = 1.3                                                       ! max imbalance in neighbour particle numbers before shipping cells
  load_balance_range = (/ 0.05, 0.95 /)                                 ! lower/upper range for LB
  buffer_change      = (/  0.5, 1.2/)                                   ! new mp on dec/increase
  priomaxmem   = .false.                                                ! default no maxout on RAM usage
  GBpercore    = 0.5                                                    ! default: 0.5 GB/core
  grav_acc     = 0.                                                     ! default: no gravity
  gpu_dev      = 0                                                      ! default: use device 0 for gpu

  call read_params
  dtp = dt

  trace = .false.
  dbg = 0
  do_dump = .false.
  do_compare = .false.
  do_check_interpolate = .true.
  do_check_ranges = .false.                                             ! print out ranges
  do_check_bounds = .false.                                             ! check that particles are within bounds
  die_in_sort = .true.                                                  ! Errors are normally due to bad initial cond or memory leaks

  call read_debug

  ! Default do values. Beware! These are updated again in species, interactions and
  ! maybe in the experiment setup, depending on the other namelists.
  ! For example if an interaction called compton is specified, then do_compton
  ! is set to true. As the very last thing the user get the choice of over
  ! ruling and setting them on or off in init_do
  do_lorentz        = .true.                                            ! calculate lorentz force by default
  do_maxwell        = .true.                                            ! solve Maxwell by default
  do_cool           = .false.                                           ! no radiative cooling
  do_compton        = .false.                                           ! no interactions
  do_coulomb        = .false.                                           ! no interactions
  do_renormalize    = .false.                                           ! no renormalization / particle merging
  do_timer          = .true.                                            ! show it's there
  do_courant        = .true.
  do_vth            = .false.                                           ! do not calculate thermal velocities
  do_parallel_io    = nodes > 1                                         ! Dump files using parallel io
  do_vay            = .true.                                            ! Use Vay Pusher by default 
  do_candy_rozmus   = .false.                                           ! Use 2nd order timestepping by default
  do_benchmark      = .false.                                           ! Do not disable io by default
  do_compression    = .false.                                           ! Don't use phase space compression
  do_local_patch    = .true.                                            ! Option for the PATCH=lean case
  do_overlap_add_at_io = .true.                                         ! If true, we have to call overlap_add
                                                                        ! before writing out field densities
  do_picmhd         = .false.
  do_shear          = .false.                                           ! Shear active ?
END SUBROUTINE init_params

!=======================================================================
SUBROUTINE write_params
  USE params
  implicit none
  real(kind=4)       :: ltime, ldt, ltend, ltime_limit, ltime_bet_restart
  character(len=mid) :: id = "params.f90 $Id$"
  integer            :: ionodes=1,iorank=0
  integer, parameter :: io_format=10
!.......................................................................
  write (data_unit) io_format,mid
  write (data_unit) id
  write (data_unit) 7
  write (data_unit) 8,1
  write (data_unit) nodes,rank,mpi%dn(3),mpi%up(3),mpi%interior(3),periodic
  write (data_unit) 7,1
  write (data_unit) mdim,mcoord,mid,mfile,mtxt,run_id,production
  write (data_unit) 6,1
  write (data_unit) nt,it,nspecies,ninteractions,nspectra,nsubcycle
  write (data_unit) 9,2 ! ups, some time vars are dbl prec. Why???
  ltime=time; ldt=dt; ltend=tend; ltime_limit=time_limit; ltime_bet_restart=time_bet_restart
  write (data_unit) ltime,ldt,ltend,ltime_limit,ltime_bet_restart,real((/Cdt,Cdtwce,Cdtwpe,dt_factor/),kind=4)
!.......................................................................
  write (data_unit) 28,1
  write (data_unit) do_lorentz,do_compton,do_benchmark,        &    !> VERBATIM copy from
                    do_e_fix, do_b_push, do_candy_rozmus,      &    !> declarations section
                    do_renormalize,do_cool,do_maxwell,do_dead, &
                    do_Weightchange,do_dump,do_compare,        &
                    do_timer,die_in_sort, do_vay, do_b_fix,    &
                    do_simple_renormalize,do_courant,          & 
                    do_synthetic_spectra,do_coulomb,do_vth,    &
                    do_check_ranges,do_any_trace,do_parallel_io, &
                    do_check_bounds,do_compression, do_local_patch
  write (data_unit) 11,1
  write (data_unit) out_e_bal,out_e_tot,out_e_em,out_e_pa,     &
                    out_namelists,out_units,out_grid,          &
                    out_particle,out_bound,out_ranks,          &
                    out_slice
  write (data_unit) 1,1
  write (data_unit)     restarting                                      ! Are we restarting?
END SUBROUTINE write_params

!=======================================================================
SUBROUTINE read_switches
  USE params
  implicit none
  namelist /do/  do_lorentz, do_compton, do_coulomb, do_renormalize, &
                 do_cool, do_maxwell, do_dead, do_dump, do_compare,    &
                 do_timer, do_courant, do_vth, do_parallel_io, &
                 do_vay, do_candy_rozmus, do_benchmark, do_compression, &
                 do_validate, do_picmhd, do_shear
!.......................................................................
  if (out_namelists) write(stdout,*) hl
  rewind (stdin); read (stdin,do)                                       ! read what to do

  do_weightchange = do_compton             .or. &                       ! Need to have extra space
                    do_simple_renormalize  .or. &
!                   do_coulomb             .or. &
                    do_compression
!-----------------------------------------------------------------------
! in cell structure? (p.t. coulomb is 'simple' full particle scatter, 
! no splitting, this may (will) change!)
!-----------------------------------------------------------------------                    
  do_dead         = do_renormalize .or. do_dead                         ! do_renormalize implies do_dead
  do_vth          = do_coulomb .or. do_vth                              ! we need vth in columb_scatter

  if (out_namelists)  write (stdout     ,do)                            ! write what to do
  if (master       )  write (params_unit,do)                            ! save to params_unit

END SUBROUTINE read_switches

!=======================================================================
SUBROUTINE init_switches
  USE params
  implicit none
!.......................................................................
  do_dead = do_compton !.or.do_coulomb                                  ! Need to remove dead?
  do_renormalize = do_dead                                              ! renormalize ?

  call read_switches

  if (do_shear) call init_shear
END SUBROUTINE init_switches

!=======================================================================
SUBROUTINE read_shear
  USE params
  implicit none
  namelist /shearing/ q_shear, shear, omega_r 

!.......................................................................
  if (out_namelists) write(stdout,*) hl
  rewind (stdin); read (stdin,shearing)                                 ! read shear parameters

  if (q_shear .ne. 0.)  shear = - q_shear * omega_r                     ! calculate rate of shear from q
  if (q_shear==0. .and. omega_r .ne. 0.) q_shear = - shear / omega_r    ! calculate q from rate of shear

  if (out_namelists)  write (stdout     ,shearing)                      ! write what to do
  if (master       )  write (params_unit,shearing)                      ! save to params_unit

END SUBROUTINE read_shear

!=======================================================================
SUBROUTINE init_shear
  USE params
  implicit none
!.......................................................................
  q_shear = 1.5
  omega_r = 1.0
  shear   = 0.0

  call read_shear

END SUBROUTINE init_shear
!-----------------------------------------------------------------------
