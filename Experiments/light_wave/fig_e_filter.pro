PRO fig_e_filter
  restore, 'k=1_square=20.save'
  setdev,'ps',file='fig_e_filter.ps'
  plot ,haver(f.by),yr=[-1,1]*1.1,xr=[10,30]
  oplot,haver(fexpl_000.by),line=1
  oplot,haver(fexpl_002.by),line=3
  oplot,haver(fimpl_504.by),line=2
  label,x=0.35,y=0.55,line=0,'initial'
  label,line=1,'explicit'
  label,line=2,'implicit, emf=0.504'
  label,line=3,'explicit, e_filter=0.002'
  device,/close
END