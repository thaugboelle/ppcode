! Experiments/plasma_oscillations/init_experiment.f90 Experiments/light_wave/init_experiment.f90 $Id$
! vim: nowrap
!-----------------------------------------------------------------------!
!=======================================================================
! Leave boundaries as they are, so no need to even read a namelist
!=======================================================================
SUBROUTINE init_experiment_boundaries
END SUBROUTINE init_experiment_boundaries

!=======================================================================
SUBROUTINE input (inputfile)
  USE params, only: mfile
  USE dumps, only: dump, dump_set
  implicit none
  character(len=mfile) inputfile
!.......................................................................
  inputfile = 'input.nml'                                               ! Where to find input
END SUBROUTINE

!=======================================================================
SUBROUTINE init_experiment
  USE params
  USE grid_m,   ONLY: Bx, By, Bz, Ex, Ey, Ez, g
  USE units,    ONLY: c, elm
  USE species,  ONLY: sp, fields
  USE dumps,    ONLY: dump, dump_set
  implicit none
  real, parameter:: pi=acos(-1.0)
  real    :: amplitude = 1.
  real    :: phase     = 0.
  real    :: square    = 0.
  integer :: k         = 1                                              ! wave number
  integer :: iz
  real    :: zc                                                         ! centered
  real    :: zs                                                         ! staggered
  real    :: w                                                          ! ang. frequency
  real    :: t = 0.0                                                    ! initial time
  real    :: E_left, E_right, B_left, B_right, a, a_left, a_right       ! scratch
  namelist /init/ amplitude, phase, k, square                           ! wave params
!.......................................................................
  Bx=0; Bz=0; Ey=0; Ez=0                                                ! wave in z-dir

  rewind(stdin);      read (stdin,init)
  if (master)   write(params_unit,init)
  if (out_namelists) write(stdout,init)

  w = k/g%gs(3)*c%c                                                     ! frequency
  phase = phase*pi/180.                                                 ! radians
  a = amplitude
  a_left  = sin(pi/4.-phase)                                 
  a_right = cos(pi/4.-phase)
  print*,pi,a_left,a_right
  a = a/(a_left+a_right)
  a_left  = a*a_left
  a_right = a*a_right
  print*,a_left,a_right,g%ds(3),g%gs(3)

  do iz=g%lb(3),g%ub(3)-1                                               ! cell loop
    zc = (g%z(iz)-g%grlb(3))/g%gs(3)                                    ! 0 < z < 1
    zs = zc + 0.5*g%ds(3)/g%gs(3)                                       ! stagger
    B_left  = -a_left *cos(2.*pi*(w*t+k*zs))                            ! travelling left
    B_right = +a_right*cos(2.*pi*(w*t-k*zs))                            ! travelling right
    E_left  = +a_left *cos(2.*pi*(w*(t+0.5*dt)+k*zc))
    E_right = +a_right*cos(2.*pi*(w*(t+0.5*dt)-k*zc))
    Ex(:,:,iz) = E_left + E_right                                       ! Ex
    By(:,:,iz) = B_left + B_right                                       ! By
    if (square > 0.0) then                                              ! square wave 
      Ex(:,:,iz) = a*tanh(Ex(:,:,iz)*square)/tanh(a*square)
      By(:,:,iz) = a*tanh(By(:,:,iz)*square)/tanh(a*square)
    end if
  end do
END SUBROUTINE init_experiment
