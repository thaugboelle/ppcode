; arrays for the snapshot data
  nt = 40
  dt = 1.0
  times = dt*findgen(nt)
  values = fltarr(nt)

; arrays for the model expression
  nz = 50
  z = findgen(nz)/nz
  model = sin(2.*!pi*z)

; project the actual Ez-field onto the model
  for i=0,nt-1 do begin
    f = rf(i)
    ez = haver(f.ez)
    values[i] = total(model*ez)
  end

; make a fit, using the Markwardt MPFITEXPR procedure
  p = [12.,0.5]
  p = mpfitexpr('p[1]*sin(2.*!pi*x/p[0])',times,values,err,p,yfit=yfit)

; compute the fitting function
  nt1 = 400
  dt1 = 0.1
  times1 = dt1*findgen(nt1)
  yfit1 = p[1]*sin(2.*!pi*times1/p[0])

; plot the fit, and the data values
  plot, times1, yfit1, line=1, xtitle='time', ytitle='amplitude'
  oplot, times, values, psym=1

; print a summary
  print,' '
  print,'  Plasmaoscillations parameter fit:'
  print,'          period  =', p[0]
  print,'       amplitude  =', p[1]
END