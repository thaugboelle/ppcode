! $ Date: 2013-09-02 09:40:06 +0200 $
! vim: nowrap
!-----------------------------------------------------------------------!
! Initialize a single particle in a homogenous magnetic field 

!=======================================================================
! Leave boundaries as they are, so no need to even read a namelist
!=======================================================================
SUBROUTINE init_experiment_boundaries
END SUBROUTINE init_experiment_boundaries

!=======================================================================
SUBROUTINE input (inputfile)
  USE params, only: mfile
  USE dumps, only: dump, dump_set
  implicit none
  character(len=mfile) inputfile
!.......................................................................
  ! Where to find input
  inputfile = 'input.nml'
END SUBROUTINE

!=======================================================================
SUBROUTINE init_experiment
  USE params
  USE grid_m,  only : Ex, Ey, Ez, Bx, By, Bz, g
  USE species, only : sp, fields, particle
  USE particleoperations, only : global_to_cell_coordinates
  implicit none

  logical                 :: do_sine_field=.true.
  real                    :: B0(mcoord), v0(mcoord), r0(mdim), w, x, y, xs, ys
  integer                 :: ix, iy, iz
  real, parameter         :: twopi = 8.*atan (1.)
  type(particle), pointer :: pa
  character(len=mid) :: id='$Id$'
  namelist /init/ B0, v0, r0, w, do_sine_field
!.......................................................................
  call print_id(id)
  periodic = .true.

  do_maxwell = .false.

  ! background magnetic field
  B0 = (/1.0,1.0,1.0/)

  ! Velocity and position of particle
  r0 = g%rlb + (/ 0.25, 0.25, 0.5 /) * g%s
  v0 = (/0.,-1.,0.2/)

  ! Weight of particle.
  w = 1.0
  
  rewind(stdin); read (stdin,init)
  if (master) write(params_unit,init)
  if (out_namelists) write(stdout,init)

  if (do_sine_field) then
    ! Setup a non-trivial background field
    do iz=g%lb(3),g%ub(3)-1
    do iy=g%lb(2),g%ub(2)-1
    do ix=g%lb(1),g%ub(1)-1
      x  = g%x(ix)
      y  = g%y(iy)
      xs = g%x(ix) + 0.5*g%ds(1)
      ys = g%y(iy) + 0.5*g%ds(2)
      Bx(ix,iy,iz) = B0(1) * cos(2.*twopi*x)*cos(2.*twopi*ys + twopi/4.)
      By(ix,iy,iz) = B0(2) * sin(2.*twopi*xs)*cos(2.*twopi*y)
      Bz(ix,iy,iz) = B0(3) * sin(twopi*xs)*cos(2.*twopi*ys)
    enddo
    enddo
    enddo
  else
    Bx = B0(1)
    By = B0(2)
    Bz = B0(3)
  endif

  Ex=0.; Ey=0.; Ez=0

  ! Setup particle
  sp%np=0                                  ! zero all particles
  sp(1)%np = 1                             ! make one particle
  pa => sp(1)%particle(1)                  ! shorthand
  pa%w = w
  pa%p = v0
  pa%r = r0
  call global_to_cell_coordinates (pa)
  print *, pa%r, pa%q, pa%p, pa%w

END SUBROUTINE init_experiment
