!-----------------------------------------------------------------------!
! vim: nowrap
!=======================================================================
! Leave boundaries as they are, so no need to even read a namelist
!=======================================================================
SUBROUTINE init_experiment_boundaries
END SUBROUTINE init_experiment_boundaries

!=======================================================================
SUBROUTINE input (inputfile)
  USE params, only: mfile
  implicit none
  character(len=mfile) inputfile
!.......................................................................
  inputfile = 'input.nml'                                               ! Where to find input
END SUBROUTINE

!=======================================================================
SUBROUTINE init_experiment
  USE params
  USE grid_m,  only: Bx,By,Bz,Ex,Ey,Ez,g
  USE units,   only: c, elm
  USE species, only: sp, fields
  USE debug,   only: debugit, dbg_init
  USE particleoperations, only: make_particle_pair, make_particle
  implicit none
  real    :: d0                                                         ! background density
  real    :: v0                                                         ! veloccity amplitude 
  integer isp, i, ix, iy, iz, isign, ksign
  real :: x, y, z, sign, vz, v, w, wpe, m_electron, q_electron, vg(3)
  real, parameter:: pi=3.14159265
  logical :: do_pairs
  namelist /init/ d0, v0, do_pairs, ksign                               ! alternative perturb 
!.......................................................................
  periodic = .true.                                                     ! MUST define for periodic
  Bx=0; By=0; Bz=0; Ex=0; Ey=0; Ez=0                                    ! only plasmaoscillations

  ksign = 2
  d0 = 0.1                                                              ! background density
  v0 = 0.3                                                              ! streaming velocity w.r.t. rest frame
  do_pairs = .true.                                                     ! initialize in pairs to remove initial electric field
  rewind(stdin); read (stdin,init)
  if (master) write(params_unit,init)
  if (out_namelists) write(stdout,init)
  
  do isp=1,nspecies                                                     ! loop over species
    if (trim(sp(isp)%name) == 'electron') then
      m_electron = sp(isp)%mass                                         ! mass
      q_electron = abs(sp(isp)%charge)                                  ! size of charge
    end if
  end do
  wpe = sqrt(d0*c%fourpi*elm%ke/m_electron)*q_electron                  ! electron plasma freq
  if (master) then
    write(stdout,*) hl
    write(stdout,*) 'Electron plasma frequency =',wpe
    write(stdout,*) 'Electron skin depth [dx]  =',(c%c / wpe) / g%ds(1)
    write(stdout,*) 'Rel skin depth [dx]       =',(c%c / wpe) / g%ds(1) * sqrt(abs(v0))
  endif

  if (do_pairs) then
    if (master) write(stdout,*) 'Making particles in pairs'

    do isp=1,nspecies,2                                                 ! loop over species
      do iz=g%lb(3),g%ub(3)-1                                           ! loop over grid cells
        w = d0/(sp(isp)%ntarget)                                        ! density weight
        do iy=g%lb(2),g%ub(2)-1                                         ! y-loop
          do ix=g%lb(1),g%ub(1)-1                                       ! x-loop
            do isign=0,1
            sign = 1 - ksign*isign
            vz = sign * v0
            vg = (/0.,0.,vz/)
            do i=1,sp(isp)%ntarget/2                                    ! particles/cell
              call make_particle_pair(isp,isp+1,ix,iy,iz,vg,w,.false.)  ! add particle pair
              fields(ix,iy,iz,isp)%d = d0                               ! number density
            end do
            end do
          end do
        end do
      end do
    end do

  else

    do isp=1,nspecies                                                   ! loop over species
      do iz=g%lb(3),g%ub(3)-1                                           ! loop over grid cells
        w = d0/(sp(isp)%ntarget)                                        ! density weight
        do iy=g%lb(2),g%ub(2)-1                                         ! y-loop
          do ix=g%lb(1),g%ub(1)-1                                       ! x-loop
            do isign=0,1
            sign = 1 - ksign*isign
            vz = sign * v0
            vg = (/0.,0.,vz/)
            do i=1,sp(isp)%ntarget/2                                    ! particles/cell
              call make_particle(isp,ix,iy,iz,vg,w)                     ! add particle
              fields(ix,iy,iz,isp)%d = d0                               ! number density
            end do
            end do
          end do
        end do
      end do
    end do
  end if

!-----------------------------------------------------------------------  
! Fields for testing interpolations (use nz=48).  Since both Ez and By
! are staggered in the z-direction, with the lb point physically at the
! coordinate rlb + dz/2, the Ey field below goes through zero at the 
! first cell mid point, and we should get negative values for fractional 
! coordinates less than 0.5, while the Bz field is centered, and goes to
! zero at rlb, and we should get positive values for all fractional 
! coordinates. With nz/4 = 12 the sin() values are {0.0,0.5,..}, with a 
! relatively linear range between -0.5 and +0.5.
!-----------------------------------------------------------------------  
  if (debugit(dbg_init,2)) then
    print*,'INIT: g%z(lb), g%rlb =',g%z(g%lb(3)), g%rlb(3), g%gs(3)
    do iz=g%lb(3),g%ub(3)-1
      bz(:,:,iz) = sin(2.*pi*4.*(g%z(iz)-g%rlb(3))/g%gs(3))
      ez(:,:,iz) = sin(2.*pi*4.*(g%z(iz)-g%rlb(3))/g%gs(3))
    end do
    call overlap_z (bz)
    call overlap_z (ez)
  end if
END SUBROUTINE init_experiment
