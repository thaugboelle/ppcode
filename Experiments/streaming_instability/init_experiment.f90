!-----------------------------------------------------------------------!
! vim: nowrap
!=======================================================================
! Leave boundaries as they are, so no need to even read a namelist
!=======================================================================
SUBROUTINE init_experiment_boundaries
END SUBROUTINE init_experiment_boundaries

!=======================================================================
SUBROUTINE input (inputfile)
  USE params, only: mfile
  implicit none
  character(len=mfile) inputfile
!.......................................................................
  inputfile = 'input.nml'                                               ! Where to find input
END SUBROUTINE

!=======================================================================
SUBROUTINE init_experiment
  USE params
  USE grid_m,             ONLY: Bx,By,Bz,Ex,Ey,Ez,g
  USE units,              ONLY: c, elm
  USE species,            ONLY: sp, fields
  USE particleoperations, ONLY: make_particle
  implicit none
  real    :: d0                                                         ! background density
  real    :: v0                                                         ! veloccity amplitude 
  integer :: kx, ky, kz                                                     ! wave numbers
  integer isp, i, ix, iy, iz
  real :: x, y, z, sign, vz, vg(3), v, a0, w, wpe, m_electron, q_electron
  real, parameter:: pi=3.14159265
  namelist /init/ a0, d0, v0, kx, ky                                    ! alternative perturb 
!.......................................................................
  periodic = .true.                                                     ! MUST define for periodic
  Bx=0; By=0; Bz=0; Ex=0; Ey=0; Ez=0                                    ! only plasmaoscillations

  kx = 1                                                                ! wavenumber in x-dir
  ky = 0                                                                ! wavenumber in y-dir
  kz = 1
  d0 = 0.1                                                              ! background density
  v0 = 0.3                                                              ! perturb strength
  a0 = 0.1
  rewind(stdin); read (stdin,init)
  if (master) write(params_unit,init)
  if (out_namelists) write(stdout,init)
  
  do isp=1,nspecies                                                     ! loop over species
    if (trim(sp(isp)%name) == 'electron') then
      m_electron = sp(isp)%mass
      q_electron = sp(isp)%charge
    end if
  end do
  wpe = sqrt(d0*c%fourpi*elm%ke/m_electron)*q_electron                  ! electron plasma freq
  if (master) write(stdout,*) 'Electron plasma frequency =',wpe

  do isp=1,nspecies                                                     ! loop over species
    sign = 2*modulo(isp,2)-1.                                           ! alternating signs
    do iz=g%lb(3),g%ub(3)-1                                             ! loop over grid cells
      w = d0/(sp(isp)%ntarget)                                          ! density weight
      z = (g%z(iz)-g%grlb(3))/g%gs(3)                                   ! 0 < z < 1
      v = v0*sign*(1.+a0*cos(2.*pi*kz*z))
      do iy=g%lb(2),g%ub(2)-1                                           ! y-loop
        y = (g%y(iy)-g%grlb(2))/g%gs(2)                                 ! 0 < y < 1
        do ix=g%lb(1),g%ub(1)-1                                         ! x-loop
          x = (g%x(ix)-g%grlb(1))/g%gs(1)                               ! 0 < x < 1
          vz = v*cos(2.*pi*(kx*x+ky*y))*m_electron/sp(isp)%mass         ! amplitude 
          vg = (/0.,0.,vz/)
          do i=1,sp(isp)%ntarget                                        ! particles/cell
            call make_particle(isp, ix, iy, iz, vg, w)                  ! add particle
            fields(ix,iy,iz,isp)%d = d0                                 ! number density
          end do
        end do
      end do
    end do
  end do

END SUBROUTINE init_experiment
