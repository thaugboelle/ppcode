import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from matplotlib import is_interactive
interactive = is_interactive ()
if not interactive: plt.ion ()

def drift (dt):

    global x, y

    x += vx*dt
    y += vy*dt

def kick (dt):

    global vx, vy
    vz = 0.

    # Scaled, effective magnetic field (purely vertical for now)
    bx = 0.
    by = 0.
    bz = 0.5*Sz*dt

    # Scaled, effective electric field
    ex = -shear*x*bz
    ey = 0.
    ez = 0.

    # Standard Boris push
    vmx = vx + ex
    vmy = vy + ey
    vmz = vz + ez

    vpx = vmx + (vmy*bz - vmz*by)
    vpy = vmy + (vmz*bx - vmx*bz)
    vpz = vmz + (vmx*by - vmy*bx)

    fac = 2./(1. + bx*bx + by*by + bz*bz)

    vx = vmx + fac*(vpy*bz - vpz*by) + ex
    vy = vmy + fac*(vpz*bx - vpx*bz) + ey
    vz = vmz + fac*(vpx*by - vpy*bx) + ez

###################
# Free parameters #
###################

# Rotation frequency and power law exponent
Omega = .25
qshear = 1.5

# Cyclotron frequency times z-component of magnetic field unit vector.
# This is positive (negative) if the magnetic field is parallel (antiparallel)
# to rotation axis.
ocbz = 1.

# Time step
dt = 0.002
dt = 0.02
dt = 0.2

# Amplitude, phase, and location of guiding center
ampl = .5
phi = np.pi
x0 = 0.
y0 = 0.

# Kick-Drift or Drift-Kick integration
KD = True

# Use 4th order Candy-Rozmus ?
# J. Candy and W. Rozmus, Journal of Computational Physics 92, 230 (1991).
CR = False

# Box dimensions
xmin, xmax = -1.0, 1.0
xmin, xmax = -0.4, 0.4
ymin, ymax = -1.0, 1.0

#################################
# Integrate equations of motion #
#################################

# CR sub step prefactors to dt -- initialized with velocity at time = - aa / 2 * dt
if CR:
  aa = 1/(2 - 2 ** (1./3.))          # substep factor
else:
  aa = 1.

alpha = [aa, 1 - 2*aa, aa]           # Kick times 
beta  = [0.5-0.5*aa, 0.5-0.5*aa, aa] # Drift times
KD = KD or CR


# Rate of shear
shear = -qshear*Omega

# Spin
Sz = ocbz + 2.*Omega

# Half time step
dt2 = .5*dt*aa

# Gyration frequency
og = np.sqrt (Sz*(Sz + shear))

# Correct for discretization error -- Correction in CR case is unknown
if not CR:
  og = np.arcsin (np.sqrt ((og*dt2)**2/(1 + (Sz*dt2)**2)))/dt2

# Periods
period = 2.*np.pi/og
tend = 1000.*period

# Exact solutions
xt = lambda t:          ampl*np.cos (og*t + phi) + x0
yt = lambda t: -(Sz/og)*ampl*np.sin (og*t + phi) + y0 + shear*t*x0
vxt = lambda t: -og*ampl*np.sin (og*t + phi)
vyt = lambda t: -Sz*ampl*np.cos (og*t + phi) + shear*x0

# Initial condition
if KD:
    t = -dt2
else:
    t = +dt2

x = xt (t)
y = yt (t)
vx = vxt (t)
vy = vyt (t)

if KD:
    drift (+dt2)
    t += dt2
else:
    drift (-dt2)
    t -= dt2

# Box size
Lx = xmax - xmin
Ly = ymax - ymin

# Python lists to hold result
result = [(x,y,vx,vy,t)]

# Time loop
while t < tend:

    if CR:
       for i in range(0,3):
         kick(alpha[i]*dt)
         drift(beta[i]*dt)
    else:
       if KD:
           kick (dt)
           drift (dt)
       else:
           drift (dt)
           kick (dt)

    t += dt

    boost = 0.

    if x >= xmax:
        x -= Lx
        boost = -shear*Lx
    if x < xmin:
        x += Lx
        boost = +shear*Lx

    y += boost*t
    vy += boost

    while y >= ymax:
        y -= Ly
    while y < ymin:
        y += Ly

    result.append ((x,y,vx,vy,t))

# Convert result into NumPy recarray
dtype = [(var, float) for var in ('x','y','vx','vy','t')]
f = np.array (result, dtype).view (np.recarray)

###############
# Plot result #
###############

# Create figure
fig = plt.figure (1, figsize = (12, 4))
fig.clear ()

# Specify layout
gs = gridspec.GridSpec(1, 2, width_ratios=[1,2])

# Parametric plot of particle orbit
ax1 = fig.add_subplot (gs[0])
ax1.plot (f.x, f.y, '.', ms = 2)
ax1.set_xlim (xmin, xmax)
ax1.set_ylim (ymin, ymax)
ax1.set_xlabel ('x')
ax1.set_ylabel ('y')

if KD:
    vya = vyt (f.t - dt2)
else:
    vya = vyt (f.t + dt2)

# Line plot of vy (t)
ax2 = fig.add_subplot (gs[1])
ax2.plot (f.t/period, vya, 'r', f.t/period, f.vy, 'k')
ax2.set_xlim (tend/period - 5., tend/period)
ax2.set_xlabel ('Epicyclic periods')
ax2.set_ylabel ('vy')

# Reduce white space
fig.set_tight_layout (True)

if not interactive: plt.ioff ()
