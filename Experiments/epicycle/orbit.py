import ppcode
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.ticker import MultipleLocator
import matplotlib.gridspec as gridspec

import os, re
from re import match

params = ppcode.Params ()
ampl = params.init.ampl
phi = params.init.phi
x0 = params.init.x0
y0 = params.init.y0
Omega = params.shearing.omega_r
S = params.shearing.shear

kappa = np.sqrt (2*Omega*(2*Omega + S))

yt = lambda t: -(2*Omega/kappa)*ampl*np.sin (kappa*t + phi) + y0 + S*t*x0

datadir = os.path.dirname (os.path.realpath (__file__)) + '/Data'

filelist = os.listdir (datadir)
filenames = [f for f in filelist if re.match (r'particles-[0-9]{6}\.dat', f)]
filenames.sort ()

filename = filenames.pop (0)
(p, h) = ppcode.rp (datadir + '/' + filename)
x = [p.x]
y = [p.y]
t = [h.time]

for filename in filenames:
    (p, h) = ppcode.rp (datadir + '/' + filename)
    x.append (p.x)
    y.append (p.y)
    t.append (h.time)

x = np.array (x)
y = np.array (y)
t = np.array (t)

plt.ion ()

fig = plt.figure (1, figsize = (12, 6))
fig.clf ()

gs = gridspec.GridSpec(1, 2, width_ratios=[1,3])

ax1 = fig.add_subplot (gs[0])
ax2 = fig.add_subplot (gs[1], sharey = ax1)

ax1.plot (x, y)

ax1.set_xlim (h.rlb[0], h.rub[0])
ax1.set_ylim (h.rlb[1], h.rub[1])
ax1.set_aspect ('equal')
ax1.xaxis.set_major_locator (MultipleLocator (0.5))
ax1.yaxis.set_major_locator (MultipleLocator (0.5))
ax1.grid ()

orbits = Omega*t/(2*np.pi)
ax2.plot (orbits, y, 'k', orbits, yt (t), 'r--')
ax2.set_xlim (round (orbits.min ()), round (orbits.max ()))

fig.set_tight_layout (True)
