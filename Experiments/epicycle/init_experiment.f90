! $ Date: 2013-09-02 09:40:06 +0200 $
! vim: nowrap
!-----------------------------------------------------------------------!
! Initialize a single particle in a homogenous magnetic field 
! in a shearing-box experiment
!=======================================================================
! Leave boundaries as they are, so no need to even read a namelist
!=======================================================================
SUBROUTINE init_experiment_boundaries
END SUBROUTINE init_experiment_boundaries

!=======================================================================
SUBROUTINE input (inputfile)
  USE params, only: mfile
  USE dumps, only: dump, dump_set
  implicit none
  character(len=mfile) inputfile
!.......................................................................
  ! Where to find input
  inputfile = 'input.nml'
END SUBROUTINE

!=======================================================================
SUBROUTINE init_experiment
  USE params
  USE grid_m,   only : Ex, Ey, Ez, Bx, By, Bz, g
  USE species,  only : sp, fields, particle
  USE hybrid_m, only : do_drift
  USE units,    only : elm
  USE particleoperations, only : global_to_cell_coordinates
  implicit none

  real, dimension (mcoord) :: B0
  real                     :: ampl, phi, x0, y0, kappa, oB, Sz, t0
  real                     :: x, y, z, vx, vy, vz
  real, parameter          :: twopi = 8.*atan (1.)
  type(particle), pointer  :: pa
  character(len=mid) :: id='$Id$'
  namelist /init/ B0, ampl, phi, x0, y0
!.......................................................................
  call print_id(id)
  periodic = .true.

  print *, 'Shear :', shear

  ! Evaluate everything at t = -dt / 2 ad then drift particles forward in time
  t0 = - dt / 2.
  do_drift = .true.

  do_maxwell = .false.

  ! background magnetic field
  B0 = (/ 0.0, 0.0, 1.0 /)

  ! Amplitude and starting phase
  ampl = 0.5
  phi  = 0.

  ! Center of epicycle
  x0 = 0.
  y0 = 0.
  
  rewind(stdin); read (stdin,init)
  if (master) write(params_unit,init)
  if (out_namelists) write(stdout,init)

  ! Cycltron frequency in the z-direction
  oB = abs(sp(1)%charge) / sp(1)%mass * elm%kf * B0(3)

  ! Spin
  Sz = oB + 2.*Omega_r

  ! Epicyclic frequency
  kappa = sqrt (Sz*(Sz + shear)) 

  ! Correct for discretization error
  kappa = Asin( sqrt( (kappa * dt / 2.)**2 / (1. + (Sz * dt / 2.)**2) ) ) / (dt / 2.)

  ! Particle position
  x =             ampl*cos (kappa*t0 + phi) + x0
  y = -(Sz/kappa)*ampl*sin (kappa*t0 + phi) + y0 + shear*t0*x0
  z = g%grlb(3) + .5*g%gs(3)

  ! Particle velocity
  vx = -kappa*ampl*sin (kappa*t0 + phi)
  vy =    -Sz*ampl*cos (kappa*t0 + phi) + shear*x0
  vz = 0.

  ! Setup particle
  sp%np=0                                  ! zero all particles
  sp(1)%np = 1                             ! make one particle
  pa => sp(1)%particle(1)                  ! shorthand
  pa%r = (/x, y, z/)
  pa%p = (/vx, vy, vz/)
  pa%w = 1.0
  call global_to_cell_coordinates (pa)
  print *, 'Particle initial data r, q, p, w :', pa%r, pa%q, pa%p, pa%w

  ! Setup magnetic field
  Bx = B0(1)
  By = B0(2)
  Bz = B0(3)

  ! Setup electric field
  Ex = 0.0
  Ey = 0.0
  Ez = 0.0

END SUBROUTINE init_experiment
