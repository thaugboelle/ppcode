!-----------------------------------------------------------------------!
! vim: nowrap
!=======================================================================
! Leave boundaries as they are, so no need to even read a namelist
!=======================================================================
SUBROUTINE init_experiment_boundaries
implicit none
END SUBROUTINE init_experiment_boundaries

!=======================================================================
SUBROUTINE input (inputfile)
  USE params, ONLY: mfile
  implicit none
  character(len=mfile) inputfile
!.......................................................................
  inputfile = 'input_pic.nml'                                               ! Where to find input
END SUBROUTINE

!=======================================================================
SUBROUTINE init_experiment
  USE params
  USE grid_m,             ONLY: Bx,By,Bz,Ex,Ey,Ez,g
  USE units,              ONLY: c, elm
  USE species,            ONLY: sp, fields
  USE debug,              ONLY: debugit, dbg_init
  USE particleoperations, ONLY: make_particle_pair, make_particle
  implicit none
  real    :: d0                                                         ! background density
  real    :: v0                                                         ! veloccity amplitude
  integer isp, i, ix, iy, iz, isign, ksign
  real :: x, y, z, sign, vz, v, w, wpe, m_electron, q_electron, vg(3)
  real, parameter:: pi=3.14159265
  logical :: do_pairs
  namelist /init/ d0, v0, do_pairs, ksign                                ! alternative perturb
!.......................................................................
  periodic = .true.                                                     ! MUST define for periodic

  Bx=0.                                                                 ! No fields initially
  By=0.
  Bz=0.
  Ex=0.
  Ey=0.
  Ez=0.

  d0 = 0.1                                                              ! background density
  v0 = 0.0                                                              ! streaming velocity w.r.t. rest frame is zero

  do_pairs = .true.                                                     ! initialize in pairs to remove initial electric field

  rewind(stdin); read (stdin,init)                                      ! rewind file descriptor and read input from file
  if (master) write(params_unit,init)
  if (out_namelists) write(stdout,init)

  v0 = 0.0                                                              ! streaming velocity w.r.t. rest frame is STILL zero

  do isp=1,nspecies                                                     ! loop over species
    if (trim(sp(isp)%name) == 'electron') then
      m_electron = sp(isp)%mass                                         ! mass
      q_electron = abs(sp(isp)%charge)                                  ! size of charge
    end if
  end do

  wpe = sqrt(d0*c%fourpi*elm%ke/m_electron)*q_electron                  ! electron plasma freq

  if (master) then
    write(stdout,*) hl
    write(stdout,*) 'Electron plasma frequency =',wpe
    write(stdout,*) 'Electron skin depth [dx]  =',(c%c / wpe) / g%ds(1)
    write(stdout,*) 'Ion      skin depth [dx]  =',(c%c / wpe) / g%ds(1)
  endif
  
  if (do_pairs) then
   if (master) write(stdout,*) 'Making particles in pairs'
   do isp=1,nspecies,2
     do iz=g%lb(3), g%ub(3)-1
       w = d0/(sp(isp)%ntarget)
       do iy=g%lb(2),g%ub(2)-1
         do ix=g%lb(1), g%ub(1)-1
           do i=1, sp(isp)%ntarget
             call make_particle_pair(isp,isp+1,ix,iy,iz,vg,w,.false.)
             fields(ix,iy,iz,isp)%d = d0
           end do
         end do
       end do
     end do
   end do
  else
   do isp=1,nspecies
    do iz=g%lb(3), g%ub(3)-1
      w = d0/(sp(isp)%ntarget)
      do iy=g%lb(2), g%ub(2)-1
       do ix=g%lb(1), g%ub(1)-1
        do i=1, sp(isp)%ntarget
         call make_particle(isp,ix,iy,iz,vg,w)
         fields(ix,iy,iz,isp)%d = d0
        end do
       end do
      end do
    end do
   end do
  end if
END SUBROUTINE init_experiment
