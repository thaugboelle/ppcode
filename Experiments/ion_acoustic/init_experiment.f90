! Experiments/alfven_wave/init_experiment.f90 $ Date: 2013-09-02 09:40:06 +0200 $
! vim: nowrap
!-----------------------------------------------------------------------!
! Initialize a circularly polarized Alfven wave propagating parallel
! to an ambient magnetic field pointing in the x-direction. At the
! moment we assume that the ions are cold. In the future we should make
! them hot and look at cyclotron damping.
!
! SCALINGS: When changing the total volume and nothing else, wpe should
! remain the same. The total energies should scale with the volume. The
! particle weights remain the same, and therefore the charge densities 
! and current densities should also remain the same.  That, again, should
! mean that the field values remain the same, and that the total EM
! energy therefore indeed should scale with the volume.
!
! The coherent initial current density is the same when the wavelength 
! is shorter, but the divergence of it is larger, so the electric charge
! grows faster.  The coherent charge densities scale inversely with the
! size, while the coherent electric potential scales linearly with the
! size.  The noise charge density, on the other hand, starts out the 
! same, so the initial noise electric field scales with the size, and is
! smaller for a smaller size.

!=======================================================================
! Leave boundaries as they are, so no need to even read a namelist
!=======================================================================
SUBROUTINE init_experiment_boundaries
END SUBROUTINE init_experiment_boundaries

!=======================================================================
SUBROUTINE input (inputfile)
  USE params, only: mfile
  USE dumps, only: dump, dump_set
  implicit none
  character(len=mfile) inputfile
!.......................................................................
  ! Where to find input
  inputfile = 'input.nml'
END SUBROUTINE

!=======================================================================
SUBROUTINE init_experiment

  USE params
  USE grid_m,  ONLY : Bx, By, Bz, Ex, Ey, Ez, g
  USE units,   ONLY : c
  USE species, ONLY : sp, fields, particle
  USE particleoperations, only : global_to_cell_coordinates, make_particle
  USE dumps,   ONLY : dump, dump_set
  USE io,      only: verbose
  USE ion_acoustic_mod

  implicit none

  real :: d0
  real :: hel, phi, w, q, m, rho0, oc, kva, va, vt
  logical :: lerr
  integer :: i, isp, ix, iy, iz, np, ip
  real, parameter:: pi = 4.*atan (1.)
  integer ::   gsend
  integer, parameter :: Ntot = 100000
  real(kind=4) :: x_vec(Ntot), v_vec(Ntot)
  integer :: mode
  real(kind=8) :: omeg_0, gam, k, Ti_Te, pert_str
  type(particle), pointer :: pa
  character(len=mid) :: id='Experiments/ion_acoustic/init_experiment.f90 $Id$'
  namelist /init/  d0
!.......................................................................
  call print_id(id)
  call dump_set ('experiment.dmp')
  ! MUST define for periodic
  periodic = .true.

  ! background number density
  d0 = 1.

  Bx = 0.0
  By = 0.0
  Bz = 0.0
  Ex = 0.0
  Ey = 0.0
  Ez = 0.0

  ix = g%lb(1)
  iy = g%lb(2)
  iz = g%lb(3)

  Ti_Te = 0.1         ! ratio of ion and electron temperature
  pert_str = 0.999  ! max(\delta f/f), should be much less than 1
  omeg_0   = 14.8177753856
  gam      = -0.231823304526
  mode     = 2
  k        = mode * 2.0*pi    ! Wave number 

  call initial_phase_space(omeg_0, gam, k, Ti_Te, pert_str, Ntot, x_vec, v_vec)
  !print *, x_vec
! Loop over species
do isp=1,nspecies
 w = d0/(sp(isp)%ntarget)
  do ip=1, Ntot

      ! add particle
      call make_particle(isp, ix, iy, iz, (/0.,0.,0./), w)
      np = sp(isp)%np
      pa => sp(isp)%particle(ip)
      
      pa%r = (/x_vec(ip), 0.0, 0.0 /)
      call global_to_cell_coordinates (pa)
      
      pa%p(1) = v_vec(ip)
      pa%p(2) = 0.0
      pa%p(3) = 0.0

  end do
enddo

if (master) then
  print *, '----------------------------'
  
  print *, '----------------------------'
endif


gsend=1
i = 0
do while (gsend > 0)                                                  ! distribute correctly
  i = i + 1
  call SendParticles_lowmem(gsend)                                    ! max mpi%n-1 hops
  if ((master .or. verbose > 0) .and. gsend > 0) write(stdout,'(a,i5.5,a,i4,a,i10)') &
                 'Rank=',rank,' Sending particles. It=',i, ' #send=', gsend
enddo

END SUBROUTINE init_experiment

