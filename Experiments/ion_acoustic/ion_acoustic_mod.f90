
MODULE ion_acoustic_mod

CONTAINS

SUBROUTINE initial_phase_space(omeg_0, gam, k, Ti_Te, pert_str, N, x_vec, v_vec)
implicit none
integer, intent(in) :: N
real(kind=4), intent(out) :: x_vec(N), v_vec(N)
integer, parameter :: N_grid = 1000, N2 = 1000000
real(kind=8), intent(in) :: omeg_0, gam, k, Ti_Te, pert_str
real(kind=4), allocatable :: rand1(:), rand2(:), rand3(:), rand4(:)
real(kind=4), allocatable :: randn1(:), randn2(:) ! Gaussian random
real(kind=4) :: prob, rand_x, rand_v
real(kind=8) :: vt, vmax, cs, L, phi, pi, alpha, A, fmax
real(kind=8) :: f_perp_val, f_v_val
real(kind=4),dimension(N_grid) :: xx,vv
real(kind=8),dimension(N_grid*N_grid) :: temp
integer :: mode, i , j, m
logical :: test
integer, parameter :: out_unit=20

allocate(rand1(N2))
allocate(rand2(N2))
allocate(rand3(N2))
allocate(rand4(N2))
allocate(randn1(N2))
!allocate(randn2(N))


call init_random_seed() 


pi = 4.*atan(1.) ! How to make global?

test = .false.

mode  = 2
!Ti_Te = 0.1         ! ratio of ion and electron temperature
!pert_str = 0.999  ! max(\delta f/f), should be much less than 1
vmax = 8.0          ! to include the tails of the Maxwellian this should be larger


! v_t/cs = sqrt (T_i/T_e)
alpha = sqrt(Ti_Te)
! Leading order sound speed (cs^2 = T_e/m_i)
cs = 1.             ! sound speed
vt    = alpha*cs    ! thermal velocity
vmax  = vmax*vt     ! maximum velocity for the particles, it's a cut off
L  = 1.0            ! Length of the box
phi = 0.0           ! phase of the electric field, zero, for now
!k = mode * 2.0*pi/L    ! Wave number

! Growth rate from linear theory. Should be read from a list.
!omeg_0   = 14.8177753856
!gam      = -0.231823304526

! Make grid
do i = 1, N_grid
   vv(i) = -vmax + (i-1)*(2.0*vmax)/N_grid
   xx(i) = 0.0 + (i-1)*L/N_grid
enddo

! Find maximum of f_perp and determine A
m = 1
do i = 1, N_grid
   do j = 1, N_grid
      temp(m) = f_perp(xx(i), vv(j), k, omeg_0, gam, phi, 1.0_8, vt)
      m = m + 1
   enddo
enddo 
A = pert_str/maxval(temp)
!print *, "A is ", A

! Find maximum of f
m = 1
do i = 1, N_grid
   do j = 1, N_grid
      f_v_val    = f_v(vv(j), vt) 
      f_perp_val = f_perp(xx(i), vv(j), k, omeg_0, gam, phi, A, vt)
      temp(m)    = f_v_val*( 1.0 + f_perp_val ) 
      m = m + 1
   enddo
enddo 
fmax = maxval(temp)
!print *, "fmax is ", fmax


j = 1
m = 0
do while (j .lt. N)
m = m + 1
call random_number(rand1)
call random_number(rand2)
call random_number(rand3)
call random_number(rand4)
randn1 = sqrt( - 2.0 * log(rand1) ) * cos( 2.0 * pi * rand2 ); ! okay to always use this one?
!randn2 = sqrt( - 2.0 * log(rand1) ) * sin( 2.0 * pi * rand2 );

do i = 1, N2
   rand_x = rand3(i)                          ! random number between 0 and 1
   rand_v = randn1(i)*vt                         ! random number between -vmax and vmax
   f_perp_val= f_perp(rand_x, rand_v, k, omeg_0, gam, phi, A, vt)
   f_v_val= f_v(rand_v, vt)
   prob   = f_v_val*( 1.0 + f_perp_val ) / (0.01+1.0 + pert_str)/f_v(rand_v, vt)
   if ( prob .gt. rand4(i) ) then
      !print *, "Found a point ", rand_x, rand_v
      x_vec(j) = rand_x
      v_vec(j) = rand_v
      j = j + 1 
   endif
   if (j>N) exit
enddo
print *, "First ", N2, "particles found."
enddo

print *, "Found ", j-1, " points using ", N2*(m-1) + i, "attempts"
print *, "So the effectiveness is ", (j-1.0)/(N2*(m-1) + i) * 100.



deallocate(rand1)
deallocate(rand2)
deallocate(rand3)
deallocate(rand4)
deallocate(randn1)
!deallocate(randn2)



END SUBROUTINE initial_phase_space

!
real(kind=8) function f_v(v, vt)
   implicit none
   real(kind=4), intent(in) :: v
   real(kind=8), intent(in) :: vt
   real(kind=8) :: pi 
   
   pi = 4.*atan(1.) ! How to make global?
   f_v = 1.0/sqrt(2*pi) /vt * exp(-(v/vt)**2.0/2.0)
end function f_v


! Helper function
real(kind=8) function f_perp(x, v, k, omeg_0, gam, phi, A, vt)
   implicit none
   real(kind=8) :: delta
   real(kind=8), intent(in) :: k, omeg_0, gam, phi, A , vt
   real(kind=4), intent(in) :: x, v
   !print *, "x, v, k, omeg_0, gam, phi", x, v, k, omeg_0, gam, phi
   delta = omeg_0 - k * v
   f_perp =  - v/vt**2*A * 2./(delta**2.+gam**2.)*( delta*sin(k*x+phi) - gam*cos(k*x+phi) )
end function f_perp

! Full distribution function
real(kind=8) function f(x, v, k, omeg_0, gam, phi, f_perp_val, f_v_val)
   implicit none
   real(kind=4), intent(in) :: x, v
   real(kind=8), intent(in) :: k, omeg_0, gam, phi, f_perp_val, f_v_val

   f = f_v_val*( 1.0 + f_perp_val ) 

end function f

END MODULE ion_acoustic_mod