! \file particleoperations.f90
! vim: nowrap
! Particles/particleoperations.f90 $Id$
!=======================================================================
MODULE particleoperations
  !
  implicit none
  !
  INTERFACE thermalvelocity                                             ! get thermal vel
    MODULE PROCEDURE thermalvelocity1
  END INTERFACE
  !
  INTERFACE addone                                                      ! Add a particle
    MODULE PROCEDURE addone1                                            ! in an unit cell
  END INTERFACE
  !
CONTAINS

!=======================================================================
SUBROUTINE check_ghostzone(iz)
  USE params, only : rank,mpi
  USE grid_m,   only : g
  integer                :: iz

  if ( (iz .lt. g%lb(3) .and. (.not. mpi%lb(3) .or. mpi%interior(3)))   .or.   &
       (iz .ge. g%ub(3) .and. (.not. mpi%ub(3) .or. mpi%interior(3))) ) then
    write (*,'(a,a,i4,a,i4,a,i4,a,i4,a,i4)') 'You are putting a particle in ghostzone', &
      '  rank:', rank, ',  iz:', iz, ',  iz-mpi%offset(3):', iz-mpi%offset(3)
  end if
  iz = max(1,min(iz,g%n(3)))
END SUBROUTINE check_ghostzone

!=======================================================================
FUNCTION thermalvelocity1 (ix,iy,iz,isp)                                ! Compute thermal vel
  USE params,        only : mcoord, tsize, do_2d2v
  USE units,         only : c
  USE distributions, only : maxwell
  implicit none
  real, dimension(mcoord) :: thermalvelocity1
  integer, intent(in)     :: ix,iy,iz,isp
  real                    :: speed, length
  integer                 :: i,j
  real, external          :: rand
  call check_ghostzone(iz)
  length = 1.1                                                          ! construct unit vector
  thermalvelocity1 = 0.0
  if (do_2d2v) then
    do while (length .gt. 1.)
      length = 0.
      do i=1,mcoord,2                                                   ! get unit vector
        thermalvelocity1(i) = 2.*(rand(ix,iy,iz) - .5)                  ! -1 < r(i) < 1
        length = length + thermalvelocity1(i)**2
      enddo
    enddo
  else
    do while (length .gt. 1.)
      length = 0.
      do i=1,mcoord                                                     ! get unit vector
        thermalvelocity1(i) = 2.*(rand(ix,iy,iz) - .5)                  ! -1 < r(i) < 1
        length = length + thermalvelocity1(i)**2
      enddo
    enddo
  endif
  j = min(tsize,1 + floor(rand(ix,iy,iz)*tsize))
  speed = maxwell(isp)%v(j)                                             ! size from maxwell
  thermalvelocity1 = thermalvelocity1 / sqrt(length) * speed            ! final thermal v*g
END FUNCTION thermalvelocity1

!=======================================================================
SUBROUTINE addone1 (pa, ix,iy,iz)                                       ! Insert into global array
  USE params,  only : mdim, mpi
  USE species, only : particle
  integer, intent(in)     :: ix,iy,iz
  type(particle), pointer :: pa
  integer                 :: i
  real, external          :: rand
  do i=1,mdim                                                           ! Normalised position inside cell
    pa%r(i) = rand(ix,iy,iz)                                            ! 0<=r(i)<1
  enddo
  pa%q = (/ix, iy, iz/) + mpi%offset                                    ! Cell number
END SUBROUTINE addone1

!=======================================================================
FUNCTION global_coordinates (pa)
  USE params,  only : mdim
  USE species, only : particle
  USE grid_m,    only : g
  implicit none
  real, dimension(mdim)   :: global_coordinates
  type(particle)          :: pa
!.......................................................................
  global_coordinates = (real(pa%r,kind=8)+pa%q)*real(g%ds,kind=8) + g%grlb
END FUNCTION global_coordinates

!=======================================================================
SUBROUTINE global_to_cell_coordinates (pa)
  USE species, only : particle
  USE grid_m,    only : g
  implicit none
  type(particle)          :: pa
!.......................................................................
  pa%q = floor((pa%r - g%grlb) * g%ods)
  pa%r = (pa%r - g%grlb)*g%ods - pa%q
END SUBROUTINE global_to_cell_coordinates

!-----------------------------------------------------------------------
! convert all particles from the old real coordinate to a normalised
! coordinate + integer cell index

!=======================================================================
SUBROUTINE all_global_to_cell_coordinates
  USE params,  only : nspecies, mdim
  USE species, only : sp
  USE grid_m,    only : g
  implicit none
  integer                  :: isp
  integer                               :: ip
  real(kind=8), dimension(mdim)         :: lb, ods
!.......................................................................
  ods = g%ods
  lb  = g%grlb
  do isp=1,nspecies
    do ip =1,sp(isp)%np
      sp(isp)%particle(ip)%q = floor((sp(isp)%particle(ip)%r - lb) * ods)
      sp(isp)%particle(ip)%r = (sp(isp)%particle(ip)%r - lb)*ods - sp(isp)%particle(ip)%q
    enddo
  enddo
END SUBROUTINE all_global_to_cell_coordinates

! convert all particles in a species from the old real coordinate to a 
! normalised coordinate + integer cell index
!=======================================================================
SUBROUTINE sp_global_to_cell_coordinates(isp)
  USE params,    only : nspecies, mdim
  USE species,   only : particle, sp
  USE grid_m,    only : g
  implicit none
  integer, intent(in)                   :: isp
  type(particle), pointer, dimension(:) :: pa
  integer                               :: ip
  real(kind=8), dimension(mdim)         :: lb, ods
  ods = g%ods
  lb  = g%grlb
  if (sp(isp)%np > 0) pa => sp(isp)%particle
  do ip =1,sp(isp)%np
    pa(ip)%q = floor((pa(ip)%r - lb) * ods)
    pa(ip)%r = (pa(ip)%r - lb)*ods - pa(ip)%q
  enddo
END SUBROUTINE sp_global_to_cell_coordinates

! Make a particle of species isp in cell (ix,iy,iz)
! It has proper bulk velocity vgam and weight w. The temperature
! is sampled according to the relativistic Maxwell-J�ttner
! disitrubtion with temperature sp(isp)%temperature
!=======================================================================
SUBROUTINE make_particle(isp, ix, iy, iz, vgam, w)
  USE params,             only : master, mcoord
  USE species,            only : sp, particle
  USE units,              only : c
  USE debug,              only : debugit, dbg_init
  implicit none
  integer, intent(in)     :: isp, ix, iy, iz
  real,    intent(in)     :: vgam(mcoord), w
  type(particle), pointer :: pa
  real                    :: gam_bulk, gam_th
!.......................................................................
  pa  => sp(isp)%particle(sp(isp)%np + 1)                               ! next particle
  call addone(pa, ix, iy, iz)                                           ! Add a particle
  pa%p = thermalvelocity(ix, iy, iz, isp)                               ! give particle a thermal velocity
  if (any(vgam .ne. 0.)) then                                           ! Lorentz trans if bulk motion
    gam_bulk = sqrt(1.0_8 + sum(real(vgam*c%oc,kind=8))**2)
    gam_th   = sqrt(1.0_8 + sum(real(pa%p*c%oc,kind=8))**2)
    pa%p     = gam_bulk*pa%p + gam_th*vgam
  endif

  pa%w = w
  sp(isp)%np = sp(isp)%np + 1
  if (sp(isp)%np == 1 .and. master .and. debugit(dbg_init,1)) then
    print*,'make_particle: the first particle added has isp =', isp
    print 1,'            ix, iy, iz:', ix, iy, iz
    print 1,'   integer coordinates:', pa%q
    print 2,'fractional coordinates:', pa%r
  endif
1 format(1x,a,3i8)
2 format(1x,a,3f8.3)
END SUBROUTINE make_particle

! Make a pair of particles of species isp1, isp2 in cell (ix,iy,iz)
! They have proper bulk velocity vgam and weight w. The temperature
! is sampled according to the relativistic Maxwell-J�ttner
! disitrubtion with temperatures sp(isp1,isp2)%temperature, but can
! also be set completely identical iff identical=.true.
!=======================================================================
SUBROUTINE make_particle_pair(isp1, isp2, ix, iy, iz, vgam, w, identical)
  USE params,             only : mcoord
  USE species,            only : sp, particle
  USE units,              only : c
  implicit none
  integer, intent(in)     :: isp1, isp2, ix, iy, iz
  logical, intent(in)     :: identical
  real,    intent(in)     :: vgam(mcoord), w
  type(particle), pointer :: pa1, pa2
  real                    :: gam_bulk, gam_th
!.......................................................................
  pa1 => sp(isp1)%particle(sp(isp1)%np + 1)                             ! next particle sp 1
  pa2 => sp(isp2)%particle(sp(isp2)%np + 1)                             ! next particle sp 2
  call addone(pa1, ix, iy, iz)                                          ! Add a particle
  pa1%p = thermalvelocity(ix, iy, iz, isp1)                             ! give particle 1 a thermal velocity
  if (identical) then                                                   ! give particle 2 a thermal velocity
    pa2%p = pa1%p
  else
    pa2%p = thermalvelocity(ix, iy, iz, isp2)
  endif
  if (any(vgam .ne. 0.)) then                                           ! Lorentz trans if bulk motion
    gam_bulk = sqrt(1.0_8 + sum(real(vgam*c%oc,kind=8))**2)
    gam_th   = sqrt(1.0_8 + sum(real(pa1%p*c%oc,kind=8))**2)
    pa1%p     = gam_bulk*pa1%p + gam_th*vgam

    gam_th   = sqrt(1.0_8 + sum(real(pa2%p*c%oc,kind=8))**2)
    pa2%p     = gam_bulk*pa2%p + gam_th*vgam
  endif
!-----------------------------------------------------------------------!
! The commented out lines below can be used to verify that the changes
! here are the only ones that cause a change of the validation results.
! This change also introduced a bug (corrected in this version), which
! could have gone undetected exactly because slightly different validation
! results were to be expected in any case!
!-----------------------------------------------------------------------!
 !if (vgam(3) .ne. 0.) then
 !  pa1%p(3) = sqrt(1.0_8 + real(vgam(3)*c%oc,kind=8)**2)*pa1%p(3) + &
 !             sqrt(1.0_8 + sum(real(pa1%p*c%oc,kind=8)**2))*vgam(3)    ! Lorentz trans if in beam
 !  pa2%p(3) = sqrt(1.0_8 + real(vgam(3)*c%oc,kind=8)**2)*pa2%p(3) + &
 !             sqrt(1.0_8 + sum(real(pa2%p*c%oc,kind=8)**2))*vgam(3)    ! Lorentz trans if in beam
 !endif
  pa2%r = pa1%r
  pa2%q = pa1%q
  pa1%w = w
  pa2%w = w
  sp(isp1)%np = sp(isp1)%np + 1
  sp(isp2)%np = sp(isp2)%np + 1
END SUBROUTINE make_particle_pair

! Make a non-relativistic particle of species isp in cell (ix,iy,iz)
! It has a three-velocity v and weight w. The temperature is sampled
! according to the non-relativistic Maxwell disitrubtion with temperature T
!=======================================================================
SUBROUTINE make_nonrelativistic_particle(isp, ix, iy, iz, v, T, w)
  USE params,             only : master, mcoord
  USE species,            only : sp, particle
  USE units,              only : c
  USE debug,              only : debugit, dbg_init
  implicit none
  integer, intent(in)     :: isp, ix, iy, iz
  real,    intent(in)     :: v(mcoord), T, w
  type(particle), pointer :: pa
  real                    :: speed
  integer                 :: i
!.......................................................................
  pa  => sp(isp)%particle(sp(isp)%np + 1)                               ! next particle
  call addone(pa, ix, iy, iz)                                           ! Add a particle
  speed = sqrt(c%kb*T/sp(isp)%mass)                                     ! thermal speed in code units
  if (speed==0.) then                                                   ! zero temperature limit; useful for tests
    pa%p = v
  else
    do i=1,mcoord
      pa%p(i) = gauss_3d(v(i),speed,ix,iy,iz)                           ! get thermal + bulk velocity
    enddo
  endif
  pa%w = w
  sp(isp)%np = sp(isp)%np + 1
  if (sp(isp)%np == 1 .and. master .and. debugit(dbg_init,1)) then
    print*,'make_nonrelativistic_particle: the first particle added has isp =', isp
    print 1,'            ix, iy, iz:', ix, iy, iz
    print 1,'   integer coordinates:', pa%q
    print 2,'fractional coordinates:', pa%r
  endif
1 format(1x,a,3i8)
2 format(1x,a,3f8.3)
END SUBROUTINE make_nonrelativistic_particle

!=======================================================================
SUBROUTINE make_particles_with_temperature_3d(isp, temp)
  USE params,             only : mpi, stdout, mcoord
  USE species,            only : sp, particle, fields
  USE units,              only : c
  USE grid_m,               only : g, drmax
  implicit none
  integer, intent(in)     :: isp
  real, dimension(g%n(1),g%n(2),g%n(3)) :: temp
  type(particle), pointer :: pa 
  integer                 :: i, ix, iy, iz, in    
  integer(kind=2)         :: q(3)
  real                    :: speed, mass, vgam(3), gam1, gam2
  real, external          :: rand
  integer, dimension(3)   :: lb, ub
!.......................................................................
  sp(isp)%np = 0                                                        ! reset particle counter
  mass = sp(isp)%mass
  lb = merge(g%lb-1,g%lb,mpi%lb)
  ub = merge(g%ub+2,g%ub,mpi%ub)
  drmax = 0.
  do iz=lb(3),ub(3)-1
  do iy=lb(2),ub(2)-1
  do ix=lb(1),ub(1)-1
   q = (/ix, iy, iz/) + mpi%offset
   do in=1,sp(isp)%ntarget 
    pa  => sp(isp)%particle(sp(isp)%np + 1)                             ! next particle
    pa%r(1) = rand(ix,iy,iz)
    pa%r(2) = rand(ix,iy,iz)
    pa%r(3) = rand(ix,iy,iz)
    pa%q    = q 

    speed = sqrt(c%kb*temp(ix,iy,iz)/mass)                              ! thermal velocity in code units
    do i=1,mcoord
      pa%p(i) = gauss_3d(0.,speed,ix,iy,iz)                             ! get thermal v*gamma
    enddo
    vgam = fields(ix,iy,iz,isp)%v
    gam1 = sqrt(1. + sum(vgam**2)*c%oc2)
    gam2 = sqrt(1. + sum(pa%p**2)*c%oc2)
    pa%p = gam1*pa%p + gam2*vgam                                        ! Lorentz transformation
    drmax = max(drmax,maxval(pa%p*g%ods))                               ! estimate only

    pa%w = fields(ix,iy,iz,isp)%d / sp(isp)%ntarget                     ! weight = particle density per unit volume per pseudo-particle
    sp(isp)%np = sp(isp)%np + 1                                         ! increment counter
   end do
  end do
  end do
  end do
END SUBROUTINE make_particles_with_temperature_3d

!=======================================================================
SUBROUTINE make_particle_pairs_with_temperature_3d(isp1, isp2, temp)
  USE params,             only : mpi, stdout, mcoord
  USE species,            only : sp, particle, fields
  USE units,              only : c
  USE grid_m,               only : g
  implicit none
  integer, intent(in)     :: isp1, isp2
  real, dimension(g%n(1),g%n(2),g%n(3)) :: temp
  type(particle), pointer :: pa1, pa2 
  integer                 :: i, ix, iy, iz, in, ntarget
  integer(kind=2)         :: q(3)
  real                    :: speed, mass1, mass2, vgam(3), gam1, gam2
  real, external          :: rand
  integer, dimension(3)   :: lb, ub
!.......................................................................
  sp(isp1)%np = 0                                                       ! reset particle counter
  sp(isp2)%np = 0
  mass1 = sp(isp1)%mass
  mass2 = sp(isp2)%mass
  ntarget = sp(isp1)%ntarget

  if (ntarget .ne. sp(isp2)%ntarget) &
    call error('make_particle_pairs_with_temperature','ntarget=npc for isp1 and isp2 has to be equal')

  lb = merge(g%lb-1,g%lb,mpi%lb)
  ub = merge(g%ub+2,g%ub,mpi%ub)
  do iz=lb(3),ub(3)-1
  do iy=lb(2),ub(2)-1
  do ix=lb(1),ub(1)-1
   q = (/ix, iy, iz/) + mpi%offset
   do in=1,ntarget 
    pa1  => sp(isp1)%particle(sp(isp1)%np + 1)                          ! next particle
    pa2  => sp(isp2)%particle(sp(isp2)%np + 1)                          ! next particle
    pa1%r(1) = rand(ix,iy,iz)
    pa1%r(2) = rand(ix,iy,iz)
    pa1%r(3) = rand(ix,iy,iz)
    pa2%r    = pa1%r
    pa1%q    = q
    pa2%q    = q

    speed = sqrt(c%kb*temp(ix,iy,iz)/mass1)                             ! thermal velocity in code units
    do i=1,mcoord
      pa1%p(i) = gauss_3d(0.,speed,ix,iy,iz)                            ! get thermal v*gamma
    enddo
    vgam = fields(ix,iy,iz,isp1)%v
    gam1 = sqrt(1. + sum(vgam**2)*c%oc2)
    gam2 = sqrt(1. + sum(pa1%p**2)*c%oc2)
    pa1%p = gam1*pa1%p + gam2*vgam                                      ! Lorentz transformation

    speed = sqrt(c%kb*temp(ix,iy,iz)/mass2)                             ! thermal velocity in code units
    do i=1,mcoord
      pa2%p(i) = gauss_3d(0.,speed,ix,iy,iz)                            ! get thermal v*gamma
    enddo
    vgam = fields(ix,iy,iz,isp2)%v
    gam1 = sqrt(1. + sum(vgam**2)*c%oc2)
    gam2 = sqrt(1. + sum(pa2%p**2)*c%oc2)
    pa2%p = gam1*pa2%p + gam2*vgam                                      ! Lorentz transformation

    pa1%w = fields(ix,iy,iz,isp1)%d / ntarget                           ! weight = particle density per unit volume per pseudo-particle
    pa2%w = fields(ix,iy,iz,isp2)%d / ntarget                           ! weight = particle density per unit volume per pseudo-particle

    sp(isp1)%np = sp(isp1)%np + 1                                       ! increment counter
    sp(isp2)%np = sp(isp2)%np + 1                                       ! increment counter
   end do
  end do
  end do
  end do
END SUBROUTINE make_particle_pairs_with_temperature_3d

! Returns in Gauss a normally distributed deviate with av mean and sd variance, 
! using our xyz-based random generator as the source of uniform deviates. 
! Taken from the Fortran 90 version of Numerical recipes
!=======================================================================
FUNCTION gauss_3d (av, sd, ix, iy, iz) result(gauss)
  USE params, only : mid
  implicit none
  real              :: gauss
  real, intent(in)  :: av, sd
  integer,intent(in):: ix,iy,iz
  real              :: rsq,v1,v2
  real, external    :: rand

  do
    v1  = 2.0*rand(ix,iy,iz)-1.0                 ! pick two uniform numbers in the square
    v2  = 2.0*rand(ix,iy,iz)-1.0                 ! extending -1 to +1 in each direction, 
    rsq = v1*v1+v2*v2                            ! see if they are in the unit circle, 
    if (rsq > 0.0 .and. rsq < 1.0) exit
  end do                                         ! otherwise try again. 
  rsq         = sqrt(-2.*alog(rsq)/rsq)          ! Make the Box-Muller transformation to 
  gauss       = av + sd*v1*rsq                   ! get normal deviate
END FUNCTION gauss_3d

!=======================================================================
END MODULE particleoperations
!=======================================================================
