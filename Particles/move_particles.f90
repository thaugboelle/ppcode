! Particles/move_particles.f90 $ Date: 2013-09-02 17:06:49 +0200 $
! vim: nowrap
!=======================================================================!
SUBROUTINE move_particles
  USE species, only: cellvector, nspecies, mid
  USE grid_m, only: nx, ny, nz
  implicit none
  type(cellvector), dimension(:), allocatable:: cell
  integer:: ix, iy, iz
  character(len=mid), save:: id = &
    'Particles/move_particles.f90 $ Date: 2013-09-02 17:06:49 +0200 $'
!.......................................................................!
  call print_id (id)
  call trace_enter ('MOVE_PARTICLES')
  
  allocate (cell(nspecies))
  call create_cell(cell)                                                ! create cell vector
  do iz=1,nz                                                            ! loop over cells
    do iy=1,ny
      do ix=1,nx
        call get_cell (ix,iy,iz,cell)                                   ! get cell data
        call move_particles_in (cell)                                   ! move particles
        call put_cell (ix,iy,iz,cell)                                   ! put cell data
      enddo
    enddo
  enddo
  call kill_cell(cell)                                                  ! kill on each thread
  deallocate (cell)
  call trace_exit ('MOVE_PARTICLES')
END SUBROUTINE move_particles

!=======================================================================!
! Move the particles that are inside the given cell
!=======================================================================!
SUBROUTINE move_particles_in (cell)
  USE params  , only: nsubcycle, dt, grav_acc, periodic, master, &
                     particleupdates, time, stdout, nspectra
  USE grid_m,   only: g, drmax, ex, ey, ez, bx, by, bz
  USE units   , only: c, elm
  USE species , only: cellvector, particle, sp, nspecies, mcoord, mdim
  USE debug   , only: debugit, dbg_init, maxprint

  implicit none

  type(cellvector), dimension(nspecies) :: cell
  type(particle), pointer, dimension(:) :: pav
  type(particle), pointer           :: pa
  real, dimension(3)                :: r,dr,dp,dps,p,v,v0,v1,dvdt,dvsdts
  real, dimension(3)                :: b,bdot
  integer, dimension(3)             :: q,ir
  real   , dimension(3)             :: ods,EE,BB,t,p1
  real                              :: hE,hB,o,odt,t2,s,ut
  real                              :: og,gg,g0,g1,p0,p2,pp,gi,denom
  real                              :: mass,w,csub,ct,cts,dts,odts,ts
  integer                           :: i,isp,isub,nsubvar,tnp,np
  integer(kind=8)                   :: ip
  integer, save                     :: nprint = 0
  real, parameter                   :: sqrt2 = sqrt(2.)
  real, allocatable, dimension(:,:) :: p_r, p_q, p_p, p_d, p_EE, p_BB
  real, allocatable, dimension(:)   :: p_e
!.......................................................................!
  ods = g%ods
  drmax = 0.
  do isp=1,nspecies                                                     ! loop over species
   if (cell(isp)%np == 0) cycle                                         ! short cut
   np = cell(isp)%np
   allocate (p_EE(np,3), p_BB(np,3))
   if (sp(isp)%charge /= 0.0) then
     odt = 1./dt                                                        ! one over dt
     hE = 0.5*dt*sp(isp)%charge/sp(isp)%mass                            ! perfectly time centered
     hB = hE*elm%kf                                                     ! B prefactor
                                               call timer('scatter','x')
     do ip=1,np                                                         ! loop over parts in cell
       pa => cell(isp)%particle(ip)                                     ! particle data
       r = pa%r
       if (any((.not. periodic) .and. &                                 ! not periodic case!
               (q < 0 .or. q >= g%n))) then                             ! outside box
           EE = 0.                                                           
           BB = 0.                                                           
       else
         q = (/cell(isp)%ix,cell(isp)%iy,cell(isp)%iz/)
         call scatter_fields (r,q,Ex,Ey,Ez,EE,Bx,By,Bz,BB)                             
       endif
       p_EE(ip,:) = EE
       p_BB(ip,:) = BB
       if (master .and. debugit(dbg_init,1)) then
         if (cell(isp)%ix >= g%lb(1) .and. &
             cell(isp)%iy >= g%lb(2) .and. &
             cell(isp)%iz >= g%lb(3) .and. &
             nprint<maxprint) then
           nprint = nprint + 1
           print*,'move_particles:'
           print 1,'   integer coordinates:', q
           print 2,'fractional coordinates:', r
           print 2,'            Ex, Ey, Ez:', EE
           print 2,'            Bx, By, Bz:', BB
         1 format(1x,a,3i8)
         2 format(1x,a,3f8.3)
         endif
       endif
     end do
                                                  call timer('Vay','x')
     do ip=1,np
       !----------------------------------------------------------------
       ! Lorentz Force - Vay pusher.
       !----------------------------------------------------------------
       pa => cell(isp)%particle(ip)                                     ! particle data
       r = pa%r
       q = pa%q
       p = pa%p                                                         ! scratch vel-array
       o = 1./sqrt(1.+(p(1)*p(1)+p(2)*p(2)+p(3)*p(3))*c%oc2) 
       t = hB * p_BB(ip,:)
       t2 = t(1)*t(1)+t(2)*t(2)+t(3)*t(3)
       p1(1) = p(1) + 2.*he*p_EE(ip,1) + o*(p(2)*t(3)-p(3)*t(2))   
       p1(2) = p(2) + 2.*he*p_EE(ip,2) + o*(p(3)*t(1)-p(1)*t(3))   
       p1(3) = p(3) + 2.*he*p_EE(ip,3) + o*(p(1)*t(2)-p(2)*t(1))   
       s = 1. + (p1(1)*p1(1)+p1(2)*p1(2)+p1(3)*p1(3))*c%oc2 - t2
       pp = (p1(1)*t(1) + p1(2)*t(2) + p1(3)*t(3))*c%oc
       o = sqrt2 * 1./sqrt(s + sqrt(s*s + 4.*(t2 + pp*pp))) 
       t = t*o
       s = 1./(1 + t2*o*o)
       ut = p1(1)*t(1) + p1(2)*t(2) + p1(3)*t(3)
       p(1) = s * (p1(1) + ut*t(1) + p1(2)*t(3) - p1(3)*t(2))
       p(2) = s * (p1(2) + ut*t(2) + p1(3)*t(1) - p1(1)*t(3))
       p(3) = s * (p1(3) + ut*t(3) + p1(1)*t(2) - p1(2)*t(1))

       !----------------------------------------------------------------- 
       ! store total momentum change for particle
       !----------------------------------------------------------------
       cell(isp)%dp(:,ip) = p - pa%p                                      
     end do                                                               
   end if
   if (sp(isp)%mass == 0.) then                                         ! No mass???
     do ip=1,cell(isp)%np                                               ! loop over part in cell
       !----------------------------------------------------------------
       ! Massless particles
       !----------------------------------------------------------------
       pa   => cell(isp)%particle(ip)                                   ! shortcut to part data
       p    = pa%p + cell(isp)%dp(:,ip)                                 ! scratch momentum array
       pa%e = sqrt(sum(p**2)*c%oc2)                                     ! photon dimensionless energy
       v    = p/pa%e                                                    ! velocity
       pa%e = pa%e*c%mc                                                 ! photon energy
       dr   = v(1:mdim)*dt*ods                                          ! position upd in normalised units
       drmax= max(drmax,maxval(dr))
       r    = pa%r + dr                                                 ! position upd in normalised units
       q    = floor(r)                                                  ! cell changes
       pa%q = pa%q + q                                                  ! integer cell coord update
       pa%r = r - q                                                     ! rescale to [0:1)
       pa%p = p                                                         ! upd momentum
     end do
   else
                                                  call timer('mass','x')
     mass = sp(isp)%mass                                                ! mass
     pav => cell(isp)%particle                                          ! for convenience
     !DEC VECTOR ALWAYS
     do ip=1,cell(isp)%np                                               ! loop over part in cell
       !---------------------------------------------------------------- 
       ! Particles with mass
       !----------------------------------------------------------------
       pav(ip)%p(1) = pav(ip)%p(1) + cell(isp)%dp(1,ip)                        
       pav(ip)%p(2) = pav(ip)%p(2) + cell(isp)%dp(2,ip)                 ! momentum upd
       pav(ip)%p(3) = pav(ip)%p(3) + cell(isp)%dp(3,ip)                 ! momentum upd
       p2= pav(ip)%p(1)*pav(ip)%p(1)+ &
           pav(ip)%p(2)*pav(ip)%p(2)+ &
           pav(ip)%p(3)*pav(ip)%p(3)                                    ! momentum squared
       og = 1./sqrt(1.+p2*c%oc2)                                        ! gamma
       pav(ip)%e = mass*p2*og/(1.+og)                                   ! kinetic energy
       og=og*dt
       dr(1) = pav(ip)%p(1)*og*ods(1)
       dr(2) = pav(ip)%p(2)*og*ods(2)
       dr(3) = pav(ip)%p(3)*og*ods(3)
       drmax = max(drmax,dr(1),dr(2),dr(3))
       r(1) = pav(ip)%r(1) + dr(1)
       r(2) = pav(ip)%r(2) + dr(2)
       r(3) = pav(ip)%r(3) + dr(3)
       q(1) = floor(r(1))
       q(2) = floor(r(2))
       q(3) = floor(r(3))
       pav(ip)%q(1) = pav(ip)%q(1) + q(1)
       pav(ip)%q(2) = pav(ip)%q(2) + q(2)
       pav(ip)%q(3) = pav(ip)%q(3) + q(3)
       pav(ip)%r(1) = r(1) - q(1)
       pav(ip)%r(2) = r(2) - q(2)
       pav(ip)%r(3) = r(3) - q(3)
     end do
   endif
   deallocate (p_EE, p_BB)
   particleupdates = particleupdates + cell(isp)%np
  end do
END SUBROUTINE
