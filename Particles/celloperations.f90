! \file celloperations.f90
! \version Particles/celloperations.f90 $Id$
! vim: nowrap
!=======================================================================
SUBROUTINE create_cell (cell)                                           ! Allocate a cell
  USE params,  only: mcoord, nspecies, do_weightchange, mid
  USE species, only: particle, cellvector, sp,do_dead
  implicit none
  integer                               :: isp, mp(nspecies), mptotal, ip1, ip2
  type(cellvector), dimension(nspecies) :: cell
  character(len=mid), save :: id = &
    'Particles/celloperations.f90 $Id$'
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
! If particles can change weight it is essential to have a buffer, where
! extra particles are copied into, on the other hand, if they do not,
! no particles are created, and we can just use a pointer directly to the
! particle array. The momentum change still needs a seperate array though
!-----------------------------------------------------------------------
  if (do_weightchange) then                                             ! Can parts change weight?
    mp      = sp%nmaxcell + maxval(sp%nmax)*4                           ! reasonable number of
    mptotal = sum(mp)                                                   ! total nr of particles
    do isp=1,nspecies
      if (mp(isp) > 0) allocate(cell(isp)%particle(mp(isp)))
    enddo
  else
    mp      = sp%nmaxcell                                               ! max parts needed
    mptotal = sum(mp)
  endif
!-----------------------------------------------------------------------
  cell%mp = mp                                                          ! max parts for any species
  do isp=1,nspecies
    if (mp(isp) > 0) allocate(cell(isp)%dp(mcoord,mp(isp)))
  enddo
  cell%np = cell%mp                                                     ! Wipe all parts initially
END SUBROUTINE create_cell

!=======================================================================
SUBROUTINE kill_cell (cell)                                             ! deallocate a cell
  USE params,  only: do_dead, do_weightchange, nspecies
  USE species, only: cellvector
  implicit none
  integer :: isp
  type(cellvector), dimension(nspecies) :: cell

  do isp=1,nspecies
    if (cell(isp)%mp > 0) then
      deallocate(cell(isp)%dp)                                          ! dealloce moment change
      if (do_weightchange) deallocate(cell(isp)%particle)               !          particles
    end if
    Nullify(cell(isp)%dp)                                               ! Remove reference
    Nullify(cell(isp)%particle)                                         ! Remove reference
  enddo
END SUBROUTINE kill_cell

!=======================================================================
SUBROUTINE wipe_cell (cell)                                             ! Initialize a cell
  USE species, only : cellvector, particle, do_weightchange, nspecies
  implicit none
  integer             :: ip, isp
  type(particle),  pointer, dimension(:)        :: pa
  type(cellvector),         dimension(nspecies) :: cell

  ! Wipe the cell clean, before doing next iteration.
  ! We only need to wipe up to cell%np, since the rest hasn't been touched
  do isp=1,nspecies
    if (do_weightchange) then
      pa => cell(isp)%particle
      do ip=1,cell(isp)%np
        pa(ip)%r = 0.
        pa(ip)%p = 0.
        pa(ip)%e = 0.
        pa(ip)%w = 0.
        pa(ip)%q = 0_2
        pa(ip)%bits = 0_2
        pa(ip)%i = 0_8
        cell(isp)%dp(:,ip) = 0.
      enddo
    else
      do ip=1,cell(isp)%np
        cell(isp)%dp(:,ip) = 0.
      enddo
    endif
    cell(isp)%np = 0
  enddo
END SUBROUTINE wipe_cell

!=======================================================================
SUBROUTINE get_cell (ix, iy, iz, cell)                                  ! Copy data to cell
  USE params,  only: do_dead, do_weightchange, nspecies
  USE species, only: cellvector, particle, sp, fields
  implicit none
  integer, intent(in) :: ix, iy, iz
  integer             :: ip, isp, ipmin, ipmax
  type(particle),   dimension(:), pointer :: pa, pac
  type(cellvector), dimension(nspecies)   :: cell
  
  call wipe_cell(cell)                                                  ! wipe the cell
  do isp = 1, nspecies                                                  ! species loop
    ipmax = fields(ix,iy,iz,isp)%i                                      ! last index
    ipmin = ipmax - fields(ix,iy,iz,isp)%n + 1                          ! first index
    cell(isp)%np = fields(ix,iy,iz,isp)%n                               ! nr of particles
    cell(isp)%ix = ix
    cell(isp)%iy = iy
    cell(isp)%iz = iz
    cell(isp)%inp= cell(isp)%np                                         ! nr of particles
    if (cell(isp)%np .gt. 0) then                                       ! Any particles?
      if (do_weightchange) then                                         ! copy particles
        pa  =>   sp(isp)%particle(ipmin:ipmax)                          ! for convenience
        pac => cell(isp)%particle(1:cell(isp)%np)
        do ip=1,cell(isp)%np
          pac(ip)%r = pa(ip)%r
          pac(ip)%p = pa(ip)%p
          pac(ip)%e = pa(ip)%e
          pac(ip)%w = pa(ip)%w
          pac(ip)%q = pa(ip)%q
          pac(ip)%bits = pa(ip)%bits
          pac(ip)%i = pa(ip)%i
        enddo
      else                                                              ! point to the particles
        cell(isp)%particle => sp(isp)%particle(ipmin:ipmax)
      end if
    endif
  enddo
END SUBROUTINE get_cell

!=======================================================================
SUBROUTINE put_cell (ix, iy, iz, cell)                                  ! Copy data from cell
  USE species, only : cellvector, particle, sp, fields, do_weightchange, nspecies
  implicit none
  integer, intent(in) :: ix,iy,iz
  integer             :: np, ip, IpMin, IpMax, isp
  type(particle),   dimension(:), pointer :: pa,pac
  type(cellvector), dimension(nspecies)   :: cell
  
  if (.not. do_weightchange) then                                       ! Nothing to put back
    do isp = 1, nspecies                                                ! Loop over species
      ipmax = fields(ix,iy,iz,isp)%i                                    ! Last particles index
      nullify(cell(isp)%particle)                                       ! Remove pointers
    enddo
    return                                                              ! return to integrate
  endif
  do isp = 1, nspecies                                                  ! Loop over species
    ipmax = fields(ix,iy,iz,isp)%i                                      ! Last particles index
    ipmin = ipmax - fields(ix,iy,iz,isp)%n + 1                          ! first particle in cell
    np = fields(ix,iy,iz,isp)%n                                         ! number of particles
    pa  => sp(isp)%particle(ipmin:ipmax)                                ! for convenience
    pac => cell(isp)%particle                                           ! for convenience
    do ip=1,np                                                          ! fill local slot
      pa(ip)%r = pac(ip)%r                                              ! copying
      pa(ip)%p = pac(ip)%p                                              ! copying
      pa(ip)%e = pac(ip)%e                                              ! copying
      pa(ip)%w = pac(ip)%w                                              ! copying
      pa(ip)%q = pac(ip)%q                                              ! copying
      pa(ip)%bits = pac(ip)%bits                                        ! copying
      pa(ip)%i = pac(ip)%i                                              ! copying
    enddo
    !-------------------------------------------------------------------
    ! FIXME this comment should be about why the next loop is "convenient"
    !-------------------------------------------------------------------
    pa => sp(isp)%particle(sp(isp)%np - np + 1:sp(isp)%mp)              ! for convenience
    do ip=np+1, cell(isp)%np                                            ! Append to end of buffer
      pa(ip)%r = pac(ip)%r                                              ! copying
      pa(ip)%p = pac(ip)%p                                              ! copying
      pa(ip)%e = pac(ip)%e                                              ! copying
      pa(ip)%w = pac(ip)%w                                              ! copying
      pa(ip)%q = pac(ip)%q                                              ! copying
      pa(ip)%bits = pac(ip)%bits                                        ! copying
      !pa(ip)%i =                                                       ! Done in RemoveDead!
    enddo
    sp(isp)%np = sp(isp)%np + max(0, cell(isp)%np-np)                   ! Upd total nr of particles
  enddo
END SUBROUTINE put_cell
