! Particles/renormalize.f90 $Id$
! vim: nowrap
!=======================================================================
MODULE renorm_mod
  real:: wmin=999., wmax=0.0
END MODULE

!=======================================================================
SUBROUTINE simple_renormalize_all
! 
! To make simpe renormalization available in combination with any 
! integrator, especially including the lean ones, do the renormalization
! separately, and not inside the integrator
!
  USE params
  USE grid_m
  USE species
  USE species_mod, only: verbose
  implicit none

  type(cellvector), dimension(nspecies)   :: cell                       ! stack allocation
  type(particle),   dimension(:), pointer :: pa
  integer :: ix, iy, iz
!-----------------------------------------------------------------------
  if (.not.do_simple_renormalize .and. verbose==0) return               ! if verbose>0 do statistics, at least
                                         call timer('simple_renormalize','start')
 
  call create_cell(cell)                                                ! Create cell vector
  do iz=1,nz                                                            ! loop over cells
    if ((dbg .gt. 4).and.(stdout.ge.0)) &
      write(stdout,*) "Entering slice ",iz                              ! not for production run :p
    cell%iz = iz
    do iy=1,ny                                                          ! loop over cells
      cell%iy = iy
      do ix=1,nx                                                        ! loop over cells
        if (any(fields(ix,iy,iz,:)%n > 0)) then                         ! avoid if empty cell
          call get_cell(ix,iy,iz,cell)                                  ! get cell info
          call simple_renormalize(cell)                                 ! Simple renormalize
          call removedead(cell)                                         ! Rem dead part again
          call put_cell(ix,iy,iz,cell)                                  ! put cell info
        endif
      enddo
    enddo
  enddo
  call kill_cell(cell)                                                   ! Kill cell vector

END SUBROUTINE simple_renormalize_all

!=======================================================================
SUBROUTINE simple_renormalize (cell)                                    ! Very simple renormalization of particle nr in cell
  USE params,  only : nspecies, mdim, do_simple_renormalize, mpi, rank
  USE species, only : cellvector, particle, sp, n_tomerge, n_merged, n_split, &
                      n_npmin, n_npmax, split_overflow, npmin, npmax, n_zero, &
                      fields
  USE species_mod, only: verbose
  USE grid_m, only: g
  USE renorm_mod, only: wmin, wmax
  implicit none
  type(cellvector), dimension(nspecies) :: cell
  type(particle), dimension(:),  pointer:: pa
  real, dimension(:,:), pointer         :: dp
  integer                               :: isp, ip, jp, np, nc, jmin, jmax, ndead
  integer                               :: nf, ns, nd, nx, iz1, iz2
  real                                  :: minw, maxw, oaver, p2, w
  real(kind=8)                          :: waver
  logical                               :: isset_i2_flag, merge_test
  integer, save                         :: nprint=5
!.......................................................................
  if (verbose==0) return

  if (.not. do_simple_renormalize) then
    do isp = 1, nspecies                                                ! Loop over species
      if (cell(isp)%ix < g%lb(1) .or. cell(isp)%ix >= g%ub(1)) cycle    ! ignore cells outside boundaries
      if (cell(isp)%iy < g%lb(2) .or. cell(isp)%iy >= g%ub(2)) cycle    ! ignore cells outside boundaries
      if (cell(isp)%iz < g%lb(3) .or. cell(isp)%iz >= g%ub(3)) cycle    ! ignore cells outside boundaries
      np = cell(isp)%np                                                 ! number of particles in cell
      if (np==0) then
        if (nprint>0) then
          nprint=nprint-1
          iz1=max(1,cell(isp)%iz-1)
          iz2=min(iz1+2,g%n(3))
          print'(1x,a,i6,i3,3i5,1p,3e9.2)', &
            'zero particles in cell at rank,isp,ix,iy,iz,d =', &
            rank,isp,cell(isp)%ix,cell(isp)%iy,cell(isp)%iz, &
            fields(cell(isp)%ix,cell(isp)%iy,iz1:iz2,isp)%d
        endif
        n_zero=n_zero+1
        cycle
      endif
      npmin = min(npmin,np)                                             ! global min
      npmax = max(npmax,np)                                             ! global max
      pa => cell(isp)%particle                                          ! for convenience
      waver = 0.0                                                       ! reset
      do ip=1,np
        waver = waver+pa(ip)%w                                          ! average weight
      enddo
      oaver = np/waver
      do ip=1,np
        wmin = min(wmin,pa(ip)%w*oaver)
        wmax = max(wmax,pa(ip)%w*oaver)       
      enddo
    enddo
    return
  endif

  do isp = 1, nspecies                                                  ! Loop over species
    if (sp(isp)%min_weight <= 0.0) cycle                                ! ignore species wih min_weight <= 0.0
    if (cell(isp)%ix < g%lb(1) .or. cell(isp)%ix >= g%ub(1)) cycle      ! ignore cells outside boundaries
    if (cell(isp)%iy < g%lb(2) .or. cell(isp)%iy >= g%ub(2)) cycle      ! ignore cells outside boundaries
    if (cell(isp)%iz < g%lb(3) .or. cell(isp)%iz >= g%ub(3)) cycle      ! ignore cells outside boundaries

    pa => cell(isp)%particle                                            ! for convenience
    dp => cell(isp)%dp                                                  ! acceleration
    np = cell(isp)%np                                                   ! number of particles in cell
    if (np==0) then
      if (nprint>0) then
        nprint=nprint-1
        iz1=max(1,cell(isp)%iz-1)
        iz2=min(iz1+2,g%n(3))
        print'(1x,a,i6,i3,3i5,1p,3e9.2)', &
          'zero particles in cell at rank,isp,ix,iy,iz,d =', &
          rank,isp,cell(isp)%ix,cell(isp)%iy,cell(isp)%iz, &
          fields(cell(isp)%ix,cell(isp)%iy,iz1:iz2,isp)%d
      endif
      n_zero=n_zero+1
      cycle
    endif

    waver = 0.0                                                         ! reset
    do ip=1,np
      waver = waver+pa(ip)%w                                            ! average weight
    enddo
    if (np > 0) waver = waver/np                                        ! could be replaced by density
    minw = waver*sp(isp)%min_weight                                     ! merger limit
    maxw = waver*sp(isp)%max_weight                                     ! split limit
    !maxw = min(maxw,sp(isp)%ntarget*waver/sp(isp)%nmin)                 ! defaults to twice the average

    jmin = 1                                                            ! index to particle with smallest w > minw
    jmax = 1                                                            ! index to particle with largest w
    wmin = 1e30
    wmax = 0.0

    nc = 0                                                              ! particle create index
    do ip=1,np
      if (pa(ip)%w < minw) then
        call set_i2_flag(pa(ip)%i)                                      ! set merge flag
      else if (pa(ip)%w < wmin) then                                    ! look for smallest non-flagged
        jmin = ip                                                       ! remember smallest non-merged
        wmin = pa(ip)%w                                                 ! and its value
      endif
      if (pa(ip)%w > maxw) then                                         ! try to split particle
        if ((np+nc) < cell(isp)%mp) then                                ! is there room?
          pa(ip)%w = pa(ip)%w*0.5                                       ! split weight
          nc = nc+1                                                     ! create index
          pa(np+nc) = pa(ip)                                            ! split particle
          dp(:,np+nc) = 1e-3*(/pa(ip)%p(2),pa(ip)%p(3),pa(ip)%p(1)/)    ! acceleration, to move apart
          n_split = n_split+1                                           ! count it
        else
          split_overflow = .true.                                       ! split prevented 
          print*,'overflow particle z =',pa(ip)%q(3)+pa(ip)%r(3)        ! be verbose -- this shouldn't happen
        endif
      else if (pa(ip)%w > wmax) then                                    ! look for the largest non-split weight
        jmax = ip                                                       ! remember largest non-merged
        wmax = pa(ip)%w                                                 ! and its value
      end if
    enddo
    if (wmax < 0.999*waver .and. verbose > 1) print'(a,3f8.3)', &
      'wmax < waver should rarely happen',wmax/waver,maxw/waver

    np = np+nc                                                          ! update no of particles

    nf = 0
    if (np > sp(isp)%nmax) then                                         ! too many particles
      call set_i2_flag(pa(jmin)%i)                                      ! flag one extra for merging
      n_npmax = n_npmax+1
      nf = nf+1                                                         ! number of flagged particles
    endif
    ns = 0
    if (np < sp(isp)%nmin) then                                         ! too few particles?
      n_npmin = n_npmin+1                                               ! increment count
      if (pa(jmax)%w > 2.*minw) then                                    ! not too low weight?
        pa(jmax)%w = pa(jmax)%w*0.5                                     ! split top weight particle
        pa(np+1) = pa(jmax)                                             ! duplicate particle info
        p2 = sum(pa(jmax)%p**2)                                         ! squared norm of p
        dp(:,np+1) = merge(0.0*dp(:,jmax), &                            ! zero acceleration if p2==0
          pa(jmax)%p*sum(dp(:,jmax)*pa(jmax)%p)/p2, &                   ! keep component along p if not
          p2==0)
        np = np+1                                                       ! update particle count
        n_split = n_split+1                                             ! increment split count
        ns = ns+1                                                       ! number of split particles
      else if (verbose > 1) then
        print'(a,3f8.4)',' particle split prevented:', &
          pa(jmax)%w/waver,wmax/waver,minw/waver                        ! be verbose -- this shouldn't happen
      endif
    endif

    cell(isp)%np = np                                                   ! update no of particles
    nd = 0
    do ip=1,np
      if (isset_i2_flag(pa(ip)%i)) then                                 ! merge candidate?
        n_tomerge = n_tomerge+1                                         ! count candidates
        do jp=1,np
          if (jp/=ip .and. merge_test(pa(ip),pa(jp))) then              ! OK to merge?
            pa(jp)%w = pa(jp)%w + pa(ip)%w                              ! add weights
            w = pa(ip)%w/pa(jp)%w                                       ! relative weight, to avoid overflow
            pa(jp)%p = (1.0-w)*pa(jp)%p + w*pa(ip)%p                    ! average momenta
            pa(jp)%r = (1.0-w)*pa(jp)%r + w*pa(ip)%r                    ! average positions
            dp(:,jp) = (1.0-w)*dp(:,jp) + w*dp(:,ip)                    ! acceleration
            pa(ip)%w = 0.0                                              ! kill particle
            call clear_i2_flag(pa(jp)%i)                                ! in case it was, but is no longer candidate
            call clear_i2_flag(pa(ip)%i)                                ! in case the slot is reused
            n_merged = n_merged+1                                       ! count mergers
            nd = nd+1                                                   ! count dead particles
            exit                                                        ! search no more
          endif
        enddo
      endif
    enddo

    ip = 1
    nx = 0
    do while (np-nd < sp(isp)%nmin .and. ip <= np)                      ! if more splits needed, take ..
      if (pa(ip)%w > waver) then                                        ! any particle over average
        pa(ip)%w = pa(ip)%w*0.5                                         ! split top weight particle
        pa(np+1) = pa(ip)                                               ! duplicate particle
        p2 = sum(pa(ip)%p**2)                                           ! squared norm of p
        dp(:,np+1) = merge(0.0*dp(:,ip), &                              ! zero acceleration if p2==0
          pa(ip)%p*sum(dp(:,ip)*pa(ip)%p)/p2, &                         ! keep component along p if not
          p2==0)
        np = np+1                                                       ! update particle count
        n_split = n_split+1                                             ! increment split count
        nx = nx+1                                                       ! number of extra splits
      endif
      ip = ip+1                                                         ! next particle
    enddo

    ip = 1
    do while (np-nd < sp(isp)%nmin .and. ip <= np)                      ! if more splits needed, take ..
      if (pa(ip)%w > 2.*minw) then                                      ! any particle that will not be merged
        pa(ip)%w = pa(ip)%w*0.5                                         ! split top weight particle
        pa(np+1) = pa(ip)                                               ! duplicate particle
        p2 = sum(pa(ip)%p**2)                                           ! squared norm of p
        dp(:,np+1) = merge(0.0*dp(:,ip), &                              ! zero acceleration if p2==0
          pa(ip)%p*sum(dp(:,ip)*pa(ip)%p)/p2, &                         ! keep component along p if not
          p2==0)
        np = np+1                                                       ! update particle count
        n_split = n_split+1                                             ! increment split count
        nx = nx+1                                                       ! number of extra splits
      endif
      ip = ip+1                                                         ! next particle
    enddo

    ip = 1
    do while (np-nd < sp(isp)%nmin .and. ip <= np)                      ! if more splits needed, take any particle
      pa(ip)%w = pa(ip)%w*0.5                                           ! split top weight particle
      pa(np+1) = pa(ip)                                                 ! duplicate particle
      p2 = sum(pa(ip)%p**2)                                             ! squared norm of p
      dp(:,np+1) = merge(0.0*dp(:,ip), &                                ! zero acceleration if p2==0
        pa(ip)%p*sum(dp(:,ip)*pa(ip)%p)/p2, &                           ! keep component along p if not
        p2==0)
      np = np+1                                                         ! update particle count
      n_split = n_split+1                                               ! increment split count
      nx = nx+1                                                         ! number of extra splits
      ip = ip+1                                                         ! next particle
    enddo

    npmin = min(npmin,np)
    npmax = max(npmax,np)

    if (verbose > 1 .and. nd > 1 .and. nx > 1) then
      print'(i3,a,2(i3,a))',nx,' extra splits needed while',nd,' particles were merged, out of',nf,' flagged'
    endif

  enddo          
END SUBROUTINE simple_renormalize

!=======================================================================
SUBROUTINE renormalize_stats
  USE params,  only : main_info,master, do_simple_renormalize
  USE species, only : n_tomerge,n_merged,n_split,n_npmin,n_npmax,npmin, &
                      npmax,n_zero,split_overflow
  USE species_mod, only: verbose
  USE renorm_mod, only: wmin, wmax
  implicit none
  integer(kind=8), dimension(7) :: sum
  integer(kind=8)               :: nzero
!.......................................................................
  if (verbose>0) then
    sum = (/n_tomerge,n_merged,n_split,n_npmin,n_npmax,n_zero,merge(1_8,0_8,split_overflow)/)
    call mpi_sum_integer8s (6, sum)
    n_tomerge = sum(1)
    n_merged  = sum(2)
    n_split   = sum(3)
    n_npmin   = sum(4)
    n_npmax   = sum(5)
    nzero     = sum(6)
    split_overflow = sum(7) > 0
    call mpi_min_integer8(npmin)
    call mpi_max_integer8(npmax)
    call mpi_min_real(wmin)
    call mpi_max_real(wmax)
    if (master) then
      if (do_simple_renormalize) then
        write(main_info,'(2i4,1p,6e8.1,l2)') &
          npmin, npmax, real(n_tomerge), real(n_merged), real(n_split), &
          real(n_npmin), real(n_npmax), real(nzero), split_overflow
      else
        write(main_info,'(2i4,1p,3e8.1)') npmin, npmax, wmin, wmax, real(n_zero)
      endif
    endif
  endif
  wmin = 1e9
  wmax = 0.0
  n_tomerge = 0
  n_merged = 0
  n_split = 0
  n_npmin = 0
  n_npmax = 0
  npmin = 999
  npmax = 0
  nzero = 0
  split_overflow = .false.
END SUBROUTINE renormalize_stats

!=======================================================================
LOGICAL FUNCTION merge_test(p1,p2)
!
! p1 is the one with a very low weight, while p2 may or may not have a
! low weight. Take this into account, since an error made when removing
! a low weight particle has a correspondingly smaller effect.
!
  USE species, only: particle, merge_tol, mdim
  type(particle):: p1, p2
  real, dimension(mdim):: p1p, p2p
  real:: p1a, p2a, p3a, pn
!.......................................................................
  merge_test = .false.
  if (p2%w==0.0) return
  p1p = p1%p
  p2p = p2%p
  pn = sum(abs(p1p))+sum(abs(p2p))
  p1p = p1p/pn
  p2p = p2p/pn
  p1a = p1p(1)**2+p1p(2)**2+p1p(3)**2
  p2a = p2p(1)**2+p2p(2)**2+p2p(3)**2
  p3a = (p1p(1)-p2p(1))**2+(p1p(2)-p2p(2))**2+(p1p(3)-p2p(3))**2
  if (p3a*p1%w < merge_tol*(p1a+p2a)*p2%w) merge_test = .true.
END 
