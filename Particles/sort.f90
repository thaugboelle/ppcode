! Particles/sort.f90 $Id$
! vim: nowrap
!-----------------------------------------------------------------------
! This routine sorts the particles belonging to an MPI process into a
! sequence with increasing compressed cell number (ix+nx*(iy+ny*iz)), in
! order to maintain good cache efficiency; i.e. making sure that data
! belonging to neighboring particles remains close in memory. The work
! is parallelized over multiple threads using OpenMP.
!-----------------------------------------------------------------------
SUBROUTINE sort
  USE params,  only : stdout,nspecies,mid,trace,dbg,hl,mdim,mcoord,     &
                      romp,nomp,periodic,rank,mpi,die_in_sort
  USE species, only : sp,fields,particle
  USE grid_m,  only : g
  USE debug,   only : maxprint
  implicit none

  type(particle), pointer,     dimension(:) :: pa,pap
  type(particle)                            :: pas
  integer, dimension(:,:),     allocatable  :: cell
  integer               :: isp, ix, iy, iz, isum, ival, i, ii(mdim),    &
                           idim, out, ilb, iub, nthreads, nworker,      &
                           promp, next_bin, npartition, deadp,          &
                           np, mp, ip, ixyz, deadip, position_error
  real,    dimension(3) :: lb, ub, glb, gub, rr
  character(len=mid)    :: id = "Particles/sort.f90 $Id$"
  integer(kind=2)       :: q(3), gn(3), qq
  type partition_t
    integer :: lb
    integer :: ub
    integer :: kl
    integer :: ku
    integer :: np
  end type
  type(partition_t), allocatable, dimension(:) :: p
!.......................................................................
  if (nspecies == 0) return
  call print_id (id)
  call trace_enter ('SORT')

  !---------------------------------------------------------------------
  ! Block of short hand declarations, used below
  !---------------------------------------------------------------------
  lb = g%rlb
  ub = g%rub
  glb = g%grlb
  gub = g%grub
  gn  = g%gn
  deadip = product(g%n)+10                                              ! something big to signal a particle is dead
 
  sp%nmaxcell = 0                                                       ! Reset max  number ofparts in cell
  np = maxval(sp%np)                                                    ! Max nr of particles
  nthreads = min(nspecies,nomp)
  nworker  = nomp / nthreads
  npartition = 64                                                       ! Nr of partitions for OpenMP quick sort

  !---------------------------------------------------------------------
  ! Nothing to sort ?
  !---------------------------------------------------------------------
  if (np .eq. 0) then
    do isp=1,nspecies                                                   ! Set the indices to zero
    do iz=1,g%n(3)
      fields(:,:,iz,isp)%i=0
      fields(:,:,iz,isp)%n=0
    enddo
    enddo
    call ParticleTotal                                                  ! Upd total nr of parts
    return
  endif
                               call timer('sort','start')

  !---------------------------------------------------------------------
  ! Main loop, sort species by species
  !---------------------------------------------------------------------
  allocate(cell(np,nthreads))                                           ! Allocate sort key array
  out = 0
  do isp=1,nspecies                                                     ! Loop over species
    promp = romp                                                        ! 1-index thread number
    qq = 0                                                              ! Defined in case run is non-periodic
    np = sp(isp)%np                                                     ! Nr of particles
    do iz=1,g%n(3)                                                      !
      fields(:,:,iz,isp)%i=0                                            ! Set the indices to zero
      fields(:,:,iz,isp)%n=0                                            ! zero particles
    enddo
    deadp = 0                                                           ! Nr of dead particles
    if (np .eq. 0) cycle                                                ! Cycle if nothing to do

    mp = sp(isp)%mp
    pa => sp(isp)%particle                                              ! Shorthand
    !------------------------------------------------------------------
    ! calculate number of particles in each cell, and cell number for
    ! each particle, keeping the species sorted
    !------------------------------------------------------------------
    if (all(periodic)) then                                            ! optimize this case
      do ip=1,np
        if (pa(ip)%w > 0) then                                         ! Only count if not dead
          q = mod(pa(ip)%q,gn); q = merge(q,q+gn,q>=0)
          ii = q - mpi%offset
          cell(ip,promp) = ii(1) + g%n(1)*((ii(2)-1)+g%n(2)*(ii(3)-1)) ! compress it
          fields(ii(1),ii(2),ii(3),isp)%n = fields(ii(1),ii(2),ii(3),isp)%n + 1 ! inc nr of parts in cell
        else
          deadp = deadp + 1
          cell(ip,promp) = deadip
        endif
      end do
    else
      do ip=1,np
        if (pa(ip)%w > 0) then                                          ! Only count if not dead
          do idim=1,3   
            if (periodic(idim)) then                                    ! after periodic folding we KNOW it is ours
              qq = mod(pa(ip)%q(idim),gn(idim)); qq = merge(qq,qq+gn(idim),qq>=0)
              ii(idim) = qq - mpi%offset(idim)
              ilb = g%lb(idim)                                          ! address bound
              iub = g%ub(idim)-1                                        ! ditto
            else
              ii(idim) = pa(ip)%q(idim) - mpi%offset(idim)
              ilb = merge(1,        g%lb(idim)  ,mpi%lb(idim))          ! address bound (not boundary)
              iub = merge(g%n(idim),g%ub(idim)-1,mpi%ub(idim))          ! address bound
            endif
            position_error = 0
            if (ii(idim) < ilb) then
              position_error = 1
              out=out+1                                                 ! count outside
            else if (ii(idim) > iub) then
              position_error = 2                                        ! error somewhere; missing boundary cond.?
              out=out+1                                                 ! count outside
            endif
            if (position_error > 0 .and. die_in_sort) then              ! help find bugs in the code
              print'(a,i2,a,i5.5,a,3(1x,i3.3),a,i3,a,i2,a,3i5,a,i10,4i5,a,i24)', &
                'Err=',position_error,' R=',rank,' MPI=',mpi%me,' ISP=',isp,' Dim=',idim,&
                ' ilb,ii,ulb=',ilb,ii(idim),iub,' q,gn,qq=',pa(ip)%q(idim),mod(pa(ip)%q(idim),gn(idim)),&
                qq,gn(idim),mpi%offset(idim),' i=',pa(ip)%i
              call error('sort','position error, and die_in_sort=t')
            endif
            if (out > maxprint .and. die_in_sort) &                     ! give up after maxprint
              call error('sort','more than maxprint position errors')
            ii(idim) = max(ilb, min(iub, ii(idim)))                     ! allow continuation, for debugging
          enddo
          cell(ip,promp) = ii(1) + g%n(1)*((ii(2)-1) + g%n(2)*(ii(3)-1))! compress it
          fields(ii(1),ii(2),ii(3),isp)%n = fields(ii(1),ii(2),ii(3),isp)%n + 1 ! inc nr of parts in cell
        else
          deadp = deadp + 1
          cell(ip,promp) = deadip
        endif
      enddo
    endif

    isum=1
    ival=0

    !-------------------------------------------------------------------
    ! If number of particles is small compute offset in
    ! particle array for each cell and do the quick sort
    !-------------------------------------------------------------------
    if (np < 1000) then
      do iz=1,g%n(3)
      do iy=1,g%n(2)
      do ix=1,g%n(1)
        ival=fields(ix,iy,iz,isp)%n
        sp(isp)%nmaxcell = max(sp(isp)%nmaxcell,ival)
        isum=isum+ival
        fields(ix,iy,iz,isp)%i=isum-1
      enddo
      enddo
      enddo
      call quick_sort(pa,cell(:,promp),np)                              ! Quick sort
    else
      !-----------------------------------------------------------------
      ! We have more than 1000 particles. Do a complicated version, where we find
      ! optimal pivot points for quick sort, while calculating the fields array.
      ! The perform log_2(npartition) manual (OMP parallelized) sweeps of quick sort
      ! partitions, and finally call quick sort on each sub partition in parallel.
      !-----------------------------------------------------------------

      ! Test if we have a massive amount of dead particles
      if ( deadp > (np / 64) ) then
        do ip=1,sp(isp)%np
          do while (cell(ip,promp) == deadip)
            if (ip <= np) then
              pas = pa(np)
              pa(np) = pa(ip)
              pa(ip) = pas
              cell(ip,promp) = cell(np,promp)
              cell(np,promp) = deadip
              np = np - 1
            endif
            if (ip >= np) exit
          enddo
        enddo
      endif

      !-----------------------------------------------------------------
      ! Find optimal partition points for manual quick sort, while constructing the fields array
      !-----------------------------------------------------------------
      allocate(p(npartition))
      p(1)%lb=1                                                         ! lower bound in array position
      p(1)%kl=1                                                         ! lower bound in sort key (cell)
      ip=1                                                              ! counter for partition number
      next_bin = ceiling(real(np)/npartition)
      do iz=1,g%n(3)
      do iy=1,g%n(2)
      do ix=1,g%n(1)
        ival=fields(ix,iy,iz,isp)%n
        sp(isp)%nmaxcell = max(sp(isp)%nmaxcell,ival)
        isum=isum+ival
        fields(ix,iy,iz,isp)%i=isum-1
        if (isum >= next_bin .or. g%n(1)*g%n(2)*g%n(3) - (ix + g%n(1)*((iy-1) + g%n(2)*(iz-1))) <= ip ) then
          p(ip)%ku = ix + g%n(1)*((iy-1) + g%n(2)*(iz-1)) 
          p(ip)%ub = isum-1
          p(ip)%np = p(ip)%ub - p(ip)%lb + 1
          if (ip < npartition) then
            p(ip+1)%kl = ix + g%n(1)*((iy-1) + g%n(2)*(iz-1)) + 1
            p(ip+1)%lb = isum
            ip=ip+1
          endif
          next_bin = ceiling(real(np)/npartition*ip)
        endif 
      enddo
      enddo
      enddo
      p(ip)%ku = deadip
      p(ip)%ub = np
      p(ip)%np = p(ip)%ub - p(ip)%lb + 1

      !-----------------------------------------------------------------
      ! Do the partition by hand for optimal parallelization
      !-----------------------------------------------------------------
      ! 1) Do partition in half-half
      ip=p(1)%lb
      ix=p(64)%ub
      do while (ip <= p(32)%ub)
        do while (cell(ip,promp) <= p(32)%ku); ip=ip+1; enddo 
        if (ip <= p(32)%ub) then
          do while (cell(ix,promp) >= p(33)%kl); ix=ix-1; enddo
          iz = cell(ip,promp)
          cell(ip,promp) = cell(ix,promp)
          cell(ix,promp) = iz
          pas = pa(ip)
          pa(ip) = pa(ix)
          pa(ix) = pas
        endif
      enddo

      ! 2) Half each half partition again in to quarters in parallel
      ip=p(1)%lb; ix=p(32)%ub
      do while (ip <= p(16)%ub)
        do while (cell(ip,promp) <= p(16)%ku); ip=ip+1; enddo 
        if (ip <= p(16)%ub) then
          do while (cell(ix,promp) >= p(17)%kl); ix=ix-1; enddo
          iz = cell(ip,promp)
          cell(ip,promp) = cell(ix,promp)
          cell(ix,promp) = iz
          pas = pa(ip)
          pa(ip) = pa(ix)
          pa(ix) = pas
        endif
      enddo
      ip=p(33)%lb; ix=p(64)%ub
      do while (ip <= p(48)%ub)
        do while (cell(ip,promp) <= p(48)%ku); ip=ip+1; enddo 
        if (ip <= p(48)%ub) then
          do while (cell(ix,promp) >= p(49)%kl); ix=ix-1; enddo
          iz = cell(ip,promp)
          cell(ip,promp) = cell(ix,promp)
          cell(ix,promp) = iz
          pas = pa(ip)
          pa(ip) = pa(ix)
          pa(ix) = pas
        endif
      enddo
      ! 3) Half each quarter partition again in to eigths in parallel
      do iy=1,4
        ip=p(16*iy-15)%lb; ix=p(16*iy)%ub
        do while (ip <= p(16*iy-8)%ub)
          do while (cell(ip,promp) <= p(16*iy-8)%ku); ip=ip+1; enddo 
          if (ip <= p(16*iy-8)%ub) then
            do while (cell(ix,promp) >= p(16*iy-7)%kl); ix=ix-1; enddo
            iz = cell(ip,promp)
            cell(ip,promp) = cell(ix,promp)
            cell(ix,promp) = iz
            pas = pa(ip)
            pa(ip) = pa(ix)
            pa(ix) = pas
          endif
        enddo
      enddo
      ! 4) Half each 8th partition again in to 16th in parallel
      do iy=1,8
        ip=p(8*iy-7)%lb; ix=p(8*iy)%ub
        do while (ip <= p(8*iy-4)%ub)
          do while (cell(ip,promp) <= p(8*iy-4)%ku); ip=ip+1; enddo 
          if (ip <= p(8*iy-4)%ub) then
            do while (cell(ix,promp) >= p(8*iy-3)%kl); ix=ix-1; enddo
            iz = cell(ip,promp)
            cell(ip,promp) = cell(ix,promp)
            cell(ix,promp) = iz
            pas = pa(ip)
            pa(ip) = pa(ix)
            pa(ix) = pas
          endif
        enddo
      enddo
      ! 5) Half each 16th partition again in to 32th in parallel
      do iy=1,16
        ip=p(4*iy-3)%lb; ix=p(4*iy)%ub
        do while (ip <= p(4*iy-2)%ub)
          do while (cell(ip,promp) <= p(4*iy-2)%ku); ip=ip+1; enddo 
          if (ip <= p(4*iy-2)%ub) then
            do while (cell(ix,promp) >= p(4*iy-1)%kl); ix=ix-1; enddo
            iz = cell(ip,promp)
            cell(ip,promp) = cell(ix,promp)
            cell(ix,promp) = iz
            pas = pa(ip)
            pa(ip) = pa(ix)
            pa(ix) = pas
          endif
        enddo
      enddo
      ! 6) Half each 32th partition again in to 64th in parallel
      do iy=1,32
        ip=p(2*iy-1)%lb; ix=p(2*iy)%ub
        do while (ip <= p(2*iy-1)%ub)
          do while (cell(ip,promp) <= p(2*iy-1)%ku); ip=ip+1; enddo 
          if (ip <= p(2*iy-1)%ub) then
            do while (cell(ix,promp) >= p(2*iy)%kl); ix=ix-1; enddo
            iz = cell(ip,promp)
            cell(ip,promp) = cell(ix,promp)
            cell(ix,promp) = iz
            pas = pa(ip)
            pa(ip) = pa(ix)
            pa(ix) = pas
          endif
        enddo
      enddo
      ! 7) send off the rest to the generic quick sort routine using up to npartition threads 
      do ix=1,npartition
        pap => sp(isp)%particle(p(ix)%lb:p(ix)%ub) 
        call quick_sort(pap,cell(p(ix)%lb:p(ix)%ub,promp),p(ix)%np)
      enddo
      deallocate(p)
    endif

    sp(isp)%np = sp(isp)%np - deadp                                     ! Change particle count


    !-------------------------------------------------------------------
    ! This generally signals an error somewhere else. Most probably in the MPI
    ! Send particles or in the boundary routines
    !-------------------------------------------------------------------
    if (fields(g%n(1),g%n(2),g%n(3),isp)%i .ne. sp(isp)%np) then
      print *, 'Error in sort'
      print *, 'Rank      :', rank
      print *, 'Species   :', isp
      print *, 'field     :', fields(g%n(1),g%n(2),g%n(3),isp)%i
      print *, 'np        :', sp(isp)%np
      call error('sort','sort has not assigned correct indices to fields%i array')
    endif
  enddo                                                                 ! next species

  deallocate(cell)                                                      ! release sort key

  call ParticleTotal                                                    ! Upd total nr of parts

  if (out > 0) call warning('sort','There are particles on the boundary;'// &
                                   ' Missing boundary condition?')
  call trace_exit ('SORT')
CONTAINS
!-----------------------------------------------------------------------
SUBROUTINE quick_sort(pa,a,n)
  !     NON-RECURSIVE STACK VERSION OF QUICKSORT FROM N.WIRTH'S PASCAL
  !     BOOK, 'ALGORITHMS + DATA STRUCTURES = PROGRAMS'.
  implicit none
  
  integer, intent(in)    :: n
  type(particle), pointer, dimension(:) :: pa
  integer,                 dimension(:) :: a
  !     Local Variables. Stack size is enough for at least 2^28 elements
  integer                :: i, j, k, l, r, s, stackl(28), stackr(28)
  integer                :: w, x, y
  type(particle)         :: ww
  
  s = 1
  stackl(1) = 1
  stackr(1) = n
  
  ! KEEP TAKING THE TOP REQUEST FROM THE STACK UNTIL S = 0.
  DO WHILE (s .NE. 0)
    l = stackl(s)
    r = stackr(s)
    s = s - 1
    
    ! KEEP SPLITTING A(L), ... , A(R) UNTIL L >= R.
    DO WHILE (l < r)
      i = l
      j = r
      k = (l+r) / 2
      x = a(k)
      
      ! FIRST HALF PART OF ITERATION BELOW
      DO WHILE (a(i) < x)
        i = i + 1
      END DO
      y = a(i)
      
      DO WHILE (x < a(j))
        j = j - 1
      END DO
      
      ! REPEAT UNTIL I > J.
      DO WHILE (i <= j)
        ! SWAP POSITIONS i & j IF KEY NOT THE SAME
        IF (a(j) .NE. y) THEN 
          w = a(i)
          ww = pa(i)
          a(i) = a(j)
          pa(i) = pa(j)
          a(j) = w
          pa(j) = ww
        END IF
        i = min(i + 1,n)
        j = max(j - 1,1)

        DO WHILE (a(i) < x)
          i = i + 1
        END DO
        y = a(i)
      
        DO WHILE (x < a(j))
          j = j - 1
        END DO
      END DO
      
      ! UPDATE STACK AND BRACKET
      IF (j-l >= r-i) THEN
        IF (l < j) THEN
          s = s + 1
          stackl(s) = l
          stackr(s) = j
        END IF
        l = i
      ELSE
        IF (i < r) THEN
          s = s + 1
          stackl(s) = i
          stackr(s) = r
        END IF
        r = j
      END IF
    
    END DO
  END DO

END SUBROUTINE quick_sort
!-----------------------------------------------------------------------
END SUBROUTINE sort
!-----------------------------------------------------------------------
