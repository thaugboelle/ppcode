! Particles/load_balance.f90 $Id$
! vim: nowrap
!=======================================================================
SUBROUTINE load_balance_one_species(isp,too_many_particles,particles_needed,name)
  USE params,  only : buffer_change, nodes, rank, mfile
  USE species, only : sp
  implicit none
  integer, intent(in)                :: isp                             ! species to check
  logical, intent(in)                :: too_many_particles              ! problems on this node?
  integer, intent(in)                :: particles_needed                ! 
  character(len=*), intent(in)       :: name
  integer                            :: irank, newmp
  logical, allocatable, dimension(:) :: too_many_particles_global
  logical                            :: change_global
  character(len=mfile)               :: message
  !---------------------------------------------------------------------
  ! See if there are any nodes that want to increase the particle number
  if (nodes .eq. 1 .and. .not. too_many_particles) return

  call mpi_allreduce_logical_isp(too_many_particles, change_global,isp) ! check first if any thread has to do anything
  if (.not. change_global) return

  allocate(too_many_particles_global(nodes))
  call scatter_load(too_many_particles,too_many_particles_global,isp)   ! check other nodes
  if (.not. any(too_many_particles_global)) return                      ! no problematic nodes -> return

  do irank=0,nodes-1                                                    ! loop of nodes
    if (.not. too_many_particles_global(irank+1)) cycle                 ! this node is A-OK, cycle

    if (irank .ne. rank) then                                           ! we do not have Rank 'rank'
      call highest_particle_id_one_species(isp)
      cycle
    endif
    newmp = (buffer_change(2) + 0.01)*(sp(isp)%mp + particles_needed)   ! new nr of parts needed not to trigger LB
    newmp = max(newmp, sp(isp)%mp+1000)                                 ! inc with min 1000 parts
    call change_particle_number(isp, newmp, name)                       ! increase buffer for species 
    if (newmp .ne. sp(isp)%mp) then                                     ! increase failed, try again
      write(message,'(a,i9,a,i9)') 'Tried to add newmp=',newmp,' from mp=',sp(isp)%mp
      call warning('load_balance_one_species',message)
      newmp = 1.01*(sp(isp)%mp + particles_needed)                      ! min new nr of parts needed
      call change_particle_number(isp, newmp, name)                     ! increase buffer for species 
      if (newmp .ne. sp(isp)%mp) then                                   ! increase failed, now stop playing nice
        call warning('noroutine','We could (maybe) avoid this error by decreasing other species, but it is not worth human time')
        call error('load_balance_one_species','We tried to increase #parts with needed + 1% but failed miserably.')
      endif
    endif
    call highest_particle_id_one_species(isp)                           ! send new highest id for all species
  enddo
END SUBROUTINE load_balance_one_species

!=======================================================================
SUBROUTINE load_balance_particles
  USE params,  only : load_balance_range, buffer_change, nodes, mfile, &
                      rank, nspecies, master, mid
  USE species, only : particle_length, sp
  implicit none
  integer, dimension(1)              :: ispinc, ispdec
  integer                            :: toincrease, todecrease, irank
  real,    allocatable, dimension(:) :: mem_required
  logical, allocatable, dimension(:) :: increase, decrease, global_load_balance
  integer, allocatable, dimension(:) :: newmp
  character(len=mfile)               :: message,LB='load_balance_particles'
  integer                            :: verbose = 0
  logical                            :: change_global
  character(len=mid), save :: id = &
    'Particles/load_balance.f90 $Id$'
!.......................................................................
  call print_id (id)
  allocate(increase(nspecies), decrease(nspecies), newmp(nspecies))
  !---------------------------------------------------------------------
  ! See if there are any potential candidates
  decrease = sp%np .lt. load_balance_range(1)*sp%mp                     ! decrease buffer ?
  increase = sp%np .gt. load_balance_range(2)*sp%mp                     ! increase buffer ?
  newmp    = merge(int(buffer_change(2)*sp%mp),sp%mp,increase)          ! how much to increase (def +20% see params)
  newmp    = merge(int(buffer_change(1)*sp%mp),newmp,decrease)          ! how much to decrease (def -50% see params)
  toincrease = sum(merge(1,0,increase))                                 ! how many to increase
  todecrease = sum(merge(1,0,decrease))                                 ! how many to decrease

  call mpi_allreduce_logical(toincrease > 0 .or. todecrease > 0, change_global) ! check first if any thread has to do anything
  if (.not. change_global) then
    deallocate(increase, decrease, newmp)
    return
  endif
  allocate(mem_required(nspecies), global_load_balance(nodes))
                               if (verbose .gt. 1) write (*,'(a,i3,a,i2,a,i2)') &
                                              'R=',rank,' D=',todecrease," I=",toincrease
  call scatter_load(toincrease .gt. 0, global_load_balance,1)           ! which nodes need to LB?
                               if (verbose .gt. 1) write (*,*) &
                                              'R=',rank,' I=', global_load_balance
                               if (verbose .eq. 1 .and. master) write (*,*) &
                                              'R=',rank,' I=', global_load_balance
  do irank=0,nodes-1
    if (.not. global_load_balance(irank+1)) cycle                       ! cycle if no reload bal.
    if (irank .ne. rank) then
      call highest_particle_id                                          ! get highest id for all species
      cycle
    endif
    !              bytes pr particle    * nr of particles  (MB)
    mem_required = (particle_length*4.) * (sp%mp+newmp) / (1024.**2)

    !-------------------------------------------------------------------
    ! Load balance using the following algorithm:
    !
    ! 1) find the species that takes most mem to increase, and do it
    ! 2) if 1) fail: if all species need to be increased give up.
    ! 3) if we pass 2), but 1) fail: find a species ready for decreasing that releases most mem
    ! 4) if there are no species ready for decrease: Find in general the species that releases most
    ! 5) return to 1)
    !-------------------------------------------------------------------
    do while (toincrease .gt. 0)
      ispinc = maxloc(mem_required,mask=increase)                       ! do biggest increase first
      call change_particle_number(ispinc(1), newmp(ispinc(1)),LB)       ! increase buffer
      if (newmp(ispinc(1)) .ne. sp(ispinc(1))%mp) then                  ! increase failed
        if (toincrease .eq. nspecies) &                                 ! major bummer
          call error('load_balance_particles', &
                     'All particles need to be increased and we are out of mem. Stopping now.')
        if (todecrease .gt. 0) then                                     ! pheww, lets decrease
          ispdec = maxloc(sp%mp-newmp,mask=decrease)                    ! best species to decrease
          call change_particle_number(ispdec(1), newmp(ispdec(1)),LB)
          todecrease = todecrease - 1
          decrease(ispdec(1)) = .false.
          newmp(ispdec(1))    = sp(ispdec(1))%mp
        else                                                            ! worse; nobody to decrease
          ispdec = maxloc(sp%mp-sp%np,mask=(.not. increase))            ! best option
          call change_particle_number(ispdec(1), int(0.5*(sp(ispdec(1))%mp+sp(ispdec(1))%np)),LB)
          newmp(ispdec(1)) = sp(ispdec(1))%mp
        endif
      else                                                              ! increase of buffer ok
        toincrease = toincrease - 1
        increase(ispinc(1)) = .false.
        newmp(ispinc(1)) = sp(ispinc(1))%mp
      endif
    enddo
    call highest_particle_id                                            ! send new highest id for all species
  enddo
END SUBROUTINE load_balance_particles

!=======================================================================
SUBROUTINE change_particle_number(isp,newmp,name)
  USE species
  USE grid_m,   only : g
  USE units,  only : c
  implicit none
  integer, intent(in)            :: isp                                 ! species
  integer, intent(in)            :: newmp                               ! new max nr of particles
  character(len=*), intent(in)   :: name
  integer                        :: mp, np, diffmp, err_alloc1, err_alloc2
  character(len=mfile)           :: message, pfile
  logical                        :: do_incore
  integer                        :: jsp
  integer(kind=8)                :: ip_off, ip, jp
  type(particle), pointer,     dimension(:)   :: pa, pa2
  type(particle), allocatable, dimension(:)   :: test_mem               ! scratch particles
  real,           allocatable, dimension(:,:) :: r, p
  real,           allocatable, dimension(:)   :: w
  integer(kind=8),allocatable, dimension(:)   :: i
  integer(kind=2),allocatable, dimension(:,:) :: q
  integer                        :: verbose = 0
  !
  mp = sp(isp)%mp
  np = sp(isp)%np
  diffmp = newmp - mp
  if (newmp > sp(isp)%lp) then
    write(stdall,*) 'Buddy, you are requesting to have more particles on thread,'// &
                    ' than the theoretical max. That is seriously bad for your memory'
    write(stdall,*) 'Species nr   ', isp
    write(stdall,*) 'Current max  ', mp
    write(stdall,*) 'Requested nr ', newmp
    write(stdall,*) 'Theory max   ', sp(isp)%lp
    write(stdall,*) 'When this line was written sum(ln)=256 mega particles'
    write(stdall,*) 'Update in species.f90 if you really need more than that many _per_thread_. I doubt it.'
    call error('change_particle_number','not enough particles in sp(isp)%lp')
  endif
  
  if (diffmp .eq. 0) return

  if (diffmp > 0) then
    write(message,'(a,i3,a,i9,a,i9,a,i5,a)') &
      'Changing species nr',isp,' ('//sp(isp)%name//') from ',mp, &
      ' to ', newmp,' on rank ',rank,' ['//trim(name)//']'
  else
    write(message,'(a,i3,a,i9,a,i9,a,i5,a)') &
      'Decrease species nr',isp,' ('//sp(isp)%name//') from ',mp, &
      ' to ', newmp,' on rank ',rank,' ['//trim(name)//']'
  endif
  write(stdall,'(a)') message
 
  ip_off = sp(isp)%imax - mp

  ! cleanup scratch arrays used in other parts of the code
                             if (verbose .gt. 1) print *, "copy-in", mp, newmp
  !---------------------------------------------------------------------------
  ! first use all the space we can find in other parts of the code.
  ! this place in the code is _the_ memory critical region
  pa=>sp(isp)%particle

  if (newmp < np) then
    allocate(i(min(mp,newmp)))
    do ip=1,min(mp,newmp)                                               ! no space for ids anywhere
      i(ip)=pa(ip)%i
    enddo
  
    jp=1                                                                ! use extra space 
    do jsp=1,nspecies
      if (jsp==isp) cycle
      pa2 => sp(jsp)%particle
      do ip=sp(jsp)%np+1, sp(jsp)%mp
        pa2(ip)%r = pa(jp)%r
        pa2(ip)%p = pa(jp)%p
        pa2(ip)%e = pa(jp)%e
        pa2(ip)%w = pa(jp)%w
        jp=jp+1
        if (jp > np) exit
      enddo
      if (jp > np) exit
    enddo
    if (jp <= np) then 
      jp = jp - 1
      allocate(r(mdim,np-jp),p(mcoord,np-jp),w(np-jp),q(4,np-jp))
      do ip=1,np-jp
        r(:,ip)=pa(ip+jp)%r
        p(:,ip)=pa(ip+jp)%p
        w(ip)=pa(ip+jp)%w
        i(ip)=pa(ip+jp)%i
        q(1,ip)=pa(ip+jp)%q(1)
        q(2,ip)=pa(ip+jp)%q(2)
        q(3,ip)=pa(ip+jp)%q(3)
        q(4,ip)=pa(ip+jp)%bits
      enddo
    endif
    
    deallocate(sp(isp)%particle)
                               if (verbose .gt. 1) print *, "copy-out", mp, newmp
    allocate(sp(isp)%particle(newmp))
    pa=>sp(isp)%particle
    jp=1                                                                ! use extra space 
    do jsp=1,nspecies
      if (jsp==isp) cycle
      pa2 => sp(jsp)%particle
      do ip=sp(jsp)%np+1, sp(jsp)%mp
        pa(jp)%r = pa2(ip)%r
        pa(jp)%p = pa2(ip)%p
        pa(jp)%e = pa2(ip)%e
        pa(jp)%w = pa2(ip)%w
        pa2(ip)%w= 0.0
        jp=jp+1
        if (jp > np) exit
      enddo
      if (jp > np) exit
    enddo
    if (jp <= np) then 
      jp = jp - 1
      do ip=1,np-jp
        pa(ip+jp)%r=r(:,ip)
        pa(ip+jp)%p=p(:,ip)
        pa(ip+jp)%w=w(ip)
        pa(ip+jp)%i=i(ip)
        pa(ip+jp)%q(1)=q(1,ip)
        pa(ip+jp)%q(2)=q(2,ip)
        pa(ip+jp)%q(3)=q(3,ip)
        pa(ip+jp)%bits=q(4,ip)
      enddo
      deallocate(r,p,w,q)                                             ! dealloc before touching >np particles
    endif
    if (trim(sp(isp)%name) .eq. "photon") then                        ! recompute energies
      do ip = 1, np
        pa(ip)%e= c%mc * sqrt(sum(pa(ip)%p**2))
      enddo
    else
      do ip = 1, np
        pa(ip)%e = sp(isp)%mass*sum(pa(ip)%p**2)/(sqrt(1.+sum(pa(ip)%p**2)*c%oc2) + 1.)
      enddo
    endif
                               if (verbose .gt. 1) print *, "add ids", ip_off,sp(isp)%imax,mp
    do ip=np+1,newmp                                                    ! copy particles
      pa(ip)%w = 0.
    enddo
    do ip=1,min(mp,newmp)                                               ! copy particles
      pa(ip)%i = i(ip)
    enddo
    deallocate(i)
  endif

  do ip=mp+1,newmp                                                    ! make new idx
    pa(ip)%w = 0
    pa(ip)%i = ip_off + ip
    !call mark_synthetic_particle (isp,ip,g%lb(3))                     ! node dep anyway; use iz=lb
  enddo

  sp(isp)%mp   = newmp
  if (newmp .gt. mp) sp(isp)%imax = ip_off + newmp
                               if (verbose .gt. 0) print *, "returning", sp(isp)%imax
END SUBROUTINE change_particle_number
