! id="Particles/mesh_sources.f90 $Id$"
! vim: nowrap
!=======================================================================
SUBROUTINE mesh_sources
  USE params,        only : nspecies, mdim, mcoord, mid, do_vth, &
                            do_dump, do_compare, time
  USE species,       only : cellvector, particle, sp, fields 
  USE grid_m,        only : g, nx, ny, nz
  USE units,         only : c
  USE dumps,         only : dump
  USE debug,         only : verbose
  implicit none
  type(particle), pointer, dimension(:) :: pa
  type(particle), pointer         :: pp
  real, dimension(mcoord)         :: p, v
  real, dimension(mdim)           :: r
  integer, dimension(mdim)        :: q
  real                            :: p2, og, v2, dd
  integer                         :: isp, ip, np, ix, iy, iz
  integer                         :: jx, jy, jz, kx, ky, kz
  integer                         :: firstcellindex                     ! first in a given cell
  real, dimension(:,:,:), allocatable:: vx, vy, vz, d, vt
  character(len=mid):: id = &
    'Particles/mesh_sources.f90 $Id$'
!.......................................................................
  call print_id(id)
  call trace_enter('MESH_SOURCES')
  
  do isp=1,nspecies                                                     ! loop over species
                                                 call timer('gather','x')
    do iz=1,g%n(3)                                                      ! zero fields
      fields(:,:,iz,isp)%d = 0.                                         ! zap charges
      fields(:,:,iz,isp)%v(1) = 0.                                      ! zap velocities
      fields(:,:,iz,isp)%v(2) = 0.                                      ! zap velocities
      fields(:,:,iz,isp)%v(3) = 0.                                      ! zap velocities
      fields(:,:,iz,isp)%vth = 0.                                       ! zap velocities
    end do
    if (sp(isp)%np .eq. 0) cycle                                        ! cycle if no particles

    pa => sp(isp)%particle                                              ! shortcut to the part data
    do iz=1,g%n(3)                                                      ! loop over cells
    do iy=1,g%n(2)
    do ix=1,g%n(1)
      np = fields(ix,iy,iz,isp)%n                                       ! particles in cell
      firstcellindex = fields(ix,iy,iz,isp)%i - np + 1
      if (np>0) then                                                    ! avoid if empty
        do ip=firstcellindex,fields(ix,iy,iz,isp)%i                     ! loop over particles
          pp => pa(ip)                                                  ! cache pointer
          r = pp%r
          if (any(r > 1.)) then
            print*,'MK',time,ip,pp%r,pp%q
          endif
          p = pp%p                                                      ! scratch vel-array
          p2= p(1)*p(1) + p(2)*p(2) + p(3)*p(3)                         ! momentum squared
          og = 1./sqrt(1.+p2*c%oc2)                                     ! 1./gamma
          v = p*og                                                      ! velocity
          q = (/ix,iy,iz/)
          call gather (r, q, fields, v, isp, pp%w)                      ! gather onto fields
        enddo                                     
      endif
    end do
    end do
    end do

!-----------------------------------------------------------------------
! FIXME: Should be recentered, but this is not a big problem
!-----------------------------------------------------------------------      
    if (do_vth) then
                                                 call timer('vth','x')
      do iz=1,g%n(3)                                                    ! loop over cells
      do iy=1,g%n(2)
      do ix=1,g%n(1)
        dd = fields(ix,iy,iz,isp)%d
        if (dd > 0.) then
          v = fields(ix,iy,iz,isp)%v  /dd  
          v2= fields(ix,iy,iz,isp)%vth/dd  !-sum(v**2)
          ! v^2 relative to average v
        else
          v2 = 0.0
        endif
        fields(ix,iy,iz,isp)%vth = sqrt(max(0.,v2))                     ! guard against centering
      enddo
      enddo
      enddo
    endif

    if (do_dump .or. do_compare .or. verbose>1) then
      allocate (d(nx,ny,nz), vt(nx,ny,nz))
      allocate (vx(nx,ny,nz), vy(nx,ny,nz), vz(nx,ny,nz))

      do iz=1,g%n(3)                                                    ! copy fields
        d (:,:,iz) = fields(:,:,iz,isp)%d
        vx(:,:,iz) = fields(:,:,iz,isp)%v(1)
        vy(:,:,iz) = fields(:,:,iz,isp)%v(2)
        vz(:,:,iz) = fields(:,:,iz,isp)%v(3)
        vt(:,:,iz) = fields(:,:,iz,isp)%vth
      end do
                                                    call dump (d , 'd' )
                                                    call dump (vx, 'vx')
                                                    call dump (vy, 'vy')
                                                    call dump (vz, 'vz')
                                                    call dump (vt, 'vt')
      deallocate (d, vt, vx, vy, vz)
    end if
  end do
                                                 call timer('end','x')
  

  call trace_exit('MESH_SOURCES')
END SUBROUTINE mesh_sources

!=======================================================================
SUBROUTINE mesh_sources_thermal
  call mesh_sources
END SUBROUTINE mesh_sources_thermal
!-----------------------------------------------------------------------
