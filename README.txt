README.txt $Id$
---------------------------------------------------

To build the code in an Experiments directory, do for example

cd Experiments/plasma_oscillations
make deps       # only needed when updating Makefile or options.mkf
make -j

For license, see Doc/LICENSE.txt

For documentation and news, see the BitBucket wiki, at 
https://bitbucket.org/thaugboelle/ppcode/wiki/Home
