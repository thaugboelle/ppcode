
ifndef MPI
  MPI = nompi
endif

GC = gfortran
ifeq ($(MPI),mpi)
  GC = mpif90
endif

FC    = $(GC) -x f95-cpp-input
LD    = $(GC)
OPT   = -O0
HW    = -I${BUILD} -J$(BUILD) -fno-range-check
PAR   = #-fopenmp
DBG   = -g -fbacktrace -fimplicit-none -ffpe-trap=invalid,zero,overflow -ftrapv -finit-real=nan
MAKE  = make
PREPROC += -D__GFORTRAN__

ifeq ($(PREC),8)
  FLTPREC=-fdefault-real-8
else
  FLTPREC=
endif
