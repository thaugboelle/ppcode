! vim: nowrap
!=======================================================================
! This routine is called by main, before main calls grid, which is again
! before main calls init_experiment.  The init_experiment_boundaries does
! not have to change the boundaries; in should in fact only do that in
! exceptional cases, but doing so cannot be delayed to when the user gets
! control in init_experiment, because by then the mesh must already be
! fixed. 
!=======================================================================
SUBROUTINE init_boundaries
  USE grid_m, only : g
  implicit none
  
  !---------------------------------------------------------------------
  ! Minimum boundaries needed for interpolation operator and field solver
  ! Note that the field_solver_boundaries need to do max/min, to respect
  ! the interpolation_boundaries.
  !---------------------------------------------------------------------
  call interpolation_boundaries
  call field_solver_boundaries
  
  !---------------------------------------------------------------------
  ! Change according to the experiment; note that this called is issued
  ! BEFORE the call to init_experiment, so it can only be used to 
  ! possibly modify the number of boundary zones, etc.
  !---------------------------------------------------------------------
  call init_experiment_boundaries

END SUBROUTINE init_boundaries
