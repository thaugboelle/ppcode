! vim: nowrap
!=======================================================================
!
! Bit table for signaling various things: The 3 upper bits are reserved
!
! Bit 63: Particle is selected for synchrotron radiation tracing
! Bit 62: Particle is selected for being dumped to disk continously for later tracing
! Bit 61: ...Free...
! Bit 0-48: Unique id of particle (This is space for 2^48 or 2.8e14 particles)
!
!=======================================================================
! Masks all flags, both selection and cell position bits, only leave 38 lower
!=======================================================================
INTEGER(KIND=8) FUNCTION mask_i_flags (i)
  implicit none
  !integer(kind=8),parameter:: mask=z'1fffffffffffffff'
  integer(kind=8), parameter :: mask=z'000000000000ffff'
  integer(kind=8) i
  mask_i_flags = iand(i,mask)
END
! Clears all flags, both selection and cell position bits, only leave 38 lower
!=======================================================================
SUBROUTINE clear_i_flags (i)
  implicit none
  integer(kind=8), parameter :: mask=z'000000000000ffff'
  integer(kind=8) i
  i = iand(i,mask)
END
! Sets the flag that signals a particle has been selected for tracing
!=======================================================================
SUBROUTINE set_i2_flag (i)
  implicit none
  integer(kind=8) i
  i = ibset(i,61)
END
! Clears the particle tracing flag
!=======================================================================
SUBROUTINE clear_i2_flag (i)
  implicit none
  integer(kind=8) i
  i = ibclr(i,61)
END
! Masks the particle tracing flag
!=======================================================================
INTEGER(KIND=8) FUNCTION mask_i2_flag (i)
  implicit none
  integer(kind=8) i
  mask_i2_flag = ibclr(i,61)
END
! Checks is a particle is selected for tracing
!=======================================================================
LOGICAL FUNCTION isset_i2_flag (i)
  implicit none
  integer(kind=8) i
  isset_i2_flag = btest(i,61)
END
! Sets the flag that signals a particle has been selected for tracing
!=======================================================================
SUBROUTINE set_i4_flag (i)
  implicit none
  integer(kind=8) i
  i = ibset(i,62)
END
! Clears the particle tracing flag
!=======================================================================
SUBROUTINE clear_i4_flag (i)
  implicit none
  integer(kind=8) i
  i = ibclr(i,62)
END
! Masks the particle tracing flag
!=======================================================================
INTEGER(KIND=8) FUNCTION mask_i4_flag (i)
  implicit none
  integer(kind=8) i
  mask_i4_flag = ibclr(i,62)
END
! Checks is a particle is selected for tracing
!=======================================================================
LOGICAL FUNCTION isset_i4_flag (i)
  implicit none
  integer(kind=8) i
  isset_i4_flag = btest(i,62)
END
!=======================================================================
! Set the flag that signals a particle is selected for synchrotron spectra
SUBROUTINE set_i8_flag (i)
  implicit none
  integer(kind=8) i
  i = ibset(i,63)
END
!=======================================================================
! Clears the synchrotron flag (1 + 2 + 4 = 7)
SUBROUTINE clear_i8_flag (i)
  implicit none
  integer(kind=8) i
  i = ibclr(i,63)
END
!=======================================================================
! Masks the synchrotron flag
INTEGER(KIND=8) FUNCTION mask_i8_flag (i)
  implicit none
  integer(kind=8) i
  mask_i8_flag = ibclr(i,63)
END
!=======================================================================
! Checks if the synchrotron flag is set
LOGICAL FUNCTION isset_i8_flag (i)
  implicit none
  integer(kind=8) i
  isset_i8_flag = btest(i,63)
END
