! Utilities/timer.f90 $Id$
! vim: nowrap
!=======================================================================
MODULE timer_m
  USE params, only : mid
  implicit none
  integer,            parameter         :: mtimer=30
  real(kind=8),       dimension(mtimer) :: start, time
  integer,            dimension(mtimer) :: ncalls
  integer                               :: ntimer=0, itimer
  character(len=mid), dimension(mtimer) :: labels
  real(kind=8)           :: current
  real(kind=8), external :: wallclock
CONTAINS
!-----------------------------------------------------------------------
SUBROUTINE findit (label)
  implicit none
  character(len=*) label
  do itimer=1,ntimer
    if (trim(label) == trim(labels(itimer))) return
  end do
  ntimer = ntimer+1
  itimer = ntimer
  labels(itimer) = label
  time(itimer) = 0.
  ncalls(itimer) = 0
END SUBROUTINE
END MODULE

!=======================================================================
SUBROUTINE timer (label, cmd)
  USE params, only: do_timer
  USE timer_m
  implicit none
  character(len=*) label, cmd
!-----------------------------------------------------------------------
  if (.not.do_timer) return
  current = wallclock()                                                 ! current time
  if (itimer > 0) then                                                  ! if timer is active ..
    time(itimer) = time(itimer) + current - start(itimer)               ! add to it
    ncalls(itimer) = ncalls(itimer)+1
  end if
  call findit (label)                                                   ! locate timer slot
  start(itimer) = current                                               ! start the timer
END SUBROUTINE

!=======================================================================
SUBROUTINE finalize_timer
  USE timer_m
  USE params,  only : stdout, nodes, it, particleupdates, nspectra, hl, &
                      do_synthetic_spectra, nsubcycle, do_timer, master, &
                      nspecies, rank, nomp, do_validate
  USE grid_m   , only : g
  !USE spectra, only : np_synth, nw
  implicit none
  real total, gtime, musec, frac
  integer(kind=8) gnp_synth, gparticleupdates
  logical o
!-----------------------------------------------------------------------
  if (.not. do_timer) return
  o = stdout > 0
  total = 1e-9
  if (o) write (stdout,'(/a,i7,a)') '     seconds    musec/part     %    STEP (rank=',rank,')'
  do itimer = 1,ntimer
    total = total + time(itimer)
  end do
  if (do_validate) total = 0.0
  do itimer = 1,ntimer
    if (o) then
      frac = merge (0.0_8, time(itimer)/total, do_validate)
      musec = frac*nomp*total*1e6/(particleupdates+1)
      if (nspecies==0 .or. do_validate) musec = 0.0
      if (frac > 0.001) &
        write (stdout,'(f12.3,4x,f10.4,f6.1,i7,4x,a16)') &
        time(itimer), musec, 100.*frac, ncalls(itimer), labels(itimer)
    endif
  end do
  if (o) then
    musec = nomp*nodes*total*1e6/(max(gparticleupdates,1_8))
    write (stdout,'(f12.3,4x,f10.4,f6.1,4x,a)') &
      total, musec, 100., 'TOTAL'
    musec = nomp*nodes*total*1e6/(max(it*product(int(g%gn,kind=8)),1_8))     
    write (stdout,'(/f12.2,1x,a)') &
      musec, ' core-microseconds per mesh point'
    musec = nomp*nodes*total*1e6/(max(particleupdates,1_8))
    if (nspecies==0) musec = 0.0
    write (stdout,'( f12.2,1x,a)') &
      musec, ' core-microseconds per particle'
  end if
  !if (do_synthetic_spectra .and. np_synth .gt. 0) then
  !  if (o) write (stdout,'( f12.2,1x,a)') nomp*total*1e9 /    &
  !    (real(np_synth,kind=8)*real(nspectra)*nw*it*nsubcycle), &
  !    ' core-nanoseconds per particle-frequency-direction'
  !end if
  if (nodes > 1) then
    call barrier

    call mpi_sum_real (total); total = total / nodes
    call mpi_sum_integer8 (particleupdates)
    if (master) write (stdout,*) hl
    if (master) write (stdout,'(/a)') '     seconds    musec/part     %    STEP'
    do itimer = 1,ntimer
      gtime = real(time(itimer))
      call mpi_sum_real(gtime); gtime = gtime / nodes
      if (do_validate) gtime = 0.0
      if (master) then
        frac = merge (0.0, gtime/total, do_validate)
        musec = frac*nomp*nodes*total*1e6/(particleupdates+1)
        if (nspecies==0) musec = 0.0
        if (frac > 0.001) write (stdout,'(f12.3,4x,f10.4,f6.1,4x,a16)') &
          gtime, musec, 100.*frac, labels(itimer)
      endif
    end do
    musec = nomp*nodes*total*1e6/(max(gparticleupdates,1_8))
    if (nspecies==0) musec = 0.0
    if (master) write (stdout,'(f12.3,4x,f10.4,f6.1,4x,a)') &
      total, musec, 100., 'TOTAL'
    musec = nomp*nodes*total*1e6/(max(it*product(int(g%gn,kind=8)),1_8))     
    if (master) write (stdout,'(/f12.2,1x,a)') &
      musec, ' core-microseconds per mesh point'
    musec = nomp*nodes*total*1e6/(max(particleupdates,1_8))
    if (nspecies==0) musec = 0.0
    if (master) write (stdout,'( f12.2,1x,a)') &
      musec, ' core-microseconds per particle'
    !if (do_synthetic_spectra) then
    !  gnp_synth = int(np_synth,kind=8)
    !  call mpi_sum_integer8 (gnp_synth)
    !  if (master .and. gnp_synth > 0) write (stdout,'( f12.2,1x,a)') nomp*nodes*total*1e9 / ( &
    !                            real(gnp_synth,kind=8)*real(nspectra)*nw*it*nsubcycle), &
    !  ' core-nanoseconds per particle-frequency-direction'
    !end if
  endif
END SUBROUTINE finalize_timer
