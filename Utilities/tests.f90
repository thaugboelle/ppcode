! Utilities/tests.f90 $Id$
! vim: nowrap
!=======================================================================
SUBROUTINE check_particle_ranges (label)
  USE params,  only : nspecies,rank, nodes, do_check_ranges, do_check_bounds
  USE species, only : sp, particle
  USE grid_m,    only : g
  implicit none
  character(len=*) label
  type(particle), pointer, dimension(:) :: pa
  integer isp,np,irank,ip,i
  real :: r(3),rmin(3),rmax(3),pmin(3),pmax(3),p(3),grlb(3),ds(3)
!...............................................................................
  if (do_check_bounds) call check_particle_boundaries (label)
  if (.not. do_check_ranges) return
  call barrier
#ifndef __xlc__
  call sleep(1)
#endif
  ds = g%ds
  grlb = g%grlb
  rmin = 1e10
  rmax = -rmin
  pmin = 1e10
  pmax = -rmin
  do irank=0,nodes-1
    if (rank==irank) then
      do isp=1,nspecies
        np = sp(isp)%np
        if (np > 0) pa => sp(isp)%particle
        do ip=1,np
          if (pa(ip)%w > 0.) then
    !r = (pa(ip)%r + pa(ip)%q)*ds + grlb
            r = pa(ip)%r + pa(ip)%q
            rmin = min(rmin,r)
            rmax = max(rmax,r)
            p = sp(isp)%particle(ip)%p
            pmin = min(pmin,p)
            pmax = max(pmax,p)
          end if
        end do
        print'(1x,a15,2i4,i10,3(1x,2g14.6))',label,rank,isp,np,(rmin(i),rmax(i),i=1,3)
        print'(1x,a15,2i4,i10,3(1x,2g14.6))',label,rank,isp,np,(pmin(i),pmax(i),i=1,3)
      end do
    end if
    call barrier
  end do
END SUBROUTINE check_particle_ranges

!=======================================================================
SUBROUTINE trace_particle (label)
  USE params,  only: rank
  USE debug,   only: ptrace
  USE species, only: sp, particle
  integer                                :: ip,isp
  type(particle), pointer, dimension(:)  :: pa
  character(len=*)                       :: label
!.......................................................................
  if (any(ptrace .eq. -1)) return 
  isp = ptrace(1)
  pa => sp(isp)%particle                                                ! point to particles
  do ip=1,sp(isp)%np                                                    ! loop over particles
    if (pa(ip)%i .eq. ptrace(2)) print'(1x,a,1x,a12,2x,a,i3,3f10.5)', &
      'Ptrace:',label,'rank,r =',rank,pa(ip)%r
  enddo
END SUBROUTINE trace_particle

!=======================================================================
SUBROUTINE check_particle_boundaries (label)
  USE params,  only : nspecies,rank, nodes, do_check_ranges, periodic, &
                      mpi, mdim, stdall
  USE species, only : sp, particle
  USE grid_m,    only : g
  USE debug,   only : verbose, maxprint
  USE particleoperations, only: global_coordinates
  implicit none
  character(len=*) label
  integer isp,np,irank,ip,i,npr,idim
  real, dimension(3) :: r, rmin, rmax, rlb, rub, grlb, grub
  type (particle), pointer, dimension(:):: pa
!.......................................................................
  call trce('check_particle_boundaries')

  rlb = (/ g%x(1)     , g%y(1)     , g%z(1) /)
  rub = (/ g%x(g%n(1)), g%y(g%n(2)), g%z(g%n(3)) /)
  where (periodic)
    rlb = g%rlb
    rub = g%rub
  endwhere

  npr = 0
  do irank=0,nodes-1
    if (rank==irank) then
      do isp=1,nspecies
        rmin = 1e10
        rmax = -rmin
        np = sp(isp)%np
        pa => sp(isp)%particle
        do ip=1,np
          if (pa(ip)%w .eq. 0) cycle
          call fold2 (global_coordinates(pa(ip)), r)
          rmin = min(rmin,r)
          rmax = max(rmax,r)
        end do
        if (any(rmin < rlb .or. rmax >= rub)) then
          npr = npr + 1
          print'(1x,2a,2i4,i10,3(1x,2(1pe15.7)))', 'position error ', &
            label,rank,isp,np,(rmin(i)-rlb(i),rub(i)-rmax(i),i=1,3)
          do ip=1,np
            if (pa(ip)%w .eq. 0) cycle
            call fold2 (global_coordinates(pa(ip)), r)
            if (any(r < rlb .or. r >= rub)) then
              do idim=1,mdim
                if (r(idim) < rlb(idim) .or. r(idim) >= rub(idim)) &
                  write(stdall,'(1x,2a,1x,i5.5,i2,f10.6,3i5,2l2)') &
                   'position error ', label, rank, idim, pa(ip)%r(idim), &
                   g%lb(idim), pa(ip)%q(idim)-mpi%offset(idim), g%ub(idim)-1, &
                   r(idim) < rlb(idim), r(idim) >= rub(idim)
              enddo
            endif
          end do
          if (npr > maxprint) &
            call error('check_particle_ranges','maxprint exceeded')
        end if
      end do
    end if
  end do
END SUBROUTINE check_particle_boundaries

!=======================================================================
SUBROUTINE print_meminfo(priomaxmem,availcoremem,memnode,memleft)
  USE params,  only : stdout
  USE species, only : kilobyte, megabyte, gigabyte
  implicit none
    logical, intent(in) :: priomaxmem
    real,    intent(in) :: availcoremem, memnode, memleft
      
    write(stdout,'(a,l8)')    'priomaxmem       =', priomaxmem
    write(stdout,'(a,f16.3)') 'availcoremem[GB] =', availcoremem
    write(stdout,'(a,f16.3)') 'availcoremem[MB] =', availcoremem*kilobyte
    write(stdout,'(a,f16.3)') 'availcoremem[KB] =', availcoremem*megabyte
    write(stdout,'(a,f16.3)') 'availcoremem[B]  =', availcoremem*gigabyte
    write(stdout,'(a,f16.3)') 'memnode[GB]      =', memnode
    write(stdout,'(a,f16.3)') 'memnode[MB]      =', memnode*kilobyte
    write(stdout,'(a,f16.3)') 'memnode[KB]      =', memnode*megabyte
    write(stdout,'(a,f16.3)') 'memnode[B]       =', memnode*gigabyte
    write(stdout,'(a,f16.3)') 'memleft[GB]      =', memleft
    write(stdout,'(a,f16.3)') 'memleft[MB]      =', memleft*kilobyte
    write(stdout,'(a,f16.3)') 'memleft[KB]      =', memleft*megabyte
    write(stdout,'(a,f16.3)') 'memleft[B]       =', memleft*gigabyte
    write(stdout,'(a,f16.3)') 'left/avail       =', memleft/availcoremem
END SUBROUTINE

!=======================================================================
SUBROUTINE test_overlap
  USE params, only: rank, nodes, periodic, stdall, stdout, master, mpi, hl
  USE grid_m, only: g
  implicit none
  logical all_ok
  real, allocatable, dimension(:,:,:):: bx,by
  integer ix,iy,iz
!.......................................................................
  all_ok = .true.
  call barrier()
  allocate(bx(g%n(1),g%n(2),g%n(3)),by(g%n(1),g%n(2),g%n(3)))

!-----------------------------------------------------------------------
  if (g%gn(1) > 1) then
    do ix=1,g%n(1)
      bx(ix,:,:) = ix + mpi%offset(1)
      by(ix,:,:) = ix + mpi%offset(1)
    end do
    if (periodic(1)) then
      if (mpi%me(1)==0) then
        do ix=1,g%lb(1)-1
          by(ix,:,:) = g%gn(1) + ix - g%lb(1)
        enddo
      endif
      if (mpi%me(1)==mpi%n(1)-1) then
        do ix=g%ub(1),g%n(1)
          by(ix,:,:) = ix - g%ub(1)
        enddo
      end if
    end if

    call overlap_x(bx)

    iz=g%lb(3)
    iy=g%lb(2)
    do ix=1,g%n(1)
      if (bx(ix,iy,iz).ne.by(ix,iy,iz)) then
        write(stdall,'(1x,a,5i5,2g12.4)') 'ERROR x    :', &
          rank,ix,iy,iz,mpi%offset(1),bx(ix,iy,iz),by(ix,iy,iz)
        all_ok = .false.
      end if
    end do
    call barrier()
  end if
!-----------------------------------------------------------------------
  if (g%gn(2) > 1) then
    do iy=1,g%n(2)
      bx(:,iy,:) = iy + mpi%offset(2)
      by(:,iy,:) = iy + mpi%offset(2)
    end do
    if (periodic(2)) then
      if (mpi%me(2)==0) then
        do iy=1,g%lb(2)-1
          by(:,iy,:) = g%gn(2) + iy - g%lb(2)
        enddo
      endif
      if (mpi%me(2)==mpi%n(2)-1) then
        do iy=g%ub(2),g%n(2)
          by(:,iy,:) = iy - g%ub(2)
        enddo
      end if
    end if

    call overlap_y(bx)

    iz=g%lb(3)
    ix=g%lb(1)
    do iy=1,g%n(2)
      if (bx(ix,iy,iz).ne.by(ix,iy,iz)) then
        write(stdall,'(1x,a,5i5,2g12.4)') 'ERROR y    :', &
          rank,ix,iy,iz,mpi%offset(2),bx(ix,iy,iz),by(ix,iy,iz)
        all_ok = .false.
      end if
    end do
    call barrier()
  endif
!-----------------------------------------------------------------------
  if (g%gn(3) > 1) then
    do iz=1,g%n(3)
      bx(:,:,iz) = iz+mpi%offset(3)                                     ! global index
      by(:,:,iz) = iz+mpi%offset(3)           
    end do
    if (periodic(3)) then
      if (mpi%me(3)==0) then
        do iz=1,g%lb(3)-1
          by(:,:,iz) = g%gn(3) + iz - g%lb(3)
        enddo
      endif
      if (mpi%me(3)==mpi%n(3)-1) then
        do iz=g%ub(3),g%n(3)
          by(:,:,iz) = iz - g%ub(3)
        enddo
      end if
    end if

    call overlap_z(bx)

    iy=g%lb(2)
    ix=g%lb(1)
    do iz=1,g%n(3)
      if (bx(ix,iy,iz).ne.by(ix,iy,iz)) then
        write(stdall,'(1x,a,4i5,2g12.4)') 'ERROR z    :', &
          rank,ix,iy,iz,bx(ix,iy,iz),by(ix,iy,iz)
        all_ok = .false.
      end if
    end do
    call barrier()
  endif
!-----------------------------------------------------------------------
  if (g%gn(1) > 1) then
    do ix=1,g%n(1)
      bx(ix,:,:) = ix + mpi%offset(1)
      by(ix,:,:) = ix + mpi%offset(1)
    end do
    if (periodic(1)) then
      if (mpi%me(1)==0) then
        do ix=1,g%lb(1)-1
          by(ix,:,:) = g%gn(1) + ix - g%lb(1)
        enddo
      endif
      if (mpi%me(1)==mpi%n(1)-1) then
        do ix=g%ub(1),g%n(1)
          by(ix,:,:) = ix - g%ub(1)
        enddo
      end if
    end if

    if (.not. mpi%lb(1)) then
      do ix=g%lb(1),g%lb(1) + g%ghub(1) - 1
        bx(ix,:,:) = bx(ix,:,:) - 0.5
      end do
      do ix=1,g%lb(1)-1
        bx(ix,:,:) = 0.5
      end do
    endif
    if (.not. mpi%ub(1)) then
      do ix=g%ub(1)-g%ghlb(1),g%ub(1)-1
        bx(ix,:,:) = bx(ix,:,:)  - 0.5
      end do
      do ix=g%ub(1),g%n(1)
        bx(ix,:,:) = 0.5
      end do
    endif
    
    call overlap_add_x(bx)

    iz=g%lb(3)
    iy=g%lb(2)
    do ix=1,g%n(1)
      if (bx(ix,iy,iz).ne.by(ix,iy,iz)) then
        write(stdall,'(1x,a,5i5,2g12.4)') 'ERROR x_add:', &
          rank,ix,g%lb(1),g%ub(1),mpi%offset(1),bx(ix,iy,iz),by(ix,iy,iz)
        all_ok = .false.
      end if
    end do
    call barrier()
  end if
!-----------------------------------------------------------------------
  if (g%gn(2) > 1) then
    do iy=1,g%n(2)
      bx(:,iy,:) = iy + mpi%offset(2)
      by(:,iy,:) = iy + mpi%offset(2)
    end do
    if (periodic(2)) then
      if (mpi%me(2)==0) then
        do iy=1,g%lb(2)-1
          by(:,iy,:) = g%gn(2) + iy - g%lb(2)
        enddo
      endif
      if (mpi%me(2)==mpi%n(2)-1) then
        do iy=g%ub(2),g%n(2)
          by(:,iy,:) = iy - g%ub(2)
        enddo
      end if
    end if

    if (.not. mpi%lb(2)) then
      do iy=g%lb(2),g%lb(2) + g%ghub(2) - 1
        bx(:,iy,:) = bx(:,iy,:) - 0.5
      end do
      do iy=1,g%lb(2)-1
        bx(:,iy,:) = 0.5
      end do
    endif
    if (.not. mpi%ub(2)) then
      do iy=g%ub(2)-g%ghlb(2),g%ub(2)-1
        bx(:,iy,:) = bx(:,iy,:)  - 0.5
      end do
      do iy=g%ub(2),g%n(2)
        bx(:,iy,:) = 0.5
      end do
    endif

    call overlap_add_y(bx)

    iz=g%lb(3)
    ix=g%lb(1)
    do iy=1,g%n(2)
      if (bx(ix,iy,iz).ne.by(ix,iy,iz)) then
        write(stdall,'(1x,a,5i5,2g12.4)') 'ERROR y_add:', &
          rank,ix,iy,iz,mpi%offset(2),bx(ix,iy,iz),by(ix,iy,iz)
        all_ok = .false.
      end if
    end do
    call barrier()
  endif
!-----------------------------------------------------------------------
  if (g%gn(3) > 1) then
    do iz=1,g%n(3)
      bx(:,:,iz) = iz+mpi%offset(3)                                     ! global index
      by(:,:,iz) = iz+mpi%offset(3)           
    end do
    if (periodic(3)) then
      if (mpi%me(3)==0) then
        do iz=1,g%lb(3)-1
          by(:,:,iz) = g%gn(3) + iz - g%lb(3)
        enddo
      endif
      if (mpi%me(3)==mpi%n(3)-1) then
        do iz=g%ub(3),g%n(3)
          by(:,:,iz) = iz - g%ub(3)
        enddo
      end if
    end if

    if (.not. mpi%lb(3)) then
      do iz=g%lb(3),g%lb(3) + g%ghub(3) - 1
        bx(:,:,iz) = bx(:,:,iz) - 0.5 + (iz - g%lb(3))
      end do
      do iz=1,g%lb(3)-1
        bx(:,:,iz) = 0.5 - (iz - 1)
      end do
    endif
    if (.not. mpi%ub(3)) then
      do iz=g%ub(3)-g%ghlb(3),g%ub(3)-1
        bx(:,:,iz) = bx(:,:,iz)  - 0.5 + (iz - (g%ub(3)-g%ghlb(3)))
      end do
      do iz=g%ub(3),g%n(3)
        bx(:,:,iz) = 0.5 - (iz - g%ub(3))
      end do
    endif

    call overlap_add_z(bx)

    iy=g%lb(2)
    ix=g%lb(1)
    do iz=1,g%n(3)
      if (bx(ix,iy,iz).ne.by(ix,iy,iz)) then
        write(stdall,'(1x,a,4i5,2g12.4)') 'ERROR z_add:', &
          rank,ix,iy,iz,bx(ix,iy,iz),by(ix,iy,iz)
        all_ok = .false.
      end if
    end do
    call barrier()
  endif
!-----------------------------------------------------------------------

  deallocate(bx,by)
  if (.not. all_ok) write(stdout,*) 'Name         rank   ix   iy   iz  off      Bx          By'
  if (.not. all_ok) call error('test_overlap','overlap test did not work; something ' // &
                               'is seriously rotten. Either the network is broken,' // &
                               ' or there have been changes in overlap or overlap_add')
  call barrier
  if (master) then
    write(stdout,*) hl
    write(stdout,*) 'Test of overlap_[xyz] and overlap_add_[xyz] passed'    
  end if
END SUBROUTINE
