! $Id$
! vim: nowrap
!===========================================================================
! VARIOUS MATH STUFF
!===========================================================================
!
! Components:
!
!    D3ROTATE In:  real :: V1(3), angle(3)   -- vector and rotation angles
!             Out: real :: V2(3)             -- result
!    Tranforms a vector V1 into vector V2, by rotation through angles angle(3).
!    NB!: Rotation is ordered as rotate around "Xaxis-then-Yaxis-then-Zaxis"
!======================================================================
MODULE MATH
  implicit none

CONTAINS
!----------------------------------------------------------------------
!Rotate a vector v1 through angle into v2, about x-then-y-then-z axes by angles 'angle(x,y,z)'.
SUBROUTINE ROTATE3D(V1,angle,V2)
   implicit none
   real, dimension(3), intent(in) :: V1, angle
   real, dimension(3)               :: V2
   real :: a,b,c                                                       ! Rotation angles
   real :: R1(3,3),R2(3,3),R3(3,3)                                     ! Component rotation matrices
   real :: R(3,3)                                                      ! Total resulting rotation

   a = angle(1) ; b = angle(2) ; c = angle(3)                          ! For convenience
   R1=RESHAPE((/1.,0.,0.,0.,cos(a),-sin(a),0.,sin(a),cos(a)/),(/3,3/)) ! Rotation around x-axis
   R2=RESHAPE((/cos(b),0.,sin(b),0.,1.,0.,-sin(b),0.,cos(b)/),(/3,3/)) ! Rotation around y-axis
   R3=RESHAPE((/cos(c),-sin(c),0.,sin(c),cos(c),0.,0.,0.,1./),(/3,3/)) ! Rotation around z-axis
!Rotation is ordered "rotate around Xaxis-then-Yaxis-then-Zaxis"
   R =(MATMUL(R3,MATMUL(R2,R1)))                                       ! J: Check matmul order
   V2=MATMUL(R,V1)                                                     ! J     ditto. order
   RETURN
  END  SUBROUTINE ROTATE3D
!----------------------------------------------------------------------
!Rotate a vector v1 by angle phi about an arbitrarily given axis n. Resulting vector in v2.
SUBROUTINE ARBROTATE3D(v1,v2,n,angle)
   implicit none
   real, dimension(3), intent(in) :: v1, n
   real, dimension(3)               :: v2
   real              , intent(in) :: angle
   real, dimension(3) :: norm
   real :: phi

   phi = -angle                                                        ! We rotate vector instead of coord-system...
   norm = n/sqrt(sum(n**2))                                            ! Normalize rot axis

   v2 = v1*cos(phi)&
      + norm*dot(norm,v1)*(1-cos(phi))&
      + cross(v1,norm)*sin(phi)                                        ! "Rotation formula", ref: Wolfram MathWorld

   RETURN
  END SUBROUTINE ARBROTATE3D
!----------------------------------------------------------------------
!Normalize a vector v of size s.
SUBROUTINE NORMALIZE(v,s)
   implicit none

   integer             , intent(in)   :: s
   real,   dimension(s)               :: v

   v = v/sqrt(sum(v**2))

   RETURN
  END SUBROUTINE NORMALIZE
!----------------------------------------------------------------------
FUNCTION cross(a,b)
  USE params, only : mcoord

  real, dimension(mcoord) :: a,b,cross
  integer :: i, i1, i2

  do i=1,mcoord
    i1 = mod(i  ,mcoord)+1
    i2 = mod(i+1,mcoord)+1
    cross(i) = a(i1)*b(i2) - b(i1)*a(i2)
  end do
END FUNCTION
!-----------------------------------------------------------------------
FUNCTION dot(a,b)
  USE params, only : mcoord

  real, dimension(mcoord) :: a,b
  real :: dot

  dot = sum(a*b)
END FUNCTION
!-----------------------------------------------------------------------
REAL FUNCTION window(t,t0,t1,k,m)
  implicit none

  real(kind=8)        :: t, t0, t1
 !real,    intent(in) :: t, t0, t1                                      ! time, tini, tend
  integer, intent(in) :: k, m                                           ! window coefficients
  real                :: rk, rm                                         ! as reals

  rk = real(k)
  rm = real(m)

  window = exp(-(rk*((t-.5*(t0+t1))/(t1-t0)))**rm)                      ! ad hoc window (rethink - maybe other windows are better? Blackman-Nutall)
END FUNCTION window
!-----------------------------------------------------------------------
SUBROUTINE divstat (label, bx, by, bz)                                  ! div(B) statistics
  USE params, only : master
  USE grid_m,   only : g, odx, ody, odz
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: bx, by, bz                   ! bx is centered on (ix    ,iy+1/2,iz+1/2) and so on
!  real, allocatable, dimension(:,:,:) :: div
  integer ix, iy, iz, nrms, n1
  real div, divmin, divmax, divrms, div1, brms
  character(len=*) :: label
!.......................................................................
!  allocate (div(g%n(1),g%n(2),g%n(3)))
  divmin = 0.
  divmax = 0.
  divrms = 0.
  brms = 0.
  nrms = 0
  do iz=g%lb(3),g%ub(3)-1                                               ! ax is defined for ix=[lb(1),ub(1)-1], iy=[lb(2),ub(2)], iz=[lb(3),ub(3)]
  div1 = 0.
  n1 = 0.
  do iy=g%lb(2),g%ub(2)-1
  do ix=g%lb(1),g%ub(1)-1
    div = (bx(ix+1,iy,iz)-bx(ix,iy,iz))*odx & 
        + (by(ix,iy+1,iz)-by(ix,iy,iz))*ody &
        + (bz(ix,iy,iz+1)-bz(ix,iy,iz))*odz
    divmin = min(divmin,div)
    divmax = max(divmax,div)
    divrms = divrms + div**2
    nrms = nrms + 1
    div1 = div1 + div**2
    brms = brms + bx(ix,iy,iz)**2 + by(ix,iy,iz)**2 + bz(ix,iy,iz)**2
    n1 = n1 + 1
  end do
  end do
  end do
  brms = sqrt(brms/nrms)
  divrms = sqrt(divrms/nrms)
  if (master) print'(a,4(1pe12.3),1x,a)',' div: rms, min, max = ', &
    divrms, divmin, divmax, brms/minval(g%ds), label
!  deallocate (div)
END SUBROUTINE divstat
!-----------------------------------------------------------------------
SUBROUTINE curlup (ax, ay, az, bx, by, bz)                              ! curl vector operator
  USE grid_m, only: g, odx, ody, odz
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: ax, ay, az, bx, by, bz       ! ax is centered on (ix    ,iy+1/2,iz+1/2) and so on
  integer ix, iy, iz                                                    ! bx is centered on (ix+1/2,iy    ,iz    ) and so on 
!.......................................................................
  do iz=2,g%n(3)                                                        ! ax is defined for ix=[lb(1),ub(1)-1], iy=[lb(2),ub(2)], iz=[lb(3),ub(3)]
  do iy=2,g%n(2)
  do ix=2,g%n(1)
    bx(ix,iy,iz) = (az(ix,iy,iz)-az(ix,iy-1,iz))*ody &
                 - (ay(ix,iy,iz)-ay(ix,iy,iz-1))*odz 
    by(ix,iy,iz) = (ax(ix,iy,iz)-ax(ix,iy,iz-1))*odz &
                 - (az(ix,iy,iz)-az(ix-1,iy,iz))*odx 
    bz(ix,iy,iz) = (ay(ix,iy,iz)-ay(ix-1,iy,iz))*odx &
                 - (ax(ix,iy,iz)-ax(ix,iy-1,iz))*ody 
  end do
  end do
  end do
END SUBROUTINE curlup
!-----------------------------------------------------------------------
SUBROUTINE curldn (ax, ay, az, bx, by, bz)                              ! curl vector operator
  USE grid_m, only: g, odx, ody, odz                                      ! ax is defined for ix=[lb(1),ub(1)-1], iy=[lb(2),ub(2)], iz=[lb(3),ub(3)]
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: ax, ay, az, bx, by, bz       ! ax is centered on (ix    ,iy+1/2,iz+1/2) and so on
  integer ix, iy, iz                                                    ! bx is centered on (ix+1/2,iy    ,iz    ) and so on 
!.......................................................................
  do iz=1,g%n(3)-1                                                      ! this loop block COULD be split into one for ..
  do iy=1,g%n(2)-1                                                      ! .. each component, with different individual ..
  do ix=1,g%n(1)-1                                                      ! .. loop boundaries, but there is no point; ..
    bx(ix,iy,iz) = (az(ix,iy+1,iz)-az(ix,iy,iz))*ody &                  ! .. a single loop block has less overhead ..
                 - (ay(ix,iy,iz+1)-ay(ix,iy,iz))*odz                    ! .. and so the few extra indices don't matter
    by(ix,iy,iz) = (ax(ix,iy,iz+1)-ax(ix,iy,iz))*odz &
                 - (az(ix+1,iy,iz)-az(ix,iy,iz))*odx 
    bz(ix,iy,iz) = (ay(ix+1,iy,iz)-ay(ix,iy,iz))*odx &
                 - (ax(ix,iy+1,iz)-ax(ix,iy,iz))*ody 
  end do
  end do
  end do
END SUBROUTINE curldn
!-----------------------------------------------------------------------
SUBROUTINE curlcurl (ax, ay, az, bx, by, bz)                            ! curl(curl()) vector operator
  USE grid_m, only: g, odx, ody, odz
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: ax, ay, az, bx, by, bz       ! ax and bx are centered on (ix,iy+1/2,iz+1/2) and so on
  real, allocatable, dimension(:,:,:) :: cx, cy, cz                     ! bx
!.......................................................................
  allocate (cx(g%n(1),g%n(2),g%n(3)), &
            cy(g%n(1),g%n(2),g%n(3)), &
            cz(g%n(1),g%n(2),g%n(3)))
  call curldn (ax, ay, az, cx, cy, cz)                                  ! one curl has an op-count of 3 add + 2 mult
  call curlup (cx, cy, cz, bx, by, bz)                                  ! a curlcurl thus takes 6 add + 4 mult
  deallocate (cx, cy, cz)                                               ! Cf. a laplace(B) which takes 3*(2 add + 2 mult) = 6 add + 6 mult
  call overlap (bx)                                                     ! or, alternatively, 3*(3 add + 1 mult) = 9 add + 3 mult
  call overlap (by)
  call overlap (bz)
END SUBROUTINE curlcurl
!-----------------------------------------------------------------------
END MODULE MATH
