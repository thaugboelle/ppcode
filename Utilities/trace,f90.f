! $Id$
!=======================================================================
MODULE clock_m
  real, save:: wc0=0.0
END MODULE
!=======================================================================
MODULE trace_data
  integer, save:: depth=1
END MODULE
!=======================================================================
SUBROUTINE trce(string)
  USE params, only : hl, rank, stdout, trace
  USE clock_m
  implicit none
  character(len=*) string
  real wallclock
  if (wc0==0) wc0=wallclock()
  if (trace) write(stdout,'(a,f8.1,a)') hl,wallclock()-wc0,' s'
  if (trace) write(stdout,'(a,i7.7,a)') 'R=', rank, ' calling '//string
END
!=======================================================================
SUBROUTINE print_id (id)
  USE params, only: mid, hl, master, stdout, trace
  implicit none
  character(len=mid) id
  if (id==' ') return
  call print_hl
  if (master) then
    write (stdout,*) id
    call flush(stdout)
  end if
  if (.not. trace) id = ' '
END SUBROUTINE
!=======================================================================
SUBROUTINE print_hl
  USE params, only: mid, hl, master, stdout, trace
  USE clock_m
  implicit none
  real wallclock
  if (wc0==0) wc0=wallclock()
  if (master) then
    write (stdout,'(a,f8.1,a)') hl,wallclock()-wc0,' s'
  end if
END SUBROUTINE
!=======================================================================
SUBROUTINE trace_enter (string)
  USE params, only: mid, master, stdout, trace, out_ranks, rank
  USE trace_data
  implicit none
  character(len=*) string
  character(len=80) line
  integer lstr
!-----------------------------------------------------------------------
  if (trace) then
    line = ' '
    lstr = len('Entering ')+len(string)
    line(depth:depth+lstr) = 'Entering '//string
    if (master .or. out_ranks) write (stdout,*) trim(line),rank,(1+depth)/2
    depth = depth+2
  end if
  !call barrier_trace (string)                                           ! this must NOT be inside the if!
END SUBROUTINE
!=======================================================================
SUBROUTINE trace_exit (string)
  USE params, only: mid, hl, master, stdout, trace, out_ranks, rank
  USE trace_data
  implicit none
  character(len=*) string
  character(len=80) line
  integer lstr
!-----------------------------------------------------------------------
  if (trace .and. (master .or. out_ranks)) then
    depth = max(depth-2,1)
    line = ' '
    lstr = len('Exiting')+len(string)
    line(depth:depth+lstr) = 'Exiting '//string
    write (stdout,*) trim(line),rank,(1+depth)/2
  end if
END SUBROUTINE
!=======================================================================
!> Make a unique id for the current run.
SUBROUTINE unique_id (id)
  USE params, only : rank
  implicit none
  integer :: id                                                         !> a unique id for this run
  integer :: cnt
  real    :: x
!-----------------------------------------------------------------------
  call system_clock(count=cnt)                                          ! get system time...
  call random_number(x)                                                 ! ...and a random number
  id = modulo(int(x*cnt*69069_8,kind=8),2_8**30)                        ! fold them
  call mpi_broadcast(id,0)                                              ! root broadcasts the id
END SUBROUTINE
!===============================================================================
SUBROUTINE dbg_trace (routine, label, level)
  USE params
  implicit none
  character(len=*) routine,label
  integer level
  character(len=120) file
  real:: wallclock
  real, save:: t0=0., t1
  logical, save:: first_time=.true., exists=.false.
!-----------------------------------------------------------------------
  if (.not.trace .or. level>trace_level) return
  !$omp master
#ifdef __INTEL_COMPILER
  inquire(directory='dbg',exist=exists)
#else
  inquire(file='dbg',exist=exists)
#endif
  !$omp end master
  !$omp barrier
  if (.not. exists) return
!
  !$omp master
  if (t0==0.0) t0=wallclock()
  t1=wallclock()
  write(file,'(a,i5.5,a)') 'dbg/'//trim(routine)//'_',rank
  if (trim(label)=='init') then
    open(trace_unit,file=file,form='formatted',status='unknown')
  else
    open(trace_unit,file=file,form='formatted',status='unknown',position='append')
  endif
  write(trace_unit,'(f13.6,2x,a)') t1-t0,label
  close(trace_unit)
  t0=wallclock()
  !$omp end master
  !$omp barrier
  end
!=======================================================================

