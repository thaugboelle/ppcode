! vim: nowrap
!=======================================================================
MODULE laplace_m
  USE params, ONLY: dt, mpi
  USE units,  ONLY: c
  USE grid_m, ONLY: g, nx, ny, nz, odxq, odyq, odzq
CONTAINS
!=======================================================================
! 7pt 6th order Laplace operator, for filtering high-k
!=======================================================================
FUNCTION laplace6 (f, a) result(h)
  implicit none
  real(8), dimension(nx,ny,nz) :: f, h
  integer ix, iy, iz, lb(3), ub(3)
  real a, lpm0, lpm1, lpm2, lpm3, odx6, ody6, odz6
!.......................................................................
  if (a <= 0) return
  
  lpm3 = + 1.*a
  lpm2 =  -6.*a
  lpm1 = +15.*a
  lpm0 = -20.*a
  odx6 = odxq**3
  ody6 = odyq**3
  odz6 = odzq**3

  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  do iz=lb(3),ub(3)
  do iy=lb(2),ub(2)
  do ix=lb(1),ub(1)
    h(ix,iy,iz) = ( lpm3*(f(ix-3,iy,iz)+f(ix+3,iy,iz)) &
                  + lpm2*(f(ix-2,iy,iz)+f(ix+2,iy,iz)) &
                  + lpm1*(f(ix-1,iy,iz)+f(ix+1,iy,iz)) &
                  + lpm0* f(ix  ,iy,iz) ) * odx6       &
                + ( lpm3*(f(ix,iy-3,iz)+f(ix,iy+3,iz)) &
                  + lpm2*(f(ix,iy-2,iz)+f(ix,iy+2,iz)) &
                  + lpm1*(f(ix,iy-1,iz)+f(ix,iy+1,iz)) &
                  + lpm0* f(ix,iy,  iz) ) * ody6       &
                + ( lpm3*(f(ix,iy,iz-3)+f(ix,iy,iz+3)) &
                  + lpm2*(f(ix,iy,iz-2)+f(ix,iy,iz+2)) &
                  + lpm1*(f(ix,iy,iz-1)+f(ix,iy,iz+1)) &
                  + lpm0* f(ix,iy,iz  ) ) * odz6
  enddo
  enddo
  enddo
END FUNCTION laplace6
!=======================================================================
! 7pt 2th order Laplace operator, for filtering high-k
!=======================================================================
FUNCTION laplace_6th (f, a) result(h)
  implicit none
  real, dimension(nx,ny,nz) :: f, h
  integer ix, iy, iz, lb(3), ub(3)
  real a, lpm0, lpm1, lpm2, lpm3
!.......................................................................
  if (a <= 0) return
  
  lpm3 =  + 2./180.*a
  lpm2 =  -27./180.*a
  lpm1 = +270./180.*a
  lpm0 = -490./180.*a

  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  do iz=lb(3),ub(3)
  do iy=lb(2),ub(2)
  do ix=lb(1),ub(1)
    h(ix,iy,iz) = ( lpm3*(f(ix-3,iy,iz)+f(ix+3,iy,iz)) &
                  + lpm2*(f(ix-2,iy,iz)+f(ix+2,iy,iz)) &
                  + lpm1*(f(ix-1,iy,iz)+f(ix+1,iy,iz)) &
                  + lpm0* f(ix  ,iy,iz) ) * odxq       &
                + ( lpm3*(f(ix,iy-3,iz)+f(ix,iy+3,iz)) &
                  + lpm2*(f(ix,iy-2,iz)+f(ix,iy+2,iz)) &
                  + lpm1*(f(ix,iy-1,iz)+f(ix,iy+1,iz)) &
                  + lpm0* f(ix,iy,  iz) ) * odyq       &
                + ( lpm3*(f(ix,iy,iz-3)+f(ix,iy,iz+3)) &
                  + lpm2*(f(ix,iy,iz-2)+f(ix,iy,iz+2)) &
                  + lpm1*(f(ix,iy,iz-1)+f(ix,iy,iz+1)) &
                  + lpm0* f(ix,iy,iz  ) ) * odzq
  enddo
  enddo
  enddo
END FUNCTION laplace_6th

!=======================================================================
! 5pt 2nd order Laplace operator, for filtering high-k
!=======================================================================
FUNCTION laplace_4th (f, a) result(h)
  implicit none
  real, dimension(nx,ny,nz) :: f, h
  integer ix, iy, iz, lb(3), ub(3)
  real a, lpm0, lpm1, lpm2
!.......................................................................
  if (a <= 0) return
  
  lpm2 =  -1./12.*a
  lpm1 = +16./12.*a
  lpm0 = -30./12.*a

  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  do iz=lb(3),ub(3)
  do iy=lb(2),ub(2)
  do ix=lb(1),ub(1)
    h(ix,iy,iz) = ( lpm2*(f(ix-2,iy,iz)+f(ix+2,iy,iz)) &
                  + lpm1*(f(ix-1,iy,iz)+f(ix+1,iy,iz)) &
                  + lpm0* f(ix  ,iy,iz) ) * odxq       &
                + ( lpm2*(f(ix,iy-2,iz)+f(ix,iy+2,iz)) &
                  + lpm1*(f(ix,iy-1,iz)+f(ix,iy+1,iz)) &
                  + lpm0* f(ix,iy,  iz) ) * odyq       &
                + ( lpm2*(f(ix,iy,iz-2)+f(ix,iy,iz+2)) &
                  + lpm1*(f(ix,iy,iz-1)+f(ix,iy,iz+1)) &
                  + lpm0* f(ix,iy,iz  ) ) * odzq
  enddo
  enddo
  enddo
END FUNCTION laplace_4th

!=======================================================================
! 3pt 2nd order Laplace operator, for filtering high-k
!=======================================================================
FUNCTION laplace_2nd (f, a) result(h)
  implicit none
  real, dimension(nx,ny,nz) :: f, h
  integer ix, iy, iz, lb(3), ub(3)
  real a, lpm0, lpm1, lpm2
!.......................................................................
  if (a <= 0) return
  
  lpm1 = +1.0*a
  lpm0 = -2.0*a

  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  do iz=lb(3),ub(3)
  do iy=lb(2),ub(2)
  do ix=lb(1),ub(1)
    h(ix,iy,iz) = ( lpm1*(f(ix-1,iy,iz)+f(ix+1,iy,iz)) &
                  + lpm0* f(ix  ,iy,iz) ) * odxq       &
                + ( lpm1*(f(ix,iy-1,iz)+f(ix,iy+1,iz)) &
                  + lpm0* f(ix,iy,  iz) ) * odyq       &
                + ( lpm1*(f(ix,iy,iz-1)+f(ix,iy,iz+1)) &
                  + lpm0* f(ix,iy,iz  ) ) * odzq
  enddo
  enddo
  enddo
END FUNCTION laplace_2nd
!=======================================================================
END MODULE laplace_m
