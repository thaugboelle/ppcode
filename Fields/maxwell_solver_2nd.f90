! Id: maxwell_solver_6th.f90,v 1.53 2013/04/28 19:10:51 aake Exp $
! vim: nowrap
!-----------------------------------------------------------------------
MODULE maxwell_solver
  USE grid_m,          ONLY : g, o_ngrid                                ! "The Grid"
  USE units,           ONLY : c, elm                                    ! unit system constants.
  USE grid_m,          ONLY : odx, ody, odz, odxq, odyq, odzq,bx,by,bz  ! grid props
  USE params,          ONLY : nspecies, &                               ! src arrs, nr of species
                              mid, dt, &                                ! other params
                              out_namelists, trace, dbg, mdim, periodic ! debug levels

  implicit none
  real    :: tol_b, tol_e, tol_divb                                     ! tolerance limits
  integer :: maxiter_b, maxiter_e, miniter_e, iter_b, iter_e, &         ! tolerance controls
             miniter_divb, maxiter_divb, iter_divb, every_e, every_b

 !These are prefactors and variables needed for the integration sceheme - common to all finit diff orders.
  real    :: emf, omemf                                                 ! time de-centering params
  real    :: filter, sor, f_2nd, f_6th                                  ! checkerboard filter and SORelaxation parameter
  real    :: dd1e, dd1b                                                 ! prefactor for e_fix and b_fix
  real    :: ce1, ce2, ce3                                              ! e_push prefactors
  real    :: cb1, cb2, cb3                                              ! b_push prefactors
  real    :: c4bx, c4by, c4bz, denom                                    ! b_push prefactors

 !This is ported from the pic_stagger module.
  real    :: aa                                                         ! stagger operator prefactors
  real    :: a1
  real    :: ax3, ay3, az3                                              ! do.
  real    :: lp0, lpm1, lpp1                                            ! LaPlace prefactors
  integer :: order=2                                                    ! sixth order
  logical :: do_explicit
  integer :: lb(3), ub(3)
CONTAINS

!----------------------------------------------------------------------
SUBROUTINE maxwell_stagger_prefactors                                   ! compute prefactors for the stagger operators
USE grid_m, only : g
  implicit none

  integer :: i
  real    :: dx, dy, dz

   !----------------
    dx = g%ds(1)
    dy = g%ds(2)
    dz = g%ds(3)

    lpm1=1.0; lp0=-2.0

    aa=0.5
    ax3=1.; ay3=1.; az3=1.

    lpp1=lpm1

    ax3=ax3/dx                ; ay3=ay3/dy                ; az3=az3/dz
   !----------------
  
END SUBROUTINE maxwell_stagger_prefactors
!-----------------------------------------------------------------------
FUNCTION ddxup (f,i,j,k)
  USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddxup
  integer                                            :: i, j, k

     ddxup =    ax3*(f(i+1 ,j,k)-f(i   ,j,k))
END FUNCTION ddxup
!-----------------------------------------------------------------------
FUNCTION ddxdn(f,i,j,k)
  USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddxdn
  integer                                            :: mx, my, mz
  integer                                            :: i, j, k

      ddxdn =    ax3*(f(i   ,j,k)-f(i-1 ,j,k))
END FUNCTION ddxdn
!-----------------------------------------------------------------------
FUNCTION ddyup (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddyup
  integer                                            :: i, j, k

    ddyup =    ay3*(f(i,j+1 ,k)-f(i,j   ,k))
END FUNCTION ddyup
!-----------------------------------------------------------------------
FUNCTION ddydn (f,i,j,k)
USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddydn
  integer                                            :: i, j, k

      ddydn =    ay3*(f(i,j   ,k)-f(i,j-1 ,k))
END FUNCTION ddydn
!-----------------------------------------------------------------------
FUNCTION ddzup (f,i,j,k)
  USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddzup
  integer                                            :: i, j, k

    ddzup  =    az3*(f(i,j,k+1 )-f(i,j,k   ))
END FUNCTION ddzup
!-----------------------------------------------------------------------
FUNCTION ddzdn (f,i,j,k)
  USE grid_m, only : g
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddzdn
  integer                                            :: i, j, k

    ddzdn  =    az3*(f(i,j,k   )-f(i,j,k-1 ))
END FUNCTION ddzdn


!=======================================================================!
SUBROUTINE b_push
  USE params,          ONLY: stdout, master, time, stdall, mpi, &
                             do_b_fix, it, dt
  USE grid_m,          ONLY: ex, ey, ez    ! fields needed
  USE grid_m,          ONLY: bx, by, bz    ! more fields needed
  USE maxwell_sources, ONLY: verbose
  USE maxwell_sources, ONLY: bxa, bya, bza, bxm, bym, bzm
  USE dumps,           ONLY: dump
  USE math,            ONLY: divstat
  USE pic_stagger,     ONLY: curlxup, curlyup, curlzup
  implicit none
  logical, save :: calldivb=.true.
  integer ::  iz
  real maxerrp
!.......................................................................!
  call trace_enter('B_PUSH')
!-----------------------------------------------------------------------!
! Step the B-field
!-----------------------------------------------------------------------!
  lb =       g%lb               
  ub = merge(g%ub, g%ub-1, mpi%ub)                                      ! proper boundaries

  if (do_explicit) then
    bxm = curlxup (ez, ey)
    bym = curlyup (ex, ez)
    bzm = curlzup (ey, ex)
    do iz=lb(3),ub(3)
           bx(lb(1):ub(1),lb(2):ub(2),iz) = & 
           bx(lb(1):ub(1),lb(2):ub(2),iz) - &
      cb1*bxm(lb(1):ub(1),lb(2):ub(2),iz)

           by(lb(1):ub(1),lb(2):ub(2),iz) = & 
           by(lb(1):ub(1),lb(2):ub(2),iz) - &
      cb1*bym(lb(1):ub(1),lb(2):ub(2),iz)

           bz(lb(1):ub(1),lb(2):ub(2),iz) = & 
           bz(lb(1):ub(1),lb(2):ub(2),iz) - &
      cb1*bzm(lb(1):ub(1),lb(2):ub(2),iz)
    end do
  else
    call b_push_implicit
  endif
!-----------------------------------------------------------------------!
! Set boundary components of b-field
!-----------------------------------------------------------------------!
  call b_boundaries(bx,by,bz)
                                                      call dump(bx,'Bx')
                                                      call dump(by,'By')
                                                      call dump(bz,'Bz')
!-----------------------------------------------------------------------!
! Possibly check div(B) for roundoff error build-up
!-----------------------------------------------------------------------!
  iter_divb = 0
  if (calldivb) call divstat ('B', bx, by, bz)
  if (do_b_fix .and. mod(it,every_b)==0) then 
    call b_fix (bx, by, bz)
    if (calldivb) call divstat ('B(fixed)', bx, by, bz)
  endif
  calldivb = .false.

  call trace_exit('B_PUSH')
END SUBROUTINE b_push

!=======================================================================!
SUBROUTINE e_push
  USE params,          ONLY: stdout, master, time, stdall, mpi, &
                             do_e_fix, it, dt
  USE grid_m,          ONLY: ex, ey, ez    ! fields needed
  USE grid_m,          ONLY: bx, by, bz    ! more fields needed
  USE maxwell_sources, ONLY: verbose
  USE maxwell_sources, ONLY: sx, sy, sz, jx, jy, jz, charge
  USE maxwell_sources, only: exa=>bxm, eya=>bym, eza=>bzm
  USE dumps,           ONLY: dump
  USE math,            ONLY: divstat
  USE pic_stagger,     ONLY: curlxdn, curlydn, curlzdn
  USE stat,            ONLY: energy_e_box, prefe
  implicit none
  logical, save :: calldivb=.true.
  integer ::  iz, lb(3), ub(3)
  real(kind=8) :: total3
  real maxerrp
!.......................................................................!
  call trace_enter('E_PUSH')
!-----------------------------------------------------------------------!
! Step the E-field
!-----------------------------------------------------------------------!
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)

  do iz=lb(3),ub(3)
    exa(:,:,iz) = ex(:,:,iz)                                            ! store current ..
    eya(:,:,iz) = ey(:,:,iz)                                            ! .. values for ..
    eza(:,:,iz) = ez(:,:,iz)                                            ! .. Io/stat.f90
  end do

  if (do_explicit) then
    sx = curlxdn (bz, by)
    sy = curlydn (bx, bz)
    sz = curlzdn (by, bx)
    do iz=lb(3),ub(3)
           ex(lb(1):ub(1),lb(2):ub(2),iz) = & 
           ex(lb(1):ub(1),lb(2):ub(2),iz) + &
       ce1*sx(lb(1):ub(1),lb(2):ub(2),iz) - &
       ce2*jx(lb(1):ub(1),lb(2):ub(2),iz)
        
           ey(lb(1):ub(1),lb(2):ub(2),iz) = & 
           ey(lb(1):ub(1),lb(2):ub(2),iz) + &
       ce1*sy(lb(1):ub(1),lb(2):ub(2),iz) - &
       ce2*jy(lb(1):ub(1),lb(2):ub(2),iz)
      
           ez(lb(1):ub(1),lb(2):ub(2),iz) = & 
           ez(lb(1):ub(1),lb(2):ub(2),iz) + &
       ce1*sz(lb(1):ub(1),lb(2):ub(2),iz) - &
       ce2*jz(lb(1):ub(1),lb(2):ub(2),iz)
    end do
  else
    call e_push_implicit
  endif

  do iz=lb(3),ub(3)
    sx(:,:,iz) = (ex(:,:,iz)+exa(:,:,iz))**2 &
               + (ey(:,:,iz)+eya(:,:,iz))**2 &
               + (ez(:,:,iz)+eza(:,:,iz))**2
  end do
  energy_e_box = 0.25*prefe*total3(sx)*g%dV

!-----------------------------------------------------------------------!
! Set boundary components of E-field
!-----------------------------------------------------------------------!
  call e_boundaries (ex, ey, ez)
                                                      call dump(ex,'Ex')
                                                      call dump(ey,'Ey')
                                                      call dump(ez,'Ez')
!-----------------------------------------------------------------------!
! Possibly check div(E) for roundoff error build-up
!-----------------------------------------------------------------------!
  if (do_e_fix .and. mod(it,every_e)==0) then 
    call e_fix (ex, ey, ez, charge)
  endif

  call trace_exit('E_PUSH')
END SUBROUTINE e_push

!=======================================================================!
SUBROUTINE b_push_implicit
  USE params,          ONLY: mpi, rank, stdout, master, do_b_fix
  USE grid_m,          ONLY: g, ex, ey, ez
  USE maxwell_sources, ONLY: jx, jy, jz, bxm, bym, bzm, &
                             bxa, bya, bza, sx, sy, sz, &
                             bmax, verbose
  USE dumps,           ONLY: dump
  USE math,            ONLY: divstat
  USE pic_stagger
  implicit none
  real, dimension(:,:,:), allocatable   :: bsqr
  logical, save  :: calldivb=.true.
  integer        :: iter
  real           :: errp, aver3, maxerr, b0e, errpp, maxerrp
  real           :: max_scalar
  integer        :: ix, iy, iz
  real, external :: max4
!......................................................................!
  call trace_enter('B_PUSH')

  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)

!----------------------------------------------------------------------!
! Reset - then construct - current density on interior.
!----------------------------------------------------------------------!
                                                    call dump(ez,'ex0')
                                                    call dump(ey,'ey0')
                                                    call dump(ez,'ez0')
!----------------------------------------------------------------------!
!Now calculate Sx,Sy,Sz on interior only - for the relaxation solve. 

  do iz = g%lb(3),ub(3)
  do iy = g%lb(2),ub(2)
  do ix = g%lb(1),ub(1)
    sx(ix,iy,iz) = bx(ix,iy,iz)                                                        &
        - cb1 * (                                       &
            (  ay3*(ez(ix,iy+1 ,iz)-ez(ix,iy   ,iz))  ) & ! ddyup(ez)
          - (  az3*(ey(ix,iy,iz+1 )-ey(ix,iy,iz   ))  ))& ! ddzup(ey)
        + cb2 * (                                       &
            (  ay3*(jz(ix,iy+1 ,iz)-jz(ix,iy   ,iz))  ) & ! ddyup(jz)
          - (  az3*(jy(ix,iy,iz+1 )-jy(ix,iy,iz   ))  ))& ! ddzup(jy)
        + cb3 * (                       &
               (+ lpm1 * bx(ix-1,iy,iz) &
                +  lp0 * bx(ix  ,iy,iz) &
                + lpp1 * bx(ix+1,iy,iz) ) * odxq &
             + (+ lpm1 * bx(ix,iy-1,iz) &
                +  lp0 * bx(ix,iy  ,iz) &
                + lpp1 * bx(ix,iy+1,iz) ) * odyq &
             + (+ lpm1 * bx(ix,iy,iz-1) &
                +  lp0 * bx(ix,iy,iz  ) &
                + lpp1 * bx(ix,iy,iz+1) ) * odzq )
  enddo
  enddo
  enddo 
  !sx=sx + (ddxupa(ddxdna(bx)) &
  !      +  ddyupa(ddydna(bx)) &
  !      +  ddzupa(ddzdna(bx)))*cb3

  do iz = g%lb(3),ub(3)
  do iy = g%lb(2),ub(2)
  do ix = g%lb(1),ub(1)
    sy(ix,iy,iz) = by(ix,iy,iz)                          &
        - cb1 * (                                        &
             (  az3*(ex(ix,iy,iz+1 )-ex(ix,iy,iz   ))  ) & ! ddzup(ex)
           - (  ax3*(ez(ix+1 ,iy,iz)-ez(ix   ,iy,iz))  ))& ! ddxup(ez)
        + cb2 * (                                        &
             (  az3*(jx(ix,iy,iz+1 )-jx(ix,iy,iz   ))  ) & ! ddzup(jx)
           - (  ax3*(jz(ix+1 ,iy,iz)-jz(ix   ,iy,iz))  ))& ! ddxup(jz)
        + cb3 * (                       &
               (+ lpm1 * by(ix-1,iy,iz) &
                +  lp0 * by(ix  ,iy,iz) &
                + lpp1 * by(ix+1,iy,iz) ) * odxq &
             + (+ lpm1 * by(ix,iy-1,iz) &
                +  lp0 * by(ix,iy  ,iz) &
                + lpp1 * by(ix,iy+1,iz) ) * odyq &
             + (+ lpm1 * by(ix,iy,iz-1) &
                +  lp0 * by(ix,iy,iz  ) &
                + lpp1 * by(ix,iy,iz+1) ) * odzq )
  enddo
  enddo
  enddo 
  !sy=sy + (ddxupa(ddxdna(by)) &
  !      +  ddyupa(ddydna(by)) &
  !      +  ddzupa(ddzdna(by)))*cb3

  do iz = g%lb(3),ub(3)
  do iy = g%lb(2),ub(2)
  do ix = g%lb(1),ub(1)
    sz(ix,iy,iz) = bz(ix,iy,iz)                                                        &
        - cb1 * (                                       &
             (  ax3*(ey(ix+1 ,iy,iz)-ey(ix   ,iy,iz)) ) & ! ddxdup(ey)
          -  (  ay3*(ex(ix,iy+1 ,iz)-ex(ix,iy   ,iz)) ))& ! ddyup(ex)
        + cb2 * (                                       &
             (  ax3*(jy(ix+1 ,iy,iz)-jy(ix   ,iy,iz)) ) & ! ddxdup(jy)
          -  (  ay3*(jx(ix,iy+1 ,iz)-jx(ix,iy   ,iz)) ))& ! ddyup(jx)
        + cb3 * (                       &
               (+ lpm1 * bz(ix-1,iy,iz) &
                +  lp0 * bz(ix  ,iy,iz) &
                + lpp1 * bz(ix+1,iy,iz) ) * odxq &
             + (+ lpm1 * bz(ix,iy-1,iz) &
                +  lp0 * bz(ix,iy  ,iz) &
                + lpp1 * bz(ix,iy+1,iz) ) * odyq &
             + (+ lpm1 * bz(ix,iy,iz-1) &
                +  lp0 * bz(ix,iy,iz  ) &
                + lpp1 * bz(ix,iy,iz+1) ) * odzq )
  enddo
  enddo
  enddo 
  !sz=sz + (ddxupa(ddxdna(bz)) &
  !      +  ddyupa(ddydna(bz)) &
  !      +  ddzupa(ddzdna(bz)))*cb3

!----------------------------------------------------------------------!
!Find, compute and print information about error level
  do iz=1,g%n(3)
    bxm(:,:,iz) = bx(:,:,iz)**2+by(:,:,iz)**2+bz(:,:,iz)**2
  end do

  b0e   = 0.
  call b_boundaries(sx,sy,sz)
  bmax = sqrt(max4(bxm))
                                                     call dump(sx,'sx')
                                                     call dump(sy,'sy')
                                                     call dump(sz,'sz')
                                                    call dump(bx,'bxa')
                                                    call dump(by,'bya')
                                                    call dump(bz,'bza')

!----------------------------------------------------------------------!
!Memorize 0'th iteration Ba for the loop and error check.
  do iz=1,g%n(3)
  do iy=1,g%n(2)
  do ix=1,g%n(1)
    bxa(ix,iy,iz) = bx(ix,iy,iz)
    bya(ix,iy,iz) = by(ix,iy,iz)
    bza(ix,iy,iz) = bz(ix,iy,iz)
  end do
  end do
  end do
!----------------------------------------------------------------------!
!Now move, NB!  Bza -- e.g. -- is defined on {lb-1:ub},{lb-1:ub},{lb-1:ub+1}

  iter = 0                                                             ! new timestep

SOLVE_LOOP : do while (iter <= maxiter_b)                              ! max maxiter_b 

  do iz = g%lb(3),ub(3)
  do iy = g%lb(2),ub(2)
  do ix = g%lb(1),ub(1)
      bxm(ix,iy,iz)=(sx(ix,iy,iz)                                       &
                                  + c4bx * (+ lpm1 * bxa(ix-1,iy  ,iz  )&
                                            + lpp1 * bxa(ix+1,iy  ,iz  ))&
                                  + c4by * (+ lpm1 * bxa(ix  ,iy-1,iz  )&
                                            + lpp1 * bxa(ix  ,iy+1,iz  ))&
                                  + c4bz * (+ lpm1 * bxa(ix  ,iy  ,iz-1)&
                                            + lpp1 * bxa(ix  ,iy  ,iz+1))&
                                                                          )*denom
  enddo
  enddo
  enddo
  do iz = g%lb(3),ub(3)
  do iy = g%lb(2),ub(2)
  do ix = g%lb(1),ub(1)
      bym(ix,iy,iz)=(sy(ix,iy,iz)                                       &
                                  + c4bx * (+ lpm1 * bya(ix-1,iy  ,iz  )&
                                            + lpp1 * bya(ix+1,iy  ,iz  ))&
                                  + c4by * (+ lpm1 * bya(ix  ,iy-1,iz  )&
                                            + lpp1 * bya(ix  ,iy+1,iz  ))&
                                  + c4bz * (+ lpm1 * bya(ix  ,iy  ,iz-1)&
                                            + lpp1 * bya(ix  ,iy  ,iz+1))&
                                                                          )*denom
  enddo
  enddo
  enddo

  do iz = g%lb(3),ub(3)
  do iy = g%lb(2),ub(2)
  do ix = g%lb(1),ub(1)
      bzm(ix,iy,iz)=(sz(ix,iy,iz)                                       &
                                  + c4bx * (+ lpm1 * bza(ix-1,iy  ,iz  )&
                                            + lpp1 * bza(ix+1,iy  ,iz  ))&
                                  + c4by * (+ lpm1 * bza(ix  ,iy-1,iz  )&
                                            + lpp1 * bza(ix  ,iy+1,iz  ))&
                                  + c4bz * (+ lpm1 * bza(ix  ,iy  ,iz-1)&
                                            + lpp1 * bza(ix  ,iy  ,iz+1))&
                                                                          )*denom
  enddo
  enddo
  enddo

!----------------------------------------------------------------------!
!Supply boundary conditions for B{xyz}m and for B{xyz} - same as above.
  call b_boundaries(bxm,bym,bzm)
                                                  call dump(bxm, 'bxm')
                                                  call dump(bym, 'bym')
                                                  call dump(bzm, 'bzm')
!----------------------------------------------------------------------!
! Produce max error on grid on interior.
  errp = 0. ; maxerr = 0.
  errpp = 0. ; maxerrp = 0.

  do iz = g%lb(3),g%ub(3)-1
  do iy = g%lb(2),g%ub(2)-1
  do ix = g%lb(1),g%ub(1)-1
      errpp = max(errpp,& 
                 abs(bxm(ix,iy,iz)-bxa(ix,iy,iz)),&
                 abs(bym(ix,iy,iz)-bya(ix,iy,iz)),&
                 abs(bzm(ix,iy,iz)-bza(ix,iy,iz))) 
    maxerrp = max(maxerrp,&                                                    
                 abs(bxm(ix,iy,iz)),&
                 abs(bym(ix,iy,iz)),&
                 abs(bzm(ix,iy,iz))) 
  enddo
  enddo
  enddo
  maxerr = max(maxerr,maxerrp)
  errp   = max(errp,errpp)

  maxerr = max(maxerr,1e-34)
  errp = max_scalar(errp)
  maxerr = max_scalar(maxerr)
  errp = errp/maxerr

!----------------------------------------------------------------------!
!Convergence check to not necessarily do max allowed # iterations.
  if (master .and. verbose>1) then 
    write(stdout,*) 'errp,maxerr,iter_b =',rank,errp,maxerr,iter       ! write some diagnostics - on demand
  endif

  iter_b = iter

  if (errp < tol_b) then                                             ! done perhaps?
    if (verbose>0) &
      write (stdout,*) 'Exiting; b_push CONVERGED - step ok.'
    EXIT SOLVE_LOOP                                                  ! exit and continue
  end if

!----------------------------------------------------------------------!
!Memorize last move iteration - same as above.
!We must know Bmem field on: Bxa: {lb-1:ub+1}{lb-1:ub}{lb-1:ub}
!                            Bya: {lb-1:ub}{lb-1:ub+1}{lb-1:ub}
!                            Bza: {lb-1:ub}{lb-1:ub}{lb-1:ub+1}
!to be able to form the operator Nabla**2(Bxa,Bya,Bza).
  do iz=1,g%n(3)
  do iy=1,g%n(2)
  do ix=1,g%n(1)
    bxa(ix,iy,iz) = bxm(ix,iy,iz)
    bya(ix,iy,iz) = bym(ix,iy,iz)
    bza(ix,iy,iz) = bzm(ix,iy,iz)
  end do
  end do
  end do

  iter=iter+1                                                          ! Next iteration if not done yet.

ENDDO SOLVE_LOOP                                                       ! end of loop for solving for LaPlace(B).

  if(errp.ge.tol_b) then                                               ! Warn us if we're not below error tolerance after max iterations.
    call warning('b_push','Max iterations reached, no convergence')
  endif

!----------------------------------------------------------------------!
!Save the B-field before updating B to B(i+1);
! needed in 'e_push' for time-averaging B(i+1)-B(i).

  do iz=1,g%n(3)
  do iy=1,g%n(2)
  do ix=1,g%n(1)
    bxa(ix,iy,iz) = bx(ix,iy,iz)
    bx(ix,iy,iz) = bxm(ix,iy,iz)
    bya(ix,iy,iz) = by(ix,iy,iz)
    by(ix,iy,iz) = bym(ix,iy,iz)
    bza(ix,iy,iz) = bz(ix,iy,iz)
    bz(ix,iy,iz) = bzm(ix,iy,iz)
  end do
  end do
  end do

  call trace_exit('B_PUSH')
END SUBROUTINE b_push_implicit

!----------------------------------------------------------------------!
SUBROUTINE e_push_implicit
  USE params,          ONLY: stdout, master, mpi, do_e_fix, it
  USE grid_m,          ONLY: ex, ey, ez
  USE grid_m,          ONLY: bx, by, bz
  USE grid_m,          ONLY: fskin, fmhd   ! increase dt
  USE maxwell_sources, ONLY: jx, jy, jz    ! current density needed
  USE maxwell_sources, ONLY: bxa,bya,bza   ! last B{t-1} needed
  USE maxwell_sources, ONLY: calc_charge, charge, verbose
  USE dumps,           ONLY: dump

  implicit none
  logical :: convergence=.false.                                          ! e_fix convergence flag
  integer ::  ix, iy, iz, ii
  real    :: de(3), detemp
  real    :: dphi, phitemp, chgmax, a, norm, emax
  real    :: max_scalar, max_dphi
!.......................................................................
  call trace_enter('E_PUSH')
!-----------------------------------------------------------------------
!Now push the whole interior grid.
  ub = merge(g%ub, g%ub-1, mpi%ub)

  do iz=g%lb(3),ub(3)
  do iy=g%lb(2),ub(2)
  do ix=g%lb(1),ub(1)
    ex(ix,iy,iz) = ex(ix,iy,iz)                                           &
                 + ce1 * (emf*(                                           &
                            (  ay3*(bz (ix,iy   ,iz)-bz (ix,iy-1 ,iz)) )  & ! ddydn(bz)
                          - (  az3*(by (ix,iy,iz   )-by (ix,iy,iz-1 )) )) & ! ddzdn(by)
                      + omemf*(                                           &
                            (  ay3*(bza(ix,iy   ,iz)-bza(ix,iy-1 ,iz)) )  & ! ddydn(bza)
                          - (  az3*(bya(ix,iy,iz   )-bya(ix,iy,iz-1 ))))) & ! ddzdn(bya)
                 - ce2 * jx(ix,iy,iz)
  enddo
  enddo
  enddo

  do iz=g%lb(3),ub(3)
  do iy=g%lb(2),ub(2)
  do ix=g%lb(1),ub(1)
    ey(ix,iy,iz) = ey(ix,iy,iz)                                           &
                 + ce1 * (emf*(                                           &
                            (  az3*(bx (ix,iy,iz   )-bx (ix,iy,iz-1 )) )  & ! ddzdn(bx)
                          - (  ax3*(bz (ix   ,iy,iz)-bz (ix-1 ,iy,iz)) )) & ! ddxdn(bz)
                      + omemf*(                                           &
                            (  az3*(bxa(ix,iy,iz   )-bxa(ix,iy,iz-1 )) )  & ! ddzdn(bxa)
                          - (  ax3*(bza(ix   ,iy,iz)-bza(ix-1 ,iy,iz))))) & ! ddxdn(bza)
                 - ce2 * jy(ix,iy,iz)
  enddo
  enddo
  enddo

  do iz=g%lb(3),ub(3)
  do iy=g%lb(2),ub(2)
  do ix=g%lb(1),ub(1)
    ez(ix,iy,iz) = ez(ix,iy,iz)                                           &
                 + ce1 * (emf*(                                           &
                            (  ax3*(by (ix   ,iy,iz)-by (ix-1 ,iy,iz)) )  & ! ddxdn(by)
                          - (  ay3*(bx (ix,iy   ,iz)-bx (ix,iy-1 ,iz)) )) & ! ddydn(bx)
                      + omemf*(                                           &
                            (  ax3*(bya(ix   ,iy,iz)-bya(ix-1 ,iy,iz)) )  & ! ddxdn(bya)
                          - (  ay3*(bxa(ix,iy   ,iz)-bxa(ix,iy-1 ,iz))))) & ! ddydn(bxa)
                 - ce2 * jz(ix,iy,iz)
  enddo
  enddo
  enddo
  if (fskin > 0.0) then
    do iz=g%lb(3),ub(3); do iy=g%lb(2),ub(2); do ix=g%lb(1),ub(1)
      ex(ix,iy,iz) = ex(ix,iy,iz)*fmhd(ix,iy,iz)
      ey(ix,iy,iz) = ey(ix,iy,iz)*fmhd(ix,iy,iz)
      ez(ix,iy,iz) = ez(ix,iy,iz)*fmhd(ix,iy,iz)
    enddo; enddo; enddo
  endif

!----------------------------------------------------------------------!
!Set boundary components of e-field (compare with 'e_fix')
  call e_boundaries(ex,ey,ez)
                                                     call dump(ex,'ex')
                                                     call dump(ey,'ey')
                                                     call dump(ez,'ez')
!  call filter_it (ex)
!  call filter_it (ey)
!  call filter_it (ez)
!  call e_boundaries(ex,ey,ez)
!                                                    call dump(ex,'Ex2')
!                                                    call dump(ey,'Ey2')
!                                                    call dump(ez,'Ez2')

!----------------------------------------------------------------------!
! find new charge density and align charges and electric field
  if (do_e_fix .and. mod(it,every_e)==0) then
    call calc_charge
    call e_fix (ex, ey, ez, charge)
  else
    iter_e = 0
  endif
  call trace_exit('E_PUSH')

END SUBROUTINE e_push_implicit                                         ! done with the electric field.
!----------------------------------------------------------------------!

SUBROUTINE e_fix (ex, ey, ez, charge)
  USE params,               ONLY : stdout, rank, time, stdall, master, mpi
  USE maxwell_sources,      ONLY : phi=>bxa, phif=>bya, verbose
  USE dumps,                ONLY : dump
  USE grid_m,               ONLY : fmhd
!!#ifdef _OPENMP
!!  USE omp_lib
!!#endif

  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: ex, ey, ez, charge
  logical :: convergence,inside                                        ! e_fix convergence flag
  integer ::  ix, iy, iz, ii
  real    :: dex, dey, dez, dexp, deyp, dezp, detemp
  real    :: dphi, phitemp, chgmax, a, norm, emax, dphip, chgmaxp, emaxp
  real    :: max_scalar, max_dphi

  call trace_enter('E_FIX')


  convergence=.false.; ii=0; dex=0.; dey=0.; dez=0.; detemp=0.         ! zap loop variables

  lb = merge(g%lb+1, g%lb, mpi%lb)                                     ! proper boundaries

!----------------------------------------------------------------------!
!NORMALIZE FILTER SO 3-D CHECKERBOARD IS THE SAME AFTER FILTERING
  a = filter/3.
  norm = 1./(1.+(3*lp0-3*lpp1-3*lpm1 - 12.*f_2nd)*a)
  if (norm .lt. 0. .and. master) then
    print *, 'Normalisation :', norm
    print *, 'Filter        :', filter
    print *, '2nd order part:', f_2nd
    print *, '4th order part:', f_6th
    print *, 'Max filter    :', abs(3. / (3*lp0-3*lpp1-3*lpm1 - 12*f_2nd))
    call error('e_fix','Your filter value is too high. Normalisation becoming negative')
  endif

  dphi = 0. ; emax = 0. ; chgmax = 0.
MAIN_LOOP : do while(ii .le. maxiter_e)                                ! loop only maxiter_e times.

!----------------------------------------------------------------------!
! Find 'source' term; PHI = div(E) - rho/epsilon0 on INTERIOR.
  dphip = 0. ; emaxp = 0. ; chgmaxp = 0.

  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    phitemp =      (  (  ax3*(ex(ix   ,iy,iz)-ex(ix-1 ,iy,iz)))&
                    + (  ay3*(ey(ix,iy   ,iz)-ey(ix,iy-1 ,iz)))&
                    + (  az3*(ez(ix,iy,iz   )-ez(ix,iy,iz-1 )))&
                    -   (ce3*fmhd(ix,iy,iz)*charge(ix,iy,iz))   )
    inside  = ix < g%ub(1) .and. iy < g%ub(2) .and. iz < g%ub(3)
    emaxp    = merge(max(emaxp,abs(ex(ix,iy,iz)),abs(ey(ix,iy,iz)),abs(ez(ix,iy,iz))),emaxp,inside)
    chgmaxp  = merge(max(chgmaxp,abs(ce3*charge(ix,iy,iz))),chgmaxp,inside)
    dphip    = merge(max(dphip,abs(phitemp)),dphip,inside)
    phi(ix,iy,iz) = phitemp*dd1e
  enddo
  enddo
  enddo
  emax = max(emaxp,emax)
  chgmax = max(chgmaxp,chgmax)
  dphi = max(dphip,dphi)

  chgmax = max(chgmax,emax/minval(g%ds))
  chgmax = max_scalar(chgmax)
  if (chgmax > 0.) dphi = dphi/chgmax

  if (maxiter_e==miniter_e) then
    max_dphi = max_scalar(dphi)
    if (master) write (stdout,*) ' dphi, tol_e, dd1e : ', max_dphi, tol_e, dd1e, dphi*dd1e
    convergence=.true.
    exit MAIN_LOOP
  endif

  max_dphi = max_scalar(dphi)
  call phi_boundaries(phi)
                                                   call dump(phi,'phi')

!----------------------------------------------------------------------!
!FILTER POISSON RESIDUAL (using bya as buffer - phif => bya above). 
if (filter > 0.) then
  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    phif(ix,iy,iz) = (1.+(3.*lp0 - 6.*f_2nd)*a) * phi(ix,iy,iz)                    &
                                         + a * ((lpm1+f_2nd) * phi(ix-1,iy  ,iz  ) &
                                              + (lpp1+f_2nd) * phi(ix+1,iy  ,iz  ))&
                                         + a * ((lpm1+f_2nd) * phi(ix  ,iy-1,iz  ) &
                                              + (lpp1+f_2nd) * phi(ix  ,iy+1,iz  ))&
                                         + a * ((lpm1+f_2nd) * phi(ix  ,iy  ,iz-1) &
                                              + (lpp1+f_2nd) * phi(ix  ,iy  ,iz+1))
  enddo
  enddo
  enddo
  
  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    phi(ix,iy,iz) = phif(ix,iy,iz)*norm
  enddo
  enddo
  enddo

  !--------------------------------------------------------------------!
  !SET BOUNDARY COMPONENTS OF PHI AGAIN
  call phi_boundaries(phi)
                                                   call dump(phi,'phi')

end if

!----------------------------------------------------------------------!
!NOW CORRECT E-FIELD, next few lines must be optimized (use temp array?)
  dex=0.; dey=0.; dez=0.
  dexp=0.; deyp=0.; dezp=0.

  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    detemp = (  ax3*(phi(ix+1 ,iy,iz)-phi(ix   ,iy,iz)) )    ! ddxdup(phi)
    dexp = max(dexp,abs(detemp))
    ex(ix,iy,iz) = ex(ix,iy,iz) + detemp
  enddo
  enddo
  enddo
  dex = max(dex,dexp)

  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    detemp = (  ay3*(phi(ix,iy+1 ,iz)-phi(ix,iy   ,iz)) )    ! ddyup(phi)
    deyp = max(deyp,abs(detemp))
    ey(ix,iy,iz) = ey(ix,iy,iz) + detemp
  enddo
  enddo
  enddo
  dey = max(dey,deyp)

  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    detemp = (  az3*(phi(ix,iy,iz+1 )-phi(ix,iy,iz   )) )    ! ddzup(phi)
    dezp = max(dezp,abs(detemp))
    ez(ix,iy,iz) = ez(ix,iy,iz) + detemp
  enddo
  enddo
  enddo
  dez = max(dez,dezp)

!----------------------------------------------------------------------!
!SET BOUNDARY COMPONENTS OF E-FIELD
  call e_boundaries(ex,ey,ez)

!----------------------------------------------------------------------!
! Output some stats
  iter_e = ii                                                          ! for stats only
  if (master .and. verbose > 1) write(stdout,*) 'max_dphi,iter_e =', &
                                                 max_dphi,iter_e
  if (master .and. verbose > 2) write(stdall,*) 'rank=',    rank,    &
                                                'tol_e=',   tol_e,   &
                                                'max_dphi=',max_dphi,&
                                                'dphi=',    dphi,    &
                                                'de(:)=',   dex,dey,dez, &
                                                'iter=',    ii

!----------------------------------------------------------------------!
!Error check part #1
  if((max_dphi >= tol_e) .and. (ii >= maxiter_e)) then                 !  bail out.
    if (master) then
      write (stdout,*) 'Exiting e_fix w/NO CONVERGENCE maxiter reached.'
      write (stdout,*) ' dphi, de, max(de(:))   : ', &
                         dphi, dex, dey, dez, maxval((/dex,dey,dez/))
      write (stdout,*) ' max_dphi, tol_e    : ', max_dphi, tol_e
      write (stdout,*) ' abs(dphi)          : ', abs(dphi)
      write (stdout,*) ' '
    end if
    convergence=.false.
    exit MAIN_LOOP
  else if ((max_dphi <= tol_e) .and. (ii >= miniter_e)) then           ! OK, exit.
    convergence=.true.
    exit MAIN_LOOP
  endif
  dphi = 0. ; emax = 0. ; chgmax = 0.

  ii=ii+1                                                              ! Next iteration.
!----------------------------------------------------------------------!
ENDDO MAIN_LOOP
                                                     call dump(ex,'Ex')
                                                     call dump(ey,'Ey')
                                                     call dump(ez,'Ez')
!----------------------------------------------------------------------!
!Error check part #2 
   if (convergence .and. verbose>0 .and. stdout>= 0) then               ! Print stats for 'good solve' conditions.
    write (stdout,*) ' '
    write (stdout,*) 'Exiting; e_fix CONVERGED - step ok.'
    write (stdout,*) ' iterations         : ', ii
    write (stdout,*) ' dphi, max(de(:))   : ', dphi, maxval((/dex,dey,dez/))
    write (stdout,*) ' max_dphi, tol_e    : ', max_dphi, tol_e
    write (stdout,*) ' abs(dphi)          : ', abs(dphi)
   endif

  if (verbose>1) write(stdall,*) 'exit',rank,max_dphi,iter_e
  call trace_exit('E_FIX')
!-----------------------------------------------------------------------
END SUBROUTINE e_fix

!-----------------------------------------------------------------------
SUBROUTINE b_fix (bx, by, bz)
  USE params,               ONLY : stdout, rank, time, stdall, mpi, master
  USE maxwell_sources,      ONLY : phi=>sx, phif=>sy 
  USE maxwell_sources,      ONLY : verbose
  USE dumps,                ONLY : dump
  USE grid_m,               ONLY : g

  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)):: bx, by, bz

  logical :: convergence=.false.
  integer :: ix, iy, iz, ii
  real    :: dex,dey,dez, detemp
  real    :: dphi, phitemp, a, norm, bmax, bmaxp, dphip
  real    :: max_scalar, max_dphi

  call trace_enter('B_FIX')

!----------------------------------------------------------------------!
  convergence=.false.; ii=0; dex=0.; dey=0.; dez=0.                    ! zap various loop variables
  a = filter/3.
  norm = 1./(1.-12.*a)
  lb = merge(g%lb+1, g%lb, mpi%lb)
  dphi = 0. ; bmax = 0.

MAIN_LOOP : do while(ii < maxiter_b)                                   ! loop only maxiter_b times.

!----------------------------------------------------------------------!
!Find 'source' term; PHI = div(B) 
  dphip = 0. ; bmaxp = 0.
  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    phitemp = dd1b*(  (  ax3*(bx(ix+1 ,iy,iz)-bx(ix   ,iy,iz)))&
                    + (  ay3*(by(ix,iy+1 ,iz)-by(ix,iy   ,iz)))&
                    + (  az3*(bz(ix,iy,iz+1 )-bz(ix,iy,iz   ))) )
    bmaxp = max(bmaxp,abs(bx(ix,iy,iz)),abs(by(ix,iy,iz)),abs(bz(ix,iy,iz)))
    dphip = max(dphip,abs(phitemp))
    phi(ix,iy,iz) = phitemp
  enddo
  enddo
  enddo
  bmax = max(bmax,bmaxp)
  dphi = max(dphi,dphip)

  call phib_boundaries(phi)
                                                 call dump(phi,'phiB1')
  bmax = bmax/minval(g%ds)
  bmax = max_scalar(bmax)

  if (bmax > 0) dphi = dphi/bmax
  max_dphi = max_scalar(dphi)

!----------------------------------------------------------------------!
!FILTER POISSON RESIDUAL (using bxh as buffer). 
if (filter > 0.) then
  do iz=lb(3), g%ub(3)-1
  do iy=lb(2), g%ub(2)-1
  do ix=lb(1), g%ub(1)-1
    phif(ix,iy,iz) = (1.-6.*a) * phi(ix,iy,iz) + a * (  lpm1 * phi(ix-1,iy,iz) &
                                                      + lpp1 * phi(ix+1,iy,iz))&
                                               + a * (  lpm1 * phi(ix,iy-1,iz) &
                                                      + lpp1 * phi(ix,iy+1,iz))&
                                               + a * (  lpm1 * phi(ix,iy,iz-1) &
                                                      + lpp1 * phi(ix,iy,iz+1))
  enddo
  enddo
  enddo

!----------------------------------------------------------------------!
!NORMALIZE FILTER SO 3-D CHECKERBOARD IS THE SAME AFTER FILTERING
  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    phi(ix,iy,iz) = phif(ix,iy,iz)*norm
  enddo
  enddo
  enddo
!----------------------------------------------------------------------!
!SET BOUNDARY COMPONENTS OF PHI AGAIN
  call phib_boundaries(phi)
                                                 call dump(phi,'phiB2')

end if

!----------------------------------------------------------------------!
!NOW CORRECT B-FIELD 

  dex=0.; dey=0.; dez=0.

  do iz=lb(3),g%ub(3)-1
  do iy=lb(2),g%ub(2)-1
  do ix=lb(1),g%ub(1)-1
    ! ddxdn(phi)
    detemp = ax3*(phi(ix   ,iy,iz)-phi(ix-1 ,iy,iz))
    dex = max(dex,abs(detemp))
    bx(ix,iy,iz) = bx(ix,iy,iz) + detemp
    ! ddydn(phi)
    detemp = ay3*(phi(ix,iy   ,iz)-phi(ix,iy-1 ,iz))
    dey = max(dey,abs(detemp))
    by(ix,iy,iz) = by(ix,iy,iz) + detemp
    ! ddzdn(phi)
    detemp = az3*(phi(ix,iy,iz   )-phi(ix,iy,iz-1 ))
    dez = max(dez,abs(detemp))
    bz(ix,iy,iz) = bz(ix,iy,iz) + detemp
  enddo
  enddo
  enddo

!----------------------------------------------------------------------!
!SET BOUNDARY COMPONENTS OF B-FIELD
  call b_boundaries(bx,by,bz)
                                                    call dump(bx,'bxc')
                                                    call dump(by,'byc')
                                                    call dump(bz,'bzc')
  iter_divb = ii                                                         ! increase iteration stats counter, for stats only.
  if (master .and. verbose > 1) write(stdout,*) 'max_dphi,iter_divb =',max_dphi,iter_divb
  if (master .and. verbose > 2) write(stdall,*) 'rank=',    rank,    &
                                'tol_divb=',tol_divb,&
                                'max_dphi=',max_dphi,&
                                'dphi=',    dphi,    &
                                'de(:)=',   dex,dey,dez,  &
                                'iter=',    ii

!-----------------------------------------------------------------------
!Error check part #1
  if((max_dphi >= tol_divb).and.(ii >= maxiter_divb).and.(stdout >= 0)) then  ! Something is rotten - no/too slow convergence; bail out.
    write (stdout,*) ' '
    write (stdout,*) 'Exiting b_fix w/NO CONVERGENCE maxiter reached.'
    write (stdout,*) ' dphi, max(de(:))   : ', dphi, maxval((/dex,dey,dez/))
    write (stdout,*) ' max_dphi, tol_divb : ', max_dphi, tol_divb
    write (stdout,*) ' abs(dphi)          : ', abs(dphi)
    convergence=.false.
    exit MAIN_LOOP
  else if((max_dphi.le.tol_divb).and.(ii.ge.miniter_divb)) then        ! Everything is OK - convergence in due time; set flag and exit.
    convergence=.true.
    exit MAIN_LOOP
  endif
  dphi = 0. ; bmax = 0.

  ii=ii+1                                                              ! Next iteration.
!----------------------------------------------------------------------!
enddo MAIN_LOOP
                                                     call dump(bx,'Bx')
                                                     call dump(by,'By')
                                                     call dump(bz,'Bz')

!----------------------------------------------------------------------!
!Error check part #2 
  if (master) then
    if (.not. convergence) then
     write (stdout,*) ' '
     write (stdout,*) 'Exiting; b_fix DID NOT CONVERGE!'
      write (stdout,*) ' iterations         : ', ii
      write (stdout,*) ' dphi, max(de(:))   : ', dphi, maxval((/dex,dey,dez/))
      write (stdout,*) ' max_dphi, tol_divb : ', max_dphi, tol_divb
      write (stdout,*) ' abs(dphi)          : ', abs(dphi)
    else if (verbose>0) then
      write (stdout,*) 'Exiting; b_fix CONVERGED - step ok.'
    end if
  end if

!----------------------------------------------------------------------!
  if (verbose>1) write(stdall,*) 'exit',rank,max_dphi,iter_b
  call trace_exit('B_FIX')

!----------------------------------------------------------------------!
END SUBROUTINE b_fix

!----------------------------------------------------------------------!
END MODULE maxwell_solver

!----------------------------------------------------------------------!
SUBROUTINE read_maxwell_solver
  USE params,          only: master, trace, stdin, stdout, params_unit, &
                             out_namelists, do_e_fix, do_b_fix
  USE grid_m,          only: g
  USE units,           only: c
  USE maxwell_sources, only: verbose
  USE energy_filter_m, only: e_filter
  USE maxwell_solver
  implicit none
  namelist /maxwell/ sor, filter, f_2nd, f_6th, emf, maxiter_b, tol_b, &
    miniter_e, maxiter_e, tol_e, miniter_divb, maxiter_divb, tol_divb, &
    verbose, do_b_fix, do_e_fix, every_e, every_b, do_explicit, e_filter
!.......................................................................!  
  rewind (stdin);    read  (stdin,maxwell)                              ! read params
  if (master)        write (params_unit,maxwell)
  if (sor == 0.) sor = 2./(1.+c%pi/maxval(g%n))
  if (out_namelists) write (stdout,maxwell)
END SUBROUTINE read_maxwell_solver

!-----------------------------------------------------------------------
SUBROUTINE init_maxwell
  USE params,     only : master, trace, stdin, stdout, mid, do_b_fix, &
    do_e_fix, mpi, do_picmhd
  USE maxwell_sources
  USE dumps,      only : dump, dump_set
  USE maxwell_solver
  USE grid_m, only : nx,ny,nz,ex,ey,ez,bx,by,bz
  implicit none  
  character(len=mid) :: id = &
    'Fields/maxwell_solver.f90 $Id$'
  real           :: dtsave
  real, external :: max4
  real, dimension(nx,ny,nz) :: scr
  integer :: ix, iy, iz
  logical :: problems
  integer :: maxprint
!.......................................................................
  call print_id(id)
  call trace_enter('INIT_MAXWELL')
  call dump_set('init_maxwell')

  !---------------------------------------------------------------------
  ! As per the IDL test, use no over-relaxation when using filter
  emf          = 0.55
  filter       = 0.105
  f_2nd        = 0.6
  f_6th        = 0.    ! FIXME, f_6th is not implemented in e_fix yet
  sor          = 0.65
  miniter_divb = 1
  miniter_e    = 1
  every_b      = 100
  every_e      = 1
  maxiter_b    = 400
  maxiter_divb = 400
  maxiter_e    = 400
  tol_b        = 1.e-5
  tol_e        = 1.e-4                                                   
  ! FIXME: are tol_e, tol_b affected by non-trivial unit scaling?
  tol_divb     = 1.e-5                                                   
  verbose      = 0
  do_b_fix     = .false.
  do_e_fix     = .false.
  do_explicit  = .false.

  call read_maxwell_solver

  if (do_picmhd) call init_picmhd

  !---------------------------------------------------------------------
  ! use over-relaxation if not using filtering
  if (filter==0.) then
    sor = 2./(1.+c%pi/maxval(g%n)) 
  endif

  !---------------------------------------------------------------------
  ! Allocate arrays, and call sort to count particles per cell
  call allocate_sources
  call sort

  !---------------------------------------------------------------------
  ! Make sure initial state has proper boundary zones
  call e_boundaries (ex, ey, ez)
  call b_boundaries (bx, by, bz)

  !---------------------------------------------------------------------!
  ! Call Maxwell with dt=0, to get bmax, %d, %v + possibly clean E and B
  dtsave = dt
  dt = 0.0
  call solve_maxwell
  dt = dtsave
  
  call trace_exit('INIT_MAXWELL_SOLVER')
END SUBROUTINE init_maxwell

!-----------------------------------------------------------------------
SUBROUTINE init_maxwell_solver_restart
  USE params,          only : master, trace, stdin, stdout 
  USE dumps,           only : dump, dump_set
  USE grid_m,          only : ex, ey, ez, bx, by, bz
  USE maxwell_sources, only : bxa, bya, bza, bmax
  USE maxwell_solver,  only : order
  implicit none  

  real,            external :: max4
  real                      :: dtsave

  call trace_enter('INIT_MAXWELL_SOLVER_RESTART')

  !---------------------------------------------------------------------
  call dump_set('init_maxwell_restart')
              call dump(bx,'Bx')
              call dump(by,'By')
              call dump(bz,'Bz')

  call overlap(bx)                                                      ! Make sure overlaps are ok
  call overlap(by)
  call overlap(bz)
  call overlap(ex)
  call overlap(ey)
  call overlap(ez)
  
  bxa = bx**2+by**2+bz**2
  bmax = sqrt(max4(bxa))                                                ! Get max B-field for set_dt

  call compute_prefactors()                                             ! compute prefactors here -- they might exhibit time variation
  
  call trace_exit('INIT_MAXWELL_SOLVER_RESTART')
END SUBROUTINE init_maxwell_solver_restart

!-----------------------------------------------------------------------
SUBROUTINE solve_maxwell
  USE params,          only: do_maxwell, dt, do_picmhd
  USE grid_m,          only: g, bx, by, bz, ex, ey, ez
  USE dumps,           only: dump, dump_set
  USE units,           only: c
  USE maxwell_solver,  only: e_push, b_push
  USE maxwell_sources, only: calc_current
  USE energy_filter_m
  implicit none
  real a
!.......................................................................
  if (.not. do_maxwell) return
  call trace_enter('SOLVE_MAXWELL')
  if (dt > 0.0) call dump_set('maxwell')

  call compute_prefactors                                               ! may vary
  call mesh_sources
  call calc_current

  if (do_picmhd) then
    call solve_picmhd
  else
    call b_push
    call e_push
 !call damping                                                          ! apply damping 
 
    if (e_filter > 0.0) then
      a = e_filter*c%c*dt/maxval(g%ds)
      call energy_filter (bx, by, bz, ex, ey, ez, a)
    endif
  endif
  call trace_exit('SOLVE_MAXWELL')
END SUBROUTINE solve_maxwell

!-----------------------------------------------------------------------!
! compute prefactors, they depend on order in finite differences
SUBROUTINE compute_prefactors                                           
  USE grid_m,         only : g, odxq, odyq, odzq
  USE params,         only : dt, it, mdim
  USE units,          only : c, elm
  USE maxwell_solver, only : ce1,  ce2,  ce3,                    &
                             cb1,  cb2,  cb3,                    &
                             c4bx, c4by, c4bz,                   &
                             lp0, maxwell_stagger_prefactors,    &
                             emf, omemf, dd1e, dd1b, denom, sor, &
                             order, f_2nd, f_6th
  implicit none
  integer :: test
!.......................................................................!
  call maxwell_stagger_prefactors
  
  omemf = 1.-emf
  ce1 = (elm%ke/elm%kb)*dt                                              ! for dE/dt, rotB & J
  ce2 = c%fourpi*elm%ke*dt                                              ! for J term only
  ce3   = c%fourpi*elm%ke                                               ! for phi in MAIN_LOOP

  cb1   = (1./elm%kf)*dt                                                ! for rot(E)
  cb2   = c%fourpi*emf*(elm%ke/elm%kf)*dt**2                            ! for rot(J), note four pi.
  cb3   = emf*omemf*(elm%ke/(elm%kf*elm%kb))*dt**2                      ! for laplace(B^{n})

  c4bx  = emf**2*dt**2*odxq*c%c2                                        ! for laplace(B^{n+1})
  c4by  = emf**2*dt**2*odyq*c%c2                                        ! for laplace(B^{n+1})
  c4bz  = emf**2*dt**2*odzq*c%c2                                        ! for laplace(B^{n+1})
  denom = 1./(1.-(lp0*(c4bx + c4by + c4bz)))                            ! for laplace, 6th order
  
  dd1e  = (1./(6. + 4.*abs(f_2nd)))*(1./(odxq+odyq+odzq))*sor           ! prefactor for the e_fix routine
  dd1b  = (1./4.)*(1./(3.*max(odxq,odyq,odzq)))*sor                     ! prefactor for the b_fix routine

END SUBROUTINE compute_prefactors

!-----------------------------------------------------------------------
! 7pt 6th order Laplace operator, for filtering high-k
SUBROUTINE filter_it (f)
  USE grid_m, only: g,nx,ny,nz
  USE units,  only: c
  USE params, only: dt
  USE maxwell_solver, only: f_6th
  implicit none
  real f(nx,ny,nz)
  integer ix,iy,iz
  real a0,b0,c0,d0,d
!.......................................................................
  if (f_6th <= 0) return
  d = f_6th*dt*c%c/(720.*minval(g%ds)**2)  !FIXME: minval(g%ds) is for testing only
  d0=d; c0=-d*6.; b0=d*15.; a0=-d*20.
  do iz=g%lb(3),g%ub(3)-1
  do iy=g%lb(2),g%ub(2)-1
  do ix=g%lb(1),g%ub(1)-1
    f(ix,iy,iz) = f(ix,iy,iz) &
       + d0*(f(ix-3,iy,iz)+f(ix+3,iy,iz)+ &
            f(ix,iy-3,iz)+f(ix,iy+3,iz)+ &
            f(ix,iy,iz-3)+f(ix,iy,iz+3)) &
       + c0*(f(ix-2,iy,iz)+f(ix+2,iy,iz)+ &
            f(ix,iy-2,iz)+f(ix,iy+2,iz)+ &
            f(ix,iy,iz-2)+f(ix,iy,iz+2)) &
       + b0*(f(ix-1,iy,iz)+f(ix+1,iy,iz)+ &
            f(ix,iy-1,iz)+f(ix,iy+1,iz)+ &
            f(ix,iy,iz-1)+f(ix,iy,iz+1)) &
       + 3.*a0*f(ix,iy,iz)
  enddo
  enddo
  enddo
END SUBROUTINE filter_it

!=======================================================================
! The boundaries zones needed by this field method
!=======================================================================
SUBROUTINE field_solver_boundaries    
  USE grid_m, only : g
  implicit none
  g%lb = max(g%lb, 4)                                                   ! lower field bndry
  g%ub = max(g%ub, 2)                                                   ! upper bndry (n-ub)
END SUBROUTINE

!=======================================================================
! Return iterations used (to avoid a module dependency)
!=======================================================================
SUBROUTINE maxwell_iterations (n_b, n_e)
  USE maxwell_solver, only: iter_b, iter_e
  implicit none
  integer n_b, n_e
  n_b = iter_b
  n_e = iter_e
END SUBROUTINE
