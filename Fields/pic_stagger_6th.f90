! $Id$
! vim: nowrap
!----------------------------------------------------------------------
MODULE pic_stagger
  USE params, only: mpi
  USE grid_m, only: g
  implicit none

  real    :: aa, bb, cc                                                 ! stagger operator prefactors
  real    :: a1
  real    :: ax1, ay1, az1                                              ! do.
  real    :: ax3, ay3, az3                                              ! do.
  real    :: bx3, by3, bz3                                              ! do.
  real    :: cx3, cy3, cz3                                              ! do.
  real    :: lp0, lpm1, lpm2, lpm3, lpp1, lpp2, lpp3                    ! LaPlace prefactors

  integer                  :: order=5                                   ! first order for now - default

CONTAINS
!----------------------------------------------------------------------
FUNCTION xup (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: xup
  integer                                            :: i, j, k

     xup = (  aa*(f(i+1,j,k)+f(i  ,j,k)) &
            + bb*(f(i+2,j,k)+f(i-1,j,k)) &
            + cc*(f(i+3,j,k)+f(i-2,j,k))  )
END FUNCTION xup
!----------------------------------------------------------------------
FUNCTION yup (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: yup
  integer                                            :: i, j, k

     yup = (  aa*(f(i,j+1,k)+f(i,j  ,k)) &
            + bb*(f(i,j+2,k)+f(i,j-1,k)) &
            + cc*(f(i,j+3,k)+f(i,j-2,k))  )
END FUNCTION yup
!----------------------------------------------------------------------
FUNCTION zup (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: zup
  integer                                            :: i, j, k

     zup = (  aa*(f(i,j,k+1)+f(i,j,k  )) &
            + bb*(f(i,j,k+2)+f(i,j,k-1)) &
            + cc*(f(i,j,k+3)+f(i,j,k-2))  )
END FUNCTION zup
!----------------------------------------------------------------------
FUNCTION xdn (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: xdn
  integer                                            :: i, j, k

     xdn = (  aa*(f(i-1,j,k)+f(i  ,j,k)) &
            + bb*(f(i-2,j,k)+f(i+1,j,k)) &
            + cc*(f(i-3,j,k)+f(i+2,j,k))  )
END FUNCTION xdn
!----------------------------------------------------------------------
FUNCTION ydn (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ydn
  integer                                            :: i, j, k

     ydn = (  aa*(f(i,j-1,k)+f(i,j  ,k)) &
            + bb*(f(i,j-2,k)+f(i,j+1,k)) &
            + cc*(f(i,j-3,k)+f(i,j+2,k))  )
END FUNCTION ydn
!----------------------------------------------------------------------
FUNCTION zdn (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: zdn
  integer                                            :: i, j, k

     zdn = (  aa*(f(i,j,k-1)+f(i,j,k  )) &
            + bb*(f(i,j,k-2)+f(i,j,k+1)) &
            + cc*(f(i,j,k-3)+f(i,j,k+2))  )
END FUNCTION zdn
!----------------------------------------------------------------------
FUNCTION ddxup (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddxup
  integer                                            :: i, j, k

     ddxup = (  ax3*(f(i+1 ,j,k)-f(i   ,j,k)) &
              + bx3*(f(i+2 ,j,k)-f(i-1 ,j,k)) &
              + cx3*(f(i+3 ,j,k)-f(i-2 ,j,k))  )
END FUNCTION ddxup
!-----------------------------------------------------------------------
FUNCTION ddxdn(f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddxdn
  integer                                            :: mx, my, mz
  integer                                            :: i, j, k

      ddxdn = (  ax3*(f(i   ,j,k)-f(i-1 ,j,k)) &
               + bx3*(f(i+1 ,j,k)-f(i-2 ,j,k)) &
               + cx3*(f(i+2 ,j,k)-f(i-3 ,j,k))  )
END FUNCTION ddxdn
!-----------------------------------------------------------------------
FUNCTION ddyup (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddyup
  integer                                            :: i, j, k

    ddyup = (  ay3*(f(i,j+1 ,k)-f(i,j   ,k)) &
             + by3*(f(i,j+2 ,k)-f(i,j-1 ,k)) &
             + cy3*(f(i,j+3 ,k)-f(i,j-2 ,k))  )
END FUNCTION ddyup
!-----------------------------------------------------------------------
FUNCTION ddydn (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddydn
  integer                                            :: i, j, k

      ddydn = (  ay3*(f(i,j   ,k)-f(i,j-1 ,k)) &
               + by3*(f(i,j+1 ,k)-f(i,j-2 ,k)) &
               + cy3*(f(i,j+2 ,k)-f(i,j-3 ,k)))
END FUNCTION ddydn
!-----------------------------------------------------------------------
FUNCTION ddzup (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddzup
  integer                                            :: i, j, k

    ddzup  = (  az3*(f(i,j,k+1 )-f(i,j,k   )) &
              + bz3*(f(i,j,k+2 )-f(i,j,k-1 )) &
              + cz3*(f(i,j,k+3 )-f(i,j,k-2 ))  )
END FUNCTION ddzup
!-----------------------------------------------------------------------
FUNCTION ddzdn (f,i,j,k)
  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in)  :: f
  real                                               :: ddzdn
  integer                                            :: i, j, k

    ddzdn  = (  az3*(f(i,j,k   )-f(i,j,k-1 )) &
              + bz3*(f(i,j,k+1 )-f(i,j,k-2 )) &
              + cz3*(f(i,j,k+2 )-f(i,j,k-3 ))  )
END FUNCTION ddzdn
!-----------------------------------------------------------------------
FUNCTION ddxdna(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(1) = max(lb(1),4)
  ub(1) = min(ub(1),g%n(1)-2)
  !$omp parallel do private(i,j,k)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddxdn(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddxdna
!-----------------------------------------------------------------------
FUNCTION xupa(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(1) = max(lb(1),3)
  ub(1) = min(ub(1),g%n(1)-3)
  !$omp parallel do private(i,j,k)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = xup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION xupa
!-----------------------------------------------------------------------
FUNCTION ddxupa(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(1) = max(lb(1),3)
  ub(1) = min(ub(1),g%n(1)-3)
  !$omp parallel do private(i,j,k)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddxup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddxupa
!-----------------------------------------------------------------------
FUNCTION ddydna(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(2) = max(lb(2),4)
  ub(2) = min(ub(2),g%n(2)-2)
  !$omp parallel do private(i,j,k)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddydn(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddydna
!-----------------------------------------------------------------------
FUNCTION yupa(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(2) = max(lb(2),3)
  ub(2) = min(ub(2),g%n(2)-3)
  !$omp parallel do private(i,j,k)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = yup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION yupa
!-----------------------------------------------------------------------
FUNCTION ddyupa(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(2) = max(lb(2),3)
  ub(2) = min(ub(2),g%n(2)-3)
  !$omp parallel do private(i,j,k)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddyup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddyupa
!-----------------------------------------------------------------------
FUNCTION ddzdna(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(3) = max(lb(3),4)
  ub(3) = min(ub(3),g%n(3)-2)
  !$omp parallel do private(i,j,k)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddzdn(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddzdna
!-----------------------------------------------------------------------
FUNCTION zupa(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(3) = max(lb(3),3)
  ub(3) = min(ub(3),g%n(3)-3)
  !$omp parallel do private(i,j,k)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = zup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION zupa
!-----------------------------------------------------------------------
FUNCTION ddzupa(f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,lb(3),ub(3)
  lb = merge(1  , g%lb  , mpi%lb)
  ub = merge(g%n, g%ub-1, mpi%ub)
  lb(3) = max(lb(3),3)
  ub(3) = min(ub(3),g%n(3)-3)
  !$omp parallel do private(i,j,k)
  do k=lb(3),ub(3); do j=lb(2),ub(2); do i=lb(1),ub(1)
    h(i,j,k) = ddzup(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION ddzupa
!-----------------------------------------------------------------------
FUNCTION d2dx2cen(f,i,j,k)
  USE grid_m, only: g, odxq
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                     :: d2dx2cen
  integer                                  :: i,j,k

    d2dx2cen = (  lpm3 * f(i-3,j,k) &
                + lpm2 * f(i-2,j,k) &
                + lpm1 * f(i-1,j,k) &
                +  lp0 * f(i  ,j,k) &
                + lpp1 * f(i+1,j,k) &
                + lpp2 * f(i+2,j,k) &
                + lpp3 * f(i+3,j,k)  ) * odxq
END FUNCTION d2dx2cen
!-----------------------------------------------------------------------
FUNCTION d2dy2cen(f,i,j,k)
  USE grid_m, only: g, odyq
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                     :: d2dy2cen
  integer                                  :: i,j,k

    d2dy2cen = (  lpm3 * f(i,j-3,k) &
                + lpm2 * f(i,j-2,k) &
                + lpm1 * f(i,j-1,k) &
                +  lp0 * f(i,j  ,k) &
                + lpp1 * f(i,j+1,k) &
                + lpp2 * f(i,j+2,k) &
                + lpp3 * f(i,j+3,k)  ) * odyq
END FUNCTION d2dy2cen
!------------------------
FUNCTION d2dz2cen(f,i,j,k)
  USE grid_m, only: g, odzq
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                     :: d2dz2cen
  integer                                  :: i,j,k

    d2dz2cen = (  lpm3 * f(i,j,k-3) &
                + lpm2 * f(i,j,k-2) &
                + lpm1 * f(i,j,k-1) &
                +  lp0 * f(i,j,k  ) &
                + lpp1 * f(i,j,k+1) &
                + lpp2 * f(i,j,k+2) &
                + lpp3 * f(i,j,k+3)  ) * odzq
END FUNCTION d2dz2cen
!------------------------
FUNCTION d2dx2cena(f) result(h)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,ub(3)
  ub = merge(g%ub,g%ub-1,mpi%ub)
  !$omp parallel do private(i,j,k)
  do k=g%lb(3),ub(3); do j=g%lb(2),ub(2); do i=g%lb(1),ub(1)
    h(i,j,k) = d2dx2cen(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION d2dx2cena
!------------------------
FUNCTION d2dy2cena(f) result(h)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,ub(3)
  ub = merge(g%ub,g%ub-1,mpi%ub)
  !$omp parallel do private(i,j,k)
  do k=g%lb(3),ub(3); do j=g%lb(2),ub(2); do i=g%lb(1),ub(1)
    h(i,j,k) = d2dy2cen(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION d2dy2cena
!------------------------
FUNCTION d2dz2cena(f) result(h)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f,h
  integer i,j,k,ub(3)
  ub = merge(g%ub,g%ub-1,mpi%ub)
  !$omp parallel do private(i,j,k)
  do k=g%lb(3),ub(3); do j=g%lb(2),ub(2); do i=g%lb(1),ub(1)
    h(i,j,k) = d2dz2cen(f,i,j,k)
  enddo; enddo; enddo
  call overlap(h)
END FUNCTION d2dz2cena
!------------------------
FUNCTION lpx(f,i,j,k)
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3))              :: f
  integer                                               :: i, j, k  
  real                                                  :: lpx
    lpx = (  lpm3 * f(i-3,j,k) &
           + lpm2 * f(i-2,j,k) &
           + lpm1 * f(i-1,j,k) &
           + lpp1 * f(i+1,j,k) &
           + lpp2 * f(i+2,j,k) &
           + lpp3 * f(i+3,j,k)  )
END FUNCTION lpx
!------------------------
FUNCTION lpy(f,i,j,k)
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3))              :: f
  integer                                               :: i, j, k  
  real                                                  :: lpy
      lpy = (  lpm3 * f(i,j-3,k) &
             + lpm2 * f(i,j-2,k) &
             + lpm1 * f(i,j-1,k) &
             + lpp1 * f(i,j+1,k) &
             + lpp2 * f(i,j+2,k) &
             + lpp3 * f(i,j+3,k)  )
END FUNCTION lpy
!------------------------
FUNCTION lpz(f,i,j,k)
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3))              :: f
  integer                                               :: i, j, k  
  real                                                  :: lpz
      lpz = (  lpm3 * f(i,j,k-3) &
             + lpm2 * f(i,j,k-2) &
             + lpm1 * f(i,j,k-1) &
             + lpp1 * f(i,j,k+1) &
             + lpp2 * f(i,j,k+2) &
             + lpp3 * f(i,j,k+3)  )
END FUNCTION lpz
!-----------------------------------------------------------------------
FUNCTION curlxdn(e,f) result(h)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cy3*(e(i,j+2,k)-e(i,j-3,k)) &
             + by3*(e(i,j+1,k)-e(i,j-2,k)) &
             + ay3*(e(i,j  ,k)-e(i,j-1,k)) &
             - cz3*(f(i,j,k+2)-f(i,j,k-3)) &
             - bz3*(f(i,j,k+1)-f(i,j,k-2)) &
             - az3*(f(i,j,k  )-f(i,j,k-1))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlxdn
!-----------------------------------------------------------------------
FUNCTION curlydn(e,f) result(h)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cz3*(e(i,j,k+2)-e(i,j,k-3)) &
             + bz3*(e(i,j,k+1)-e(i,j,k-2)) &
             + az3*(e(i,j,k  )-e(i,j,k-1)) &
             - cx3*(f(i+2,j,k)-f(i-3,j,k)) &
             - bx3*(f(i+1,j,k)-f(i-2,j,k)) &
             - ax3*(f(i  ,j,k)-f(i-1,j,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlydn
!-----------------------------------------------------------------------
FUNCTION curlzdn(e,f) result(h)
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cx3*(e(i+2,j,k)-e(i-3,j,k)) &
             + bx3*(e(i+1,j,k)-e(i-2,j,k)) &
             + ax3*(e(i  ,j,k)-e(i-1,j,k)) &
             - cy3*(f(i,j+2,k)-f(i,j-3,k)) &
             - by3*(f(i,j+1,k)-f(i,j-2,k)) &
             - ay3*(f(i,j,  k)-f(i,j-1,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlzdn
!-----------------------------------------------------------------------
FUNCTION curlxup(e,f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cy3*(e(i,j+3,k)-e(i,j-2,k)) &
             + by3*(e(i,j+2,k)-e(i,j-1,k)) &
             + ay3*(e(i,j+1,k)-e(i,j  ,k)) &
             - cz3*(f(i,j,k+3)-f(i,j,k-2)) &
             - bz3*(f(i,j,k+2)-f(i,j,k-1)) &
             - az3*(f(i,j,k+1)-f(i,j,k  ))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlxup
!-----------------------------------------------------------------------
FUNCTION curlyup(e,f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cz3*(e(i,j,k+3)-e(i,j,k-2)) &
             + bz3*(e(i,j,k+2)-e(i,j,k-1)) &
             + az3*(e(i,j,k+1)-e(i,j,k  )) &
             - cx3*(f(i+3,j,k)-f(i-2,j,k)) &
             - bx3*(f(i+2,j,k)-f(i-1,j,k)) &
             - ax3*(f(i+1,j,k)-f(i  ,j,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlyup
!-----------------------------------------------------------------------
FUNCTION curlzup(e,f) result(h)
  real, dimension(g%n(1),g%n(2),g%n(3)) :: e,f,h
  integer i,j,k,lb(3),ub(3)
!.......................................................................
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  !$do private(i,j,k)
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    h(i,j,k) = cx3*(e(i+3,j,k)-e(i-2,j,k)) &
             + bx3*(e(i+2,j,k)-e(i-1,j,k)) &
             + ax3*(e(i+1,j,k)-e(i  ,j,k)) &
             - cy3*(f(i,j+3,k)-f(i,j-2,k)) &
             - by3*(f(i,j+2,k)-f(i,j-1,k)) &
             - ay3*(f(i,j+1,k)-f(i,j  ,k))
  enddo
  enddo
  enddo
  !$end do nowait
END FUNCTION curlzup

END MODULE pic_stagger
!-----------------------------------------------------------------------
  subroutine test_stagger
!
!  Check the correctness of the stagger operators
!
  USE params, only: mpi, periodic, rank, master, mid
  USE grid_m, only : g, bx, by, bz, ex, ey, ez
  use pic_stagger, only: ddxdna, ddydna, ddzdna, &
                         ddxupa, ddyupa, ddzupa, &
                         d2dx2cena, d2dy2cena, d2dz2cena
  use units, only : c
  implicit none
  integer i,j,k, mx, my, mz
  real(kind=8) jx,jy,jz
  real epsx, epsy, epsz, eps1, eps2, eps3, fx, fy, fz, dx, dy, dz, pi
  real, dimension(:,:,:), allocatable :: scr
  logical ok, all_mpi, allok

  character(len=mid):: &
  id='$Id$'

  call print_id(id)

  mx=g%gn(1); my=g%gn(2); mz=g%gn(3); dx=g%ds(1); dy=g%ds(2); dz=g%ds(3)
  allocate(scr(g%n(1),g%n(2),g%n(3)))

  pi = c%pi
  fx = 2.*pi/(mx*dx)
  fy = 2.*pi/(my*dy)
  fz = 2.*pi/(mz*dz)
  allok = .true.

  !$omp parallel do private(i,j,k,jx,jy,jz)
  do k=1,g%n(3)
  do j=1,g%n(2)
  do i=1,g%n(1)
    jx = i-g%lb(1)+mpi%offset(1)+0.25
    jy = j-g%lb(2)+mpi%offset(2)+0.25
    jz = k-g%lb(3)+mpi%offset(3)+0.25
    bx(i,j,k)=sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)=sin(2.*pi*real(jy)/real(my))
    bz(i,j,k)=sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=cos(2.*pi*real(jx-0.5)/real(mx))*fx
    ey(i,j,k)=cos(2.*pi*real(jy-0.5)/real(my))*fy
    ez(i,j,k)=cos(2.*pi*real(jz-0.5)/real(mz))*fz
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(bz)
  call overlap(ex); call overlap(ey); call overlap(ez)
  scr=ddxdna(bx); bx=scr
  bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=&
    abs(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
    ex(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsx=maxval(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  if (g%gn(1).eq.1) epsx=0.
  scr = ddydna(by); by=scr
  by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=&
    abs(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
    ey(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsy=maxval(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  if (g%gn(2).eq.1) epsy=0.
  scr = ddzdna(bz); bz=scr
  bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=&
    abs(bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
    ez(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsz=maxval(bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  if (g%gn(3).eq.1) epsz=0.
  call overlap(bx); call overlap(by); call overlap(bz)
  eps1 = max(0.5*fx**3*dx**2/(3.*2.*1.),1.e-6/dx)
  eps2 = max(0.5*fy**3*dy**2/(3.*2.*1.),1.e-6/dy)
  eps3 = max(0.5*fz**3*dz**2/(3.*2.*1.),1.e-6/dz)
  ok = (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  allok = allok .and. ok
  if (.not. ok) print'(a,i5,1p,6g12.4,l4)', &
  ' ddn     :', rank, epsx, eps1, epsy, eps2, epsz, eps3, ok

  !$omp parallel do private(i,j,k,jx,jy,jz)
  do k=1,g%n(3)
  do j=1,g%n(2)
  do i=1,g%n(1)
    jx = i-g%lb(1)+mpi%offset(1)+0.25
    jy = j-g%lb(2)+mpi%offset(2)+0.25
    jz = k-g%lb(3)+mpi%offset(3)+0.25
    bx(i,j,k)=sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)=sin(2.*pi*real(jy)/real(my))
    bz(i,j,k)=sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=cos(2.*pi*real(jx+0.5)/real(mx))*fx
    ey(i,j,k)=cos(2.*pi*real(jy+0.5)/real(my))*fy
    ez(i,j,k)=cos(2.*pi*real(jz+0.5)/real(mz))*fz
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(bz)
  call overlap(ex); call overlap(ey); call overlap(ez)
  scr = ddxupa(bx); bx=scr
  bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=&
    abs(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
    ex(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsx=maxval(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  if (g%gn(1).eq.1) epsx=0.
  scr = ddyupa(by); by=scr
  by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=&
    abs(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
    ey(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsy=maxval(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  if (g%gn(2).eq.1) epsy=0.
  scr = ddzupa(bz); bz=scr
  bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=&
    abs(bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
    ez(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsz=maxval(bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  if (g%gn(3).eq.1) epsz=0.
  eps1 = max(0.5*fx**3*dx**2/(3.*2.*1.),1.e-6/dx)
  eps2 = max(0.5*fy**3*dy**2/(3.*2.*1.),1.e-6/dy)
  eps3 = max(0.5*fz**3*dz**2/(3.*2.*1.),1.e-6/dz)
  ok = (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  allok = allok .and. ok
  if (.not. ok) print'(a,i5,1p,6g12.4,l4)',' dup     :', &
    rank, epsx, eps1, epsy, eps2, epsz, eps3, ok

  !$omp parallel do private(i,j,k,jx,jy,jz)
  do k=1,g%n(3)
  do j=1,g%n(2)
  do i=1,g%n(1)
    jx = i-g%lb(1)+mpi%offset(1)+0.25
    jy = j-g%lb(2)+mpi%offset(2)+0.25
    jz = k-g%lb(3)+mpi%offset(3)+0.25
    bx(i,j,k)= sin(2.*pi*real(jx)/real(mx))
    by(i,j,k)= sin(2.*pi*real(jy)/real(my))
    bz(i,j,k)= sin(2.*pi*real(jz)/real(mz))
    ex(i,j,k)=-sin(2.*pi*real(jx)/real(mx))*fx**2
    ey(i,j,k)=-sin(2.*pi*real(jy)/real(my))*fy**2
    ez(i,j,k)=-sin(2.*pi*real(jz)/real(mz))*fz**2
  end do
  end do
  end do
  call overlap(bx); call overlap(by); call overlap(bz)
  call overlap(ex); call overlap(ey); call overlap(ez)
  scr = d2dx2cena(bx); bx=scr
  bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=&
    abs(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
        ex(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsx=maxval(bx(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  if (g%gn(1).eq.1) epsx=0.
  scr = d2dy2cena(by); by=scr
  by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1)=&
    abs(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
        ey(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsy=maxval(by(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  if (g%gn(2).eq.1) epsy=0.
  scr = d2dz2cena(bz); bz=scr
  bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) = &
    abs(bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1) - &
        ez(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  epsz=maxval(bz(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
  if (g%gn(3).eq.1) epsz=0.
  eps1 = 2.*max(fx**4*dx**2/(3.*2.*1.),2.e-6*fx/dx)
  eps2 = 2.*max(fy**4*dy**2/(3.*2.*1.),2.e-6*fy/dy)
  eps3 = 2.*max(fz**4*dz**2/(3.*2.*1.),2.e-6*fz/dz)
  ok = (epsx < eps1) .and. (epsy < eps2) .and. (epsz < eps3)
  allok = allok .and. ok
  if (.not. ok .and. rank < 10) print'(a,i5,1p,6g12.4,l4)', &
    ' d2d[xyz]:', rank, epsx, eps1, epsy, eps2, epsz, eps3, ok

  allok = all_mpi(allok)
  if (allok) then
    if (master) print *,'The stagger routines appear to be working correctly'
  else
    call warning('stagger_test','THERE APPEARS TO BE A PROBLEM WITH THE STAGGER ROUTINES')
  end if

  deallocate(scr)

  end subroutine

! Compute prefactors for the stagger operators
!-----------------------------------------------------------------------
SUBROUTINE compute_stagger_prefactors                                  
USE grid_m,           only : g
USE pic_stagger,  only : aa,  bb,  cc, a1,     &
                           ax1, ay1, az1,      &
                           ax3, ay3, az3,      &
                           bx3, by3, bz3,      &
                           cx3, cy3, cz3,      &
                           lp0, lpm1, lpm2,    &
                           lpm3, lpp1, lpp2,   &
                           lpp3, order
  implicit none

  integer :: i
  real    :: dx, dy, dz

   !----------------
    dx = g%ds(1)
    dy = g%ds(2)
    dz = g%ds(3)

    cc=3./256.   ; cx3=(-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3.))
    bb=-25./256. ; cy3=(-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3.))
    aa=.5-bb-cc  ; cz3=(-1.+(3.**5-3.)/(3.**3-3.))/(5.**5-5.-5.*(3.**5-3.))
    bx3=(-1.-120.*cx3)/24.    ; by3=(-1.-120.*cy3)/24.    ; bz3=(-1.-120.*cz3)/24.
    ax3=(1.-3.*bx3-5.*cx3)/dx ; ay3=(1.-3.*by3-5.*cy3)/dy ; az3=(1.-3.*bz3-5.*cz3)/dz
    bx3=bx3/dx                ; by3=by3/dy                ; bz3=bz3/dz
    cx3=cx3/dx                ; cy3=cy3/dy                ; cz3=cz3/dz

    lpm3 = +   2./180.
    lpm2 = -  27./180.
    lpm1 = + 270./180.
    lp0  = - 490./180.
    lpp1 = + 270./180.
    lpp2 = -  27./180.
    lpp3 = +   2./180.
   !----------------

  call test_stagger
  
END SUBROUTINE compute_stagger_prefactors
!----------------------------------------------------------------------


!======================================================================
!CONVENTIONS:
!
!
!
!
!GRID:	i-3		i-2		i-1		i		i+1		i+2		i+3		i+1
!
!			ddxdn[i,j,k](i-3,i-2,i-1,i,i+1,i+2) / (i-2,i-1,i,i+1) / (i-1,i)
!							|
!		----------------------------------------|---------------------------
!		|		|		|		|		|		|
!X	O	X	O	X	O	X	O	X	O	X	O	X	O	X	O
!			|		|		|		|		|		|
!			----------------------------------------|-------------------------
!								|
!				 ddxup[i,j,k](i-2,i-1,i,i+1,i+2,i+3) / (i-1,i,i+1,i+2) / (i,i+1)
!
!=======================================================================


!From the coefficients: 
!    bz3=-1./24.
!    az3=(1. - 3.*bz3)/dz
!    bz3=bz3/dz
!
! we get for example that 
!    ddzup  = (  az3*(f(i,j,k+1 )-f(i,j,k   )) &
!              + bz3*(f(i,j,k+2 )-f(i,j,k-1 ))  )
!
! can be rewritten as
!    ddzup  = ( -f[+3/2] + 27*f[+1/2] - 27*f[-1/2]  + 1*f[-3/2] )/(24*dz).
!
! which is the correct representation on HALF-CENTERED grids. 
!
!We should, however, use the corresponding CENTERED LaPlace 
! operator to keep things in line, since we're finding d^2/d[Q]^2
! at it's 'own' point.
