! vim: nowrap
!=======================================================================
! Fields/maxwell_sources.f90 $Id$
!=======================================================================
MODULE maxwell_sources
  USE params,  ONLY : hl, trace, stdout, mdim, mcoord
  USE grid_m,  ONLY : g
  USE species, ONLY : nspecies
  USE units,   ONLY : c
  implicit none
  integer, public  :: verbose
  integer, private :: ix, iy, iz
  real,    public  :: d_charge, max_charge
  real,    public  :: bmax
  real,    public  :: maxwpe 
  logical, public  :: do_filter
  integer, public  :: filter_order
  real, allocatable, dimension(:,:,:) :: charge 
  real, allocatable, dimension(:,:,:) :: jx, jy, jz
  real(kind=8), allocatable, dimension(:,:) :: jjx, jjy, jjz, ccharge
  real, allocatable, dimension(:,:,:) :: bxm, bym, bzm
  real, allocatable, dimension(:,:,:) :: bxa, bya, bza
  real, allocatable, dimension(:,:,:) :: sx, sy, sz
!.......................................................................
CONTAINS
!=======================================================================
SUBROUTINE reset_charge
  implicit none
  do iz=1,g%n(3)
    charge(:,:,iz) = 0.
  end do
END SUBROUTINE reset_charge
!=======================================================================
SUBROUTINE reset_current
  implicit none
  do iz=1,g%n(3)
    jx(:,:,iz) = 0.
    jy(:,:,iz) = 0.
    jz(:,:,iz) = 0.
  end do
END SUBROUTINE reset_current
!=======================================================================
! Calculate the charge density ON THE INTERIOR only of the grid.
!=======================================================================
SUBROUTINE calc_charge
  USE params,  only: stdout, trace, mpi
  USE species, only: fields, sp
  USE units,   only: c, elm
  USE grid_m,  only: nx,ny,nz,fmhd
  USE dumps,   only: dump
  implicit none
  integer :: ix, iy, iz, isp, lb(3), ub(3)
  real    :: nfac, ns, max_scalar, maxwpep
  real    :: min4, max4, fmin, fmax
!.......................................................................
  call trace_enter('CALC_CHARGES')
  call reset_charge
  maxwpe  = 0. 
  maxwpep = 0.
  allocate (ccharge(nx,ny))                                             ! private copy
  do iz=1,g%n(3)
    ccharge=0.0
    do isp=1,nspecies
      if (sp(isp)%charge .eq. 0.) cycle                                 ! no contrib
      nfac = &
        c%fourpi*elm%ke*sp(isp)%charge*(sp(isp)%charge/sp(isp)%mass)    ! prefactor
      do iy=1,g%n(2)
      do ix=1,g%n(1)
        ns = fields(ix,iy,iz,isp)%d                                     ! nr dens for species
        ccharge(ix,iy) = ccharge(ix,iy) + sp(isp)%charge * ns           ! charge dens: (q*Z)*n
        maxwpep = max(maxwpep,nfac*ns*fmhd(ix,iy,iz))                   ! (max_plasma_freq)**2
      enddo                                                             ! Rel. wpe always lower 
      enddo
    enddo
    charge(:,:,iz) = ccharge
  enddo
  maxwpe = max(maxwpe,maxwpep)
  deallocate (ccharge)                                                  ! deallocate copy
  maxwpe = max_scalar(sqrt(maxwpe))                                     ! species plasma freq 
  call charge_boundaries(charge)
                                                   call dump(charge,'q')
  call trace_exit('CALC_CHARGES')
END SUBROUTINE calc_charge
!=======================================================================
! Calculate the current density ON THE INTERIOR only of the grid.
!=======================================================================
SUBROUTINE calc_current
  USE params,  only: stdall, mid
  USE species, only: fields, sp
  USE grid_m,  only: nx,ny,nz
  USE debug,   only: dbg_maxwell, debugit
  USE dumps,   only: dump
  implicit none
  integer :: ix, iy, iz, isp
  character(len=mid), save :: id = &
    'Fields/maxwell_sources.f90 $Id$'
!.......................................................................
  call trace_enter('CALC_CURRENT')

  allocate(jjx(nx,ny))
  allocate(jjy(nx,ny))
  allocate(jjz(nx,ny))

  do iz=1,g%n(3)
    jjx = 0.0
    if (mcoord > 1) jjy = 0.0
    if (mcoord > 2) jjz = 0.0

    do isp=1,nspecies
     if (sp(isp)%charge .eq. 0.) cycle
     do iy=1,g%n(2)
      do ix=1,g%n(1)
        jjx(ix,iy) = jjx(ix,iy) + sp(isp)%charge * fields(ix,iy,iz,isp)%v(1)
      enddo
     enddo

     if (mcoord .eq. 1) cycle
     do iy=1,g%n(2)
      do ix=1,g%n(1)
        jjy(ix,iy) = jjy(ix,iy) + sp(isp)%charge * fields(ix,iy,iz,isp)%v(2)
      enddo
     enddo

     if (mcoord .eq. 2) cycle
     do iy=1,g%n(2)
      do ix=1,g%n(1)
        jjz(ix,iy) = jjz(ix,iy) + sp(isp)%charge * fields(ix,iy,iz,isp)%v(3)
      enddo
     enddo
    enddo

    jx(:,:,iz) = jjx
    if (mcoord > 1) jy(:,:,iz) = jjy
    if (mcoord > 2) jz(:,:,iz) = jjz
  enddo
  deallocate(jjx)
  deallocate(jjy)
  deallocate(jjz)
  call j_boundaries(jx,jy,jz)
                                                      call dump(jx,'jx')
                                                      call dump(jy,'jy')
                                                      call dump(jz,'jz')
  call trace_exit('CALC_CURRENT')
END SUBROUTINE calc_current
!=======================================================================
END MODULE maxwell_sources

!=======================================================================
SUBROUTINE allocate_sources
  USE maxwell_sources
  USE grid_m, only : nx,ny,nz
  implicit none
!.......................................................................
  call trace_enter ('ALLOCATE_SOURCES')
  if (.not. allocated(charge)) then
    allocate (charge(nx,ny,nz))
    if (mcoord == 2) then
      allocate (bxm(nx,ny,nz), bym(nx,ny,nz))
      allocate (bxa(nx,ny,nz), bya(nx,ny,nz))
      allocate (sx (nx,ny,nz), sy (nx,ny,nz))
      allocate (jx (nx,ny,nz), jy (nx,ny,nz))
    else
      allocate (bxm(nx,ny,nz), bym(nx,ny,nz), bzm(nx,ny,nz))
      allocate (bxa(nx,ny,nz), bya(nx,ny,nz), bza(nx,ny,nz))
      allocate (sx (nx,ny,nz), sy (nx,ny,nz), sz (nx,ny,nz))
      allocate (jx (nx,ny,nz), jy (nx,ny,nz), jz (nx,ny,nz))
    end if
  end if
  bxm=0. ; bym=0.; bzm=0.
  bxa=0. ; bya=0.; bza=0. 
  sx =0. ; sy =0.; sz =0.
  call reset_charge
  call reset_current
  call trace_exit ('ALLOCATE_SOURCES')
END SUBROUTINE allocate_sources
