! vim: nowrap
!=======================================================================
MODULE energy_filter_m
  implicit none
  real :: e_filter = 0.0  ! 0.005 is a good value
!-----------------------------------------------------------------------
CONTAINS 
!=======================================================================
SUBROUTINE energy_filter (bx, by, bz, ex, ey, ez, a)
  USE params,    only: mpi, mid
  USE grid_m,    only: g, nx, ny, nz
  USE laplace_m, only: laplace6
  implicit none
  real, dimension(nx,ny,nz)                :: bx, by, bz, ex, ey, ez
  real                                     :: a, fav
  real(8)                                  :: f
  real(8), dimension(:,:,:), allocatable   :: e1, e2
  real(8), dimension(:,:,:,:), allocatable :: b, e
  integer                                  :: ix, iy, iz, jx, jy, jz, n
  integer                                  :: lb(3), ub(3)
  character(len=mid), save:: id = &
    'Fields/energy_filter.f90 $Id$'
!.......................................................................
  call print_id (id)
  allocate (e1(nx,ny,nz), e2(nx,ny,nz), b(nx,ny,nz,3), e(nx,ny,nz,3))
  lb =       g%lb
  ub = merge(g%ub, g%ub-1, mpi%ub)
  fav = 0.0
  n = 0
!-----------------------------------------------------------------------
! Calculate the field energy -- should really be centered, but this
! would imply a non-local computation of the energy balance condition.
!-----------------------------------------------------------------------
  do iz=1,nz
    e1(:,:,iz) = bx(:,:,iz)**2 + by(:,:,iz)**2 + bz(:,:,iz)**2 &
               + ex(:,:,iz)**2 + ey(:,:,iz)**2 + ez(:,:,iz)**2
  end do
!-----------------------------------------------------------------------
! Suggest a change that will smooth out ripples.  Possible choices are
! three 2nd order Laplace operators: poisson_2nd, poisson_4th, and
! poisson_6th, and the 6th order derivative based laplace6.
!-----------------------------------------------------------------------
  e2 = laplace6(e1,a)
  call phi_boundaries(e2)
!-----------------------------------------------------------------------
! First check for cells with vanishing EM energy.  If any are found,
! prepare them to be filled with energy, with E & B values averaged
! over neighboring cells.  This is to allow energy conservation even
! in this uncommon case.
!-----------------------------------------------------------------------
  do iz=lb(3),ub(3)
  do iy=lb(2),ub(2)
  do ix=lb(1),ub(1)
    if (e1(ix,iy,iz) == 0.) then
      b(ix,iy,iz,:) = 0.0
      e(ix,iy,iz,:) = 0.0
      do jz=iz-1,iz+1
      do jy=iy-1,iy+1
      do jx=ix-1,ix+1
        b(ix,iy,iz,1) = b(ix,iy,iz,1) + bx(jx,jy,jz)
        b(ix,iy,iz,2) = b(ix,iy,iz,2) + by(jx,jy,jz)
        b(ix,iy,iz,3) = b(ix,iy,iz,3) + bz(jx,jy,jz)
        e(ix,iy,iz,1) = e(ix,iy,iz,1) + ex(jx,jy,jz)
        e(ix,iy,iz,2) = e(ix,iy,iz,2) + ey(jx,jy,jz)
        e(ix,iy,iz,3) = e(ix,iy,iz,3) + ez(jx,jy,jz)
      end do
      end do
      end do
      e1(ix,iy,iz) = sum(b(ix,iy,iz,:)**2 + e(ix,iy,iz,:)**2)
    endif
!-----------------------------------------------------------------------
! Compute the factor of change needed for this energy change, and apply
! it to all field components.  Note that the B and E values must not 
! nowait. Note that the check is still relevant, since we cannot be sure
! that the average above produces a non-zero result.
!-----------------------------------------------------------------------
    if (e1(ix,iy,iz) > 0.0) then
      f = sqrt(1.+e2(ix,iy,iz)/e1(ix,iy,iz))
      fav = fav+f
      n = n+1
      b(ix,iy,iz,1) = f*bx(ix,iy,iz)
      b(ix,iy,iz,2) = f*by(ix,iy,iz)
      b(ix,iy,iz,3) = f*bz(ix,iy,iz)
      e(ix,iy,iz,1) = f*ex(ix,iy,iz)
      e(ix,iy,iz,2) = f*ey(ix,iy,iz)
      e(ix,iy,iz,3) = f*ez(ix,iy,iz)
    endif
  end do
  end do
  end do
!-----------------------------------------------------------------------
! Copy the field components back and apply boundary conditions.
!-----------------------------------------------------------------------
  do iz=lb(3),ub(3)
  do iy=lb(2),ub(2)
  do ix=lb(1),ub(1)
    bx(ix,iy,iz) = b(ix,iy,iz,1)
    by(ix,iy,iz) = b(ix,iy,iz,2)
    bz(ix,iy,iz) = b(ix,iy,iz,3)
    ex(ix,iy,iz) = e(ix,iy,iz,1)
    ey(ix,iy,iz) = e(ix,iy,iz,2)
    ez(ix,iy,iz) = e(ix,iy,iz,3)
  end do
  end do
  end do
  call b_boundaries (bx, by, bz)
  call e_boundaries (ex, ey, ez)
!------------------------------------------------------------------------
  deallocate (e1, e2, b, e)
END SUBROUTINE energy_filter
!=======================================================================
END MODULE energy_filter_m
