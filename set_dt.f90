! set_dt.f90 $Id$
! vim: nowrap
!-----------------------------------------------------------------------
SUBROUTINE set_dt
!
! Sets the time step so it satisfies:
!       - The Courant-Fredriech-Levy condition
!       - The (electron) Plasma frequency   
!       - The (electron) Cyclotron frequency 
!       - ....
  USE params,          only : dt, dtp, Cdt, Cdtwpe, Cdtwce, nspecies, do_maxwell, &
                              master, stdout, trace, dt_factor, do_vth, rank, mpi
  USE debug,           only : dbg_select, dbg_dt
  USE grid_m,            only : g, fmhd, fdebye, fskin, drmax
  USE units,           only : c, elm
  USE species,         only : sp, fields
  USE maxwell_sources, only : maxwpe, bmax
  USE dumps,           only : dump!, dump_reset
  implicit none
  integer           :: isp, ix, iy, iz, firstcellindex, lb(3), ub(3)
  real(kind=8)      :: dtnew
  real(kind=8)      :: bfac, maxbfac, maxwce, dsmin, dsmax, tpe, delta, dtwpe
  real              :: dsp, vth, debye, debye_min, debye_min_z, min_scalar, max_scalar
  character(len=80) :: text
  logical, save     :: first_real_call=.true.
!.......................................................................
  call trace_enter('set_dt')
                               call timer('set_dt','start')

  if (Cdt <= 0) then                                                    ! for tests
    dtp = dt
    return
  endif

  dtnew = 1e10                                                          ! make sure
  dsmax = maxval(g%ds)
  dsmin = minval(g%ds)

  drmax = max_scalar(drmax)
  if (drmax>0) dtnew = Cdt*dt/drmax                                     ! max Cdt mesh motion

  if (do_maxwell) then                                                  
    dtnew = Cdt*minval(g%ds)/c%c                                        ! check CFL-condition
    maxwce = 0.
    maxbfac = 0.
    do isp=1,nspecies
     if (sp(isp)%charge .eq. 0.) cycle                                  ! photons - exit
      bfac = elm%kf*abs(sp(isp)%charge/sp(isp)%mass)                    ! system-free wce
      maxbfac = max(bfac,maxbfac)                                       ! maximum prefactor
    enddo
    maxwce  = maxbfac*bmax                                              ! max cycl. frequency. 
    ! Relativistic wce always less or equal so OK with classical expression

    if (maxwce .gt. 0.) dtnew = min(dtnew,real(Cdtwce/maxwce,kind=8))   ! cyclotron frequency
    if (maxwpe .gt. 0.) dtnew = min(dtnew,real(Cdtwpe/maxwpe,kind=8))   ! plasma frequency
    if (master.and.(trace .or. iand(dbg_select,dbg_dt)>0)) then 
      if (maxwpe .gt. 0. .and. maxwce .gt. 0.) &
        write(stdout,1) 'skin d,  maxwpe, maxwce : ', &
        c%c/maxwpe, maxwpe,maxwce
      if (maxwpe .gt. 0.) &
        write(stdout,1) 'dt for plasma frequency : ',Cdtwpe/maxwpe
      if (maxwce .gt. 0.) &
        write(stdout,1) 'dt for cycl. frequency  : ',Cdtwce/maxwce
      if (maxwce .gt. 0.) &
        write(stdout,1) 'dt for speed of light   : ', &
        Cdt*minval(g%ds)/c%c, c%c, minval(g%ds), maxval(g%ds)
1     format(1x,a,1p,5e12.3)
    endif
  else
    maxwce = 0.
    maxwpe = 0.
  endif

  lb = merge(g%lb, g%lb  , mpi%lb)
  ub = merge(g%ub, g%ub-1, mpi%ub)

  do iz=1,g%n(3); do iy=1,g%n(2); do ix=1,g%n(1)
    fmhd(ix,iy,iz)=1.0
  enddo; enddo; enddo
  
  !---------------------------------------------------------------------
  ! fskin = 1 prevents the wpe timestep from becomings shorter than the Cdt
  ! timestep, thus preventing a large density region to limit the timestep
  ! below what is anyway necessary, due to the speed of light.
  !---------------------------------------------------------------------
  if (fskin>0.) then
    !call dump_reset ('fmhd.dmp')
    do isp=1, nspecies
      if (TRIM(sp(isp)%name)=='electron') then
        do iz=lb(3),ub(3); do iy=lb(2),ub(2); do ix=lb(1),ub(1)
          dsp = max(fields(ix,iy,iz,isp)%d,1e-10)
          delta = c%c*sqrt(dble(sp(isp)%mass)/(dsp*c%fourpi*elm%ke))/c%elm_q
          fmhd(ix,iy,iz) = min(1.0_8,(delta/(fskin*dsmin))**2)
        enddo; enddo; enddo
      end if
    enddo
    call overlap (fmhd)
    call dump (fmhd, 'fmhd')
  endif

  if (abs(alog(real(dtnew/dt))) < alog(dt_factor)) then                 ! If change less then dt_fact ..
    dtnew = dt                                                          ! .. don't change dt ..
  else                                                                  ! .. and if it is ..
    dtnew = min(dt*2.,max(dt*0.5,dtnew))                                ! .. don't change w more than factor 2
  end if
  
  if (dt .ne. dtnew .or. (first_real_call .and. dt .ne. 0.)) then
    first_real_call = .false.
  endif

  if (dt .gt. dtnew) then                                               ! Only change dt if it is too large...
    if (master) then
      if (maxwce==0.) maxwce=1e10
      if (maxwpe==0.) maxwpe=1e10
      if (do_maxwell) then
        write(*,'(a19,f9.5,a4,f9.5,9x,a,f10.5,2f8.4,a1)') &
         'Reducing dt from ',dt,' to ',dtnew,' wce, wpe, cdt =>', &
         Cdtwce/maxwce,Cdtwpe/maxwpe,Cdt*minval(g%ds)/c%c
      else if (drmax > 0.) then
        write(*,'(a19,f9.5,a4,f9.5,9x,a,f10.5,a1)') &
         'Reducing dt from ',dt,' to ',dtnew,' cdt =>',Cdt*dt/drmax
      else 
        write(*,'(a19,f9.5,a4,f9.5)') &
         'Reducing dt from ',dt,' to ',dtnew
      endif
    endif
    dt = dtnew
  else if (dt .lt. dtnew) then                                          !...or too small
    if (master) then
      if (maxwce==0.) maxwce=1e10
      if (maxwpe==0.) maxwpe=1e10
      if (do_maxwell) then
        write(*,'(a19,f9.5,a4,f9.5,9x,a,f10.5,2f8.4,a1)') &
         'Increasing dt from ',dt,' to ',dtnew,' wce, wpe, cdt =>', &
         Cdtwce/maxwce,Cdtwpe/maxwpe,Cdt*minval(g%ds)/c%c
      else if (drmax > 0.) then
        write(*,'(a19,f9.5,a4,f9.5,9x,a,f10.5,a1)') &
         'Increasing dt from ',dt,' to ',dtnew,' cdt =>',Cdt*dt/drmax
      else 
        write(*,'(a19,f9.5,a4,f9.5)') &
         'Increasing dt from ',dt,' to ',dtnew
      endif
    endif
    dt = dtnew
  else if (dt.le.0.) then
    call error('set_dt','dt .le. 0.0!')
  end if

  dtp = dt

  call trace_exit('set_dt')
END SUBROUTINE set_dt
