! Io/io.f90 $Id$
! vim: nowrap
!=======================================================================
SUBROUTINE read_io
  USE params,  only: master, stdin, stdout, mfile, &
                     params_unit, production, writefac_global, &
                     out_namelists, do_parallel_io, datadir, run_info, &
                     mprint
  USE pic_io,  only: verbose, toutf, toutp, toutr, touts, indices, &
                     restart, do_get_old_data, oldsnapshot, nslice, &
                     do_scp, scpdir
  USE grid_m,  only: nx, ny, nz
  implicit none
  character(len=mfile) :: pfile
  integer              :: writefac, ionodes
  logical, save        :: first_call=.true.
  logical              :: void
  logical, external    :: dir_exists, mkdir
  namelist /mpi/ nx, ny, nz
  namelist /output/ writefac, toutf, toutp, toutr, touts, verbose, &
           datadir, indices, restart, do_get_old_data, oldsnapshot, &
           production, nslice, do_scp, scpdir, ionodes, mprint
!.......................................................................
  call trace_enter('read_io')
  ionodes = 1
  writefac = writefac_global
  rewind (stdin); read (stdin,output)
  datadir = trim(datadir)
  if (first_call) then
    pfile = trim(datadir)//'/params.nml'
  else
    pfile = trim(datadir)//'/params_new.nml'  
  endif
  
  if (.not. dir_exists(trim(datadir))) &                                ! check if dir exists, else make it
    void = mkdir(datadir,'read_io',10,.true.)

  if (master) then
    open(params_unit,file=pfile,form='formatted', &
         status='unknown',delim='quote')
    write (params_unit,mpi)
    write (params_unit,output)
    write (params_unit,run_info)
  endif
  if (out_namelists) write (stdout,output) 
  writefac_global = writefac
  first_call = .false.
  call trace_exit('read_io')
END SUBROUTINE read_io

!=======================================================================
SUBROUTINE init_io
  USE params,       only : mid,stdin,params_unit,restarting,production, &
                           writefac_global,do_parallel_io,datadir
  USE species,      only : sp
  USE pic_io,       only : verbose,toutf,toutp,toutr,touts,indices, &
                           oldptime,oldftime, &
                           oldrtime,oldwrtime,oldstime,oldfctime,restart, &
                           do_get_old_data,oldsnapshot,nslice,do_scp,scpdir
  implicit none
  character(len=mid) :: &
    id = "Io/io.f90 $Id$"
  character(len=mid) :: r1,r2,r3,r4
  logical, external  :: file_exists


  call print_id(id)

  do_get_old_data = (/.false.,.false.,.false./)                         ! (/params,fields,particles/)?
  oldsnapshot = -1                                                      ! no snapshot = no read

  writefac_global = 1_8                                                 !> dump every writefac part
  toutf = 20.                                                           !> write fields evry toutf
  toutp = 20.                                                           !> write parts evry toutp
  touts = 20.                                                           !> synth spectra evry touts
  toutr = 1000.                                                         !> write restart evry toutr
  datadir = 'Data'                                                      !> dir to write to
  verbose = 0                                                           !> verbosity in io
  indices = .true.                                                      !> do not dump part ids
  restart = 0                                                           !> we are not restarting
  production = .false.                                                  !> production mode?
  nslice  = 0                                                           !> nr of 2D slices to dump
  do_scp = .false.                                                      !> scp to copy output files
  scpdir = ''                                                           !> where to scp to

  call read_io

  if (restart .gt. 0) restarting = .true.
  oldptime=-10000                                                       ! Always dump the
  oldftime=-10000                                                       ! first timestep
  oldfctime=-10000                                                      ! first timestep
  oldstime=-10000                                                       !
  oldwrtime= 0.                                                         ! Last restart [wall secs]
  oldrtime = 0.                                                         ! Last restart [code time]
END SUBROUTINE init_io

!=======================================================================
SUBROUTINE write_io()
  USE params, only : time_bet_restart, master, touch_unit, datadir, time, params_unit
  USE pic_io
  implicit none
  character(len=6)               :: cdump
  real(kind=8), external         :: wallclock
  logical, external              :: file_exists, file_flag
  logical                        :: dump_everything
  integer                        :: do_restart
  integer, save :: fcntf=-1
  integer, save :: fcntp=-1
  logical, save :: write_all_f=.false.
  logical, save :: write_all_p=.false.
  logical, save :: first_call=.true.
  !
  if (first_call) then
    if (master) flush(params_unit)
    first_call = .false.
  endif
  !
  if (file_flag(trim(datadir)//'/reread')) call re_read_input_file()
  !
  ! Check if we should dump a restart file:
  do_restart = merge(1,0,wallclock()-oldwrtime > time_bet_restart &
                           .or. file_flag(trim(trim(datadir)//'/restart')))
  if (do_restart==1) oldwrtime = time
  call mpi_broadcast(do_restart,0)                                      ! broadcast flag
  dump_everything = &
    file_flag(trim(trim(datadir)//'/dump_now')) .or. do_restart==1      ! dump right now ?

  if (write_all_p) then                                                 ! dump at all time steps?
    fcntp=fcntp+1
    write(cdump,'(i6.6)') int(fcntp)                 ! FIXME: what to do if toutf<dt or toutp<dt?
    if (do_restart==1) cdump='restar'                                   ! dont interrupt snapshot
    call write_particles(cdump,time,do_restart)
    if (do_restart/=1) oldptime = int(time/toutp)*toutp
  else 
    if (time.ge.toutp+oldptime .or. dump_everything) then               ! dump particles?
      write(cdump,'(i6.6)') int(time/toutp)            ! FIXME: what to do if toutf<dt or toutp<dt?
      if (do_restart==1) cdump='restar'                                 ! dont interrupt snapshot
      call write_particles(cdump,time,do_restart)
      if (do_restart/=1) oldptime = int(time/toutp)*toutp
    endif
  endif

  if (write_all_f) then                 ! dump fields at all time steps?
    fcntf=fcntf+1
    write(cdump,'(i6.6)') int(fcntf)            ! FIXME: what to do if toutf<dt or toutp<dt?
    if (do_restart==1) cdump='restar'                                   ! dont interrupt snapshot
    call write_fields(cdump,time)
    if (do_restart/=1) oldftime = int(time/toutf)*toutf
  else 
    if (time.ge.toutf+oldftime .or. dump_everything) then               ! dump fields?
      write(cdump,'(i6.6)') int(time/toutf)            ! FIXME: what to do if toutf<dt or toutp<dt?
      if (do_restart==1) cdump='restar'                                 ! dont interrupt snapshot
      call write_fields(cdump,time)
      if (do_restart/=1) oldftime = int(time/toutf)*toutf
    endif
  endif

  !call write_stat(ptime)                                                ! write statistics?
END SUBROUTINE write_io

!=======================================================================
SUBROUTINE re_read_input_file()
  USE params, only : re_read_init_namelists,params_unit,stdin,stdout, &
                     mfile,hl,master,datadir
  USE units,  only : u !, read_units, consistent_units
  implicit none
  logical              :: opened
  logical, external    :: file_exists
  character(len=mfile) :: inputfile

  inputfile = trim(datadir)//'/input.txt'
  if (.not. file_exists(inputfile)) then
    call warning('re_read_input_file','Cannot find '//trim(inputfile)// &
                 '. Resuming running without updating parameters')
    return
  endif
  
  if (master) then
    write(stdout,'(1x,a)') hl
    write(stdout,'(1x,a)') 'Re-reading inputfile from '//trim(inputfile)
    write(stdout,'(1x,a)') 'Beware that changing parameters during run is dangerous'
    write(stdout,'(1x,a)') hl
  endif

  inquire(unit=stdin, opened=opened)                                    ! check if stdin connected
  if (opened) close(stdin)                                              ! close prior to reopening
  open (stdin,file=inputfile,status='old')                              ! to read namelist from

  call reread

  close(params_unit)
END SUBROUTINE re_read_input_file

!=======================================================================
SUBROUTINE write_header (ptime)
  USE params, only : data_unit, mid, nspecies
  USE pic_io, only : indices, nslice, oldptime, oldftime, oldstime, &
                     oldrtime, oldwrtime, oldfctime
  implicit none
  real(kind=8), intent(in)  :: ptime
  character(len=mid) :: &
    id = "Io/io.f90 $Id$"
  integer, parameter :: io_format=13                                    !> change whenever changing io format
!.......................................................................
  write (data_unit) io_format,mid
  write (data_unit) id
  write (data_unit) 2
  write (data_unit) 7,2
  write (data_unit) real((/ptime, oldptime, oldftime, oldrtime, &
                           oldwrtime, oldstime, oldfctime /), kind=4) 
  write (data_unit) 2,1
  write (data_unit) indices,nslice
  call write_params
  call write_grid
  !call write_units
  if (nspecies > 0) call write_species                                  ! only when we have species
  write (data_unit) -1,mid
END SUBROUTINE write_header

!=======================================================================
SUBROUTINE read_header
  USE params, only : data_unit, mid, nspecies
  USE pic_io, only : oldptime,oldftime,oldfctime,oldstime,oldrtime, &
                     oldwrtime
  USE header
  implicit none
  character(len=mid) :: &
    id = "Io/io.f90 $Id$"
  integer            :: io_format=12                                    !> change whenever changing io format
  integer            :: lnslice
!.......................................................................
  hunit = data_unit                                                     ! set I/O unit for header
  call check_header_format(io_format, id, "read_header")
!.......................................................................
  call read_header_line ! do we have to return ptime; what is it for ??
  oldptime  = h_line%floats(2)
  oldftime  = h_line%floats(3)
  oldrtime  = h_line%floats(4)
  oldwrtime = h_line%floats(5)
  oldstime  = h_line%floats(6)
  oldfctime = h_line%floats(7)
  !write (data_unit) real(ptime), real(oldptime), real(oldftime), &
  !  real(oldrtime), real(oldwrtime), real(oldstime), real(oldfctime)
!.......................................................................
  call read_header_line
  !write (data_unit) indices,nslice
  lnslice = h_line%ints(2)
!.......................................................................
  !call read_params_header
  !call read_grid_header
  !call read_units_header
  !if (nspecies > 0) call read_species_header
  !call read_stat_header
  !call read_synthetic_spectra_header
  !if (lnslice .gt. 0) call read_slice_header
!.......................................................................
  read(data_unit) lnslice                                               ! termination of header 
  !write (data_unit) -1,mid
  if (lnslice .ne. -1) &
    call error("read_header","More sections in header, something rotten") 
END SUBROUTINE read_header

!=======================================================================
SUBROUTINE skip_header(lio_format)
  USE params, only : data_unit, nspecies, mid, time
  USE pic_io
  USE header
  implicit none
  character(len=mid) :: &
    id = "Io/io.f90 $Id$"
  integer :: first_io_format, lio_format
!.......................................................................
  hunit = data_unit                                                     ! set I/O unit for header
  lio_format = 12 
  call check_header_format(lio_format, id, "read_header")
  first_io_format = lio_format
!.......................................................................
  call read_header_line
  time  = h_line%floats(1)
  oldptime  = h_line%floats(2)
  oldftime  = h_line%floats(3)
  oldrtime  = h_line%floats(4)
  oldwrtime = h_line%floats(5)
  oldstime  = h_line%floats(6)
  if (lio_format >= 11) oldfctime = h_line%floats(7)
  call read_header_line
!.......................................................................
  do while (lio_format .ne. -1)
    call skip_header_block(lio_format)
  enddo
  lio_format = first_io_format
  !.......................................................................
END SUBROUTINE skip_header

!=======================================================================
SUBROUTINE read_particle_dump(fname,vgamma,header_size)
  USE params,  only: mpi, rank, nodes, mdim, mcoord, nspecies, &
                     data_unit, periodic, stdout, master, &
                     do_validate, do_parallel_io
  USE debug,   only: maxprint, verbose
  USE species, only: sp, particle
  USE grid_m,  only: g
  USE units,   only: c
  USE stat,    only: particle_statistics
  USE pic_io,  only: file_io_type
  USE particleoperations, only: sp_global_to_cell_coordinates
  implicit none
  character(len=*),             intent(in)   :: fname
  real,                         intent(in)   :: vgamma
  integer,                      intent(in)   :: header_size
  real                                       :: gamma, global_frac
  integer                                    :: lio_format,isp,i,np,gsend,&
                                                nprint,out_lb,out_ub,irank
  integer(kind=8)                            :: p, imax, ip, c1, c2, chunk, chunk2, off, off2
  type(particle),  pointer,     dimension(:) :: pa
  real(kind=4),    allocatable, dimension(:) :: scr
  integer(kind=2), allocatable, dimension(:) :: qscr
  integer(kind=8), allocatable, dimension(:) :: iscr, tnp
  real(kind=8)                               :: rq
  real,                         parameter    :: maxfrac = 0.01          ! max 1% outside
  integer,                      parameter    :: cb = 8                  ! bytes per counter
  real,                         external     :: sum_scalar
  real(kind=8)                               :: ttotal
  real(kind=8),                 external     :: wallclock
  integer(kind=8),              external     :: file_position, mask_i_flags
  integer(kind=8), save                      :: print_lim = 1000000000_8
  type(file_io_type)                         :: fio
  !
  !---------------------------------------------------------------------
  ! Open the particle dump, and get first position after the header 
  do irank=0,nodes-1
    ! open the file staggered to make the filesystem suffer less on big runs
    if (irank==rank) then
      open (data_unit,file=fname,form='unformatted',status='old',action='READ')
      call skip_header(lio_format)                                      ! assume everything ok
      allocate(tnp(nspecies))
      read(data_unit) tnp                                               ! read how many particles there are
      if (master) write(stdout,*) 'Reading ', tnp, ' particles from ' &
                        //trim(fname)//' with I/O format ',lio_format
      if (do_parallel_io) then
        if (master) p = file_position(data_unit)                        ! mpi pos == pos on disk + 0 per definition!!
        if (master .and. header_size > 0) p = header_size
        if (master) write(stdout,*) 'Reading from position ', p
        close(data_unit)
      else
        p = file_position(data_unit)+1_8                                ! stream pos == pos on disk + 1 per definition!!
        if (header_size > 0) p = header_size + 1_8
        if (master) write(stdout,*) 'Reading from position ', p
        close(data_unit)
        open(data_unit,file=fname,access='stream')                      ! reopen as stream to read chunks
      endif
    endif
#ifndef __xlc__
    call barrier()
#endif
  enddo

  gamma = sqrt(1.0_8 + real(vgamma*c%oc, kind=8)**2)                    ! calc gamma

  !---------------------------------------------------------------------
  ! The grand loop where we load in particles, lorentz transform, etc etc
  if (master) ttotal = wallclock()
  if (do_parallel_io) then
    allocate(fio%np(nspecies),fio%cnp(nspecies),fio%gnp(nspecies))
    call file_openr_seq(fname,p,'particles',fio)
  endif
  !
  do isp=1,nspecies
    if (tnp(isp) > print_lim .and. master) &
      write(stdout,'(a,i3)') 'Reading particles for species ', isp 
    off = rank * (tnp(isp) / nodes)                                     ! where to start reading
    np  = (tnp(isp) / nodes)                                            ! how many to read
    if (rank==nodes-1) np = tnp(isp)-off                                ! adjust end-node
    call load_balance_one_species(isp,np > sp(isp)%mp, &
                       max(np-sp(isp)%mp, 0),'read_particle_dump')      ! adjust part nr if needed
    sp(isp)%np = np
    if (tnp(isp)==0) cycle                                              ! cycle if no particles
    if (do_parallel_io) then 
      fio%np(isp)  = np
      fio%cnp(isp) = off
      fio%gnp(isp) = tnp(isp)
    endif
    ! + cb*2 and + cb because of Fortran counters (which we skip to be ok with >2GB blocks)
    chunk = tnp(isp)*4 + cb*2                                           ! size of one chunk
    chunk2= tnp(isp)*2 + cb*2                                           ! size of one integer(kind=2) chunk
    off2  = 2*off + cb                                                  ! offset in integer(kind=2)
    off   = 4*off + cb                                                  ! offset in bytes
    pa => sp(isp)%particle                                              ! shorthand
    if (np > 0) allocate(scr(np))                                       ! scratch array

    do i=1,mdim                                                         ! position
      if (do_parallel_io) then
        call file_read_seq_particles_real(scr, fio, isp)
      else
#ifdef __PGI
        if (master) then
          call file_seek(data_unit, p) 
          read(data_unit) c1
        endif
        if (np > 0) then
          call file_seek(data_unit, p+off) 
          read(data_unit) scr
        endif
#else
        if (master) read(data_unit, pos=p) c1
        if (np > 0) read(data_unit, pos=p+off) scr
#endif
      endif
      if (tnp(isp) > print_lim .and. master) write(stdout,'(a,i1)') 'Read positions dim ', i 
      do ip=1,np; pa(ip)%r(i) = scr(ip); enddo
      if (.not. do_parallel_io) then
        p = p + chunk                                                   ! inc one chunk
#ifdef __PGI
        if (master) then
          call file_seek(data_unit, p-cb) 
          read(data_unit) c2
        endif
#else
        if (master) read(data_unit, pos=p-cb) c2
#endif
        if (master) then; if (c1 .ne. c2) then
          print *, 'Position: isp, idim, Control blocks, chunk :', &
                  isp, i, c1, c2, chunk-cb*2
          call error('read_particle_dump', &
            'Fortran file corrupted. Either cb is wrong, '// &
            'or your chunk > 2GB and cb=4')
        endif; endif
      endif
    enddo

    if (lio_format >= 12) then                                          ! use new format, and read q's
      if (allocated(scr)) deallocate(scr)
      if (np > 0) allocate(qscr(np))                                    ! scratch array
      do i=1,mdim                                                       ! integer position
        if (do_parallel_io) then
          call file_read_seq_particles_int2(qscr, fio, isp)
        else
#ifdef __PGI
          if (master) then
            call file_seek(data_unit, p)
            read(data_unit) c1
          endif
          if (np > 0) then
            call file_seek(data_unit, p+off2) 
            read(data_unit) qscr
          endif
#else
          if (master) read(data_unit, pos=p) c1
          if (np > 0) read(data_unit, pos=p+off2) qscr
#endif
        endif
        if (tnp(isp) > print_lim .and. master) write(stdout,'(a,i1)') 'Read integer positions dim ', i 
        do ip=1,np; pa(ip)%q(i) = qscr(ip); enddo
        if (.not. do_parallel_io) then
          p = p + chunk2                                                ! inc one chunk
          if (master) then
#ifdef __PGI
            call file_seek(data_unit, p-cb) 
            read(data_unit) c2
#else
            read(data_unit, pos=p-cb) c2
#endif
            if (c1 .ne. c2) then
              print *, 'integer pos: isp, idim, Control blocks, chunk :', &
                      isp, i, c1, c2, chunk2-cb*2
              call error('read_particle_dump',&
                'Fortran file corrupted. Either cb is wrong, '// &
                'or your chunk > 2GB and cb=4')
            endif
          endif
        endif
      enddo
      if (allocated(qscr)) deallocate(qscr)
      if (vgamma .ne. 0.) then                                          ! lorentz trans
        do ip=1, np
          rq = (real(pa(ip)%r(3),kind=8)+ &
                real(pa(ip)%q(3),kind=8))/gamma + &                     ! contract by gamma
                 g%grlb(3)*(1./gamma - 1.)*g%ods(3)
          pa(ip)%q(3) = floor(rq)                                       ! integer cell
          pa(ip)%r(3) = rq - pa(ip)%q(3)                                ! fractional part
        enddo
      endif
    else
      if (master) &
        print *, 'Converting old position format to new position format'
      if (vgamma .ne. 0.) then                                          ! lorentz trans
        do ip=1, np
          pa(ip)%r(3) = pa(ip)%r(3)/gamma                               ! contract by gamma
        enddo
      endif
      call sp_global_to_cell_coordinates(isp)                           ! convert to new format
    endif

    nprint = 0
    do i=1,mdim                                                         ! chk out of bounds
      if (periodic(i)) cycle                                            ! but not periodic
      out_lb=0; out_ub=0                                                ! counters
      do ip=1,np                                                        ! loop of parts
        rq = (real(pa(ip)%r(i),kind=8)+&
              real(pa(ip)%q(i),kind=8))*g%ds(i)+g%grlb(i)               ! get global position
        if (rq < g%grlb(i)-3.0*g%ds(i)) then                            ! lower bndry
          out_lb = out_lb + 1 
          nprint = nprint + 1
          if (nprint <= maxprint .and. verbose > 1) write(stdout,*) &
            'read_particle_dump: rank, isp, dim, lb, pos:', &
            rank, isp, i, g%grlb(i), pa(ip)%r(i), pa(ip)%q(i), rq
        endif
        if (rq >= g%grub(i)+3.0*g%ds(i)) then                           ! upper bndry
          out_ub = out_ub + 1 
          nprint = nprint + 1
          if (nprint <= maxprint .and. verbose > 1) write(stdout,*) &
            'read_particle_dump: rank, isp, dim, ub, pos:', &
            rank, isp, i, g%grub(i), pa(ip)%r(i), pa(ip)%q(i), rq
        endif
      enddo
      global_frac = sum_scalar(real(out_lb+out_ub)) / tnp(isp)
      if (out_lb > 0 .and. (verbose > 0 .or. global_frac > maxfrac)) write(stdout,*) & ! write stats
        'read_particle_dump: rank, isp, dim, out_lb:', rank, isp, i, out_lb
      if (out_ub > 0 .and. (verbose > 0 .or. global_frac > maxfrac)) write(stdout,*) &
        'read_particle_dump: rank, isp, dim, out_ub:', rank, isp, i, out_ub
      if (global_frac > maxfrac) then
        call barrier
#ifndef __xlc__
        call sleep(1)
#endif
        call barrier
        if (master) then
          write(stdout,*) 'isp, dim, #outside, fraction outside, tnp:', &
                          isp, i, int(global_frac*tnp(isp)), global_frac, tnp(isp)
          call error('read_particle_dump','Too many particles outside bounds')
        endif
      endif
    enddo

    if (np > 0 .and. lio_format >= 12) allocate(scr(np))                ! scratch array

    do i=1,mcoord                                                       ! four velocity
      if (do_parallel_io) then
        call file_read_seq_particles_real(scr, fio, isp)
      else
#ifdef __PGI
        if (master) then
          call file_seek(data_unit, p) 
          read(data_unit) c1
        endif
        if (np > 0) then
          call file_seek(data_unit, p+off) 
          read(data_unit) scr
        endif
#else
        if (master) read(data_unit, pos=p) c1
        if (np > 0) read(data_unit, pos=p+off) scr
#endif
      endif
      if (tnp(isp) > print_lim .and. master) write(stdout,'(a,i1)') 'Read velocities dim ', i 
      do ip=1,np; pa(ip)%p(i) = scr(ip); enddo
      if (.not. do_parallel_io) then
        p = p + chunk                                                   ! inc one chunk
        if (master) then
#ifdef __PGI
          call file_seek(data_unit, p-cb) 
          read(data_unit) c2
#else
          read(data_unit, pos=p-cb) c2
#endif
          if (c1 .ne. c2) then
            print *, 'Momentum: isp, icoord, Control blocks, chunk :', isp, i, c1, c2, chunk-cb*2
            call error('read_particle_dump',&
              'Fortran file corrupted. Either cb is wrong, or your chunk > 2GB and cb=4')
          endif
        endif
      endif
    enddo
    if (vgamma .ne. 0.) then                                            ! lorentz trans
      do ip=1, np
        pa(ip)%p(3) = gamma*pa(ip)%p(3) + &                             ! vg = Gvg' + g'VG
              sqrt(1.0_8 + sum(real(pa(ip)%p*c%oc,kind=8)**2))*vgamma   ! g' = sqrt(1 + vg'^2)
      enddo
    endif

    ! we can skip energy, and calculate it later
    if (do_parallel_io) then
      fio%position = fio%position + chunk
    else
      p = p + chunk                                                     ! inc one chunk
    endif

    if (do_parallel_io) then
      call file_read_seq_particles_real(scr, fio, isp)
    else
#ifdef __PGI
      if (master) then
        call file_seek(data_unit, p) 
        read(data_unit) c1
      endif
      if (np > 0) then
        call file_seek(data_unit, p+off) 
        read(data_unit) scr
      endif
#else
      if (master) read(data_unit, pos=p) c1
      if (np > 0) read(data_unit, pos=p+off) scr
#endif
    endif
    if (tnp(isp) > print_lim .and. master) write(stdout,'(a)') 'Read weights'
    do ip=1,np; pa(ip)%w = scr(ip); enddo
    if (.not. do_parallel_io) then
      p = p + chunk                                                     ! inc one chunk
      if (master) then
#ifdef __PGI
        call file_seek(data_unit, p-cb) 
        read(data_unit) c2
#else
        read(data_unit, pos=p-cb) c2
#endif
        if (c1 .ne. c2) then
          print *, 'Weights: isp, Control blocks, chunk :', isp, c1, c2, chunk-cb*2
          call error('read_particle_dump',&
            'Fortran file corrupted. Either cb is wrong, or your chunk > 2GB and cb=4')
        endif
      endif
    endif
    if (vgamma .ne. 0.) then                                            ! lorentz trans
      do ip=1, np
        pa(ip)%w = pa(ip)%w * gamma                                     ! lorentz stretch
      enddo
    endif
    if (allocated(scr)) deallocate(scr)


    if (np > 0) allocate(iscr(np))
    if (do_parallel_io) then
      call file_read_seq_particles_int8(iscr, fio, isp)
    else
#ifdef __PGI
      if (master) then
        call file_seek(data_unit, p) 
        read(data_unit) c1
      endif
#else
      if (master) read(data_unit, pos=p) c1
#endif
      if (np > 0) then
#ifdef __PGI
        call file_seek(data_unit, p+off)
        read(data_unit) iscr
#else
        read(data_unit, pos=p+off) iscr
#endif
      endif
    endif
    if (tnp(isp) > print_lim .and. master) write(stdout,'(a)') 'Read indices'
    imax = 0_8
    do ip=1,np; pa(ip)%i = iscr(ip); imax = max(imax, mask_i_flags(pa(ip)%i)); enddo
    sp(isp)%imax = imax
    if (np > 0) deallocate(iscr)
    if (.not. do_parallel_io) then
      p = p + 2*chunk - 2*cb                                            ! inc two chunks
      if (master) then
#ifdef __PGI
        call file_seek(data_unit, p-cb) 
        read(data_unit) c2
#else
        read(data_unit, pos=p-cb) c2
#endif
        if (c1 .ne. c2) then
          print *, 'Indices: isp, Control blocks, chunk :', isp, c1, c2, 2*(chunk-cb*2)
          call error('read_particle_dump',&
            'Fortran file corrupted. Either cb is wrong, or your chunk > 2GB and cb=4')
        endif
      endif
    endif

  enddo                                                                 ! end of isp-loop over species
  if (master) then
    ttotal = wallclock() - ttotal
    if (do_validate) ttotal=1e30
    write(stdout,*) 'Read restart particle snapshot with ', &
      sum(tnp)*nspecies*(mdim*(4+merge(2,0,lio_format >= 12)) + mcoord*4 + 4 + 8)/(1024.*1024.*ttotal), 'MB/s'
  endif
  if (do_parallel_io) then                                              ! done reading
    call file_close(fio)
  else
    close(data_unit)
  endif

  call highest_particle_id                                              ! fix what is the highest id in use

  !---------------------------------------------------------------------
  ! get particle ids right
  do isp = 1,nspecies
    if (rank==0) imax = sp(isp)%imax                                    ! starting point
    if (rank > 0) call mpi_recv_integer8(rank-1,1,imax)                 ! recv cumulative imax
    imax = imax + sp(isp)%mp - sp(isp)%np                               ! inc for this thread for max id used
    if (rank < nodes-1) call mpi_send_integer8(rank+1,1,imax)           ! send to next thread
                               if (verbose > 0) print *, '1) rank, imax :', isp, rank, imax
    imax = imax - sp(isp)%mp                                            ! zero based starting point for ids
    do ip=sp(isp)%np+1, sp(isp)%mp
      sp(isp)%particle(ip)%i = imax + ip
    enddo
    if (sp(isp)%mp > 0) then
      sp(isp)%imax = sp(isp)%particle(sp(isp)%mp)%i
    else
      sp(isp)%imax = imax
    endif 
                               if (verbose > 0) print *, '2) rank, imax :', isp, rank, sp(isp)%imax 
    do ip=sp(isp)%np+1, sp(isp)%mp
      !call mark_synthetic_particle (isp,ip,g%lb(3))                     ! mark for synthetic spectra
      sp(isp)%particle(ip)%w = 0.0                                      ! make sure weight is zeroed
    enddo
  enddo
  call highest_particle_id                                              ! fix what is the highest id in use

  !---------------------------------------------------------------------
  ! distribute particles to correct threads 
  call ParticleTotal
  !call particle_statistics()
  gsend=1
  i = 0
  do while (gsend > 0)                                                  ! distribute correctly
    i = i + 1
    call SendParticles_lowmem(gsend)                                    ! max mpi%n-1 hops
    if ((master .or. verbose > 0) .and. gsend > 0) write(stdout,'(a,i5.5,a,i4,a,i10)') &
                   'Rank=',rank,' Sending particles. It=',i, ' #send=', gsend
  enddo
  
  call sort
  call particle_statistics()

END SUBROUTINE read_particle_dump

! Read x-y-z chunk from field data
!=======================================================================
SUBROUTINE read_field_array_new(p, name, f)
  USE params,  only : mpi,rank,nodes,mdim,mcoord,data_unit,master,stdout,periodic
  USE grid_m,    only : g
  implicit none
  integer(kind=8)                          :: p, c18, c28, bytes_expected, xychunk
  character(len=*),     intent(in)         :: name
  real, dimension(g%n(1),g%n(2),g%n(3))    :: f
  integer                                  :: iz,jz,c14,c24,i(4),ir
  integer,                  dimension(mdim):: lb, ub, ubl, gn
  real,        allocatable, dimension(:)   :: scr
  real(kind=4),allocatable, dimension(:,:) :: zsl
  real,        allocatable, dimension(:,:) :: patch
  integer,     allocatable, dimension(:,:) :: ii
  integer                                  :: cb = 4                       ! bytes per counter

  if (master) print *, 'Reading '//trim(name)
  gn = merge(g%gn,g%gn+1,periodic)                                      ! check if physical bndry point is present
  ubl= merge(g%ub+1,g%ub,mpi%ub)                                        ! check if physical bndry point is present
  lb = g%lb + mpi%offset; ub = lb + g%ub - g%lb + merge(1,0,mpi%ub)     ! global bndry
  xychunk = 4*gn(1)*gn(2)                                               ! one plane

  c18=0; c28=0; c14=0; c24=0                                            ! zero counters
  bytes_expected = product(int(gn,kind=8))*4_8
  ! Read first counter and try to autodetect 4 or 8 byte counters
  if (cb .eq. 4) then
    read(data_unit, pos=p) c14                                          ! fortran counter
    if (c14 .ne. bytes_expected) then
      read(data_unit, pos=p) c18                                        ! fortran counter
      if (master) write(stdout,*) 'Changing to 64bit counters. New, Old, Expected, global-grid : ', &
                                  c18, c14, bytes_expected, gn
      c14=0; cb=8
    endif
  else
    read(data_unit, pos=p) c18                                                  ! fortran counter
    if (c18 .ne. bytes_expected) then
      read(data_unit, pos=p) c14                                                ! fortran counter
      if (master) write(stdout,*) 'Changing to 32bit counters. New, Old, Expected, global-grid : ', &
                                  c14, c18, bytes_expected, gn
      c18=0; cb=4
    endif
  endif
  p = p + cb
  jz = g%lb(3)
  i = (/ lb(1)+1, lb(2)+1, &                                                    ! offset in global grid
         ub(1) - lb(1), ub(2) - lb(2) /)                                        ! size of patch
  if (mpi%xy%master) then
    allocate(zsl(gn(1),gn(2)),ii(4,0:mpi%xy%nodes-1))
    ii(:,0) = i
    do ir=1, mpi%xy%nodes-1
      call mpi_recv_integer_xy(ir,4,i)
      ii(:,ir) = i
    enddo
    i = ii(:,0)
  else
    allocate(patch(ub(1)-lb(1), ub(2)-lb(2)))
    call mpi_send_integer_xy(0,4,i)
  endif

  do iz=0,gn(3)-1                                                               ! z-loop
    if (iz >= lb(3) .and. iz < ub(3)) then                                      ! our slice ?
      if (mpi%xy%master) then                                                   ! I have to read
        read(data_unit, pos=p) zsl                                              ! read a full xy-slice
        f(g%lb(1):ubl(1)-1,g%lb(2):ubl(2)-1,jz) = zsl(1:i(3),1:i(4))            ! own part
        do ir=1,mpi%xy%nodes-1                                                  ! distribute the rest
          allocate(patch(ii(3,ir),ii(4,ir)))                                    ! patch to send
          patch = zsl(ii(1,ir):ii(1,ir)+ii(3,ir)-1,ii(2,ir):ii(2,ir)+ii(4,ir)-1)! copy data
          call mpi_send_real_xy(ir,ii(3,ir)*ii(4,ir),patch)                     ! send it
          deallocate(patch)
        enddo
      else
        call mpi_recv_real_xy(0,i(3)*i(4),patch)
        f(g%lb(1):ubl(1)-1,g%lb(2):ubl(2)-1,jz) = patch
      endif
      jz = jz+1                                                                 ! inc slice
      if (mpi%n(3)==1) call barrier()                                           ! if only xy-threads guard mpi mem-buffs
    endif
    p = p + xychunk                                                             ! next slice
  enddo
  if (cb .eq. 4) then
    read(data_unit, pos=p) c24
  else
    read(data_unit, pos=p) c28
  endif
  p = p + cb
  if (c14 .ne. c24 .or. c18 .ne. c28) call error('read_field_array_new', &
                       'Fortran counters do not match for '//trim(name))
  if (mpi%xy%master) then
    deallocate(zsl,ii)
  else
    deallocate(patch)
  endif

END SUBROUTINE read_field_array_new

! Read x-y-z chunk from field data
!=======================================================================
SUBROUTINE read_field_array(p, name, f)
  USE params,  only : mpi,rank,nodes,mdim,mcoord,data_unit,master,stdout,periodic
  USE grid_m,    only : g
  implicit none
  integer(kind=8)                         :: p, c18, c28, bytes_expected, &
                                             off, xychunk, xchunk
  character(len=*),     intent(in)        :: name
  real, dimension(g%n(1),g%n(2),g%n(3))   :: f
  integer                                 :: ix,iy,iz,jy,jz,c14,c24
  integer,                dimension(mdim) :: lb, ub, gn, ubl
  real(kind=4), allocatable, dimension(:) :: scr
  integer                                 :: cb = 4                     ! bytes per counter

  if (master) print *, 'Reading '//trim(name)
  gn = merge(g%gn,g%gn+1,periodic)                                      ! check if physical boundary point is present
  ubl= merge(g%ub+1, g%ub, mpi%ub)                                      ! check if physical boundary point is present
  lb = g%lb + mpi%offset; ub = lb + g%ub - g%lb + merge(1,0,mpi%ub)     ! global bndry
  allocate(scr(1:ub(1)-lb(1)))                                          ! pencil
  xychunk = 4*gn(1)*gn(2)                                               ! one plane
  xchunk  = 4*gn(1)                                                     ! one pencil
  off     = 4*lb(1)                                                     ! offset in pencil

  c18=0; c28=0; c14=0; c24=0                                            ! zero counters
  bytes_expected = product(int(gn,kind=8))*4_8
  ! Read first counter and try to autodetect 4 or 8 byte counters
  if (cb .eq. 4) then
#ifdef __PGI
    call file_seek(data_unit, p) 
    read(data_unit) c14
#else
    read(data_unit, pos=p) c14
#endif
    if (c14 .ne. bytes_expected) then
#ifdef __PGI
      call file_seek(data_unit, p) 
      read(data_unit) c18
#else
      read(data_unit, pos=p) c18
#endif
      if (master) write(stdout,*) 'Changing to 64bit counters. New, Old, Expected, global-grid : ', &
                                  c18, c14, bytes_expected, gn
      c14=0; cb=8
    endif
  else
#ifdef __PGI
    call file_seek(data_unit, p) 
    read(data_unit) c18
#else
    read(data_unit, pos=p) c18
#endif
    if (c18 .ne. bytes_expected) then
#ifdef __PGI
      call file_seek(data_unit, p) 
      read(data_unit) c14
#else
      read(data_unit, pos=p) c14
#endif
      if (master) write(stdout,*) 'Changing to 32bit counters. New, Old, Expected, global-grid : ', &
                                  c14, c18, bytes_expected, gn
      c18=0; cb=4
    endif
  endif
  p = p + cb
  jz = g%lb(3)
  do iz=0,gn(3)-1                                                       ! z-loop
    if (iz >= lb(3) .and. iz < ub(3)) then                              ! our slice ?
      jy = g%lb(2)
      do iy=0,gn(2)-1                                                   ! y-loop
        if (iy >= lb(2) .and. iy < ub(2)) then                          ! our pencil ?
#ifdef __PGI
          call file_seek(data_unit, p+off)
          read(data_unit) scr
#else
          read(data_unit, pos=p+off) scr
#endif
          f(g%lb(1):ubl(1)-1,jy,jz) = scr
          jy = jy + 1
        endif
        p = p + xchunk                                                  ! skip pencil
      enddo
      jz = jz+1                                                         ! inc slice
    else
      p = p + xychunk                                                   ! skip slice
    endif
  enddo
#ifdef __PGI
  call file_seek(data_unit, p)
  if (cb .eq. 4) then
    read(data_unit) c24
  else
    read(data_unit) c28
  endif
#else
  if (cb .eq. 4) then
    read(data_unit, pos=p) c24
  else
    read(data_unit, pos=p) c28
  endif
#endif
  p = p + cb
  if (c14 .ne. c24 .or. c18 .ne. c28) call error('read_field_array', &
                       'Fortran counters do not match for '//trim(name))

END SUBROUTINE read_field_array

!=======================================================================
SUBROUTINE read_field_dump(fname,vgamma,header_size)
  USE params,  only : mpi, rank, nodes, mdim, mcoord, stdout, master, &
                      data_unit, time, periodic, do_parallel_io, &
                      do_validate
  USE grid_m,    only : g, bx, by, bz, ex, ey, ez
  USE units,   only : c
  USE pic_io,  only : oldftime, oldptime, oldfctime, file_io_type
  implicit none
  character(len=*),  intent(in)       :: fname
  real,              intent(in)       :: vgamma
  integer,           intent(in)       :: header_size
  real                                :: gamma
  integer(kind=8)                     :: p
  logical                             :: do_em
  integer                             :: ix,iy,iz,irank,lio_format
  integer,           dimension(mdim)  :: lb,ub,gn,gn_here
  real, allocatable, dimension(:,:,:) :: exi,eyi,bxi,byi
  integer(kind=8),   external         :: file_position
  real(kind=8),      external         :: wallclock
  real(kind=8)                        :: ttotal,t0,t1,t2
  type(file_io_type)                  :: fio
  !
  gn_here = merge(g%gn,g%gn+1,periodic)                                 ! check if physical bndry point is needed
  do irank=0,nodes-1
    ! open the file staggered to make the filesystem suffer less on big runs
    if (irank==rank) then
      open (data_unit,file=fname,form='unformatted',status='old')
      call skip_header(lio_format)                                      ! assume everything ok
      read(data_unit) do_em, lb, ub                                     ! read general data
      gn = ub - lb
      if (do_parallel_io) then
        if (master) p = file_position(data_unit)                        ! mpi pos == pos on disk + 0 per definition!!
        if (master .and. header_size > 0) p = header_size + 12_8
        if (master) write(stdout,*) 'Reading from position ', p
        close(data_unit)
      else
        p = file_position(data_unit)+1_8                                ! stream pos == pos on disk + 1 per definition!!
        if (header_size > 0) p = header_size + 12_8 + 1_8
        if (master) write(stdout,*) 'Reading from position ', p
        close(data_unit)
        open(data_unit,file=fname,access='stream',position='ASIS')      ! reopen as stream to read chunks
      endif
    endif
#ifndef __xlc__
    call barrier()
#endif
  enddo

  if (.not. do_em) return                                               ! no EM fields in snapshot

  if (any(gn .ne. gn_here) .and. master) then                           ! oops bad grid
    write(stdout,*) 'Global grid in run  :', gn_here, g%gn, periodic
    write(stdout,*) 'Global grid in file :', gn
    write(stdout,*) 'Field filename      : '//trim(fname)
    !call error('read_field_dump', 'Grid sizes do not match')
    write(stdout,*) 'Resetting gn, trusting your input file; things may go very wrong'
    gn = gn_here
  endif

  if (master) ttotal = wallclock()
  if (do_parallel_io) then 
    call file_openr_seq(fname,p,'field',fio)
    call file_read_seq (Bx, fio)
    call file_read_seq (By, fio)
    call file_read_seq (Bz, fio)
    call file_read_seq (Ex, fio)
    call file_read_seq (Ey, fio)
    call file_read_seq (Ez, fio)
    call file_close(fio)
  else
    ! automatically select the faster method for this file system
    t0 = wallclock()
    call read_field_array_new(p, 'Bx', bx)                                ! xy-master reads, and use mpi-comm
    t1 = wallclock(); t0 = t1 - t0
    call mpi_sum_real(t0,t2); t0 = t2
    call read_field_array(p, 'By', by)                                    ! direct reading, no mpi-comm
    t1 = wallclock() - t1
    call mpi_sum_real(t1,t2); t1 = t2
    if (t1 > t0) then                                                     ! mpi-comm method faster
      call read_field_array_new(p, 'Bz', bz)
      call read_field_array_new(p, 'Ex', ex)
      call read_field_array_new(p, 'Ey', ey)
      call read_field_array_new(p, 'Ez', ez)
    else                                                                  ! direct reading faster
      call read_field_array(p, 'Bz', bz)
      call read_field_array(p, 'Ex', ex)
      call read_field_array(p, 'Ey', ey)
      call read_field_array(p, 'Ez', ez)
    endif
    close(data_unit)                                                      ! done reading
  endif
  if (master) then
    ttotal = wallclock()-ttotal
    if (do_validate) ttotal=1e30
    write(stdout,*) 'Read restart field data with ',&
      product(real(g%gn,kind=8))*6.*4. / (1024.**2*ttotal), ' MB/s'
  endif

  ! Lorentz transform fields
  !---------------------------------------------------------------------
  gamma = sqrt(1.0_8 + real(vgamma, kind=8)**2)                         ! calc gamma
  if (vgamma .ne. 0) then
    ! fix mpi boundaries with call to overlap
    call overlap(Bx); call overlap(By); call overlap(Bz)
    call overlap(Ex); call overlap(Ey); call overlap(Ez)
    ! scratch fields for interpolation
    allocate(exi(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1), &
             eyi(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1), &
             bxi(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1), &
             byi(g%lb(1):g%ub(1)-1,g%lb(2):g%ub(2)-1,g%lb(3):g%ub(3)-1))
    ! simple fix for physical boundaries
    if (mpi%lb(3)) then                                                 ! we have lb
      bx(:,:,g%lb(3)-1) = bx(:,:,g%lb(3))
      by(:,:,g%lb(3)-1) = by(:,:,g%lb(3))
    endif
    if (mpi%ub(3)) then                                                 ! we have ub
      ex(:,:,g%ub(3)+1) = ex(:,:,g%ub(3))
      ey(:,:,g%ub(3)+1) = ey(:,:,g%ub(3))
    endif
    ! interpolate half point up and down
    do iz=g%lb(3), g%ub(3); do iy=g%lb(2), g%ub(2); do ix=g%lb(1), g%ub(1) 
      exi(ix,iy,iz) = 0.5*(ex(ix,iy,iz) + ex(ix,iy,iz+1))
      eyi(ix,iy,iz) = 0.5*(ey(ix,iy,iz) + ey(ix,iy,iz+1))
      bxi(ix,iy,iz) = 0.5*(bx(ix,iy,iz) + bx(ix,iy,iz-1))
      byi(ix,iy,iz) = 0.5*(by(ix,iy,iz) + by(ix,iy,iz-1))
    enddo; enddo; enddo
    ! Lorentz transform
    do iz=g%lb(3), g%ub(3); do iy=g%lb(2), g%ub(2); do ix=g%lb(1), g%ub(1)
      ex(ix,iy,iz) = gamma*ex(ix,iy,iz) + vgamma*byi(ix,iy,iz)
      ey(ix,iy,iz) = gamma*ey(ix,iy,iz) - vgamma*bxi(ix,iy,iz)
      bx(ix,iy,iz) = gamma*bx(ix,iy,iz) - vgamma*eyi(ix,iy,iz)*c%oc2
      by(ix,iy,iz) = gamma*by(ix,iy,iz) + vgamma*exi(ix,iy,iz)*c%oc2
    enddo; enddo; enddo
    deallocate(exi,eyi,bxi,byi)
  endif

  ! fix boundaries, inclusive the physical boundaries
  call e_boundaries(ex, ey, ez)
  call b_boundaries(bx, by, bz)
END SUBROUTINE read_field_dump

!=======================================================================
SUBROUTINE write_particles(cdump,ptime,do_restart)
  USE params,      only : do_validate
  USE pic_io,      only : indices,write_ioarray_part,file_io_type
  USE params,      only : data_unit,nspecies,mdim,mcoord,mfile,rank,nodes, &
                          datadir,writefac,stdall,master,do_parallel_io
  USE grid_m,        only : g
  USE species,     only : sp, particle
  implicit none
  character(len=6), intent(in)            :: cdump
  character(len=4)                        :: ndump
  real(kind=8),     intent(in)            :: ptime
  integer,          intent(in)            :: do_restart
  character(len=mfile)                    :: pfile
  integer                                 :: isp, idim, ip, iwp, cnt, irank
  real(kind=4), allocatable, dimension(:) :: scr
  integer*2,    allocatable, dimension(:) :: qscr
  integer,      allocatable, dimension(:) :: idx
  integer*8,    allocatable, dimension(:) :: iscr, wnp, cwnp,gwnp, writefac_local
  integer*8                               :: p
  integer, parameter                      :: verbose=0
  type(particle), dimension(:),pointer    :: pa
  type(file_io_type)                      :: fio
  logical                                 :: dump_all_particles, do_parallel_io_old
  logical, external                       :: file_exists
  integer(kind=8), external               :: mask_i_flags, file_position
  real(kind=8)                            :: twrite
  real(kind=8), external                  :: wallclock
  character(len=16)                       :: message
  ! currently we can only write from more threads using stream I/O or MPI-IO

  pfile=trim(datadir)//'/particles-'//cdump//'.dat'
  if (file_exists(trim(datadir)//'/dump_all_particles') .or. &
                                              do_restart==1) then       ! dump all particles for this timestep ?
    allocate(writefac_local(nspecies))
    writefac_local = writefac                                           ! store value of writefac
    writefac = 1                                                        ! set it to 1 
    dump_all_particles = .true.
    call barrier()
    if (master .and. file_exists(trim(datadir)//'/dump_all_particles')) &
                      call file_delete(trim(datadir)//'/dump_all_particles')
    call barrier()
#ifndef __xlc__
    call sleep(1)                                                       ! give cluster filesystem some time
#endif
  else
    dump_all_particles = .false.
  endif
    
  if (master) then
    twrite = wallclock()
    open (data_unit,file=pfile,form='unformatted')                      ! write header the usual way
    call write_header (ptime)
  endif
  allocate(wnp(nspecies),cwnp(nspecies),gwnp(nspecies))
  do isp=1,nspecies                                                     ! how many to dump on node
    wnp(isp) = 0
    pa => sp(isp)%particle                                              ! shorthand
    do ip=1,sp(isp)%np
      wnp(isp) = wnp(isp) + merge(1_8 , 0_8 , &
                   modulo(mask_i_flags(pa(ip)%i),writefac(isp))==0)
    enddo
                               if(verbose>0)write(stdall,*)"WP isp,wnp,np:",isp,wnp(isp),sp(isp)%np
  enddo

  ! Get the total nr of particles, and the total nr of particles on threads with lower rank
  gwnp = wnp
  call mpi_sum_integer8s (nspecies, gwnp)
  cwnp = 0
  if (rank > 0)       call mpi_recv_integer8(rank-1,nspecies,cwnp)
  cwnp = cwnp + wnp
  if (rank < nodes-1) call mpi_send_integer8(rank+1,nspecies,cwnp)
  cwnp = cwnp - wnp

  if (master) then
    write(data_unit) gwnp
  endif

  if (do_parallel_io) then
    if (master) p = file_position(data_unit)
    if (master) close (data_unit)
    allocate(fio%np(nspecies),fio%cnp(nspecies),fio%gnp(nspecies))
    fio%np  = wnp
    fio%cnp = cwnp
    fio%gnp = gwnp
    call file_openw_seq (pfile,p,'particles',fio)
  else
    if (master) close(data_unit)
    !---------------------------------------------------------------------
    ! Reopen the particle snapshot, and seek to end of file
    do irank=0,nodes-1
      ! open the file staggered to make the filesystem suffer less on big runs
      if (irank==rank) then
#ifdef __PGI
        open (data_unit,file=pfile,form='unformatted',access='stream')            ! reopen with stream acces to write in chunks
        call file_append(data_unit)
#else
        open (data_unit,file=pfile,form='unformatted',access='stream',position='append')! reopen with stream acces to write in chunks
#endif
        p = file_position(data_unit)+1                                            ! stream pos == pos on disk + 1 per definition!!
      endif
      call barrier()                                                              ! make sure all threads are in sync
    enddo
  endif

  do isp=1,nspecies
    if (gwnp(isp) .eq. 0_8) cycle                                               ! no parts in species in this IO block
    pa => sp(isp)%particle                                                      ! shorthand
    if (wnp(isp) .gt. 0_8) then
      allocate(idx(wnp(isp)))                                                   ! get idx of particles
      iwp = 0
      do ip=1,sp(isp)%np
        if (modulo(mask_i_flags(pa(ip)%i),writefac(isp))==0) then
          iwp = iwp + 1
          idx(iwp) = ip
        endif
      enddo
      allocate(scr(wnp(isp)))
    endif
    do idim=1,mdim
      do iwp=1,wnp(isp); scr(iwp) = pa(idx(iwp))%r(idim); enddo
      if (do_parallel_io) then
        call file_write_seq_particles_real(scr,fio,isp)
      else
        call write_ioarray_part(p,wnp(isp),cwnp(isp),gwnp(isp),scr)
      endif
    enddo
    if (wnp(isp) .gt. 0_8) then
      deallocate(scr)
      allocate(qscr(wnp(isp)))
    endif
    do idim=1,mdim
      do iwp=1,wnp(isp); qscr(iwp) = pa(idx(iwp))%q(idim); enddo
      if (do_parallel_io) then
        call file_write_seq_particles_int2(qscr,fio,isp)
      else
        call write_ioarray_part(p,wnp(isp),cwnp(isp),gwnp(isp),qscr)
      endif
    enddo
    if (wnp(isp) .gt. 0_8) then
      deallocate(qscr)
      allocate(scr(wnp(isp)))
    endif
    do idim=1,mcoord
      do iwp=1,wnp(isp); scr(iwp) = pa(idx(iwp))%p(idim); enddo
      if (do_parallel_io) then
        call file_write_seq_particles_real(scr,fio,isp)
      else
        call write_ioarray_part(p,wnp(isp),cwnp(isp),gwnp(isp),scr)
      endif
    enddo
    do iwp=1,wnp(isp); scr(iwp) = pa(idx(iwp))%e; enddo
    if (do_parallel_io) then
      call file_write_seq_particles_real(scr,fio,isp)
    else
      call write_ioarray_part(p,wnp(isp),cwnp(isp),gwnp(isp),scr)
    endif
    do iwp=1,wnp(isp); scr(iwp) = pa(idx(iwp))%w; enddo
    if (do_parallel_io) then
      call file_write_seq_particles_real(scr,fio,isp)
    else
      call write_ioarray_part(p,wnp(isp),cwnp(isp),gwnp(isp),scr)
    endif
    if (wnp(isp) .gt. 0_8) deallocate(scr)

    if(indices)then
      if (wnp(isp) .gt. 0_8) allocate(iscr(wnp(isp)))
      do iwp=1,wnp(isp); iscr(iwp) = pa(idx(iwp))%i; enddo
      if (do_parallel_io) then
        call file_write_seq_particles_int8(iscr,fio,isp)
      else
        call write_ioarray_part(p,wnp(isp),cwnp(isp),gwnp(isp),iscr)
      endif
      if (wnp(isp) .gt. 0_8) deallocate(iscr)
    endif

    if (wnp(isp) .gt. 0_8) deallocate(idx)
  enddo
  if (do_parallel_io) then
    call file_close(fio)
  else
    close(data_unit)
  endif
  call barrier()
  if (master) call scp(pfile)
  call barrier()
  if (dump_all_particles) then 
    writefac = writefac_local                                           ! restore value of writefac
    deallocate(writefac_local)
  endif
  !
  if (master) then
    twrite = max(wallclock() - twrite,1e-7)
    if (do_validate) twrite = 1e30
    !                        pos  + vel +   w+e + qpos     + indices
    twrite = sum(gwnp)*(4_8*(mdim + mcoord + 2) + 2_8*mdim + &
             merge(8_8,0_8,indices))/twrite/1024. ! speed in KB/s
    if (twrite > 1024.) then                                                    ! speed > 1 MB/s
      twrite = twrite / 1024.
      if (do_validate) twrite = 0.0
      if (twrite > 1024.)  then                                                 ! speed > 1 GB/s
        twrite = twrite / 1024.
        write(message,'(a4,f7.2,a5)') ' PIO ',twrite,' GB/s'
      else
        write(message,'(a4,f7.2,a5)') ' PIO ',twrite,' MB/s'
      endif
    else
      write(message,'(a4,f7.2,a5)') ' PIO ',twrite,' KB/s'
    endif
    call extra_info(message)
  endif
END SUBROUTINE write_particles

!=======================================================================
SUBROUTINE write_fields(cdump,ptime)
  USE pic_io,      only : verbose,file_io_type
  USE params,      only : data_unit,mfile,mcoord,mid,nspecies,rank, &
                          datadir,nodes,time,stdout,do_maxwell,periodic,    &
                          do_lorentz, do_vth, master, do_parallel_io, &
                          do_overlap_add_at_io, do_validate
  USE grid_m,        only : g,Bx,By,Bz,Ex,Ey,Ez
  USE species,     only : fields
  USE units,       only : c
  implicit none
  character(len=6), intent(in)  :: cdump
  character(len=4)              :: ndump
  real(kind=8),     intent(in)  :: ptime
  character(len=mfile)          :: pfile
  integer                       :: cnt, isp, icoord
  real,allocatable,dimension(:,:,:) :: scr, d
  logical, save                 :: first_dump=.true.
  logical                       :: do_em
  type(file_io_type)            :: fio
  integer(kind=8)               :: position
  integer(kind=8),   external   :: file_position
  real(kind=8)                  :: twrite
  real(kind=8), external        :: wallclock
  character(len=16)             :: message
!.......................................................................
  call trace_enter ('WRITE_FIELDS')

  pfile=trim(datadir)//'/fields-'//cdump//'.dat'
  do_em = do_maxwell .or. (do_lorentz .and. first_dump)                 ! are we dumping em fields ?

  !---------------------------------------------------------------------
  ! Write header and the global size of a single io chunk 
  if (master) then
    twrite = wallclock()
    if (do_validate) twrite = 1e30
    open (data_unit,file=pfile,form='unformatted')                      ! write header the usual way
    call write_header (ptime)
    ! Write the global size and type of a single io chunk, filetype=1 
    write(data_unit) do_em, g%lb, g%lb+g%gn+merge(0,1,periodic)         ! lb and ub for whole chunk
  endif

  if (do_parallel_io) then
    position = file_position(data_unit)
    if (master) close (data_unit)
    call file_openw_seq (pfile,position,'field',fio)
  else
    if (master) then
      close(data_unit)
#ifdef __PGI
    open (data_unit,file=pfile,form='unformatted', &                    ! reopen with stream acces to write in chunks
                    access='stream')
    call file_append(data_unit)
#else
    open (data_unit,file=pfile,form='unformatted', &                    ! reopen with stream acces to write in chunks
                    access='stream',position='append')
#endif
    endif
  endif

  if (do_em) then
    if (do_parallel_io) then
      call file_write_seq (Bx, fio)
      call file_write_seq (By, fio)
      call file_write_seq (Bz, fio)
      call file_write_seq (Ex, fio)
      call file_write_seq (Ey, fio)
      call file_write_seq (Ez, fio)
    else
      call write_iofield(Bx)
      call write_iofield(By)
      call write_iofield(Bz)
      call write_iofield(Ex)
      call write_iofield(Ey)
      call write_iofield(Ez)
    endif
  endif 

  allocate (d(g%n(1),g%n(2),g%n(3)))
  do isp=1,nspecies
    d = fields(:,:,:,isp)%d
    if (do_overlap_add_at_io) call overlap_add(d)
    if (do_parallel_io) then
      call file_write_seq (d, fio)
    else
      call write_iofield(d)
    endif
    do icoord=1,mcoord
      d = fields(:,:,:,isp)%v(icoord)
      if (do_overlap_add_at_io) call overlap_add(d)
      if (do_parallel_io) then
        call file_write_seq (d, fio)
      else
        call write_iofield(d)
      endif
    end do
    if (do_vth) then
      d = fields(:,:,:,isp)%vth
      if (do_parallel_io) then
        call file_write_seq (d, fio)
      else
        call write_iofield(d)
      endif
    endif
  end do
  deallocate (d)

  if (do_parallel_io) then
    call file_close(fio)
  else
    if (master) close(data_unit)
  endif
  if (master) call scp(pfile)
  !
  if (master) then
    twrite = max(wallclock() - twrite,1e-7)
    if (do_validate) twrite = 1e30
    twrite = 4_8*product(int(g%gn+merge(0,1,periodic),kind=8)) * &
      (merge(6_8,0_8,do_em) + (4_8 + merge(1_8,0_8,do_vth))*nspecies)/twrite/1024. ! speed in KB/s
    if (twrite > 1024.) then                                                    ! speed > 1 MB/s
      twrite = twrite / 1024.
      if (do_validate) twrite = 0.0
      if (twrite > 1024.)  then                                                 ! speed > 1 GB/s
        twrite = twrite / 1024.
        write(message,'(a4,f7.2,a5)') ' FIO ',twrite,' GB/s'
      else
        write(message,'(a4,f7.2,a5)') ' FIO ',twrite,' MB/s'
      endif
    else
      write(message,'(a4,f7.2,a5)') ' FIO ',twrite,' KB/s'
    endif
    call extra_info(message)
  endif
  !
  first_dump=.false.
 
  call trace_exit ('WRITE_FIELDS')
END SUBROUTINE write_fields

!=======================================================================
SUBROUTINE safe_system(cmd,ii)
  USE params, only: master, rank, mfile
  implicit none
  character(len=mfile):: cmd
  integer            :: status, system, i, j, k, n, m,ii
  n=10000
  m=0
  do i=0,100
    status = system(cmd)
    if (status==0) exit
    do j=1,n
      do k=1,n
        m=m+k/100
      enddo
    enddo
    print*,i
  enddo
  if (i > 0) then
    print*,'safe_system: i,cmd =',i,m,cmd
  endif
  ii=i
END SUBROUTINE

!=======================================================================
SUBROUTINE scp_dir(file, dir)
  USE params, only: master, mfile, dbg
  USE pic_io, only: do_scp, scpdir
  implicit none
  character(len=*)    :: file, dir
  character(len=mfile):: cmd
  integer, save       :: n=5
  integer :: tries
  if (.not.do_scp) return
  cmd = "SCP "//trim(file)//" "//trim(scpdir)//"/"//trim(dir)//" &"
  if (n .gt. 0) then
    print*,cmd
    n = n-1
  endif
  call safe_system (cmd,tries)
END SUBROUTINE scp_dir

!=======================================================================
SUBROUTINE scp(file)
  USE params, only: master, mfile, rank, data_unit
  USE pic_io, only: do_scp, scpdir
  implicit none
  character(len=*)   :: file
  character(len=mfile):: cmd
  integer, save      :: n=5
  integer            :: status, system, i
  if (.not.do_scp) return
  open (data_unit,file='cmd.log',form='formatted',status='unknown',position='append')
  write(cmd,'(a,i4.4,a)') 'echo rank=',rank,' hostname : `hostname` >>scp.log'
  call safe_system (cmd,i)
  write (data_unit,1) i,status,cmd
  cmd = "SCP "//trim(file)//" "//trim(scpdir)//" </dev/null >>scp.log &"
  if (n .gt. 0) then
    print 1,0,cmd
    n = n-1
  endif
  call safe_system (cmd,i)
  write (data_unit,1) i,status,cmd
  close (data_unit)
1 format (1x,i3,1x,o16,1x,a130)
END SUBROUTINE scp
