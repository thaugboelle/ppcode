! $Id$
! vim: nowrap
!=======================================================================
MODULE stat
  implicit none
  integer, parameter :: energy_kind = kind(1.0D0)
  real (kind=energy_kind) :: energy_em_init, energy_sp_init, &
              energy_em_tot, energy_em_box, energy_em_inj, energy_em_damp, &
              energy_sp_tot, energy_sp_box, energy_sp_inj, energy_sp_left, &
              energy_b_box,  energy_e_box,  energy_sp_rad
  real    :: energy_sp_inj2
  real    :: prefe, prefb
  integer :: stat_z_layer=-5                                              
CONTAINS

!=======================================================================
SUBROUTINE particle_statistics(isp)
  USE params,  only : hl,mid,dbg,trace,stdout
  USE species, only : nspecies
  integer, intent(in), optional         :: isp
  integer                               :: i
  character(len=mid) :: &
    id = "$Id$"
!.......................................................................
  if ((dbg.gt.0 .or. trace) .and. id .ne. '' .and. stdout.ge.0) then
    write(stdout,*) hl
    write(stdout,*) id
    id=''
  endif
 
  if (present(isp)) then
    call PStatistics(isp)
  else
    do i=1,nspecies
      call PStatistics(i)
    enddo
  endif
END SUBROUTINE particle_statistics

!=======================================================================
SUBROUTINE PStatistics(isp)
  USE params,  only : mdim, mcoord, mtxt, stdout
  USE species, only : sp, particle
  USE grid_m,    only : g
  implicit none
  integer, intent(in)                   :: isp
  integer                               :: i,ip,np
  real                                  :: mn,mx,av,t,ds(3),lb(3)
  type(particle), pointer, dimension(:) :: pa
  real, allocatable,       dimension(:) :: scrpa
  character(len=mtxt)                   :: name
  real, external                        :: aver
    
  if (sp(isp)%tnp .eq. 0) return
  name = sp(isp)%name
  np = sp(isp)%np
  ds = g%ds
  lb = g%grlb
  pa   => sp(isp)%particle(1:np)
  allocate(scrpa(np))
  do ip=1,np
    scrpa(ip) = pa(ip)%w
  enddo
  t = 1./aver(scrpa,np)
  do i=1,mdim
    do ip=1,np
      scrpa(ip) = ((pa(ip)%r(i) + pa(ip)%q(i))*ds(i)+lb(i))*pa(ip)%w
    enddo
    call stats(scrpa,mn,av,mx,np)
    if (stdout.ge.0) &
      write(stdout,'(1x,"mn,av,mx of pos",i1,"   :",3(1pe10.2)," ",a)') &
        i,mn*t,av*t,mx*t,name
  enddo
  do i=1,mcoord
    do ip=1,np
      scrpa(ip) = pa(ip)%p(i)*pa(ip)%w
    enddo
    call stats(scrpa,mn,av,mx,np)
    if (stdout.ge.0) & 
      write(stdout,'(1x,"mn,av,mx of mom",i1,"   :",3(1pe10.2)," ",a)') &
        i,mn*t,av*t,mx*t,name
  enddo
  do ip=1,np
    scrpa(ip) = pa(ip)%e*pa(ip)%w
  enddo
  call stats(scrpa,mn,av,mx,np)
  if (stdout.ge.0) &
    write(stdout,'(1x,"mn,av,mx of energy :",3(1pe10.2)," ",a)') &
      mn*t,av*t,mx*t,name
  do ip=1,np
    scrpa(ip) = pa(ip)%w
  enddo
  call stats(scrpa,mn,av,mx,np)
  if (stdout.ge.0) &
    write(stdout,'(1x,"mn,av,mx of weight :",3(1pe10.2)," ",a)') &
      mn,av,mx,name
  deallocate(scrpa)
END SUBROUTINE PStatistics
!-----------------------------------------------------------------------
END MODULE stat

!=======================================================================
SUBROUTINE init_diagnostics
  USE stat, only: particle_statistics
  call zero_energy_counters
  call init_energy
  call particle_statistics
END SUBROUTINE init_diagnostics

!=======================================================================
SUBROUTINE energy(energy_balance)
  USE grid_m,   only: g, ex, ey, ez, bx, by, bz
  USE species,  only: sp, particle, nspecies
  USE params,   only: do_maxwell, stdout
  USE units,    only: c, elm
  USE debug,    only: dbg_select, dbg_stat
  USE stat
  implicit none 
  real(kind=energy_kind)                :: energy_balance
  type(particle), pointer, dimension(:) :: pa
  real,   allocatable, dimension(:,:,:) :: scr
  real(kind=energy_kind)                :: e_tot_now,e_tot_input
  real(kind=8)                          :: aver2
  real(kind=8)                          :: total3
  integer :: isp,ix,iy,iz
  logical, save :: first_warning=.true.
  
  if (do_maxwell) then
    allocate(scr(g%n(1),g%n(2),g%n(3)))
    do iz=1,g%n(3)
      scr(:,:,iz) = bx(:,:,iz)**2 + by(:,:,iz)**2 + bz(:,:,iz)**2
    enddo
    energy_b_box  = prefb*total3(scr)*g%dV
    energy_em_box = energy_b_box + energy_e_box
    deallocate(scr)
  else
    energy_e_box  = 0. 
    energy_b_box  = 0. 
    energy_em_box = 0.
  endif
 !----

  energy_sp_box = 0.
  do isp = 1, nspecies
    pa => sp(isp)%particle
    energy_sp_box = energy_sp_box + sp(isp)%tnp*aver2(isp)*g%dV
  enddo

  energy_em_tot = energy_em_box - energy_em_inj + energy_em_damp
  energy_sp_tot = energy_sp_box - energy_sp_inj + energy_sp_left + energy_sp_rad

  if (iand(dbg_select,dbg_stat)>0 .and. stdout>0) then
    write(stdout,*) energy_em_box, energy_em_inj, energy_em_damp
    write(stdout,*) energy_sp_box, energy_sp_inj, energy_sp_left
    write(stdout,*) energy_em_init, energy_sp_init
  end if

  e_tot_now  = energy_em_tot  + energy_sp_tot + energy_em_inj + energy_sp_inj
  e_tot_input = energy_em_init + energy_sp_init + energy_em_inj + energy_sp_inj

  if (e_tot_input .gt. 0.) then
    energy_balance= (e_tot_now-e_tot_input)/e_tot_input*100.
  else
    if (first_warning) then
      call warning('energy','Zero initial energy!')
      first_warning=.false.
    endif
    energy_balance= 0.
  endif
END SUBROUTINE energy

!=======================================================================
SUBROUTINE energy_balance_printout()
  USE params !only : stdout, master, hl, do_maxwell
  USE stat
  implicit none
  real(kind=energy_kind):: energy_balance
  call energy(energy_balance)
  if (master) then
    write(stdout,'(a,1pe20.12)') ""
    write(stdout,'(a,1pe20.12)') "Energy balance:"
    write(stdout,'(a,1pe20.12)') hl
    write(stdout,'(a,1pe20.12)') &
     "Initial energy balance    : ", energy_em_init + energy_sp_init 
    write(stdout,'(a,1pe20.12)') &
     "Final energy balance      : ", energy_em_tot  + energy_sp_tot
    if (do_maxwell) then
      write(stdout,'(a,1pe20.12)') &
       "EM energy balance         : ", energy_em_tot
      write(stdout,'(a,1pe20.12)') &
       "EM energy in box          : ", energy_em_box
      write(stdout,'(a,1pe20.12)') &
       "EM energy injected        : ", energy_em_inj
      write(stdout,'(a,1pe20.12)') &
       "EM energy damped          : ", energy_em_damp
    endif
    write(stdout,'(a,1pe20.12)') "Kinetic energy balance    : ", energy_sp_tot
    write(stdout,'(a,1pe20.12)') "Kinetic energy in box     : ", energy_sp_box
    write(stdout,'(a,1pe20.12)') "Kinetic energy injected   : ", energy_sp_inj
    write(stdout,'(a,1pe20.12)') "Kinetic energy left box   : ", energy_sp_left
    write(stdout,'(a,1pe20.12)') "Kinetic energy rad cooled : ", energy_sp_rad
    write(stdout,'(a,1pe20.12)') ""
  endif
  call barrier()
END SUBROUTINE energy_balance_printout

!=======================================================================
SUBROUTINE zero_energy_counters()
  USE stat
  implicit none
  real(kind=energy_kind), parameter :: z = 0.0_energy_kind
  energy_em_init = z
  energy_sp_init = z
  energy_em_tot  = z
  energy_em_box  = z 
  energy_em_inj  = z
  energy_em_damp = z
  energy_sp_tot  = z
  energy_sp_box  = z
  energy_sp_inj  = z
  energy_sp_left = z
  energy_sp_rad  = z
  energy_b_box   = z
  energy_e_box   = z
END SUBROUTINE zero_energy_counters

!=======================================================================
SUBROUTINE init_energy
  USE grid_m,            only: g, ex, ey, ez, bx, by, bz, drmax
  USE species,           only: sp, particle, nspecies, fields, &
                               get_SpeciesNr
  USE params !           only: do_maxwell, master, stdout, dbg, rank, Cdt
  USE units,             only: c, elm
  !USE dumps,             only: dump, dump_set
  USE stat
  implicit none
  type(particle), pointer,     dimension(:)    :: pa
  real,           allocatable, dimension(:,:,:):: scr,d
  real(kind=8)   :: aver2,total3,charge
  real           :: mass,p2,wpe,max_scalar,min_scalar, &
                    dmax_isp,dmax,wpmax,v_a,smin
  real           :: rms_betagamma(nspecies),thermal_speed(nspecies), &
                    restmass_energy,kinetic_energy
  real           :: ods, oc, gg, Z, wpe_max, v_e_rms, wce, wcemax
  real           :: skin_depth, debye_length, sound_speed, v_e, &
                    v_e_max, v_e_th_rms, v_p_th_rms
  real, external :: max4, sum_scalar
  integer        :: isp,ip,ix,iy,iz,lb(3),ub(3)
  integer        :: spe,spp,n_rms
  character(len=mtxt) :: name
  character(len=mid)  :: id='$Id$'
  
  if (stat_z_layer >= -4) then
    call init_energy_layer
    return
  endif
  
  call print_id (id)
  if (master) write(stdout,'(/a)') ' Initial state statistics and plasma parameters:'
!----
! this is the system-free version of eps0/2 - as in the SI(MKS) case.
  prefe = 1./(2.*c%fourpi*elm%ke)                                          
! this is the system-free version of 1./(2.*mu0) - as in the SI(MKS) case.
  prefb = elm%kf/(2.*c%fourpi*elm%kb)                                      
  
  if (do_maxwell) then
    allocate(scr(g%n(1),g%n(2),g%n(3)))
    scr = prefb*(bx**2+by**2+bz**2) + prefe*(ex**2+ey**2+ez**2)
    energy_em_init = total3(scr)*g%dV
    deallocate(scr)
  else
    energy_em_init = 0.
  endif
 !----
  call ParticleTotal
  energy_sp_init = 0.
  energy_sp_inj  = 0.
  do isp = 1, nspecies
   if (sp(isp)%tnp .gt. 0) then
    pa => sp(isp)%particle
    if (trim(sp(isp)%name) .eq. "photon") then
      do ip = 1, sp(isp)%np
        pa(ip)%e= c%mc * sqrt(sum(pa(ip)%p**2))
      enddo
    else
      mass = sp(isp)%mass
      ! After this initialisation it is computed only in move_paricles
      do ip = 1, sp(isp)%np
        pa(ip)%e = mass*c%c2
      enddo
      restmass_energy = sp(isp)%tnp*aver2(isp)*g%dV
      do ip = 1, sp(isp)%np
        p2       = sum(pa(ip)%p**2)
        pa(ip)%e = mass*p2/(sqrt(1.+p2*c%oc2) + 1.)
      enddo
      kinetic_energy = sp(isp)%tnp*aver2(isp)*g%dV
      rms_betagamma(isp) = sqrt(2.*kinetic_energy/restmass_energy)
    endif
    energy_sp_init = energy_sp_init + kinetic_energy
    if (master) write(stdout,'(a10,a30,1p,e11.3)') &
      trim(sp(isp)%name),' root-mean-square beta*gamma =', rms_betagamma(isp)
   endif
  enddo

  dmax = 0.
  wpmax = 0.
  allocate (d(g%n(1),g%n(2),g%n(3)))
  allocate (scr(g%n(1),g%n(2),g%n(3)))
  scr = 0.
  do isp=1,nspecies
    charge = sp(isp)%charge
    if (sp(isp)%mass == 0) cycle
    d = fields(:,:,:,isp)%d
    call overlap_add(d)
    if (do_maxwell) then
      do iz=1,g%n(3); do iy=1,g%n(2); do ix=1,g%n(1)
        scr(ix,iy,iz) = scr(ix,iy,iz) + d(ix,iy,iz)*sp(isp)%mass
      enddo; enddo; enddo
    endif
    dmax_isp = max4(d)
    dmax  = max(dmax,dmax_isp)
    wpmax = max(dble(wpmax),dble(dmax_isp*charge*(charge/sp(isp)%mass)))
  end do
  smin=huge(0.)

  ! sensible default values for lb and ub
  lb = g%lb
  ub = g%ub-1

  do iz=lb(3),ub(3)
  do iy=lb(2),ub(2)
  do ix=lb(1),ub(1)
    smin = merge(min(smin,scr(ix,iy,iz)),smin,scr(ix,iy,iz) > 0.)
  enddo
  enddo
  enddo
  smin = min_scalar(smin)
  do iz=1,g%n(3)
  do iy=1,g%n(2)
  do ix=1,g%n(1)
    scr(ix,iy,iz) = merge(smin,scr(ix,iy,iz),scr(ix,iy,iz)==0.)
  enddo
  enddo
  enddo
  if (do_maxwell) then
    scr = 1./sqrt(scr)
    scr = sqrt(2. * prefb * (bx**2 + by**2 + bz**2))*scr
    v_a = max4(scr)
  endif
  if (dmax > 0.) then
    name = 'electron' ; spe = get_SpeciesNr(name)
    name = 'proton'   ; spp = get_SpeciesNr(name)
    if (spe<=0 .and. spp<=0) then
      if (master) print*,'Using species 1 for sound speed'
      spp = 1
    endif

    ods = 1./maxval(g%ds)
    oc  = c%oc
    gg  = c%gamma_gas
    Z   = 1.

    thermal_speed = rms_betagamma
    wpmax         = sqrt(c%fourpi*elm%ke*wpmax)
    skin_depth    = c%c/wpmax
    lb = merge(g%lb+2,g%lb  ,mpi%lb)
    ub = merge(g%ub-2,g%ub-1,mpi%ub)
    if (spe .ge. 0) then 
      d = fields(:,:,:,spe)%d
      call overlap_add(d)
      v_e_max = 0.
      v_e_th_rms = 0.
      v_e_rms = 0.
      n_rms = 0
      wcemax = 0.
      debye_length = 1e35
      charge = sp(spe)%charge
      do iz=lb(3),ub(3)
      do iy=lb(2),ub(2)
      do ix=lb(1),ub(1)
        if (d(ix,iy,iz)<=0.) then
          print*,'NON-POSITIVE DENS at',ix,iy,iz,rank
          d(ix,iy,iz)=dmax                                              ! protect missing density
        endif
        wpe = sqrt(c%fourpi*elm%ke*d(ix,iy,iz)*charge*(charge/sp(spe)%mass))
        if (do_vth) debye_length = &
          min(debye_length, fields(ix,iy,iz,spe)%vth/wpe)
        wce = elm%kf*sqrt(bx(ix,iy,iz)**2+by(ix,iy,iz)**2+bz(ix,iy,iz)**2) &
          *abs(charge)/sp(spe)%mass
        wcemax = max(wce,wcemax)
        v_e = sqrt((fields(ix,iy,iz,spe)%v(1)/d(ix,iy,iz))**2+ &
                   (fields(ix,iy,iz,spe)%v(2)/d(ix,iy,iz))**2+ &
                   (fields(ix,iy,iz,spe)%v(3)/d(ix,iy,iz))**2)
        v_e_rms = v_e_rms + v_e**2
        v_e_max = max(v_e, v_e_max)
        if (do_vth) v_e_th_rms = v_e_th_rms + fields(ix,iy,iz,spe)%vth**2
        n_rms = n_rms+1
        scr(ix,iy,iz) = fields(ix,iy,iz,spe)%v(1)/d(ix,iy,iz)
      end do
      end do
      end do
      debye_length = min_scalar(debye_length)/sqrt(3.)                  ! 1-D vth in Debye length
      v_e_max = max_scalar(v_e_max)
      v_e_rms = sqrt(sum_scalar(v_e_rms)/sum_scalar(real(n_rms)))
      v_e_th_rms = sqrt(sum_scalar(v_e_th_rms)/sum_scalar(real(n_rms)))
      !call dump (scr,'ue2')
    else if (do_maxwell) then
      call warning('init_energy','no electrons defined, what to use for L_D?')
      debye_length = -1.
    endif
    if (spp .ge. 0 .and. do_vth) then 
      v_p_th_rms=0.
      n_rms = 0
      do iz=lb(3),ub(3)
      do iy=lb(2),ub(2)
      do ix=lb(1),ub(1)
        v_p_th_rms = v_p_th_rms + fields(ix,iy,iz,spp)%vth**2
        n_rms = n_rms+1
      end do
      end do
      end do
      v_p_th_rms  = sqrt(sum_scalar(v_p_th_rms)/sum_scalar(real(n_rms)))
      sound_speed = sqrt(gg/3.)*v_p_th_rms
    else
      if (do_vth) call warning('init_energy','no protons defined, what to use for c_s?')
      sound_speed = -1.
    endif      
    sound_speed = max_scalar(sound_speed)

    if (master) then
      write(stdout,'(a28,1p,e11.3)') ' Max number density      =', dmax
      if (do_maxwell) then
        write(stdout,'(a28,1p,e11.3)') ' Max plasma frequency    =', wpmax
        write(stdout,'(a28,1p,e11.3)') ' Max electron gyrofreq   =', wcemax
        write(stdout,'(a28,1p,e11.3)') ' Minimum skin depth / ds =', skin_depth    * ods
        if (do_vth) write(stdout,'(a28,1p,e11.3)') ' Debye length / ds       =', debye_length  * ods
        if (do_vth) write(stdout,'(a28,1p,e11.3)') ' el. thermal speed / c   =', v_e_th_rms    * oc
        if (do_vth) write(stdout,'(a28,1p,e11.3)') ' pr. thermal speed / c   =', v_p_th_rms    * oc
        write(stdout,'(a28,1p,e11.3)') ' Rms current speed / c   =', v_e_rms       * oc
        write(stdout,'(a28,1p,e11.3)') ' Max current speed / c   =', v_e_max       * oc
        write(stdout,'(a28,1p,e11.3)') ' Alfven speed / c        =', v_a           * oc
      endif 
      write(stdout,'(a28,1p,e11.3)') ' Sound speed / c         =', sound_speed   * oc
      write(stdout,'(a28,1p,e11.3)') ' Speed of light          =', c%c
      if (do_maxwell) then
        if (skin_depth < 4.*maxval(g%ds)) print *, 'WARNING, your skin depth is very low'
        if (debye_length < 0.1*maxval(g%ds) .and. do_vth) print *, 'WARNING, your Debye length is very low'
      endif
    endif
  end if
  deallocate (d,scr)

  if (dt==0.) then
    drmax = max_scalar(drmax)
    if (do_maxwell) then
      dt = Cdt*minval(g%ds)/c%c
    else if (drmax > 0.) then
      dt = Cdt/drmax
      drmax = drmax*dt
    else
      dt = Cdt*minval(g%ds)
    endif
    dt = 2.**(floor(log(dt)/log(2.)))                                   ! nearest power of two
    if (master) print*,'init_energy: Setting initial dt =',dt
  endif
  
  if (master .and. dbg .gt. 0.) write(stdout,*) 'energy_em_init =',energy_em_init
  if (master .and. dbg .gt. 0.) write(stdout,*) 'energy_sp_init =',energy_sp_init
  if (master) then
    if (time > 0.) then                                                 ! restarting ?
      open(stat_unit,file=trim(datadir)//'/stat.dat', &
                     form='unformatted',position='append',status='old')
    else                                                                ! initial run
      open(stat_unit,file=trim(datadir)//'/stat.dat',form='unformatted')
      ! Write how many nr there are in each record, and the type of variables.
      write(stat_unit) 14, energy_kind
    endif
  endif
END SUBROUTINE init_energy
!> write out various statistics for the whole box.

!=======================================================================
SUBROUTINE write_stat(ptime)
  USE params, only : stat_unit, master
  USE stat
  implicit none
  real(kind=8), intent(in) :: ptime
  real(kind=energy_kind)   :: etime
  if (.not. master) return
  etime = real(ptime,kind=energy_kind)
  write(stat_unit) etime, &
                   energy_em_init, energy_sp_init, energy_em_tot, energy_em_box, &
                   energy_em_inj,  energy_em_damp, energy_sp_tot, energy_sp_box, &
                   energy_sp_inj,  energy_sp_left, energy_b_box, energy_e_box,   &
                   energy_sp_rad
  call flush(stat_unit)
END SUBROUTINE write_stat
!> write out various statistics for the whole box.

!=======================================================================
SUBROUTINE write_stat_header
  USE params, only : data_unit, master, mid
  USE stat
  implicit none
  integer, parameter :: io_format=2
  integer            :: realk = Kind(1.0_4)
  character(len=mid) :: id = "$Id$"
!.......................................................................
  write (data_unit) io_format,mid
  write (data_unit) id
  write (data_unit) 1
  write (data_unit) 13*energy_kind/realk,2                               
! normally e_kind=8 hence *e_kind/realk
  write(data_unit) energy_em_init, energy_sp_init, energy_em_tot,  &
                   energy_em_box,  energy_em_inj,  energy_em_damp, &
                   energy_sp_tot,  energy_sp_box,  energy_sp_inj,  &
                   energy_sp_left, energy_b_box,   energy_e_box,   &
                   energy_sp_rad
END SUBROUTINE write_stat_header
!> read various statistics for the whole box.

!=======================================================================
SUBROUTINE read_stat_header
  USE params, only : data_unit, master, mid
  USE stat
  USE header, only : check_header_format
  implicit none
  integer            :: io_format=2
  integer            :: scr
  character(len=mid) :: id = "$Id$"
!.......................................................................
  call check_header_format(io_format, id, 'read_stat_header')
  read(data_unit) scr                                                   
! length of the following record
  read(data_unit) energy_em_init, energy_sp_init, energy_em_tot,  &
                  energy_em_box,  energy_em_inj,  energy_em_damp, &
                  energy_sp_tot,  energy_sp_box,  energy_sp_inj,  &
                  energy_sp_left, energy_b_box,   energy_e_box,   &
                  energy_sp_rad
END SUBROUTINE read_stat_header

!=======================================================================
SUBROUTINE finalize_stat
  USE params, only : stat_unit, master, datadir
  implicit none
  if (.not. master) return
  close(stat_unit)
  call scp(trim(datadir)//'/stat.dat')
END SUBROUTINE finalize_stat

! to-be-corrected to single layer calculation, cut'n'paste from subroutine
! "init_energy" above.
!=======================================================================
SUBROUTINE init_energy_layer                                            
  USE grid_m,              only : g, ex, ey, ez, bx, by, bz
  USE species,           only : sp, particle, nspecies, fields, &
                                get_SpeciesNr
  USE params !           only : do_maxwell, master, stdout, dbg, rank
  USE units,             only : c, elm
  !USE dumps,             only : dump, dump_set
  USE stat
  implicit none
  type(particle), pointer,     dimension(:)    :: pa
  real,           allocatable, dimension(:,:,:):: scr,d
  real(kind=8)   :: aver2,total3,charge
  real           :: mass,p2,wpe,max_scalar,min_scalar, &
                    dmax_isp,dmax,wpmax,v_a
  real           :: rms_betagamma(nspecies),thermal_speed(nspecies), &
                    restmass_energy,kinetic_energy
  real           :: ods, oc, gg, Z, wpe_max, v_e_rms, wce, wcemax
  real           :: skin_depth, debye_length, sound_speed, v_e, v_e_max, &
                    v_e_th_rms, v_p_th_rms
  real, external :: max4, sum_scalar
  integer        :: isp,ip,ix,iy,iz,lb(3),ub(3)
  integer        :: spe,spp,n_rms
  character(len=mtxt) :: name
  character(len=mid)  :: &
    id='$Id$'
  
  call print_id (id)
  if (master) write(stdout,'(/a)') &
    ' Initial state statistics and plasma parameters, for single layer:'
 !----
  if (do_maxwell) then
    allocate(scr(g%n(1),g%n(2),g%n(3)))
    scr = prefb*(bx**2+by**2+bz**2) + prefe*(ex**2+ey**2+ez**2)
    energy_em_init = total3(scr)*g%dV
    deallocate(scr)
  else
    energy_em_init = 0.
  endif
 !----
  call ParticleTotal
  energy_sp_init = 0.
  energy_sp_inj  = 0.
  do isp = 1, nspecies
   if (sp(isp)%tnp .gt. 0) then
    pa => sp(isp)%particle
    if (trim(sp(isp)%name) .eq. "photon") then
      do ip = 1, sp(isp)%np
        pa(ip)%e= c%mc * sqrt(sum(pa(ip)%p**2))
      enddo
    else
      mass = sp(isp)%mass
      ! After this initialisation it is computed only in move_paricles
      do ip = 1, sp(isp)%np
        pa(ip)%e = mass*c%c2
      enddo
      restmass_energy = sp(isp)%tnp*aver2(isp)*g%dV
      do ip = 1, sp(isp)%np
        p2       = sum(pa(ip)%p**2)
        pa(ip)%e = mass*p2/(sqrt(1.+p2*c%oc2) + 1.)
      enddo
      kinetic_energy = sp(isp)%tnp*aver2(isp)*g%dV
      rms_betagamma(isp) = sqrt(2.*kinetic_energy/restmass_energy)
    endif
    energy_sp_init = energy_sp_init + kinetic_energy
    if (master) write(stdout,'(a10,a30,1p,e11.3)') &
      trim(sp(isp)%name),' root-mean-square beta*gamma =', rms_betagamma(isp)
   endif
  enddo

  dmax = 0.
  wpmax = 0.
  allocate (d(g%n(1),g%n(2),g%n(3)))
  allocate (scr(g%n(1),g%n(2),g%n(3)))
  do isp=1,nspecies
    charge = sp(isp)%charge
    if (sp(isp)%mass == 0) cycle
    d = fields(:,:,:,isp)%d
    call overlap_add(d)
    if (do_maxwell) scr = scr + d*sp(isp)%mass
    dmax_isp = max4(d)
    dmax  = max(dmax,dmax_isp)
    wpmax = max(dble(wpmax),dble(dmax_isp*charge*(charge/sp(isp)%mass)))
  end do
  if (do_maxwell) then
    scr = 1./sqrt(scr)
    scr = sqrt(2. * prefb * (bx**2 + by**2 + bz**2))*scr
    v_a = max4(scr)
  endif
  if (dmax > 0.) then
    name = 'electron' ; spe = get_SpeciesNr(name)
    name = 'proton'   ; spp = get_SpeciesNr(name)
    if (spe<=0 .and. spp<=0) then
      if (master) print*,'Using species 1 for sound speed'
      spp = 1
    endif

    ods = 1./maxval(g%ds)
    oc  = c%oc
    gg  = c%gamma_gas
    Z   = 1.

    thermal_speed = rms_betagamma
    wpmax         = sqrt(c%fourpi*elm%ke*wpmax)
    skin_depth    = c%c/wpmax
    lb = merge(g%lb+2,g%lb  ,mpi%lb)
    ub = merge(g%ub-2,g%ub-1,mpi%ub)
    if (spe .ge. 0) then 
      d = fields(:,:,:,spe)%d
      call overlap_add(d)
      v_e_max = 0.
      v_e_th_rms = 0.
      v_e_rms = 0.
      n_rms = 0
      wcemax = 0.
      debye_length = 1e35
      charge = sp(spe)%charge
      do iz=lb(3),ub(3)
      do iy=lb(2),ub(2)
      do ix=lb(1),ub(1)
        if (d(ix,iy,iz)<=0.) then
          print*,'NON-POSITIVE DENS at',ix,iy,iz,rank
        endif
        wpe = sqrt(c%fourpi*elm%ke*d(ix,iy,iz)*charge*(charge/sp(spe)%mass))
        debye_length = min(debye_length, fields(ix,iy,iz,spe)%vth/wpe)
        wce = elm%kf*sqrt(bx(ix,iy,iz)**2+by(ix,iy,iz)**2+bz(ix,iy,iz)**2)*abs(charge)/sp(spe)%mass
        wcemax = max(wce,wcemax)
        v_e = sqrt((fields(ix,iy,iz,spe)%v(1)/d(ix,iy,iz))**2+ &
                   (fields(ix,iy,iz,spe)%v(2)/d(ix,iy,iz))**2+ &
                   (fields(ix,iy,iz,spe)%v(3)/d(ix,iy,iz))**2)
        v_e_rms = v_e_rms + v_e**2
        v_e_max = max(v_e, v_e_max)
        v_e_th_rms = v_e_th_rms + fields(ix,iy,iz,spe)%vth**2
        n_rms = n_rms+1
      end do
      end do
      end do
      debye_length = min_scalar(debye_length)/sqrt(3.)
      v_e_max = max_scalar(v_e_max)
      v_e_rms = sqrt(sum_scalar(v_e_rms)/sum_scalar(real(n_rms)))
      v_e_th_rms = sqrt(sum_scalar(v_e_th_rms)/sum_scalar(real(n_rms)))
      scr = fields(:,:,:,spe)%v(1)/d(:,:,:)
      !call dump (scr,'ue2')
    else if (do_maxwell) then
      call warning('init_energy','no electrons defined, what to use for L_D?')
      debye_length = -1.
    endif
    if (spp .ge. 0) then 
      v_p_th_rms = 0.
      n_rms = 0
      do iz=lb(3),ub(3)
      do iy=lb(2),ub(2)
      do ix=lb(1),ub(1)
        v_p_th_rms = v_p_th_rms + fields(ix,iy,iz,spp)%vth**2
        n_rms = n_rms+1
      end do
      end do
      end do
      v_p_th_rms  = sqrt(sum_scalar(v_p_th_rms)/sum_scalar(real(n_rms)))
      sound_speed = sqrt(gg/3.)*v_p_th_rms
    else
      call warning('init_energy','no protons defined, what to use for c_s?')
      sound_speed = -1.
    endif      

    if (master) then
      write(stdout,'(a28,1p,e11.3)') ' Max number density      =', dmax
      write(stdout,'(a28,1p,e11.3)') ' Max plasma frequency    =', wpmax
      write(stdout,'(a28,1p,e11.3)') ' Max electron gyrofreq   =', wcemax
      write(stdout,'(a28,1p,e11.3)') ' Minimum skin depth / ds =', skin_depth    * ods
      write(stdout,'(a28,1p,e11.3)') ' Debye length / ds       =', debye_length  * ods
      write(stdout,'(a28,1p,e11.3)') ' el. thermal speed / c   =', v_e_th_rms    * oc
      write(stdout,'(a28,1p,e11.3)') ' pr. thermal speed / c   =', v_p_th_rms    * oc
      write(stdout,'(a28,1p,e11.3)') ' Rms current speed / c   =', v_e_rms       * oc
      write(stdout,'(a28,1p,e11.3)') ' Max current speed / c   =', v_e_max       * oc
      write(stdout,'(a28,1p,e11.3)') ' Sound speed / c         =', sound_speed   * oc
      write(stdout,'(a28,1p,e11.3)') ' Alfven speed / c        =', v_a           * oc
      write(stdout,'(a28,1p,e11.3)') ' Speed of light          =', c%c
      if (skin_depth < 4.*maxval(g%ds)) print *, 'WARNING, your skin depth is very low'
      if (debye_length < 0.1*maxval(g%ds)) print *, 'WARNING, your Debye length is very low'
    endif
  end if
  deallocate (d,scr)
  
  if (master .and. dbg .gt. 0.) write(stdout,*) 'energy_em_init =',energy_em_init
  if (master .and. dbg .gt. 0.) write(stdout,*) 'energy_sp_init =',energy_sp_init
  if (master) then
    if (time > 0.) then                                                 ! restarting ?
      open(stat_unit,file=trim(datadir)//'/stat.dat', &
                     form='unformatted',position='append',status='old')
    else                                                                ! initial run
      open(stat_unit,file=trim(datadir)//'/stat.dat',form='unformatted')
      ! Write how many nr there are in each record, and the type of variables.
      write(stat_unit) 14, energy_kind
    endif
  endif

  if (master) write(stdout,'(/a)') &
    ' Exit Initial state statistics , single layer calculaton:'
  STOP 
  ! Still need to actually implement the single layer (skip z-loops specifically 
  ! for aulanier experiment)
END SUBROUTINE init_energy_layer
