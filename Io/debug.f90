! $Id$
! vim: nowrap
!=======================================================================
MODULE debug
  real                :: tolerance     = 0.0                            !> dump/compare tolerance
  real                :: relative_tolerance = 0.0                       !> dump/compare relative tolerance
  integer             :: verbose       = 0                              !> noise level
  integer             :: maxprint      = 10                             !> no. of diffs printed
  integer             :: maxwarn       = 1000, warnings = 0             !> no. of warnings
  integer             :: dbg_select    = 0
  integer, parameter  :: dbg_fields    = 1
  integer, parameter  :: dbg_particles = 2
  integer, parameter  :: dbg_stat      = 4
  integer, parameter  :: dbg_maxwell   = 64
  integer, parameter  :: dbg_io        = 128
  integer, parameter  :: dbg_mpi       = 256
  integer, parameter  :: dbg_omp       = 512
  integer, parameter  :: dbg_dt        = 1024
  integer, parameter  :: dbg_synth     = 2048
  integer, parameter  :: dbg_init      = 4096
  integer             :: memory_ceiling= 1024                           !> [MB] Make noise if thread mem use is larger
  integer(kind=8), dimension(2) :: ptrace = (/-1,-1/)
!-----------------------------------------------------------------------
CONTAINS

!=======================================================================
! Check whether or not to a specific debug flag bit is set 
!-----------------------------------------------------------------------
LOGICAL FUNCTION debugit(select,level)
  USE params
  implicit none
  integer select, level
!.......................................................................
  debugit = (iand(select,dbg_select) .ne. 0) .and. (dbg .ge. level)
END FUNCTION debugit
!-----------------------------------------------------------------------
END MODULE debug
!=======================================================================

!=======================================================================
! Read the namelist that controls debugging
!-----------------------------------------------------------------------
SUBROUTINE read_debug
  USE params
  USE debug
  implicit none
  namelist /dbgctl/ trace, dbg, dbg_select, do_dump, do_compare, &
    tolerance, verbose, maxwarn, maxprint, do_check_interpolate, &
    ptrace, do_check_ranges, do_check_bounds, &
    memory_ceiling, die_in_sort, trace_level
!.......................................................................
  rewind (stdin); read (stdin,dbgctl)                                   ! read debug params
  if (master)        write (params_unit,dbgctl)
  if (out_namelists) write (stdout,dbgctl)                              ! write debug params
  if (stdout < 0) trace = .false.                                       ! No output => no trace
END SUBROUTINE read_debug
