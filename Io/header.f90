! \file header.f90
! vim: nowrap
! \version $Id$
!> Module with useful definitions and functions for reading/writing headers
!===============================================================================
MODULE header
  implicit none
  private

  public :: hunit, h_line, h_line_, check_header_format, &
            read_header_line, skip_header_block, master

  ! clumsy work around for params depending on header
  logical   :: master                                                           ! should be set by call in init_mpi

  integer   :: hunit                                                            ! unit to read header from
  integer   :: nr_of_lines                                                      ! nr of lines in header
  character(len=120) :: hname                                                   ! name of current header
  type h_line_
    integer :: n                                                                ! nr of vars in header line
    logical :: i,r                                                              ! true if cont ints,reals
    integer,      pointer, dimension(:) :: ints => null()                       ! pointer to ints
    real(kind=4), pointer, dimension(:) :: floats => null()                     ! pointer to reals
    logical,      pointer, dimension(:) :: logs => null()                       ! pointer to logicals
  end type
#if defined(__GFORTRAN__) || defined(__G95__) || defined(__xlc__)
  type(h_line_), save :: h_line
#else
  type(h_line_)       :: h_line
#endif
CONTAINS
!===============================================================================
SUBROUTINE check_header_format(io_format, id, name)
  implicit none
  integer                      :: io_format
  character(len=*), intent(in) :: id, name
  !
  character(len=len(id))       :: lid
  integer                      :: i, lio_format, mid
  integer, parameter           :: verbose = 0
  !
  read (hunit) lio_format, mid
                               if (verbose > 0) print *, trim(name)//" :", len(id), id, io_format, lio_format
  read (hunit) lid
  if (trim(id).ne.trim(lid) .and. master) then                                 ! check cvs id is ok
    write(*,*) "Current CVS     : "//trim(id)
    write(*,*) "CVS from file   : "//trim(lid)
  endif
  if (io_format .ne. lio_format) then                                          ! check io format is ok
    if (master) then
      if (trim(id).eq.trim(lid)) write(*,*) "CVS tag         : "//trim(lid)    ! check cvs id is ok
      write(*,*) "Current version : ", io_format
      write(*,*) "Version in file : ", lio_format
      call warning(name,"IO formats do not match")
    endif
    io_format = lio_format
  endif
  read (hunit) nr_of_lines                                                     ! nr of lines in the header
  hname = ''                                                                   ! clear header name
  do i=1, min(len_trim(name),120)                                              ! record new name
    hname(i:i) = name(i:i)
  enddo
  if (associated(h_line%ints))   deallocate(h_line%ints)                       ! clear header line
  if (associated(h_line%floats)) deallocate(h_line%floats)
  if (associated(h_line%logs))   deallocate(h_line%logs)
END SUBROUTINE check_header_format
!===============================================================================
SUBROUTINE skip_header_block(io_format)
  implicit none
  integer            :: io_format
  character(len=72)  :: id
  !
  integer            :: done
  integer            :: i, mid
  integer, parameter :: verbose = 0
  !
  read (hunit) io_format, mid
  if (io_format .eq. -1) return
  read (hunit) id
                               if (verbose > 0) print *, "skip_header_block :", id, io_format
  read (hunit) nr_of_lines                                                     ! nr of lines in the header
  hname = 'skip_header'
  do i=1, nr_of_lines
    call read_header_line()
  enddo
END SUBROUTINE skip_header_block
!===============================================================================
SUBROUTINE read_header_line()
  implicit none
  integer :: i, cnt, typ
  integer, parameter           :: verbose = 0
  !
  if (nr_of_lines .eq. 0) &                                                    ! should not happen
    call error(trim(hname),"IO FORMATS DO NOT MATCH when reading header line (read_header_line)")

  if (associated(h_line%ints))   deallocate(h_line%ints)                       ! clear header line
  if (associated(h_line%floats)) deallocate(h_line%floats)
  if (associated(h_line%logs))   deallocate(h_line%logs)

  read(hunit) cnt, typ                                                         ! get nr of items and type

  if (typ .eq. 1 .or. typ .eq. 3) then                                         ! read as integers
    h_line%i = .true.
    allocate(h_line%ints(cnt))
    allocate(h_line%logs(cnt))
    read(hunit) h_line%ints
    h_line%logs = (h_line%ints .ne. 0)                                         ! ..or interpret as logicals
                               if (verbose > 0) then
                                 print *, trim(hname)//" ints :"
                                 print *, h_line%ints
                               endif
  endif

  if (typ .eq. 2 .or. typ .eq. 3) then                                         ! read as reals
    h_line%r = .true.
    allocate(h_line%floats(cnt))
    read(hunit) h_line%floats
                               if (verbose > 0) then
                                 print *, trim(hname)//" reals :"
                                 print *, h_line%floats
                               endif
  endif
  nr_of_lines = nr_of_lines - 1                                                ! dec counter
END SUBROUTINE read_header_line
!===============================================================================
END MODULE
!===============================================================================
SUBROUTINE set_master_in_header(mast)
  USE header, only : master
  implicit none
  logical, intent(in) :: mast
  master = mast
END SUBROUTINE set_master_in_header
!===============================================================================
