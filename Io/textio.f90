! Io/textio.f90 $Id$
! vim: nowrap
!=======================================================================
MODULE pic_io
  USE params, only: mid,mfile
  implicit none
  integer                           :: verbose
!--- Restart flag ------------------------------------------------------
! Restart<0   : No restart file on reaching endtime, for testing purposes 
!               of large runs
! Restart=0   : No restart
! Restart=1-3 : Restart.
! Restart=4   : Debug restart. Same as restart=1-3, but do not write a new 
!               restart file on reaching endtime.
  integer                           :: restart
!-----------------------------------------------------------------------
  real(kind=8)                      :: toutf, toutp, touts, toutr, oldptime, oldftime, &
                                       oldstime, oldrtime, oldwrtime, oldfctime
  logical                           :: indices
  character(len=mid)                :: hdf5file                         !> file name
  integer                           :: restartidx=0
  logical, dimension(3)             :: do_get_old_data                  !> read old data?
  integer                           :: oldsnapshot                      !> snapshot number?
  integer                           :: nslice                           !> nr of slices in run
  logical                           :: do_scp                           !> ship file somewhere?
  character(len=mfile)              :: scpdir                           !> where to copy to
  type file_io_type                                                     !> writing files, mpi
    integer             :: handle, filetype
    integer(kind=8)     :: position                                     ! position sequential
    integer(kind=8), allocatable, dimension(:) :: gnp, cnp              ! total size of buffer
    integer             :: size(3), lb(3), ub(3), offset(3)             ! offsets etc for mpi
    integer,         allocatable, dimension(:) :: np                    ! buffer for a thread
    integer             :: unit                                         ! unit number, serial
    character(len=mfile) :: type
  end type
  INTERFACE write_ioarray_part
    MODULE PROCEDURE write_ioarray_part_real, &
                     write_ioarray_part_int2, &
                     write_ioarray_part_int8
  END INTERFACE
CONTAINS

!=======================================================================
SUBROUTINE write_ioarray_part_real(p,ns,cns,gns,x)
  USE params, only: master,nodes,rank,data_unit,mpi
  implicit none
  integer(kind=8),            intent(in) :: ns,cns,gns                  ! size,cumul size, global
  integer(kind=8)                        :: p                           ! file position
  real(kind=4), dimension(:), intent(in) :: x                           ! input array
  integer(kind=8)                        :: sz = 4_8                    ! size in bytes
  integer(kind=8)                        :: cb8
  cb8 = gns*sz
  if (gns==0) return

  if (master) write(data_unit,pos=p) cb8                                ! fortran counter
  if (ns > 0) write(data_unit,pos=p+sz*cns+8_8) x                       ! write data
  if (rank == nodes-1) write(data_unit,pos=p+cb8+8_8) cb8               ! write fortran counter
  p = p + cb8 + 2_8*8_8                                                 ! inc position
END SUBROUTINE write_ioarray_part_real

!=======================================================================
SUBROUTINE write_ioarray_part_int2(p,ns,cns,gns,x)
  USE params, only: master,nodes,rank,data_unit,mpi
  implicit none
  integer(kind=8),               intent(in)  :: ns,cns,gns              ! cumul size, global
  integer(kind=8)                            :: p                       ! file position
  integer(kind=2), dimension(:), intent(in)  :: x                       ! input array
  integer(kind=8)                            :: sz = 2_8                ! size in bytes 
  integer(kind=8)                            :: cb8
  cb8 = gns*sz
  if (gns==0) return                                                    ! no data to write

  if (master) write(data_unit,pos=p) cb8                                ! write fortran counter
  if (ns > 0) write(data_unit,pos=p+sz*cns+8_8) x                       ! write data
  if (rank == nodes-1) write(data_unit,pos=p+cb8+8_8) cb8               ! write fortran counter
  p = p + cb8 + 2_8*8_8                                                 ! inc position
END SUBROUTINE write_ioarray_part_int2

!=======================================================================
SUBROUTINE write_ioarray_part_int8(p,ns,cns,gns,x)
  USE params, only: master,nodes,rank,data_unit,mpi
  implicit none
  integer(kind=8),               intent(in)  :: ns,cns,gns              ! size,cumul size, global
  integer(kind=8)                            :: p                       ! file position
  integer(kind=8), dimension(:), intent(in)  :: x                       ! input array
  integer(kind=8)                            :: sz = 8_8                ! size in bytes
  integer(kind=8)                            :: cb8
!.......................................................................
  cb8 = gns*sz
  if (gns==0) return                                                    ! if no data to write

  if (master) write(data_unit,pos=p) cb8                                ! write fortran counter
  if (ns > 0) write(data_unit,pos=p+sz*cns+8_8) x                       ! write data
  if (rank == nodes-1) write(data_unit,pos=p+cb8+8_8) cb8               ! if last thread
  p = p + cb8 + 2_8*8_8                                                 ! inc position
END SUBROUTINE write_ioarray_part_int8
!=======================================================================

END MODULE
!=======================================================================
SUBROUTINE init_stdio(printout)
  USE params
  implicit none
  character(len=mfile) inputfile, arg
  logical printout, file_exists

  stdout = 6
  stderr = 6
  stdall = stdout

  !call input (inputfile)                                                ! (for tests)
  if (file_exists('input.nml')) inputfile = 'input.nml'                 ! (for production)
  call getarg (1, arg)                                                  ! command argument 
  if (arg /= ' ') inputfile = arg
  open (stdin,file=inputfile,status='old')

  if (master .and. printout) then 
    write(stdout,*) hl
    select case (real_size)
    case (4)
      write(stdout,*) 'Running the PhotonPlasma code in single precision ...'
    case (8)
      write(stdout,*) 'Running the PhotonPlasma code in double precision ...'
    case (16)
      write(stdout,*) 'Running the PhotonPlasma code in quadruple precision ...'
    end select
    write(stdout,'(1x,a,a)') 'Reading namelist data from ',inputfile
  endif
END SUBROUTINE init_stdio

!=======================================================================
SUBROUTINE extra_info_init(string)
  USE params, only: main_info_bar,main_info_line,mtxt
  implicit none
  character(len=*), intent(in):: string
  integer, save               :: line_len=0
  integer:: i
  line_len       = min(len(string)+line_len,mtxt)
  main_info_line = repeat('-',line_len)
  i = index(main_info_bar,'  ')
  main_info_bar  = main_info_bar(1:i)//string
END SUBROUTINE

!=======================================================================
SUBROUTINE extra_info(string)
  USE params, only: main_info, minf
  implicit none
  character(len=*), intent(in):: string
  character(len=minf+len(string)+1) :: tmp
  integer:: i
  i = len_trim(main_info)
  tmp = main_info(1:i)//" "//string
  i = len_trim(tmp)
  main_info = tmp(1:max(i,minf))
END SUBROUTINE

!=======================================================================
FUNCTION wallclock_pr()
  USE params, only: do_validate
  implicit none
  real(kind=8)  :: wallclock, wallclock_pr
  external :: wallclock
!.......................................................................
  wallclock_pr = merge (0.0_8, wallclock(), do_validate)
END FUNCTION wallclock_pr

! This routine writes out a status line to stdout
!=======================================================================
SUBROUTINE write_log
  USE params,  only: master, walltime, walltime0, walltime_step, nomp, &
                     time, tend, it, dt, mfile, stdout, main_info, &
                     nspecies, do_validate, mprint
  USE species, only: sp
  USE stat,    only: energy_kind
  USE grid_m,  only: g
  implicit none

  real,            save :: tstart
  logical,         save :: first_call=.true.
  integer(kind=8), save :: oldsumnp, sumnp
  real                  :: mus, remain, wc
  character(len=mfile)  :: info_bar
  character(len=6)      :: fmt
  character(len=1)      :: tunit
  real(8), external     :: wallclock_pr
  real(energy_kind)     :: energy_balance
  integer               :: iter_b, iter_e
!.......................................................................
  call ParticleTotal
  
  ! Initialize log output on first call
  if (first_call) then
    walltime0 = wallclock_pr()
    ! zero point for time spent in main loop
    info_bar = '    It      dt        time        #part   added it_b '// &
               'it_e   rem    mus    energy_balance    i/o   '
    fmt = '(1x,a)'
    if (master) then
      write (stdout,fmt) repeat('-',len(trim(info_bar)))
      write (stdout,fmt) info_bar
      write (stdout,fmt) repeat('-',len(trim(info_bar)))
    endif
    if (nspecies > 0) then
      oldsumnp = sum(sp%tnp)
    else
      oldsumnp = 0
    endif
    tstart   = time
    main_info = ''

    first_call = .false.
    return
  endif
  
  tstart=0.

  ! wall clock time used
  wc = wallclock_pr()
  walltime = wc - walltime0                                             ! since start of main loop
  remain = (tend-time)*walltime/(time-tstart+1e-6)                      ! remaing sec estimate
  tunit = 's'                                                           ! unit = seconds
  if (remain > 60) then
    remain = remain/60.                                                 ! minutes
    tunit = 'm'                                                         ! unit
    if (remain > 60) then
      remain = remain/60.                                               ! hours
      tunit = 'h'                                                       ! unit
      if (remain > 24) then
        remain = remain/24.                                             ! days
        tunit = 'd'                                                     ! unit
      end if
    end if
  end if

  mus = (wc - walltime_step); call mpi_sum_real (mus)                   ! microsecs
  if (nspecies > 0) then
    if (sum(sp%tnp).le.0) then                                           
      mus = nomp*mus*1e6 / product(real(g%ub-g%lb))                     ! per point 
    else
      mus = nomp*mus*1e6 / real(sum(sp%tnp))                            ! per particle
    endif
    sumnp = sum(sp%tnp)
  else
    mus = nomp*mus*1e6 / product(real(g%ub-g%lb))
    sumnp = 0
  endif

  if (do_validate) then
    remain = 0.0
    mus = 0.0
  endif

  call energy (energy_balance)
  call maxwell_iterations (iter_b, iter_e)
  if (master.and.mod(it,mprint)==0) write (stdout, &                    ! output line for log file
!      '(i7,f8.4,f12.3,i13,i8,2i5,f6.1,a1,f6.2,f24.12,a1,a5,1x,a)')  &  ! format
    '(i7,f8.4,f12.3,i13,i8,2i5,f6.1,a1,f6.2,f18.12,a1,1x,a)')    &      ! format
    it,dt,time,sumnp,sumnp-oldsumnp,iter_b,iter_e, &                    ! particle info
    remain,tunit,mus,energy_balance,'%',&!trim(adjustl(ioactivity)), &  ! remaing time etc
    trim(main_info)                                                     ! add info

  ! Update for next time step
  if (nspecies > 0) then
    oldsumnp = sum(sp%tnp)
  else
    oldsumnp = 0
  endif

  main_info = ''
END SUBROUTINE write_log
