! $Id$
! vim: nowrap
! Routines for inlined interpolation and particle operations
!=======================================================================
MODULE interpolation_cached
  implicit none
  integer, parameter :: slc=-2, slb=-1, sub=2, suc=3
  real, dimension(slc:sub,slb:sub,slb:sub) :: sEx                       ! Cached E-field
  real, dimension(slb:sub,slc:sub,slb:sub) :: sEy
  real, dimension(slb:sub,slb:sub,slc:sub) :: sEz
  real, dimension(slb:sub,slc:sub,slc:sub) :: sBx                       ! Cached B-field
  real, dimension(slc:sub,slb:sub,slc:sub) :: sBy
  real, dimension(slc:sub,slc:sub,slb:sub) :: sBz
  real, dimension(slc:suc,slc:suc,slc:suc) :: d                         ! Cached number density
  real, dimension(slc:suc,slc:suc,slc:suc,3) :: v                       ! Cached number density flux
  real, dimension(slc:suc,3)              :: w_old, ws_old, w_new, ws_new ! Interpolation weights
  real, parameter :: zero=0., one_sixth=1./6., two_third=2./3.
CONTAINS
! Calculate weights for centered quantities
!-----------------------------------------------------------------------
SUBROUTINE make_centered_weights(r,w)
  implicit none
  real, dimension(3), intent(in) :: r
  real, dimension(slc:suc,3)     :: w
  real :: s, sm1, o
  !-------------
  ! X_COORD CENTERED
  s = r(1); sm1 = 1. - s; o = 2. - s
  w(-1,1) = one_sixth * sm1*sm1*sm1
  w( 0,1) = two_third - 0.5*s*s*o
  w( 1,1) = two_third - 0.5*sm1*sm1*(1.+ s)
  w( 2,1) = 1. - (w(-1,1) + w(0,1) + w(1,1))
  !-------------
  ! Y_COORD CENTERED
  s = r(2); sm1 = 1. - s; o = 2. - s
  w(-1,2) = one_sixth * sm1*sm1*sm1
  w( 0,2) = two_third - 0.5*s*s*o
  w( 1,2) = two_third - 0.5*sm1*sm1*(1.+ s)
  w( 2,2) = 1. - (w(-1,2) + w(0,2) + w(1,2))
  !-------------
  ! Z_COORD CENTERED
  s = r(3); sm1 = 1. - s; o = 2. - s
  w(-1,3) = one_sixth * sm1*sm1*sm1
  w( 0,3) = two_third - 0.5*s*s*o
  w( 1,3) = two_third - 0.5*sm1*sm1*(1.+ s)
  w( 2,3) = 1. - (w(-1,3) + w(0,3) + w(1,3))
END SUBROUTINE make_centered_weights
! Calculate weights for staggered quantities
!-----------------------------------------------------------------------
SUBROUTINE make_staggered_weights(r,w)
  implicit none
  real, dimension(3), intent(in) :: r
  real, dimension(slc:suc,3)     :: w
  real :: s, sm1, o, wa, wb, wc, wd
  logical :: up
  !-------------
  ! X_COORD SHIFTED IN X
  s = r(1) - 0.5; up = s >= 0
  s = s + merge(0.,1.,up); sm1 = 1. - s; o = 2. - s
  wa = one_sixth * sm1*sm1*sm1
  wb = two_third - 0.5*s*s*o
  wc = two_third - 0.5*sm1*sm1*(1.+ s)
  wd = 1. - (wa + wb + wc)
  w(-2,1) = merge(zero, wa, up)
  w(-1,1) = merge(wa, wb, up)
  w( 0,1) = merge(wb, wc, up)
  w( 1,1) = merge(wc, wd, up)
  w( 2,1) = merge(wd, zero, up)
  !-------------
  ! Y_COORD SHIFTED IN Y
  s = r(2) - 0.5; up = s >= 0
  s = s + merge(0.,1.,up); sm1 = 1. - s; o = 2. - s
  wa = one_sixth * sm1*sm1*sm1
  wb = two_third - 0.5*s*s*o
  wc = two_third - 0.5*sm1*sm1*(1.+ s)
  wd = 1. - (wa + wb + wc)
  w(-2,2) = merge(zero, wa, up)
  w(-1,2) = merge(wa, wb, up)
  w( 0,2) = merge(wb, wc, up)
  w( 1,2) = merge(wc, wd, up)
  w( 2,2) = merge(wd, zero, up)
  !-------------
  ! Z_COORD SHIFTED IN Z
  s = r(3) - 0.5; up = s >= 0
  s = s + merge(0.,1.,up); sm1 = 1. - s; o = 2. - s
  wa = one_sixth * sm1*sm1*sm1
  wb = two_third - 0.5*s*s*o
  wc = two_third - 0.5*sm1*sm1*(1.+ s)
  wd = 1. - (wa + wb + wc)
  w(-2,3) = merge(zero, wa, up)
  w(-1,3) = merge(wa, wb, up)
  w( 0,3) = merge(wb, wc, up)
  w( 1,3) = merge(wc, wd, up)
  w( 2,3) = merge(wd, zero, up)
END SUBROUTINE make_staggered_weights
! Calculate weights for centered quantities, after particle has moved
!-----------------------------------------------------------------------
SUBROUTINE make_centered_weights_updated(r,ox,oy,oz,w)
  implicit none
  real, dimension(3), intent(in) :: r
  integer,            intent(in) :: ox, oy, oz
  real, dimension(slc:suc,3)     :: w
  !
  real :: s, sm1, o, we(8)
  ! By making a longer support array, which is always zero outside
  ! the particle stencil, we can do the interpolation without if-statements
  we(1) = 0.
  we(2) = 0.
  we(7) = 0.
  we(8) = 0.
  ! ---------------------------------------------------------
  ! Find signed distance to center points in all directions.
  ! Project index into allowed range for interpolation.
  !-------------
  ! X_COORD CENTERED
  s = r(1); sm1 = 1. - s; o = 2. - s
  we(3) = one_sixth * sm1*sm1*sm1
  we(4) = two_third - 0.5*s*s*o
  we(5) = two_third - 0.5*sm1*sm1*(1.+ s)
  we(6) = 1.0 - (we(3)+we(4)+we(5))
  w(-2,1) = we(2-ox)
  w(-1,1) = we(3-ox)
  w( 0,1) = we(4-ox)
  w(+1,1) = we(5-ox)
  w(+2,1) = we(6-ox)
  w(+3,1) = we(7-ox)
  !-------------
  ! Y_COORD CENTERED
  s = r(2); sm1 = 1. - s; o = 2. - s
  we(3) = one_sixth * sm1*sm1*sm1
  we(4) = two_third - 0.5*s*s*o
  we(5) = two_third - 0.5*sm1*sm1*(1.+ s)
  we(6) = 1.0 - (we(3)+we(4)+we(5))
  w(-2,2) = we(2-oy)
  w(-1,2) = we(3-oy)
  w( 0,2) = we(4-oy)
  w(+1,2) = we(5-oy)
  w(+2,2) = we(6-oy)
  w(+3,2) = we(7-oy)
  !-------------
  ! Z_COORD CENTERED
  s = r(3); sm1 = 1. - s; o = 2. - s
  we(3) = one_sixth * sm1*sm1*sm1
  we(4) = two_third - 0.5*s*s*o
  we(5) = two_third - 0.5*sm1*sm1*(1.+ s)
  we(6) = 1.0 - (we(3)+we(4)+we(5))
  w(-2,3) = we(2-oz)
  w(-1,3) = we(3-oz)
  w( 0,3) = we(4-oz)
  w(+1,3) = we(5-oz)
  w(+2,3) = we(6-oz)
  w(+3,3) = we(7-oz)
END SUBROUTINE make_centered_weights_updated
! Calculate weights for centered quantities, after particle has moved
!-----------------------------------------------------------------------
SUBROUTINE make_staggered_weights_updated(r,ox,oy,oz,w)
  implicit none
  real, dimension(3), intent(in) :: r
  integer,            intent(in) :: ox, oy, oz
  real, dimension(slc:suc,3)     :: w
  !
  real :: s, sm1, we(6)
  integer :: o
  ! By making a longer support array, which is always zero outside
  ! the particle stencil, we can do the interpolation without if-statements
  we(1) = 0.
  we(6) = 0.
  ! ---------------------------------------------------------
  ! Find signed distance to center points in all directions.
  ! Project index into allowed range for interpolation.
  !-------------
  ! X_COORD STAGGERED
  s = r(1) - 0.5
  o = merge(ox, ox - 1, s >= 0)
  s = s + merge(0., 1., s >= 0); sm1 = 1. - s
  we(2) = one_sixth * sm1*sm1*sm1
  we(3) = two_third - 0.5*s*s*(2. - s)
  we(4) = two_third - 0.5*sm1*sm1*(1.+ s)
  we(5) = 1.0 - (we(2)+we(3)+we(4))
  w(-2,1) = we(1-o)
  w(-1,1) = we(2-o)
  w( 0,1) = we(3-o)
  w(+1,1) = we(4-o)
  w(+2,1) = we(5-o)
  !-------------
  ! Y_COORD STAGGERED
  s = r(2) - 0.5
  o = merge(oy, oy - 1, s >= 0)
  s = s + merge(0., 1., s >= 0); sm1 = 1. - s
  we(2) = one_sixth * sm1*sm1*sm1
  we(3) = two_third - 0.5*s*s*(2.- s)
  we(4) = two_third - 0.5*sm1*sm1*(1.+ s)
  we(5) = 1.0 - (we(2)+we(3)+we(4))
  w(-2,2) = we(1-o)
  w(-1,2) = we(2-o)
  w( 0,2) = we(3-o)
  w(+1,2) = we(4-o)
  w(+2,2) = we(5-o)
  !-------------
  ! Z_COORD STAGGERED
  s = r(3) - 0.5
  o = merge(oz, oz - 1, s >= 0)
  s = s + merge(0., 1., s >= 0); sm1 = 1. - s
  we(2) = one_sixth * sm1*sm1*sm1
  we(3) = two_third - 0.5*s*s*(2.- s)
  we(4) = two_third - 0.5*sm1*sm1*(1.+ s)
  we(5) = 1.0 - (we(2)+we(3)+we(4))
  w(-2,3) = we(1-o)
  w(-1,3) = we(2-o)
  w( 0,3) = we(3-o)
  w(+1,3) = we(4-o)
  w(+2,3) = we(5-o)
END SUBROUTINE make_staggered_weights_updated
! Make a mass conserving number density and number density flux deposition
!=======================================================================
SUBROUTINE mass_conserving_deposition(w_old,w_new,pw,p,ox,oy,oz,dxdt,dydt,dzdt)
  USE grid_m, only : g
  implicit none
  real,    intent(in), dimension(slc:suc,3) :: w_old, w_new
  real,    intent(in) :: pw, p(3), dxdt, dydt, dzdt
  integer, intent(in) :: ox, oy, oz
  !
  real    :: wa, wb, wc, wd, we                                         ! Scratch weights
  integer :: jxu,jyu,jzu,jxl,jyl,jzl,jx,jy,jz

  !---------------------------------------------------------------
  ! density is d(jx,jy,jz) = pw * w_new(jx,1) * w_new(jy,2) * w_new(jz,3)
  do jz=-1+oz,2+oz
    wa = pw*w_new(jz,3)
    do jy=-1+oy,2+oy
      we = w_new(jy,2)*wa
      d(-1+ox,jy,jz) = d(-1+ox,jy,jz) + w_new(-1+ox,1)*we
      d(   ox,jy,jz) = d( 0+ox,jy,jz) + w_new(   ox,1)*we
      d( 1+ox,jy,jz) = d( 1+ox,jy,jz) + w_new( 1+ox,1)*we
      d( 2+ox,jy,jz) = d( 2+ox,jy,jz) + w_new( 2+ox,1)*we
    enddo
  enddo

  !---------------------------------------------------------------
  ! Number density fluxes:
  jxl = min(-1+ox,-1); jxu=max(2+ox,2)                            ! Loop bounds
  jyl = min(-1+oy,-1); jyu=max(2+oy,2)
  jzl = min(-1+oz,-1); jzu=max(2+oz,2)
  wc = one_sixth * pw                                             ! particle weight / 6
  ! The weights for number density fluxes, in directions with more than one cell, are (for x-dir)
  ! I)   W_x(jx,jy,jz) = 1/6 * pw * dx/dt * (w_new(jx,3)-w_new(jx,3)) *
  !                             ( w_old(jy,2) * (2 w_old(jz,3) +   w_new(jz,3) ) +
  !                               w_new(jy,2) * (  w_old(jz,3) + 2 w_new(jz,3) ) )
  ! For directions with only one cell it becomes
  ! II)  W_x(jx,jy,jz) = 1/6 * pw * v_x *
  !                             ( w_old(jy,2) * (2 w_old(jz,3) +   w_new(jz,3) ) +
  !                               w_new(jy,2) * (  w_old(jz,3) + 2 w_new(jz,3) ) )
  ! Expression (I) computes d(nVx)/dt, while (II) computes directly nVx
  !
  ! See paper by Esirkepov (Comp Phys Comm 135 (2001) 144-153) and
  ! the Photon-Plasma code paper (in $code/Doc) for details
  !---------------------------------------------------------------
  ! x-number density flux
  if (g%gn(1)==1) then
    wd = wc * p(1)
    do jz=jzl,jzu
      wa = wd * (2*w_old(jz,3) +   w_new(jz,3))
      wb = wd * (  w_old(jz,3) + 2*w_new(jz,3))
      do jy=jyl,jyu
        we = wa*w_old(jy,2) + wb*w_new(jy,2)
        v(0,jy,jz,1)=v(0,jy,jz,1) + we
      enddo
    enddo
  else
    wd = wc * dxdt
    do jz=jzl,jzu
      wa = wd * (2*w_old(jz,3) +   w_new(jz,3))
      wb = wd * (  w_old(jz,3) + 2*w_new(jz,3))
      do jy=jyl,jyu
        we = wa*w_old(jy,2) + wb*w_new(jy,2)
        do jx=jxl,jxu
          v(jx,jy,jz,1)=v(jx,jy,jz,1) - (w_new(jx,1) - w_old(jx,1))*we
        enddo
      enddo
    enddo
  endif
  !---------------------------------------------------------------
  ! y-number density flux
  if (g%gn(2)==1) then
    wd = wc * p(2)
    do jx=jxl,jxu
      wa = wd * (2*w_old(jx,1) +   w_new(jx,1))
      wb = wd * (  w_old(jx,1) + 2*w_new(jx,1))
      do jz=jzl,jzu
        we = wa*w_old(jz,3) + wb*w_new(jz,3)
        v(jx,0,jz,2)=v(jx,0,jz,2) + we
      enddo
    enddo
  else
    wd = wc * dydt
    do jx=jxl,jxu
      wa = wd * (2*w_old(jx,1) +   w_new(jx,1))
      wb = wd * (  w_old(jx,1) + 2*w_new(jx,1))
      do jz=jzl,jzu
        we = wa*w_old(jz,3) + wb*w_new(jz,3)
        do jy=jyl,jyu
          v(jx,jy,jz,2)=v(jx,jy,jz,2) - (w_new(jy,2) - w_old(jy,2))*we
        enddo
      enddo
    enddo
  endif
  !---------------------------------------------------------------
  ! z-number density fluc
  if (g%gn(3)==1) then
    wd = wc * p(3)
    do jy=jyl,jyu
      wa = wd * (2*w_old(jy,2) +   w_new(jy,2))
      wb = wd * (  w_old(jy,2) + 2*w_new(jy,2))
      do jx=jxl,jxu
        we = wa*w_old(jx,1) + wb*w_new(jx,1)
        v(jx,jy,0,3)=v(jx,jy,0,3) + we
      enddo
    enddo
  else
    wd = wc * dzdt
    do jy=jyl,jyu
      wa = wd * (2*w_old(jy,2) +   w_new(jy,2))
      wb = wd * (  w_old(jy,2) + 2*w_new(jy,2))
      do jx=jxl,jxu
        we = wa*w_old(jx,1) + wb*w_new(jx,1)
        do jz=jzl,jzu
          v(jx,jy,jz,3)=v(jx,jy,jz,3) - (w_new(jz,3) - w_old(jz,3))*we
        enddo
      enddo
    enddo
  endif
END SUBROUTINE mass_conserving_deposition
! Make number density and number density flux deposition
!=======================================================================
SUBROUTINE energy_conserving_deposition(w,ws,w_old,ws_old,pw,p,ox,oy,oz)
  implicit none
  real,    intent(in), dimension(slc:suc,3) :: w, ws, w_old, ws_old
  real,    intent(in) :: pw, p(3)
  integer, intent(in) :: ox, oy, oz
  !
  real    :: wa, wb, wav(-2:3,3)
  integer :: jy,jz,jxl,jxu,jyl,jyu,jzl,jzu

  !---------------------------------------------------------------
  ! density is d(jx,jy,jz) = pw * w(jx,1) * w(jy,2) * w(jz,3)
  do jz=-1+oz,2+oz
    wa = pw*w(jz,3)
    do jy=-1+oy,2+oy
      wb = w(jy,2)*wa
      d(-1+ox,jy,jz) = d(-1+ox,jy,jz) + w(-1+ox,1)*wb
      d(   ox,jy,jz) = d( 0+ox,jy,jz) + w(   ox,1)*wb
      d( 1+ox,jy,jz) = d( 1+ox,jy,jz) + w( 1+ox,1)*wb
      d( 2+ox,jy,jz) = d( 2+ox,jy,jz) + w( 2+ox,1)*wb
    enddo
  enddo

  !---------------------------------------------------------------
  ! The velocity deposition is at t + dt/2, and we therefore use time averaged weights
  jxl = min(-1,ox-1); jxu=max(2,ox+2) ! limits encompasing both old and new non-zero weights
  jyl = min(-1,oy-1); jyu=max(2,oy+2)
  jzl = min(-1,oz-1); jzu=max(2,oz+2)
  wav(jxl:jxu,1)  = 0.5 * (w(jxl:jxu,1)  + w_old(jxl:jxu,1))
  wav(jyl:jyu,2)  = 0.5 * (w(jyl:jyu,2)  + w_old(jyl:jyu,2))
  wav(jzl:jzu,3)  = 0.5 * (w(jzl:jzu,3)  + w_old(jzl:jzu,3))

  !---------------------------------------------------------------
  ! x-velocity is v_x(jx,jy,jz) = p(1) * pw * ws(jx,1) * w(jy,2) * w(jz,3)
  do jz=jzl,jzu
    wa = 0.5*p(1)*pw*wav(jz,3)
    do jy=jyl,jyu
      wb = wav(jy,2)*wa
      v(-2:2,jy,jz,1) = v(-2:2,jy,jz,1) + (ws(-2:2,1) + ws_old(-2:2,1))*wb
    enddo
  enddo

  !---------------------------------------------------------------
  ! y-velocity is vy(jx,jy,jz) = p(2) * pw * w(jx,1) * ws(jy,2) * w(jz,3)
  do jz=jzl,jzu
    wa = 0.5*p(2)*pw*wav(jz,3)
    do jy=-2,2
      wb = (ws(jy,2) + ws_old(jy,2))*wa
      v(jxl:jxu,jy,jz,2) = v(jxl:jxu,jy,jz,2) + wav(jxl:jxu,1)*wb
    enddo
  enddo

  !---------------------------------------------------------------
  ! z-velocity is vz(jx,jy,jz) = p(3) * pw * w(jx,1) * w(jy,2) * ws(jz,3)
  do jz=-2,2
    wa = 0.5*p(3)*pw*(ws(jz,3) + ws_old(jz,3))
    do jy=jyl,jyu
      wb = wav(jy,2)*wa
      v(jxl:jxu,jy,jz,3) = v(jxl:jxu,jy,jz,3) + wav(jxl:jxu,1)*wb
    enddo
  enddo

END SUBROUTINE energy_conserving_deposition
! Cache global EM-fields to local arrays. If touched_row is true,
! we only need to read in the last row. The rest can be rolled up.
!=======================================================================
SUBROUTINE cache_em_fields(ix, iy, iz, touched_row, Ex, Ey, Ez, Bx, By, Bz)
  USE grid_m, only : g
  implicit none
  integer, intent(in) :: ix, iy, iz
  logical             :: touched_row
  real,    intent(in), dimension(g%n(1),g%n(2),g%n(3)) :: Ex, Ey, Ez, Bx, By, Bz
  !
  integer :: jx, jy, jz, kx, ky, kz
  ! copy fields over, taking care of boundaries and reusing rows in a rolling cache if touched_row=.true.
  !---------------------------------------------------------------------
  do jz=slc,sub
    kz = jz + iz
    if (kz > 0 .and. kz <= g%n(3)) then
      do jy=slc,sub
        ky = jy + iy
        if (ky > 0 .and. ky <= g%n(2)) then
          do jx=slc,sub
            kx = jx + ix
            if (kx > 0 .and. kx <= g%n(1)) then
              if (touched_row) then
                if (jx==slc) then
                  if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = sEx(jx+1,jy,jz)
                  if (jy > slc) sBy(jx,jy,jz) = sBy(jx+1,jy,jz)
                  if (jz > slc) sBz(jx,jy,jz) = sBz(jx+1,jy,jz)
                else if (jx < sub) then
                  if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = sEx(jx+1,jy,jz)
                  if (jz > slc) sEy(jx,jy,jz) = sEy(jx+1,jy,jz)
                  if (jy > slc) sEz(jx,jy,jz) = sEz(jx+1,jy,jz)
                  sBx(jx,jy,jz) = sBx(jx+1,jy,jz)
                  if (jy > slc) sBy(jx,jy,jz) = sBy(jx+1,jy,jz)
                  if (jz > slc) sBz(jx,jy,jz) = sBz(jx+1,jy,jz)
                else !if (jx==sub) then ! "if" not needed, this is the last option
                  if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = Ex(kx,ky,kz)
                  if (jz > slc) sEy(jx,jy,jz) = Ey(kx,ky,kz)
                  if (jy > slc) sEz(jx,jy,jz) = Ez(kx,ky,kz)
                  sBx(jx,jy,jz) = Bx(kx,ky,kz)
                  if (jy > slc) sBy(jx,jy,jz) = By(kx,ky,kz)
                  if (jz > slc) sBz(jx,jy,jz) = Bz(kx,ky,kz)
                endif
              else
                if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = Ex(kx,ky,kz)
                if (jx > slc .and. jz > slc) sEy(jx,jy,jz) = Ey(kx,ky,kz)
                if (jx > slc .and. jy > slc) sEz(jx,jy,jz) = Ez(kx,ky,kz)
                if (jx > slc) sBx(jx,jy,jz) = Bx(kx,ky,kz)
                if (jy > slc) sBy(jx,jy,jz) = By(kx,ky,kz)
                if (jz > slc) sBz(jx,jy,jz) = Bz(kx,ky,kz)
              endif
            else
              if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = 0.
              if (jx > slc .and. jz > slc) sEy(jx,jy,jz) = 0.
              if (jx > slc .and. jy > slc) sEz(jx,jy,jz) = 0.
              if (jx > slc) sBx(jx,jy,jz) = 0.
              if (jy > slc) sBy(jx,jy,jz) = 0.
              if (jz > slc) sBz(jx,jy,jz) = 0.
            endif
          enddo
        else
          do jx=slc,sub
            if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = 0.
            if (jx > slc .and. jz > slc) sEy(jx,jy,jz) = 0.
            if (jx > slc .and. jy > slc) sEz(jx,jy,jz) = 0.
            if (jx > slc) sBx(jx,jy,jz) = 0.
            if (jy > slc) sBy(jx,jy,jz) = 0.
            if (jz > slc) sBz(jx,jy,jz) = 0.
          enddo
        endif
      enddo
    else
      do jy=slc,sub
        do jx=slc,sub
          if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = 0.
          if (jx > slc .and. jz > slc) sEy(jx,jy,jz) = 0.
          if (jx > slc .and. jy > slc) sEz(jx,jy,jz) = 0.
          if (jx > slc) sBx(jx,jy,jz) = 0.
          if (jy > slc) sBy(jx,jy,jz) = 0.
          if (jz > slc) sBz(jx,jy,jz) = 0.
        enddo
      enddo
    endif
  enddo
  touched_row = .true.
END SUBROUTINE cache_em_fields
! Given weights, find the E- and B-fields interpolated at the particle position.
!-----------------------------------------------------------------------
SUBROUTINE interpolate_fields(w,ws,E,B)
  implicit none
  real, intent(in), dimension(slc:suc,3) :: w, ws                       ! Weights for centered and staggered vertices
  real, intent(out) :: E(3), B(3)
!-----------------------------------
! X SHIFTED WEIGHTS
E(1) = &
   ((sEx(-2,-1,-1)*ws(-2,1) + &
     sEx(-1,-1,-1)*ws(-1,1) + &
     sEx( 0,-1,-1)*ws( 0,1) + &
     sEx(+1,-1,-1)*ws( 1,1) + &
     sEx(+2,-1,-1)*ws( 2,1))*w(-1,2) + &
    (sEx(-2, 0,-1)*ws(-2,1) + &
     sEx(-1, 0,-1)*ws(-1,1) + &
     sEx( 0, 0,-1)*ws( 0,1) + &
     sEx(+1, 0,-1)*ws( 1,1) + &
     sEx(+2, 0,-1)*ws( 2,1))*w( 0,2) + &
    (sEx(-2,+1,-1)*ws(-2,1) + &
     sEx(-1,+1,-1)*ws(-1,1) + &
     sEx( 0,+1,-1)*ws( 0,1) + &
     sEx(+1,+1,-1)*ws( 1,1) + &
     sEx(+2,+1,-1)*ws( 2,1))*w( 1,2) + &
    (sEx(-2,+2,-1)*ws(-2,1) + &
     sEx(-1,+2,-1)*ws(-1,1) + &
     sEx( 0,+2,-1)*ws( 0,1) + &
     sEx(+1,+2,-1)*ws( 1,1) + &
     sEx(+2,+2,-1)*ws( 2,1))*w( 2,2))*w(-1,3) + &
   ((sEx(-2,-1, 0)*ws(-2,1) + &
     sEx(-1,-1, 0)*ws(-1,1) + &
     sEx( 0,-1, 0)*ws( 0,1) + &
     sEx(+1,-1, 0)*ws( 1,1) + &
     sEx(+2,-1, 0)*ws( 2,1))*w(-1,2) + &
    (sEx(-2, 0, 0)*ws(-2,1) + &
     sEx(-1, 0, 0)*ws(-1,1) + &
     sEx( 0, 0, 0)*ws( 0,1) + &
     sEx(+1, 0, 0)*ws( 1,1) + &
     sEx(+2, 0, 0)*ws( 2,1))*w( 0,2) + &
    (sEx(-2,+1, 0)*ws(-2,1) + &
     sEx(-1,+1, 0)*ws(-1,1) + &
     sEx( 0,+1, 0)*ws( 0,1) + &
     sEx(+1,+1, 0)*ws( 1,1) + &
     sEx(+2,+1, 0)*ws( 2,1))*w( 1,2) + &
    (sEx(-2,+2, 0)*ws(-2,1) + &
     sEx(-1,+2, 0)*ws(-1,1) + &
     sEx( 0,+2, 0)*ws( 0,1) + &
     sEx(+1,+2, 0)*ws( 1,1) + &
     sEx(+2,+2, 0)*ws( 2,1))*w( 2,2))*w( 0,3) + &
   ((sEx(-2,-1,+1)*ws(-2,1) + &
     sEx(-1,-1,+1)*ws(-1,1) + &
     sEx( 0,-1,+1)*ws( 0,1) + &
     sEx(+1,-1,+1)*ws( 1,1) + &
     sEx(+2,-1,+1)*ws( 2,1))*w(-1,2) + &
    (sEx(-2, 0,+1)*ws(-2,1) + &
     sEx(-1, 0,+1)*ws(-1,1) + &
     sEx( 0, 0,+1)*ws( 0,1) + &
     sEx(+1, 0,+1)*ws( 1,1) + &
     sEx(+2, 0,+1)*ws( 2,1))*w( 0,2) + &
    (sEx(-2,+1,+1)*ws(-2,1) + &
     sEx(-1,+1,+1)*ws(-1,1) + &
     sEx( 0,+1,+1)*ws( 0,1) + &
     sEx(+1,+1,+1)*ws( 1,1) + &
     sEx(+2,+1,+1)*ws( 2,1))*w( 1,2) + &
    (sEx(-2,+2,+1)*ws(-2,1) + &
     sEx(-1,+2,+1)*ws(-1,1) + &
     sEx( 0,+2,+1)*ws( 0,1) + &
     sEx(+1,+2,+1)*ws( 1,1) + &
     sEx(+2,+2,+1)*ws( 2,1))*w( 2,2))*w( 1,3) + &
   ((sEx(-2,-1,+2)*ws(-2,1) + &
     sEx(-1,-1,+2)*ws(-1,1) + &
     sEx( 0,-1,+2)*ws( 0,1) + &
     sEx(+1,-1,+2)*ws( 1,1) + &
     sEx(+2,-1,+2)*ws( 2,1))*w(-1,2) + &
    (sEx(-2, 0,+2)*ws(-2,1) + &
     sEx(-1, 0,+2)*ws(-1,1) + &
     sEx( 0, 0,+2)*ws( 0,1) + &
     sEx(+1, 0,+2)*ws( 1,1) + &
     sEx(+2, 0,+2)*ws( 2,1))*w( 0,2) + &
    (sEx(-2,+1,+2)*ws(-2,1) + &
     sEx(-1,+1,+2)*ws(-1,1) + &
     sEx( 0,+1,+2)*ws( 0,1) + &
     sEx(+1,+1,+2)*ws( 1,1) + &
     sEx(+2,+1,+2)*ws( 2,1))*w( 1,2) + &
    (sEx(-2,+2,+2)*ws(-2,1) + &
     sEx(-1,+2,+2)*ws(-1,1) + &
     sEx( 0,+2,+2)*ws( 0,1) + &
     sEx(+1,+2,+2)*ws( 1,1) + &
     sEx(+2,+2,+2)*ws( 2,1))*w( 2,2))*w( 2,3)
!-----------------------------------
! Y SHIFTED WEIGHTS
E(2) = &
  ((sEy(-1,-2,-1)*w(-1,1) + &
    sEy( 0,-2,-1)*w( 0,1) + &
    sEy(+1,-2,-1)*w( 1,1) + &
    sEy(+2,-2,-1)*w( 2,1))*ws(-2,2) + &
   (sEy(-1,-1,-1)*w(-1,1) + &
    sEy( 0,-1,-1)*w( 0,1) + &
    sEy(+1,-1,-1)*w( 1,1) + &
    sEy(+2,-1,-1)*w( 2,1))*ws(-1,2) + &
   (sEy(-1, 0,-1)*w(-1,1) + &
    sEy( 0, 0,-1)*w( 0,1) + &
    sEy(+1, 0,-1)*w( 1,1) + &
    sEy(+2, 0,-1)*w( 2,1))*ws( 0,2) + &
   (sEy(-1,+1,-1)*w(-1,1) + &
    sEy( 0,+1,-1)*w( 0,1) + &
    sEy(+1,+1,-1)*w( 1,1) + &
    sEy(+2,+1,-1)*w( 2,1))*ws( 1,2) + &
   (sEy(-1,+2,-1)*w(-1,1) + &
    sEy( 0,+2,-1)*w( 0,1) + &
    sEy(+1,+2,-1)*w( 1,1) + &
    sEy(+2,+2,-1)*w( 2,1))*ws( 2,2))*w(-1,3) + &
  ((sEy(-1,-2, 0)*w(-1,1) + &
    sEy( 0,-2, 0)*w( 0,1) + &
    sEy(+1,-2, 0)*w( 1,1) + &
    sEy(+2,-2, 0)*w( 2,1))*ws(-2,2) + &
   (sEy(-1,-1, 0)*w(-1,1) + &
    sEy( 0,-1, 0)*w( 0,1) + &
    sEy(+1,-1, 0)*w( 1,1) + &
    sEy(+2,-1, 0)*w( 2,1))*ws(-1,2) + &
   (sEy(-1, 0, 0)*w(-1,1) + &
    sEy( 0, 0, 0)*w( 0,1) + &
    sEy(+1, 0, 0)*w( 1,1) + &
    sEy(+2, 0, 0)*w( 2,1))*ws( 0,2) + &
   (sEy(-1,+1, 0)*w(-1,1) + &
    sEy( 0,+1, 0)*w( 0,1) + &
    sEy(+1,+1, 0)*w( 1,1) + &
    sEy(+2,+1, 0)*w( 2,1))*ws( 1,2) + &
   (sEy(-1,+2, 0)*w(-1,1) + &
    sEy( 0,+2, 0)*w( 0,1) + &
    sEy(+1,+2, 0)*w( 1,1) + &
    sEy(+2,+2, 0)*w( 2,1))*ws( 2,2))*w( 0,3) + &
  ((sEy(-1,-2,+1)*w(-1,1) + &
    sEy( 0,-2,+1)*w( 0,1) + &
    sEy(+1,-2,+1)*w( 1,1) + &
    sEy(+2,-2,+1)*w( 2,1))*ws(-2,2) + &
   (sEy(-1,-1,+1)*w(-1,1) + &
    sEy( 0,-1,+1)*w( 0,1) + &
    sEy(+1,-1,+1)*w( 1,1) + &
    sEy(+2,-1,+1)*w( 2,1))*ws(-1,2) + &
   (sEy(-1, 0,+1)*w(-1,1) + &
    sEy( 0, 0,+1)*w( 0,1) + &
    sEy(+1, 0,+1)*w( 1,1) + &
    sEy(+2, 0,+1)*w( 2,1))*ws( 0,2) + &
   (sEy(-1,+1,+1)*w(-1,1) + &
    sEy( 0,+1,+1)*w( 0,1) + &
    sEy(+1,+1,+1)*w( 1,1) + &
    sEy(+2,+1,+1)*w( 2,1))*ws( 1,2) + &
   (sEy(-1,+2,+1)*w(-1,1) + &
    sEy( 0,+2,+1)*w( 0,1) + &
    sEy(+1,+2,+1)*w( 1,1) + &
    sEy(+2,+2,+1)*w( 2,1))*ws( 2,2))*w( 1,3) + &
  ((sEy(-1,-2,+2)*w(-1,1) + &
    sEy( 0,-2,+2)*w( 0,1) + &
    sEy(+1,-2,+2)*w( 1,1) + &
    sEy(+2,-2,+2)*w( 2,1))*ws(-2,2) + &
   (sEy(-1,-1,+2)*w(-1,1) + &
    sEy( 0,-1,+2)*w( 0,1) + &
    sEy(+1,-1,+2)*w( 1,1) + &
    sEy(+2,-1,+2)*w( 2,1))*ws(-1,2) + &
   (sEy(-1, 0,+2)*w(-1,1) + &
    sEy( 0, 0,+2)*w( 0,1) + &
    sEy(+1, 0,+2)*w( 1,1) + &
    sEy(+2, 0,+2)*w( 2,1))*ws( 0,2) + &
   (sEy(-1,+1,+2)*w(-1,1) + &
    sEy( 0,+1,+2)*w( 0,1) + &
    sEy(+1,+1,+2)*w( 1,1) + &
    sEy(+2,+1,+2)*w( 2,1))*ws( 1,2) + &
   (sEy(-1,+2,+2)*w(-1,1) + &
    sEy( 0,+2,+2)*w( 0,1) + &
    sEy(+1,+2,+2)*w( 1,1) + &
    sEy(+2,+2,+2)*w( 2,1))*ws( 2,2))*w( 2,3)
!-----------------------------------
! Z SHIFTED WEIGHTS
E(3) = &
 ((sEz(-1,-1,-2)*w(-1,1) + &
   sEz( 0,-1,-2)*w( 0,1) + &
   sEz(+1,-1,-2)*w( 1,1) + &
   sEz(+2,-1,-2)*w( 2,1))*w(-1,2) + &
  (sEz(-1, 0,-2)*w(-1,1) + &
   sEz( 0, 0,-2)*w( 0,1) + &
   sEz(+1, 0,-2)*w( 1,1) + &
   sEz(+2, 0,-2)*w( 2,1))*w( 0,2) + &
  (sEz(-1,+1,-2)*w(-1,1) + &
   sEz( 0,+1,-2)*w( 0,1) + &
   sEz(+1,+1,-2)*w( 1,1) + &
   sEz(+2,+1,-2)*w( 2,1))*w( 1,2) + &
  (sEz(-1,+2,-2)*w(-1,1) + &
   sEz( 0,+2,-2)*w( 0,1) + &
   sEz(+1,+2,-2)*w( 1,1) + &
   sEz(+2,+2,-2)*w( 2,1))*w( 2,2))*ws(-2,3) + &
 ((sEz(-1,-1,-1)*w(-1,1) + &
   sEz( 0,-1,-1)*w( 0,1) + &
   sEz(+1,-1,-1)*w( 1,1) + &
   sEz(+2,-1,-1)*w( 2,1))*w(-1,2) + &
  (sEz(-1, 0,-1)*w(-1,1) + &
   sEz( 0, 0,-1)*w( 0,1) + &
   sEz(+1, 0,-1)*w( 1,1) + &
   sEz(+2, 0,-1)*w( 2,1))*w( 0,2) + &
  (sEz(-1,+1,-1)*w(-1,1) + &
   sEz( 0,+1,-1)*w( 0,1) + &
   sEz(+1,+1,-1)*w( 1,1) + &
   sEz(+2,+1,-1)*w( 2,1))*w( 1,2) + &
  (sEz(-1,+2,-1)*w(-1,1) + &
   sEz( 0,+2,-1)*w( 0,1) + &
   sEz(+1,+2,-1)*w( 1,1) + &
   sEz(+2,+2,-1)*w( 2,1))*w( 2,2))*ws(-1,3) + &
 ((sEz(-1,-1, 0)*w(-1,1) + &
   sEz( 0,-1, 0)*w( 0,1) + &
   sEz(+1,-1, 0)*w( 1,1) + &
   sEz(+2,-1, 0)*w( 2,1))*w(-1,2) + &
  (sEz(-1, 0, 0)*w(-1,1) + &
   sEz( 0, 0, 0)*w( 0,1) + &
   sEz(+1, 0, 0)*w( 1,1) + &
   sEz(+2, 0, 0)*w( 2,1))*w( 0,2) + &
  (sEz(-1,+1, 0)*w(-1,1) + &
   sEz( 0,+1, 0)*w( 0,1) + &
   sEz(+1,+1, 0)*w( 1,1) + &
   sEz(+2,+1, 0)*w( 2,1))*w( 1,2) + &
  (sEz(-1,+2, 0)*w(-1,1) + &
   sEz( 0,+2, 0)*w( 0,1) + &
   sEz(+1,+2, 0)*w( 1,1) + &
   sEz(+2,+2, 0)*w( 2,1))*w( 2,2))*ws( 0,3) + &
 ((sEz(-1,-1,+1)*w(-1,1) + &
   sEz( 0,-1,+1)*w( 0,1) + &
   sEz(+1,-1,+1)*w( 1,1) + &
   sEz(+2,-1,+1)*w( 2,1))*w(-1,2) + &
  (sEz(-1, 0,+1)*w(-1,1) + &
   sEz( 0, 0,+1)*w( 0,1) + &
   sEz(+1, 0,+1)*w( 1,1) + &
   sEz(+2, 0,+1)*w( 2,1))*w( 0,2) + &
  (sEz(-1,+1,+1)*w(-1,1) + &
   sEz( 0,+1,+1)*w( 0,1) + &
   sEz(+1,+1,+1)*w( 1,1) + &
   sEz(+2,+1,+1)*w( 2,1))*w( 1,2) + &
  (sEz(-1,+2,+1)*w(-1,1) + &
   sEz( 0,+2,+1)*w( 0,1) + &
   sEz(+1,+2,+1)*w( 1,1) + &
   sEz(+2,+2,+1)*w( 2,1))*w( 2,2))*ws( 1,3) + &
 ((sEz(-1,-1,+2)*w(-1,1) + &
   sEz( 0,-1,+2)*w( 0,1) + &
   sEz(+1,-1,+2)*w( 1,1) + &
   sEz(+2,-1,+2)*w( 2,1))*w(-1,2) + &
  (sEz(-1, 0,+2)*w(-1,1) + &
   sEz( 0, 0,+2)*w( 0,1) + &
   sEz(+1, 0,+2)*w( 1,1) + &
   sEz(+2, 0,+2)*w( 2,1))*w( 0,2) + &
  (sEz(-1,+1,+2)*w(-1,1) + &
   sEz( 0,+1,+2)*w( 0,1) + &
   sEz(+1,+1,+2)*w( 1,1) + &
   sEz(+2,+1,+2)*w( 2,1))*w( 1,2) + &
  (sEz(-1,+2,+2)*w(-1,1) + &
   sEz( 0,+2,+2)*w( 0,1) + &
   sEz(+1,+2,+2)*w( 1,1) + &
   sEz(+2,+2,+2)*w( 2,1))*w( 2,2))*ws( 2,3)
!-----------------------------------
! YZ SHIFTED WEIGHTS
B(1) = &
 ((sBx(-1,-2,-2)*w(-1,1) + &
   sBx( 0,-2,-2)*w( 0,1) + &
   sBx(+1,-2,-2)*w( 1,1) + &
   sBx(+2,-2,-2)*w( 2,1))*ws(-2,2) + &
  (sBx(-1,-1,-2)*w(-1,1) + &
   sBx( 0,-1,-2)*w( 0,1) + &
   sBx(+1,-1,-2)*w( 1,1) + &
   sBx(+2,-1,-2)*w( 2,1))*ws(-1,2) + &
  (sBx(-1, 0,-2)*w(-1,1) + &
   sBx( 0, 0,-2)*w( 0,1) + &
   sBx(+1, 0,-2)*w( 1,1) + &
   sBx(+2, 0,-2)*w( 2,1))*ws( 0,2) + &
  (sBx(-1,+1,-2)*w(-1,1) + &
   sBx( 0,+1,-2)*w( 0,1) + &
   sBx(+1,+1,-2)*w( 1,1) + &
   sBx(+2,+1,-2)*w( 2,1))*ws( 1,2) + &
  (sBx(-1,+2,-2)*w(-1,1) + &
   sBx( 0,+2,-2)*w( 0,1) + &
   sBx(+1,+2,-2)*w( 1,1) + &
   sBx(+2,+2,-2)*w( 2,1))*ws( 2,2))*ws(-2,3) + &
 ((sBx(-1,-2,-1)*w(-1,1) + &
   sBx( 0,-2,-1)*w( 0,1) + &
   sBx(+1,-2,-1)*w( 1,1) + &
   sBx(+2,-2,-1)*w( 2,1))*ws(-2,2) + &
  (sBx(-1,-1,-1)*w(-1,1) + &
   sBx( 0,-1,-1)*w( 0,1) + &
   sBx(+1,-1,-1)*w( 1,1) + &
   sBx(+2,-1,-1)*w( 2,1))*ws(-1,2) + &
  (sBx(-1, 0,-1)*w(-1,1) + &
   sBx( 0, 0,-1)*w( 0,1) + &
   sBx(+1, 0,-1)*w( 1,1) + &
   sBx(+2, 0,-1)*w( 2,1))*ws( 0,2) + &
  (sBx(-1,+1,-1)*w(-1,1) + &
   sBx( 0,+1,-1)*w( 0,1) + &
   sBx(+1,+1,-1)*w( 1,1) + &
   sBx(+2,+1,-1)*w( 2,1))*ws( 1,2) + &
  (sBx(-1,+2,-1)*w(-1,1) + &
   sBx( 0,+2,-1)*w( 0,1) + &
   sBx(+1,+2,-1)*w( 1,1) + &
   sBx(+2,+2,-1)*w( 2,1))*ws( 2,2))*ws(-1,3) + &
 ((sBx(-1,-2, 0)*w(-1,1) + &
   sBx( 0,-2, 0)*w( 0,1) + &
   sBx(+1,-2, 0)*w( 1,1) + &
   sBx(+2,-2, 0)*w( 2,1))*ws(-2,2) + &
  (sBx(-1,-1, 0)*w(-1,1) + &
   sBx( 0,-1, 0)*w( 0,1) + &
   sBx(+1,-1, 0)*w( 1,1) + &
   sBx(+2,-1, 0)*w( 2,1))*ws(-1,2) + &
  (sBx(-1, 0, 0)*w(-1,1) + &
   sBx( 0, 0, 0)*w( 0,1) + &
   sBx(+1, 0, 0)*w( 1,1) + &
   sBx(+2, 0, 0)*w( 2,1))*ws( 0,2) + &
  (sBx(-1,+1, 0)*w(-1,1) + &
   sBx( 0,+1, 0)*w( 0,1) + &
   sBx(+1,+1, 0)*w( 1,1) + &
   sBx(+2,+1, 0)*w( 2,1))*ws( 1,2) + &
  (sBx(-1,+2, 0)*w(-1,1) + &
   sBx( 0,+2, 0)*w( 0,1) + &
   sBx(+1,+2, 0)*w( 1,1) + &
   sBx(+2,+2, 0)*w( 2,1))*ws( 2,2))*ws( 0,3) + &
 ((sBx(-1,-2,+1)*w(-1,1) + &
   sBx( 0,-2,+1)*w( 0,1) + &
   sBx(+1,-2,+1)*w( 1,1) + &
   sBx(+2,-2,+1)*w( 2,1))*ws(-2,2) + &
  (sBx(-1,-1,+1)*w(-1,1) + &
   sBx( 0,-1,+1)*w( 0,1) + &
   sBx(+1,-1,+1)*w( 1,1) + &
   sBx(+2,-1,+1)*w( 2,1))*ws(-1,2) + &
  (sBx(-1, 0,+1)*w(-1,1) + &
   sBx( 0, 0,+1)*w( 0,1) + &
   sBx(+1, 0,+1)*w( 1,1) + &
   sBx(+2, 0,+1)*w( 2,1))*ws( 0,2) + &
  (sBx(-1,+1,+1)*w(-1,1) + &
   sBx( 0,+1,+1)*w( 0,1) + &
   sBx(+1,+1,+1)*w( 1,1) + &
   sBx(+2,+1,+1)*w( 2,1))*ws( 1,2) + &
  (sBx(-1,+2,+1)*w(-1,1) + &
   sBx( 0,+2,+1)*w( 0,1) + &
   sBx(+1,+2,+1)*w( 1,1) + &
   sBx(+2,+2,+1)*w( 2,1))*ws( 2,2))*ws( 1,3) + &
 ((sBx(-1,-2,+2)*w(-1,1) + &
   sBx( 0,-2,+2)*w( 0,1) + &
   sBx(+1,-2,+2)*w( 1,1) + &
   sBx(+2,-2,+2)*w( 2,1))*ws(-2,2) + &
  (sBx(-1,-1,+2)*w(-1,1) + &
   sBx( 0,-1,+2)*w( 0,1) + &
   sBx(+1,-1,+2)*w( 1,1) + &
   sBx(+2,-1,+2)*w( 2,1))*ws(-1,2) + &
  (sBx(-1, 0,+2)*w(-1,1) + &
   sBx( 0, 0,+2)*w( 0,1) + &
   sBx(+1, 0,+2)*w( 1,1) + &
   sBx(+2, 0,+2)*w( 2,1))*ws( 0,2) + &
  (sBx(-1,+1,+2)*w(-1,1) + &
   sBx( 0,+1,+2)*w( 0,1) + &
   sBx(+1,+1,+2)*w( 1,1) + &
   sBx(+2,+1,+2)*w( 2,1))*ws( 1,2) + &
  (sBx(-1,+2,+2)*w(-1,1) + &
   sBx( 0,+2,+2)*w( 0,1) + &
   sBx(+1,+2,+2)*w( 1,1) + &
   sBx(+2,+2,+2)*w( 2,1))*ws( 2,2))*ws( 2,3)
!-----------------------------------
! ZX SHIFTED WEIGHTS
B(2) = &
 ((sBy(-2,-1,-2)*ws(-2,1) + &
   sBy(-1,-1,-2)*ws(-1,1) + &
   sBy( 0,-1,-2)*ws( 0,1) + &
   sBy(+1,-1,-2)*ws( 1,1) + &
   sBy(+2,-1,-2)*ws( 2,1))*w(-1,2) + &
  (sBy(-2, 0,-2)*ws(-2,1) + &
   sBy(-1, 0,-2)*ws(-1,1) + &
   sBy( 0, 0,-2)*ws( 0,1) + &
   sBy(+1, 0,-2)*ws( 1,1) + &
   sBy(+2, 0,-2)*ws( 2,1))*w( 0,2) + &
  (sBy(-2,+1,-2)*ws(-2,1) + &
   sBy(-1,+1,-2)*ws(-1,1) + &
   sBy( 0,+1,-2)*ws( 0,1) + &
   sBy(+1,+1,-2)*ws( 1,1) + &
   sBy(+2,+1,-2)*ws( 2,1))*w( 1,2) + &
  (sBy(-2,+2,-2)*ws(-2,1) + &
   sBy(-1,+2,-2)*ws(-1,1) + &
   sBy( 0,+2,-2)*ws( 0,1) + &
   sBy(+1,+2,-2)*ws( 1,1) + &
   sBy(+2,+2,-2)*ws( 2,1))*w( 2,2))*ws(-2,3) + &
 ((sBy(-2,-1,-1)*ws(-2,1) + &
   sBy(-1,-1,-1)*ws(-1,1) + &
   sBy( 0,-1,-1)*ws( 0,1) + &
   sBy(+1,-1,-1)*ws( 1,1) + &
   sBy(+2,-1,-1)*ws( 2,1))*w(-1,2) + &
  (sBy(-2, 0,-1)*ws(-2,1) + &
   sBy(-1, 0,-1)*ws(-1,1) + &
   sBy( 0, 0,-1)*ws( 0,1) + &
   sBy(+1, 0,-1)*ws( 1,1) + &
   sBy(+2, 0,-1)*ws( 2,1))*w( 0,2) + &
  (sBy(-2,+1,-1)*ws(-2,1) + &
   sBy(-1,+1,-1)*ws(-1,1) + &
   sBy( 0,+1,-1)*ws( 0,1) + &
   sBy(+1,+1,-1)*ws( 1,1) + &
   sBy(+2,+1,-1)*ws( 2,1))*w( 1,2) + &
  (sBy(-2,+2,-1)*ws(-2,1) + &
   sBy(-1,+2,-1)*ws(-1,1) + &
   sBy( 0,+2,-1)*ws( 0,1) + &
   sBy(+1,+2,-1)*ws( 1,1) + &
   sBy(+2,+2,-1)*ws( 2,1))*w( 2,2))*ws(-1,3) + &
 ((sBy(-2,-1, 0)*ws(-2,1) + &
   sBy(-1,-1, 0)*ws(-1,1) + &
   sBy( 0,-1, 0)*ws( 0,1) + &
   sBy(+1,-1, 0)*ws( 1,1) + &
   sBy(+2,-1, 0)*ws( 2,1))*w(-1,2) + &
  (sBy(-2, 0, 0)*ws(-2,1) + &
   sBy(-1, 0, 0)*ws(-1,1) + &
   sBy( 0, 0, 0)*ws( 0,1) + &
   sBy(+1, 0, 0)*ws( 1,1) + &
   sBy(+2, 0, 0)*ws( 2,1))*w( 0,2) + &
  (sBy(-2,+1, 0)*ws(-2,1) + &
   sBy(-1,+1, 0)*ws(-1,1) + &
   sBy( 0,+1, 0)*ws( 0,1) + &
   sBy(+1,+1, 0)*ws( 1,1) + &
   sBy(+2,+1, 0)*ws( 2,1))*w( 1,2) + &
  (sBy(-2,+2, 0)*ws(-2,1) + &
   sBy(-1,+2, 0)*ws(-1,1) + &
   sBy( 0,+2, 0)*ws( 0,1) + &
   sBy(+1,+2, 0)*ws( 1,1) + &
   sBy(+2,+2, 0)*ws( 2,1))*w( 2,2))*ws( 0,3) + &
 ((sBy(-2,-1,+1)*ws(-2,1) + &
   sBy(-1,-1,+1)*ws(-1,1) + &
   sBy( 0,-1,+1)*ws( 0,1) + &
   sBy(+1,-1,+1)*ws( 1,1) + &
   sBy(+2,-1,+1)*ws( 2,1))*w(-1,2) + &
  (sBy(-2, 0,+1)*ws(-2,1) + &
   sBy(-1, 0,+1)*ws(-1,1) + &
   sBy( 0, 0,+1)*ws( 0,1) + &
   sBy(+1, 0,+1)*ws( 1,1) + &
   sBy(+2, 0,+1)*ws( 2,1))*w( 0,2) + &
  (sBy(-2,+1,+1)*ws(-2,1) + &
   sBy(-1,+1,+1)*ws(-1,1) + &
   sBy( 0,+1,+1)*ws( 0,1) + &
   sBy(+1,+1,+1)*ws( 1,1) + &
   sBy(+2,+1,+1)*ws( 2,1))*w( 1,2) + &
  (sBy(-2,+2,+1)*ws(-2,1) + &
   sBy(-1,+2,+1)*ws(-1,1) + &
   sBy( 0,+2,+1)*ws( 0,1) + &
   sBy(+1,+2,+1)*ws( 1,1) + &
   sBy(+2,+2,+1)*ws( 2,1))*w( 2,2))*ws( 1,3) + &
 ((sBy(-2,-1,+2)*ws(-2,1) + &
   sBy(-1,-1,+2)*ws(-1,1) + &
   sBy( 0,-1,+2)*ws( 0,1) + &
   sBy(+1,-1,+2)*ws( 1,1) + &
   sBy(+2,-1,+2)*ws( 2,1))*w(-1,2) + &
  (sBy(-2, 0,+2)*ws(-2,1) + &
   sBy(-1, 0,+2)*ws(-1,1) + &
   sBy( 0, 0,+2)*ws( 0,1) + &
   sBy(+1, 0,+2)*ws( 1,1) + &
   sBy(+2, 0,+2)*ws( 2,1))*w( 0,2) + &
  (sBy(-2,+1,+2)*ws(-2,1) + &
   sBy(-1,+1,+2)*ws(-1,1) + &
   sBy( 0,+1,+2)*ws( 0,1) + &
   sBy(+1,+1,+2)*ws( 1,1) + &
   sBy(+2,+1,+2)*ws( 2,1))*w( 1,2) + &
  (sBy(-2,+2,+2)*ws(-2,1) + &
   sBy(-1,+2,+2)*ws(-1,1) + &
   sBy( 0,+2,+2)*ws( 0,1) + &
   sBy(+1,+2,+2)*ws( 1,1) + &
   sBy(+2,+2,+2)*ws( 2,1))*w( 2,2))*ws( 2,3)
!-----------------------------------
! XY SHIFTED WEIGHTS
B(3) = &
 ((sBz(-2,-2,-1)*ws(-2,1) + &
   sBz(-1,-2,-1)*ws(-1,1) + &
   sBz( 0,-2,-1)*ws( 0,1) + &
   sBz(+1,-2,-1)*ws( 1,1) + &
   sBz(+2,-2,-1)*ws( 2,1))*ws(-2,2) + &
  (sBz(-2,-1,-1)*ws(-2,1) + &
   sBz(-1,-1,-1)*ws(-1,1) + &
   sBz( 0,-1,-1)*ws( 0,1) + &
   sBz(+1,-1,-1)*ws( 1,1) + &
   sBz(+2,-1,-1)*ws( 2,1))*ws(-1,2) + &
  (sBz(-2, 0,-1)*ws(-2,1) + &
   sBz(-1, 0,-1)*ws(-1,1) + &
   sBz( 0, 0,-1)*ws( 0,1) + &
   sBz(+1, 0,-1)*ws( 1,1) + &
   sBz(+2, 0,-1)*ws( 2,1))*ws( 0,2) + &
  (sBz(-2,+1,-1)*ws(-2,1) + &
   sBz(-1,+1,-1)*ws(-1,1) + &
   sBz( 0,+1,-1)*ws( 0,1) + &
   sBz(+1,+1,-1)*ws( 1,1) + &
   sBz(+2,+1,-1)*ws( 2,1))*ws( 1,2) + &
  (sBz(-2,+2,-1)*ws(-2,1) + &
   sBz(-1,+2,-1)*ws(-1,1) + &
   sBz( 0,+2,-1)*ws( 0,1) + &
   sBz(+1,+2,-1)*ws( 1,1) + &
   sBz(+2,+2,-1)*ws( 2,1))*ws( 2,2))*w(-1,3) + &
 ((sBz(-2,-2, 0)*ws(-2,1) + &
   sBz(-1,-2, 0)*ws(-1,1) + &
   sBz( 0,-2, 0)*ws( 0,1) + &
   sBz(+1,-2, 0)*ws( 1,1) + &
   sBz(+2,-2, 0)*ws( 2,1))*ws(-2,2) + &
  (sBz(-2,-1, 0)*ws(-2,1) + &
   sBz(-1,-1, 0)*ws(-1,1) + &
   sBz( 0,-1, 0)*ws( 0,1) + &
   sBz(+1,-1, 0)*ws( 1,1) + &
   sBz(+2,-1, 0)*ws( 2,1))*ws(-1,2) + &
  (sBz(-2, 0, 0)*ws(-2,1) + &
   sBz(-1, 0, 0)*ws(-1,1) + &
   sBz( 0, 0, 0)*ws( 0,1) + &
   sBz(+1, 0, 0)*ws( 1,1) + &
   sBz(+2, 0, 0)*ws( 2,1))*ws( 0,2) + &
  (sBz(-2,+1, 0)*ws(-2,1) + &
   sBz(-1,+1, 0)*ws(-1,1) + &
   sBz( 0,+1, 0)*ws( 0,1) + &
   sBz(+1,+1, 0)*ws( 1,1) + &
   sBz(+2,+1, 0)*ws( 2,1))*ws( 1,2) + &
  (sBz(-2,+2, 0)*ws(-2,1) + &
   sBz(-1,+2, 0)*ws(-1,1) + &
   sBz( 0,+2, 0)*ws( 0,1) + &
   sBz(+1,+2, 0)*ws( 1,1) + &
   sBz(+2,+2, 0)*ws( 2,1))*ws( 2,2))*w( 0,3) + &
 ((sBz(-2,-2,+1)*ws(-2,1) + &
   sBz(-1,-2,+1)*ws(-1,1) + &
   sBz( 0,-2,+1)*ws( 0,1) + &
   sBz(+1,-2,+1)*ws( 1,1) + &
   sBz(+2,-2,+1)*ws( 2,1))*ws(-2,2) + &
  (sBz(-2,-1,+1)*ws(-2,1) + &
   sBz(-1,-1,+1)*ws(-1,1) + &
   sBz( 0,-1,+1)*ws( 0,1) + &
   sBz(+1,-1,+1)*ws( 1,1) + &
   sBz(+2,-1,+1)*ws( 2,1))*ws(-1,2) + &
  (sBz(-2, 0,+1)*ws(-2,1) + &
   sBz(-1, 0,+1)*ws(-1,1) + &
   sBz( 0, 0,+1)*ws( 0,1) + &
   sBz(+1, 0,+1)*ws( 1,1) + &
   sBz(+2, 0,+1)*ws( 2,1))*ws( 0,2) + &
  (sBz(-2,+1,+1)*ws(-2,1) + &
   sBz(-1,+1,+1)*ws(-1,1) + &
   sBz( 0,+1,+1)*ws( 0,1) + &
   sBz(+1,+1,+1)*ws( 1,1) + &
   sBz(+2,+1,+1)*ws( 2,1))*ws( 1,2) + &
  (sBz(-2,+2,+1)*ws(-2,1) + &
   sBz(-1,+2,+1)*ws(-1,1) + &
   sBz( 0,+2,+1)*ws( 0,1) + &
   sBz(+1,+2,+1)*ws( 1,1) + &
   sBz(+2,+2,+1)*ws( 2,1))*ws( 2,2))*w( 1,3) + &
 ((sBz(-2,-2,+2)*ws(-2,1) + &
   sBz(-1,-2,+2)*ws(-1,1) + &
   sBz( 0,-2,+2)*ws( 0,1) + &
   sBz(+1,-2,+2)*ws( 1,1) + &
   sBz(+2,-2,+2)*ws( 2,1))*ws(-2,2) + &
  (sBz(-2,-1,+2)*ws(-2,1) + &
   sBz(-1,-1,+2)*ws(-1,1) + &
   sBz( 0,-1,+2)*ws( 0,1) + &
   sBz(+1,-1,+2)*ws( 1,1) + &
   sBz(+2,-1,+2)*ws( 2,1))*ws(-1,2) + &
  (sBz(-2, 0,+2)*ws(-2,1) + &
   sBz(-1, 0,+2)*ws(-1,1) + &
   sBz( 0, 0,+2)*ws( 0,1) + &
   sBz(+1, 0,+2)*ws( 1,1) + &
   sBz(+2, 0,+2)*ws( 2,1))*ws( 0,2) + &
  (sBz(-2,+1,+2)*ws(-2,1) + &
   sBz(-1,+1,+2)*ws(-1,1) + &
   sBz( 0,+1,+2)*ws( 0,1) + &
   sBz(+1,+1,+2)*ws( 1,1) + &
   sBz(+2,+1,+2)*ws( 2,1))*ws( 1,2) + &
  (sBz(-2,+2,+2)*ws(-2,1) + &
   sBz(-1,+2,+2)*ws(-1,1) + &
   sBz( 0,+2,+2)*ws( 0,1) + &
   sBz(+1,+2,+2)*ws( 1,1) + &
   sBz(+2,+2,+2)*ws( 2,1))*ws( 2,2))*w( 2,3)
END SUBROUTINE interpolate_fields
!-----------------------------------------------------------------------
END MODULE interpolation_cached
!=======================================================================
! Interpolation/interpolation_cubic.f90 $Id$
!=======================================================================
MODULE interpolation
  implicit none
  ! Shorthand variables that doesn't change in the
  ! big particle loop
 
  ! Scratch variables needed for interpolation
  real    :: wxm,wx0,wx1,wx2,wym,wy0,wy1,wy2,wzm,wz0,wz1,wz2                    ! centered weights
  real    :: dxm,dx0,dx1,dx2,dym,dy0,dy1,dy2,dzm,dz0,dz1,dz2                    ! staggered weights
  integer :: ixc,iyc,izc,ixs,iys,izs                                            ! indices for centred and staggered weights
  integer, parameter :: sdb=-2, slb = -1, sub = 2                               ! parameters for stencil for local patch
!=======================================================================
END MODULE Interpolation

!=======================================================================
! The boundaries zones needed by this method
!=======================================================================
SUBROUTINE interpolation_boundaries
  USE params, only: mid
  USE grid_m, only: g
  implicit none
  character(len=mid), save:: id = &
    'Interpolation/interpolation_cubic.f90 $Id$'
!.......................................................................
  call print_id (id)
  g%lb = 3
  g%ub = 1
END SUBROUTINE

!=======================================================================
! Compute interpolation weights, given integer and fractional coordinates
!=======================================================================
SUBROUTINE makeweights (r, q)
  USE params,  only: mdim, stdout, stdall, master, mpi, periodic, &
                     do_check_interpolate
  USE grid_m,  only: g
  USE species, only: particle
  USE debug,   only: debugit, dbg_init, maxprint
  USE interpolation
  implicit none
  real, intent(in)    :: r(mdim)
  integer, intent(in) :: q(mdim)
  logical             :: err
  real                :: s, sm1, sm2
  real, parameter     :: one_sixth=1./6., two_third=2./3.
  logical             :: up
  integer, save       :: nprint = 0
  integer             :: lb, ub
!.......................................................................

  !-------------
  ! X COORDINATE CENTERED
  ixc = q(1); s = r(1); sm1 = 1. - s; sm2 = 2. - s 
  wxm = one_sixth * sm1*sm1*sm1
  wx0 = two_third - 0.5*s*s*sm2
  wx1 = two_third - 0.5*sm1*sm1*(1.+ s) 
  wx2 = 1. - (wxm + wx0 + wx1)

  !-------------
  ! Y COORDINATE CENTERED
  iyc = q(2); s = r(2); sm1 = 1. - s; sm2 = 2. - s 
  wym = one_sixth * sm1*sm1*sm1
  wy0 = two_third - 0.5*s*s*sm2
  wy1 = two_third - 0.5*sm1*sm1*(1.+ s) 
  wy2 = 1. - (wym + wy0 + wy1)

  !-------------
  ! Z COORDINATE CENTERED
  izc = q(3); s = r(3); sm1 = 1. - s; sm2 = 2. - s 
  wzm = one_sixth * sm1*sm1*sm1
  wz0 = two_third - 0.5*s*s*sm2
  wz1 = two_third - 0.5*sm1*sm1*(1.+ s) 
  wz2 = 1. - (wzm + wz0 + wz1)

  !-------------
  ! X COORDINATE SHIFTED IN X
  ixs = floor(r(1)+0.5); 
  s = r(1)+0.5-ixs; ixs = q(1)+ixs-1; sm1 = 1. - s; sm2 = 2. - s
  dxm = one_sixth * sm1*sm1*sm1
  dx0 = two_third - 0.5*s*s*sm2
  dx1 = two_third - 0.5*sm1*sm1*(1.+ s) 
  dx2 = 1. - (dxm + dx0 + dx1)

  !-------------
  ! Y COORDINATE SHIFTED IN Y
  iys = floor(r(2)+0.5); 
  s = r(2)+0.5-iys; iys = q(2)+iys-1; sm1 = 1. - s; sm2 = 2. - s
  dym = one_sixth * sm1*sm1*sm1
  dy0 = two_third - 0.5*s*s*sm2
  dy1 = two_third - 0.5*sm1*sm1*(1.+ s) 
  dy2 = 1. - (dym + dy0 + dy1)

  !-------------
  ! Z COORDINATE SHIFTED IN Z
  izs = floor(r(3)+0.5); 
  s = r(3)+0.5-izs; izs = q(3)+izs-1; sm1 = 1. - s; sm2 = 2. - s
  dzm = one_sixth * sm1*sm1*sm1
  dz0 = two_third - 0.5*s*s*sm2
  dz1 = two_third - 0.5*sm1*sm1*(1.+ s) 
  dz2 = 1. - (dzm + dz0 + dz1)

  if (master .and. nprint<maxprint .and. debugit(dbg_init,1)) then
    nprint = nprint+1
    print*,'makeweights: the first particle considered has'
    print 1,'                  g%lb:', g%lb
    print 1,'   integer coordinates:', q - g%lb
    print 1,'         ixc, iyc, izc:', ixc, iyc, izc
    print 1,'         ixs, iys, izs:', ixs, iys, izs
    print 2,'fractional coordinates:', r
    print 2,'         dx1, dy1, dz1:', dx1, dy1, dz1
  endif
1 format(1x,a,3i8)
2 format(1x,a,3f8.3)

  err = .false.
  if (do_check_interpolate) then
    if ( any((/ixc,iyc,izc,ixs,iys,izs/) < 2) .or. &
         any((/ixc,iyc,izc/) > g%n-2)         .or. &
         any((/ixs,iys,izs/) > g%n-2) ) then
      err = .true.

      write(stdout,*) ' '
      write(stdout,*) 'mpi%me(3)        : ',mpi%me(3)
      write(stdout,*) 'g%z(g%lb(3)      : ',g%z(g%lb(3))
      write(stdout,*) '(1.50001*g%ds(3)): ',(1.50001*g%ds(3))
      write(stdout,*) ' '

      write(stdall,*) 'makeweights: out of bounds result, thread', mpi%me(3)
      write(stdall,*) 'lb             =', g%lb
      write(stdall,*) 'ixc, iyc, izc  =', ixc,iyc,izc
      write(stdall,*) 'ub             =', g%ub
      write(stdall,*) 'ixs, iys, izs  =', ixs, iys, izs
      write(stdall,*) 'g%n            =', g%n
      write(stdall,'(a,3f16.10)') 'g%grlb         =', g%grlb
      write(stdall,'(a,3f16.10)') 'g%rlb          =', g%rlb
      write(stdall,'(a,3f16.10)') 'r              =', r
      write(stdall,'(a,3i16   )') 'q              =', q
      write(stdall,'(a,3f16.10)') 'g%grub         =', g%grub
      write(stdall,'(a,3f16.10)') 'g%rub          =', g%rub
      write(stdall,'(a,3f16.10)') 'g%r(ub)-pa%r   =', g%rub-r
      write(stdall,*) 's  =', g%s
      call warning('interpolation','index out of bounds')
    endif
  endif

  ! make sure that we dont go out-of-bounds. This can happen in approx 1 out of 10^7 cases
  lb = 2; ub = g%n(1)-2
  ixc = min(max(ixc,lb),ub)
  ixs = min(max(ixs,lb),ub)
  ub = g%n(2)-2
  iyc = min(max(iyc,lb),ub)
  iys = min(max(iys,lb),ub)
  ub = g%n(3)-2
  izc = min(max(izc,lb),ub)
  izs = min(max(izs,lb),ub)

END SUBROUTINE

!=======================================================================
SUBROUTINE scatter_fields (r,q,fx,fy,fz,fp1,fyz,fzx,fxy,fp2)
  USE params, only: mcoord, mdim, master
  USE grid_m, only: nx, ny, nz
  USE debug,  only: debugit, dbg_init, maxprint
  USE interpolation
  implicit none
  real,    intent(in) :: r(mdim)
  integer, intent(in) :: q(mdim)
  real, dimension(nx,ny,nz), intent(in)  :: fx,fy,fz,fxy,fyz,fzx        ! input fields
  real, dimension(mcoord)                :: fp1,fp2                     ! output vectors
  integer, save:: nprint = 0
!.......................................................................
  call makeweights (r, q)
  !-----------------------------------
  ! X SHIFTED WEIGHTS
  fp1(1) = &
       ((fx(ixs-1,iyc-1,izc-1)*dxm + &
         fx(ixs  ,iyc-1,izc-1)*dx0 + &
         fx(ixs+1,iyc-1,izc-1)*dx1 + &
         fx(ixs+2,iyc-1,izc-1)*dx2)*wym + &
        (fx(ixs-1,iyc  ,izc-1)*dxm + &
         fx(ixs  ,iyc  ,izc-1)*dx0 + &
         fx(ixs+1,iyc  ,izc-1)*dx1 + &
         fx(ixs+2,iyc  ,izc-1)*dx2)*wy0 + &
        (fx(ixs-1,iyc+1,izc-1)*dxm + &
         fx(ixs  ,iyc+1,izc-1)*dx0 + &
         fx(ixs+1,iyc+1,izc-1)*dx1 + &
         fx(ixs+2,iyc+1,izc-1)*dx2)*wy1 + &
        (fx(ixs-1,iyc+2,izc-1)*dxm + &
         fx(ixs  ,iyc+2,izc-1)*dx0 + &
         fx(ixs+1,iyc+2,izc-1)*dx1 + &
         fx(ixs+2,iyc+2,izc-1)*dx2)*wy2)*wzm + &
       ((fx(ixs-1,iyc-1,izc  )*dxm + &
         fx(ixs  ,iyc-1,izc  )*dx0 + &
         fx(ixs+1,iyc-1,izc  )*dx1 + &
         fx(ixs+2,iyc-1,izc  )*dx2)*wym + &
        (fx(ixs-1,iyc  ,izc  )*dxm + &
         fx(ixs  ,iyc  ,izc  )*dx0 + &
         fx(ixs+1,iyc  ,izc  )*dx1 + &
         fx(ixs+2,iyc  ,izc  )*dx2)*wy0 + &
        (fx(ixs-1,iyc+1,izc  )*dxm + &
         fx(ixs  ,iyc+1,izc  )*dx0 + &
         fx(ixs+1,iyc+1,izc  )*dx1 + &
         fx(ixs+2,iyc+1,izc  )*dx2)*wy1 + &
        (fx(ixs-1,iyc+2,izc  )*dxm + &
         fx(ixs  ,iyc+2,izc  )*dx0 + &
         fx(ixs+1,iyc+2,izc  )*dx1 + &
         fx(ixs+2,iyc+2,izc  )*dx2)*wy2)*wz0 + &
       ((fx(ixs-1,iyc-1,izc+1)*dxm + &
         fx(ixs  ,iyc-1,izc+1)*dx0 + &
         fx(ixs+1,iyc-1,izc+1)*dx1 + &
         fx(ixs+2,iyc-1,izc+1)*dx2)*wym + &
        (fx(ixs-1,iyc  ,izc+1)*dxm + &
         fx(ixs  ,iyc  ,izc+1)*dx0 + &
         fx(ixs+1,iyc  ,izc+1)*dx1 + &
         fx(ixs+2,iyc  ,izc+1)*dx2)*wy0 + &
        (fx(ixs-1,iyc+1,izc+1)*dxm + &
         fx(ixs  ,iyc+1,izc+1)*dx0 + &
         fx(ixs+1,iyc+1,izc+1)*dx1 + &
         fx(ixs+2,iyc+1,izc+1)*dx2)*wy1 + &
        (fx(ixs-1,iyc+2,izc+1)*dxm + &
         fx(ixs  ,iyc+2,izc+1)*dx0 + &
         fx(ixs+1,iyc+2,izc+1)*dx1 + &
         fx(ixs+2,iyc+2,izc+1)*dx2)*wy2)*wz1 + &
       ((fx(ixs-1,iyc-1,izc+2)*dxm + &
         fx(ixs  ,iyc-1,izc+2)*dx0 + &
         fx(ixs+1,iyc-1,izc+2)*dx1 + &
         fx(ixs+2,iyc-1,izc+2)*dx2)*wym + &
        (fx(ixs-1,iyc  ,izc+2)*dxm + &
         fx(ixs  ,iyc  ,izc+2)*dx0 + &
         fx(ixs+1,iyc  ,izc+2)*dx1 + &
         fx(ixs+2,iyc  ,izc+2)*dx2)*wy0 + &
        (fx(ixs-1,iyc+1,izc+2)*dxm + &
         fx(ixs  ,iyc+1,izc+2)*dx0 + &
         fx(ixs+1,iyc+1,izc+2)*dx1 + &
         fx(ixs+2,iyc+1,izc+2)*dx2)*wy1 + &
        (fx(ixs-1,iyc+2,izc+2)*dxm + &
         fx(ixs  ,iyc+2,izc+2)*dx0 + &
         fx(ixs+1,iyc+2,izc+2)*dx1 + &
         fx(ixs+2,iyc+2,izc+2)*dx2)*wy2)*wz2

  !-----------------------------------
  ! Y SHIFTED WEIGHTS
  fp1(2) = &
       ((fy(ixc-1,iys-1,izc-1)*wxm + &
         fy(ixc  ,iys-1,izc-1)*wx0 + &
         fy(ixc+1,iys-1,izc-1)*wx1 + &
         fy(ixc+2,iys-1,izc-1)*wx2)*dym + &
        (fy(ixc-1,iys  ,izc-1)*wxm + &
         fy(ixc  ,iys  ,izc-1)*wx0 + &
         fy(ixc+1,iys  ,izc-1)*wx1 + &
         fy(ixc+2,iys  ,izc-1)*wx2)*dy0 + &
        (fy(ixc-1,iys+1,izc-1)*wxm + &
         fy(ixc  ,iys+1,izc-1)*wx0 + &
         fy(ixc+1,iys+1,izc-1)*wx1 + &
         fy(ixc+2,iys+1,izc-1)*wx2)*dy1 + &
        (fy(ixc-1,iys+2,izc-1)*wxm + &
         fy(ixc  ,iys+2,izc-1)*wx0 + &
         fy(ixc+1,iys+2,izc-1)*wx1 + &
         fy(ixc+2,iys+2,izc-1)*wx2)*dy2)*wzm + &
       ((fy(ixc-1,iys-1,izc  )*wxm + &
         fy(ixc  ,iys-1,izc  )*wx0 + &
         fy(ixc+1,iys-1,izc  )*wx1 + &
         fy(ixc+2,iys-1,izc  )*wx2)*dym + &
        (fy(ixc-1,iys  ,izc  )*wxm + &
         fy(ixc  ,iys  ,izc  )*wx0 + &
         fy(ixc+1,iys  ,izc  )*wx1 + &
         fy(ixc+2,iys  ,izc  )*wx2)*dy0 + &
        (fy(ixc-1,iys+1,izc  )*wxm + &
         fy(ixc  ,iys+1,izc  )*wx0 + &
         fy(ixc+1,iys+1,izc  )*wx1 + &
         fy(ixc+2,iys+1,izc  )*wx2)*dy1 + &
        (fy(ixc-1,iys+2,izc  )*wxm + &
         fy(ixc  ,iys+2,izc  )*wx0 + &
         fy(ixc+1,iys+2,izc  )*wx1 + &
         fy(ixc+2,iys+2,izc  )*wx2)*dy2)*wz0 + &
       ((fy(ixc-1,iys-1,izc+1)*wxm + &
         fy(ixc  ,iys-1,izc+1)*wx0 + &
         fy(ixc+1,iys-1,izc+1)*wx1 + &
         fy(ixc+2,iys-1,izc+1)*wx2)*dym + &
        (fy(ixc-1,iys  ,izc+1)*wxm + &
         fy(ixc  ,iys  ,izc+1)*wx0 + &
         fy(ixc+1,iys  ,izc+1)*wx1 + &
         fy(ixc+2,iys  ,izc+1)*wx2)*dy0 + &
        (fy(ixc-1,iys+1,izc+1)*wxm + &
         fy(ixc  ,iys+1,izc+1)*wx0 + &
         fy(ixc+1,iys+1,izc+1)*wx1 + &
         fy(ixc+2,iys+1,izc+1)*wx2)*dy1 + &
        (fy(ixc-1,iys+2,izc+1)*wxm + &
         fy(ixc  ,iys+2,izc+1)*wx0 + &
         fy(ixc+1,iys+2,izc+1)*wx1 + &
         fy(ixc+2,iys+2,izc+1)*wx2)*dy2)*wz1 + &
       ((fy(ixc-1,iys-1,izc+2)*wxm + &
         fy(ixc  ,iys-1,izc+2)*wx0 + &
         fy(ixc+1,iys-1,izc+2)*wx1 + &
         fy(ixc+2,iys-1,izc+2)*wx2)*dym + &
        (fy(ixc-1,iys  ,izc+2)*wxm + &
         fy(ixc  ,iys  ,izc+2)*wx0 + &
         fy(ixc+1,iys  ,izc+2)*wx1 + &
         fy(ixc+2,iys  ,izc+2)*wx2)*dy0 + &
        (fy(ixc-1,iys+1,izc+2)*wxm + &
         fy(ixc  ,iys+1,izc+2)*wx0 + &
         fy(ixc+1,iys+1,izc+2)*wx1 + &
         fy(ixc+2,iys+1,izc+2)*wx2)*dy1 + &
        (fy(ixc-1,iys+2,izc+2)*wxm + &
         fy(ixc  ,iys+2,izc+2)*wx0 + &
         fy(ixc+1,iys+2,izc+2)*wx1 + &
         fy(ixc+2,iys+2,izc+2)*wx2)*dy2)*wz2

  !-----------------------------------
  ! Z SHIFTED WEIGHTS
  fp1(3) = &
       ((fz(ixc-1,iyc-1,izs-1)*wxm + &
         fz(ixc  ,iyc-1,izs-1)*wx0 + &
         fz(ixc+1,iyc-1,izs-1)*wx1 + &
         fz(ixc+2,iyc-1,izs-1)*wx2)*wym + &
        (fz(ixc-1,iyc  ,izs-1)*wxm + &
         fz(ixc  ,iyc  ,izs-1)*wx0 + &
         fz(ixc+1,iyc  ,izs-1)*wx1 + &
         fz(ixc+2,iyc  ,izs-1)*wx2)*wy0 + &
        (fz(ixc-1,iyc+1,izs-1)*wxm + &
         fz(ixc  ,iyc+1,izs-1)*wx0 + &
         fz(ixc+1,iyc+1,izs-1)*wx1 + &
         fz(ixc+2,iyc+1,izs-1)*wx2)*wy1 + &
        (fz(ixc-1,iyc+2,izs-1)*wxm + &
         fz(ixc  ,iyc+2,izs-1)*wx0 + &
         fz(ixc+1,iyc+2,izs-1)*wx1 + &
         fz(ixc+2,iyc+2,izs-1)*wx2)*wy2)*dzm + &
       ((fz(ixc-1,iyc-1,izs  )*wxm + &
         fz(ixc  ,iyc-1,izs  )*wx0 + &
         fz(ixc+1,iyc-1,izs  )*wx1 + &
         fz(ixc+2,iyc-1,izs  )*wx2)*wym + &
        (fz(ixc-1,iyc  ,izs  )*wxm + &
         fz(ixc  ,iyc  ,izs  )*wx0 + &
         fz(ixc+1,iyc  ,izs  )*wx1 + &
         fz(ixc+2,iyc  ,izs  )*wx2)*wy0 + &
        (fz(ixc-1,iyc+1,izs  )*wxm + &
         fz(ixc  ,iyc+1,izs  )*wx0 + &
         fz(ixc+1,iyc+1,izs  )*wx1 + &
         fz(ixc+2,iyc+1,izs  )*wx2)*wy1 + &
        (fz(ixc-1,iyc+2,izs  )*wxm + &
         fz(ixc  ,iyc+2,izs  )*wx0 + &
         fz(ixc+1,iyc+2,izs  )*wx1 + &
         fz(ixc+2,iyc+2,izs  )*wx2)*wy2)*dz0 + &
       ((fz(ixc-1,iyc-1,izs+1)*wxm + &
         fz(ixc  ,iyc-1,izs+1)*wx0 + &
         fz(ixc+1,iyc-1,izs+1)*wx1 + &
         fz(ixc+2,iyc-1,izs+1)*wx2)*wym + &
        (fz(ixc-1,iyc  ,izs+1)*wxm + &
         fz(ixc  ,iyc  ,izs+1)*wx0 + &
         fz(ixc+1,iyc  ,izs+1)*wx1 + &
         fz(ixc+2,iyc  ,izs+1)*wx2)*wy0 + &
        (fz(ixc-1,iyc+1,izs+1)*wxm + &
         fz(ixc  ,iyc+1,izs+1)*wx0 + &
         fz(ixc+1,iyc+1,izs+1)*wx1 + &
         fz(ixc+2,iyc+1,izs+1)*wx2)*wy1 + &
        (fz(ixc-1,iyc+2,izs+1)*wxm + &
         fz(ixc  ,iyc+2,izs+1)*wx0 + &
         fz(ixc+1,iyc+2,izs+1)*wx1 + &
         fz(ixc+2,iyc+2,izs+1)*wx2)*wy2)*dz1 + &
       ((fz(ixc-1,iyc-1,izs+2)*wxm + &
         fz(ixc  ,iyc-1,izs+2)*wx0 + &
         fz(ixc+1,iyc-1,izs+2)*wx1 + &
         fz(ixc+2,iyc-1,izs+2)*wx2)*wym + &
        (fz(ixc-1,iyc  ,izs+2)*wxm + &
         fz(ixc  ,iyc  ,izs+2)*wx0 + &
         fz(ixc+1,iyc  ,izs+2)*wx1 + &
         fz(ixc+2,iyc  ,izs+2)*wx2)*wy0 + &
        (fz(ixc-1,iyc+1,izs+2)*wxm + &
         fz(ixc  ,iyc+1,izs+2)*wx0 + &
         fz(ixc+1,iyc+1,izs+2)*wx1 + &
         fz(ixc+2,iyc+1,izs+2)*wx2)*wy1 + &
        (fz(ixc-1,iyc+2,izs+2)*wxm + &
         fz(ixc  ,iyc+2,izs+2)*wx0 + &
         fz(ixc+1,iyc+2,izs+2)*wx1 + &
         fz(ixc+2,iyc+2,izs+2)*wx2)*wy2)*dz2

  !-----------------------------------
  ! YZ SHIFTED WEIGHTS
  fp2(1) = &
       ((fyz(ixc-1,iys-1,izs-1)*wxm + &
         fyz(ixc  ,iys-1,izs-1)*wx0 + &
         fyz(ixc+1,iys-1,izs-1)*wx1 + &
         fyz(ixc+2,iys-1,izs-1)*wx2)*dym + &
        (fyz(ixc-1,iys  ,izs-1)*wxm + &
         fyz(ixc  ,iys  ,izs-1)*wx0 + &
         fyz(ixc+1,iys  ,izs-1)*wx1 + &
         fyz(ixc+2,iys  ,izs-1)*wx2)*dy0 + &
        (fyz(ixc-1,iys+1,izs-1)*wxm + &
         fyz(ixc  ,iys+1,izs-1)*wx0 + &
         fyz(ixc+1,iys+1,izs-1)*wx1 + &
         fyz(ixc+2,iys+1,izs-1)*wx2)*dy1 + &
        (fyz(ixc-1,iys+2,izs-1)*wxm + &
         fyz(ixc  ,iys+2,izs-1)*wx0 + &
         fyz(ixc+1,iys+2,izs-1)*wx1 + &
         fyz(ixc+2,iys+2,izs-1)*wx2)*dy2)*dzm + &
       ((fyz(ixc-1,iys-1,izs  )*wxm + &
         fyz(ixc  ,iys-1,izs  )*wx0 + &
         fyz(ixc+1,iys-1,izs  )*wx1 + &
         fyz(ixc+2,iys-1,izs  )*wx2)*dym + &
        (fyz(ixc-1,iys  ,izs  )*wxm + &
         fyz(ixc  ,iys  ,izs  )*wx0 + &
         fyz(ixc+1,iys  ,izs  )*wx1 + &
         fyz(ixc+2,iys  ,izs  )*wx2)*dy0 + &
        (fyz(ixc-1,iys+1,izs  )*wxm + &
         fyz(ixc  ,iys+1,izs  )*wx0 + &
         fyz(ixc+1,iys+1,izs  )*wx1 + &
         fyz(ixc+2,iys+1,izs  )*wx2)*dy1 + &
        (fyz(ixc-1,iys+2,izs  )*wxm + &
         fyz(ixc  ,iys+2,izs  )*wx0 + &
         fyz(ixc+1,iys+2,izs  )*wx1 + &
         fyz(ixc+2,iys+2,izs  )*wx2)*dy2)*dz0 + &
       ((fyz(ixc-1,iys-1,izs+1)*wxm + &
         fyz(ixc  ,iys-1,izs+1)*wx0 + &
         fyz(ixc+1,iys-1,izs+1)*wx1 + &
         fyz(ixc+2,iys-1,izs+1)*wx2)*dym + &
        (fyz(ixc-1,iys  ,izs+1)*wxm + &
         fyz(ixc  ,iys  ,izs+1)*wx0 + &
         fyz(ixc+1,iys  ,izs+1)*wx1 + &
         fyz(ixc+2,iys  ,izs+1)*wx2)*dy0 + &
        (fyz(ixc-1,iys+1,izs+1)*wxm + &
         fyz(ixc  ,iys+1,izs+1)*wx0 + &
         fyz(ixc+1,iys+1,izs+1)*wx1 + &
         fyz(ixc+2,iys+1,izs+1)*wx2)*dy1 + &
        (fyz(ixc-1,iys+2,izs+1)*wxm + &
         fyz(ixc  ,iys+2,izs+1)*wx0 + &
         fyz(ixc+1,iys+2,izs+1)*wx1 + &
         fyz(ixc+2,iys+2,izs+1)*wx2)*dy2)*dz1 + &
       ((fyz(ixc-1,iys-1,izs+2)*wxm + &
         fyz(ixc  ,iys-1,izs+2)*wx0 + &
         fyz(ixc+1,iys-1,izs+2)*wx1 + &
         fyz(ixc+2,iys-1,izs+2)*wx2)*dym + &
        (fyz(ixc-1,iys  ,izs+2)*wxm + &
         fyz(ixc  ,iys  ,izs+2)*wx0 + &
         fyz(ixc+1,iys  ,izs+2)*wx1 + &
         fyz(ixc+2,iys  ,izs+2)*wx2)*dy0 + &
        (fyz(ixc-1,iys+1,izs+2)*wxm + &
         fyz(ixc  ,iys+1,izs+2)*wx0 + &
         fyz(ixc+1,iys+1,izs+2)*wx1 + &
         fyz(ixc+2,iys+1,izs+2)*wx2)*dy1 + &
        (fyz(ixc-1,iys+2,izs+2)*wxm + &
         fyz(ixc  ,iys+2,izs+2)*wx0 + &
         fyz(ixc+1,iys+2,izs+2)*wx1 + &
         fyz(ixc+2,iys+2,izs+2)*wx2)*dy2)*dz2

  !-----------------------------------
  ! ZX SHIFTED WEIGHTS
  fp2(2) = &
       ((fzx(ixs-1,iyc-1,izs-1)*dxm + &
         fzx(ixs  ,iyc-1,izs-1)*dx0 + &
         fzx(ixs+1,iyc-1,izs-1)*dx1 + &
         fzx(ixs+2,iyc-1,izs-1)*dx2)*wym + &
        (fzx(ixs-1,iyc  ,izs-1)*dxm + &
         fzx(ixs  ,iyc  ,izs-1)*dx0 + &
         fzx(ixs+1,iyc  ,izs-1)*dx1 + &
         fzx(ixs+2,iyc  ,izs-1)*dx2)*wy0 + &
        (fzx(ixs-1,iyc+1,izs-1)*dxm + &
         fzx(ixs  ,iyc+1,izs-1)*dx0 + &
         fzx(ixs+1,iyc+1,izs-1)*dx1 + &
         fzx(ixs+2,iyc+1,izs-1)*dx2)*wy1 + &
        (fzx(ixs-1,iyc+2,izs-1)*dxm + &
         fzx(ixs  ,iyc+2,izs-1)*dx0 + &
         fzx(ixs+1,iyc+2,izs-1)*dx1 + &
         fzx(ixs+2,iyc+2,izs-1)*dx2)*wy2)*dzm + &
       ((fzx(ixs-1,iyc-1,izs  )*dxm + &
         fzx(ixs  ,iyc-1,izs  )*dx0 + &
         fzx(ixs+1,iyc-1,izs  )*dx1 + &
         fzx(ixs+2,iyc-1,izs  )*dx2)*wym + &
        (fzx(ixs-1,iyc  ,izs  )*dxm + &
         fzx(ixs  ,iyc  ,izs  )*dx0 + &
         fzx(ixs+1,iyc  ,izs  )*dx1 + &
         fzx(ixs+2,iyc  ,izs  )*dx2)*wy0 + &
        (fzx(ixs-1,iyc+1,izs  )*dxm + &
         fzx(ixs  ,iyc+1,izs  )*dx0 + &
         fzx(ixs+1,iyc+1,izs  )*dx1 + &
         fzx(ixs+2,iyc+1,izs  )*dx2)*wy1 + &
        (fzx(ixs-1,iyc+2,izs  )*dxm + &
         fzx(ixs  ,iyc+2,izs  )*dx0 + &
         fzx(ixs+1,iyc+2,izs  )*dx1 + &
         fzx(ixs+2,iyc+2,izs  )*dx2)*wy2)*dz0 + &
       ((fzx(ixs-1,iyc-1,izs+1)*dxm + &
         fzx(ixs  ,iyc-1,izs+1)*dx0 + &
         fzx(ixs+1,iyc-1,izs+1)*dx1 + &
         fzx(ixs+2,iyc-1,izs+1)*dx2)*wym + &
        (fzx(ixs-1,iyc  ,izs+1)*dxm + &
         fzx(ixs  ,iyc  ,izs+1)*dx0 + &
         fzx(ixs+1,iyc  ,izs+1)*dx1 + &
         fzx(ixs+2,iyc  ,izs+1)*dx2)*wy0 + &
        (fzx(ixs-1,iyc+1,izs+1)*dxm + &
         fzx(ixs  ,iyc+1,izs+1)*dx0 + &
         fzx(ixs+1,iyc+1,izs+1)*dx1 + &
         fzx(ixs+2,iyc+1,izs+1)*dx2)*wy1 + &
        (fzx(ixs-1,iyc+2,izs+1)*dxm + &
         fzx(ixs  ,iyc+2,izs+1)*dx0 + &
         fzx(ixs+1,iyc+2,izs+1)*dx1 + &
         fzx(ixs+2,iyc+2,izs+1)*dx2)*wy2)*dz1 + &
       ((fzx(ixs-1,iyc-1,izs+2)*dxm + &
         fzx(ixs  ,iyc-1,izs+2)*dx0 + &
         fzx(ixs+1,iyc-1,izs+2)*dx1 + &
         fzx(ixs+2,iyc-1,izs+2)*dx2)*wym + &
        (fzx(ixs-1,iyc  ,izs+2)*dxm + &
         fzx(ixs  ,iyc  ,izs+2)*dx0 + &
         fzx(ixs+1,iyc  ,izs+2)*dx1 + &
         fzx(ixs+2,iyc  ,izs+2)*dx2)*wy0 + &
        (fzx(ixs-1,iyc+1,izs+2)*dxm + &
         fzx(ixs  ,iyc+1,izs+2)*dx0 + &
         fzx(ixs+1,iyc+1,izs+2)*dx1 + &
         fzx(ixs+2,iyc+1,izs+2)*dx2)*wy1 + &
        (fzx(ixs-1,iyc+2,izs+2)*dxm + &
         fzx(ixs  ,iyc+2,izs+2)*dx0 + &
         fzx(ixs+1,iyc+2,izs+2)*dx1 + &
         fzx(ixs+2,iyc+2,izs+2)*dx2)*wy2)*dz2

  !-----------------------------------
  ! XY SHIFTED WEIGHTS
  fp2(3) = &
       ((fxy(ixs-1,iys-1,izc-1)*dxm + &
         fxy(ixs  ,iys-1,izc-1)*dx0 + &
         fxy(ixs+1,iys-1,izc-1)*dx1 + &
         fxy(ixs+2,iys-1,izc-1)*dx2)*dym + &
        (fxy(ixs-1,iys  ,izc-1)*dxm + &
         fxy(ixs  ,iys  ,izc-1)*dx0 + &
         fxy(ixs+1,iys  ,izc-1)*dx1 + &
         fxy(ixs+2,iys  ,izc-1)*dx2)*dy0 + &
        (fxy(ixs-1,iys+1,izc-1)*dxm + &
         fxy(ixs  ,iys+1,izc-1)*dx0 + &
         fxy(ixs+1,iys+1,izc-1)*dx1 + &
         fxy(ixs+2,iys+1,izc-1)*dx2)*dy1 + &
        (fxy(ixs-1,iys+2,izc-1)*dxm + &
         fxy(ixs  ,iys+2,izc-1)*dx0 + &
         fxy(ixs+1,iys+2,izc-1)*dx1 + &
         fxy(ixs+2,iys+2,izc-1)*dx2)*dy2)*wzm + &
       ((fxy(ixs-1,iys-1,izc  )*dxm + &
         fxy(ixs  ,iys-1,izc  )*dx0 + &
         fxy(ixs+1,iys-1,izc  )*dx1 + &
         fxy(ixs+2,iys-1,izc  )*dx2)*dym + &
        (fxy(ixs-1,iys  ,izc  )*dxm + &
         fxy(ixs  ,iys  ,izc  )*dx0 + &
         fxy(ixs+1,iys  ,izc  )*dx1 + &
         fxy(ixs+2,iys  ,izc  )*dx2)*dy0 + &
        (fxy(ixs-1,iys+1,izc  )*dxm + &
         fxy(ixs  ,iys+1,izc  )*dx0 + &
         fxy(ixs+1,iys+1,izc  )*dx1 + &
         fxy(ixs+2,iys+1,izc  )*dx2)*dy1 + &
        (fxy(ixs-1,iys+2,izc  )*dxm + &
         fxy(ixs  ,iys+2,izc  )*dx0 + &
         fxy(ixs+1,iys+2,izc  )*dx1 + &
         fxy(ixs+2,iys+2,izc  )*dx2)*dy2)*wz0 + &
       ((fxy(ixs-1,iys-1,izc+1)*dxm + &
         fxy(ixs  ,iys-1,izc+1)*dx0 + &
         fxy(ixs+1,iys-1,izc+1)*dx1 + &
         fxy(ixs+2,iys-1,izc+1)*dx2)*dym + &
        (fxy(ixs-1,iys  ,izc+1)*dxm + &
         fxy(ixs  ,iys  ,izc+1)*dx0 + &
         fxy(ixs+1,iys  ,izc+1)*dx1 + &
         fxy(ixs+2,iys  ,izc+1)*dx2)*dy0 + &
        (fxy(ixs-1,iys+1,izc+1)*dxm + &
         fxy(ixs  ,iys+1,izc+1)*dx0 + &
         fxy(ixs+1,iys+1,izc+1)*dx1 + &
         fxy(ixs+2,iys+1,izc+1)*dx2)*dy1 + &
        (fxy(ixs-1,iys+2,izc+1)*dxm + &
         fxy(ixs  ,iys+2,izc+1)*dx0 + &
         fxy(ixs+1,iys+2,izc+1)*dx1 + &
         fxy(ixs+2,iys+2,izc+1)*dx2)*dy2)*wz1 + &
       ((fxy(ixs-1,iys-1,izc+2)*dxm + &
         fxy(ixs  ,iys-1,izc+2)*dx0 + &
         fxy(ixs+1,iys-1,izc+2)*dx1 + &
         fxy(ixs+2,iys-1,izc+2)*dx2)*dym + &
        (fxy(ixs-1,iys  ,izc+2)*dxm + &
         fxy(ixs  ,iys  ,izc+2)*dx0 + &
         fxy(ixs+1,iys  ,izc+2)*dx1 + &
         fxy(ixs+2,iys  ,izc+2)*dx2)*dy0 + &
        (fxy(ixs-1,iys+1,izc+2)*dxm + &
         fxy(ixs  ,iys+1,izc+2)*dx0 + &
         fxy(ixs+1,iys+1,izc+2)*dx1 + &
         fxy(ixs+2,iys+1,izc+2)*dx2)*dy1 + &
        (fxy(ixs-1,iys+2,izc+2)*dxm + &
         fxy(ixs  ,iys+2,izc+2)*dx0 + &
         fxy(ixs+1,iys+2,izc+2)*dx1 + &
         fxy(ixs+2,iys+2,izc+2)*dx2)*dy2)*wz2
  if (master .and. nprint<maxprint .and. debugit(dbg_init,1)) then
    nprint = nprint+1
    print 1,'fp1(3) gets contributions from the range', izs-1, izs+2
    print 2,'contributing values:',fz (ixc,iyc,izs-1:izs+2)
    print 2,'             result:',fp1(3)
    print 1,'fp2(3) gets contributions from the range', izc-1, izc+2
    print 2,'contributing values:',fxy(ixs,iys,izc-1:izc+2)
    print 2,'             result:',fp2(3)
  1 format(1x,a,i2,' -',i2)
  2 format(1x,a,1p,4g12.3)
  end if
END SUBROUTINE

!=======================================================================
SUBROUTINE scatter_fields_v (np,pa,fx,fy,fz,fp1,fyz,fzx,fxy,fp2)
  USE params, only: mdim, mcoord, stdout, stdall, master, mpi, periodic, &
                    do_check_interpolate
  USE grid_m, only : g,nx,ny,nz
  USE species, only: particle
  USE debug,   only: debugit, dbg_init
  implicit none
!.......................................................................
  type(particle)       :: pa(np)
  integer              :: np
  real                 :: r(np,mdim)
  integer              :: q(np,mdim)
  real, dimension(nx,ny,nz), intent(in)  :: fx,fy,fz,fxy,fyz,fzx        ! input fields
  real, dimension(np,mcoord)              :: fp1,fp2                     ! output vectors
  real                 :: s, sm1, sm2
  real, parameter      :: one_sixth=1./6., two_third=2./3.
  integer              :: i, idim, lb, ub
  real   ,dimension(np):: wxm,wx0,wx1,wx2,wym,wy0,wy1,wy2,wzm,wz0,wz1,wz2 ! centered weights
  real   ,dimension(np):: dxm,dx0,dx1,dx2,dym,dy0,dy1,dy2,dzm,dz0,dz1,dz2 ! staggered weights
  integer,dimension(np):: kxc,kyc,kzc,kxs,kys,kzs                         ! indices for centred and staggered weights
  integer              :: ixc,iyc,izc,ixs,iys,izs                         ! indices for centred and staggered weights
  integer              :: jxc,jyc,jzc,jxs,jys,jzs                         ! indices for centred and staggered weights
  integer              :: jx ,jy ,jz                                      ! indices for centred and staggered weights
  integer              :: f(-1:2,-1:2,-1:2)
!.......................................................................

!-----------------------------------------------------------------------
! Pick up coordinates    
!-----------------------------------------------------------------------
  do i=1,np
    r(i,:) = pa(i)%r
    q(i,:) = pa(i)%q
  end do

!-----------------------------------------------------------------------
! PERIODIC WRAPPING MUST BE DONE SEPARATELY!    
!-----------------------------------------------------------------------
  do idim=1,mdim
    if (periodic(idim)) then
      do i=1,np    
        q(i,idim) = modulo(q(i,idim),g%gn(idim)) + g%lb(idim) - 1
      end do
    else
      do i=1,np   
        q(i,idim) = q(i,idim) + g%lb(idim) - 1
      end do
    end if
  end do

!-----------------------------------------------------------------------
! Weight calculations:  Each of the six centering groups requires about
! 17 flops times 6 = 100 flops per particle, vectorized
!-----------------------------------------------------------------------
  do i=1,np
    !-------------
    ! X COORDINATE CENTERED
    kxc(i) = q(i,1); s = r(i,1); sm1 = 1. - s; sm2 = 2. - s 
    wxm(i) = one_sixth * sm1*sm1*sm1
    wx0(i) = two_third - 0.5*s*s*sm2
    wx1(i) = two_third - 0.5*sm1*sm1*(1.+ s) 
    wx2(i) = 1. - (wxm(i) + wx0(i) + wx1(i))

    !-------------
    ! Y COORDINATE CENTERED
    kyc(i) = q(i,2); s = r(i,2); sm1 = 1. - s; sm2 = 2. - s 
    wym(i) = one_sixth * sm1*sm1*sm1
    wy0(i) = two_third - 0.5*s*s*sm2
    wy1(i) = two_third - 0.5*sm1*sm1*(1.+ s) 
    wy2(i) = 1. - (wym(i) + wy0(i) + wy1(i))

    !-------------
    ! Z COORDINATE CENTERED
    kzc(i) = q(i,3); s = r(i,3); sm1 = 1. - s; sm2 = 2. - s 
    wzm(i) = one_sixth * sm1*sm1*sm1
    wz0(i) = two_third - 0.5*s*s*sm2
    wz1(i) = two_third - 0.5*sm1*sm1*(1.+ s) 
    wz2(i) = 1. - (wzm(i) + wz0(i) + wz1(i))

    !-------------
    ! X COORDINATE SHIFTED IN X
    jxs = floor(r(i,1)+0.5); 
    s = r(i,1)+0.5-jxs; kxs(i) = q(i,1)+jxs-1; sm1 = 1. - s; sm2 = 2. - s
    dxm(i) = one_sixth * sm1*sm1*sm1
    dx0(i) = two_third - 0.5*s*s*sm2
    dx1(i) = two_third - 0.5*sm1*sm1*(1.+ s) 
    dx2(i) = 1. - (dxm(i) + dx0(i) + dx1(i))

    !-------------
    ! Y COORDINATE SHIFTED IN Y
    jys = floor(r(i,2)+0.5); 
    s = r(i,2)+0.5-jys; kys(i) = q(i,2)+jys-1; sm1 = 1. - s; sm2 = 2. - s
    dym(i) = one_sixth * sm1*sm1*sm1
    dy0(i) = two_third - 0.5*s*s*sm2
    dy1(i) = two_third - 0.5*sm1*sm1*(1.+ s) 
    dy2(i) = 1. - (dym(i) + dy0(i) + dy1(i))

    !-------------
    ! Z COORDINATE SHIFTED IN Z
    jzs = floor(r(i,3)+0.5); 
    s = r(i,3)+0.5-jzs; kzs(i) = q(i,3)+jzs-1; sm1 = 1. - s; sm2 = 2. - s
    dzm(i) = one_sixth * sm1*sm1*sm1
    dz0(i) = two_third - 0.5*s*s*sm2
    dz1(i) = two_third - 0.5*sm1*sm1*(1.+ s) 
    dz2(i) = 1. - (dzm(i) + dz0(i) + dz1(i))
  end do

!-----------------------------------------------------------------------
! Originally:
! Each of the six results required 64 multiply-add, plus some, for about
! 128*6 flops per particle = approx 800 non-vectorized flops.
!-----------------------------------------------------------------------

!-----------------------------------------------------------------------
! These types of snippets are now the only ones that do not vectorize!
!-----------------------------------------------------------------------
  do i=1,np
    ixs=kxs(i)                          
                iyc=kyc(i); izc=kzc(i)
    do jz=-1,2; do jy=-1,2; do jx=-1,2
      f(jx,jy,jz) = fx(ixs+jx,iyc+jy,izc+jz)
    end do; end do; end do
  end do

  do i=1,np
  !-----------------------------------
  ! X SHIFTED WEIGHTS
  fp1(i,1) = &
       ((f(-1,-1,-1)*dxm(i) + &
         f(+0,-1,-1)*dx0(i) + &
         f(+1,-1,-1)*dx1(i) + &
         f(+2,-1,-1)*dx2(i))*wym(i) + &
        (f(-1,+0,-1)*dxm(i) + &
         f(+0,+0,-1)*dx0(i) + &
         f(+1,+0,-1)*dx1(i) + &
         f(+2,+0,-1)*dx2(i))*wy0(i) + &
        (f(-1,+1,-1)*dxm(i) + &
         f(+0,+1,-1)*dx0(i) + &
         f(+1,+1,-1)*dx1(i) + &
         f(+2,+1,-1)*dx2(i))*wy1(i) + &
        (f(-1,+2,-1)*dxm(i) + &
         f(+0,+2,-1)*dx0(i) + &
         f(+1,+2,-1)*dx1(i) + &
         f(+2,+2,-1)*dx2(i))*wy2(i))*wzm(i) + &
       ((f(-1,-1, 0)*dxm(i) + &
         f(+0,-1, 0)*dx0(i) + &
         f(+1,-1, 0)*dx1(i) + &
         f(+2,-1, 0)*dx2(i))*wym(i) + &
        (f(-1,+0, 0)*dxm(i) + &
         f(+0,+0, 0)*dx0(i) + &
         f(+1,+0, 0)*dx1(i) + &
         f(+2,+0, 0)*dx2(i))*wy0(i) + &
        (f(-1,+1, 0)*dxm(i) + &
         f(+0,+1, 0)*dx0(i) + &
         f(+1,+1, 0)*dx1(i) + &
         f(+2,+1, 0)*dx2(i))*wy1(i) + &
        (f(-1,+2, 0)*dxm(i) + &
         f(+0,+2, 0)*dx0(i) + &
         f(+1,+2, 0)*dx1(i) + &
         f(+2,+2, 0)*dx2(i))*wy2(i))*wz0(i) + &
       ((f(-1,-1,+1)*dxm(i) + &
         f(+0,-1,+1)*dx0(i) + &
         f(+1,-1,+1)*dx1(i) + &
         f(+2,-1,+1)*dx2(i))*wym(i) + &
        (f(-1,+0,+1)*dxm(i) + &
         f(+0,+0,+1)*dx0(i) + &
         f(+1,+0,+1)*dx1(i) + &
         f(+2,+0,+1)*dx2(i))*wy0(i) + &
        (f(-1,+1,+1)*dxm(i) + &
         f(+0,+1,+1)*dx0(i) + &
         f(+1,+1,+1)*dx1(i) + &
         f(+2,+1,+1)*dx2(i))*wy1(i) + &
        (f(-1,+2,+1)*dxm(i) + &
         f(+0,+2,+1)*dx0(i) + &
         f(+1,+2,+1)*dx1(i) + &
         f(+2,+2,+1)*dx2(i))*wy2(i))*wz1(i) + &
       ((f(-1,-1,+2)*dxm(i) + &
         f(+0,-1,+2)*dx0(i) + &
         f(+1,-1,+2)*dx1(i) + &
         f(+2,-1,+2)*dx2(i))*wym(i) + &
        (f(-1,+0,+2)*dxm(i) + &
         f(+0,+0,+2)*dx0(i) + &
         f(+1,+0,+2)*dx1(i) + &
         f(+2,+0,+2)*dx2(i))*wy0(i) + &
        (f(-1,+1,+2)*dxm(i) + &
         f(+0,+1,+2)*dx0(i) + &
         f(+1,+1,+2)*dx1(i) + &
         f(+2,+1,+2)*dx2(i))*wy1(i) + &
        (f(-1,+2,+2)*dxm(i) + &
         f(+0,+2,+2)*dx0(i) + &
         f(+1,+2,+2)*dx1(i) + &
         f(+2,+2,+2)*dx2(i))*wy2(i))*wz2(i)
  end do

  !=======================================================================
  do i=1,np
                iys=kys(i)
    ixc=kxc(i);             izc=kzc(i)
    do jz=-1,2; do jy=-1,2; do jx=-1,2
      f(jx,jy,jz) = fy(ixc+jx,iys+jy,izc+jz)
    end do; end do; end do
  end do
  do i=1,np
  !-----------------------------------
  ! Y SHIFTED WEIGHTS
  fp1(i,2) = &
       ((f(-1,-1,-1)*wxm(i) + &
         f(+0,-1,-1)*wx0(i) + &
         f(+1,-1,-1)*wx1(i) + &
         f(+2,-1,-1)*wx2(i))*dym(i) + &
        (f(-1,+0,-1)*wxm(i) + &
         f(+0,+0,-1)*wx0(i) + &
         f(+1,+0,-1)*wx1(i) + &
         f(+2,+0,-1)*wx2(i))*dy0(i) + &
        (f(-1,+1,-1)*wxm(i) + &
         f(+0,+1,-1)*wx0(i) + &
         f(+1,+1,-1)*wx1(i) + &
         f(+2,+1,-1)*wx2(i))*dy1(i) + &
        (f(-1,+2,-1)*wxm(i) + &
         f(+0,+2,-1)*wx0(i) + &
         f(+1,+2,-1)*wx1(i) + &
         f(+2,+2,-1)*wx2(i))*dy2(i))*wzm(i) + &
       ((f(-1,-1, 0)*wxm(i) + &
         f(+0,-1, 0)*wx0(i) + &
         f(+1,-1, 0)*wx1(i) + &
         f(+2,-1, 0)*wx2(i))*dym(i) + &
        (f(-1,+0, 0)*wxm(i) + &
         f(+0,+0, 0)*wx0(i) + &
         f(+1,+0, 0)*wx1(i) + &
         f(+2,+0, 0)*wx2(i))*dy0(i) + &
        (f(-1,+1, 0)*wxm(i) + &
         f(+0,+1, 0)*wx0(i) + &
         f(+1,+1, 0)*wx1(i) + &
         f(+2,+1, 0)*wx2(i))*dy1(i) + &
        (f(-1,+2, 0)*wxm(i) + &
         f(+0,+2, 0)*wx0(i) + &
         f(+1,+2, 0)*wx1(i) + &
         f(+2,+2, 0)*wx2(i))*dy2(i))*wz0(i) + &
       ((f(-1,-1,+1)*wxm(i) + &
         f(+0,-1,+1)*wx0(i) + &
         f(+1,-1,+1)*wx1(i) + &
         f(+2,-1,+1)*wx2(i))*dym(i) + &
        (f(-1,+0,+1)*wxm(i) + &
         f(+0,+0,+1)*wx0(i) + &
         f(+1,+0,+1)*wx1(i) + &
         f(+2,+0,+1)*wx2(i))*dy0(i) + &
        (f(-1,+1,+1)*wxm(i) + &
         f(+0,+1,+1)*wx0(i) + &
         f(+1,+1,+1)*wx1(i) + &
         f(+2,+1,+1)*wx2(i))*dy1(i) + &
        (f(-1,+2,+1)*wxm(i) + &
         f(+0,+2,+1)*wx0(i) + &
         f(+1,+2,+1)*wx1(i) + &
         f(+2,+2,+1)*wx2(i))*dy2(i))*wz1(i) + &
       ((f(-1,-1,+2)*wxm(i) + &
         f(+0,-1,+2)*wx0(i) + &
         f(+1,-1,+2)*wx1(i) + &
         f(+2,-1,+2)*wx2(i))*dym(i) + &
        (f(-1,+0,+2)*wxm(i) + &
         f(+0,+0,+2)*wx0(i) + &
         f(+1,+0,+2)*wx1(i) + &
         f(+2,+0,+2)*wx2(i))*dy0(i) + &
        (f(-1,+1,+2)*wxm(i) + &
         f(+0,+1,+2)*wx0(i) + &
         f(+1,+1,+2)*wx1(i) + &
         f(+2,+1,+2)*wx2(i))*dy1(i) + &
        (f(-1,+2,+2)*wxm(i) + &
         f(+0,+2,+2)*wx0(i) + &
         f(+1,+2,+2)*wx1(i) + &
         f(+2,+2,+2)*wx2(i))*dy2(i))*wz2(i)
  end do
  !=======================================================================
  do i=1,np
                            izs=kzs(i)
    ixc=kxc(i); iyc=kyc(i)            
    do jz=-1,2; do jy=-1,2; do jx=-1,2
      f(jx,jy,jz) = fz(ixc+jx,iyc+jy,izs+jz)
    end do; end do; end do
  end do
  do i=1,np
  !-----------------------------------
  ! Z SHIFTED WEIGHTS
  fp1(i,3) = &
       ((f(-1,-1,-1)*wxm(i) + &
         f(+0,-1,-1)*wx0(i) + &
         f(+1,-1,-1)*wx1(i) + &
         f(+2,-1,-1)*wx2(i))*wym(i) + &
        (f(-1,+0,-1)*wxm(i) + &
         f(+0,+0,-1)*wx0(i) + &
         f(+1,+0,-1)*wx1(i) + &
         f(+2,+0,-1)*wx2(i))*wy0(i) + &
        (f(-1,+1,-1)*wxm(i) + &
         f(+0,+1,-1)*wx0(i) + &
         f(+1,+1,-1)*wx1(i) + &
         f(+2,+1,-1)*wx2(i))*wy1(i) + &
        (f(-1,+2,-1)*wxm(i) + &
         f(+0,+2,-1)*wx0(i) + &
         f(+1,+2,-1)*wx1(i) + &
         f(+2,+2,-1)*wx2(i))*wy2(i))*dzm(i) + &
       ((f(-1,-1, 0)*wxm(i) + &
         f(+0,-1, 0)*wx0(i) + &
         f(+1,-1, 0)*wx1(i) + &
         f(+2,-1, 0)*wx2(i))*wym(i) + &
        (f(-1,+0, 0)*wxm(i) + &
         f(+0,+0, 0)*wx0(i) + &
         f(+1,+0, 0)*wx1(i) + &
         f(+2,+0, 0)*wx2(i))*wy0(i) + &
        (f(-1,+1, 0)*wxm(i) + &
         f(+0,+1, 0)*wx0(i) + &
         f(+1,+1, 0)*wx1(i) + &
         f(+2,+1, 0)*wx2(i))*wy1(i) + &
        (f(-1,+2, 0)*wxm(i) + &
         f(+0,+2, 0)*wx0(i) + &
         f(+1,+2, 0)*wx1(i) + &
         f(+2,+2, 0)*wx2(i))*wy2(i))*dz0(i) + &
       ((f(-1,-1,+1)*wxm(i) + &
         f(+0,-1,+1)*wx0(i) + &
         f(+1,-1,+1)*wx1(i) + &
         f(+2,-1,+1)*wx2(i))*wym(i) + &
        (f(-1,+0,+1)*wxm(i) + &
         f(+0,+0,+1)*wx0(i) + &
         f(+1,+0,+1)*wx1(i) + &
         f(+2,+0,+1)*wx2(i))*wy0(i) + &
        (f(-1,+1,+1)*wxm(i) + &
         f(+0,+1,+1)*wx0(i) + &
         f(+1,+1,+1)*wx1(i) + &
         f(+2,+1,+1)*wx2(i))*wy1(i) + &
        (f(-1,+2,+1)*wxm(i) + &
         f(+0,+2,+1)*wx0(i) + &
         f(+1,+2,+1)*wx1(i) + &
         f(+2,+2,+1)*wx2(i))*wy2(i))*dz1(i) + &
       ((f(-1,-1,+2)*wxm(i) + &
         f(+0,-1,+2)*wx0(i) + &
         f(+1,-1,+2)*wx1(i) + &
         f(+2,-1,+2)*wx2(i))*wym(i) + &
        (f(-1,+0,+2)*wxm(i) + &
         f(+0,+0,+2)*wx0(i) + &
         f(+1,+0,+2)*wx1(i) + &
         f(+2,+0,+2)*wx2(i))*wy0(i) + &
        (f(-1,+1,+2)*wxm(i) + &
         f(+0,+1,+2)*wx0(i) + &
         f(+1,+1,+2)*wx1(i) + &
         f(+2,+1,+2)*wx2(i))*wy1(i) + &
        (f(-1,+2,+2)*wxm(i) + &
         f(+0,+2,+2)*wx0(i) + &
         f(+1,+2,+2)*wx1(i) + &
         f(+2,+2,+2)*wx2(i))*wy2(i))*dz2(i)
  end do
  !=======================================================================
  do i=1,np
                iys=kys(i); izs=kzs(i)
    ixc=kxc(i)                        
    do jz=-1,2; do jy=-1,2; do jx=-1,2
      f(jx,jy,jz) = fx(ixc+jx,iys+jy,izs+jz)
    end do; end do; end do
  end do
  do i=1,np
  !-----------------------------------
  ! YZ SHIFTED WEIGHTS
  fp2(i,1) = &
       ((f(-1,-1,-1)*wxm(i) + &
         f(+0,-1,-1)*wx0(i) + &
         f(+1,-1,-1)*wx1(i) + &
         f(+2,-1,-1)*wx2(i))*dym(i) + &
        (f(-1,+0,-1)*wxm(i) + &
         f(+0,+0,-1)*wx0(i) + &
         f(+1,+0,-1)*wx1(i) + &
         f(+2,+0,-1)*wx2(i))*dy0(i) + &
        (f(-1,+1,-1)*wxm(i) + &
         f(+0,+1,-1)*wx0(i) + &
         f(+1,+1,-1)*wx1(i) + &
         f(+2,+1,-1)*wx2(i))*dy1(i) + &
        (f(-1,+2,-1)*wxm(i) + &
         f(+0,+2,-1)*wx0(i) + &
         f(+1,+2,-1)*wx1(i) + &
         f(+2,+2,-1)*wx2(i))*dy2(i))*dzm(i) + &
       ((f(-1,-1, 0)*wxm(i) + &
         f(+0,-1, 0)*wx0(i) + &
         f(+1,-1, 0)*wx1(i) + &
         f(+2,-1, 0)*wx2(i))*dym(i) + &
        (f(-1,+0, 0)*wxm(i) + &
         f(+0,+0, 0)*wx0(i) + &
         f(+1,+0, 0)*wx1(i) + &
         f(+2,+0, 0)*wx2(i))*dy0(i) + &
        (f(-1,+1, 0)*wxm(i) + &
         f(+0,+1, 0)*wx0(i) + &
         f(+1,+1, 0)*wx1(i) + &
         f(+2,+1, 0)*wx2(i))*dy1(i) + &
        (f(-1,+2, 0)*wxm(i) + &
         f(+0,+2, 0)*wx0(i) + &
         f(+1,+2, 0)*wx1(i) + &
         f(+2,+2, 0)*wx2(i))*dy2(i))*dz0(i) + &
       ((f(-1,-1,+1)*wxm(i) + &
         f(+0,-1,+1)*wx0(i) + &
         f(+1,-1,+1)*wx1(i) + &
         f(+2,-1,+1)*wx2(i))*dym(i) + &
        (f(-1,+0,+1)*wxm(i) + &
         f(+0,+0,+1)*wx0(i) + &
         f(+1,+0,+1)*wx1(i) + &
         f(+2,+0,+1)*wx2(i))*dy0(i) + &
        (f(-1,+1,+1)*wxm(i) + &
         f(+0,+1,+1)*wx0(i) + &
         f(+1,+1,+1)*wx1(i) + &
         f(+2,+1,+1)*wx2(i))*dy1(i) + &
        (f(-1,+2,+1)*wxm(i) + &
         f(+0,+2,+1)*wx0(i) + &
         f(+1,+2,+1)*wx1(i) + &
         f(+2,+2,+1)*wx2(i))*dy2(i))*dz1(i) + &
       ((f(-1,-1,+2)*wxm(i) + &
         f(+0,-1,+2)*wx0(i) + &
         f(+1,-1,+2)*wx1(i) + &
         f(+2,-1,+2)*wx2(i))*dym(i) + &
        (f(-1,+0,+2)*wxm(i) + &
         f(+0,+0,+2)*wx0(i) + &
         f(+1,+0,+2)*wx1(i) + &
         f(+2,+0,+2)*wx2(i))*dy0(i) + &
        (f(-1,+1,+2)*wxm(i) + &
         f(+0,+1,+2)*wx0(i) + &
         f(+1,+1,+2)*wx1(i) + &
         f(+2,+1,+2)*wx2(i))*dy1(i) + &
        (f(-1,+2,+2)*wxm(i) + &
         f(+0,+2,+2)*wx0(i) + &
         f(+1,+2,+2)*wx1(i) + &
         f(+2,+2,+2)*wx2(i))*dy2(i))*dz2(i)
  end do
  !=======================================================================
  do i=1,np
    ixs=kxs(i);             izs=kzs(i)
                iyc=kyc(i)            
    do jz=-1,2; do jy=-1,2; do jx=-1,2
      f(jx,jy,jz) = fy(ixs+jx,iyc+jy,izs+jz)
    end do; end do; end do
  end do
  do i=1,np
  !-----------------------------------
  ! ZX SHED WETS
  fp2(i,2) = &
       ((f(-1,-1,-1)*dxm(i) + &
         f(+0,-1,-1)*dx0(i) + &
         f(+1,-1,-1)*dx1(i) + &
         f(+2,-1,-1)*dx2(i))*wym(i) + &
        (f(-1,+0,-1)*dxm(i) + &
         f(+0,+0,-1)*dx0(i) + &
         f(+1,+0,-1)*dx1(i) + &
         f(+2,+0,-1)*dx2(i))*wy0(i) + &
        (f(-1,+1,-1)*dxm(i) + &
         f(+0,+1,-1)*dx0(i) + &
         f(+1,+1,-1)*dx1(i) + &
         f(+2,+1,-1)*dx2(i))*wy1(i) + &
        (f(-1,+2,-1)*dxm(i) + &
         f(+0,+2,-1)*dx0(i) + &
         f(+1,+2,-1)*dx1(i) + &
         f(+2,+2,-1)*dx2(i))*wy2(i))*dzm(i) + &
       ((f(-1,-1, 0)*dxm(i) + &
         f(+0,-1, 0)*dx0(i) + &
         f(+1,-1, 0)*dx1(i) + &
         f(+2,-1, 0)*dx2(i))*wym(i) + &
        (f(-1,+0, 0)*dxm(i) + &
         f(+0,+0, 0)*dx0(i) + &
         f(+1,+0, 0)*dx1(i) + &
         f(+2,+0, 0)*dx2(i))*wy0(i) + &
        (f(-1,+1, 0)*dxm(i) + &
         f(+0,+1, 0)*dx0(i) + &
         f(+1,+1, 0)*dx1(i) + &
         f(+2,+1, 0)*dx2(i))*wy1(i) + &
        (f(-1,+2, 0)*dxm(i) + &
         f(+0,+2, 0)*dx0(i) + &
         f(+1,+2, 0)*dx1(i) + &
         f(+2,+2, 0)*dx2(i))*wy2(i))*dz0(i) + &
       ((f(-1,-1,+1)*dxm(i) + &
         f(+0,-1,+1)*dx0(i) + &
         f(+1,-1,+1)*dx1(i) + &
         f(+2,-1,+1)*dx2(i))*wym(i) + &
        (f(-1,+0,+1)*dxm(i) + &
         f(+0,+0,+1)*dx0(i) + &
         f(+1,+0,+1)*dx1(i) + &
         f(+2,+0,+1)*dx2(i))*wy0(i) + &
        (f(-1,+1,+1)*dxm(i) + &
         f(+0,+1,+1)*dx0(i) + &
         f(+1,+1,+1)*dx1(i) + &
         f(+2,+1,+1)*dx2(i))*wy1(i) + &
        (f(-1,+2,+1)*dxm(i) + &
         f(+0,+2,+1)*dx0(i) + &
         f(+1,+2,+1)*dx1(i) + &
         f(+2,+2,+1)*dx2(i))*wy2(i))*dz1(i) + &
       ((f(-1,-1,+2)*dxm(i) + &
         f(+0,-1,+2)*dx0(i) + &
         f(+1,-1,+2)*dx1(i) + &
         f(+2,-1,+2)*dx2(i))*wym(i) + &
        (f(-1,+0,+2)*dxm(i) + &
         f(+0,+0,+2)*dx0(i) + &
         f(+1,+0,+2)*dx1(i) + &
         f(+2,+0,+2)*dx2(i))*wy0(i) + &
        (f(-1,+1,+2)*dxm(i) + &
         f(+0,+1,+2)*dx0(i) + &
         f(+1,+1,+2)*dx1(i) + &
         f(+2,+1,+2)*dx2(i))*wy1(i) + &
        (f(-1,+2,+2)*dxm(i) + &
         f(+0,+2,+2)*dx0(i) + &
         f(+1,+2,+2)*dx1(i) + &
         f(+2,+2,+2)*dx2(i))*wy2(i))*dz2(i)
  end do
  !=======================================================================
  do i=1,np
    ixs=kxs(i); iys=kys(i)            
                            izc=kzc(i)
    do jz=-1,2; do jy=-1,2; do jx=-1,2
      f(jx,jy,jz) = fy(ixs+jx,iys+jy,izc+jz)
    end do; end do; end do
  end do
  do i=1,np
  !-----------------------------------
  ! XY SHED WETS
  fp2(i,3) = &
       ((f(-1,-1,-1)*dxm(i) + &
         f(+0,-1,-1)*dx0(i) + &
         f(+1,-1,-1)*dx1(i) + &
         f(+2,-1,-1)*dx2(i))*dym(i) + &
        (f(-1,+0,-1)*dxm(i) + &
         f(+0,+0,-1)*dx0(i) + &
         f(+1,+0,-1)*dx1(i) + &
         f(+2,+0,-1)*dx2(i))*dy0(i) + &
        (f(-1,+1,-1)*dxm(i) + &
         f(+0,+1,-1)*dx0(i) + &
         f(+1,+1,-1)*dx1(i) + &
         f(+2,+1,-1)*dx2(i))*dy1(i) + &
        (f(-1,+2,-1)*dxm(i) + &
         f(+0,+2,-1)*dx0(i) + &
         f(+1,+2,-1)*dx1(i) + &
         f(+2,+2,-1)*dx2(i))*dy2(i))*wzm(i) + &
       ((f(-1,-1, 0)*dxm(i) + &
         f(+0,-1, 0)*dx0(i) + &
         f(+1,-1, 0)*dx1(i) + &
         f(+2,-1, 0)*dx2(i))*dym(i) + &
        (f(-1,+0, 0)*dxm(i) + &
         f(+0,+0, 0)*dx0(i) + &
         f(+1,+0, 0)*dx1(i) + &
         f(+2,+0, 0)*dx2(i))*dy0(i) + &
        (f(-1,+1, 0)*dxm(i) + &
         f(+0,+1, 0)*dx0(i) + &
         f(+1,+1, 0)*dx1(i) + &
         f(+2,+1, 0)*dx2(i))*dy1(i) + &
        (f(-1,+2, 0)*dxm(i) + &
         f(+0,+2, 0)*dx0(i) + &
         f(+1,+2, 0)*dx1(i) + &
         f(+2,+2, 0)*dx2(i))*dy2(i))*wz0(i) + &
       ((f(-1,-1,+1)*dxm(i) + &
         f(+0,-1,+1)*dx0(i) + &
         f(+1,-1,+1)*dx1(i) + &
         f(+2,-1,+1)*dx2(i))*dym(i) + &
        (f(-1,+0,+1)*dxm(i) + &
         f(+0,+0,+1)*dx0(i) + &
         f(+1,+0,+1)*dx1(i) + &
         f(+2,+0,+1)*dx2(i))*dy0(i) + &
        (f(-1,+1,+1)*dxm(i) + &
         f(+0,+1,+1)*dx0(i) + &
         f(+1,+1,+1)*dx1(i) + &
         f(+2,+1,+1)*dx2(i))*dy1(i) + &
        (f(-1,+2,+1)*dxm(i) + &
         f(+0,+2,+1)*dx0(i) + &
         f(+1,+2,+1)*dx1(i) + &
         f(+2,+2,+1)*dx2(i))*dy2(i))*wz1(i) + &
       ((f(-1,-1,+2)*dxm(i) + &
         f(+0,-1,+2)*dx0(i) + &
         f(+1,-1,+2)*dx1(i) + &
         f(+2,-1,+2)*dx2(i))*dym(i) + &
        (f(-1,+0,+2)*dxm(i) + &
         f(+0,+0,+2)*dx0(i) + &
         f(+1,+0,+2)*dx1(i) + &
         f(+2,+0,+2)*dx2(i))*dy0(i) + &
        (f(-1,+1,+2)*dxm(i) + &
         f(+0,+1,+2)*dx0(i) + &
         f(+1,+1,+2)*dx1(i) + &
         f(+2,+1,+2)*dx2(i))*dy1(i) + &
        (f(-1,+2,+2)*dxm(i) + &
         f(+0,+2,+2)*dx0(i) + &
         f(+1,+2,+2)*dx1(i) + &
         f(+2,+2,+2)*dx2(i))*dy2(i))*wz2(i)
  end do
  do i=1,np
    q(i,:) = pa(i)%q
    if (any((.not. periodic) .and. &                              ! not periodic case!
            (q(i,:) < 0 .or. q(i,:) >= g%n))) then                      ! outside box
      fp1(i,:) = 0.                                                           
      fp2(i,:) = 0.                                                           
    endif
  end do
END SUBROUTINE

SUBROUTINE gather (r, q, f, fp, k, w)
  USE params,  only: mdim, mcoord, nspecies, do_vth, master
  USE species, only: field, particle
  USE grid_m,  only: g,nx,ny,nz
  USE debug,   only: debugit, dbg_init, maxprint
  USE interpolation
  implicit none
  real, dimension(mdim), intent(in)          :: r
  integer, dimension(mdim), intent(in)       :: q
  type(field), dimension(nx,ny,nz,nspecies)  :: f                       ! receiving fields
  real,        intent(in), dimension(mcoord) :: fp                      ! velocity
  integer,     intent(in)                    :: k                       ! species index
  real,        intent(in)                    :: w                       ! weight
  ! Local variables
  real           :: ww,www,fp2
  integer        :: jy, jz
  integer, save  :: nprint = 0
!.......................................................................
  call makeweights (r, q)

  !---------------------------------------------------------------------
  ! particle density
  www = w*wzm
  ww = wym*www
  f(ixc-1,iyc-1,izc-1,k)%d = f(ixc-1,iyc-1,izc-1,k)%d + wxm*ww
  f(ixc  ,iyc-1,izc-1,k)%d = f(ixc  ,iyc-1,izc-1,k)%d + wx0*ww
  f(ixc+1,iyc-1,izc-1,k)%d = f(ixc+1,iyc-1,izc-1,k)%d + wx1*ww
  f(ixc+2,iyc-1,izc-1,k)%d = f(ixc+2,iyc-1,izc-1,k)%d + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izc-1,k)%d = f(ixc-1,iyc  ,izc-1,k)%d + wxm*ww
  f(ixc  ,iyc  ,izc-1,k)%d = f(ixc  ,iyc  ,izc-1,k)%d + wx0*ww
  f(ixc+1,iyc  ,izc-1,k)%d = f(ixc+1,iyc  ,izc-1,k)%d + wx1*ww
  f(ixc+2,iyc  ,izc-1,k)%d = f(ixc+2,iyc  ,izc-1,k)%d + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izc-1,k)%d = f(ixc-1,iyc+1,izc-1,k)%d + wxm*ww
  f(ixc  ,iyc+1,izc-1,k)%d = f(ixc  ,iyc+1,izc-1,k)%d + wx0*ww
  f(ixc+1,iyc+1,izc-1,k)%d = f(ixc+1,iyc+1,izc-1,k)%d + wx1*ww
  f(ixc+2,iyc+1,izc-1,k)%d = f(ixc+2,iyc+1,izc-1,k)%d + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izc-1,k)%d = f(ixc-1,iyc+2,izc-1,k)%d + wxm*ww
  f(ixc  ,iyc+2,izc-1,k)%d = f(ixc  ,iyc+2,izc-1,k)%d + wx0*ww
  f(ixc+1,iyc+2,izc-1,k)%d = f(ixc+1,iyc+2,izc-1,k)%d + wx1*ww
  f(ixc+2,iyc+2,izc-1,k)%d = f(ixc+2,iyc+2,izc-1,k)%d + wx2*ww
  www = w*wz0
  ww = wym*www
  f(ixc-1,iyc-1,izc  ,k)%d = f(ixc-1,iyc-1,izc  ,k)%d + wxm*ww
  f(ixc  ,iyc-1,izc  ,k)%d = f(ixc  ,iyc-1,izc  ,k)%d + wx0*ww
  f(ixc+1,iyc-1,izc  ,k)%d = f(ixc+1,iyc-1,izc  ,k)%d + wx1*ww
  f(ixc+2,iyc-1,izc  ,k)%d = f(ixc+2,iyc-1,izc  ,k)%d + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izc  ,k)%d = f(ixc-1,iyc  ,izc  ,k)%d + wxm*ww
  f(ixc  ,iyc  ,izc  ,k)%d = f(ixc  ,iyc  ,izc  ,k)%d + wx0*ww
  f(ixc+1,iyc  ,izc  ,k)%d = f(ixc+1,iyc  ,izc  ,k)%d + wx1*ww
  f(ixc+2,iyc  ,izc  ,k)%d = f(ixc+2,iyc  ,izc  ,k)%d + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izc  ,k)%d = f(ixc-1,iyc+1,izc  ,k)%d + wxm*ww
  f(ixc  ,iyc+1,izc  ,k)%d = f(ixc  ,iyc+1,izc  ,k)%d + wx0*ww
  f(ixc+1,iyc+1,izc  ,k)%d = f(ixc+1,iyc+1,izc  ,k)%d + wx1*ww
  f(ixc+2,iyc+1,izc  ,k)%d = f(ixc+2,iyc+1,izc  ,k)%d + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izc  ,k)%d = f(ixc-1,iyc+2,izc  ,k)%d + wxm*ww
  f(ixc  ,iyc+2,izc  ,k)%d = f(ixc  ,iyc+2,izc  ,k)%d + wx0*ww
  f(ixc+1,iyc+2,izc  ,k)%d = f(ixc+1,iyc+2,izc  ,k)%d + wx1*ww
  f(ixc+2,iyc+2,izc  ,k)%d = f(ixc+2,iyc+2,izc  ,k)%d + wx2*ww
  www = w*wz1
  ww = wym*www
  f(ixc-1,iyc-1,izc+1,k)%d = f(ixc-1,iyc-1,izc+1,k)%d + wxm*ww
  f(ixc  ,iyc-1,izc+1,k)%d = f(ixc  ,iyc-1,izc+1,k)%d + wx0*ww
  f(ixc+1,iyc-1,izc+1,k)%d = f(ixc+1,iyc-1,izc+1,k)%d + wx1*ww
  f(ixc+2,iyc-1,izc+1,k)%d = f(ixc+2,iyc-1,izc+1,k)%d + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izc+1,k)%d = f(ixc-1,iyc  ,izc+1,k)%d + wxm*ww
  f(ixc  ,iyc  ,izc+1,k)%d = f(ixc  ,iyc  ,izc+1,k)%d + wx0*ww
  f(ixc+1,iyc  ,izc+1,k)%d = f(ixc+1,iyc  ,izc+1,k)%d + wx1*ww
  f(ixc+2,iyc  ,izc+1,k)%d = f(ixc+2,iyc  ,izc+1,k)%d + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izc+1,k)%d = f(ixc-1,iyc+1,izc+1,k)%d + wxm*ww
  f(ixc  ,iyc+1,izc+1,k)%d = f(ixc  ,iyc+1,izc+1,k)%d + wx0*ww
  f(ixc+1,iyc+1,izc+1,k)%d = f(ixc+1,iyc+1,izc+1,k)%d + wx1*ww
  f(ixc+2,iyc+1,izc+1,k)%d = f(ixc+2,iyc+1,izc+1,k)%d + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izc+1,k)%d = f(ixc-1,iyc+2,izc+1,k)%d + wxm*ww
  f(ixc  ,iyc+2,izc+1,k)%d = f(ixc  ,iyc+2,izc+1,k)%d + wx0*ww
  f(ixc+1,iyc+2,izc+1,k)%d = f(ixc+1,iyc+2,izc+1,k)%d + wx1*ww
  f(ixc+2,iyc+2,izc+1,k)%d = f(ixc+2,iyc+2,izc+1,k)%d + wx2*ww
  www = w*wz2
  ww = wym*www
  f(ixc-1,iyc-1,izc+2,k)%d = f(ixc-1,iyc-1,izc+2,k)%d + wxm*ww
  f(ixc  ,iyc-1,izc+2,k)%d = f(ixc  ,iyc-1,izc+2,k)%d + wx0*ww
  f(ixc+1,iyc-1,izc+2,k)%d = f(ixc+1,iyc-1,izc+2,k)%d + wx1*ww
  f(ixc+2,iyc-1,izc+2,k)%d = f(ixc+2,iyc-1,izc+2,k)%d + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izc+2,k)%d = f(ixc-1,iyc  ,izc+2,k)%d + wxm*ww
  f(ixc  ,iyc  ,izc+2,k)%d = f(ixc  ,iyc  ,izc+2,k)%d + wx0*ww
  f(ixc+1,iyc  ,izc+2,k)%d = f(ixc+1,iyc  ,izc+2,k)%d + wx1*ww
  f(ixc+2,iyc  ,izc+2,k)%d = f(ixc+2,iyc  ,izc+2,k)%d + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izc+2,k)%d = f(ixc-1,iyc+1,izc+2,k)%d + wxm*ww
  f(ixc  ,iyc+1,izc+2,k)%d = f(ixc  ,iyc+1,izc+2,k)%d + wx0*ww
  f(ixc+1,iyc+1,izc+2,k)%d = f(ixc+1,iyc+1,izc+2,k)%d + wx1*ww
  f(ixc+2,iyc+1,izc+2,k)%d = f(ixc+2,iyc+1,izc+2,k)%d + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izc+2,k)%d = f(ixc-1,iyc+2,izc+2,k)%d + wxm*ww
  f(ixc  ,iyc+2,izc+2,k)%d = f(ixc  ,iyc+2,izc+2,k)%d + wx0*ww
  f(ixc+1,iyc+2,izc+2,k)%d = f(ixc+1,iyc+2,izc+2,k)%d + wx1*ww
  f(ixc+2,iyc+2,izc+2,k)%d = f(ixc+2,iyc+2,izc+2,k)%d + wx2*ww

  !---------------------------------------------------------------------
  ! particle thermal velocity
  if (do_vth) then
    fp2 = (fp(1)**2+fp(2)**2+fp(3)**2)*w
    www = fp2*wzm
    ww = wym*www
    f(ixc-1,iyc-1,izc-1,k)%vth = f(ixc-1,iyc-1,izc-1,k)%vth + wxm*ww
    f(ixc  ,iyc-1,izc-1,k)%vth = f(ixc  ,iyc-1,izc-1,k)%vth + wx0*ww
    f(ixc+1,iyc-1,izc-1,k)%vth = f(ixc+1,iyc-1,izc-1,k)%vth + wx1*ww
    f(ixc+2,iyc-1,izc-1,k)%vth = f(ixc+2,iyc-1,izc-1,k)%vth + wx2*ww
    ww = wy0*www
    f(ixc-1,iyc  ,izc-1,k)%vth = f(ixc-1,iyc  ,izc-1,k)%vth + wxm*ww
    f(ixc  ,iyc  ,izc-1,k)%vth = f(ixc  ,iyc  ,izc-1,k)%vth + wx0*ww
    f(ixc+1,iyc  ,izc-1,k)%vth = f(ixc+1,iyc  ,izc-1,k)%vth + wx1*ww
    f(ixc+2,iyc  ,izc-1,k)%vth = f(ixc+2,iyc  ,izc-1,k)%vth + wx2*ww
    ww = wy1*www
    f(ixc-1,iyc+1,izc-1,k)%vth = f(ixc-1,iyc+1,izc-1,k)%vth + wxm*ww
    f(ixc  ,iyc+1,izc-1,k)%vth = f(ixc  ,iyc+1,izc-1,k)%vth + wx0*ww
    f(ixc+1,iyc+1,izc-1,k)%vth = f(ixc+1,iyc+1,izc-1,k)%vth + wx1*ww
    f(ixc+2,iyc+1,izc-1,k)%vth = f(ixc+2,iyc+1,izc-1,k)%vth + wx2*ww
    ww = wy2*www
    f(ixc-1,iyc+2,izc-1,k)%vth = f(ixc-1,iyc+2,izc-1,k)%vth + wxm*ww
    f(ixc  ,iyc+2,izc-1,k)%vth = f(ixc  ,iyc+2,izc-1,k)%vth + wx0*ww
    f(ixc+1,iyc+2,izc-1,k)%vth = f(ixc+1,iyc+2,izc-1,k)%vth + wx1*ww
    f(ixc+2,iyc+2,izc-1,k)%vth = f(ixc+2,iyc+2,izc-1,k)%vth + wx2*ww
    www = fp2*wz0
    ww = wym*www
    f(ixc-1,iyc-1,izc  ,k)%vth = f(ixc-1,iyc-1,izc  ,k)%vth + wxm*ww
    f(ixc  ,iyc-1,izc  ,k)%vth = f(ixc  ,iyc-1,izc  ,k)%vth + wx0*ww
    f(ixc+1,iyc-1,izc  ,k)%vth = f(ixc+1,iyc-1,izc  ,k)%vth + wx1*ww
    f(ixc+2,iyc-1,izc  ,k)%vth = f(ixc+2,iyc-1,izc  ,k)%vth + wx2*ww
    ww = wy0*www
    f(ixc-1,iyc  ,izc  ,k)%vth = f(ixc-1,iyc  ,izc  ,k)%vth + wxm*ww
    f(ixc  ,iyc  ,izc  ,k)%vth = f(ixc  ,iyc  ,izc  ,k)%vth + wx0*ww
    f(ixc+1,iyc  ,izc  ,k)%vth = f(ixc+1,iyc  ,izc  ,k)%vth + wx1*ww
    f(ixc+2,iyc  ,izc  ,k)%vth = f(ixc+2,iyc  ,izc  ,k)%vth + wx2*ww
    ww = wy1*www
    f(ixc-1,iyc+1,izc  ,k)%vth = f(ixc-1,iyc+1,izc  ,k)%vth + wxm*ww
    f(ixc  ,iyc+1,izc  ,k)%vth = f(ixc  ,iyc+1,izc  ,k)%vth + wx0*ww
    f(ixc+1,iyc+1,izc  ,k)%vth = f(ixc+1,iyc+1,izc  ,k)%vth + wx1*ww
    f(ixc+2,iyc+1,izc  ,k)%vth = f(ixc+2,iyc+1,izc  ,k)%vth + wx2*ww
    ww = wy2*www
    f(ixc-1,iyc+2,izc  ,k)%vth = f(ixc-1,iyc+2,izc  ,k)%vth + wxm*ww
    f(ixc  ,iyc+2,izc  ,k)%vth = f(ixc  ,iyc+2,izc  ,k)%vth + wx0*ww
    f(ixc+1,iyc+2,izc  ,k)%vth = f(ixc+1,iyc+2,izc  ,k)%vth + wx1*ww
    f(ixc+2,iyc+2,izc  ,k)%vth = f(ixc+2,iyc+2,izc  ,k)%vth + wx2*ww
    www = fp2*wz1
    ww = wym*www
    f(ixc-1,iyc-1,izc+1,k)%vth = f(ixc-1,iyc-1,izc+1,k)%vth + wxm*ww
    f(ixc  ,iyc-1,izc+1,k)%vth = f(ixc  ,iyc-1,izc+1,k)%vth + wx0*ww
    f(ixc+1,iyc-1,izc+1,k)%vth = f(ixc+1,iyc-1,izc+1,k)%vth + wx1*ww
    f(ixc+2,iyc-1,izc+1,k)%vth = f(ixc+2,iyc-1,izc+1,k)%vth + wx2*ww
    ww = wy0*www
    f(ixc-1,iyc  ,izc+1,k)%vth = f(ixc-1,iyc  ,izc+1,k)%vth + wxm*ww
    f(ixc  ,iyc  ,izc+1,k)%vth = f(ixc  ,iyc  ,izc+1,k)%vth + wx0*ww
    f(ixc+1,iyc  ,izc+1,k)%vth = f(ixc+1,iyc  ,izc+1,k)%vth + wx1*ww
    f(ixc+2,iyc  ,izc+1,k)%vth = f(ixc+2,iyc  ,izc+1,k)%vth + wx2*ww
    ww = wy1*www
    f(ixc-1,iyc+1,izc+1,k)%vth = f(ixc-1,iyc+1,izc+1,k)%vth + wxm*ww
    f(ixc  ,iyc+1,izc+1,k)%vth = f(ixc  ,iyc+1,izc+1,k)%vth + wx0*ww
    f(ixc+1,iyc+1,izc+1,k)%vth = f(ixc+1,iyc+1,izc+1,k)%vth + wx1*ww
    f(ixc+2,iyc+1,izc+1,k)%vth = f(ixc+2,iyc+1,izc+1,k)%vth + wx2*ww
    ww = wy2*www
    f(ixc-1,iyc+2,izc+1,k)%vth = f(ixc-1,iyc+2,izc+1,k)%vth + wxm*ww
    f(ixc  ,iyc+2,izc+1,k)%vth = f(ixc  ,iyc+2,izc+1,k)%vth + wx0*ww
    f(ixc+1,iyc+2,izc+1,k)%vth = f(ixc+1,iyc+2,izc+1,k)%vth + wx1*ww
    f(ixc+2,iyc+2,izc+1,k)%vth = f(ixc+2,iyc+2,izc+1,k)%vth + wx2*ww
    www = fp2*wz2
    ww = wym*www
    f(ixc-1,iyc-1,izc+2,k)%vth = f(ixc-1,iyc-1,izc+2,k)%vth + wxm*ww
    f(ixc  ,iyc-1,izc+2,k)%vth = f(ixc  ,iyc-1,izc+2,k)%vth + wx0*ww
    f(ixc+1,iyc-1,izc+2,k)%vth = f(ixc+1,iyc-1,izc+2,k)%vth + wx1*ww
    f(ixc+2,iyc-1,izc+2,k)%vth = f(ixc+2,iyc-1,izc+2,k)%vth + wx2*ww
    ww = wy0*www
    f(ixc-1,iyc  ,izc+2,k)%vth = f(ixc-1,iyc  ,izc+2,k)%vth + wxm*ww
    f(ixc  ,iyc  ,izc+2,k)%vth = f(ixc  ,iyc  ,izc+2,k)%vth + wx0*ww
    f(ixc+1,iyc  ,izc+2,k)%vth = f(ixc+1,iyc  ,izc+2,k)%vth + wx1*ww
    f(ixc+2,iyc  ,izc+2,k)%vth = f(ixc+2,iyc  ,izc+2,k)%vth + wx2*ww
    ww = wy1*www
    f(ixc-1,iyc+1,izc+2,k)%vth = f(ixc-1,iyc+1,izc+2,k)%vth + wxm*ww
    f(ixc  ,iyc+1,izc+2,k)%vth = f(ixc  ,iyc+1,izc+2,k)%vth + wx0*ww
    f(ixc+1,iyc+1,izc+2,k)%vth = f(ixc+1,iyc+1,izc+2,k)%vth + wx1*ww
    f(ixc+2,iyc+1,izc+2,k)%vth = f(ixc+2,iyc+1,izc+2,k)%vth + wx2*ww
    ww = wy2*www
    f(ixc-1,iyc+2,izc+2,k)%vth = f(ixc-1,iyc+2,izc+2,k)%vth + wxm*ww
    f(ixc  ,iyc+2,izc+2,k)%vth = f(ixc  ,iyc+2,izc+2,k)%vth + wx0*ww
    f(ixc+1,iyc+2,izc+2,k)%vth = f(ixc+1,iyc+2,izc+2,k)%vth + wx1*ww
    f(ixc+2,iyc+2,izc+2,k)%vth = f(ixc+2,iyc+2,izc+2,k)%vth + wx2*ww
  end if 

  !---------------------------------------------------------------------
  ! Particle x-flux
  www = fp(1)*w*wzm
  ww = wym*www
  f(ixs-1,iyc-1,izc-1,k)%v(1) = f(ixs-1,iyc-1,izc-1,k)%v(1) + dxm*ww
  f(ixs  ,iyc-1,izc-1,k)%v(1) = f(ixs  ,iyc-1,izc-1,k)%v(1) + dx0*ww
  f(ixs+1,iyc-1,izc-1,k)%v(1) = f(ixs+1,iyc-1,izc-1,k)%v(1) + dx1*ww
  f(ixs+2,iyc-1,izc-1,k)%v(1) = f(ixs+2,iyc-1,izc-1,k)%v(1) + dx2*ww
  ww = wy0*www
  f(ixs-1,iyc  ,izc-1,k)%v(1) = f(ixs-1,iyc  ,izc-1,k)%v(1) + dxm*ww
  f(ixs  ,iyc  ,izc-1,k)%v(1) = f(ixs  ,iyc  ,izc-1,k)%v(1) + dx0*ww
  f(ixs+1,iyc  ,izc-1,k)%v(1) = f(ixs+1,iyc  ,izc-1,k)%v(1) + dx1*ww
  f(ixs+2,iyc  ,izc-1,k)%v(1) = f(ixs+2,iyc  ,izc-1,k)%v(1) + dx2*ww
  ww = wy1*www
  f(ixs-1,iyc+1,izc-1,k)%v(1) = f(ixs-1,iyc+1,izc-1,k)%v(1) + dxm*ww
  f(ixs  ,iyc+1,izc-1,k)%v(1) = f(ixs  ,iyc+1,izc-1,k)%v(1) + dx0*ww
  f(ixs+1,iyc+1,izc-1,k)%v(1) = f(ixs+1,iyc+1,izc-1,k)%v(1) + dx1*ww
  f(ixs+2,iyc+1,izc-1,k)%v(1) = f(ixs+2,iyc+1,izc-1,k)%v(1) + dx2*ww
  ww = wy2*www
  f(ixs-1,iyc+2,izc-1,k)%v(1) = f(ixs-1,iyc+2,izc-1,k)%v(1) + dxm*ww
  f(ixs  ,iyc+2,izc-1,k)%v(1) = f(ixs  ,iyc+2,izc-1,k)%v(1) + dx0*ww
  f(ixs+1,iyc+2,izc-1,k)%v(1) = f(ixs+1,iyc+2,izc-1,k)%v(1) + dx1*ww
  f(ixs+2,iyc+2,izc-1,k)%v(1) = f(ixs+2,iyc+2,izc-1,k)%v(1) + dx2*ww
  www = fp(1)*w*wz0
  ww = wym*www
  f(ixs-1,iyc-1,izc  ,k)%v(1) = f(ixs-1,iyc-1,izc  ,k)%v(1) + dxm*ww
  f(ixs  ,iyc-1,izc  ,k)%v(1) = f(ixs  ,iyc-1,izc  ,k)%v(1) + dx0*ww
  f(ixs+1,iyc-1,izc  ,k)%v(1) = f(ixs+1,iyc-1,izc  ,k)%v(1) + dx1*ww
  f(ixs+2,iyc-1,izc  ,k)%v(1) = f(ixs+2,iyc-1,izc  ,k)%v(1) + dx2*ww
  ww = wy0*www
  f(ixs-1,iyc  ,izc  ,k)%v(1) = f(ixs-1,iyc  ,izc  ,k)%v(1) + dxm*ww
  f(ixs  ,iyc  ,izc  ,k)%v(1) = f(ixs  ,iyc  ,izc  ,k)%v(1) + dx0*ww
  f(ixs+1,iyc  ,izc  ,k)%v(1) = f(ixs+1,iyc  ,izc  ,k)%v(1) + dx1*ww
  f(ixs+2,iyc  ,izc  ,k)%v(1) = f(ixs+2,iyc  ,izc  ,k)%v(1) + dx2*ww
  ww = wy1*www
  f(ixs-1,iyc+1,izc  ,k)%v(1) = f(ixs-1,iyc+1,izc  ,k)%v(1) + dxm*ww
  f(ixs  ,iyc+1,izc  ,k)%v(1) = f(ixs  ,iyc+1,izc  ,k)%v(1) + dx0*ww
  f(ixs+1,iyc+1,izc  ,k)%v(1) = f(ixs+1,iyc+1,izc  ,k)%v(1) + dx1*ww
  f(ixs+2,iyc+1,izc  ,k)%v(1) = f(ixs+2,iyc+1,izc  ,k)%v(1) + dx2*ww
  ww = wy2*www
  f(ixs-1,iyc+2,izc  ,k)%v(1) = f(ixs-1,iyc+2,izc  ,k)%v(1) + dxm*ww
  f(ixs  ,iyc+2,izc  ,k)%v(1) = f(ixs  ,iyc+2,izc  ,k)%v(1) + dx0*ww
  f(ixs+1,iyc+2,izc  ,k)%v(1) = f(ixs+1,iyc+2,izc  ,k)%v(1) + dx1*ww
  f(ixs+2,iyc+2,izc  ,k)%v(1) = f(ixs+2,iyc+2,izc  ,k)%v(1) + dx2*ww
  www = fp(1)*w*wz1
  ww = wym*www
  f(ixs-1,iyc-1,izc+1,k)%v(1) = f(ixs-1,iyc-1,izc+1,k)%v(1) + dxm*ww
  f(ixs  ,iyc-1,izc+1,k)%v(1) = f(ixs  ,iyc-1,izc+1,k)%v(1) + dx0*ww
  f(ixs+1,iyc-1,izc+1,k)%v(1) = f(ixs+1,iyc-1,izc+1,k)%v(1) + dx1*ww
  f(ixs+2,iyc-1,izc+1,k)%v(1) = f(ixs+2,iyc-1,izc+1,k)%v(1) + dx2*ww
  ww = wy0*www
  f(ixs-1,iyc  ,izc+1,k)%v(1) = f(ixs-1,iyc  ,izc+1,k)%v(1) + dxm*ww
  f(ixs  ,iyc  ,izc+1,k)%v(1) = f(ixs  ,iyc  ,izc+1,k)%v(1) + dx0*ww
  f(ixs+1,iyc  ,izc+1,k)%v(1) = f(ixs+1,iyc  ,izc+1,k)%v(1) + dx1*ww
  f(ixs+2,iyc  ,izc+1,k)%v(1) = f(ixs+2,iyc  ,izc+1,k)%v(1) + dx2*ww
  ww = wy1*www
  f(ixs-1,iyc+1,izc+1,k)%v(1) = f(ixs-1,iyc+1,izc+1,k)%v(1) + dxm*ww
  f(ixs  ,iyc+1,izc+1,k)%v(1) = f(ixs  ,iyc+1,izc+1,k)%v(1) + dx0*ww
  f(ixs+1,iyc+1,izc+1,k)%v(1) = f(ixs+1,iyc+1,izc+1,k)%v(1) + dx1*ww
  f(ixs+2,iyc+1,izc+1,k)%v(1) = f(ixs+2,iyc+1,izc+1,k)%v(1) + dx2*ww
  ww = wy2*www
  f(ixs-1,iyc+2,izc+1,k)%v(1) = f(ixs-1,iyc+2,izc+1,k)%v(1) + dxm*ww
  f(ixs  ,iyc+2,izc+1,k)%v(1) = f(ixs  ,iyc+2,izc+1,k)%v(1) + dx0*ww
  f(ixs+1,iyc+2,izc+1,k)%v(1) = f(ixs+1,iyc+2,izc+1,k)%v(1) + dx1*ww
  f(ixs+2,iyc+2,izc+1,k)%v(1) = f(ixs+2,iyc+2,izc+1,k)%v(1) + dx2*ww
  www = fp(1)*w*wz2
  ww = wym*www
  f(ixs-1,iyc-1,izc+2,k)%v(1) = f(ixs-1,iyc-1,izc+2,k)%v(1) + dxm*ww
  f(ixs  ,iyc-1,izc+2,k)%v(1) = f(ixs  ,iyc-1,izc+2,k)%v(1) + dx0*ww
  f(ixs+1,iyc-1,izc+2,k)%v(1) = f(ixs+1,iyc-1,izc+2,k)%v(1) + dx1*ww
  f(ixs+2,iyc-1,izc+2,k)%v(1) = f(ixs+2,iyc-1,izc+2,k)%v(1) + dx2*ww
  ww = wy0*www
  f(ixs-1,iyc  ,izc+2,k)%v(1) = f(ixs-1,iyc  ,izc+2,k)%v(1) + dxm*ww
  f(ixs  ,iyc  ,izc+2,k)%v(1) = f(ixs  ,iyc  ,izc+2,k)%v(1) + dx0*ww
  f(ixs+1,iyc  ,izc+2,k)%v(1) = f(ixs+1,iyc  ,izc+2,k)%v(1) + dx1*ww
  f(ixs+2,iyc  ,izc+2,k)%v(1) = f(ixs+2,iyc  ,izc+2,k)%v(1) + dx2*ww
  ww = wy1*www
  f(ixs-1,iyc+1,izc+2,k)%v(1) = f(ixs-1,iyc+1,izc+2,k)%v(1) + dxm*ww
  f(ixs  ,iyc+1,izc+2,k)%v(1) = f(ixs  ,iyc+1,izc+2,k)%v(1) + dx0*ww
  f(ixs+1,iyc+1,izc+2,k)%v(1) = f(ixs+1,iyc+1,izc+2,k)%v(1) + dx1*ww
  f(ixs+2,iyc+1,izc+2,k)%v(1) = f(ixs+2,iyc+1,izc+2,k)%v(1) + dx2*ww
  ww = wy2*www
  f(ixs-1,iyc+2,izc+2,k)%v(1) = f(ixs-1,iyc+2,izc+2,k)%v(1) + dxm*ww
  f(ixs  ,iyc+2,izc+2,k)%v(1) = f(ixs  ,iyc+2,izc+2,k)%v(1) + dx0*ww
  f(ixs+1,iyc+2,izc+2,k)%v(1) = f(ixs+1,iyc+2,izc+2,k)%v(1) + dx1*ww
  f(ixs+2,iyc+2,izc+2,k)%v(1) = f(ixs+2,iyc+2,izc+2,k)%v(1) + dx2*ww

  !---------------------------------------------------------------------
  ! Particle y-flux
  www = fp(2)*w*wzm
  ww = dym*www
  f(ixc-1,iys-1,izc-1,k)%v(2) = f(ixc-1,iys-1,izc-1,k)%v(2) + wxm*ww
  f(ixc  ,iys-1,izc-1,k)%v(2) = f(ixc  ,iys-1,izc-1,k)%v(2) + wx0*ww
  f(ixc+1,iys-1,izc-1,k)%v(2) = f(ixc+1,iys-1,izc-1,k)%v(2) + wx1*ww
  f(ixc+2,iys-1,izc-1,k)%v(2) = f(ixc+2,iys-1,izc-1,k)%v(2) + wx2*ww
  ww = dy0*www
  f(ixc-1,iys  ,izc-1,k)%v(2) = f(ixc-1,iys  ,izc-1,k)%v(2) + wxm*ww
  f(ixc  ,iys  ,izc-1,k)%v(2) = f(ixc  ,iys  ,izc-1,k)%v(2) + wx0*ww
  f(ixc+1,iys  ,izc-1,k)%v(2) = f(ixc+1,iys  ,izc-1,k)%v(2) + wx1*ww
  f(ixc+2,iys  ,izc-1,k)%v(2) = f(ixc+2,iys  ,izc-1,k)%v(2) + wx2*ww
  ww = dy1*www
  f(ixc-1,iys+1,izc-1,k)%v(2) = f(ixc-1,iys+1,izc-1,k)%v(2) + wxm*ww
  f(ixc  ,iys+1,izc-1,k)%v(2) = f(ixc  ,iys+1,izc-1,k)%v(2) + wx0*ww
  f(ixc+1,iys+1,izc-1,k)%v(2) = f(ixc+1,iys+1,izc-1,k)%v(2) + wx1*ww
  f(ixc+2,iys+1,izc-1,k)%v(2) = f(ixc+2,iys+1,izc-1,k)%v(2) + wx2*ww
  ww = dy2*www
  f(ixc-1,iys+2,izc-1,k)%v(2) = f(ixc-1,iys+2,izc-1,k)%v(2) + wxm*ww
  f(ixc  ,iys+2,izc-1,k)%v(2) = f(ixc  ,iys+2,izc-1,k)%v(2) + wx0*ww
  f(ixc+1,iys+2,izc-1,k)%v(2) = f(ixc+1,iys+2,izc-1,k)%v(2) + wx1*ww
  f(ixc+2,iys+2,izc-1,k)%v(2) = f(ixc+2,iys+2,izc-1,k)%v(2) + wx2*ww
  www = fp(2)*w*wz0
  ww = dym*www
  f(ixc-1,iys-1,izc  ,k)%v(2) = f(ixc-1,iys-1,izc  ,k)%v(2) + wxm*ww
  f(ixc  ,iys-1,izc  ,k)%v(2) = f(ixc  ,iys-1,izc  ,k)%v(2) + wx0*ww
  f(ixc+1,iys-1,izc  ,k)%v(2) = f(ixc+1,iys-1,izc  ,k)%v(2) + wx1*ww
  f(ixc+2,iys-1,izc  ,k)%v(2) = f(ixc+2,iys-1,izc  ,k)%v(2) + wx2*ww
  ww = dy0*www
  f(ixc-1,iys  ,izc  ,k)%v(2) = f(ixc-1,iys  ,izc  ,k)%v(2) + wxm*ww
  f(ixc  ,iys  ,izc  ,k)%v(2) = f(ixc  ,iys  ,izc  ,k)%v(2) + wx0*ww
  f(ixc+1,iys  ,izc  ,k)%v(2) = f(ixc+1,iys  ,izc  ,k)%v(2) + wx1*ww
  f(ixc+2,iys  ,izc  ,k)%v(2) = f(ixc+2,iys  ,izc  ,k)%v(2) + wx2*ww
  ww = dy1*www
  f(ixc-1,iys+1,izc  ,k)%v(2) = f(ixc-1,iys+1,izc  ,k)%v(2) + wxm*ww
  f(ixc  ,iys+1,izc  ,k)%v(2) = f(ixc  ,iys+1,izc  ,k)%v(2) + wx0*ww
  f(ixc+1,iys+1,izc  ,k)%v(2) = f(ixc+1,iys+1,izc  ,k)%v(2) + wx1*ww
  f(ixc+2,iys+1,izc  ,k)%v(2) = f(ixc+2,iys+1,izc  ,k)%v(2) + wx2*ww
  ww = dy2*www
  f(ixc-1,iys+2,izc  ,k)%v(2) = f(ixc-1,iys+2,izc  ,k)%v(2) + wxm*ww
  f(ixc  ,iys+2,izc  ,k)%v(2) = f(ixc  ,iys+2,izc  ,k)%v(2) + wx0*ww
  f(ixc+1,iys+2,izc  ,k)%v(2) = f(ixc+1,iys+2,izc  ,k)%v(2) + wx1*ww
  f(ixc+2,iys+2,izc  ,k)%v(2) = f(ixc+2,iys+2,izc  ,k)%v(2) + wx2*ww
  www = fp(2)*w*wz1
  ww = dym*www
  f(ixc-1,iys-1,izc+1,k)%v(2) = f(ixc-1,iys-1,izc+1,k)%v(2) + wxm*ww
  f(ixc  ,iys-1,izc+1,k)%v(2) = f(ixc  ,iys-1,izc+1,k)%v(2) + wx0*ww
  f(ixc+1,iys-1,izc+1,k)%v(2) = f(ixc+1,iys-1,izc+1,k)%v(2) + wx1*ww
  f(ixc+2,iys-1,izc+1,k)%v(2) = f(ixc+2,iys-1,izc+1,k)%v(2) + wx2*ww
  ww = dy0*www
  f(ixc-1,iys  ,izc+1,k)%v(2) = f(ixc-1,iys  ,izc+1,k)%v(2) + wxm*ww
  f(ixc  ,iys  ,izc+1,k)%v(2) = f(ixc  ,iys  ,izc+1,k)%v(2) + wx0*ww
  f(ixc+1,iys  ,izc+1,k)%v(2) = f(ixc+1,iys  ,izc+1,k)%v(2) + wx1*ww
  f(ixc+2,iys  ,izc+1,k)%v(2) = f(ixc+2,iys  ,izc+1,k)%v(2) + wx2*ww
  ww = dy1*www
  f(ixc-1,iys+1,izc+1,k)%v(2) = f(ixc-1,iys+1,izc+1,k)%v(2) + wxm*ww
  f(ixc  ,iys+1,izc+1,k)%v(2) = f(ixc  ,iys+1,izc+1,k)%v(2) + wx0*ww
  f(ixc+1,iys+1,izc+1,k)%v(2) = f(ixc+1,iys+1,izc+1,k)%v(2) + wx1*ww
  f(ixc+2,iys+1,izc+1,k)%v(2) = f(ixc+2,iys+1,izc+1,k)%v(2) + wx2*ww
  ww = dy2*www
  f(ixc-1,iys+2,izc+1,k)%v(2) = f(ixc-1,iys+2,izc+1,k)%v(2) + wxm*ww
  f(ixc  ,iys+2,izc+1,k)%v(2) = f(ixc  ,iys+2,izc+1,k)%v(2) + wx0*ww
  f(ixc+1,iys+2,izc+1,k)%v(2) = f(ixc+1,iys+2,izc+1,k)%v(2) + wx1*ww
  f(ixc+2,iys+2,izc+1,k)%v(2) = f(ixc+2,iys+2,izc+1,k)%v(2) + wx2*ww
  www = fp(2)*w*wz2
  ww = dym*www
  f(ixc-1,iys-1,izc+2,k)%v(2) = f(ixc-1,iys-1,izc+2,k)%v(2) + wxm*ww
  f(ixc  ,iys-1,izc+2,k)%v(2) = f(ixc  ,iys-1,izc+2,k)%v(2) + wx0*ww
  f(ixc+1,iys-1,izc+2,k)%v(2) = f(ixc+1,iys-1,izc+2,k)%v(2) + wx1*ww
  f(ixc+2,iys-1,izc+2,k)%v(2) = f(ixc+2,iys-1,izc+2,k)%v(2) + wx2*ww
  ww = dy0*www
  f(ixc-1,iys  ,izc+2,k)%v(2) = f(ixc-1,iys  ,izc+2,k)%v(2) + wxm*ww
  f(ixc  ,iys  ,izc+2,k)%v(2) = f(ixc  ,iys  ,izc+2,k)%v(2) + wx0*ww
  f(ixc+1,iys  ,izc+2,k)%v(2) = f(ixc+1,iys  ,izc+2,k)%v(2) + wx1*ww
  f(ixc+2,iys  ,izc+2,k)%v(2) = f(ixc+2,iys  ,izc+2,k)%v(2) + wx2*ww
  ww = dy1*www
  f(ixc-1,iys+1,izc+2,k)%v(2) = f(ixc-1,iys+1,izc+2,k)%v(2) + wxm*ww
  f(ixc  ,iys+1,izc+2,k)%v(2) = f(ixc  ,iys+1,izc+2,k)%v(2) + wx0*ww
  f(ixc+1,iys+1,izc+2,k)%v(2) = f(ixc+1,iys+1,izc+2,k)%v(2) + wx1*ww
  f(ixc+2,iys+1,izc+2,k)%v(2) = f(ixc+2,iys+1,izc+2,k)%v(2) + wx2*ww
  ww = dy2*www
  f(ixc-1,iys+2,izc+2,k)%v(2) = f(ixc-1,iys+2,izc+2,k)%v(2) + wxm*ww
  f(ixc  ,iys+2,izc+2,k)%v(2) = f(ixc  ,iys+2,izc+2,k)%v(2) + wx0*ww
  f(ixc+1,iys+2,izc+2,k)%v(2) = f(ixc+1,iys+2,izc+2,k)%v(2) + wx1*ww
  f(ixc+2,iys+2,izc+2,k)%v(2) = f(ixc+2,iys+2,izc+2,k)%v(2) + wx2*ww


  !---------------------------------------------------------------------
  ! Particle y-flux
  www = fp(3)*w*dzm
  ww = wym*www
  f(ixc-1,iyc-1,izs-1,k)%v(3) = f(ixc-1,iyc-1,izs-1,k)%v(3) + wxm*ww
  f(ixc  ,iyc-1,izs-1,k)%v(3) = f(ixc  ,iyc-1,izs-1,k)%v(3) + wx0*ww
  f(ixc+1,iyc-1,izs-1,k)%v(3) = f(ixc+1,iyc-1,izs-1,k)%v(3) + wx1*ww
  f(ixc+2,iyc-1,izs-1,k)%v(3) = f(ixc+2,iyc-1,izs-1,k)%v(3) + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izs-1,k)%v(3) = f(ixc-1,iyc  ,izs-1,k)%v(3) + wxm*ww
  f(ixc  ,iyc  ,izs-1,k)%v(3) = f(ixc  ,iyc  ,izs-1,k)%v(3) + wx0*ww
  f(ixc+1,iyc  ,izs-1,k)%v(3) = f(ixc+1,iyc  ,izs-1,k)%v(3) + wx1*ww
  f(ixc+2,iyc  ,izs-1,k)%v(3) = f(ixc+2,iyc  ,izs-1,k)%v(3) + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izs-1,k)%v(3) = f(ixc-1,iyc+1,izs-1,k)%v(3) + wxm*ww
  f(ixc  ,iyc+1,izs-1,k)%v(3) = f(ixc  ,iyc+1,izs-1,k)%v(3) + wx0*ww
  f(ixc+1,iyc+1,izs-1,k)%v(3) = f(ixc+1,iyc+1,izs-1,k)%v(3) + wx1*ww
  f(ixc+2,iyc+1,izs-1,k)%v(3) = f(ixc+2,iyc+1,izs-1,k)%v(3) + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izs-1,k)%v(3) = f(ixc-1,iyc+2,izs-1,k)%v(3) + wxm*ww
  f(ixc  ,iyc+2,izs-1,k)%v(3) = f(ixc  ,iyc+2,izs-1,k)%v(3) + wx0*ww
  f(ixc+1,iyc+2,izs-1,k)%v(3) = f(ixc+1,iyc+2,izs-1,k)%v(3) + wx1*ww
  f(ixc+2,iyc+2,izs-1,k)%v(3) = f(ixc+2,iyc+2,izs-1,k)%v(3) + wx2*ww
  www = fp(3)*w*dz0
  ww = wym*www
  f(ixc-1,iyc-1,izs  ,k)%v(3) = f(ixc-1,iyc-1,izs  ,k)%v(3) + wxm*ww
  f(ixc  ,iyc-1,izs  ,k)%v(3) = f(ixc  ,iyc-1,izs  ,k)%v(3) + wx0*ww
  f(ixc+1,iyc-1,izs  ,k)%v(3) = f(ixc+1,iyc-1,izs  ,k)%v(3) + wx1*ww
  f(ixc+2,iyc-1,izs  ,k)%v(3) = f(ixc+2,iyc-1,izs  ,k)%v(3) + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izs  ,k)%v(3) = f(ixc-1,iyc  ,izs  ,k)%v(3) + wxm*ww
  f(ixc  ,iyc  ,izs  ,k)%v(3) = f(ixc  ,iyc  ,izs  ,k)%v(3) + wx0*ww
  f(ixc+1,iyc  ,izs  ,k)%v(3) = f(ixc+1,iyc  ,izs  ,k)%v(3) + wx1*ww
  f(ixc+2,iyc  ,izs  ,k)%v(3) = f(ixc+2,iyc  ,izs  ,k)%v(3) + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izs  ,k)%v(3) = f(ixc-1,iyc+1,izs  ,k)%v(3) + wxm*ww
  f(ixc  ,iyc+1,izs  ,k)%v(3) = f(ixc  ,iyc+1,izs  ,k)%v(3) + wx0*ww
  f(ixc+1,iyc+1,izs  ,k)%v(3) = f(ixc+1,iyc+1,izs  ,k)%v(3) + wx1*ww
  f(ixc+2,iyc+1,izs  ,k)%v(3) = f(ixc+2,iyc+1,izs  ,k)%v(3) + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izs  ,k)%v(3) = f(ixc-1,iyc+2,izs  ,k)%v(3) + wxm*ww
  f(ixc  ,iyc+2,izs  ,k)%v(3) = f(ixc  ,iyc+2,izs  ,k)%v(3) + wx0*ww
  f(ixc+1,iyc+2,izs  ,k)%v(3) = f(ixc+1,iyc+2,izs  ,k)%v(3) + wx1*ww
  f(ixc+2,iyc+2,izs  ,k)%v(3) = f(ixc+2,iyc+2,izs  ,k)%v(3) + wx2*ww
  www = fp(3)*w*dz1
  ww = wym*www
  f(ixc-1,iyc-1,izs+1,k)%v(3) = f(ixc-1,iyc-1,izs+1,k)%v(3) + wxm*ww
  f(ixc  ,iyc-1,izs+1,k)%v(3) = f(ixc  ,iyc-1,izs+1,k)%v(3) + wx0*ww
  f(ixc+1,iyc-1,izs+1,k)%v(3) = f(ixc+1,iyc-1,izs+1,k)%v(3) + wx1*ww
  f(ixc+2,iyc-1,izs+1,k)%v(3) = f(ixc+2,iyc-1,izs+1,k)%v(3) + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izs+1,k)%v(3) = f(ixc-1,iyc  ,izs+1,k)%v(3) + wxm*ww
  f(ixc  ,iyc  ,izs+1,k)%v(3) = f(ixc  ,iyc  ,izs+1,k)%v(3) + wx0*ww
  f(ixc+1,iyc  ,izs+1,k)%v(3) = f(ixc+1,iyc  ,izs+1,k)%v(3) + wx1*ww
  f(ixc+2,iyc  ,izs+1,k)%v(3) = f(ixc+2,iyc  ,izs+1,k)%v(3) + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izs+1,k)%v(3) = f(ixc-1,iyc+1,izs+1,k)%v(3) + wxm*ww
  f(ixc  ,iyc+1,izs+1,k)%v(3) = f(ixc  ,iyc+1,izs+1,k)%v(3) + wx0*ww
  f(ixc+1,iyc+1,izs+1,k)%v(3) = f(ixc+1,iyc+1,izs+1,k)%v(3) + wx1*ww
  f(ixc+2,iyc+1,izs+1,k)%v(3) = f(ixc+2,iyc+1,izs+1,k)%v(3) + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izs+1,k)%v(3) = f(ixc-1,iyc+2,izs+1,k)%v(3) + wxm*ww
  f(ixc  ,iyc+2,izs+1,k)%v(3) = f(ixc  ,iyc+2,izs+1,k)%v(3) + wx0*ww
  f(ixc+1,iyc+2,izs+1,k)%v(3) = f(ixc+1,iyc+2,izs+1,k)%v(3) + wx1*ww
  f(ixc+2,iyc+2,izs+1,k)%v(3) = f(ixc+2,iyc+2,izs+1,k)%v(3) + wx2*ww
  www = fp(3)*w*dz2
  ww = wym*www
  f(ixc-1,iyc-1,izs+2,k)%v(3) = f(ixc-1,iyc-1,izs+2,k)%v(3) + wxm*ww
  f(ixc  ,iyc-1,izs+2,k)%v(3) = f(ixc  ,iyc-1,izs+2,k)%v(3) + wx0*ww
  f(ixc+1,iyc-1,izs+2,k)%v(3) = f(ixc+1,iyc-1,izs+2,k)%v(3) + wx1*ww
  f(ixc+2,iyc-1,izs+2,k)%v(3) = f(ixc+2,iyc-1,izs+2,k)%v(3) + wx2*ww
  ww = wy0*www
  f(ixc-1,iyc  ,izs+2,k)%v(3) = f(ixc-1,iyc  ,izs+2,k)%v(3) + wxm*ww
  f(ixc  ,iyc  ,izs+2,k)%v(3) = f(ixc  ,iyc  ,izs+2,k)%v(3) + wx0*ww
  f(ixc+1,iyc  ,izs+2,k)%v(3) = f(ixc+1,iyc  ,izs+2,k)%v(3) + wx1*ww
  f(ixc+2,iyc  ,izs+2,k)%v(3) = f(ixc+2,iyc  ,izs+2,k)%v(3) + wx2*ww
  ww = wy1*www
  f(ixc-1,iyc+1,izs+2,k)%v(3) = f(ixc-1,iyc+1,izs+2,k)%v(3) + wxm*ww
  f(ixc  ,iyc+1,izs+2,k)%v(3) = f(ixc  ,iyc+1,izs+2,k)%v(3) + wx0*ww
  f(ixc+1,iyc+1,izs+2,k)%v(3) = f(ixc+1,iyc+1,izs+2,k)%v(3) + wx1*ww
  f(ixc+2,iyc+1,izs+2,k)%v(3) = f(ixc+2,iyc+1,izs+2,k)%v(3) + wx2*ww
  ww = wy2*www
  f(ixc-1,iyc+2,izs+2,k)%v(3) = f(ixc-1,iyc+2,izs+2,k)%v(3) + wxm*ww
  f(ixc  ,iyc+2,izs+2,k)%v(3) = f(ixc  ,iyc+2,izs+2,k)%v(3) + wx0*ww
  f(ixc+1,iyc+2,izs+2,k)%v(3) = f(ixc+1,iyc+2,izs+2,k)%v(3) + wx1*ww
  f(ixc+2,iyc+2,izs+2,k)%v(3) = f(ixc+2,iyc+2,izs+2,k)%v(3) + wx2*ww

  if (master .and. nprint<maxprint .and. debugit(dbg_init,1)) then
    nprint = nprint + 1
    print *,'gather:'
    print 1,' centered x-coordinates: ', ixc-1, '-', ixc+2 
    print 1,' centered y-coordinates: ', iyc-1, '-', iyc+2 
    print 1,' centered z-coordinates: ', izc-1, '-', izc+2 
    print 1,'staggered x-coordinates: ', ixs-1, '-', ixs+2 
    print 1,'staggered y-coordinates: ', iys-1, '-', iys+2 
    print 1,'staggered z-coordinates: ', izs-1, '-', izs+2 
    print 2,'where g%lb is:', g%lb, ' with 1e-4 times the values below'
    do jz=-1,2
      print *,izc+jz, izs+jz
      do jy=-1,2
        print 3,f(ixc-1:ixc+2,iyc+jy,izc+jz,k)%d   , &
                f(ixs-1:ixs+2,iyc+jy,izc+jz,k)%v(1), &
                f(ixc-1:ixc+2,iys+jy,izc+jz,k)%v(2), &
                f(ixc-1:ixc+2,iyc+jy,izs+jz,k)%v(3)
      end do
    end do
  end if
1 format(1x,a,i4,2x,a,i3)
2 format(1x,a,3(i2,','),a)
3 format(4(4p,4f7.3,1x))

END SUBROUTINE

!=======================================================================
SUBROUTINE test_interpolation
  USE params, only : stdout, mpi
  USE grid_m,   only : g
  USE interpolation
  implicit none
  logical err
  integer iz, q(3)
  real r(3)
!.......................................................................
  if (mpi%me(3) > 0) return
  write(stdout,*) 'TESTING INTERPOLATION:'
  write(stdout,*) '      r  iz1 ize1   wz0   wz1   wz2  wsz0  wsz1  wsz2'
  do iz=1,40
    r = (/0.1,0.1,0.1/)
    q = (/0, 0, 0/)
    call makeweights (r, q)
    write(stdout,'(f8.2,2(i5,3f6.2))') r(3),izc,wz0,wz1,wz2,izs,dz0,dz1,dz2
  end do
END SUBROUTINE test_interpolation
