! $Id$
! vim: nowrap
!-----------------------------------------------------------------------
! Routines for inlined interpolation and particle operations
!=======================================================================
MODULE interpolation_cached
  implicit none
  integer, parameter :: slc=-1, slb=0, sub=1, suc=2
  real, dimension(slc:sub,slb:sub,slb:sub) :: sEx                       ! Cached E-field
  real, dimension(slb:sub,slc:sub,slb:sub) :: sEy
  real, dimension(slb:sub,slb:sub,slc:sub) :: sEz
  real, dimension(slb:sub,slc:sub,slc:sub) :: sBx                       ! Cached B-field
  real, dimension(slc:sub,slb:sub,slc:sub) :: sBy
  real, dimension(slc:sub,slc:sub,slb:sub) :: sBz
  real, dimension(slc:suc,slc:suc,slc:suc) :: d                         ! Cached number density
  real, dimension(slc:suc,slc:suc,slc:suc,3) :: v                       ! Cached number density flux
  real, dimension(slc:suc,3)              :: w_old, ws_old, w_new, ws_new ! Interpolation weights
  real, parameter :: one_sixth=1./6.
CONTAINS
!=======================================================================
! Calculate weights for centered quantities
!-----------------------------------------------------------------------
SUBROUTINE make_centered_weights(r,w)
  implicit none
  real, dimension(3), intent(in) :: r
  real, dimension(slc:suc,3)     :: w
  !-------------
  ! X_COORD CENTERED
  w(0,1) = 1. - r(1)
  w(1,1) = r(1)
  !-------------
  ! Y_COORD CENTERED
  w(0,2) = 1. - r(2)
  w(1,2) = r(2)
  !-------------
  ! Z_COORD CENTERED
  w(0,3) = 1. - r(3)
  w(1,3) = r(3)
END SUBROUTINE make_centered_weights
! Calculate weights for staggered quantities
!-----------------------------------------------------------------------
SUBROUTINE make_staggered_weights(r,w)
  implicit none
  real, dimension(3), intent(in) :: r
  real, dimension(slc:suc,3)     :: w
  real :: s
  logical :: up
  !-------------
  ! X_COORD SHIFTED IN X
  s = r(1) - 0.5; up = s > 0
  w(-1,1) = merge(0.0,-s, up)
  w( 1,1) = merge(s, 0.0, up)
  w( 0,1) = 1. - w(-1,1) - w( 1,1)
  !-------------
  ! Y_COORD SHIFTED IN Y
  s = r(2) - 0.5; up = s > 0
  w(-1,2) = merge(0.0,-s, up)
  w( 1,2) = merge(s, 0.0, up)
  w( 0,2) = 1. - w(-1,2) - w( 1,2)
  !-------------
  ! Z_COORD SHIFTED IN Z
  s = r(3) - 0.5; up = s > 0
  w(-1,3) = merge(0.0,-s, up)
  w( 1,3) = merge(s, 0.0, up)
  w( 0,3) = 1. - w(-1,3) - w( 1,3)
END SUBROUTINE make_staggered_weights
! Calculate weights for centered quantities, after particle has moved
!-----------------------------------------------------------------------
SUBROUTINE make_centered_weights_updated(r,ox,oy,oz,w)
  implicit none
  real, dimension(3), intent(in) :: r
  integer,            intent(in) :: ox, oy, oz
  real, dimension(slc:suc,3)     :: w
  ! ---------------------------------------------------------
  ! Find signed distance to center points in all directions.
  ! Project index into allowed range for interpolation.
  !-------------
  ! X_COORD CENTERED
  w(  ox,1) = 1. - r(1)
  w(1+ox,1) = r(1)
  !-------------
  ! Y_COORD CENTERED
  w(  oy,2) = 1. - r(2)
  w(1+oy,2) = r(2)
  !-------------
  ! Z_COORD CENTERED
  w(  oz,3) = 1. - r(3)
  w(1+oz,3) = r(3)
END SUBROUTINE make_centered_weights_updated
! Calculate weights for centered quantities, after particle has moved
!-----------------------------------------------------------------------
SUBROUTINE make_staggered_weights_updated(r,ox,oy,oz,w)
  implicit none
  real, dimension(3), intent(in) :: r
  integer,            intent(in) :: ox, oy, oz
  real, dimension(slc:suc,3)     :: w
  !
  real    :: s
  logical :: up
  ! ---------------------------------------------------------
  ! Find signed distance to center points in all directions.
  ! Project index into allowed range for interpolation.
  ! X_COORD STAGGERED
  s = r(1) + ox - 0.5; up = s > 0
  w(-1,1) = merge(0.0, -s, up)
  w( 1,1) = merge(s, 0.0, up)
  w( 0,1) = 1. - w(-1,1) - w( 1,1)
  !-------------
  ! Y_COORD SHIFTED IN Y
  s = r(2) + oy - 0.5; up = s > 0
  w(-1,2) = merge(0.0, -s, up)
  w( 1,2) = merge(s, 0.0, up)
  w( 0,2) = 1. - w(-1,2) - w( 1,2)
  !-------------
  ! Z_COORD SHIFTED IN Z
  s = r(3) + oz - 0.5; up = s > 0
  w(-1,3) = merge(0.0, -s, up)
  w( 1,3) = merge(s, 0.0, up)
  w( 0,3) = 1. - w(-1,3) - w( 1,3)
END SUBROUTINE make_staggered_weights_updated
! Make a mass conserving number density and number density flux deposition
!=======================================================================
SUBROUTINE mass_conserving_deposition(w_old,w_new,pw,p,ox,oy,oz,dxdt,dydt,dzdt)
  USE grid_m, only : g
  implicit none
  real,    intent(in), dimension(slc:suc,3) :: w_old, w_new
  real,    intent(in) :: pw, p(3), dxdt, dydt, dzdt
  integer, intent(in) :: ox, oy, oz
  !
  real    :: wa, wb, wc, wd, we                                         ! Scratch weights
  integer :: jxu,jyu,jzu,jxl,jyl,jzl,jx,jy,jz
  !---------------------------------------------------------------
  ! density is d(jx,jy,jz) = pw * w_new(jx,1) * w_new(jy,2) * w_new(jz,3)
  do jz=oz,oz+1
    wa = pw*w_new(jz,3)
    do jy=oy,oy+1
      we = w_new(jy,2)*wa
      d(  ox,jy,jz) = d(  ox,jy,jz) + w_new(  ox,1)*we
      d(1+ox,jy,jz) = d(1+ox,jy,jz) + w_new(1+ox,1)*we
    enddo
  enddo
  !---------------------------------------------------------------
  ! Number density fluxes:
  jxl = min(0,ox); jxu=max(1,ox+1)                                ! Loop bounds
  jyl = min(0,oy); jyu=max(1,oy+1)
  jzl = min(0,oz); jzu=max(1,oz+1)
  wc = one_sixth * pw                                             ! particle weight / 6
  ! The weights for number density fluxes, in directions with more than one cell, are (for x-dir)
  ! I)   W_x(jx,jy,jz) = 1/6 * pw * dx/dt * (w_new(jx,3)-w_new(jx,3)) *
  !                             ( w_old(jy,2) * (2 w_old(jz,3) +   w_new(jz,3) ) +
  !                               w_new(jy,2) * (  w_old(jz,3) + 2 w_new(jz,3) ) )
  ! For directions with only one cell it becomes
  ! II)  W_x(jx,jy,jz) = 1/6 * pw * v_x *
  !                             ( w_old(jy,2) * (2 w_old(jz,3) +   w_new(jz,3) ) +
  !                               w_new(jy,2) * (  w_old(jz,3) + 2 w_new(jz,3) ) )
  ! Expression (I) computes d(nVx)/dt, while (II) computes directly nVx
  !
  ! See paper by Esirkepov (Comp Phys Comm 135 (2001) 144-153) and
  ! the Photon-Plasma code paper (in $code/Doc) for details
  !---------------------------------------------------------------
  ! x-number density flux
  if (g%gn(1)==1) then
    wd = wc * p(1)
    do jz=jzl,jzu
      wa = wd * (2*w_old(jz,3) +   w_new(jz,3))
      wb = wd * (  w_old(jz,3) + 2*w_new(jz,3))
      do jy=jyl,jyu
        we = wa*w_old(jy,2) + wb*w_new(jy,2)
        v(0,jy,jz,1)=v(0,jy,jz,1) + we
      enddo
    enddo
  else
    wd = wc * dxdt
    do jz=jzl,jzu
      wa = wd * (2*w_old(jz,3) +   w_new(jz,3))
      wb = wd * (  w_old(jz,3) + 2*w_new(jz,3))
      do jy=jyl,jyu
        we = wa*w_old(jy,2) + wb*w_new(jy,2)
        do jx=jxl,jxu
          v(jx,jy,jz,1)=v(jx,jy,jz,1) - (w_new(jx,1) - w_old(jx,1))*we
        enddo
      enddo
    enddo
  endif
  !---------------------------------------------------------------
  ! y-number density flux
  if (g%gn(2)==1) then
    wd = wc * p(2)
    do jx=jxl,jxu
      wa = wd * (2*w_old(jx,1) +   w_new(jx,1))
      wb = wd * (  w_old(jx,1) + 2*w_new(jx,1))
      do jz=jzl,jzu
        we = wa*w_old(jz,3) + wb*w_new(jz,3)
        v(jx,0,jz,2)=v(jx,0,jz,2) + we
      enddo
    enddo
  else
    wd = wc * dydt
    do jx=jxl,jxu
      wa = wd * (2*w_old(jx,1) +   w_new(jx,1))
      wb = wd * (  w_old(jx,1) + 2*w_new(jx,1))
      do jz=jzl,jzu
        we = wa*w_old(jz,3) + wb*w_new(jz,3)
        do jy=jyl,jyu
          v(jx,jy,jz,2)=v(jx,jy,jz,2) - (w_new(jy,2) - w_old(jy,2))*we
        enddo
      enddo
    enddo
  endif
  !---------------------------------------------------------------
  ! z-number density fluc
  if (g%gn(3)==1) then
    wd = wc * p(3)
    do jy=jyl,jyu
      wa = wd * (2*w_old(jy,2) +   w_new(jy,2))
      wb = wd * (  w_old(jy,2) + 2*w_new(jy,2))
      do jx=jxl,jxu
        we = wa*w_old(jx,1) + wb*w_new(jx,1)
        v(jx,jy,0,3)=v(jx,jy,0,3) + we
      enddo
    enddo
  else
    wd = wc * dzdt
    do jy=jyl,jyu
      wa = wd * (2*w_old(jy,2) +   w_new(jy,2))
      wb = wd * (  w_old(jy,2) + 2*w_new(jy,2))
      do jx=jxl,jxu
        we = wa*w_old(jx,1) + wb*w_new(jx,1)
        do jz=jzl,jzu
          v(jx,jy,jz,3)=v(jx,jy,jz,3) - (w_new(jz,3) - w_old(jz,3))*we
        enddo
      enddo
    enddo
  endif
END SUBROUTINE mass_conserving_deposition
! Make number density and number density flux deposition
!=======================================================================
SUBROUTINE energy_conserving_deposition(w,ws,w_old,ws_old,pw,p,ox,oy,oz)
  implicit none
  real,    intent(in), dimension(slc:suc,3) :: w, ws, w_old, ws_old
  real,    intent(in) :: pw, p(3)
  integer, intent(in) :: ox, oy, oz
  !
  real    :: wa, wb
  integer :: jy, jz, jyl, jzl, jyu, jzu
  real, dimension(slc:suc,2:3) :: wav
  !---------------------------------------------------------------
  ! density is d(jx,jy,jz) = pw * w(jx,1) * w(jy,2) * w(jz,3)
  do jz=oz,oz+1
    wa = pw * w(jz,3)
    do jy=oy,oy+1
      wb = wa * w(jy,2)
      d(  ox,jy,jz) = d(  ox,jy,jz) + w(  ox,1)*wb
      d(1+ox,jy,jz) = d(1+ox,jy,jz) + w(1+ox,1)*wb
    enddo
  enddo

  !---------------------------------------------------------------
  ! The velocity deposition is at t + dt/2, and we therefore use time averaged weights
  jyl = min(0,oy); jyu=max(1,oy+1) ! limits encompasing both non-zero old and new weights
  jzl = min(0,oz); jzu=max(1,oz+1)
  wav(jyl:jyu,2) = 0.5 * (w(jyl:jyu,2) + w_old(jyl:jyu,2))
  wav(jzl:jzu,3) = 0.5 * (w(jzl:jzu,3) + w_old(jzl:jzu,3))

  !---------------------------------------------------------------
  ! x-velocity is v_x(jx,jy,jz) = p(1) * pw * ws(jx,1) * w(jy,2) * w(jz,3)
  do jz=jzl,jzu
    wa = 0.5*p(1)*pw*wav(jz,3)
    do jy=jyl,jyu
      wb = wa*wav(jy,2)
      v(-1,jy,jz,1) = v(-1,jy,jz,1) + (ws(-1,1)   + ws_old(-1,1))*wb
      v( 0,jy,jz,1) = v( 0,jy,jz,1) + (ws( 0,1)   + ws_old( 0,1))*wb
      v( 1,jy,jz,1) = v( 1,jy,jz,1) + (ws( 1,1)   + ws_old( 1,1))*wb
    enddo
  enddo

  !---------------------------------------------------------------
  ! y-velocity is vy(jx,jy,jz) = p(2) * pw * w(jx,1) * ws(jy,2) * w(jz,3)
  do jz=jzl,jzu
    wa = 0.25*p(2)*pw*wav(jz,3)
    do jy=-1,1
      wb = (ws(jy,2) + ws_old(jy,2))*wa
      v(   0,jy,jz,2) = v(   0,jy,jz,2) + w_old(0,1)*wb
      v(   1,jy,jz,2) = v(   1,jy,jz,2) + w_old(1,1)*wb
      v(  ox,jy,jz,2) = v(  ox,jy,jz,2) + w(  ox,1)*wb
      v(1+ox,jy,jz,2) = v(1+ox,jy,jz,2) + w(1+ox,1)*wb
    enddo
  enddo

  !---------------------------------------------------------------
  ! z-velocity is vz(jx,jy,jz) = p(3) * pw * w(jx,1) * w(jy,2) * ws(jz,3)
  do jz=-1,1
    wa = 0.25*p(3)*pw*(ws(jz,3) + ws_old(jz,3))
    do jy=jyl,jyu
      wb = wav(jy,2)*wa
      v(   0,jy,jz,3) = v(   0,jy,jz,3) + w_old(0,1)*wb
      v(   1,jy,jz,3) = v(   1,jy,jz,3) + w_old(1,1)*wb
      v(  ox,jy,jz,3) = v(  ox,jy,jz,3) + w(  ox,1)*wb
      v(1+ox,jy,jz,3) = v(1+ox,jy,jz,3) + w(1+ox,1)*wb
    enddo
  enddo
END SUBROUTINE energy_conserving_deposition
! Cache global EM-fields to local arrays. If touched_row is true,
! we only need to read in the last row. The rest can be rolled up.
!=======================================================================
SUBROUTINE cache_em_fields(ix, iy, iz, touched_row, Ex, Ey, Ez, Bx, By, Bz)
  USE grid_m, only : g
  implicit none
  integer, intent(in) :: ix, iy, iz
  logical             :: touched_row
  real,    intent(in), dimension(g%n(1),g%n(2),g%n(3)) :: Ex, Ey, Ez, Bx, By, Bz
  !
  integer :: jx, jy, jz, kx, ky, kz
  ! copy fields over, taking care of boundaries and reusing rows in a rolling cache if touched_row=.true.
  !---------------------------------------------------------------------
  do jz=slc,sub
    kz = jz + iz
    if (kz > 0 .and. kz <= g%n(3)) then
      do jy=slc,sub
        ky = jy + iy
        if (ky > 0 .and. ky <= g%n(2)) then
          do jx=slc,sub
            kx = jx + ix
            if (kx > 0 .and. kx <= g%n(1)) then
              if (touched_row) then
                if (jx==slc) then
                  if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = sEx(jx+1,jy,jz)
                  if (jy > slc) sBy(jx,jy,jz) = sBy(jx+1,jy,jz)
                  if (jz > slc) sBz(jx,jy,jz) = sBz(jx+1,jy,jz)
                else if (jx < sub) then
                  if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = sEx(jx+1,jy,jz)
                  if (jz > slc) sEy(jx,jy,jz) = sEy(jx+1,jy,jz)
                  if (jy > slc) sEz(jx,jy,jz) = sEz(jx+1,jy,jz)
                  sBx(jx,jy,jz) = sBx(jx+1,jy,jz)
                  if (jy > slc) sBy(jx,jy,jz) = sBy(jx+1,jy,jz)
                  if (jz > slc) sBz(jx,jy,jz) = sBz(jx+1,jy,jz)
                else !if (jx==sub) then ! "if" not needed, this is the last option
                  if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = Ex(kx,ky,kz)
                  if (jz > slc) sEy(jx,jy,jz) = Ey(kx,ky,kz)
                  if (jy > slc) sEz(jx,jy,jz) = Ez(kx,ky,kz)
                  sBx(jx,jy,jz) = Bx(kx,ky,kz)
                  if (jy > slc) sBy(jx,jy,jz) = By(kx,ky,kz)
                  if (jz > slc) sBz(jx,jy,jz) = Bz(kx,ky,kz)
                endif
              else
                if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = Ex(kx,ky,kz)
                if (jx > slc .and. jz > slc) sEy(jx,jy,jz) = Ey(kx,ky,kz)
                if (jx > slc .and. jy > slc) sEz(jx,jy,jz) = Ez(kx,ky,kz)
                if (jx > slc) sBx(jx,jy,jz) = Bx(kx,ky,kz)
                if (jy > slc) sBy(jx,jy,jz) = By(kx,ky,kz)
                if (jz > slc) sBz(jx,jy,jz) = Bz(kx,ky,kz)
              endif
            else
              if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = 0.
              if (jx > slc .and. jz > slc) sEy(jx,jy,jz) = 0.
              if (jx > slc .and. jy > slc) sEz(jx,jy,jz) = 0.
              if (jx > slc) sBx(jx,jy,jz) = 0.
              if (jy > slc) sBy(jx,jy,jz) = 0.
              if (jz > slc) sBz(jx,jy,jz) = 0.
            endif
          enddo
        else
          do jx=slc,sub
            if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = 0.
            if (jx > slc .and. jz > slc) sEy(jx,jy,jz) = 0.
            if (jx > slc .and. jy > slc) sEz(jx,jy,jz) = 0.
            if (jx > slc) sBx(jx,jy,jz) = 0.
            if (jy > slc) sBy(jx,jy,jz) = 0.
            if (jz > slc) sBz(jx,jy,jz) = 0.
          enddo
        endif
      enddo
    else
      do jy=slc,sub
        do jx=slc,sub
          if (jy > slc .and. jz > slc) sEx(jx,jy,jz) = 0.
          if (jx > slc .and. jz > slc) sEy(jx,jy,jz) = 0.
          if (jx > slc .and. jy > slc) sEz(jx,jy,jz) = 0.
          if (jx > slc) sBx(jx,jy,jz) = 0.
          if (jy > slc) sBy(jx,jy,jz) = 0.
          if (jz > slc) sBz(jx,jy,jz) = 0.
        enddo
      enddo
    endif
  enddo
  touched_row = .true.
END SUBROUTINE cache_em_fields
! Given weights, find the E- and B-fields interpolated at the particle position.
!-----------------------------------------------------------------------
SUBROUTINE interpolate_fields(w,ws,E,B)
  implicit none
  real, intent(in), dimension(slc:suc,3) :: w, ws                       ! Weights for centered and staggered vertices
  real, intent(out) :: E(3), B(3)
!-----------------------------------
! X SHIFTED WEIGHTS
E(1) = &
   ((sEx(-1, 0, 0)*ws(-1,1) + &
     sEx( 0, 0, 0)*ws( 0,1) + &
     sEx(+1, 0, 0)*ws( 1,1))*w( 0,2) + &
    (sEx(-1,+1, 0)*ws(-1,1) + &
     sEx( 0,+1, 0)*ws( 0,1) + &
     sEx(+1,+1, 0)*ws( 1,1))*w( 1,2))*w( 0,3) + &
   ((sEx(-1, 0,+1)*ws(-1,1) + &
     sEx( 0, 0,+1)*ws( 0,1) + &
     sEx(+1, 0,+1)*ws( 1,1))*w( 0,2) + &
    (sEx(-1,+1,+1)*ws(-1,1) + &
     sEx( 0,+1,+1)*ws( 0,1) + &
     sEx(+1,+1,+1)*ws( 1,1))*w( 1,2))*w( 1,3)
!-----------------------------------
! Y SHIFTED WEIGHTS
E(2) = &
  ((sEy( 0,-1, 0)*w( 0,1) + &
    sEy(+1,-1, 0)*w( 1,1))*ws(-1,2) + &
   (sEy( 0, 0, 0)*w( 0,1) + &
    sEy(+1, 0, 0)*w( 1,1))*ws( 0,2) + &
   (sEy( 0,+1, 0)*w( 0,1) + &
    sEy(+1,+1, 0)*w( 1,1))*ws( 1,2))*w( 0,3) + &
  ((sEy( 0,-1,+1)*w( 0,1) + &
    sEy(+1,-1,+1)*w( 1,1))*ws(-1,2) + &
   (sEy( 0, 0,+1)*w( 0,1) + &
    sEy(+1, 0,+1)*w( 1,1))*ws( 0,2) + &
   (sEy( 0,+1,+1)*w( 0,1) + &
    sEy(+1,+1,+1)*w( 1,1))*ws( 1,2))*w( 1,3)
!-----------------------------------
! Z SHIFTED WEIGHTS
E(3) = &
 ((sEz( 0, 0,-1)*w( 0,1) + &
   sEz(+1, 0,-1)*w( 1,1))*w( 0,2) + &
  (sEz( 0,+1,-1)*w( 0,1) + &
   sEz(+1,+1,-1)*w( 1,1))*w( 1,2))*ws(-1,3) + &
 ((sEz( 0, 0, 0)*w( 0,1) + &
   sEz(+1, 0, 0)*w( 1,1))*w( 0,2) + &
  (sEz( 0,+1, 0)*w( 0,1) + &
   sEz(+1,+1, 0)*w( 1,1))*w( 1,2))*ws( 0,3) + &
 ((sEz( 0, 0,+1)*w( 0,1) + &
   sEz(+1, 0,+1)*w( 1,1))*w( 0,2) + &
  (sEz( 0,+1,+1)*w( 0,1) + &
   sEz(+1,+1,+1)*w( 1,1))*w( 1,2))*ws( 1,3)
!-----------------------------------
! YZ SHIFTED WEIGHTS
B(1) = &
 ((sBx( 0,-1,-1)*w( 0,1) + &
   sBx(+1,-1,-1)*w( 1,1))*ws(-1,2) + &
  (sBx( 0, 0,-1)*w( 0,1) + &
   sBx(+1, 0,-1)*w( 1,1))*ws( 0,2) + &
  (sBx( 0,+1,-1)*w( 0,1) + &
   sBx(+1,+1,-1)*w( 1,1))*ws( 1,2))*ws(-1,3) + &
 ((sBx( 0,-1, 0)*w( 0,1) + &
   sBx(+1,-1, 0)*w( 1,1))*ws(-1,2) + &
  (sBx( 0, 0, 0)*w( 0,1) + &
   sBx(+1, 0, 0)*w( 1,1))*ws( 0,2) + &
  (sBx( 0,+1, 0)*w( 0,1) + &
   sBx(+1,+1, 0)*w( 1,1))*ws( 1,2))*ws( 0,3) + &
 ((sBx( 0,-1,+1)*w( 0,1) + &
   sBx(+1,-1,+1)*w( 1,1))*ws(-1,2) + &
  (sBx( 0, 0,+1)*w( 0,1) + &
   sBx(+1, 0,+1)*w( 1,1))*ws( 0,2) + &
  (sBx( 0,+1,+1)*w( 0,1) + &
   sBx(+1,+1,+1)*w( 1,1))*ws( 1,2))*ws( 1,3)
!-----------------------------------
! ZX SHIFTED WEIGHTS
B(2) = &
 ((sBy(-1, 0,-1)*ws(-1,1) + &
   sBy( 0, 0,-1)*ws( 0,1) + &
   sBy(+1, 0,-1)*ws( 1,1))*w( 0,2) + &
  (sBy(-1,+1,-1)*ws(-1,1) + &
   sBy( 0,+1,-1)*ws( 0,1) + &
   sBy(+1,+1,-1)*ws( 1,1))*w( 1,2))*ws(-1,3) + &
 ((sBy(-1, 0, 0)*ws(-1,1) + &
   sBy( 0, 0, 0)*ws( 0,1) + &
   sBy(+1, 0, 0)*ws( 1,1))*w( 0,2) + &
  (sBy(-1,+1, 0)*ws(-1,1) + &
   sBy( 0,+1, 0)*ws( 0,1) + &
   sBy(+1,+1, 0)*ws( 1,1))*w( 1,2))*ws( 0,3) + &
 ((sBy(-1, 0,+1)*ws(-1,1) + &
   sBy( 0, 0,+1)*ws( 0,1) + &
   sBy(+1, 0,+1)*ws( 1,1))*w( 0,2) + &
  (sBy(-1,+1,+1)*ws(-1,1) + &
   sBy( 0,+1,+1)*ws( 0,1) + &
   sBy(+1,+1,+1)*ws( 1,1))*w( 1,2))*ws( 1,3)
!-----------------------------------
! XY SHIFTED WEIGHTS
B(3) = &
 ((sBz(-1,-1, 0)*ws(-1,1) + &
   sBz( 0,-1, 0)*ws( 0,1) + &
   sBz(+1,-1, 0)*ws( 1,1))*ws(-1,2) + &
  (sBz(-1, 0, 0)*ws(-1,1) + &
   sBz( 0, 0, 0)*ws( 0,1) + &
   sBz(+1, 0, 0)*ws( 1,1))*ws( 0,2) + &
  (sBz(-1,+1, 0)*ws(-1,1) + &
   sBz( 0,+1, 0)*ws( 0,1) + &
   sBz(+1,+1, 0)*ws( 1,1))*ws( 1,2))*w( 0,3) + &
 ((sBz(-1,-1,+1)*ws(-1,1) + &
   sBz( 0,-1,+1)*ws( 0,1) + &
   sBz(+1,-1,+1)*ws( 1,1))*ws(-1,2) + &
  (sBz(-1, 0,+1)*ws(-1,1) + &
   sBz( 0, 0,+1)*ws( 0,1) + &
   sBz(+1, 0,+1)*ws( 1,1))*ws( 0,2) + &
  (sBz(-1,+1,+1)*ws(-1,1) + &
   sBz( 0,+1,+1)*ws( 0,1) + &
   sBz(+1,+1,+1)*ws( 1,1))*ws( 1,2))*w( 1,3)
END SUBROUTINE interpolate_fields
!-----------------------------------------------------------------------
END MODULE interpolation_cached
!=======================================================================
MODULE interpolation
  implicit none
  real                                   :: xx,yy,zz,xx1,yy1,zz1        ! float indices
  real                                   :: px ,py ,pz ,qx ,qy ,qz      ! float fractions, centered
  real                                   :: px1,py1,pz1,qx1,qy1,qz1     ! float fractions, staggered
  integer                                :: ixe,iye,ize,ixe1,iye1,ize1  ! integer indices  
  logical debug
END MODULE interpolation
!-----------------------------------------------------------------------
SUBROUTINE makeweights (r, q)
  USE params, only:mdim, mcoord, stdout, stdall, rank, nodes, periodic, &
                   do_check_interpolate
  USE grid_m, only:g,odx,ody,odz,nx,ny,nz
  USE interpolation
  implicit none
  real   , dimension(mdim), intent(in) :: r                             ! fraction position
  integer, dimension(mdim), intent(in) :: q                             ! integer position
  real   , dimension(mdim)             :: p, p1                         ! fraction position
  integer, dimension(mdim)             :: i, ii, ii1                    ! integer position
  logical err
!.......................................................................
  where (periodic) 
    ii = modulo(q,g%gn) + g%lb                                           ! wrap
  else where
    ii = q + g%lb
  end where

  p = r
  i = floor(r+0.5)
  p1  = r+0.5-i
  ii1 = ii-1+i

  ixe = ii(1); px=p(1); qx=1.-px; ixe1=ii1(1); px1=p1(1); qx1=1.-px1
  iye = ii(2); py=p(2); qy=1.-py; iye1=ii1(2); py1=p1(2); qy1=1.-py1
  ize = ii(3); pz=p(3); qz=1.-pz; ize1=ii1(3); pz1=p1(3); qz1=1.-pz1
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE scatter_fields (r,q,fx,fy,fz,fp1,fyz,fzx,fxy,fp2)
  USE params, only:mdim, mcoord, stdout, stdall, rank, nodes, master, &
                   do_check_interpolate
  USE grid_m, only:g,odx,ody,odz,nx,ny,nz
  USE interpolation
  implicit none
  real   , dimension(mdim), intent(in)   :: r                           ! fraction position
  integer, dimension(mdim), intent(in)   :: q                           ! integer position
  real, dimension(nx,ny,nz), intent(in)  :: fx,fy,fz,fxy,fyz,fzx        ! input fields
  real, dimension(mcoord)                :: fp1,fp2                     ! output vectors
!.......................................................................
  call makeweights (r, q)

  fp1(1) = qz *(qy *(qx1*fx (ixe1,iye   ,ize   ) + px1*fx (ixe1+1,iye   ,ize   ))  & ! x-staggered
              + py *(qx1*fx (ixe1,iye +1,ize   ) + px1*fx (ixe1+1,iye +1,ize   ))) &
         + pz *(qy *(qx1*fx (ixe1,iye   ,ize +1) + px1*fx (ixe1+1,iye   ,ize +1))  &
              + py *(qx1*fx (ixe1,iye +1,ize +1) + px1*fx (ixe1+1,iye +1,ize +1)))

  fp1(2) = qz *(qy1*(qx *fy (ixe ,iye1  ,ize   ) + px *fy (ixe +1,iye1  ,ize   ))  & ! y-staggered
              + py1*(qx *fy (ixe ,iye1+1,ize   ) + px *fy (ixe +1,iye1+1,ize   ))) &
         + pz *(qy1*(qx *fy (ixe ,iye1  ,ize +1) + px *fy (ixe +1,iye1  ,ize +1))  &
              + py1*(qx *fy (ixe ,iye1+1,ize +1) + px *fy (ixe +1,iye1+1,ize +1)))

  fp1(3) = qz1*(qy *(qx *fz (ixe ,iye   ,ize1  ) + px *fz (ixe +1,iye   ,ize1  ))  & ! z-staggered
              + py *(qx *fz (ixe ,iye +1,ize1  ) + px *fz (ixe +1,iye +1,ize1  ))) &
         + pz1*(qy *(qx *fz (ixe ,iye   ,ize1+1) + px *fz (ixe +1,iye   ,ize1+1))  &
              + py *(qx *fz (ixe ,iye +1,ize1+1) + px *fz (ixe +1,iye +1,ize1+1)))

  fp2(1) = qz1*(qy1*(qx *fyz(ixe ,iye1  ,ize1  ) + px *fyz(ixe +1,iye1  ,ize1  ))  & ! yz-staggered
              + py1*(qx *fyz(ixe ,iye1+1,ize1  ) + px *fyz(ixe +1,iye1+1,ize1  ))) &
         + pz1*(qy1*(qx *fyz(ixe ,iye1  ,ize1+1) + px *fyz(ixe +1,iye1  ,ize1+1))  &
              + py1*(qx *fyz(ixe ,iye1+1,ize1+1) + px *fyz(ixe +1,iye1+1,ize1+1)))

  fp2(2) = qz1*(qy *(qx1*fzx(ixe1,iye   ,ize1  ) + px1*fzx(ixe1+1,iye   ,ize1  ))  & ! zx-staggered
              + py *(qx1*fzx(ixe1,iye +1,ize1  ) + px1*fzx(ixe1+1,iye +1,ize1  ))) &
         + pz1*(qy *(qx1*fzx(ixe1,iye   ,ize1+1) + px1*fzx(ixe1+1,iye   ,ize1+1))  &
              + py *(qx1*fzx(ixe1,iye +1,ize1+1) + px1*fzx(ixe1+1,iye +1,ize1+1)))

  fp2(3) = qz *(qy1*(qx1*fxy(ixe1,iye1  ,ize   ) + px1*fxy(ixe1+1,iye1  ,ize   ))  & ! xy-staggered
              + py1*(qx1*fxy(ixe1,iye1+1,ize   ) + px1*fxy(ixe1+1,iye1+1,ize   ))) &
         + pz *(qy1*(qx1*fxy(ixe1,iye1  ,ize +1) + px1*fxy(ixe1+1,iye1  ,ize +1))  &
              + py1*(qx1*fxy(ixe1,iye1+1,ize +1) + px1*fxy(ixe1+1,iye1+1,ize +1)))
  if (debug) print'(70x,a,6f10.3)','fp1,fp2=',fp1,fp2
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE gather(r,q,f,fp,k,w)
  USE params,  only: mdim, mcoord, nspecies, stdout
  USE species, only: field
  USE grid_m,    only: odx,ody,odz,nx,ny,nz,g
  USE interpolation
  implicit none
  real   , dimension(mdim), intent(in)      :: r                        ! fraction position
  integer, dimension(mdim), intent(in)      :: q                        ! integer position
  type(field), dimension(nx,ny,nz,nspecies) :: f                        ! receiving fields
  real, dimension(mcoord)                   :: fp                       ! velocity
  integer,                    intent(in)    :: k                        ! species index
  real,                       intent(in)    :: w                        ! weight
!.......................................................................
  call makeweights (r, q)
!-----------------------------------------------------------------------
  f(ixe  ,iye  ,ize  ,k)%d = f(ixe  ,iye  ,ize  ,k)%d + w*qx*qy*qz
  f(ixe+1,iye  ,ize  ,k)%d = f(ixe+1,iye  ,ize  ,k)%d + w*px*qy*qz
  f(ixe  ,iye+1,ize  ,k)%d = f(ixe  ,iye+1,ize  ,k)%d + w*qx*py*qz
  f(ixe+1,iye+1,ize  ,k)%d = f(ixe+1,iye+1,ize  ,k)%d + w*px*py*qz
  f(ixe  ,iye  ,ize+1,k)%d = f(ixe  ,iye  ,ize+1,k)%d + w*qx*qy*pz
  f(ixe+1,iye  ,ize+1,k)%d = f(ixe+1,iye  ,ize+1,k)%d + w*px*qy*pz
  f(ixe  ,iye+1,ize+1,k)%d = f(ixe  ,iye+1,ize+1,k)%d + w*qx*py*pz
  f(ixe+1,iye+1,ize+1,k)%d = f(ixe+1,iye+1,ize+1,k)%d + w*px*py*pz

  !-----------------------------------------------
  ! DISTRIBUTE PARTICLE X-FLUX
  f(ixe1  ,iye  ,ize  ,k)%v(1) = f(ixe1  ,iye  ,ize  ,k)%v(1) + w*qx1*qy*qz*fp(1)
  f(ixe1+1,iye  ,ize  ,k)%v(1) = f(ixe1+1,iye  ,ize  ,k)%v(1) + w*px1*qy*qz*fp(1)
  f(ixe1  ,iye+1,ize  ,k)%v(1) = f(ixe1  ,iye+1,ize  ,k)%v(1) + w*qx1*py*qz*fp(1)
  f(ixe1+1,iye+1,ize  ,k)%v(1) = f(ixe1+1,iye+1,ize  ,k)%v(1) + w*px1*py*qz*fp(1)
  f(ixe1  ,iye  ,ize+1,k)%v(1) = f(ixe1  ,iye  ,ize+1,k)%v(1) + w*qx1*qy*pz*fp(1)
  f(ixe1+1,iye  ,ize+1,k)%v(1) = f(ixe1+1,iye  ,ize+1,k)%v(1) + w*px1*qy*pz*fp(1)
  f(ixe1  ,iye+1,ize+1,k)%v(1) = f(ixe1  ,iye+1,ize+1,k)%v(1) + w*qx1*py*pz*fp(1)
  f(ixe1+1,iye+1,ize+1,k)%v(1) = f(ixe1+1,iye+1,ize+1,k)%v(1) + w*px1*py*pz*fp(1)

  ! -----------------------------------------------
  ! DISTRIBUTE PARTICLE Y-FLUX
  f(ixe  ,iye1  ,ize  ,k)%v(2) = f(ixe  ,iye1  ,ize  ,k)%v(2) + w*qx*qy1*qz*fp(2)
  f(ixe+1,iye1  ,ize  ,k)%v(2) = f(ixe+1,iye1  ,ize  ,k)%v(2) + w*px*qy1*qz*fp(2)
  f(ixe  ,iye1+1,ize  ,k)%v(2) = f(ixe  ,iye1+1,ize  ,k)%v(2) + w*qx*py1*qz*fp(2)
  f(ixe+1,iye1+1,ize  ,k)%v(2) = f(ixe+1,iye1+1,ize  ,k)%v(2) + w*px*py1*qz*fp(2)
  f(ixe  ,iye1  ,ize+1,k)%v(2) = f(ixe  ,iye1  ,ize+1,k)%v(2) + w*qx*qy1*pz*fp(2)
  f(ixe+1,iye1  ,ize+1,k)%v(2) = f(ixe+1,iye1  ,ize+1,k)%v(2) + w*px*qy1*pz*fp(2)
  f(ixe  ,iye1+1,ize+1,k)%v(2) = f(ixe  ,iye1+1,ize+1,k)%v(2) + w*qx*py1*pz*fp(2)
  f(ixe+1,iye1+1,ize+1,k)%v(2) = f(ixe+1,iye1+1,ize+1,k)%v(2) + w*px*py1*pz*fp(2)

  ! -----------------------------------------------
  ! DISTRIBUTE PARTICLE Z-FLUX
  f(ixe  ,iye  ,ize1  ,k)%v(3) = f(ixe  ,iye  ,ize1  ,k)%v(3) + w*qx*qy*qz1*fp(3)
  f(ixe+1,iye  ,ize1  ,k)%v(3) = f(ixe+1,iye  ,ize1  ,k)%v(3) + w*px*qy*qz1*fp(3)
  f(ixe  ,iye+1,ize1  ,k)%v(3) = f(ixe  ,iye+1,ize1  ,k)%v(3) + w*qx*py*qz1*fp(3)
  f(ixe+1,iye+1,ize1  ,k)%v(3) = f(ixe+1,iye+1,ize1  ,k)%v(3) + w*px*py*qz1*fp(3)
  f(ixe  ,iye  ,ize1+1,k)%v(3) = f(ixe  ,iye  ,ize1+1,k)%v(3) + w*qx*qy*pz1*fp(3)
  f(ixe+1,iye  ,ize1+1,k)%v(3) = f(ixe+1,iye  ,ize1+1,k)%v(3) + w*px*qy*pz1*fp(3)
  f(ixe  ,iye+1,ize1+1,k)%v(3) = f(ixe  ,iye+1,ize1+1,k)%v(3) + w*qx*py*pz1*fp(3)
  f(ixe+1,iye+1,ize1+1,k)%v(3) = f(ixe+1,iye+1,ize1+1,k)%v(3) + w*px*py*pz1*fp(3)
END SUBROUTINE

!-----------------------------------------------------------------------
! The boundaries zones needed by this method
!-----------------------------------------------------------------------
SUBROUTINE interpolation_boundaries
  USE params, only: mid
  USE grid_m, only: g
  implicit none
  character(len=mid), save:: id = &
    '$Id$'
!.......................................................................
  call print_id (id)
  g%lb = 2
  g%ub = 0
END SUBROUTINE
