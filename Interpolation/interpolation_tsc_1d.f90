! $Id$
! vim: nowrap
! Routines for inlined interpolation and particle operations
!=======================================================================
MODULE interpolation_cached
  implicit none
  integer, parameter :: slc=-2, slb=-1, sub=1, suc=2
  real, dimension(slb:sub) :: sEx                       ! Cached E-field
  real, dimension(slb:suc) :: sEy
  real, dimension(slb:suc) :: sEz
  real, dimension(slb:suc) :: sBx                       ! Cached B-field
  real, dimension(slb:sub) :: sBy
  real, dimension(slb:sub) :: sBz
  real, dimension(slc:suc) :: d                         ! Cached number density
  real, dimension(slc:suc,3) :: v                       ! Cached number density flux
  real, dimension(slc:suc,1) :: w_old, ws_old, w_new, ws_new ! Interpolation weights
  real, parameter :: one_sixth=1./6.
CONTAINS
!=======================================================================
! Calculate weights for centered quantities
!-----------------------------------------------------------------------
SUBROUTINE make_centered_weights(r,w)
  implicit none
  real, dimension(3), intent(in) :: r
  real, dimension(slc:suc,1)     :: w
  real :: s, wl, wc, wu
  logical :: up
  !-------------
  ! X_COORD CENTERED
  s = r(1) - floor(r(1) + 0.5); up = s < 0
  wc = (.75 - s*s); s = 1.5 - abs(s-1.); wu = .5*s*s; wl = 1. - (wu+wc)
  w(-1,1) = merge(0., wl, up)
  w( 0,1) = merge(wl, wc, up)
  w( 1,1) = merge(wc, wu, up)
  w( 2,1) = merge(wu, 0., up)
END SUBROUTINE make_centered_weights
! Calculate weights for staggered quantities
!-----------------------------------------------------------------------
SUBROUTINE make_staggered_weights(r,w)
  implicit none
  real, dimension(3), intent(in) :: r
  real, dimension(slc:suc,1)     :: w
  real :: s
  !-------------
  ! X_COORD SHIFTED IN X
  s = 0.5 - r(1)
  w( 0,1) = .75 - s*s                                                  ! for |xs| =< 1/2
  s = 1.0 - r(1)
  w(-1,1) = .5*s*s                                                     ! 1/2 =< |xs| =< 3/2
  w( 1,1) = 1. - (w( 0,1) + w(-1,1))
END SUBROUTINE make_staggered_weights
! Calculate weights for centered quantities, after particle has moved
!-----------------------------------------------------------------------
SUBROUTINE make_centered_weights_updated(r,ox,oy,oz,w)
  implicit none
  real, dimension(3), intent(in) :: r
  integer,            intent(in) :: ox, oy, oz
  real, dimension(slc:suc,1)     :: w
  !
  real :: s, wl, wc, wu
  logical :: up
  ! ---------------------------------------------------------
  ! Find signed distance to center points in all directions.
  ! Project index into allowed range for interpolation.
  !-------------
  ! X_COORD CENTERED
  if (abs(r(1) + ox - 0.5) > 1.) print *, 'Out-of-bounds :', r(1), ox
  s = r(1) - floor(r(1) + 0.5); up = (r(1) + ox) > 0.5
  wc = (.75 - s*s); s = 1.5 - abs(s-1.); wu = .5*s*s; wl = 1. - (wu+wc)
  w(-1,1) = merge(0., wl, up)
  w( 0,1) = merge(wl, wc, up)
  w( 1,1) = merge(wc, wu, up)
  w( 2,1) = merge(wu, 0., up)

END SUBROUTINE make_centered_weights_updated
! Calculate weights for centered quantities, after particle has moved
!-----------------------------------------------------------------------
SUBROUTINE make_staggered_weights_updated(r,ox,oy,oz,w)
  implicit none
  real, dimension(3), intent(in) :: r
  integer,            intent(in) :: ox, oy, oz
  real, dimension(slc:suc,1)     :: w
  !
  real :: s
  ! ---------------------------------------------------------
  ! Find signed distance to center points in all directions.
  ! Project index into allowed range for interpolation.
  ! X_COORD STAGGERED
  s = 0.5 - r(1)
  w( 0+ox,1) = .75 - s*s                                               ! for |xs| =< 1/2
  s = 1.0 - r(1)
  w(-1+ox,1) = .5*s*s                                                  ! 1/2 =< |xs| =< 3/2
  w( 1+ox,1) = 1. - (w( 0+ox,1) + w(-1+ox,1))
END SUBROUTINE make_staggered_weights_updated
! Make a mass conserving number density and number density flux deposition
!=======================================================================
SUBROUTINE mass_conserving_deposition(w_old,w_new,pw,p,ox,oy,oz,dxdt,dydt,dzdt)
  USE grid_m, only : g
  implicit none
  real,    intent(in), dimension(slc:suc,1) :: w_old, w_new
  real,    intent(in) :: pw, p(3), dxdt, dydt, dzdt
  integer, intent(in) :: ox, oy, oz
  !
  real    :: wc, wd, we                                                 ! Scratch weights
  integer :: jxu,jxl,jx
  !---------------------------------------------------------------
  ! density is d(jx,jy,jz) = pw * w_new(jx,1)
  d(-1) = d(-1) + w_new(-1,1)*pw
  d( 0) = d( 0) + w_new( 0,1)*pw
  d( 1) = d( 1) + w_new( 1,1)*pw
  d( 2) = d( 2) + w_new( 2,1)*pw
  !---------------------------------------------------------------
  ! Number density fluxes:
  jxl = -1; jxu=2                                                 ! Loop bounds
  wc = 3. * one_sixth * pw                                        ! particle weight / 6
  ! The weights for number density fluxes, in directions with more than one cell, are (for x-dir)
  ! I)   W_x(jx,jy,jz) = 1/6 * pw * dx/dt * (w_new(jx,3)-w_new(jx,3)) *
  !                             ( w_old(jy,2) * (2 w_old(jz,3) +   w_new(jz,3) ) +
  !                               w_new(jy,2) * (  w_old(jz,3) + 2 w_new(jz,3) ) )
  ! For directions with only one cell it becomes
  ! II)  W_x(jx,jy,jz) = 1/6 * pw * v_x *
  !                             ( w_old(jy,2) * (2 w_old(jz,3) +   w_new(jz,3) ) +
  !                               w_new(jy,2) * (  w_old(jz,3) + 2 w_new(jz,3) ) )
  ! Expression (I) computes d(nVx)/dt, while (II) computes directly nVx
  !
  ! See paper by Esirkepov (Comp Phys Comm 135 (2001) 144-153) and
  ! the Photon-Plasma code paper (in $code/Doc) for details
  !---------------------------------------------------------------
  ! x-number density flux
  wd = 2. * wc * dxdt
  do jx=jxl,jxu
    v(jx,1)=v(jx,1) - (w_new(jx,1) - w_old(jx,1))*wd
  enddo
  !---------------------------------------------------------------
  ! y- and z-number density flux
  wd = wc * p(2)
  we = wc * p(3)
  do jx=jxl,jxu
    v(jx,2)=v(jx,2) + wd * (w_old(jx,1) + w_new(jx,1))
    v(jx,3)=v(jx,3) + we * (w_old(jx,1) + w_new(jx,1))
  enddo
END SUBROUTINE mass_conserving_deposition
! Make number density and number density flux deposition
!=======================================================================
SUBROUTINE energy_conserving_deposition(w,ws,w_old,ws_old,pw,p,ox,oy,oz)
  implicit none
  real,    intent(in), dimension(slc:suc,1) :: w, ws, w_old, ws_old
  real,    intent(in) :: pw, p(3)
  integer, intent(in) :: ox, oy, oz
  !
  integer :: jxl, jxu
  real    :: wa, wav(-1:2)
  !---------------------------------------------------------------
  ! density is d(jx,jy,jz) = pw * w(jx,1) * w(jy,2) * w(jz,3)
  d(-1) = d(-1) + w(-1,1)*pw
  d( 0) = d( 0) + w( 0,1)*pw
  d( 1) = d( 1) + w( 1,1)*pw
  d( 2) = d( 2) + w( 2,1)*pw

  !---------------------------------------------------------------
  ! The velocity deposition is at t + dt/2, and we therefore use time averaged weights
  jxl = min(-1,ox-1); jxu=max(1,ox+1) ! limits encompasing both old and new non-zero weights
  wav = 0.5 * (w(-1:2,1) + w_old(-1:2,1))

  !---------------------------------------------------------------
  ! x-velocity is v_x(jx,jy,jz) = p(1) * pw * ws(jx,1)
  wa = 0.5*p(1)*pw
  v(jxl:jxu,1) = v(jxl:jxu,1) + (ws(jxl:jxu,1) + ws_old(jxl:jxu,1))*wa

  !---------------------------------------------------------------
  ! y-velocity is vy(jx,jy,jz) = p(2) * pw * w(jx,1)
  wa = p(2)*pw
  v(-1,2) = v(-1,2) + wav(-1)*wa
  v( 0,2) = v( 0,2) + wav( 0)*wa
  v( 1,2) = v( 1,2) + wav( 1)*wa
  v( 2,2) = v( 2,2) + wav( 2)*wa

  !---------------------------------------------------------------
  ! z-velocity is vz(jx) = p(3) * pw * w(jx,1)
  wa = p(3)*pw
  v(-1,3) = v(-1,3) + wav(-1)*wa
  v( 0,3) = v( 0,3) + wav( 0)*wa
  v( 1,3) = v( 1,3) + wav( 1)*wa
  v( 2,3) = v( 2,3) + wav( 2)*wa
END SUBROUTINE energy_conserving_deposition
! Cache global EM-fields to local arrays. If touched_row is true,
! we only need to read in the last row. The rest can be rolled up.
!=======================================================================
SUBROUTINE cache_em_fields(ix, iy, iz, touched_row, Ex, Ey, Ez, Bx, By, Bz)
  USE grid_m, only : g
  implicit none
  integer, intent(in) :: ix, iy, iz
  logical             :: touched_row
  real,    intent(in), dimension(g%n(1),g%n(2),g%n(3)) :: Ex, Ey, Ez, Bx, By, Bz
  !
  integer :: jx, kx
  ! copy fields over, taking care of boundaries and reusing rows in a rolling cache if touched_row=.true.
  !---------------------------------------------------------------------
  do jx=slb,suc
    kx = jx + ix
    if (kx > 0 .and. kx <= g%n(1)) then
      if (touched_row) then
        if (jx < sub) then
          sEx(jx) = sEx(jx+1)
          sEy(jx) = sEy(jx+1)
          sEz(jx) = sEz(jx+1)
          sBx(jx) = sBx(jx+1)
          sBy(jx) = sBy(jx+1)
          sBz(jx) = sBz(jx+1)
        else if (jx==sub) then
          sEx(jx) = Ex(kx,iy,iz)
          sEy(jx) = sEy(jx+1)
          sEz(jx) = sEz(jx+1)
          sBx(jx) = sBx(jx+1)
          sBy(jx) = By(kx,iy,iz)
          sBz(jx) = Bz(kx,iy,iz)
        else
          sEy(jx) = Ey(kx,iy,iz)
          sEz(jx) = Ez(kx,iy,iz)
          sBx(jx) = Bx(kx,iy,iz)
        endif
      else
        if (jx < suc) sEx(jx) = Ex(kx,iy,iz)
        sEy(jx) = Ey(kx,iy,iz)
        sEz(jx) = Ez(kx,iy,iz)
        sBx(jx) = Bx(kx,iy,iz)
        if (jx < suc) sBy(jx) = By(kx,iy,iz)
        if (jx < suc) sBz(jx) = Bz(kx,iy,iz)
      endif
    else
      if (jx < suc) sEx(jx) = 0.
      sEy(jx) = 0.
      sEz(jx) = 0.
      sBx(jx) = 0.
      if (jx < suc) sBy(jx) = 0.
      if (jx < suc) sBz(jx) = 0.
    endif
  enddo
  touched_row=.true.
END SUBROUTINE cache_em_fields
! Given weights, find the E- and B-fields interpolated at the particle position.
!-----------------------------------------------------------------------
SUBROUTINE interpolate_fields(w,ws,E,B)
  implicit none
  real, intent(in), dimension(slc:suc,1) :: w, ws                       ! Weights for centered and staggered vertices
  real, intent(out) :: E(3), B(3)
!-----------------------------------
! X SHIFTED WEIGHTS
E(1) = sEx(-1)*ws(-1,1) + sEx( 0)*ws( 0,1) + sEx(+1)*ws( 1,1)
!-----------------------------------
! Y SHIFTED WEIGHTS
E(2) = sEy(-1)*w(-1,1) + sEy( 0)*w( 0,1) + sEy(+1)*w( 1,1) + sEy(+2)*w( 2,1)
!-----------------------------------
! Z SHIFTED WEIGHTS
E(3) = sEz(-1)*w(-1,1) + sEz( 0)*w( 0,1) + sEz(+1)*w( 1,1) + sEz(+2)*w( 2,1)
!-----------------------------------
! YZ SHIFTED WEIGHTS
B(1) = sBx(-1)*w(-1,1) + sBx( 0)*w( 0,1) + sBx(+1)*w( 1,1) + sBx(+2)*w( 2,1)
!-----------------------------------
! ZX SHIFTED WEIGHTS
B(2) = sBy(-1)*ws(-1,1) + sBy( 0)*ws( 0,1) + sBy(+1)*ws( 1,1)
!-----------------------------------
! XY SHIFTED WEIGHTS
B(3) = sBz(-1)*ws(-1,1) + sBz( 0)*ws( 0,1) + sBz(+1)*ws( 1,1)
END SUBROUTINE interpolate_fields
!-----------------------------------------------------------------------
END MODULE interpolation_cached
!=======================================================================
MODULE interpolation
  USE params, only : mdim
  implicit none
  ! Scratch variables needed inside the loop (hence threadprivate)
  real                  :: wsx0,wsy0,wsz0, &
                           wsx1,wsy1,wsz1, &
                           wsx2,wsy2,wsz2
  real                  :: wx0,wx1,wx2, &
                           wy0,wy1,wy2, &
                           wz0,wz1,wz2
  real, dimension(mdim) :: w0,w1,w2,ws0,ws1,ws2
  integer               :: ixe,iye,ize,ixe1,iye1,ize1
!.......................................................................
END MODULE Interpolation
!=======================================================================

!=======================================================================
! The boundaries zones needed by this method
!=======================================================================
SUBROUTINE interpolation_boundaries
  USE params, only : mid
  USE grid_m, only : g
  implicit none
  character(len=mid), save:: id = &
    '$Id$'
!.......................................................................
  call print_id (id)
  g%lb = 3
  g%ub = 1
END SUBROUTINE

!=======================================================================
! Compute interpolation weights, given integer and fractional coordinates
!=======================================================================
SUBROUTINE makeweights (r, q)
  USE params, only : stdout, stdall, mpi, periodic, &
                     do_check_interpolate, master
  USE grid_m,   only : g,odx,ody,odz
  USE interpolation
  implicit none
  real   , intent(in)      :: r(mdim)
  integer                  :: q(mdim)
  real   , dimension(mdim) :: rr, r1, r2, rs1, rs2
  integer, dimension(mdim) :: ii, ii1, lb, ub
  integer, save            :: nprint=10
  logical                  :: err
  real                     :: xx,yy,zz,x1,xs1,y1,ys1,z1,zs1

!==========================================================
! Construct TSC weights for DIRECT interpolation of fields to particle
!==========================================================
!
! Find nearest gridpoint indices; 3 center + 3x1 shifted in x,y,z (6 total)
! These are for center points in the 3x3x3 point cube for the TSC interpolation.
! When center points are known we also know the others...{-1,0,+1}
! ---------------------------------------------------------
! CENTER INDICES for particle position (!!!different from CIC version!!!)
! Construct TSC weights for DIRECT interpolation of fields to particle
!==========================================================
!
! Find nearest gridpoint indices; 3 center + 3x1 shifted in x,y,z (6 total)
! These are for center points in the 3x3x3 point cube for the TSC interpolation.
! When center points are known we also know the others...{-1,0,+1}
! ---------------------------------------------------------
! CENTER INDICES for particle position (!!!different from CIC version!!!)
  
! ---------------------------------------------------------
! Find signed distance to center points in all directions.
! Project index into allowed range for interpolation.  For
! reasons of continuity in synthetic spectra we do NOT want
! to wrap the actual coordinates of particles!

  where (periodic) 
    q = modulo(q,g%gn) + g%lb                                          ! wrap
  else where
    q = q + g%lb
  end where

  ii = floor(r+0.5)                                                     ! 0-> at r=0.5
  r1 = r-ii                                                             ! -0.5 < s1 < 0.5
  ii = q+ii                                                             ! q or q+1

  rs1 = r-0.5                                                           ! -0.5 < s2 < 0.5
  ii1 = q                                                               ! q
  
  if (master .and. nprint > 0) then
    nprint = nprint - 1
    print'(1x,a,1x,i3,2(3f8.3,3x),2(3i4,3f7.3,2x))', &
      'interp: mpi%me(3),r,i',mpi%me(3),r,xx,yy,zz,ixe,iye,ize, &
      x1,y1,z1,ixe1,iye1,ize1,xs1,ys1,zs1
  end if

  ixe = ii(1); ixe1=ii1(1)
  iye = ii(2); iye1=ii1(2)
  ize = ii(3); ize1=ii1(3)
  
!-----------------------------------------------------------------------
! Sanity: check that all particles' indices are [inside] the boundaries of my node.
  err = .false.
  if(do_check_interpolate) then
   lb = merge(    2, g%lb, mpi%lb)
   ub = merge(g%n-1, g%ub, mpi%ub)
   if(any(ii1 < lb) .or. any(ii > ub)) then
    err = .true.

    write(stdall,*) ' '
    write(stdall,*) 'mpi%me(3)        : ',mpi%me(3)
    write(stdall,*) 'g%z(g%lb(3)      : ',g%z(g%lb(3))
    write(stdall,*) '(1.50001*g%ds(3)): ',(1.50001*g%ds(3))
    write(stdall,*) ' '

    write(stdall,*) 'makeweights: out of bounds result, thread', &
                     mpi%me(3), do_check_interpolate
    write(stdall,*) 'lb             =', g%lb
    write(stdall,*) 'ixe1,iye1,ize1 =', ixe1,iye1,ize1
    write(stdall,*) 'ub             =', g%ub
    write(stdall,*) 'ixe, iye, ize  =', ixe, iye, ize
    write(stdall,*) 'g%n            =', g%n
    write(stdall,'(a,3f16.10)') 'g%r(g%lb)      =', &
                     g%x(g%lb(1)),g%y(g%lb(2)),g%z(g%lb(3))
    write(stdall,'(a,3f16.10)') 'r == xx,yy,zz  =',xx,yy,zz
    write(stdall,'(a,3f16.10)') 'g%r(g%ub)      =', &
                     g%x(g%ub(1)),g%y(g%ub(2)),g%z(g%ub(3))
    write(stdall,'(a,3f16.10)') 'g%r(ub)-pa%r   =', &
                    (g%x(g%ub(1))-xx),(g%y(g%ub(2))-yy),(g%z(g%ub(3))-zz)
    write(stdall,*) 's  =', g%s
    call warning('interpolation','index out of bounds')
   end if
  end if

! ---------------------------------------------------------
! The following line ensure that we never make actual addressing
! errors; thus avoiding crashes caused by possibly just a few particles
! that are treated incorrectly.  To hear about such particles, enable the
! test above (do_check_interpolate=T in the input file).
  ii  = max(min(ii ,g%n-1),2)
  ii1 = max(min(ii1,g%n-1),2)

! ---------------------------------------------------------
! Now we construct the weights for the particle, which come to a
! grand total of...: 6x27!! = 162 ; due to the staggering (ex,ey,ez,bx,by,bz)
! (Here we advantageously use the info that the weights in each
! direction are normalized to unity (need only calculate two of three)
! (How about the shifting; can that be exploited too?)
! (For the source interpolation we need 'only' 4x27=108 points (jx,jy,jz,rho))
! ---------------------------------------------------------
! Weights are indexed as follows:
!
!       w   : weight
!       cc  : grid center points
!       sy  : grid y-shifted points
!       szx : grid x-shifted and z-shifted points
!
!       lmn : 3x3 indexed cube as fx. for the center point : lmn = {111}
!
!       Z
!       ^
!       |    THIS ON STAGGERED X-GRID
!
!          x-------x-------x
!         .       .       .|
!        1----wsx1*w12---. |
!       .       .       .  |
!       2-------x-------x  |
!       |       |       |  x
!       |       |       | .|
!       |       |       |. |
!       x-------x-------x  |
!       |       |       |  x
!       |       |       | .
!       |       |       |.
!       x-------1-------x  -> X
!
!
!
! The weighting scheme is TSC (triangular cloud scheme):
!
!                  {  3./4. - (x*odx)**2                        for |x| =< 1/2
!   wsxlmn   = {  1./2.*(3./2. - |x|*odx)**2            for 1/2 =< |x| =< 3/2
!                  {  0                                         for everything else
!
!               where x = xx - xe(ixe) , |x| = abs(xx - xe(ixe))
!
!       The indices will produce the right |x| when used in xe(ixe) etc.
!

!-------------------------------
! These are the remaining 2 points' coords distances in each direction x 1 + 1
! points (center + shift) x degrees of freedom (center+shift); total of 12

  r2  = r1  - 1.
  rs2 = rs1 - 1.

!---------------------------------------
! Our TSC function is a product function:
!
!       U(x,y,z)=(1/V_cell)*[u(x/Hx)*u(y/Hy)*u(z/Hz)]
!
!       ref: Hockney&Eastwood "Computer Simulation Using Particles", 1981
!
!       (where V_cell = Hx^2.+Hy^2.+Hz^2.   ; 
!       Hi defined as L_i / (ni-2) (Lx/(nx-2) = really just dx.))
!
! Now we construct the weights for each coordinate:
!
!-------------
  w1  = .75 - (r1 )**2                                ! for  |x| =< 1/2  (i.e. x1)
  ws1 = .75 - (rs1)**2                                
  w2  = .5*(1.5 - abs(r2 ))**2                        ! for 1/2 =< |x| =< 3/2 (i.e. x0, x2)
  ws2 = .5*(1.5 - abs(rs2))**2 
  w0  = 1. - (w1  + w2 )                              ! this one can only be the rest
  ws0 = 1. - (ws1 + ws2)
  
  wx0=w0(1); wx1=w1(1); wx2=w2(1); wsx0=ws0(1); wsx1=ws1(1); wsx2=ws2(1)
  wy0=w0(2); wy1=w1(2); wy2=w2(2); wsy0=ws0(2); wsy1=ws1(2); wsy2=ws2(2)
  wz0=w0(3); wz1=w1(3); wz2=w2(3); wsz0=ws0(3); wsz1=ws1(3); wsz2=ws2(3)

  ixe=ii(1); ixe1=ii1(1)
  iye=ii(2); iye1=ii1(2)
  ize=ii(3); ize1=ii1(3)
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE scatter_fields (r,q,fx,fy,fz,fp1,fyz,fzx,fxy,fp2)
  USE params, only : mcoord
  USE grid_m,   only : nx,ny,nz
  USE interpolation
  implicit none
  real   , intent(in) :: r(mdim)
  integer, intent(in) :: q(mdim)
  real, dimension(nx,ny,nz), intent(in)  :: fx,fy,fz,fxy,fyz,fzx        ! input fields
  real, dimension(mcoord)                :: fp1,fp2                     ! output vectors
  real                                   :: w00,w10,w01,w11,w20,w02,w12,w21,w22
!.......................................................................
  call makeweights (r, q)

  !-----------------------------------
  ! X SHIFTED WEIGHTS
  w00 = wy0*wz0
  w10 = wy1*wz0
  w01 = wy0*wz1
  w11 = wy1*wz1
  w20 = wy2*wz0   ! these are for saving some multiplies...about 96
  w02 = wy0*wz2
  w12 = wy1*wz2
  w21 = wy2*wz1
  w22 = wy2*wz2
  !
  fp1(1) = &
       fx(ixe1-1,iye-1,ize-1)*wsx0*w00 + &
       fx(ixe1  ,iye-1,ize-1)*wsx1*w00 + &
       fx(ixe1+1,iye-1,ize-1)*wsx2*w00 + &
       fx(ixe1-1,iye  ,ize-1)*wsx0*w10 + &
       fx(ixe1  ,iye  ,ize-1)*wsx1*w10 + &
       fx(ixe1+1,iye  ,ize-1)*wsx2*w10 + &
       fx(ixe1-1,iye+1,ize-1)*wsx0*w20 + &
       fx(ixe1  ,iye+1,ize-1)*wsx1*w20 + &
       fx(ixe1+1,iye+1,ize-1)*wsx2*w20 + &
       fx(ixe1-1,iye-1,ize  )*wsx0*w01 + &
       fx(ixe1  ,iye-1,ize  )*wsx1*w01 + &
       fx(ixe1+1,iye-1,ize  )*wsx2*w01 + &
       fx(ixe1-1,iye  ,ize  )*wsx0*w11 + &
       fx(ixe1  ,iye  ,ize  )*wsx1*w11 + &
       fx(ixe1+1,iye  ,ize  )*wsx2*w11 + &
       fx(ixe1-1,iye+1,ize  )*wsx0*w21 + &
       fx(ixe1  ,iye+1,ize  )*wsx1*w21 + &
       fx(ixe1+1,iye+1,ize  )*wsx2*w21 + &
       fx(ixe1-1,iye-1,ize+1)*wsx0*w02 + &
       fx(ixe1  ,iye-1,ize+1)*wsx1*w02 + &
       fx(ixe1+1,iye-1,ize+1)*wsx2*w02 + &
       fx(ixe1-1,iye  ,ize+1)*wsx0*w12 + &
       fx(ixe1  ,iye  ,ize+1)*wsx1*w12 + &
       fx(ixe1+1,iye  ,ize+1)*wsx2*w12 + &
       fx(ixe1-1,iye+1,ize+1)*wsx0*w22 + &
       fx(ixe1  ,iye+1,ize+1)*wsx1*w22 + &
       fx(ixe1+1,iye+1,ize+1)*wsx2*w22

  !-----------------------------------
  ! Y SHIFTED WEIGHTS
  w00 = wz0*wx0
  w10 = wz1*wx0
  w01 = wz0*wx1
  w11 = wz1*wx1
  w20 = wz2*wx0
  w02 = wz0*wx2
  w12 = wz1*wx2
  w21 = wz2*wx1
  w22 = wz2*wx2
  !
  fp1(2) = &
       fy(ixe-1,iye1-1,ize-1)*wsy0*w00 + &
       fy(ixe  ,iye1-1,ize-1)*wsy0*w01 + &
       fy(ixe+1,iye1-1,ize-1)*wsy0*w02 + &
       fy(ixe-1,iye1  ,ize-1)*wsy1*w00 + &
       fy(ixe  ,iye1  ,ize-1)*wsy1*w01 + &
       fy(ixe+1,iye1  ,ize-1)*wsy1*w02 + &
       fy(ixe-1,iye1+1,ize-1)*wsy2*w00 + &
       fy(ixe  ,iye1+1,ize-1)*wsy2*w01 + &
       fy(ixe+1,iye1+1,ize-1)*wsy2*w02 + &
       fy(ixe-1,iye1-1,ize  )*wsy0*w10 + &
       fy(ixe  ,iye1-1,ize  )*wsy0*w11 + &
       fy(ixe+1,iye1-1,ize  )*wsy0*w12 + &
       fy(ixe-1,iye1  ,ize  )*wsy1*w10 + &
       fy(ixe  ,iye1  ,ize  )*wsy1*w11 + &
       fy(ixe+1,iye1  ,ize  )*wsy1*w12 + &
       fy(ixe-1,iye1+1,ize  )*wsy2*w10 + &
       fy(ixe  ,iye1+1,ize  )*wsy2*w11 + &
       fy(ixe+1,iye1+1,ize  )*wsy2*w12 + &
       fy(ixe-1,iye1-1,ize+1)*wsy0*w20 + &
       fy(ixe  ,iye1-1,ize+1)*wsy0*w21 + &
       fy(ixe+1,iye1-1,ize+1)*wsy0*w22 + &
       fy(ixe-1,iye1  ,ize+1)*wsy1*w20 + &
       fy(ixe  ,iye1  ,ize+1)*wsy1*w21 + &
       fy(ixe+1,iye1  ,ize+1)*wsy1*w22 + &
       fy(ixe-1,iye1+1,ize+1)*wsy2*w20 + &
       fy(ixe  ,iye1+1,ize+1)*wsy2*w21 + &
       fy(ixe+1,iye1+1,ize+1)*wsy2*w22

!-----------------------------------
! Z SHIFTED WEIGHTS
  w00 = wx0*wy0
  w10 = wx1*wy0
  w01 = wx0*wy1
  w11 = wx1*wy1
  w20 = wx2*wy0
  w02 = wx0*wy2
  w12 = wx1*wy2
  w21 = wx2*wy1
  w22 = wx2*wy2

  fp1(3) = & 
       fz(ixe-1,iye-1,ize1-1)*wsz0*w00 + &
       fz(ixe  ,iye-1,ize1-1)*wsz0*w10 + &
       fz(ixe+1,iye-1,ize1-1)*wsz0*w20 + &
       fz(ixe-1,iye  ,ize1-1)*wsz0*w01 + &
       fz(ixe  ,iye  ,ize1-1)*wsz0*w11 + &
       fz(ixe+1,iye  ,ize1-1)*wsz0*w21 + &
       fz(ixe-1,iye+1,ize1-1)*wsz0*w02 + &
       fz(ixe  ,iye+1,ize1-1)*wsz0*w12 + &
       fz(ixe+1,iye+1,ize1-1)*wsz0*w22 + &
       fz(ixe-1,iye-1,ize1  )*wsz1*w00 + &
       fz(ixe  ,iye-1,ize1  )*wsz1*w10 + &
       fz(ixe+1,iye-1,ize1  )*wsz1*w20 + &
       fz(ixe-1,iye  ,ize1  )*wsz1*w01 + &
       fz(ixe  ,iye  ,ize1  )*wsz1*w11 + &
       fz(ixe+1,iye  ,ize1  )*wsz1*w21 + &
       fz(ixe-1,iye+1,ize1  )*wsz1*w02 + &
       fz(ixe  ,iye+1,ize1  )*wsz1*w12 + &
       fz(ixe+1,iye+1,ize1  )*wsz1*w22 + &
       fz(ixe-1,iye-1,ize1+1)*wsz2*w00 + &
       fz(ixe  ,iye-1,ize1+1)*wsz2*w10 + &
       fz(ixe+1,iye-1,ize1+1)*wsz2*w20 + &
       fz(ixe-1,iye  ,ize1+1)*wsz2*w01 + &
       fz(ixe  ,iye  ,ize1+1)*wsz2*w11 + &
       fz(ixe+1,iye  ,ize1+1)*wsz2*w21 + &
       fz(ixe-1,iye+1,ize1+1)*wsz2*w02 + &
       fz(ixe  ,iye+1,ize1+1)*wsz2*w12 + &
       fz(ixe+1,iye+1,ize1+1)*wsz2*w22

!-----------------------------------
! YZ SHIFTED WEIGHTS
  w00 = wsy0*wsz0
  w10 = wsy1*wsz0
  w01 = wsy0*wsz1
  w11 = wsy1*wsz1
  w20 = wsy2*wsz0
  w02 = wsy0*wsz2
  w12 = wsy1*wsz2
  w21 = wsy2*wsz1
  w22 = wsy2*wsz2

  fp2(1) = & 
       fyz(ixe-1,iye1-1,ize1-1)*wx0*w00 + &
       fyz(ixe  ,iye1-1,ize1-1)*wx1*w00 + &
       fyz(ixe+1,iye1-1,ize1-1)*wx2*w00 + &
       fyz(ixe-1,iye1  ,ize1-1)*wx0*w10 + &
       fyz(ixe  ,iye1  ,ize1-1)*wx1*w10 + &
       fyz(ixe+1,iye1  ,ize1-1)*wx2*w10 + &
       fyz(ixe-1,iye1+1,ize1-1)*wx0*w20 + &
       fyz(ixe  ,iye1+1,ize1-1)*wx1*w20 + &
       fyz(ixe+1,iye1+1,ize1-1)*wx2*w20 + &
       fyz(ixe-1,iye1-1,ize1  )*wx0*w01 + &
       fyz(ixe  ,iye1-1,ize1  )*wx1*w01 + &
       fyz(ixe+1,iye1-1,ize1  )*wx2*w01 + &
       fyz(ixe-1,iye1  ,ize1  )*wx0*w11 + &
       fyz(ixe  ,iye1  ,ize1  )*wx1*w11 + &
       fyz(ixe+1,iye1  ,ize1  )*wx2*w11 + &
       fyz(ixe-1,iye1+1,ize1  )*wx0*w21 + &
       fyz(ixe  ,iye1+1,ize1  )*wx1*w21 + &
       fyz(ixe+1,iye1+1,ize1  )*wx2*w21 + &
       fyz(ixe-1,iye1-1,ize1+1)*wx0*w02 + &
       fyz(ixe  ,iye1-1,ize1+1)*wx1*w02 + &
       fyz(ixe+1,iye1-1,ize1+1)*wx2*w02 + &
       fyz(ixe-1,iye1  ,ize1+1)*wx0*w12 + &
       fyz(ixe  ,iye1  ,ize1+1)*wx1*w12 + &
       fyz(ixe+1,iye1  ,ize1+1)*wx2*w12 + &
       fyz(ixe-1,iye1+1,ize1+1)*wx0*w22 + &
       fyz(ixe  ,iye1+1,ize1+1)*wx1*w22 + &
       fyz(ixe+1,iye1+1,ize1+1)*wx2*w22

!-----------------------------------
! ZX SHIFTED WEIGHTS
  w00 = wsz0*wsx0
  w10 = wsz1*wsx0
  w01 = wsz0*wsx1
  w11 = wsz1*wsx1
  w20 = wsz2*wsx0
  w02 = wsz0*wsx2
  w12 = wsz1*wsx2
  w21 = wsz2*wsx1
  w22 = wsz2*wsx2

  fp2(2) = &
       fzx(ixe1-1,iye-1,ize1-1)*wy0*w00 + &
       fzx(ixe1  ,iye-1,ize1-1)*wy0*w01 + &
       fzx(ixe1+1,iye-1,ize1-1)*wy0*w02 + &
       fzx(ixe1-1,iye  ,ize1-1)*wy1*w00 + &
       fzx(ixe1  ,iye  ,ize1-1)*wy1*w01 + &
       fzx(ixe1+1,iye  ,ize1-1)*wy1*w02 + &
       fzx(ixe1-1,iye+1,ize1-1)*wy2*w00 + &
       fzx(ixe1  ,iye+1,ize1-1)*wy2*w01 + &
       fzx(ixe1+1,iye+1,ize1-1)*wy2*w02 + &
       fzx(ixe1-1,iye-1,ize1  )*wy0*w10 + &
       fzx(ixe1  ,iye-1,ize1  )*wy0*w11 + &
       fzx(ixe1+1,iye-1,ize1  )*wy0*w12 + &
       fzx(ixe1-1,iye  ,ize1  )*wy1*w10 + &
       fzx(ixe1  ,iye  ,ize1  )*wy1*w11 + &
       fzx(ixe1+1,iye  ,ize1  )*wy1*w12 + &
       fzx(ixe1-1,iye+1,ize1  )*wy2*w10 + &
       fzx(ixe1  ,iye+1,ize1  )*wy2*w11 + &
       fzx(ixe1+1,iye+1,ize1  )*wy2*w12 + &
       fzx(ixe1-1,iye-1,ize1+1)*wy0*w20 + &
       fzx(ixe1  ,iye-1,ize1+1)*wy0*w21 + &
       fzx(ixe1+1,iye-1,ize1+1)*wy0*w22 + &
       fzx(ixe1-1,iye  ,ize1+1)*wy1*w20 + &
       fzx(ixe1  ,iye  ,ize1+1)*wy1*w21 + &
       fzx(ixe1+1,iye  ,ize1+1)*wy1*w22 + &
       fzx(ixe1-1,iye+1,ize1+1)*wy2*w20 + &
       fzx(ixe1  ,iye+1,ize1+1)*wy2*w21 + &
       fzx(ixe1+1,iye+1,ize1+1)*wy2*w22

!-----------------------------------
! XY SHIFTED WEIGHTS
  w00 = wsx0*wsy0
  w10 = wsx1*wsy0
  w01 = wsx0*wsy1
  w11 = wsx1*wsy1
  w20 = wsx2*wsy0
  w02 = wsx0*wsy2
  w12 = wsx1*wsy2
  w21 = wsx2*wsy1
  w22 = wsx2*wsy2

  fp2(3) = &
       fxy(ixe1-1,iye1-1,ize-1)*wz0*w00 + &
       fxy(ixe1  ,iye1-1,ize-1)*wz0*w10 + &
       fxy(ixe1+1,iye1-1,ize-1)*wz0*w20 + &
       fxy(ixe1-1,iye1  ,ize-1)*wz0*w01 + &
       fxy(ixe1  ,iye1  ,ize-1)*wz0*w11 + &
       fxy(ixe1+1,iye1  ,ize-1)*wz0*w21 + &
       fxy(ixe1-1,iye1+1,ize-1)*wz0*w02 + &
       fxy(ixe1  ,iye1+1,ize-1)*wz0*w12 + &
       fxy(ixe1+1,iye1+1,ize-1)*wz0*w22 + &
       fxy(ixe1-1,iye1-1,ize  )*wz1*w00 + &
       fxy(ixe1  ,iye1-1,ize  )*wz1*w10 + &
       fxy(ixe1+1,iye1-1,ize  )*wz1*w20 + &
       fxy(ixe1-1,iye1  ,ize  )*wz1*w01 + &
       fxy(ixe1  ,iye1  ,ize  )*wz1*w11 + &
       fxy(ixe1+1,iye1  ,ize  )*wz1*w21 + &
       fxy(ixe1-1,iye1+1,ize  )*wz1*w02 + &
       fxy(ixe1  ,iye1+1,ize  )*wz1*w12 + &
       fxy(ixe1+1,iye1+1,ize  )*wz1*w22 + &
       fxy(ixe1-1,iye1-1,ize+1)*wz2*w00 + &
       fxy(ixe1  ,iye1-1,ize+1)*wz2*w10 + &
       fxy(ixe1+1,iye1-1,ize+1)*wz2*w20 + &
       fxy(ixe1-1,iye1  ,ize+1)*wz2*w01 + &
       fxy(ixe1  ,iye1  ,ize+1)*wz2*w11 + &
       fxy(ixe1+1,iye1  ,ize+1)*wz2*w21 + &
       fxy(ixe1-1,iye1+1,ize+1)*wz2*w02 + &
       fxy(ixe1  ,iye1+1,ize+1)*wz2*w12 + &
       fxy(ixe1+1,iye1+1,ize+1)*wz2*w22
END SUBROUTINE

!=======================================================================
! Gather contributions to fields, given integer and fractional coordinate
!=======================================================================
SUBROUTINE gather(r,q,f,fp,k,w)
  USE params,  only : mcoord, nspecies, do_vth
  USE species, only : field
  USE grid_m,    only : nx,ny,nz
  USE interpolation
  implicit none
  real   , intent(in)                        :: r(mdim)
  integer, intent(in)                        :: q(mdim)
  type(field), dimension(nx,ny,nz,nspecies)  :: f                       ! receiving fields
  real,        intent(in), dimension(mcoord) :: fp                      ! velocity
  integer,     intent(in)                    :: k                       ! species index
  real,        intent(in)                    :: w                       ! weight
  ! Local variables
  real :: w00,w10,w01,w11,w20,w02,w12,w21,w22,fp2
!.......................................................................

  call makeweights (r, q)

  ! -----------------------------------------------
  ! CENTERED WEIGHTS: Spread out a particle on the centered density field
  w00 = wx0*wy0*w
  w01 = wx0*wy1*w
  w02 = wx0*wy2*w
  w10 = wx1*wy0*w
  w11 = wx1*wy1*w
  w12 = wx1*wy2*w
  w20 = wx2*wy0*w
  w21 = wx2*wy1*w
  w22 = wx2*wy2*w
!
  f(ixe-1,iye-1,ize-1,k)%d = f(ixe-1,iye-1,ize-1,k)%d + w00*wz0
  f(ixe  ,iye-1,ize-1,k)%d = f(ixe  ,iye-1,ize-1,k)%d + w10*wz0 
  f(ixe+1,iye-1,ize-1,k)%d = f(ixe+1,iye-1,ize-1,k)%d + w20*wz0 
  f(ixe-1,iye  ,ize-1,k)%d = f(ixe-1,iye  ,ize-1,k)%d + w01*wz0 
  f(ixe  ,iye  ,ize-1,k)%d = f(ixe  ,iye  ,ize-1,k)%d + w11*wz0 
  f(ixe+1,iye  ,ize-1,k)%d = f(ixe+1,iye  ,ize-1,k)%d + w21*wz0 
  f(ixe-1,iye+1,ize-1,k)%d = f(ixe-1,iye+1,ize-1,k)%d + w02*wz0 
  f(ixe  ,iye+1,ize-1,k)%d = f(ixe  ,iye+1,ize-1,k)%d + w12*wz0 
  f(ixe+1,iye+1,ize-1,k)%d = f(ixe+1,iye+1,ize-1,k)%d + w22*wz0 
  f(ixe-1,iye-1,ize  ,k)%d = f(ixe-1,iye-1,ize  ,k)%d + w00*wz1 
  f(ixe  ,iye-1,ize  ,k)%d = f(ixe  ,iye-1,ize  ,k)%d + w10*wz1 
  f(ixe+1,iye-1,ize  ,k)%d = f(ixe+1,iye-1,ize  ,k)%d + w20*wz1 
  f(ixe-1,iye  ,ize  ,k)%d = f(ixe-1,iye  ,ize  ,k)%d + w01*wz1 
  f(ixe  ,iye  ,ize  ,k)%d = f(ixe  ,iye  ,ize  ,k)%d + w11*wz1 
  f(ixe+1,iye  ,ize  ,k)%d = f(ixe+1,iye  ,ize  ,k)%d + w21*wz1 
  f(ixe-1,iye+1,ize  ,k)%d = f(ixe-1,iye+1,ize  ,k)%d + w02*wz1 
  f(ixe  ,iye+1,ize  ,k)%d = f(ixe  ,iye+1,ize  ,k)%d + w12*wz1 
  f(ixe+1,iye+1,ize  ,k)%d = f(ixe+1,iye+1,ize  ,k)%d + w22*wz1 
  f(ixe-1,iye-1,ize+1,k)%d = f(ixe-1,iye-1,ize+1,k)%d + w00*wz2 
  f(ixe  ,iye-1,ize+1,k)%d = f(ixe  ,iye-1,ize+1,k)%d + w10*wz2 
  f(ixe+1,iye-1,ize+1,k)%d = f(ixe+1,iye-1,ize+1,k)%d + w20*wz2 
  f(ixe-1,iye  ,ize+1,k)%d = f(ixe-1,iye  ,ize+1,k)%d + w01*wz2 
  f(ixe  ,iye  ,ize+1,k)%d = f(ixe  ,iye  ,ize+1,k)%d + w11*wz2 
  f(ixe+1,iye  ,ize+1,k)%d = f(ixe+1,iye  ,ize+1,k)%d + w21*wz2 
  f(ixe-1,iye+1,ize+1,k)%d = f(ixe-1,iye+1,ize+1,k)%d + w02*wz2 
  f(ixe  ,iye+1,ize+1,k)%d = f(ixe  ,iye+1,ize+1,k)%d + w12*wz2 
  f(ixe+1,iye+1,ize+1,k)%d = f(ixe+1,iye+1,ize+1,k)%d + w22*wz2
   
  if (do_vth) then
    fp2 = fp(1)**2+fp(2)**2+fp(3)**2
    f(ixe-1,iye-1,ize-1,k)%vth = f(ixe-1,iye-1,ize-1,k)%vth + w00*wz0*fp2
    f(ixe  ,iye-1,ize-1,k)%vth = f(ixe  ,iye-1,ize-1,k)%vth + w10*wz0*fp2 
    f(ixe+1,iye-1,ize-1,k)%vth = f(ixe+1,iye-1,ize-1,k)%vth + w20*wz0*fp2 
    f(ixe-1,iye  ,ize-1,k)%vth = f(ixe-1,iye  ,ize-1,k)%vth + w01*wz0*fp2 
    f(ixe  ,iye  ,ize-1,k)%vth = f(ixe  ,iye  ,ize-1,k)%vth + w11*wz0*fp2 
    f(ixe+1,iye  ,ize-1,k)%vth = f(ixe+1,iye  ,ize-1,k)%vth + w21*wz0*fp2 
    f(ixe-1,iye+1,ize-1,k)%vth = f(ixe-1,iye+1,ize-1,k)%vth + w02*wz0*fp2 
    f(ixe  ,iye+1,ize-1,k)%vth = f(ixe  ,iye+1,ize-1,k)%vth + w12*wz0*fp2 
    f(ixe+1,iye+1,ize-1,k)%vth = f(ixe+1,iye+1,ize-1,k)%vth + w22*wz0*fp2 
    f(ixe-1,iye-1,ize  ,k)%vth = f(ixe-1,iye-1,ize  ,k)%vth + w00*wz1*fp2 
    f(ixe  ,iye-1,ize  ,k)%vth = f(ixe  ,iye-1,ize  ,k)%vth + w10*wz1*fp2 
    f(ixe+1,iye-1,ize  ,k)%vth = f(ixe+1,iye-1,ize  ,k)%vth + w20*wz1*fp2 
    f(ixe-1,iye  ,ize  ,k)%vth = f(ixe-1,iye  ,ize  ,k)%vth + w01*wz1*fp2 
    f(ixe  ,iye  ,ize  ,k)%vth = f(ixe  ,iye  ,ize  ,k)%vth + w11*wz1*fp2 
    f(ixe+1,iye  ,ize  ,k)%vth = f(ixe+1,iye  ,ize  ,k)%vth + w21*wz1*fp2 
    f(ixe-1,iye+1,ize  ,k)%vth = f(ixe-1,iye+1,ize  ,k)%vth + w02*wz1*fp2 
    f(ixe  ,iye+1,ize  ,k)%vth = f(ixe  ,iye+1,ize  ,k)%vth + w12*wz1*fp2 
    f(ixe+1,iye+1,ize  ,k)%vth = f(ixe+1,iye+1,ize  ,k)%vth + w22*wz1*fp2 
    f(ixe-1,iye-1,ize+1,k)%vth = f(ixe-1,iye-1,ize+1,k)%vth + w00*wz2*fp2 
    f(ixe  ,iye-1,ize+1,k)%vth = f(ixe  ,iye-1,ize+1,k)%vth + w10*wz2*fp2 
    f(ixe+1,iye-1,ize+1,k)%vth = f(ixe+1,iye-1,ize+1,k)%vth + w20*wz2*fp2 
    f(ixe-1,iye  ,ize+1,k)%vth = f(ixe-1,iye  ,ize+1,k)%vth + w01*wz2*fp2 
    f(ixe  ,iye  ,ize+1,k)%vth = f(ixe  ,iye  ,ize+1,k)%vth + w11*wz2*fp2 
    f(ixe+1,iye  ,ize+1,k)%vth = f(ixe+1,iye  ,ize+1,k)%vth + w21*wz2*fp2 
    f(ixe-1,iye+1,ize+1,k)%vth = f(ixe-1,iye+1,ize+1,k)%vth + w02*wz2*fp2 
    f(ixe  ,iye+1,ize+1,k)%vth = f(ixe  ,iye+1,ize+1,k)%vth + w12*wz2*fp2 
    f(ixe+1,iye+1,ize+1,k)%vth = f(ixe+1,iye+1,ize+1,k)%vth + w22*wz2*fp2
  end if 

  !-----------------------------------------------
  ! DISTRIBUTE PARTICLE X-FLUX
  w00 = wy0*wz0*w
  w10 = wy1*wz0*w
  w01 = wy0*wz1*w
  w11 = wy1*wz1*w
  w20 = wy2*wz0*w   ! these are for saving some multiplies...about 96
  w02 = wy0*wz2*w   ! ??is there danger in re-using w00-w22 for the other calcs below??
  w12 = wy1*wz2*w   ! !!!!!possible to use some more convenient matrix-syntax????
  w21 = wy2*wz1*w
  w22 = wy2*wz2*w
  !
  f(ixe1-1,iye-1,ize-1,k)%v(1)=f(ixe1-1,iye-1,ize-1,k)%v(1)+fp(1)*wsx0*w00
  f(ixe1  ,iye-1,ize-1,k)%v(1)=f(ixe1  ,iye-1,ize-1,k)%v(1)+fp(1)*wsx1*w00 
  f(ixe1+1,iye-1,ize-1,k)%v(1)=f(ixe1+1,iye-1,ize-1,k)%v(1)+fp(1)*wsx2*w00 
  f(ixe1-1,iye  ,ize-1,k)%v(1)=f(ixe1-1,iye  ,ize-1,k)%v(1)+fp(1)*wsx0*w10 
  f(ixe1  ,iye  ,ize-1,k)%v(1)=f(ixe1  ,iye  ,ize-1,k)%v(1)+fp(1)*wsx1*w10 
  f(ixe1+1,iye  ,ize-1,k)%v(1)=f(ixe1+1,iye  ,ize-1,k)%v(1)+fp(1)*wsx2*w10 
  f(ixe1-1,iye+1,ize-1,k)%v(1)=f(ixe1-1,iye+1,ize-1,k)%v(1)+fp(1)*wsx0*w20 
  f(ixe1  ,iye+1,ize-1,k)%v(1)=f(ixe1  ,iye+1,ize-1,k)%v(1)+fp(1)*wsx1*w20
  f(ixe1+1,iye+1,ize-1,k)%v(1)=f(ixe1+1,iye+1,ize-1,k)%v(1)+fp(1)*wsx2*w20 
  f(ixe1-1,iye-1,ize  ,k)%v(1)=f(ixe1-1,iye-1,ize  ,k)%v(1)+fp(1)*wsx0*w01 
  f(ixe1  ,iye-1,ize  ,k)%v(1)=f(ixe1  ,iye-1,ize  ,k)%v(1)+fp(1)*wsx1*w01 
  f(ixe1+1,iye-1,ize  ,k)%v(1)=f(ixe1+1,iye-1,ize  ,k)%v(1)+fp(1)*wsx2*w01 
  f(ixe1-1,iye  ,ize  ,k)%v(1)=f(ixe1-1,iye  ,ize  ,k)%v(1)+fp(1)*wsx0*w11 
  f(ixe1  ,iye  ,ize  ,k)%v(1)=f(ixe1  ,iye  ,ize  ,k)%v(1)+fp(1)*wsx1*w11 
  f(ixe1+1,iye  ,ize  ,k)%v(1)=f(ixe1+1,iye  ,ize  ,k)%v(1)+fp(1)*wsx2*w11 
  f(ixe1-1,iye+1,ize  ,k)%v(1)=f(ixe1-1,iye+1,ize  ,k)%v(1)+fp(1)*wsx0*w21 
  f(ixe1  ,iye+1,ize  ,k)%v(1)=f(ixe1  ,iye+1,ize  ,k)%v(1)+fp(1)*wsx1*w21 
  f(ixe1+1,iye+1,ize  ,k)%v(1)=f(ixe1+1,iye+1,ize  ,k)%v(1)+fp(1)*wsx2*w21 
  f(ixe1-1,iye-1,ize+1,k)%v(1)=f(ixe1-1,iye-1,ize+1,k)%v(1)+fp(1)*wsx0*w02 
  f(ixe1  ,iye-1,ize+1,k)%v(1)=f(ixe1  ,iye-1,ize+1,k)%v(1)+fp(1)*wsx1*w02 
  f(ixe1+1,iye-1,ize+1,k)%v(1)=f(ixe1+1,iye-1,ize+1,k)%v(1)+fp(1)*wsx2*w02 
  f(ixe1-1,iye  ,ize+1,k)%v(1)=f(ixe1-1,iye  ,ize+1,k)%v(1)+fp(1)*wsx0*w12 
  f(ixe1  ,iye  ,ize+1,k)%v(1)=f(ixe1  ,iye  ,ize+1,k)%v(1)+fp(1)*wsx1*w12 
  f(ixe1+1,iye  ,ize+1,k)%v(1)=f(ixe1+1,iye  ,ize+1,k)%v(1)+fp(1)*wsx2*w12 
  f(ixe1-1,iye+1,ize+1,k)%v(1)=f(ixe1-1,iye+1,ize+1,k)%v(1)+fp(1)*wsx0*w22 
  f(ixe1  ,iye+1,ize+1,k)%v(1)=f(ixe1  ,iye+1,ize+1,k)%v(1)+fp(1)*wsx1*w22 
  f(ixe1+1,iye+1,ize+1,k)%v(1)=f(ixe1+1,iye+1,ize+1,k)%v(1)+fp(1)*wsx2*w22

  ! -----------------------------------------------
  ! DISTRIBUTE PARTICLE Y-FLUX
  w00 = wz0*wx0*w
  w10 = wz1*wx0*w
  w01 = wz0*wx1*w
  w11 = wz1*wx1*w
  w20 = wz2*wx0*w
  w02 = wz0*wx2*w
  w12 = wz1*wx2*w
  w21 = wz2*wx1*w
  w22 = wz2*wx2*w
  !
  f(ixe-1,iye1-1,ize-1,k)%v(2)=f(ixe-1,iye1-1,ize-1,k)%v(2)+fp(2)*wsy0*w00
  f(ixe  ,iye1-1,ize-1,k)%v(2)=f(ixe  ,iye1-1,ize-1,k)%v(2)+fp(2)*wsy0*w01 
  f(ixe+1,iye1-1,ize-1,k)%v(2)=f(ixe+1,iye1-1,ize-1,k)%v(2)+fp(2)*wsy0*w02 
  f(ixe-1,iye1  ,ize-1,k)%v(2)=f(ixe-1,iye1  ,ize-1,k)%v(2)+fp(2)*wsy1*w00 
  f(ixe  ,iye1  ,ize-1,k)%v(2)=f(ixe  ,iye1  ,ize-1,k)%v(2)+fp(2)*wsy1*w01 
  f(ixe+1,iye1  ,ize-1,k)%v(2)=f(ixe+1,iye1  ,ize-1,k)%v(2)+fp(2)*wsy1*w02 
  f(ixe-1,iye1+1,ize-1,k)%v(2)=f(ixe-1,iye1+1,ize-1,k)%v(2)+fp(2)*wsy2*w00 
  f(ixe  ,iye1+1,ize-1,k)%v(2)=f(ixe  ,iye1+1,ize-1,k)%v(2)+fp(2)*wsy2*w01 
  f(ixe+1,iye1+1,ize-1,k)%v(2)=f(ixe+1,iye1+1,ize-1,k)%v(2)+fp(2)*wsy2*w02 
  f(ixe-1,iye1-1,ize  ,k)%v(2)=f(ixe-1,iye1-1,ize  ,k)%v(2)+fp(2)*wsy0*w10 
  f(ixe  ,iye1-1,ize  ,k)%v(2)=f(ixe  ,iye1-1,ize  ,k)%v(2)+fp(2)*wsy0*w11 
  f(ixe+1,iye1-1,ize  ,k)%v(2)=f(ixe+1,iye1-1,ize  ,k)%v(2)+fp(2)*wsy0*w12 
  f(ixe-1,iye1  ,ize  ,k)%v(2)=f(ixe-1,iye1  ,ize  ,k)%v(2)+fp(2)*wsy1*w10 
  f(ixe  ,iye1  ,ize  ,k)%v(2)=f(ixe  ,iye1  ,ize  ,k)%v(2)+fp(2)*wsy1*w11 
  f(ixe+1,iye1  ,ize  ,k)%v(2)=f(ixe+1,iye1  ,ize  ,k)%v(2)+fp(2)*wsy1*w12 
  f(ixe-1,iye1+1,ize  ,k)%v(2)=f(ixe-1,iye1+1,ize  ,k)%v(2)+fp(2)*wsy2*w10 
  f(ixe  ,iye1+1,ize  ,k)%v(2)=f(ixe  ,iye1+1,ize  ,k)%v(2)+fp(2)*wsy2*w11 
  f(ixe+1,iye1+1,ize  ,k)%v(2)=f(ixe+1,iye1+1,ize  ,k)%v(2)+fp(2)*wsy2*w12 
  f(ixe-1,iye1-1,ize+1,k)%v(2)=f(ixe-1,iye1-1,ize+1,k)%v(2)+fp(2)*wsy0*w20 
  f(ixe  ,iye1-1,ize+1,k)%v(2)=f(ixe  ,iye1-1,ize+1,k)%v(2)+fp(2)*wsy0*w21 
  f(ixe+1,iye1-1,ize+1,k)%v(2)=f(ixe+1,iye1-1,ize+1,k)%v(2)+fp(2)*wsy0*w22 
  f(ixe-1,iye1  ,ize+1,k)%v(2)=f(ixe-1,iye1  ,ize+1,k)%v(2)+fp(2)*wsy1*w20 
  f(ixe  ,iye1  ,ize+1,k)%v(2)=f(ixe  ,iye1  ,ize+1,k)%v(2)+fp(2)*wsy1*w21 
  f(ixe+1,iye1  ,ize+1,k)%v(2)=f(ixe+1,iye1  ,ize+1,k)%v(2)+fp(2)*wsy1*w22 
  f(ixe-1,iye1+1,ize+1,k)%v(2)=f(ixe-1,iye1+1,ize+1,k)%v(2)+fp(2)*wsy2*w20 
  f(ixe  ,iye1+1,ize+1,k)%v(2)=f(ixe  ,iye1+1,ize+1,k)%v(2)+fp(2)*wsy2*w21 
  f(ixe+1,iye1+1,ize+1,k)%v(2)=f(ixe+1,iye1+1,ize+1,k)%v(2)+fp(2)*wsy2*w22

  ! -----------------------------------------------
  ! DISTRIBUTE PARTICLE Z-FLUX
  w00 = wx0*wy0*w
  w10 = wx1*wy0*w
  w01 = wx0*wy1*w
  w11 = wx1*wy1*w
  w20 = wx2*wy0*w
  w02 = wx0*wy2*w
  w12 = wx1*wy2*w
  w21 = wx2*wy1*w
  w22 = wx2*wy2*w

  f(ixe-1,iye-1,ize1-1,k)%v(3)=f(ixe-1,iye-1,ize1-1,k)%v(3)+fp(3)*wsz0*w00
  f(ixe  ,iye-1,ize1-1,k)%v(3)=f(ixe  ,iye-1,ize1-1,k)%v(3)+fp(3)*wsz0*w10
  f(ixe+1,iye-1,ize1-1,k)%v(3)=f(ixe+1,iye-1,ize1-1,k)%v(3)+fp(3)*wsz0*w20
  f(ixe-1,iye  ,ize1-1,k)%v(3)=f(ixe-1,iye  ,ize1-1,k)%v(3)+fp(3)*wsz0*w01
  f(ixe  ,iye  ,ize1-1,k)%v(3)=f(ixe  ,iye  ,ize1-1,k)%v(3)+fp(3)*wsz0*w11
  f(ixe+1,iye  ,ize1-1,k)%v(3)=f(ixe+1,iye  ,ize1-1,k)%v(3)+fp(3)*wsz0*w21
  f(ixe  ,iye+1,ize1-1,k)%v(3)=f(ixe  ,iye+1,ize1-1,k)%v(3)+fp(3)*wsz0*w12
  f(ixe-1,iye+1,ize1-1,k)%v(3)=f(ixe-1,iye+1,ize1-1,k)%v(3)+fp(3)*wsz0*w02
  f(ixe+1,iye+1,ize1-1,k)%v(3)=f(ixe+1,iye+1,ize1-1,k)%v(3)+fp(3)*wsz0*w22
  f(ixe-1,iye-1,ize1  ,k)%v(3)=f(ixe-1,iye-1,ize1  ,k)%v(3)+fp(3)*wsz1*w00
  f(ixe  ,iye-1,ize1  ,k)%v(3)=f(ixe  ,iye-1,ize1  ,k)%v(3)+fp(3)*wsz1*w10
  f(ixe+1,iye-1,ize1  ,k)%v(3)=f(ixe+1,iye-1,ize1  ,k)%v(3)+fp(3)*wsz1*w20
  f(ixe-1,iye  ,ize1  ,k)%v(3)=f(ixe-1,iye  ,ize1  ,k)%v(3)+fp(3)*wsz1*w01
  f(ixe  ,iye  ,ize1  ,k)%v(3)=f(ixe  ,iye  ,ize1  ,k)%v(3)+fp(3)*wsz1*w11
  f(ixe+1,iye  ,ize1  ,k)%v(3)=f(ixe+1,iye  ,ize1  ,k)%v(3)+fp(3)*wsz1*w21
  f(ixe-1,iye+1,ize1  ,k)%v(3)=f(ixe-1,iye+1,ize1  ,k)%v(3)+fp(3)*wsz1*w02
  f(ixe  ,iye+1,ize1  ,k)%v(3)=f(ixe  ,iye+1,ize1  ,k)%v(3)+fp(3)*wsz1*w12
  f(ixe+1,iye+1,ize1  ,k)%v(3)=f(ixe+1,iye+1,ize1  ,k)%v(3)+fp(3)*wsz1*w22
  f(ixe-1,iye-1,ize1+1,k)%v(3)=f(ixe-1,iye-1,ize1+1,k)%v(3)+fp(3)*wsz2*w00
  f(ixe  ,iye-1,ize1+1,k)%v(3)=f(ixe  ,iye-1,ize1+1,k)%v(3)+fp(3)*wsz2*w10
  f(ixe+1,iye-1,ize1+1,k)%v(3)=f(ixe+1,iye-1,ize1+1,k)%v(3)+fp(3)*wsz2*w20
  f(ixe-1,iye  ,ize1+1,k)%v(3)=f(ixe-1,iye  ,ize1+1,k)%v(3)+fp(3)*wsz2*w01
  f(ixe  ,iye  ,ize1+1,k)%v(3)=f(ixe  ,iye  ,ize1+1,k)%v(3)+fp(3)*wsz2*w11
  f(ixe+1,iye  ,ize1+1,k)%v(3)=f(ixe+1,iye  ,ize1+1,k)%v(3)+fp(3)*wsz2*w21
  f(ixe-1,iye+1,ize1+1,k)%v(3)=f(ixe-1,iye+1,ize1+1,k)%v(3)+fp(3)*wsz2*w02
  f(ixe  ,iye+1,ize1+1,k)%v(3)=f(ixe  ,iye+1,ize1+1,k)%v(3)+fp(3)*wsz2*w12
  f(ixe+1,iye+1,ize1+1,k)%v(3)=f(ixe+1,iye+1,ize1+1,k)%v(3)+fp(3)*wsz2*w22
END SUBROUTINE

!=======================================================================
! Test call
!=======================================================================
SUBROUTINE test_interpolation
  USE params, only : stdout, mpi
  USE grid_m,   only : g
  USE interpolation
  implicit none
  integer iz, q(3)
  real r(3)
!.......................................................................
  if (mpi%me(3) > 0) return
  write(stdout,*) 'TESTING INTERPOLATION:'
  write(stdout,*) '      r  iz1 ize1   wz0   wz1   wz2  wsz0  wsz1  wsz2'
  do iz=1,40
    r = (/0.1,0.1,(iz-30)*0.1/) + g%rlb
    q = floor((r - g%rlb)*g%ods)
    r = (r - g%rlb)*g%ods - q
    call makeweights (r,q)
    write(stdout,'(f8.2,2(i5,3f6.2))') r(3),ize,wz0,wz1,wz2,ize1,wsz0,wsz1,wsz2
  end do
END SUBROUTINE test_interpolation
