! Mpi/nompi.f90 $ Date: 2013-09-02 14:22:40 +0200 $
! vim: nowrap
!-----------------------------------------------------------------------
MODULE comm_arrays
  implicit none
  integer :: request1,request2,request3,request4,request5,request6,request7,request8
  INTERFACE comm_array
    MODULE PROCEDURE comm_array1d,comm_array2d,comm_array3d
  END INTERFACE
CONTAINS
!-----------------------------------------------------------------------
FUNCTION wait_requests(requests,block)
  integer                           :: wait_requests
  integer, dimension(:), intent(in) :: requests
  logical, optional                 :: block
  wait_requests = 0
END FUNCTION wait_requests
!-----------------------------------------------------------------------
SUBROUTINE comm_array1d(dir,ndn,nup,scr1,scr2,scr3,scr4,req1,req2,req3,req4)
  integer,              intent(in) :: dir
  integer,              intent(in) :: ndn,nup
  real, dimension(:)               :: scr1,scr2,scr3,scr4
  integer                          :: req1,req2,req3,req4
END SUBROUTINE comm_array1d
!-----------------------------------------------------------------------
SUBROUTINE comm_array2d(dir,ndn,nup,scr1,scr2,scr3,scr4,req1,req2,req3,req4)
  integer,              intent(in) :: dir
  integer,dimension(2), intent(in) :: ndn,nup
  real, dimension(:,:)             :: scr1,scr2,scr3,scr4
  integer                          :: req1,req2,req3,req4
END SUBROUTINE comm_array2d
!-----------------------------------------------------------------------
SUBROUTINE comm_array3d(dir,ndn,nup,scr1,scr2,scr3,scr4,req1,req2,req3,req4)
  integer,              intent(in) :: dir
  integer,dimension(3), intent(in) :: ndn,nup
  real, dimension(:,:,:)           :: scr1,scr2,scr3,scr4
  integer                          :: req1,req2,req3,req4
END SUBROUTINE comm_array3d
!-----------------------------------------------------------------------
END MODULE comm_arrays
!-----------------------------------------------------------------------
SUBROUTINE Barrier
  implicit none
END SUBROUTINE Barrier
!-----------------------------------------------------------------------
SUBROUTINE barrier_trace (label)
  implicit none
  character(len=*) label 
END SUBROUTINE barrier_trace
!-----------------------------------------------------------------------
SUBROUTINE init_mpi
  USE params, ONLY : nodes, rank, mpi, comm, master
  implicit none

!-----------------------------------------------------------------------
! Initialize OpenMP
!-----------------------------------------------------------------------
  call init_omp

  nodes      = 1
  rank       = 0
  mpi%dn     = 0
  mpi%me     = 0
  mpi%up     = 0
  mpi%lb     = .true.
  mpi%ub     = .true.
  mpi%n      = 1
  comm       = 0
  mpi%interior = .false.
  master     = .true.
  ! Default values for communicators
  mpi%x%comm = 0;  mpi%x%master = .true.;  mpi%x%rank = 0;  mpi%x%nodes = 1
  mpi%y%comm = 0;  mpi%y%master = .true.;  mpi%y%rank = 0;  mpi%y%nodes = 1
  mpi%z%comm = 0;  mpi%z%master = .true.;  mpi%z%rank = 0;  mpi%z%nodes = 1
  mpi%xy%comm = 0; mpi%xy%master = .true.; mpi%xy%rank = 0; mpi%xy%nodes = 1
  mpi%yz%comm = 0; mpi%yz%master = .true.; mpi%yz%rank = 0; mpi%yz%nodes = 1
  mpi%zx%comm = 0; mpi%zx%master = .true.; mpi%zx%rank = 0; mpi%zx%nodes = 1
  call init_stdio(.true.)

END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE species_communicators
  USE params,  only : nspecies, mpi, comm, master, rank, nodes
  integer :: isp
  allocate(mpi%sp(nspecies))
  do isp=1,nspecies
    mpi%sp(isp)%comm   = 0
    mpi%sp(isp)%rank   = rank
    mpi%sp(isp)%master = master
    mpi%sp(isp)%nodes  = nodes
  enddo
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE Finalize_Mpi(finalizeflag)
  implicit none
  logical finalizeflag
  stop
END SUBROUTINE Finalize_Mpi
!-----------------------------------------------------------------------
SUBROUTINE ParticleTotal
  USE species, only : sp
  implicit none
  
  sp%tnp = sp%np
END SUBROUTINE ParticleTotal
!-----------------------------------------------------------------------
SUBROUTINE FieldEnergyTotal
  USE species, only : sp
  implicit none
!> Calculate total elm-field energy in box.
END SUBROUTINE FieldEnergyTotal
!-----------------------------------------------------------------------
SUBROUTINE SendParticles_lowmem (gmax)
  implicit none
  integer gmax
  gmax = 0
END SUBROUTINE SendParticles_lowmem
!-----------------------------------------------------------------------
SUBROUTINE Send_Particles
END SUBROUTINE Send_Particles
!-----------------------------------------------------------------------
SUBROUTINE SendParticles_omp
END SUBROUTINE SendParticles_omp
!-----------------------------------------------------------------------
SUBROUTINE stats(f,mn,av,mx,s)
  USE params,  only : nodes,comm,mpi_err
  implicit none
  real                :: mn,mx
  integer,           intent(in)  :: s
  real,dimension(s), intent(in)  :: f
  integer                        :: ip,ipp,sqnp,lb,ub
  real                           :: av
  real(kind=8)                   :: a1,a2
  if (s==0) then
    av = 0.
    return
  end if
  mn = f(1) ; mx = f(1)                                                 ! Resetting
  sqnp = floor(sqrt(real(s)))                                           ! We do everything in
  a1 = 0.
  do ipp=0,sqnp-1                                                       ! planes to guard roundoff
    lb = ipp*sqnp+1
    ub = (ipp+1)*sqnp
    if (ipp .eq. sqnp-1) ub = s
    a2   = 0.
    do ip =lb,ub
      a2 = a2 + f(ip)
      mn = min(mn ,f(ip))
      mx = max(mx ,f(ip))
    enddo
    a1  = a1  + a2
  enddo
  av = a1 /s
END SUBROUTINE stats
!-----------------------------------------------------------------------
FUNCTION aver(f,s)
  USE params,  only : nodes,comm,mpi_err
  implicit none
  real                                  :: aver
  integer,                  intent(in)  :: s
  real,dimension(max(1,s)), intent(in)  :: f
  integer                               :: ip,ipp,sqnp,lb,ub
  real(kind=8)                          :: a1,a2
  a2 = 0.
  sqnp = floor(sqrt(real(s)))                                           ! We do everything in
  do ipp=0,sqnp-1                                                       ! planes to guard roundoff
    lb = ipp*sqnp+1
    ub = (ipp+1)*sqnp
    if (ipp .eq. sqnp-1) ub = s
    a1  = 0.
    do ip =lb,ub
      a1 = a1 + f(ip)
    enddo
    a2 = a2 + a1
  enddo
  aver = a2/s
END FUNCTION aver
!-----------------------------------------------------------------------
FUNCTION aver_r8(f,s)
  USE params,  only : nodes,comm,mpi_err
  implicit none
  real(kind=8)                           :: aver_r8
  integer,                   intent(in)  :: s
  real(kind=8),dimension(s), intent(in)  :: f
  integer                                :: ip,ipp,sqnp,lb,ub
  real(kind=8)                           :: a1,a2
  a2 = 0.
  sqnp = floor(sqrt(real(s)))                                           ! We do everything in
  do ipp=0,sqnp-1                                                       ! planes to guard roundoff
    lb = ipp*sqnp+1
    ub = (ipp+1)*sqnp
    if (ipp .eq. sqnp-1) ub = s
    a1  = 0.
    do ip =lb,ub
      a1 = a1 + f(ip)
    enddo
    a2 = a2 + a1
  enddo
  aver_r8 = a2/s
END FUNCTION aver_r8
!-----------------------------------------------------------------------
! Routine to compute the total or average energy
FUNCTION aver2(isp)
  USE params,  only : nodes,comm,mpi_err
  USE species, only : sp,particle
  implicit none
  type(particle),dimension(:),pointer :: pa
  real(kind=8)                   :: aver2
  integer,           intent(in)  :: isp
  integer                        :: ip,ipp,sqnp,lb,ub
  real(kind=8)                   :: a1,a2
  if (sp(isp)%tnp .eq. 0) then
    aver2 = 0.
    return
  endif
  if (sp(isp)%np .eq. 0) then
    a2 = 0.
  else
    pa => sp(isp)%particle
    a2 = 0.
    sqnp = floor(sqrt(real(sp(isp)%np)))                                ! We do everything in
    do ipp=0,sqnp-1                                                     ! planes to guard roundoff
      lb = ipp*sqnp+1
      ub = (ipp+1)*sqnp
      if (ipp .eq. sqnp-1) ub = sp(isp)%np
      a1 = 0.
      do ip =lb,ub
        a1 = a1 + pa(ip)%e*pa(ip)%w
      enddo
      a2 = a2 + a1
    enddo
  endif
  aver2 = a2/sp(isp)%tnp
END FUNCTION aver2
!-----------------------------------------------------------------------
! Basiclly a routine to compute the total of a field quantity
! No checks done for boundaries! Neither for interior nodes nor anywhere!!!! 
FUNCTION total3(f)
  USE grid_m,            only : g
  implicit none
  real,    dimension(g%n(1),g%n(2),g%n(3)) :: f
  real(kind=8)                             :: total3
  real(kind=8)                             :: sum2,sum3
  integer                                  :: i,j,k

  total3 = 0.
  do k=g%lb(3),g%ub(3)-1
    sum2 = 0.
    do j=g%lb(2),g%ub(2)-1
      sum3 = 0.
      do i=g%lb(1),g%ub(1)-1
        sum3 = sum3 + f(i,j,k)
      enddo
      sum2 = sum2 + sum3
    enddo
    total3 = total3 + sum2
  enddo
END FUNCTION total3
!-----------------------------------------------------------------------
! Basically a routine to compute the average of a field quantity
! In principle, one more z-point should be included in the non-periodic
! case, but we don't bother
FUNCTION aver3(f)
  USE params, only : mpi_err,rank,nodes,comm
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3))   :: f
  real                     :: aver3
  real(kind=8), external   :: total3

  aver3 = total3(f) / product(g%gn)
END FUNCTION aver3
!-----------------------------------------------------------------------
! Basiclly a routine to compute the maximum of a field discarding boundaries
FUNCTION max4(f)
  USE params, only : mpi_err,rank,nodes,comm
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                  :: max4
  integer                               :: i,j,k

  max4 = f(g%lb(1),g%lb(2),g%lb(3))
  do k=g%lb(3),g%ub(3)-1
    do j=g%lb(2),g%ub(2)-1
      do i=g%lb(1),g%ub(1)-1
        max4 = max(max4,f(i,j,k))
      enddo
    enddo
  enddo
END FUNCTION max4
!-----------------------------------------------------------------------
! Basiclly a routine to compute the maximum of a field discarding boundaries
FUNCTION min4(f)
  USE params, only : mpi_err,rank,nodes,comm
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                  :: min4
  integer                               :: i,j,k

  min4 = f(g%lb(1),g%lb(2),g%lb(3))
  do k=g%lb(3),g%ub(3)-1
    do j=g%lb(2),g%ub(2)-1
      do i=g%lb(1),g%ub(1)-1
        min4 = min(min4,f(i,j,k))
      enddo
    enddo
  enddo
END FUNCTION min4
!-----------------------------------------------------------------------
SUBROUTINE overlap_x(f)
  USE params, only : periodic
  USE grid_m, only: g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  integer ix,iy,iz

  if (.not.periodic(1)) return
  do iz=1,g%n(3)
  do iy=1,g%n(2)
    do ix=g%lb(1)-1,1,-1
      f(ix,iy,iz) = f(ix+g%gn(1),iy,iz)
    enddo
    do ix=g%ub(1),g%n(1)
      f(ix,iy,iz) = f(ix-g%gn(1),iy,iz)
    enddo
  end do
  end do
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE overlap_y(f)
  USE params, only : periodic
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  integer ix,iy,iz

  if (.not.periodic(2)) return
  do iz=1,g%n(3)
    do iy=g%lb(2)-1,1,-1
    do ix=1,g%n(1)
      f(ix,iy,iz) = f(ix,iy+g%gn(2),iz)
    end do
    end do
    do iy=g%ub(2),g%n(2)
    do ix=1,g%n(1)
      f(ix,iy,iz) = f(ix,iy-g%gn(2),iz)
    end do
    end do
  end do
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE overlap_z(f)
  USE params, only : periodic
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  integer ix,iy,iz

  if (.not.periodic(3)) return
  do iz=g%lb(3)-1,1,-1
  do iy=1,g%n(2)
  do ix=1,g%n(1)
    f(ix,iy,iz) = f(ix,iy,iz+g%gn(3))
  end do
  end do
  end do
  do iz=g%ub(3),g%n(3)
  do iy=1,g%n(2)
  do ix=1,g%n(1)
    f(ix,iy,iz) = f(ix,iy,iz-g%gn(3))
  end do
  end do
  end do
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE overlap_add_x(f)
  USE params, only : stdout,periodic
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  integer ix,iy,iz,edge

  if (.not.periodic(1)) return
  edge = g%n(1) - g%gn(1)                                               ! nr of bndry zones

  if (g%gn(1) >= edge) then                                             ! general case
    do iz=1,g%n(3)
     do iy=1,g%n(2)
      do ix=1,g%n(1)-g%gn(1)
        f(ix,iy,iz) = f(ix,iy,iz) + f(ix+g%gn(1),iy,iz)
        f(ix+g%gn(1),iy,iz) = f(ix,iy,iz)
      end do
     end do
    end do
  else  if (g%gn(1) .eq. 1) then                                        ! 1-D in x-direction
    do iz=1,g%n(3)
     do iy=1,g%n(2)
      do ix=1,g%n(1)-1
        f(g%n(1),iy,iz) = f(ix,iy,iz) + f(g%n(1),iy,iz)
      end do
      do ix=1,g%n(1)-1
        f(ix,iy,iz) = f(g%n(1),iy,iz)
      end do
     end do
    end do
  else                                                                  ! [2:bndry[ cells in x-direction
    write(stdout,*) 'Total number of cells in x-dir    :', g%n(1)
    write(stdout,*) 'Physical number of cells in x-dir :', g%gn(1)
    write(stdout,*) 'Number of bndry cells in x-dir    :', g%n(1)-g%gn(1)
    call error ('overlap_add_x','We have bndry > #of cells in x-dir, '// &
      'but not 1-D. Cannot handle this yet')
  endif
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE overlap_add_y(f)
  USE params, only : stdout,periodic
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  integer ix,iy,iz,edge

  if (.not.periodic(2)) return
  edge = g%n(2) - g%gn(2)                                                  ! nr of bndry zones

  if (g%gn(2) >= edge) then                                                ! general case
    do iz=1,g%n(3)
     do iy=1,g%n(2)-g%gn(2)
      do ix=1,g%n(1)
        f(ix,iy,iz) = f(ix,iy,iz) + f(ix,iy+g%gn(2),iz)
        f(ix,iy+g%gn(2),iz) = f(ix,iy,iz)
      end do
     end do
    end do
  else if (g%gn(2) .eq. 1) then                                          ! 1-D in y-direction
    do iz=1,g%n(3)
     do iy=1,g%n(2)-1
      do ix=1,g%n(1)
        f(ix,g%n(2),iz) = f(ix,iy,iz) + f(ix,g%n(2),iz)
      end do
     end do
     do iy=1,g%n(2)-1
      do ix=1,g%n(1)
        f(ix,iy,iz) = f(ix,g%n(2),iz)
      end do
     end do
    end do
  else                                                                ! [2:bndry[ cells in y-direction
    write(stdout,*) 'Total number of cells in y-dir    :', g%n(2)
    write(stdout,*) 'Physical number of cells in y-dir :', g%gn(2)
    write(stdout,*) 'Number of bndry cells in y-dir    :', g%n(2)-g%gn(2)
    call error('overlap_add_y','We have bndry > #of cells in y-dir, but not 1-D. Cannot handle this case')
  endif
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE overlap_add_z(f)
  USE params, only : stdout,periodic
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  integer ix,iy,iz,edge

  if (.not.periodic(3)) return
  edge = g%n(3) - g%gn(3)                                                  ! nr of bndry zones
  
  if (g%gn(3) >= edge) then                                                ! general case
    do iz=1,g%n(3)-g%gn(3)
     do iy=1,g%n(2)
      do ix=1,g%n(1)
        f(ix,iy,iz) = f(ix,iy,iz) + f(ix,iy,iz+g%gn(3))                   ! send lb layers "up" and add them in
        f(ix,iy,iz+g%gn(3)) = f(ix,iy,iz)                                 ! copy lb layers "down" (in wrap sense)
      end do
     end do
    end do
  else if (g%gn(3) .eq. 1) then                                          ! 1-D in y-direction
    do iz=1,g%n(3)-1
     do iy=1,g%n(2)
      do ix=1,g%n(1)
        f(ix,iy,g%n(3)) = f(ix,iy,iz) + f(ix,iy,g%n(3))
      end do
     end do
    end do
    do iz=1,g%n(3)-1
     do iy=1,g%n(2)
      do ix=1,g%n(1)
        f(ix,iy,iz) = f(ix,iy,g%n(3))
      end do
     end do
    end do
  else
    write(stdout,*) 'Total number of cells in z-dir    :', g%n(3)
    write(stdout,*) 'Physical number of cells in z-dir :', g%gn(3)
    write(stdout,*) 'Number of bndry cells in z-dir    :', g%n(3)-g%gn(3)
    call error('overlap_add_z','We have bndry > #of cells in z-dir, but not 1-D. Cannot handle this case')
  endif
END SUBROUTINE
!-----------------------------------------------------------------------
FUNCTION max_scalar(a)
  implicit none
  real a,max_scalar
  max_scalar = a
END FUNCTION
!-----------------------------------------------------------------------
FUNCTION min_scalar(a)
  implicit none
  real a,min_scalar
  min_scalar = a
END FUNCTION
!-----------------------------------------------------------------------
FUNCTION sum_scalar(a)
  implicit none
  real a,sum_scalar
  sum_scalar = a
END FUNCTION
!-----------------------------------------------------------------------
SUBROUTINE mpi_broadcast(integer_number,root)
  implicit none
  integer, intent(in) :: integer_number
  integer, intent(in) :: root
END SUBROUTINE mpi_broadcast
!-----------------------------------------------------------------------
SUBROUTINE mpi_broadcast_logical(integer_number,root)
  implicit none
  logical, intent(in) :: integer_number
  integer, intent(in) :: root
END SUBROUTINE mpi_broadcast_logical
!-----------------------------------------------------------------------
SUBROUTINE mpi_broadcast_real(integer_number,root)
  implicit none
  real,    intent(in) :: integer_number
  integer, intent(in) :: root
END SUBROUTINE mpi_broadcast_real
!-----------------------------------------------------------------------
SUBROUTINE global_nr_of_particles(gnp,gmp)
  USE params,  only: nodes, nspecies
  USE species, only: sp 
  implicit none
  integer, dimension(nspecies,nodes)                :: gnp,gmp
  gnp(:,1) = sp%np
  gmp(:,1) = sp%mp
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_integers (n, glob)
  implicit none
  integer          :: n, glob
END SUBROUTINE mpi_sum_integers
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_integer (glob)
  implicit none
  integer          :: glob
END SUBROUTINE mpi_sum_integer
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_integer8s (n,glob)
  implicit none
  integer,         intent(in)   :: n
  integer(kind=8), dimension(n) :: glob
END SUBROUTINE mpi_sum_integer8s
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_integer8 (glob)
  implicit none
  integer(kind=8) :: glob
END SUBROUTINE mpi_sum_integer8
!-----------------------------------------------------------------------
SUBROUTINE mpi_max_integer8s (n, loc)
  implicit none
  integer, intent(in) :: n
  integer(kind=8) :: loc
END SUBROUTINE mpi_max_integer8s
!-----------------------------------------------------------------------
SUBROUTINE mpi_max_integer8 (loc)
  implicit none
  integer(kind=8) :: loc
END SUBROUTINE mpi_max_integer8
!-----------------------------------------------------------------------
SUBROUTINE mpi_min_integer8s (n, loc)
  implicit none
  integer, intent(in) :: n
  integer(kind=8) :: loc
END SUBROUTINE mpi_min_integer8s
!-----------------------------------------------------------------------
SUBROUTINE mpi_min_integer8 (loc)
  implicit none
  integer(kind=8) :: loc
END SUBROUTINE mpi_min_integer8
!-----------------------------------------------------------------------
SUBROUTINE mpi_max_reals (n, loc)
  implicit none
  integer, intent(in) :: n
  real :: loc
END SUBROUTINE mpi_max_reals
!-----------------------------------------------------------------------
SUBROUTINE mpi_max_real(loc)
  implicit none
  real :: loc
END SUBROUTINE mpi_max_real
!-----------------------------------------------------------------------
SUBROUTINE mpi_min_reals (n, loc)
  implicit none
  integer, intent(in) :: n
  real :: loc
END SUBROUTINE mpi_min_reals
!-----------------------------------------------------------------------
SUBROUTINE mpi_min_real(loc)
  implicit none
  real :: loc
END SUBROUTINE mpi_min_real
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_integer_xy(n,loc,glob)
  implicit none
  integer, intent(in)   :: n
  integer, dimension(n) :: loc,glob
  glob = loc
END SUBROUTINE mpi_sum_integer_xy
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_integer8_xy(n,loc,glob)
  implicit none
  integer,         intent(in)   :: n
  integer(kind=8), dimension(n) :: loc,glob
  glob = loc
END SUBROUTINE mpi_sum_integer8_xy
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_real (loc)
  implicit none
  real             :: loc
END SUBROUTINE mpi_sum_real
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_reals (n, loc)
  implicit none
  integer          :: n
  real             :: loc(n)
END SUBROUTINE mpi_sum_reals
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_real8 (loc)
  implicit none
  real(kind=8)     :: loc
END SUBROUTINE mpi_sum_real8
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_real8s (n, loc)
  implicit none
  integer          :: n
  real(kind=8)     :: loc(n)
END SUBROUTINE mpi_sum_real8s
!-----------------------------------------------------------------------
SUBROUTINE highest_particle_id_one_species(isp)
  implicit none
  integer, intent(in) :: isp
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE highest_particle_id
  implicit none
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE mpi_allreduce_logical(llogical,glogical)
  implicit none
  logical, intent(in) :: llogical
  logical             :: glogical
  glogical = llogical
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE mpi_allreduce_logical_isp(llogical,glogical,isp)
  implicit none
  integer, intent(in) :: isp
  logical, intent(in) :: llogical
  logical             :: glogical
  glogical = llogical
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE scatter_load(node, global, isp)
  USE params,  only : nodes, mpi_err,comm
  implicit none
  integer,       intent(in) :: isp
  logical,       intent(in) :: node
  logical, dimension(nodes) :: global
  global(1) = node 
END SUBROUTINE
!-----------------------------------------------------------------------
FUNCTION sum_energy(energy)
  USE stat,    only : energy_kind
  implicit none
  real(kind=energy_kind), intent(in) :: energy                          ! partial energy from node
  real(kind=energy_kind)             :: sum_energy                      ! energy from all nodes
  sum_energy = energy
END FUNCTION
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_local_slice(inside,axis,llb,lub,ssl)                 ! integrate 2-D slice along axis
  implicit none
  logical,               intent(in) :: inside                           !> do we contribute anything ?
  integer,               intent(in) :: axis                             !> axis to sum over
  integer, dimension(2), intent(in) :: llb, lub                         !> dimensions of slice
  real,    dimension(llb(1):lub(1),llb(2):lub(2)) :: ssl                !> slice
  ! nothing to do, input==output
END SUBROUTINE mpi_sum_local_slice
!-----------------------------------------------------------------------
SUBROUTINE mpi_gather_global_slice(axis,llb,lub,ssl,gn,gsl,win,gnn)
  integer                           :: win                               !> mpi window identifier
  integer, dimension(2), intent(in) :: gnn                               !> global slice dims
  
  integer,               intent(in) :: axis                              !> axis to sum over
  integer, dimension(2), intent(in) :: llb, lub                          !> dimensions of slice
  real,    dimension(llb(1):lub(1),llb(2):lub(2)) :: ssl                 !> local slice chunk
  integer, dimension(2), intent(in) :: gn                                !> global slice dims
  real,    dimension(gn(1),gn(2))   :: gsl                               !> globl slice
  gsl = ssl                                                              !> global=local since only 1 thread
END SUBROUTINE mpi_gather_global_slice
!-----------------------------------------------------------------------
SUBROUTINE mpi_release_window(win)
  implicit none
  integer              :: win
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE write_iofield(f)
  USE params, only : data_unit,periodic
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in) :: f
  real(kind=4), dimension(:,:,:),       allocatable :: fs
  integer :: ix, iy, iz, c, ub(3)
!.......................................................................
  c = product(merge(g%gn,g%gn+1,periodic))*4                            ! add physical bndry point if not periodic
  write(data_unit) c
  ub = merge(g%ub,g%ub+1,periodic)                                      ! add bndry point if ub is physical bndry
  allocate(fs(g%lb(1):ub(1)-1,g%lb(2):ub(2)-1,g%lb(3):ub(3)-1))
  fs=f(g%lb(1):ub(1)-1,g%lb(2):ub(2)-1,g%lb(3):ub(3)-1)
  write(data_unit) fs
  deallocate(fs)
  write(data_unit) c
END
!-----------------------------------------------------------------------
REAL(kind=8) FUNCTION wallclock()
  integer, save:: count(2), count_rate=0
  real, save:: norm, offset=0.

  !real cpu(2)
  !wallclock=etime(cpu)                                                 ! for approx 2x faster timing on steno
  !return

  if (count_rate == 0) then
    call system_clock(count=count(1), count_rate=count_rate)
    norm=1./real(count_rate)
  end if
  call system_clock(count=count(2))
  wallclock = (count(2)-count(1))*norm
  if (wallclock < 0.) then
    offset = offset + 24.*3600.
    wallclock = wallclock + 24.*3600.
  end if
END FUNCTION wallclock
!-----------------------------------------------------------------------
SUBROUTINE flush_all
END SUBROUTINE 
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_real(thread,n,x)
  implicit none
  integer,            intent(in) :: thread, n
  real, dimension(n), intent(in) :: x
  call error('mpi_send_real','This send is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_send_real
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_integer(thread,n,x)
  implicit none
  integer,               intent(in) :: thread, n
  integer, dimension(n), intent(in) :: x
  call error('mpi_send_integer','This send is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_send_integer
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_integer8(thread,n,x)
  implicit none
  integer,                       intent(in) :: thread, n
  integer(kind=8), dimension(n), intent(in) :: x
  call error('mpi_send_integer8','This send is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_send_integer8
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_logical(thread,n,x)
  implicit none
  integer,               intent(in) :: thread, n
  logical, dimension(n), intent(in) :: x
  call error('mpi_send_logical','This send is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_send_logical
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_real_xy(thread,n,x)
  implicit none
  integer,            intent(in) :: thread, n
  real, dimension(n), intent(in) :: x
  call error('mpi_send_real_xy','This send is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_send_real_xy
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_integer_xy(thread,n,x)
  implicit none
  integer,               intent(in) :: thread, n
  integer, dimension(n), intent(in) :: x
  call error('mpi_send_integer_xy','This send is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_send_integer_xy
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_integer8_xy(thread,n,x)
  implicit none
  integer,                       intent(in) :: thread, n
  integer(kind=8), dimension(n), intent(in) :: x
  call error('mpi_send_integer8_xy','This send is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_send_integer8_xy
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_real(thread,n,x)
  implicit none
  integer,            intent(in) :: thread, n
  real, dimension(n), intent(in) :: x
  call error('mpi_recv_real','This recv is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_recv_real
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_integer(thread,n,x)
  implicit none
  integer,   intent(in) :: thread, n
  integer, dimension(n) :: x
  call error('mpi_recv_integer','This recv is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_recv_integer
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_integer8(thread,n,x)
  implicit none
  integer,           intent(in) :: thread, n
  integer(kind=8), dimension(n) :: x
  call error('mpi_recv_integer8','This recv is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_recv_integer8
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_logical(thread,n,x)
  implicit none
  integer,   intent(in) :: thread, n
  logical, dimension(n) :: x
  call error('mpi_recv_logical','This recv is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_recv_logical
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_real_xy(thread,n,x)
  implicit none
  integer,            intent(in) :: thread, n
  real, dimension(n), intent(in) :: x
  call error('mpi_recv_real_xy','This recv is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_recv_real_xy
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_integer_xy(thread,n,x)
  implicit none
  integer,   intent(in) :: thread, n
  integer, dimension(n) :: x
  call error('mpi_recv_integer_xy','This recv is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_recv_integer_xy
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_integer8_xy(thread,n,x)
  implicit none
  integer,           intent(in) :: thread, n
  integer(kind=8), dimension(n) :: x
  call error('mpi_recv_integer8_xy','This recv is blocking. Should never be called with only one thread')
END SUBROUTINE mpi_recv_integer8_xy
!-----------------------------------------------------------------------
SUBROUTINE file_open (file, mode, type, fio)
  USE params, only: data_unit, periodic, stdout
  USE grid_m, only: g
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type) :: fio
  character(len=*) file, type, mode
  integer mw
!.......................................................................
  select case(trim(type))
  case('dump')
    fio%lb = merge (g%lb, 1,     periodic)
    fio%ub = merge (g%ub, g%n+1, periodic)
  case('field')
    fio%size=merge (g%gn, g%gn+1,periodic)
    fio%lb = g%lb
    fio%ub = merge (g%ub, g%ub+1,periodic)
  case default
    call error('file_open','File type '//trim(type)//' is unknown')
  end select
  fio%size = fio%ub - fio%lb
  mw = product(fio%size)
  write(stdout,*) 'file_open: dimensions expected =',fio%size
  open (unit=fio%unit, file=file, form='unformatted', access='direct', &
        recl=4*mw,status=mode)
END
!-----------------------------------------------------------------------
SUBROUTINE file_openr_seq (file,pos,type,fio)
  USE pic_io,     only : file_io_type
  implicit none
  type(file_io_type) :: fio
  integer(kind=8) pos
  character(len=*) file, type
!.......................................................................
  call error('file_openr_seq','should never be called when running in sequential mode')
END
!-----------------------------------------------------------------------
SUBROUTINE file_openw (file,type,fio)
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type) :: fio
  character(len=*) file, type
!.......................................................................
  call file_open (file, 'unknown',type,fio)
END
!-----------------------------------------------------------------------
SUBROUTINE file_openw_seq (file,pos,type,fio)
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type) :: fio
  integer(kind=8)  pos
  character(len=*) file,type
!.......................................................................
  call error('file_openw_seq','should never be called when running in sequential mode')
END
!-----------------------------------------------------------------------
SUBROUTINE file_openr (file,type,fio)
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type) :: fio
  character(len=*) file,type
!.......................................................................
  call file_open (file, 'old',type,fio)
END
!-----------------------------------------------------------------------
SUBROUTINE file_write (f, rec,fio)
  USE params, only: data_unit, mpi
  USE grid_m, only: g
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type) :: fio
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  real(kind=4), allocatable, dimension(:,:,:):: f1
  integer rec, lb(3), ub(3)
!.......................................................................
  lb = fio%lb
  ub = fio%ub 
  allocate (f1(ub(1)-lb(1),ub(2)-lb(2),ub(3)-lb(3)))
  f1=f(lb(1):ub(1)-1,lb(2):ub(2)-1,lb(3):ub(3)-1)
  write (fio%unit,rec=rec) f1
  deallocate (f1)
END
!-----------------------------------------------------------------------
SUBROUTINE file_read_seq_particles_real (x, fio, isp)
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type)           :: fio
  integer, intent(in)          :: isp
  real(kind=4), dimension(fio%np(isp)) :: x
!.......................................................................
  call error('file_read_seq_particles_real','should never be called when running in sequential mode')
END SUBROUTINE file_read_seq_particles_real
!-----------------------------------------------------------------------
SUBROUTINE file_read_seq_particles_int8 (x, fio, isp)
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type)                      :: fio
  integer, intent(in)                     :: isp
  integer(kind=8), dimension(fio%np(isp)) :: x
!.......................................................................
  call error('file_read_seq_particles_int8','should never be called when running in sequential mode')
END SUBROUTINE file_read_seq_particles_int8
!-----------------------------------------------------------------------
SUBROUTINE file_read_seq_particles_int2 (x, fio, isp)
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type)                      :: fio
  integer, intent(in)                     :: isp
  integer(kind=2), dimension(fio%np(isp)) :: x
!.......................................................................
  call error('file_read_seq_particles_int2','should never be called when running in sequential mode')
END SUBROUTINE file_read_seq_particles_int2
!-----------------------------------------------------------------------
SUBROUTINE file_read_seq (f,fio)
  USE grid_m, only : g
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type) :: fio
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
!.......................................................................
  call error('file_read_seq','should never be called when running in sequential mode')
END SUBROUTINE file_read_seq
!-----------------------------------------------------------------------
SUBROUTINE file_write_seq_particles_real (x, fio, isp)
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type)           :: fio
  integer, intent(in)          :: isp
  real(kind=4), dimension(fio%np(isp)) :: x
!.......................................................................
  call error('file_write_seq_particles_real','should never be called when running in sequential mode')
END SUBROUTINE file_write_seq_particles_real
!-----------------------------------------------------------------------
SUBROUTINE file_write_seq_particles_int8 (x, fio, isp)
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type)                      :: fio
  integer, intent(in)                     :: isp
  integer(kind=8), dimension(fio%np(isp)) :: x
!.......................................................................
  call error('file_write_seq_particles_int8','should never be called when running in sequential mode')
END SUBROUTINE file_write_seq_particles_int8
!-----------------------------------------------------------------------
SUBROUTINE file_write_seq_particles_int2 (x, fio, isp)
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type)                      :: fio
  integer, intent(in)                     :: isp
  integer(kind=2), dimension(fio%np(isp)) :: x
!.......................................................................
  call error('file_write_seq_particles_int2','should never be called when running in sequential mode')
END SUBROUTINE file_write_seq_particles_int2
!-----------------------------------------------------------------------
SUBROUTINE file_write_seq (f,fio)
  USE grid_m, only : g
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type) :: fio
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
!.......................................................................
  call error('file_write_seq','should never be called when running in sequential mode')
END SUBROUTINE file_write_seq
!-----------------------------------------------------------------------
SUBROUTINE file_read (f, rec, fio)
  USE params, only: data_unit, mpi
  USE grid_m, only: g
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type) :: fio
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  real(kind=4), allocatable, dimension(:,:,:):: f1
  integer rec, lb(3), ub(3), i
!
  lb = fio%lb
  ub = fio%ub 
  allocate (f1(ub(1)-lb(1),ub(2)-lb(2),ub(3)-lb(3)))
  read (fio%unit,rec=rec) f1
  f(lb(1):ub(1)-1,lb(2):ub(2)-1,lb(3):ub(3)-1)=f1
  deallocate (f1)
  call overlap(f)
END
!-----------------------------------------------------------------------
SUBROUTINE file_close(fio)
  USE pic_io, only : file_io_type
  USE params, only: data_unit
  implicit none
  type(file_io_type) :: fio
  close (fio%unit)
END
!-----------------------------------------------------------------------
FUNCTION flag(file)
  USE params, only: data_unit, master
  implicit none
  character(len=*) file
  logical flag
!-----------------------------------------------------------------------
  if (master) then
    inquire (file=file//'.flag',exist=flag)
    if (flag) then
      open (data_unit,file=file//'.flag',status='old')
      close (data_unit,status='delete')
    end if
  end if
END
!-----------------------------------------------------------------------
SUBROUTINE mpi_stats
  implicit none
END SUBROUTINE mpi_stats
!-----------------------------------------------------------------------
SUBROUTINE finalize_mpi_stats
  implicit none
END SUBROUTINE finalize_mpi_stats
!===============================================================================
LOGICAL FUNCTION any_mpi (bad)
  implicit none
  logical bad
  any_mpi = bad
END FUNCTION
!===============================================================================
LOGICAL FUNCTION all_mpi (ok)
  implicit none
  logical ok
  all_mpi = ok
END FUNCTION
!===============================================================================

