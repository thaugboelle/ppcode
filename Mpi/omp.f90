! $Id$
! vim: nowrap
!-----------------------------------------------------------------------
! Initialize OpenMP.  The use of a 'threadprivate' common block ensures
! that each thread has a private value of that variable -- the thread
! number.
!=======================================================================
SUBROUTINE init_omp
  USE params, only: romp, nomp
#if defined (_OPENMP)
  USE omp_lib, only : omp_get_thread_num
!.......................................................................
  nomp = 0
  !$omp parallel
  romp = omp_get_thread_num() + 1
  !$omp atomic
  nomp = nomp + 1
  !$omp end parallel
#else
  romp = 1
  nomp = 1
#endif
END SUBROUTINE

