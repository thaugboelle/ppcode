! Mpi/mpi.f90 $Id$
! vim: nowrap
!-----------------------------------------------------------------------
MODULE PhotonPlasmaMpi
  implicit none
  include 'mpif.h'

  integer               :: verbose=0                                    ! amount of dbg : 1->one line, 2->each send
  integer               :: status(MPI_STATUS_SIZE)                      ! Communicator id
  integer               :: request1,request2,request3,request4,&
                           request5,request6,request7,request8
  integer               :: MPI_REAL_ENERGY                              ! correct type for energies
  integer               :: CMPI_REAL                                    ! correct type for sending reals
  integer               :: pa_t  
  integer               :: barrier_count                                ! number of barriers passed
  integer               :: err
  ! Send and receive tags should be seprated by 1000.000 (max nr of perceived nodes in the near future)
  integer, parameter    :: max_nr_of_nodes = 1024*1024
  integer, parameter    :: send_indices_tag = max_nr_of_nodes, iorank_tag = max_nr_of_nodes
END MODULE PhotonPlasmaMpi
!-----------------------------------------------------------------------
MODULE comm_arrays
  USE params, only: rank,comm,mpi_err,mpi 
  USE PhotonPlasmaMPI
  implicit none
  private
  public:: comm_array,wait_requests
  INTERFACE comm_array
    MODULE PROCEDURE comm_array1d,comm_array2d,comm_array3d
  END INTERFACE
CONTAINS
!-----------------------------------------------------------------------
FUNCTION wait_requests(requests,block)                                  ! Returns request nr
  implicit none
  integer                           :: wait_requests                    ! that have finished
  integer, dimension(:), intent(in) :: requests                         ! Int arr containing reqs
  logical, optional                 :: block                            ! Block until all done?
  integer                           :: reqsz
  integer, dimension(:,:), allocatable :: statusall
  wait_requests = 0
  reqsz = size(requests)
  if (sum(requests)==reqsz*MPI_REQUEST_NULL) return
  if (.not. present(block)) then                                        ! Wait for one to finish
      if (verbose>2) print*,'wait_requests: MPI_WAITANY',rank
    call MPI_WAITANY(reqsz,requests,wait_requests,status,mpi_err)
  else
    allocate(statusall(MPI_STATUS_SIZE,reqsz))                          ! Wait for all to finish
      if (verbose>2) print*,'wait_requests: MPI_WAITALL',rank
    call MPI_WAITALL(reqsz,requests,statusall,mpi_err)
    deallocate(statusall)
    wait_requests = mpi_err
  endif
END FUNCTION wait_requests
!-----------------------------------------------------------------------
SUBROUTINE comm_array1d(dir,ndn,nup,scr1,scr2,scr3,scr4,req1,req2,req3,req4)
  USE params, only: trace, stdout, nodes
  implicit none
  integer,              intent(in) :: dir                               ! Communication direction
  integer,              intent(in) :: ndn                               ! Size of down ghostzone
  integer,              intent(in) :: nup                               ! Size of up ghostzone
  real, dimension(ndn), intent(in) :: scr1                              ! Array to send to down
  real, dimension(nup), intent(in) :: scr2                              ! Array to send to up
  real, dimension(ndn)             :: scr3                              ! Array to recv from down
  real, dimension(nup)             :: scr4                              ! Array to recv from up
  integer               :: req1,req2,req3,req4             ! Related requests
  req1 = MPI_REQUEST_NULL; req2 = MPI_REQUEST_NULL
  req3 = MPI_REQUEST_NULL; req4 = MPI_REQUEST_NULL
  if (trace) write(stdout,*) 'Entering comm_array1d', rank
  if (mpi%interior(dir)) then                                           ! Interior node
    call MPI_ISEND(scr1,ndn,CMPI_REAL,mpi%dn(dir),rank,       comm,req1,mpi_err)! send lower
    call MPI_IRECV(scr3,ndn,CMPI_REAL,mpi%up(dir),mpi%up(dir),comm,req3,mpi_err)! recv lower
    call MPI_ISEND(scr2,nup,CMPI_REAL,mpi%up(dir),rank,       comm,req2,mpi_err)! send upper
    call MPI_IRECV(scr4,nup,CMPI_REAL,mpi%dn(dir),mpi%dn(dir),comm,req4,mpi_err)! recv upper
  else if (mpi%n(dir).gt.1) then                                        ! node==1 -> no comm
   if (mpi%me(dir).eq.0) then                                           ! first node
    call MPI_ISEND(scr2,nup,CMPI_REAL,mpi%up(dir),rank,       comm,req2,mpi_err)! send upper
    call MPI_IRECV(scr3,ndn,CMPI_REAL,mpi%up(dir),mpi%up(dir),comm,req4,mpi_err)! recv upper
   else                                                                 ! last node
    call MPI_ISEND(scr1,ndn,CMPI_REAL,mpi%dn(dir),rank,       comm,req1,mpi_err)! send lower
    call MPI_IRECV(scr4,nup,CMPI_REAL,mpi%dn(dir),mpi%dn(dir),comm,req3,mpi_err)! recv lower
   endif
  endif
  if (trace) write(stdout,*) 'Exiting comm_array1d'
END SUBROUTINE comm_array1d
!-----------------------------------------------------------------------
SUBROUTINE comm_array2d(dir,ndn,nup,scr1,scr2,scr3,scr4,req1,req2,req3,req4)
  USE params, only: trace, stdout, nodes
  implicit none
  integer,                        intent(in) :: dir                     ! Communication direction
  integer, dimension(2),          intent(in) :: ndn                     ! Size of down ghostzone
  integer, dimension(2),          intent(in) :: nup                     ! Size of up ghostzone
  real, dimension(ndn(1),ndn(2)), intent(in) :: scr1                    ! Array to send to down
  real, dimension(nup(1),nup(2)), intent(in) :: scr2                    ! Array to send to up
  real, dimension(ndn(1),ndn(2))             :: scr3                    ! Array to recv from down
  real, dimension(nup(1),nup(2))             :: scr4                    ! Array to recv from up
  integer                                    :: req1,req2,req3,req4     ! Related requests
  integer                                    :: npdn,npup
  npdn = product(ndn);     npup = product(nup)
  req1 = MPI_REQUEST_NULL; req2 = MPI_REQUEST_NULL
  req3 = MPI_REQUEST_NULL; req4 = MPI_REQUEST_NULL
  if (trace) write(stdout,*) 'Entering comm_array2d', rank
  if (mpi%interior(dir)) then                                           ! Interior node
      if (verbose>1) print 1,'thread',rank,' sending',npdn,' words to  ',mpi%dn(dir)
1     format(1x,a,i7,a,i6,a,i4)
    call MPI_ISEND(scr1,npdn,CMPI_REAL,mpi%dn(dir),rank,       comm,req1,mpi_err)! send lower
      if (verbose>1) print 1,'thread',rank,' recving',npdn,' words from',mpi%up(dir)
    call MPI_IRECV(scr3,npdn,CMPI_REAL,mpi%up(dir),mpi%up(dir),comm,req2,mpi_err)! recv upper
      if (verbose>1) print 1,'thread',rank,' sending',npup,' words to  ',mpi%up(dir)
    call MPI_ISEND(scr2,npup,CMPI_REAL,mpi%up(dir),rank,       comm,req3,mpi_err)! send upper
      if (verbose>1) print 1,'thread',rank,' recving',npup,' words from',mpi%dn(dir)
    call MPI_IRECV(scr4,npup,CMPI_REAL,mpi%dn(dir),mpi%dn(dir),comm,req4,mpi_err)! recv lower
  else if (mpi%n(dir).gt.1) then                                        ! node==1 -> no comm
   if (mpi%me(dir).eq.0) then                                           ! first node
      if (verbose>1) print 1,'thread',rank,' sending',npup,' words to  ',mpi%up(dir)
    call MPI_ISEND(scr2,npup,CMPI_REAL,mpi%up(dir),rank,       comm,req2,mpi_err)! send upper
      if (verbose>1) print 1,'thread',rank,' recving',npdn,' words from',mpi%up(dir)
    call MPI_IRECV(scr3,npdn,CMPI_REAL,mpi%up(dir),mpi%up(dir),comm,req4,mpi_err)! recv upper
   else                                                                 ! last node
      if (verbose>1) print 1,'thread',rank,' sending',npup,' words to  ',mpi%up(dir)
    call MPI_ISEND(scr1,npdn,CMPI_REAL,mpi%dn(dir),rank,       comm,req1,mpi_err)! send lower
      if (verbose>1) print 1,'thread',rank,' recving',npup,' words from',mpi%dn(dir)
    call MPI_IRECV(scr4,npup,CMPI_REAL,mpi%dn(dir),mpi%dn(dir),comm,req3,mpi_err)! recv lower
   endif
  endif
  if (trace) write(stdout,*) 'Exiting comm_array2d'
END SUBROUTINE comm_array2d
!-----------------------------------------------------------------------
SUBROUTINE comm_array3d(dir,ndn,nup,scr1,scr2,scr3,scr4,req1,req2,req3,req4)
  USE params, only: trace, stdout, nodes
  implicit none
  integer,                intent(in) :: dir                             ! Communication direction
  integer, dimension(3),  intent(in) :: ndn                             ! Size of down ghostzone
  integer, dimension(3),  intent(in) :: nup                             ! Size of up ghostzone
  real, dimension(ndn(1),ndn(2),ndn(3)), intent(in) :: scr1             ! Array to send to down
  real, dimension(nup(1),nup(2),nup(3)), intent(in) :: scr2             ! Array to send to up
  real, dimension(ndn(1),nup(2),ndn(3))             :: scr3             ! Array to recv from up
  real, dimension(nup(1),nup(2),nup(3))             :: scr4             ! Array to recv from down
  integer                            :: req1,req2,req3,req4             ! Related requests
  integer                            :: npdn,npup
  npdn = product(ndn);     npup = product(nup)
  req1 = MPI_REQUEST_NULL; req2 = MPI_REQUEST_NULL
  req3 = MPI_REQUEST_NULL; req4 = MPI_REQUEST_NULL
  if (mpi%interior(dir)) then                                           ! Interior node
      if (verbose>1) print *,'Doing direction :', dir
      if (verbose>1) print 1,'thread',rank,' sending',npdn,' words dn to  ',mpi%dn(dir)
    call MPI_ISEND(scr1,npdn,CMPI_REAL,mpi%dn(dir),rank,       comm,req1,mpi_err)! send lower
      if (verbose>1) print 1,'thread',rank,' recving',npdn,' words up from',mpi%up(dir)
    call MPI_IRECV(scr3,npdn,CMPI_REAL,mpi%up(dir),mpi%up(dir),comm,req2,mpi_err)! recv upper
      if (verbose>1) print 1,'thread',rank,' sending',npup,' words up to  ',mpi%up(dir)
    call MPI_ISEND(scr2,npup,CMPI_REAL,mpi%up(dir),rank,       comm,req3,mpi_err)! send upper
      if (verbose>1) print 1,'thread',rank,' recving',npup,' words dn from',mpi%dn(dir)
    call MPI_IRECV(scr4,npup,CMPI_REAL,mpi%dn(dir),mpi%dn(dir),comm,req4,mpi_err)! recv lower
  else if (mpi%n(dir).gt.1) then                                        ! node==1 -> no comm
   if (mpi%me(dir).eq.0) then                                           ! first node
      if (verbose>1) print 1,'thread',rank,' sending',npup,' words up to  ',mpi%up(dir)
    call MPI_ISEND(scr2,npup,CMPI_REAL,mpi%up(dir),rank,       comm,req2,mpi_err)! send upper
      if (verbose>1) print 1,'thread',rank,' recving',npdn,' words up from',mpi%up(dir)
    call MPI_IRECV(scr3,npdn,CMPI_REAL,mpi%up(dir),mpi%up(dir),comm,req4,mpi_err)! recv upper
   else                                                                 ! last node
      if (verbose>1) print 1,'thread',rank,' sending',npdn,' words dn to  ',mpi%dn(dir)
    call MPI_ISEND(scr1,npdn,CMPI_REAL,mpi%dn(dir),rank,       comm,req1,mpi_err)! send lower
      if (verbose>1) print 1,'thread',rank,' recving',npup,' words dn from',mpi%dn(dir)
    call MPI_IRECV(scr4,npup,CMPI_REAL,mpi%dn(dir),mpi%dn(dir),comm,req3,mpi_err)! recv lower
   endif
  endif
1 format(1x,a,i7,a,i6,a,i4)
END SUBROUTINE comm_array3d
!-----------------------------------------------------------------------
END MODULE comm_arrays
!-----------------------------------------------------------------------
SUBROUTINE Barrier
  USE PhotonPlasmaMpi, only: barrier_count
  USE params, only : mpi_err,comm,rank,nodes
  USE debug, only: debugit, dbg_mpi
  implicit none
  real a,b
  integer, save :: c=0, d=4500
!.......................................................................
  if (nodes.eq.1) return
  barrier_count = barrier_count + 1
  if (debugit(dbg_mpi,0)) &
    print*,'BARRIER: rank,count =',rank,barrier_count
  !c=c+1
  !if (c .gt. d) write(*,'(a,i2,a,i4)') "Rank=",rank," Barrier #",c
  !a=0. ; call MPI_ALLREDUCE(a,b,1,CMPI_REAL,MPI_SUM,comm,mpi_err)
  !if (c .gt. d) call sleep(1)
  call MPI_BARRIER(comm,mpi_err)
  !if (rank==0 .and. c .gt. d) print *,'----------------------'
  !if (c .gt. d) call sleep(1)
  call assert(mpi_err==0,'barrier','MPI_BARRIER')
END SUBROUTINE Barrier
!-----------------------------------------------------------------------
SUBROUTINE barrier_trace (label)
  USE PhotonPlasmaMpi, only: barrier_count
  USE params, only : mpi_err,comm,rank,nodes
  USE debug, only: debugit, dbg_mpi
  implicit none
  character(len=*) label
!.......................................................................
  if (nodes.eq.1) return
  barrier_count = barrier_count + 1
  if (debugit(dbg_mpi,0)) &
    print*,'BARRIER: rank,count =',trim(label),rank,barrier_count
  call MPI_BARRIER(comm,mpi_err)
  call assert(mpi_err==0,'barrier','MPI_BARRIER')
END SUBROUTINE barrier_trace
!-----------------------------------------------------------------------
SUBROUTINE read_mpi
  USE params, only : stdin, stdout, master, nodes, out_namelists, &
                     mpi_params=>mpi, params_unit
  implicit none
  integer               :: nx, ny, nz, onx, ony, onz, sz
  real                  :: power
  integer               :: n(3)
  real                  :: s(3), ds(3), xmin, ymin, zmin, b0, e0, fdebye, fskin
  logical               :: problem
  namelist /mpi/ nx, ny, nz
  namelist /grid/ n, s, ds, xmin, ymin, zmin, b0, e0, fdebye, fskin

  nx=0; ny=0; nz=0
  rewind (stdin); read (stdin,mpi)
  rewind (stdin); read (stdin,grid)
  if (n(1) == 1) nx=1
  if (n(2) == 1) ny=1
  if (n(3) == 1) nz=1
  if (out_namelists) write (stdout,mpi)

  onx=nx; ony=ny; onz=nz                                                ! store for dbg printout

  ! check that the explicitly chosen sizes are divisor in the total number of threads
  problem = .false.
  if (nx > 0 .and. mod(nodes,nx) > 0) then
    if (master) write(stdout,*) 'mpi%n(1) is not a divisor in nodes. Check your mpi namelist.'
    nx = 0; problem = .true.
  endif
  if (ny > 0 .and. mod(nodes,ny) > 0) then
    if (master) write(stdout,*) 'mpi%n(2) is not a divisor in nodes. Check your mpi namelist.'
    ny = 0; problem = .true.
  endif
  if (nz > 0 .and. mod(nodes,nz) > 0) then
    if (master) write(stdout,*) 'mpi%n(3) is not a divisor in nodes. Check your mpi namelist.'
    nz = 0; problem = .true.
  endif
  if (nx > 0 .and. ny > 0 .and. mod(nodes,nx*ny) > 0) then
    if (master) write(stdout,*) 'mpi%n(1)*mpi%n(2) is not a divisor in nodes. Check your mpi namelist.'
    nx = 0; ny = 0; problem = .true.
  endif
  if (nx > 0 .and. nz > 0 .and. mod(nodes,nx*nz) > 0) then
    if (master) write(stdout,*) 'mpi%n(1)*mpi%n(3) is not a divisor in nodes. Check your mpi namelist.'
    nx = 0; nz = 0; problem = .true.
  endif
  if (ny > 0 .and. nz > 0 .and. mod(nodes,ny*nz) > 0) then
    if (master) write(stdout,*) 'mpi%n(2)*mpi%n(3) is not a divisor in nodes. Check your mpi namelist.'
    ny = 0; nz = 0; problem = .true.
  endif
  if (nx > 0 .and. ny > 0 .and. nz >0 .and. nodes .ne. nx*ny*nz) then
    if (master) write(stdout,*) 'mpi%n(1)*mpi%n(2)*mpi%n(3) is not equal to nodes. Check your mpi namelist.'
    nx = 0; ny = 0; nz = 0; problem = .true.
  endif
  if (problem .and. master) then
    print *,'The size of the mpi grid is not compatible with the problem size'
    print *,'Chosen nx, ny, nz =', onx, ony, onz
    print *,'I will now try to find you a better choice!'
  endif

  ! rescale remaining number of threads to distribute
  sz = nodes
  if (nx > 0) sz = sz / nx
  if (ny > 0) sz = sz / ny
  if (nz > 0) sz = sz / nz

  ! find divisors in an (almost) optimal manner
  if (nz == 0) then
    power = 3.
    if (nx > 0) power = power-1
    if (ny > 0) power = power-1
    nz = max(floor((real(sz))**(1./power+0.001)),1)                     ! search for a suitable nz
    do while (mod(sz,nz) > 0)
      nz = nz-1
    end do
    sz = sz / nz
  end if

  if (ny == 0) then
    power = 2.
    if (nx > 0) power = power-1
    ny = max(floor((real(sz))**(1./power+0.001)),1)                     ! search for a suitable ny
    do while (mod(sz,ny) > 0)
      ny = ny-1
    end do
    sz = sz / ny
  end if

  if (nx == 0) nx = sz                                                  ! let the x-dir take the rest

  mpi_params%n = (/ nx, ny, nz /)

  if (problem .and. master) then
    write(stdout,'(a,3i5)') 'New geometry is: nx, ny, nz =', nx, ny, nz
  endif

  if (out_namelists) write (stdout,mpi)

END SUBROUTINE read_mpi

!=======================================================================
SUBROUTINE init_mpi
#if defined (_OPENMP)
  USE OMP_LIB, only : omp_get_num_threads
#endif
  USE params
  USE species, only : particle
  USE stat,    only : energy_kind
  USE debug,   only : debugit, dbg_mpi
  USE PhotonPlasmaMpi
#ifdef __xlc__
  USE iso_c_binding
#endif
  implicit none
  character(len=mid) :: id = "Mpi/mpi.f90 $Id$"
  logical, dimension(3) :: mpi_periodic                                 ! Periodic grid?
  logical               :: mpi_reorder                                  ! Reorder grid
  integer               :: i,mpi_provided
  real                  :: realvar
  integer               :: st1(3), st3(3), MpiSizeReal, MpiSizeInt2, MpiSizeInt8
  integer(KIND=MPI_ADDRESS_KIND) :: st2(3)
  type(particle)        :: pa1,pa2
!-----------------------------------------------------------------------
! Initialize OpenMP
!-----------------------------------------------------------------------
  call init_omp

!-----------------------------------------------------------------------
! select the correct data type for sending variables
! REMEMBER TO CORRECT PARTICLE_LENGTH BY HAND IN SPECIES IF CHANGED FROM REAL4
!-----------------------------------------------------------------------
  select case (kind(realvar))
  case(kind(1.0e0_4))
    CMPI_REAL = MPI_REAL4
    real_size = 4
  case(kind(1.0e0_8))
    CMPI_REAL = MPI_REAL8
    real_size = 8
 ! UNCOMMENT FOR QUAD PRECISION
 !case(kind(1.0e0_16))
 !  CMPI_REAL = MPI_REAL16
 !  real_size = 16
 ! UNCOMMENT FOR QUAD PRECISION
  endselect
!-----------------------------------------------------------------------
! Start up MPI and get number of nodes and our node (mpi_rank)
!-----------------------------------------------------------------------
#ifdef _OPENMP
  call MPI_INIT_THREAD(MPI_THREAD_MULTIPLE,mpi_provided,mpi_err)        ! Start MPI
#else
  call MPI_INIT (mpi_err)                                               ! Start MPI
#endif
  call MPI_COMM_SIZE (mpi_comm_world, nodes, mpi_err)                   ! Get number of nodes

  ! temporary fix of master defined through mpi_comm_world before calling init_stdio
  call MPI_COMM_RANK (mpi_comm_world, rank, mpi_err)                    ! Get node number in cart
  master = (rank == 0)

#ifdef _OPENMP
  if (omp_get_num_threads() > 1) then
    if (mpi_provided==MPI_THREAD_SINGLE) then
      if (master) print *, 'MPI implementation provides support for MPI calls outside openmp regions (MPI_THREAD_SINGLE)'
      if (master) print *, 'Error: This MPI implementation is not multi threaded enough for PP-code'
      stop
    endif
    if (mpi_provided==MPI_THREAD_FUNNELED) then
      if (master) print *, 'MPI implementation provides support for MPI calls in master regions (MPI_THREAD_FUNNELED)'
      if (master) print *, 'Error: This MPI implementation is not multi threaded enough for PP-code'
      stop
    endif
    if (mpi_provided==MPI_THREAD_SERIALIZED) then
      if (master) print *, 'MPI implementation provides support for serial MPI calls by any thread (MPI_THREAD_SERIALIZED)'
      if (master) print *, 'Error: This MPI implementation is not multi threaded enough for PP-code'
      stop
    endif
    if (mpi_provided==MPI_THREAD_MULTIPLE .and. master) &
      print *, 'MPI implementation provides support for simultaneous MPI calls by different threads (MPI_THREAD_MULTIPLE)'
  endif
#endif

  mpi%pnodes=1; do while (nodes > mpi%pnodes); mpi%pnodes = 2*mpi%pnodes; enddo ! pnodes=2**ceil(log_2(nodes))

  call set_master_in_header(master)                                     ! comm master to header module (clumsy work-around)

  call init_stdio(.true.)

!-----------------------------------------------------------------------
! Compute mpi dimensions, our place in the node space, and our neighbors
!-----------------------------------------------------------------------
  call read_mpi

  mpi_reorder  = .false.                                                ! Reorder grid to optimise
  mpi_periodic = .true.                                                 ! hardware layout
  call MPI_CART_CREATE (mpi_comm_world, 3, mpi%n, mpi_periodic, mpi_reorder, comm,  mpi_err)
  call MPI_COMM_RANK   (comm, rank, mpi_err)                            ! Get node number in cart
  call MPI_CART_COORDS (comm, rank, 3, mpi%me, mpi_err)

  do i=0,2 
    call MPI_CART_SHIFT (comm, i, 1, mpi%dn(i+1), mpi%up(i+1), mpi_err) ! Rank of neighbour nodes 
  end do
  mpi%lb = mpi%me == 0                                                  ! set 3d thread geometry bndry
  mpi%ub = mpi%me == mpi%n-1
  mpi%interior = .not. (mpi%lb .or. mpi%ub)                             ! Interior node?

  master = rank==0                                                      ! master is per def rank=0
  
!-----------------------------------------------------------------------
!  Allow printout only on the master node, unless out_ranks is set
!-----------------------------------------------------------------------
  if (master) then
    write(stdout,*) id
    write (stdout,'(a,i7)') ' Number of MPI threads:', nodes
    write(stdout,'(1x,a)') hl
  end if

!-----------------------------------------------------------------------
! Setup communicators for different subsets of the nodes
!-----------------------------------------------------------------------
  ! set up mpi group for communication in an x-pencil
  mpi%x%nodes = mpi%n(1)
  call MPI_COMM_SPLIT  (comm, mpi%me(2) + mpi%me(3)*mpi%n(2),mpi%me(1),mpi%x%comm,mpi_err)
  call MPI_COMM_RANK   (mpi%x%comm, mpi%x%rank, mpi_err)
  mpi%x%master = (mpi%x%rank == 0)
  ! set up mpi group for communication in an y-pencil
  mpi%y%nodes = mpi%n(2)
  call MPI_COMM_SPLIT  (comm, mpi%me(1) + mpi%me(3)*mpi%n(1),mpi%me(2),mpi%y%comm,mpi_err)
  call MPI_COMM_RANK   (mpi%y%comm, mpi%y%rank, mpi_err)
  mpi%y%master = (mpi%y%rank == 0)
  ! set up mpi group for communication in an z-pencil
  mpi%z%nodes = mpi%n(3)
  call MPI_COMM_SPLIT  (comm, mpi%me(1) + mpi%me(2)*mpi%n(1),mpi%me(3),mpi%z%comm,mpi_err)
  call MPI_COMM_RANK   (mpi%z%comm, mpi%z%rank, mpi_err)
  mpi%z%master = (mpi%z%rank == 0)
  ! set up mpi group for communication in an xy-plane
  mpi%xy%nodes = mpi%n(1) * mpi%n(2)
  call MPI_COMM_SPLIT  (comm, mpi%me(3),mpi%me(1)+mpi%me(2)*mpi%n(1),mpi%xy%comm,mpi_err)
  call MPI_COMM_RANK   (mpi%xy%comm, mpi%xy%rank, mpi_err)
  mpi%xy%master = (mpi%xy%rank == 0)
  ! set up mpi group for communication in an yz-plane
  mpi%yz%nodes = mpi%n(2) * mpi%n(3)
  call MPI_COMM_SPLIT  (comm, mpi%me(1),mpi%me(2)+mpi%me(3)*mpi%n(2),mpi%yz%comm,mpi_err)
  call MPI_COMM_RANK   (mpi%yz%comm, mpi%yz%rank, mpi_err)
  mpi%yz%master = (mpi%yz%rank == 0)
  ! set up mpi group for communication in an zx-plane
  mpi%zx%nodes = mpi%n(3) * mpi%n(1)
  call MPI_COMM_SPLIT  (comm, mpi%me(2),mpi%me(3)+mpi%me(1)*mpi%n(3),mpi%zx%comm,mpi_err)
  call MPI_COMM_RANK   (mpi%zx%comm, mpi%zx%rank, mpi_err)
  mpi%zx%master = (mpi%zx%rank == 0)

!-----------------------------------------------------------------------
! dump in a text file the logical and physical layout of threads
!-----------------------------------------------------------------------
#ifdef __xlc__
  call check_host_order_BGQ()
#endif

  mpi%pset_d = mpi%y
  mpi%pset_s = mpi%zx

!-----------------------------------------------------------------------
! this 2nd call is necessary, because FIXME: ??
!-----------------------------------------------------------------------
  call init_stdio(.false.)                                                      

!-----------------------------------------------------------------------
!  Allow printout only on the master node, unless out_ranks is set
!-----------------------------------------------------------------------
  if (.not. master) then
    stdout = -1
  end if
  ! now stdin and stdout are properly defined
  call init_run_info                                                    ! set out_namelists

  do i=1,3
    if (mpi%dn(i) < 0) then
      call warning_all_nodes('init_mpi', 'mpi%dn(i) less than zero')
      write (stdall,'(a,4i7)') 'init_mpi:', rank, i, mpi%dn(i), mpi%up(i)
    end if
    if (mpi%up(i) < 0) then
      call warning_all_nodes('init_mpi', 'mpi%up(i) less than zero')
      write (stdall,'(a,4i7)') 'init_mpi:', rank, i, mpi%dn(i), mpi%up(i)
    end if
    if (debugit(dbg_mpi,1)) &
      write (stdall,'(a,4i7)') 'init_mpi:', rank, i, mpi%dn(i), mpi%up(i)
  enddo

!-----------------------------------------------------------------------
! select the correct data type for sending energy variables
!-----------------------------------------------------------------------
  select case (energy_kind)
  case(kind(1.0e0_4))
    MPI_REAL_ENERGY = MPI_REAL4
  case(kind(1.0e0_8))
    MPI_REAL_ENERGY = MPI_REAL8
  ! UNCOMMENT FOR QUAD PRECISION
  !case(kind(1.0e0_16))
  !  MPI_REAL_ENERGY = MPI_REAL16
  ! UNCOMMENT FOR QUAD PRECISION
  endselect

!-----------------------------------------------------------------------
! construct compound type for sending particles
!-----------------------------------------------------------------------
  call MPI_TYPE_EXTENT(CMPI_REAL,   MpiSizeReal,err)                    ! Size of real in bytes
  call MPI_TYPE_EXTENT(MPI_INTEGER2,MpiSizeInt2,err)                    ! Size of int2 in bytes
  call MPI_TYPE_EXTENT(MPI_INTEGER8,MpiSizeInt8,err)                    ! Size of int8 in bytes
  st1 = (/mdim+1+mcoord+1,mdim+1,1/)                                    ! Number of elements in each type
  st2 = (/0,(mdim+1+mcoord+1)*MpiSizeReal,(mdim+1+mcoord+1)*MpiSizeReal+4*MpiSizeInt2/) ! offsets in bytes
  st3 = (/CMPI_REAL,MPI_INTEGER2,MPI_INTEGER8/)                         ! Data types for each block
  
  call MPI_TYPE_CREATE_STRUCT(3,st1,st2,st3,pa_t,err)                   ! Commit particle data type
  call MPI_TYPE_COMMIT(pa_t,err)

END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE species_communicators
  USE params,  only : nspecies, mpi, comm, master, rank, nodes
  USE PhotonPlasmaMpi
  integer :: isp
  allocate(mpi%sp(nspecies))
  do isp=1,nspecies
    call MPI_COMM_DUP(comm, mpi%sp(isp)%comm,err)
    mpi%sp(isp)%rank   = rank
    mpi%sp(isp)%master = master
    mpi%sp(isp)%nodes  = nodes
  enddo
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE finalize_mpi(finalize_flag)
  USE params,          only : comm
  USE PhotonPlasmaMPI, only : err, pa_t 
  implicit none
  logical           :: finalize_flag
  integer           :: error_code

  if (finalize_flag) then
    call MPI_TYPE_FREE(pa_t, err)
    call MPI_FINALIZE (err) 
  else
    error_code = 127
    call MPI_ABORT(comm, error_code, err)
  endif
  stop
END SUBROUTINE finalize_mpi
!-----------------------------------------------------------------------
SUBROUTINE ParticleTotal
  USE PhotonPlasmaMPI
  USE params,  only : nspecies,comm,mpi_err,nodes
  USE species, only : sp
  implicit none
  integer(kind=8), allocatable, dimension(:) :: ptl,pt
  if (nspecies == 0) return
  if (nodes.gt.1) then
    allocate(pt(nspecies),ptl(nspecies))
    ptl = sp(:)%np
    call MPI_ALLREDUCE(ptl,pt,nspecies,MPI_INTEGER8,MPI_SUM,mpi_comm_world,mpi_err)
    sp%tnp = pt
    deallocate(pt,ptl)
  else
    sp%tnp = sp%np
  endif
END SUBROUTINE ParticleTotal
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_integers (n, loc)
  USE PhotonPlasmaMPI
  USE params,  only : rank,comm,mpi_err,nodes
  implicit none
  integer, intent(in) :: n
  integer             :: loc(n), glob(n)
!.......................................................................
  if (nodes.gt.1) then
    call MPI_ALLREDUCE(loc,glob,n,MPI_INTEGER,MPI_SUM,comm,mpi_err)
    loc = glob
  endif
END SUBROUTINE mpi_sum_integers
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_integer (loc)
  implicit none
  integer :: loc, lloc(1)
!.......................................................................
  lloc(1) = loc
  call mpi_sum_integers (1, lloc)
  loc = lloc(1)
END SUBROUTINE mpi_sum_integer
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_integer8s (n, loc)
  USE PhotonPlasmaMPI
  USE params,  only : rank,comm,mpi_err,mpi,nodes
  implicit none
  integer,         intent(in)   :: n
  integer(kind=8), dimension(n) :: loc,glob
!.......................................................................
  if (nodes.gt.1) then
    call MPI_ALLREDUCE(loc,glob,n,MPI_INTEGER8,MPI_SUM,comm,mpi_err)
    loc = glob
  endif
END SUBROUTINE mpi_sum_integer8s
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_integer8 (loc)
  implicit none
  integer(kind=8) :: loc, lloc(1)
!.......................................................................
  lloc(1) = loc
  call mpi_sum_integer8s (1, lloc)
  loc = lloc(1)
END SUBROUTINE mpi_sum_integer8
!-----------------------------------------------------------------------
SUBROUTINE mpi_max_integer8 (loc)
  USE PhotonPlasmaMPI
  USE params,  only : comm,mpi_err,mpi,nodes
  implicit none
  integer(kind=8) :: loc,glob

  if (nodes.gt.1) then
    call MPI_ALLREDUCE(loc,glob,1,MPI_INTEGER8,MPI_MAX,comm,mpi_err)
    loc = glob
  endif
END SUBROUTINE mpi_max_integer8
!-----------------------------------------------------------------------
SUBROUTINE mpi_min_integer8 (loc)
  USE PhotonPlasmaMPI
  USE params,  only : comm,mpi_err,mpi,nodes
  implicit none
  integer(kind=8) :: loc,glob

  if (nodes.gt.1) then
    call MPI_ALLREDUCE(loc,glob,1,MPI_INTEGER8,MPI_MIN,comm,mpi_err)
    loc = glob
  endif
END SUBROUTINE mpi_min_integer8
!-----------------------------------------------------------------------
SUBROUTINE mpi_max_reals (n, loc)
  USE PhotonPlasmaMPI
  USE params,  only : comm,mpi_err,mpi,nodes
  implicit none
  integer :: n
  real    :: loc(n), glob(n)

  if (nodes.gt.1) then
    call MPI_ALLREDUCE(loc,glob,n,CMPI_REAL,MPI_MAX,comm,mpi_err)
    loc = glob
  endif
END SUBROUTINE mpi_max_reals
!-----------------------------------------------------------------------
SUBROUTINE mpi_max_real(loc)
  implicit none
  real :: loc, lloc(1)
  lloc(1) = loc
  call mpi_max_reals (1, lloc)
  loc = lloc(1)
END SUBROUTINE mpi_max_real
!-----------------------------------------------------------------------
SUBROUTINE mpi_min_reals (n, loc)
  USE PhotonPlasmaMPI
  USE params,  only : comm,mpi_err,mpi,nodes
  implicit none
  integer :: n
  real    :: loc(n), glob(n)

  if (nodes.gt.1) then
    call MPI_ALLREDUCE(loc,glob,n,CMPI_REAL,MPI_MIN,comm,mpi_err)
    loc = glob
  endif
END SUBROUTINE mpi_min_reals
!-----------------------------------------------------------------------
SUBROUTINE mpi_min_real(loc)
  implicit none
  real :: loc, lloc(1)
  lloc(1) = loc
  call mpi_min_reals (1, lloc)
  loc = lloc(1)
END SUBROUTINE mpi_min_real
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_reals (n, loc)
  USE PhotonPlasmaMPI
  USE params,  only : rank,comm,mpi_err,nodes
  implicit none
  integer            :: n
  real, dimension(n) :: loc, glob
!.......................................................................
  if (nodes.gt.1) then
    call MPI_ALLREDUCE(loc,glob,n,CMPI_REAL,MPI_SUM,comm,mpi_err)
    loc = glob
  endif
END SUBROUTINE mpi_sum_reals
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_real (loc)
  implicit none
  real :: loc, lloc(1)
  lloc(1) = loc
  call mpi_sum_reals (1, lloc)
  loc = lloc(1)
END SUBROUTINE mpi_sum_real
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_real8s (n, loc)
  USE PhotonPlasmaMPI
  USE params,  only : rank,comm,mpi_err,nodes
  implicit none
  integer                    :: n
  real(kind=8), dimension(n) :: loc, glob
!.......................................................................
  if (nodes.gt.1) then
    call MPI_ALLREDUCE(loc,glob,n,CMPI_REAL,MPI_SUM,comm,mpi_err)
    loc = glob
  endif
END SUBROUTINE mpi_sum_real8s
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_real8 (loc)
  implicit none
  real(kind=8) :: loc, lloc(1)
  lloc(1) = loc
  call mpi_sum_real8s (1, lloc)
  loc = lloc(1)
END SUBROUTINE mpi_sum_real8
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_integer_xy (n, loc)
  USE PhotonPlasmaMPI
  USE params,  only : rank,mpi_err,mpi
  implicit none
  integer, intent(in)   :: n
  integer, dimension(n) :: loc,glob

  if (mpi%xy%nodes.gt.1) then
    call MPI_ALLREDUCE(loc,glob,n,MPI_INTEGER,MPI_SUM,mpi%xy%comm,mpi_err)
    loc = glob
  endif
END SUBROUTINE mpi_sum_integer_xy
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_integer8_xy (n, loc)
  USE PhotonPlasmaMPI
  USE params,  only : rank,mpi_err,mpi
  implicit none
  integer,         intent(in)   :: n
  integer(kind=8), dimension(n) :: loc,glob

  if (mpi%xy%nodes.gt.1) then
    call MPI_ALLREDUCE(loc,glob,n,MPI_INTEGER8,MPI_SUM,mpi%xy%comm,mpi_err)
    loc = glob
  endif
END SUBROUTINE mpi_sum_integer8_xy
!-----------------------------------------------------------------------
SUBROUTINE ZeroRequests
  USE PhotonPlasmaMPI
  implicit none
  request1 = MPI_REQUEST_NULL
  request2 = MPI_REQUEST_NULL
  request3 = MPI_REQUEST_NULL
  request4 = MPI_REQUEST_NULL
  request5 = MPI_REQUEST_NULL
  request6 = MPI_REQUEST_NULL
  request7 = MPI_REQUEST_NULL
  request8 = MPI_REQUEST_NULL
END SUBROUTINE ZeroRequests
!-----------------------------------------------------------------------
SUBROUTINE SendParticles_lowmem(gmax)
  USE PhotonPlasmaMPI
  USE params ! only : nspecies,comm,mpi_err,nodes,rank,mpi,&
             !        interior,master,stdout,stdall,buffer_change
  USE species, only : sp,particle
  USE grid_m,    only : g
  USE debug,   only : ptrace, void=>verbose, debugit, dbg_mpi
  implicit none
  integer                                      :: gmax
  type(particle),    dimension(:), pointer     :: pa
  type(particle)                               :: pas
  type(particle),    dimension(:), allocatable :: pal1,pal2,pau1,pau2
  type list
    type(list), pointer                        :: next=>null(), prev=>null()
    integer                                    :: ip=-1
  end type
  type(list), pointer                          :: cl=>null(), cu=>null(), cw=>null()
  integer                                      :: ip,ip0,ip1,ip2,np,isp,i,ilb,iub,nmax,npal,npal1,npal2,npau,&
                                                  npau1,npau2,npaw,max_len
  integer,           dimension(2)              :: reqall
  integer,           dimension(MPI_STATUS_SIZE,2) :: statusall          ! Communicator id
  logical                                      :: ww,palb,paub,too_many_particles,gre_send,re_send
  integer                                      :: particles_needed, req_done
  integer(kind=2)                              :: glb,gub,gn,q
  integer(kind=8),   external                  :: mask_i_flags
  character(len=1), dimension(3)               :: cdir

  if (nodes.eq.1) then; gmax=0; return; endif                             ! If uni-cpu no particle sends
  call trace_enter('Send Particles')

  nmax  = 0
  cdir = (/ 'x', 'y', 'z' /)

  allocate(cl,cu,cw)

  do i=1,mdim
    if (mpi%n(i) == 1) cycle                                              ! nothing to send if only 1 thread in this dir
    gn = g%gn(i)                                                          ! global size
    ilb= g%lb(i) + mpi%offset(i)
    iub= g%ub(i) + mpi%offset(i)
    glb= g%ghlb(i)
    gub= g%gn(i) - g%ghub(i)
    if (mpi%lb(i)) ilb = ilb-10                                           ! don't ship
    if (mpi%ub(i)) iub = iub+10                                           ! don't ship
    do isp=1,nspecies
      gre_send = .true.
      do while (gre_send)
        re_send= .false.
        call ZeroRequests                                                   ! Zero request vars
        pa  => sp(isp)%particle
        np  =  sp(isp)%np
        !-------------------------------------------------------------------------
        ! clean out weightless particles 
        if (np > 0) then
          pa => sp(isp)%particle
          ip = 1
          do
            if (pa(ip)%w .eq. 0.0 .and. np>0) then
              pas = pa(np)
              pa(np) = pa(ip)
              pa(ip) = pas
              np = np - 1
            else
              ip = ip + 1
            endif
            if (ip == np .or. np == 0) exit
          enddo
        endif
        sp(isp)%np = np
        max_len = sp(isp)%mp / 10                                           ! max length of list is 10% of mp
        ip0 = 0                                                             ! Counter for lower bndry
        ip1 = 0                                                             ! Counter for upper bndry
        ip2 = 0                                                             ! Cntr for weightless parts
        !-------------------------------------------------------------------
        ! Make an array with the index of the particles we will transfer
        !-------------------------------------------------------------------
        if (periodic(i)) then
         if (mpi%me(i)==0) then                                             ! first node in direction i ?
          do ip=1,np
           ww = pa(ip)%w.ne.0.
           q = mod(pa(ip)%q(i),gn); q = merge(q,q+gn,q>=0)                  ! wrap integer position
           paub = q >= iub .and. ww                                         ! crossed upper bndry?
           palb = q >= gub .and. ww                                         ! crossed lower bndry?  CHECK!!
           paub = merge(.false.,paub,palb)                                  ! cannot cross both simulataneously
           if (palb) then
             if (ip0 < max_len) then
               ip0 = ip0 + 1                                                  ! add to nr of lb particles
               cl%ip = ip                                                     ! store ip
               if (.not. associated(cl%next)) allocate(cl%next)               ! check if we need more
               cl%next%prev => cl                                             ! advance in list
               cl => cl%next
             else
               re_send=.true.
               exit
             endif
           endif
           if (paub) then
             if (ip1 < max_len) then
               ip1 = ip1 + 1
               cu%ip = ip
               if (.not. associated(cu%next)) allocate(cu%next)
               cu%next%prev => cu
               cu => cu%next
             else
               re_send=.true.
               exit
             endif
           endif
           if (.not. ww) then
             if (ip2 < max_len) then
               ip2 = ip2 + 1
               cw%ip = ip
               if (.not. associated(cw%next)) allocate(cw%next)
               cw%next%prev => cw
               cw => cw%next
             else
               re_send=.true.
               exit
             endif
           endif
          enddo
         else if (mpi%me(i)==mpi%n(i)-1) then                               ! last node in direction i ?
          do ip=1,np
           ww = pa(ip)%w.ne.0.
           q = mod(pa(ip)%q(i),gn); q = merge(q,q+gn,q>=0)                  ! wrap integer position
           palb = q <  ilb .and. ww                                         ! crossed lower bndry?
           paub = q <= glb .and. ww                                         ! crossed upper bndry?  CHECK!!
           palb = merge(.false.,palb,paub)                                  ! cannot cross both simulataneously
           if (palb) then
             if (ip0 < max_len) then
               ip0 = ip0 + 1
               cl%ip = ip
               if (.not. associated(cl%next)) allocate(cl%next)
               cl%next%prev => cl
               cl => cl%next
             else
               re_send=.true.
               exit
             endif
           endif
           if (paub) then
             if (ip1 < max_len) then
               ip1 = ip1 + 1
               cu%ip = ip
               if (.not. associated(cu%next)) allocate(cu%next)
               cu%next%prev => cu
               cu => cu%next
             else
               re_send=.true.
               exit
             endif
           endif
           if (.not. ww) then
             if (ip2 < max_len) then
               ip2 = ip2 + 1
               cw%ip = ip
               if (.not. associated(cw%next)) allocate(cw%next)
               cw%next%prev => cw
               cw => cw%next
             else
               re_send=.true.
               exit
             endif
           endif
          enddo
         else                                                               ! interior node in direction i
          do ip=1,np
           ww = pa(ip)%w.ne.0.
           q = mod(pa(ip)%q(i),gn); q = merge(q,q+gn,q>=0)                  ! wrap integer position
           paub = q >= iub .and. ww
           palb = q <  ilb .and. ww
           if (palb) then
             if (ip0 < max_len) then
               ip0 = ip0 + 1
               cl%ip = ip
               if (.not. associated(cl%next)) allocate(cl%next)
               cl%next%prev => cl
               cl => cl%next
             else
               re_send=.true.
               exit
             endif
           endif
           if (paub) then
             if (ip1 < max_len) then
               ip1 = ip1 + 1
               cu%ip = ip
               if (.not. associated(cu%next)) allocate(cu%next)
               cu%next%prev => cu
               cu => cu%next
             else
               re_send=.true.
               exit
             endif
           endif
           if (.not. ww) then
             if (ip2 < max_len) then
               ip2 = ip2 + 1
               cw%ip = ip
               if (.not. associated(cw%next)) allocate(cw%next)
               cw%next%prev => cw
               cw => cw%next
             else
               re_send=.true.
               exit
             endif
           endif
          enddo
         endif
        else
         do ip=1,np
          ww   = pa(ip)%w.ne.0.                                             ! Particle weightless?
          palb = pa(ip)%q(i) <  ilb .and. ww                                ! crossed lower bndry?
          paub = pa(ip)%q(i) >= iub .and. ww                                ! crossed upper bndry?
          if (palb) then
            if (ip0 < max_len) then
              ip0 = ip0 + 1
              cl%ip = ip
              if (.not. associated(cl%next)) allocate(cl%next)
              cl%next%prev => cl
              cl => cl%next
            else
              re_send=.true.
              exit
            endif
          endif
          if (paub) then
            if (ip1 < max_len) then
              ip1 = ip1 + 1
              cu%ip = ip
              if (.not. associated(cu%next)) allocate(cu%next)
              cu%next%prev => cu
              cu => cu%next
            else
              re_send=.true.
              exit
            endif
          endif
          if (.not. ww) then
            if (ip2 < max_len) then
              ip2 = ip2 + 1
              cw%ip = ip
              if (.not. associated(cw%next)) allocate(cw%next)
              cw%next%prev => cw
              cw => cw%next
            else
              re_send=.true.
              exit
            endif
          endif
         enddo
         if (ip0 > 0 .and. mpi%lb(i)) then
           call warning_all_nodes('SendParticles-'//cdir(i), &
             'Trying to send particles downwards, even though we are at a physical lower bndry')
           ip0=0
           call rewind_list(cl)
         endif
         if (ip1 > 0 .and. mpi%ub(i)) then
           call warning_all_nodes('SendParticles-'//cdir(i), &
             'Trying to send particles upwards, even though we are at a physical upper bndry')
           ip1=0
           call rewind_list(cu)
         endif
        end if
        call MPI_ALLREDUCE(re_send,gre_send,1,MPI_LOGICAL,MPI_LOR,comm,mpi_err)  ! check if we have to re_send 
           
        ! Debug: trace if a single particle is shipped:
        !-------------------------------------------------------------------
        if (ptrace(1).eq.isp) then
          do ip=1,sp(isp)%mp
            if (pa(ip)%i .eq. ptrace(2)) then
              ww   = pa(ip)%w.ne.0.                                         ! Particle weightless?
              palb = (pa(ip)%q(i) < ilb).and.ww                             ! crossed lower bndry?
              paub = (pa(ip)%q(i) > iub).and.ww                             ! crossed upper bndry?
              print '(a,i7.7,a,3l2,a,i1,a,i2,a,3i6,a,i8,3i6,4f6.3,i18)', &
                "Ptrace1: R=",rank," LB,UB,ERR=",palb,paub,ip>sp(isp)%np .and. ww," DIM=",i, &
                " SP=",isp, " lb,q,ub=",ilb,pa(ip)%q(i),iub," ip,q,r,w,i=",ip,pa(ip)%q,pa(ip)%r,pa(ip)%w,pa(ip)%i
            endif
          enddo
        endif
        npal1 = ip0                                                         ! nr of parts < lower bndry
        npau1 = ip1                                                         ! nr of parts > upper bndry
        npaw  = ip2                                                         ! nr of weightless parts
        nmax = max(npal1,npau1,nmax)                                        ! max nr of parts to send 
 
       !print'(a,3i4,2i3,3l3,3f9.3,2i5)','me,i,isp,,lb,ub,per',mpi%me,i,isp,mpi%lb(i),mpi%ub(i),periodic(i),lb,ub,g%grlb(i),npal1,npau1
        if (debugit(dbg_mpi,0) .or. verbose>0) &
         write(stdall,'(1x,a,2i3,i7,3i4,3i6,2i4,l3)') &
            'SendParticles: i,isp,rank,me,ndn,nup,nw0,dn,up,int =', &
            i,isp,rank,mpi%me,npal1,npau1,npaw,mpi%dn(i),mpi%up(i),mpi%interior(i)
        
        if (mpi%interior(i)) then                                           ! interior node?
          if (verbose>2) print *,'interior',rank
          call MPI_ISEND(npal1,1,MPI_INTEGER,mpi%dn(i),rank           ,comm,request1,mpi_err)   ! send lower
          call MPI_ISEND(npau1,1,MPI_INTEGER,mpi%up(i),rank+nodes     ,comm,request2,mpi_err)   ! send upper
          call MPI_IRECV(npal2,1,MPI_INTEGER,mpi%dn(i),mpi%dn(i)+nodes,comm,request3,mpi_err)   ! recieve lower
          call MPI_IRECV(npau2,1,MPI_INTEGER,mpi%up(i),mpi%up(i)      ,comm,request4,mpi_err)   ! recieve upper
          !call MPI_WAIT(request1,status,mpi_err)
          !call MPI_WAIT(request2,status,mpi_err)
          call MPI_WAIT(request3,status,mpi_err)
          call MPI_WAIT(request4,status,mpi_err)
        else
          if (mpi%me(i) == 0) then                                          ! lower edge. non-periodic
            if (verbose>2) print *,'rank=0',rank,mpi%dn(i),mpi%up(i)
            npal1 = 0                                                       ! no dnward transfer
            npal2 = 0
            call MPI_ISEND(npau1,1,MPI_INTEGER,mpi%up(i),rank+nodes,comm,request2,mpi_err) ! send upper
            call MPI_IRECV(npau2,1,MPI_INTEGER,mpi%up(i),mpi%up(i) ,comm,request4,mpi_err) ! recieve upper
            !call MPI_WAIT(request2,status,mpi_err)
            call MPI_WAIT(request4,status,mpi_err)
          else                                                              ! upper edge. non-periodic
            if (verbose>2) print *,'rank=n-1',rank,mpi%dn(i),mpi%up(i)
            npau1 = 0                                                       ! no upward transfer
            npau2 = 0
            call MPI_ISEND(npal1,1,MPI_INTEGER,mpi%dn(i),rank           ,comm,request1,mpi_err) ! send lower
            call MPI_IRECV(npal2,1,MPI_INTEGER,mpi%dn(i),mpi%dn(i)+nodes,comm,request3,mpi_err) ! recieve lower
            !call MPI_WAIT(request1,status,mpi_err)
            call MPI_WAIT(request3,status,mpi_err)
          endif
        endif 
        if (verbose>2) print *,'done',rank
        !if (mpi%me(i)<=1) print '(a,i2,a,i5,a,3i4,a,2i9,a,2i9)', &
        !  'I=',i,' R=',rank,' ME=',mpi%me,' SEND D,U:',npal1,npau1,' RECV D,U:',npal2,npau2
        npal = max(npal1,npal2)                                             ! parts to send dnward
        npau = max(npau1,npau2)                                             ! parts to send upward
        !-------------------------------------------------------------------
        ! Check if we have enough space to receive the particles. This could be
        ! refined, in fact only condition nr. III is strictly speaking a problem.
        !-------------------------------------------------------------------
        ! I)            total from dn > dn   +w=0  +free slots
        too_many_particles =  npal .gt. npal1+npaw+sp(isp)%mp-np
        ! II)           total from up > yp   +w=0  +free slots
        too_many_particles = (npau .gt. npau1+npaw+sp(isp)%mp-np).or.too_many_particles
        ! III)              total to recv > dn   +up   +w=0  +free slots 
        too_many_particles = (npal+npau.gt.npal1+npau1+npaw+sp(isp)%mp-np) &
                                       .or.too_many_particles
        call MPI_WAIT(request1,status,mpi_err)                              ! Finish pending sends
        call MPI_WAIT(request2,status,mpi_err)
        particles_needed = max(npal - (sp(isp)%mp - (np-npal1+npaw)),0) + & ! needed from below
                           max(npau - (sp(isp)%mp - (np-npau1+npaw)),0)     ! needed from above
        call load_balance_one_species(isp,too_many_particles,particles_needed,'SendParticles-'//cdir(i)) 
        if (too_many_particles) pa => sp(isp)%particle                      ! repoint pointer, since array can be somewhere else
 
        !-------------------------------------------------------------------
        ! Fortunately there are enough free particle slots; what a luck!
        !-------------------------------------------------------------------
        if (npau.gt.0) then
          allocate(pau1(npau),pau2(npau))                                   ! Upper scratch arrs
          call MPI_IRECV(pau2,npau,pa_t,mpi%up(i),mpi%up(i),comm,request8,mpi_err) ! recv upper parts
        endif
        !-------------------------------------------------------------------
        ! Copy particle data to the scratch arrays for sending the lower parts
        !-------------------------------------------------------------------
        if (npal.gt.0) then                                                 ! Move parts downwards
          allocate(pal1(npal),pal2(npal))                                   ! Lower scratch arrs
          call MPI_IRECV(pal2,npal,pa_t,mpi%dn(i),mpi%dn(i)+nodes,comm,request7,mpi_err) ! recv lower parts
          call CopyToScratch(npal,pal1,isp,cl,cw)
          call MPI_ISEND(pal1,npal,pa_t,mpi%dn(i),rank      ,comm,request5,mpi_err) ! send lower parts
        endif
        !-------------------------------------------------------------------
        ! Copy particle data to the scratch arrays for sending the upper parts
        !-------------------------------------------------------------------
        if (npau.gt.0) then                                                 ! Move parts upwards
          call CopyToScratch(npau,pau1,isp,cu,cw)
          call MPI_ISEND(pau1,npau,pa_t,mpi%up(i),rank+nodes,comm,request6,mpi_err) ! send lower parts
        endif
        !-------------------------------------------------------------------
        ! Wait for the receive requests to finish. When they finish copy particle data back
        !-------------------------------------------------------------------
        req_done = 0
        if (npal > 0 .and. npau > 0) then
          reqall = (/ request7, request8 /)
          call MPI_WAITANY(2, reqall, req_done, status, mpi_err)            ! start copying whatever returns first
          if (req_done == 1) then                                           ! lower done first
            call CopyFromScratch(npal,pal2,isp,cl,cw,-1)
            call MPI_WAIT(request8,status,mpi_err)                          ! wait for upper
            call CopyFromScratch(npau,pau2,isp,cu,cw,+1)
          else                                                              ! upper done first
            call CopyFromScratch(npau,pau2,isp,cu,cw,+1)
            call MPI_WAIT(request7,status,mpi_err)                          ! wait for lower
            call CopyFromScratch(npal,pal2,isp,cl,cw,-1)
          endif
        else
          if (npal.gt.0) then                                               ! do we have parts coming from lower ?
            call MPI_WAIT(request7,status,mpi_err)                          ! Wait for lower to finish
            call CopyFromScratch(npal,pal2,isp,cl,cw,-1)
          endif
          if (npau.gt.0) then                                               ! do we have parts coming from upper ?
            call MPI_WAIT(request8,status,mpi_err)                          ! Wait for upper to finish
            call CopyFromScratch(npau,pau2,isp,cu,cw,-1)
          endif
        endif
        reqall = (/request1,request2/) 
        call MPI_WAITALL(2,reqall,statusall,mpi_err)
        if (npal.gt.0) then
          call MPI_WAIT(request5,status,mpi_err)
          deallocate(pal1,pal2)
        endif
        if (npau.gt.0) then 
          call MPI_WAIT(request6,status,mpi_err)
          deallocate(pau1,pau2)
        endif
 
        if (ptrace(1).eq.isp) then
          do ip=1,sp(isp)%mp
            if (pa(ip)%i .eq. ptrace(2)) then
              ww   = pa(ip)%w.ne.0.                                         ! Particle weightless?
              palb = (pa(ip)%q(i) < ilb).and.ww                             ! crossed lower bndry?
              paub = (pa(ip)%q(i) > iub).and.ww                             ! crossed upper bndry?
              print '(a,i7.7,a,3l2,a,i1,a,i2,a,3i6,a,i8,3i6,4f6.3,i18)', &
                "Ptrace2: R=",rank," LB,UB,ERR=",palb,paub,ip>sp(isp)%np .and. ww," DIM=",i, &
                " SP=",isp, " lb,q,ub=",ilb,pa(ip)%q(i),iub," ip,q,r,w,i=",ip,pa(ip)%q,pa(ip)%r,pa(ip)%w,pa(ip)%i
            endif
          enddo
        endif
 
        call deallocate_list(cl)
        call deallocate_list(cu)
        call deallocate_list(cw)
      enddo                                                             ! while loop for resend
    enddo                                                               ! loop over species
  enddo                                                                 ! loop over direction

  call MPI_ALLREDUCE(nmax,gmax,1,MPI_INTEGER,MPI_MAX,comm,mpi_err)      ! find global max

  call trace_exit('Send Particles lowmen')
CONTAINS
!-----------------------------------------------------------------------
SUBROUTINE deallocate_list(cc)
  implicit none
  type(list), pointer :: cc
  call rewind_list(cc)
  do while (associated(cc%next)); cc => cc%next; deallocate(cc%prev); enddo
  cc%ip=-1
END SUBROUTINE deallocate_list
!-----------------------------------------------------------------------
SUBROUTINE rewind_list(cc)
  implicit none
  type(list), pointer :: cc
  do while (associated(cc%prev)); cc%ip=-1; cc => cc%prev; enddo
  cc%ip =  -1
END SUBROUTINE rewind_list
!-----------------------------------------------------------------------
SUBROUTINE CopyToScratch(npa,pascr,isp,cc,cw)
  USE params,    only : rank
  USE species,   only : sp, particle
  implicit none
  integer,                    intent(in) :: isp, npa
  type(particle),  dimension(npa)        :: pascr
  type(particle),  dimension(:), pointer :: pa
  type(list),                    pointer :: cc, cw
  integer                                :: ip, np
  pa => sp(isp)%particle
  ip = 0
  np = sp(isp)%np
  do while (associated(cc%prev))                                        ! traverse particle list backwards,
    cc => cc%prev                                                       ! while copying particles
    ip = ip + 1
    pascr(ip) = pa(cc%ip)
    if (pascr(ip)%i .eq. ptrace(2)) then
      ww   = pascr(ip)%w.ne.0.                                         ! Particle weightless?
      palb = (pascr(ip)%q(i) < ilb).and.ww                             ! crossed lower bndry?
      paub = (pascr(ip)%q(i) > iub).and.ww                             ! crossed upper bndry?
      print '(a,i7.7,a,2l2,a,i1,a,i2,a,3i6,a,i8,3i6,4f6.3,i18)', &
                  "PtraceT: R=",rank," LB,UB    =",palb,paub,"   DIM=",i," SP=",isp, &
                  " lb,q,ub=",ilb,pascr(ip)%q(i),iub, &
                  " ip,q,r,w,i=",ip,pascr(ip)%q,pascr(ip)%r,pascr(ip)%w,pascr(ip)%i
    endif
  enddo
  do while (ip < npa)                                                   ! traverse weightless list backwards,
    if (.not. associated(cw%prev)) then                                 ! add new weightless particles, if needed
      allocate(cw%prev)
      cw%prev%next => cw
      cw%prev%ip = np + 1
      np = np + 1
    endif
    cw => cw%prev                                                       ! copy particles
    ip = ip + 1
    pascr(ip) = pa(cw%ip)
    if (pascr(ip)%i .eq. ptrace(2)) then
      ww   = pascr(ip)%w.ne.0.                                         ! Particle weightless?
      palb = (pascr(ip)%q(i) < ilb).and.ww                             ! crossed lower bndry?
      paub = (pascr(ip)%q(i) > iub).and.ww                             ! crossed upper bndry?
      print '(a,i7.7,a,2l2,a,i1,a,i2,a,3i6,a,i8,3i6,4f6.3,i18)', &
                  "PtracTW: R=",rank," LB,UB    =",palb,paub,"   DIM=",i," SP=",isp, &
                  " lb,q,ub=",ilb,pascr(ip)%q(i),iub, &
                  " ip,q,r,w,i=",ip,pascr(ip)%q,pascr(ip)%r,pascr(ip)%w,pascr(ip)%i
    endif
  enddo
  sp(isp)%np = np                                                       ! update new limit
END SUBROUTINE CopyToScratch
!-----------------------------------------------------------------------
SUBROUTINE CopyFromScratch(npa,pascr,isp,cc,cw,dir)
  USE species,   only : particle
  implicit none
  integer,                    intent(in) :: isp, npa,dir
  type(particle),  dimension(npa)        :: pascr
  type(particle),  dimension(:), pointer :: pa
  type(list),                    pointer :: cc, cw
  integer                                :: ip,sr
  pa => sp(isp)%particle
  ip = 0
  if (dir==-1) then; SR=mpi%dn(i); else; SR=mpi%up(i); endif
  do while (cc%ip .ne. -1)                                              ! traverse particle list forwards,
    ip = ip + 1                                                         ! while copying particles
    pa(cc%ip) = pascr(ip)
    cc => cc%next
    if (pascr(ip)%i .eq. ptrace(2)) then
      ww   = pascr(ip)%w.ne.0.                                           ! Particle weightless?
      palb = (pascr(ip)%q(i) < ilb).and.ww                               ! crossed lower bndry?
      paub = (pascr(ip)%q(i) > iub).and.ww                               ! crossed upper bndry?
      print '(a,i7.7,a,2l2,i2,a,i1,a,i2,a,i5.5,a,3i6,a,i8,3i6,4f6.3,i18)', &
                  "PtraceF: R=",rank," LB,UB,DIR=",palb,paub,dir," DIM=",i," SP=",isp, &
                  " SR=",sr," lb,q,ub=",ilb,pascr(ip)%q(i),iub, &
                  " ip,q,r,w,i=",ip,pascr(ip)%q,pascr(ip)%r,pascr(ip)%w,pascr(ip)%i
    endif
  enddo
  do while (ip < npa)                                                   ! traverse weightless list forwards,
    ip = ip + 1                                                         ! while copying particles
    if (cw%ip .eq. -1) call error('CopyFromScratch','Arrived at end of linked list -- should never happen')
    pa(cw%ip) = pascr(ip)
    cw => cw%next
  enddo
END SUBROUTINE CopyFromScratch
END SUBROUTINE SendParticles_lowmem
!-----------------------------------------------------------------------
SUBROUTINE Send_Particles
  USE PhotonPlasmaMPI
  USE params ! only : nspecies,comm,mpi_err,nodes,rank,mpi,&
             !        interior,master,stdout,stdall,buffer_change
  USE species, only : sp,particle
  USE grid_m,    only : g
  USE debug,   only : ptrace, void=>verbose, debugit, dbg_mpi
  implicit none
  type(particle),    dimension(:), pointer     :: pa
  type(particle)                               :: pas
  type(particle),    dimension(:), allocatable :: pal1,pal2,pau1,pau2
  type list
    type(list), pointer                        :: next=>null(), prev=>null()
    integer                                    :: ip=-1
  end type
  type(list), pointer, save                    :: cl=>null(), cu=>null(), cw=>null()
  integer                                      :: ip,ip0,ip1,ip2,np,isp,i,ilb,iub,npal,npal1,npal2,npau,&
                                                  npau1,npau2,npaw
  integer,           dimension(2)              :: reqall
  integer,           dimension(MPI_STATUS_SIZE,2) :: statusall          ! Communicator id
  logical                                      :: ww,palb,paub,too_many_particles
  integer                                      :: particles_needed, req_done
  integer(kind=2)                              :: glb,gub,gn,q
  integer(kind=8),   external                  :: mask_i_flags
  character(len=1), dimension(3)               :: cdir

  if (nodes.eq.1) return                                                  ! If uni-cpu no particle sends
  call trace_enter('Send Particles')

  cdir = (/ 'x', 'y', 'z' /)

  if (.not. associated(cl)) allocate(cl,cu,cw)

  do i=1,mdim

    if (mpi%n(i) == 1) cycle                                              ! nothing to send if only 1 thread in this dir
    gn = g%gn(i)                                                          ! global size
    ilb= g%lb(i) + mpi%offset(i)
    iub= g%ub(i) + mpi%offset(i)
    glb= g%ghlb(i)
    gub= g%gn(i) - g%ghub(i)
    if (mpi%lb(i)) ilb = ilb-10                                           ! don't ship
    if (mpi%ub(i)) iub = iub+10                                           ! don't ship
    do isp=1,nspecies
      call ZeroRequests                                                   ! Zero request vars
      pa  => sp(isp)%particle
      np  =  sp(isp)%np
      ip0 = 0                                                             ! Counter for lower bndry
      ip1 = 0                                                             ! Counter for upper bndry
      ip2 = 0                                                             ! Cntr for weightless parts
      !-------------------------------------------------------------------
      ! Make an array with the index of the particles we will transfer
      !-------------------------------------------------------------------
      if (periodic(i)) then
       if (mpi%me(i)==0) then                                             ! first node in direction i ?
        do ip=1,np
         ww = pa(ip)%w.ne.0.
         q = mod(pa(ip)%q(i),gn); q = merge(q,q+gn,q>=0)                  ! wrap integer position
         paub = q >= iub .and. ww                                         ! crossed upper bndry?
         palb = q >= gub .and. ww                                         ! crossed lower bndry?  CHECK!!
         paub = merge(.false.,paub,palb)                                  ! cannot cross both simulataneously
         if (palb) then
           ip0 = ip0 + 1                                                  ! add to nr of lb particles
           cl%ip = ip                                                     ! store ip
           if (.not. associated(cl%next)) allocate(cl%next)               ! check if we need more
           cl%next%prev => cl                                             ! advance in list
           cl => cl%next
         endif
         if (paub) then
           ip1 = ip1 + 1
           cu%ip = ip
           if (.not. associated(cu%next)) allocate(cu%next)
           cu%next%prev => cu
           cu => cu%next
         endif
         if (.not. ww) then
           ip2 = ip2 + 1
           cw%ip = ip
           if (.not. associated(cw%next)) allocate(cw%next)
           cw%next%prev => cw
           cw => cw%next
         endif
        enddo
       else if (mpi%me(i)==mpi%n(i)-1) then                               ! last node in direction i ?
        do ip=1,np
         ww = pa(ip)%w.ne.0.
         q = mod(pa(ip)%q(i),gn); q = merge(q,q+gn,q>=0)                  ! wrap integer position
         palb = q <  ilb .and. ww                                         ! crossed lower bndry?
         paub = q <= glb .and. ww                                         ! crossed upper bndry?  CHECK!!
         palb = merge(.false.,palb,paub)                                  ! cannot cross both simulataneously
         if (palb) then
           ip0 = ip0 + 1
           cl%ip = ip
           if (.not. associated(cl%next)) allocate(cl%next)
           cl%next%prev => cl
           cl => cl%next
         endif
         if (paub) then
           ip1 = ip1 + 1
           cu%ip = ip
           if (.not. associated(cu%next)) allocate(cu%next)
           cu%next%prev => cu
           cu => cu%next
         endif
         if (.not. ww) then
           ip2 = ip2 + 1
           cw%ip = ip
           if (.not. associated(cw%next)) allocate(cw%next)
           cw%next%prev => cw
           cw => cw%next
         endif
        enddo
       else                                                               ! interior node in direction i
        do ip=1,np
         ww = pa(ip)%w.ne.0.
         q = mod(pa(ip)%q(i),gn); q = merge(q,q+gn,q>=0)                  ! wrap integer position
         paub = q >= iub .and. ww
         palb = q <  ilb .and. ww
         if (palb) then
           ip0 = ip0 + 1
           cl%ip = ip
           if (.not. associated(cl%next)) allocate(cl%next)
           cl%next%prev => cl
           cl => cl%next
         endif
         if (paub) then
           ip1 = ip1 + 1
           cu%ip = ip
           if (.not. associated(cu%next)) allocate(cu%next)
           cu%next%prev => cu
           cu => cu%next
         endif
         if (.not. ww) then
           ip2 = ip2 + 1
           cw%ip = ip
           if (.not. associated(cw%next)) allocate(cw%next)
           cw%next%prev => cw
           cw => cw%next
         endif
        enddo
       endif
      else
       do ip=1,np
        ww   = pa(ip)%w.ne.0.                                             ! Particle weightless?
        palb = pa(ip)%q(i) <  ilb .and. ww                                ! crossed lower bndry?
        paub = pa(ip)%q(i) >= iub .and. ww                                ! crossed upper bndry?
        if (palb) then
          ip0 = ip0 + 1
          cl%ip = ip
          if (.not. associated(cl%next)) allocate(cl%next)
          cl%next%prev => cl
          cl => cl%next
        endif
        if (paub) then
          ip1 = ip1 + 1
          cu%ip = ip
          if (.not. associated(cu%next)) allocate(cu%next)
          cu%next%prev => cu
          cu => cu%next
        endif
        if (.not. ww) then
          ip2 = ip2 + 1
          cw%ip = ip
          if (.not. associated(cw%next)) allocate(cw%next)
          cw%next%prev => cw
          cw => cw%next
        endif
       enddo
       if (ip0 > 0 .and. mpi%lb(i)) then
         call warning_all_nodes('SendParticles-'//cdir(i), &
           'Trying to send particles downwards, even though we are at a physical lower bndry')
         ip0=0
         call rewind_list(cl)
       endif
       if (ip1 > 0 .and. mpi%ub(i)) then
         call warning_all_nodes('SendParticles-'//cdir(i), &
           'Trying to send particles upwards, even though we are at a physical upper bndry')
         ip1=0
         call rewind_list(cu)
       endif
      end if
         
      ! Debug: trace if a single particle is shipped:
      !-------------------------------------------------------------------
      if (ptrace(1).eq.isp) then
        do ip=1,sp(isp)%mp
          if (pa(ip)%i .eq. ptrace(2)) then
            ww   = pa(ip)%w.ne.0.                                         ! Particle weightless?
            palb = (pa(ip)%q(i) < ilb).and.ww                             ! crossed lower bndry?
            paub = (pa(ip)%q(i) > iub).and.ww                             ! crossed upper bndry?
            print '(a,i7.7,a,3l2,a,i1,a,i2,a,3i6,a,i8,3i6,4f6.3,i18)', &
              "Ptrace1: R=",rank," LB,UB,ERR=",palb,paub,ip>sp(isp)%np .and. ww," DIM=",i, &
              " SP=",isp, " lb,q,ub=",ilb,pa(ip)%q(i),iub," ip,q,r,w,i=",ip,pa(ip)%q,pa(ip)%r,pa(ip)%w,pa(ip)%i
          endif
        enddo
      endif
      npal1 = ip0                                                         ! nr of parts < lower bndry
      npau1 = ip1                                                         ! nr of parts > upper bndry
      npaw  = ip2                                                         ! nr of weightless parts

     !print'(a,3i4,2i3,3l3,3f9.3,2i5)','me,i,isp,,lb,ub,per',mpi%me,i,isp,mpi%lb(i),mpi%ub(i),periodic(i),lb,ub,g%grlb(i),npal1,npau1
      if (debugit(dbg_mpi,0) .or. verbose>0) &
       write(stdall,'(1x,a,2i3,i7,3i4,3i6,2i4,l3)') &
          'SendParticles: i,isp,rank,me,ndn,nup,nw0,dn,up,int =', &
          i,isp,rank,mpi%me,npal1,npau1,npaw,mpi%dn(i),mpi%up(i),mpi%interior(i)
      
      if (mpi%interior(i)) then                                           ! interior node?
        if (verbose>2) print *,'interior',rank
        call MPI_IRECV(npal2,1,MPI_INTEGER,mpi%dn(i),mpi%dn(i)+nodes,comm,request3,mpi_err)   ! recieve lower
        call MPI_IRECV(npau2,1,MPI_INTEGER,mpi%up(i),mpi%up(i)      ,comm,request4,mpi_err)   ! recieve upper
        call MPI_ISEND(npal1,1,MPI_INTEGER,mpi%dn(i),rank           ,comm,request1,mpi_err)   ! send lower
        call MPI_ISEND(npau1,1,MPI_INTEGER,mpi%up(i),rank+nodes     ,comm,request2,mpi_err)   ! send upper
        !call MPI_WAIT(request1,status,mpi_err)
        !call MPI_WAIT(request2,status,mpi_err)
        call MPI_WAIT(request3,status,mpi_err)
        call MPI_WAIT(request4,status,mpi_err)
      else
        if (mpi%me(i) == 0) then                                          ! lower edge. non-periodic
          if (verbose>2) print *,'rank=0',rank,mpi%dn(i),mpi%up(i)
          npal1 = 0                                                       ! no dnward transfer
          npal2 = 0
          call MPI_IRECV(npau2,1,MPI_INTEGER,mpi%up(i),mpi%up(i) ,comm,request4,mpi_err) ! recieve upper
          call MPI_ISEND(npau1,1,MPI_INTEGER,mpi%up(i),rank+nodes,comm,request2,mpi_err) ! send upper
          !call MPI_WAIT(request2,status,mpi_err)
          call MPI_WAIT(request4,status,mpi_err)
        else                                                              ! upper edge. non-periodic
          if (verbose>2) print *,'rank=n-1',rank,mpi%dn(i),mpi%up(i)
          npau1 = 0                                                       ! no upward transfer
          npau2 = 0
          call MPI_IRECV(npal2,1,MPI_INTEGER,mpi%dn(i),mpi%dn(i)+nodes,comm,request3,mpi_err) ! recieve lower
          call MPI_ISEND(npal1,1,MPI_INTEGER,mpi%dn(i),rank           ,comm,request1,mpi_err) ! send lower
          !call MPI_WAIT(request1,status,mpi_err)
          call MPI_WAIT(request3,status,mpi_err)
        endif
      endif 
      if (verbose>2) print *,'done',rank
      !if (mpi%me(i)<=1) print '(a,i2,a,i5,a,3i4,a,2i9,a,2i9)', &
      !  'I=',i,' R=',rank,' ME=',mpi%me,' SEND D,U:',npal1,npau1,' RECV D,U:',npal2,npau2
      npal = max(npal1,npal2)                                             ! parts to send dnward
      npau = max(npau1,npau2)                                             ! parts to send upward
      !-------------------------------------------------------------------
      ! Check if we have enough space to receive the particles. This could be
      ! refined, in fact only condition nr. III is strictly speaking a problem.
      !-------------------------------------------------------------------
      ! I)            total from dn > dn   +w=0  +free slots
      too_many_particles =  npal .gt. npal1+npaw+sp(isp)%mp-np
      ! II)           total from up > yp   +w=0  +free slots
      too_many_particles = (npau .gt. npau1+npaw+sp(isp)%mp-np).or.too_many_particles
      ! III)              total to recv > dn   +up   +w=0  +free slots 
      too_many_particles = (npal+npau.gt.npal1+npau1+npaw+sp(isp)%mp-np) &
                                     .or.too_many_particles
      call MPI_WAIT(request1,status,mpi_err)                              ! Finish pending sends
      call MPI_WAIT(request2,status,mpi_err)
      particles_needed = max(npal - (sp(isp)%mp - (np-npal1+npaw)),0) + & ! needed from below
                         max(npau - (sp(isp)%mp - (np-npau1+npaw)),0)     ! needed from above
      call load_balance_one_species(isp,too_many_particles,particles_needed,'SendParticles-'//cdir(i)) 
      if (too_many_particles) pa => sp(isp)%particle                      ! repoint pointer, since array can be somewhere else

      !-------------------------------------------------------------------
      ! Fortunately there are enough free particle slots; what a luck!
      !-------------------------------------------------------------------
      if (npau.gt.0) then
        allocate(pau1(npau),pau2(npau))                                   ! Upper scratch arrs
        call MPI_IRECV(pau2,npau,pa_t,mpi%up(i),mpi%up(i),comm,request8,mpi_err) ! recv upper parts
      endif
      !-------------------------------------------------------------------
      ! Copy particle data to the scratch arrays for sending the lower parts
      !-------------------------------------------------------------------
      if (npal.gt.0) then                                                 ! Move parts downwards
        allocate(pal1(npal),pal2(npal))                                   ! Lower scratch arrs
        call MPI_IRECV(pal2,npal,pa_t,mpi%dn(i),mpi%dn(i)+nodes,comm,request7,mpi_err) ! recv lower parts
        call CopyToScratch(npal,pal1,isp,cl,cw)
        call MPI_ISEND(pal1,npal,pa_t,mpi%dn(i),rank,comm,request5,mpi_err) ! send lower parts
      endif
      !-------------------------------------------------------------------
      ! Copy particle data to the scratch arrays for sending the upper parts
      !-------------------------------------------------------------------
      if (npau.gt.0) then                                                 ! Move parts upwards
        call CopyToScratch(npau,pau1,isp,cu,cw)
        call MPI_ISEND(pau1,npau,pa_t,mpi%up(i),rank+nodes,comm,request6,mpi_err) ! send lower parts
      endif
      !-------------------------------------------------------------------
      ! Wait for the receive requests to finish. When they finish copy particle data back
      !-------------------------------------------------------------------
      req_done = 0
      if (npal > 0 .and. npau > 0) then
        reqall = (/ request7, request8 /)
        call MPI_WAITANY(2, reqall, req_done, status, mpi_err)            ! start copying whatever returns first
        if (req_done == 1) then                                           ! lower done first
          call CopyFromScratch(npal,pal2,isp,cl,cw,-1)
          call MPI_WAIT(request8,status,mpi_err)                          ! wait for upper
          call CopyFromScratch(npau,pau2,isp,cu,cw,+1)
        else                                                              ! upper done first
          call CopyFromScratch(npau,pau2,isp,cu,cw,+1)
          call MPI_WAIT(request7,status,mpi_err)                          ! wait for lower
          call CopyFromScratch(npal,pal2,isp,cl,cw,-1)
        endif
      else
        if (npal.gt.0) then                                               ! do we have parts coming from lower ?
          call MPI_WAIT(request7,status,mpi_err)                          ! Wait for lower to finish
          call CopyFromScratch(npal,pal2,isp,cl,cw,-1)
        endif
        if (npau.gt.0) then                                               ! do we have parts coming from upper ?
          call MPI_WAIT(request8,status,mpi_err)                          ! Wait for upper to finish
          call CopyFromScratch(npau,pau2,isp,cu,cw,-1)
        endif
      endif
      reqall = (/request1,request2/) 
      !call MPI_WAITALL(2,reqall,statusall,mpi_err)
      call MPI_WAIT(request1,status,mpi_err)
      call MPI_WAIT(request2,status,mpi_err)
      if (npal.gt.0) then
        call MPI_WAIT(request5,status,mpi_err)
        deallocate(pal1,pal2)
      endif
      if (npau.gt.0) then 
        call MPI_WAIT(request6,status,mpi_err)
        deallocate(pau1,pau2)
      endif

      if (ptrace(1).eq.isp) then
        do ip=1,sp(isp)%mp
          if (pa(ip)%i .eq. ptrace(2)) then
            ww   = pa(ip)%w.ne.0.                                         ! Particle weightless?
            palb = (pa(ip)%q(i) < ilb).and.ww                             ! crossed lower bndry?
            paub = (pa(ip)%q(i) > iub).and.ww                             ! crossed upper bndry?
            print '(a,i7.7,a,3l2,a,i1,a,i2,a,3i6,a,i8,3i6,4f6.3,i18)', &
              "Ptrace2: R=",rank," LB,UB,ERR=",palb,paub,ip>sp(isp)%np .and. ww," DIM=",i, &
              " SP=",isp, " lb,q,ub=",ilb,pa(ip)%q(i),iub," ip,q,r,w,i=",ip,pa(ip)%q,pa(ip)%r,pa(ip)%w,pa(ip)%i
          endif
        enddo
      endif

      call rewind_list(cl)
      call rewind_list(cu)
      call rewind_list(cw)
    enddo
  enddo

  call trace_exit('Send Particles')
CONTAINS
!-----------------------------------------------------------------------
SUBROUTINE deallocate_list(cc)
  implicit none
  type(list), pointer :: cc
  call rewind_list(cc)
  do while (associated(cc%next)); cc => cc%next; deallocate(cc%prev); enddo
END SUBROUTINE deallocate_list
!-----------------------------------------------------------------------
SUBROUTINE rewind_list(cc)
  implicit none
  type(list), pointer :: cc
  do while (associated(cc%prev)); cc%ip=-1; cc => cc%prev; enddo
  cc%ip =  -1
END SUBROUTINE rewind_list
!-----------------------------------------------------------------------
SUBROUTINE CopyToScratch(npa,pascr,isp,cc,cw)
  USE params,    only : rank
  USE species,   only : sp, particle
  implicit none
  integer,                    intent(in) :: isp, npa
  type(particle),  dimension(npa)        :: pascr
  type(particle),  dimension(:), pointer :: pa
  type(list),                    pointer :: cc, cw
  integer                                :: ip, np
  pa => sp(isp)%particle
  ip = 0
  np = sp(isp)%np
  do while (associated(cc%prev))                                        ! traverse particle list backwards,
    cc => cc%prev                                                       ! while copying particles
    ip = ip + 1
    pascr(ip) = pa(cc%ip)
    if (pascr(ip)%i .eq. ptrace(2)) then
      ww   = pascr(ip)%w.ne.0.                                         ! Particle weightless?
      palb = (pascr(ip)%q(i) < ilb).and.ww                             ! crossed lower bndry?
      paub = (pascr(ip)%q(i) > iub).and.ww                             ! crossed upper bndry?
      print '(a,i7.7,a,2l2,a,i1,a,i2,a,3i6,a,i8,3i6,4f6.3,i18)', &
                  "PtraceT: R=",rank," LB,UB    =",palb,paub,"   DIM=",i," SP=",isp, &
                  " lb,q,ub=",ilb,pascr(ip)%q(i),iub, &
                  " ip,q,r,w,i=",ip,pascr(ip)%q,pascr(ip)%r,pascr(ip)%w,pascr(ip)%i
    endif
  enddo
  do while (ip < npa)                                                   ! traverse weightless list backwards,
    if (.not. associated(cw%prev)) then                                 ! add new weightless particles, if needed
      allocate(cw%prev)
      cw%prev%next => cw
      cw%prev%ip = np + 1
      np = np + 1
    endif
    cw => cw%prev                                                       ! copy particles
    ip = ip + 1
    pascr(ip) = pa(cw%ip)
    if (pascr(ip)%i .eq. ptrace(2)) then
      ww   = pascr(ip)%w.ne.0.                                         ! Particle weightless?
      palb = (pascr(ip)%q(i) < ilb).and.ww                             ! crossed lower bndry?
      paub = (pascr(ip)%q(i) > iub).and.ww                             ! crossed upper bndry?
      print '(a,i7.7,a,2l2,a,i1,a,i2,a,3i6,a,i8,3i6,4f6.3,i18)', &
                  "PtracTW: R=",rank," LB,UB    =",palb,paub,"   DIM=",i," SP=",isp, &
                  " lb,q,ub=",ilb,pascr(ip)%q(i),iub, &
                  " ip,q,r,w,i=",ip,pascr(ip)%q,pascr(ip)%r,pascr(ip)%w,pascr(ip)%i
    endif
  enddo
  sp(isp)%np = np                                                       ! update new limit
END SUBROUTINE CopyToScratch
!-----------------------------------------------------------------------
SUBROUTINE CopyFromScratch(npa,pascr,isp,cc,cw,dir)
  USE species,   only : particle
  implicit none
  integer,                    intent(in) :: isp, npa,dir
  type(particle),  dimension(npa)        :: pascr
  type(particle),  dimension(:), pointer :: pa
  type(list),                    pointer :: cc, cw
  integer                                :: ip,sr
  pa => sp(isp)%particle
  ip = 0
  if (dir==-1) then; SR=mpi%dn(i); else; SR=mpi%up(i); endif
  do while (cc%ip .ne. -1)                                              ! traverse particle list forwards,
    ip = ip + 1                                                         ! while copying particles
    pa(cc%ip) = pascr(ip)
    cc => cc%next
    if (pascr(ip)%i .eq. ptrace(2)) then
      ww   = pascr(ip)%w.ne.0.                                           ! Particle weightless?
      palb = (pascr(ip)%q(i) < ilb).and.ww                               ! crossed lower bndry?
      paub = (pascr(ip)%q(i) > iub).and.ww                               ! crossed upper bndry?
      print '(a,i7.7,a,2l2,i2,a,i1,a,i2,a,i5.5,a,3i6,a,i8,3i6,4f6.3,i18)', &
                  "PtraceF: R=",rank," LB,UB,DIR=",palb,paub,dir," DIM=",i," SP=",isp, &
                  " SR=",sr," lb,q,ub=",ilb,pascr(ip)%q(i),iub, &
                  " ip,q,r,w,i=",ip,pascr(ip)%q,pascr(ip)%r,pascr(ip)%w,pascr(ip)%i
    endif
  enddo
  do while (ip < npa)                                                   ! traverse weightless list forwards,
    ip = ip + 1                                                         ! while copying particles
    if (cw%ip .eq. -1) call error('CopyFromScratch','Arrived at end of linked list -- should never happen')
    pa(cw%ip) = pascr(ip)
    cw => cw%next
  enddo
END SUBROUTINE CopyFromScratch
END SUBROUTINE Send_Particles
!-----------------------------------------------------------------------
SUBROUTINE stats(f,mn,av,mx,s)
  USE params,  only: nodes,comm,mpi_err
  USE PhotonPlasmaMPI
  real                :: mn,av,mx
  integer,           intent(in)  :: s
  real,dimension(s), intent(in)  :: f
  integer                        :: sg,ip,ipp,sqnp,lb,ub
  real                           :: a,mnl,avl,mxl
  if (nodes.gt.1) then
    call MPI_ALLREDUCE(s,sg,1,MPI_INTEGER,MPI_SUM,comm,mpi_err)
  else
    sg = s
  endif
  if (s.gt.0) then
    mnl= f(1) ; mxl= f(1)                                               ! Resetting
  else
    mnl = huge(a)
    mxl = -huge(a)
  endif
  sqnp = floor(sqrt(real(s)))                                           ! We do everything in
  avl= 0.
  do ipp=0,sqnp-1                                                       ! planes to guard roundoff
    lb = ipp*sqnp+1
    ub = (ipp+1)*sqnp
    if (ipp .eq. sqnp-1) ub = s
    a   = 0.
    do ip =lb,ub
      a = a + f(ip)
      mnl = min(mnl,f(ip))
      mxl = max(mxl,f(ip))
    enddo
    avl = avl + a
  enddo
  if (nodes.gt.1) then
    call MPI_ALLREDUCE(mnl,mn,1,CMPI_REAL,MPI_MIN,comm,mpi_err)
    call MPI_ALLREDUCE(avl,av,1,CMPI_REAL,MPI_SUM,comm,mpi_err)
    call MPI_ALLREDUCE(mxl,mx,1,CMPI_REAL,MPI_MAX,comm,mpi_err)
  else
    mn = mnl
    av = avl
    mx = mxl
  endif
  av = av/sg
END SUBROUTINE stats
!-----------------------------------------------------------------------
FUNCTION aver(f,s)
  USE params,  only : nodes,comm,mpi_err
  USE PhotonPlasmaMPI
  real                           :: aver
  integer,           intent(in)  :: s
  real,dimension(max(1,s)), intent(in)  :: f
  integer                        :: sg,ip,ipp,sqnp,lb,ub
  real(kind=8)                   :: a1,a2,a3
  if (nodes.gt.1) then
    call MPI_ALLREDUCE(s,sg,1,MPI_INTEGER,MPI_SUM,comm,mpi_err)
  else
    sg = s
  endif
  sqnp = floor(sqrt(real(s)))                                           ! We do everything in
  a2 = 0.
  do ipp=0,sqnp-1                                                       ! planes to guard roundoff
    lb = ipp*sqnp+1
    ub = (ipp+1)*sqnp
    if (ipp .eq. sqnp-1) ub = s
    a1 = 0.
    do ip =lb,ub
      a1 = a1 + f(ip)
    enddo
    a2 = a2 + a1
  enddo
  if (nodes.gt.1) then
    call MPI_ALLREDUCE(a2,a3,1,MPI_REAL8,MPI_SUM,comm,mpi_err)
  else
    a3 = a2
  endif
  aver = a3/sg
END FUNCTION aver
!-----------------------------------------------------------------------
FUNCTION aver_r8(f,s)
  USE params,  only : nodes,comm,mpi_err
  USE PhotonPlasmaMPI
  real(kind=8)                           :: aver_r8
  integer,                   intent(in)  :: s
  real(kind=8),dimension(s), intent(in)  :: f
  integer                                :: sg,ip,ipp,sqnp,lb,ub
  real(kind=8)                           :: a1,a2,a3
  if (nodes.gt.1) then
    call MPI_ALLREDUCE(s,sg,1,MPI_INTEGER,MPI_SUM,comm,mpi_err)
  else
    sg = s
  endif
  sqnp = floor(sqrt(real(s)))                                           ! We do everything in
  a2 = 0.
  do ipp=0,sqnp-1                                                       ! planes to guard roundoff
    lb = ipp*sqnp+1
    ub = (ipp+1)*sqnp
    if (ipp .eq. sqnp-1) ub = s
    a1 = 0.
    do ip =lb,ub
      a1 = a1 + f(ip)
    enddo
    a2 = a2 + a1
  enddo
  if (nodes.gt.1) then
    call MPI_ALLREDUCE(a2,a3,1,MPI_REAL8,MPI_SUM,comm,mpi_err)
  else
    a3 = a2
  endif
  aver_r8 = a3/sg
END FUNCTION aver_r8
!-----------------------------------------------------------------------
! Basiclly a routine to compute the total or average energy
FUNCTION aver2(isp)
  USE params,  only : nodes,comm,mpi_err
  USE species, only : sp,particle
  USE PhotonPlasmaMPI
  type(particle),dimension(:),pointer :: pa
  real(kind=8)                        :: aver2
  integer,           intent(in)  :: isp
  integer                        :: ip,ipp,sqnp,lb,ub
  real(kind=8)                   :: a1,a2,a3
  if (sp(isp)%tnp .eq. 0) then
    aver2 = 0.
    return
  endif
  if (sp(isp)%np .eq. 0) then
    a2 = 0.
  else
    pa => sp(isp)%particle
    a2 = 0.
    sqnp = floor(sqrt(real(sp(isp)%np)))                                ! We do everything in
    do ipp=0,sqnp-1                                                     ! planes to guard roundoff
      lb = ipp*sqnp+1
      ub = (ipp+1)*sqnp
      if (ipp .eq. sqnp-1) ub = sp(isp)%np
      a1 = 0.
      do ip =lb,ub
        a1 = a1 + pa(ip)%e*pa(ip)%w
      enddo
      a2 = a2 + a1
    enddo
  endif
  if (nodes.gt.1) then
    call MPI_ALLREDUCE(a2,a3,1,MPI_REAL8,MPI_SUM,comm,mpi_err)
  else
    a3 = a2
  endif
  aver2 = a3/sp(isp)%tnp
END FUNCTION aver2
!-----------------------------------------------------------------------
! Basiclly a routine to compute the total of a field quantity
! No checks done for boundaries! Neither for interior nodes nor anywhere!!!! 
FUNCTION total3(f)
  USE params,          only : mpi_err,comm
  USE grid_m,            only : g
  USE PhotonPlasmaMPI

  implicit none

  real, dimension(g%n(1),g%n(2),g%n(3)) :: f
  real(kind=8)                          :: total3
  real(kind=8)                          :: sum1,sum2,sum3
  integer                               :: i,j,k

  sum1 = 0.
  do k=g%lb(3),g%ub(3)-1
    sum2 = 0.
    do j=g%lb(2),g%ub(2)-1
      sum3 = 0.
      do i=g%lb(1),g%ub(1)-1
        sum3 = sum3 + f(i,j,k)
      enddo
      sum2 = sum2 + sum3
    enddo
    sum1 = sum1 + sum2
  enddo

  call MPI_ALLREDUCE(sum1,total3,1,MPI_REAL8,MPI_SUM,comm,mpi_err)
END FUNCTION total3
!-----------------------------------------------------------------------
! Basiclly a routine to compute the average of a field quantity
! No checks done for boundaries! Neither for interior nodes nor anywhere!!!! 
FUNCTION aver3(f)
  USE grid_m, only : g
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                  :: aver3
  real(kind=8), external                :: total3

  aver3 = total3(f) / product(int(g%gn,kind=8))
END FUNCTION aver3
!-----------------------------------------------------------------------
! Basiclly a routine to compute the maximum of a field quantity - discarding boundaries.
FUNCTION max4(f)
  USE params, only : mpi_err,rank,nodes,comm,mpi
  USE grid_m,   only : g
  USE PhotonPlasmaMPI
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                  :: max4l, max4
  integer                               :: i,j,k,lb(3),ub(3)

  lb = merge(g%lb+2, g%lb  , mpi%lb)
  ub = merge(g%ub-3, g%ub-1, mpi%ub)
  max4l = f(lb(1),lb(2),lb(3))
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    max4l = max(max4l,f(i,j,k))
  enddo
  enddo
  enddo
  if (nodes.gt.1) then
    call MPI_ALLREDUCE(max4l,max4,1,CMPI_REAL,MPI_MAX,comm,mpi_err)
  else
    max4 = max4l
  endif
END FUNCTION max4
!-----------------------------------------------------------------------
! Basiclly a routine to compute the minimum of a field quantity - discarding boundaries.
FUNCTION min4(f)
  USE params, only : mpi_err,rank,nodes,comm,mpi
  USE grid_m,   only : g
  USE PhotonPlasmaMPI
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)) :: f
  real                                  :: min4l, min4
  integer                               :: i,j,k,lb(3),ub(3)

  lb = merge(g%lb+2, g%lb  , mpi%lb)
  ub = merge(g%ub-3, g%ub-1, mpi%ub)
  min4l = f(lb(1),lb(2),lb(3))
  do k=lb(3),ub(3)
  do j=lb(2),ub(2)
  do i=lb(1),ub(1)
    min4l = min(min4l,f(i,j,k))
  enddo
  enddo
  enddo
  if (nodes.gt.1) then
    call MPI_ALLREDUCE(min4l,min4,1,CMPI_REAL,MPI_MIN,comm,mpi_err)
  else
    min4 = min4l
  endif
END FUNCTION min4
!-----------------------------------------------------------------------
REAL FUNCTION max_scalar(s)
  USE params, only : mpi_err,comm
  USE PhotonPlasmaMpi
  implicit none
  real s, sl, smax
  sl = s
  call MPI_ALLREDUCE(sl,smax,1,CMPI_REAL,MPI_MAX,comm,mpi_err)
  call assert(mpi_err.eq.0,'max_scalar','ALLREDUCE')
  max_scalar = smax
END FUNCTION
!-----------------------------------------------------------------------
REAL FUNCTION min_scalar(s)
  USE params, only : mpi_err,comm
  USE PhotonPlasmaMpi
  implicit none
  real s, sl, smin
  sl = s
  call MPI_ALLREDUCE(sl,smin,1,CMPI_REAL,MPI_MIN,comm,mpi_err)
  call assert(mpi_err.eq.0,'min_scalar','ALLREDUCE')
  min_scalar = smin
END FUNCTION
!-----------------------------------------------------------------------
REAL FUNCTION sum_scalar(s)
  USE params, only : mpi_err,comm
  USE PhotonPlasmaMpi
  implicit none
  real s, sl, smax
  sl = s
  call MPI_ALLREDUCE(sl,smax,1,CMPI_REAL,MPI_SUM,comm,mpi_err)
  call assert(mpi_err.eq.0,'sum_scalar','ALLREDUCE')
  sum_scalar = smax
END FUNCTION
!-----------------------------------------------------------------------
SUBROUTINE overlap_x(f)
  USE params, only : mpi,mpi_err,periodic
  USE grid_m,   only : g
  USE comm_arrays
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  real, allocatable, dimension(:,:,:):: fup,fdn,fups,fdns
  integer ix,iy,iz,lb,ub,ghlb,ghub,nup(3),ndn(3),req(4)
  logical, parameter:: block=.true.

  lb=g%lb(1)
  ub=g%ub(1)
  ghlb=g%ghlb(1)
  ghub=g%ghub(1)
  if (mpi%n(1) .eq. 1) then                                             ! no need to send anything
    if (.not.periodic(1)) return
    do iz=1,g%n(3)
    do iy=1,g%n(2)
    do ix=g%lb(1)-1,1,-1
      f(ix,iy,iz) = f(ix+g%gn(1),iy,iz)
    enddo
    do ix=g%ub(1),g%n(1)
      f(ix,iy,iz) = f(ix-g%gn(1),iy,iz)
    enddo
    end do
    end do
    return
  endif

  ndn=(/ghub,g%n(2),g%n(3)/)
  nup=(/ghlb,g%n(2),g%n(3)/)
  allocate (fdns(ndn(1),ndn(2),ndn(3)))
  allocate (fups(nup(1),nup(2),nup(3)))
  allocate (fdn(ndn(1),ndn(2),ndn(3)))
  allocate (fup(nup(1),nup(2),nup(3)))

  if (.not. mpi%lb(1)) fdns = f(lb  :lb+ghub-1,:,:)
  if (.not. mpi%ub(1)) fups = f(ub-ghlb:ub-1,:,:)
  call comm_array(1,ndn,nup, &
    fdns, &                                                 ! send two (lb) layers down (unless first node)
    fups, &                                                 ! send one (nz-ub) layer up (unless last node)
    fdn             , &                                                 ! receive two layers from above (unless last node)
    fup             , &                                                 ! receive one layer from below (unless first node)
    req(1),req(2),req(3),req(4))
  mpi_err = wait_requests(req,block)
  if (.not. mpi%ub(1)) f(ub     :ub+ghub-1,:,:) = fdn
  if (.not. mpi%lb(1)) f(lb-ghlb:lb-1,     :,:) = fup
  deallocate(fdn,fup,fdns,fups)

END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE overlap_y(f)
  USE params, only : mpi,mpi_err,periodic
  USE grid_m,   only : g
  USE comm_arrays
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  real, allocatable, dimension(:,:,:):: fup,fdn,fups,fdns
  integer ix,iy,iz,lb,ub,ghlb,ghub,nup(3),ndn(3),req(4)
  logical, parameter:: block=.true.

  lb=g%lb(2)
  ub=g%ub(2)
  ghlb=g%ghlb(2)
  ghub=g%ghub(2)
  if (mpi%n(2) .eq. 1) then                                             ! no need to send anything
    if (.not.periodic(2)) return
    do iz=1,g%n(3)
      do iy=g%lb(2)-1,1,-1
      do ix=1,g%n(1)
        f(ix,iy,iz) = f(ix,iy+g%gn(2),iz)
      end do
      end do
      do iy=g%ub(2),g%n(2)
      do ix=1,g%n(1)
        f(ix,iy,iz) = f(ix,iy-g%gn(2),iz)
      end do
      end do
    end do
    return
  endif

  ndn=(/g%n(1),ghub,g%n(3)/)
  nup=(/g%n(1),ghlb,g%n(3)/)
  allocate (fdn(ndn(1),ndn(2),ndn(3)))
  allocate (fup(nup(1),nup(2),nup(3)))
  allocate (fdns(ndn(1),ndn(2),ndn(3)))
  allocate (fups(nup(1),nup(2),nup(3)))

  if (.not. mpi%lb(2)) fdns = f(:,lb     :lb+ghub-1,:)
  if (.not. mpi%ub(2)) fups = f(:,ub-ghlb:ub-1,     :)
  call comm_array(2,ndn,nup, &
    fdns, &                                                             ! ship two incomplete layers down
    fups, &                                                             ! ship one incomplete layer up
    fdn, &                                                              ! receive from above
    fup, &                                                              ! receive from below
    req(1),req(2),req(3),req(4))
  mpi_err = wait_requests(req,block)
  
  ! add from below and from above, if not on physical boundary
  if (.not. mpi%ub(2)) f(:,ub:g%n(2),:) = fdn
  if (.not. mpi%lb(2)) f(:,1 :lb-1,  :) = fup
  deallocate(fdn,fup,fdns,fups)
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE overlap_z(f)
  USE params, only : mpi,periodic,mpi_err
  USE grid_m,   only : g
  USE comm_arrays
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  real, allocatable, dimension(:,:,:):: fup,fdn,fups,fdns
  integer ix,iy,iz,lb,ub,ghlb,ghub,nup(3),ndn(3),req(4)
  logical, parameter:: block=.true.

  lb=g%lb(3)
  ub=g%ub(3)
  ghlb=g%ghlb(2)
  ghub=g%ghub(2)

  if (mpi%n(3) .eq. 1) then
    if (.not.periodic(3)) return

    do iz=g%lb(3)-1,1,-1
    do iy=1,g%n(2)
    do ix=1,g%n(1)
      f(ix,iy,iz) = f(ix,iy,iz+g%gn(3))
    end do
    end do
    end do
    do iz=g%ub(3),g%n(3)
    do iy=1,g%n(2)
    do ix=1,g%n(1)
      f(ix,iy,iz) = f(ix,iy,iz-g%gn(3))
    end do
    end do
    end do
  else

    ndn=(/g%n(1),g%n(2),ghub/)
    nup=(/g%n(1),g%n(2),ghlb/)
    allocate (fdn(ndn(1),ndn(2),ndn(3)))
    allocate (fup(nup(1),nup(2),nup(3)))
    allocate (fdns(ndn(1),ndn(2),ndn(3)))
    allocate (fups(nup(1),nup(2),nup(3)))
 
    if (.not. mpi%lb(3)) fdns = f(:,:,lb     :lb+ghub-1)
    if (.not. mpi%ub(3)) fups = f(:,:,ub-ghlb:ub-1)
    call comm_array(3,ndn,nup, &
      fdns, &                                                           ! ship one incomplete layer down
      fups, &                                                           ! ship two incomplete layers up
      fdn, &                                                            ! receive from above
      fup, &                                                            ! receive from below
      req(1),req(2),req(3),req(4))
    mpi_err = wait_requests(req,block)
    if (.not. mpi%ub(3)) f(:,:,ub:g%n(3)) = fdn
    if (.not. mpi%lb(3)) f(:,:,1:lb-1) = fup
    deallocate (fdn,fup,fdns,fups)
  endif
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE overlap_add_x(f)
  USE params, only : mpi,stdout,periodic,mpi_err,periodic
  USE grid_m,   only : g
  USE comm_arrays
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  real, allocatable, dimension(:,:,:):: fup,fdn,fups,fdns
  integer ix,iy,iz,lb,ub,ghlb,ghub,nup(3),ndn(3),req(4),edge,dom
  logical, parameter:: block=.true.

  lb=g%lb(1)
  ub=g%ub(1)
  ghlb=g%ghlb(1)
  ghub=g%ghub(1)
  dom  = ub - lb
  edge = g%n(1) - (ub - lb)
  if (dom < edge  .and. .not. (mpi%n(1)==1 .and. g%gn(1)==1)) then      ! [2:bndry[ cells in x-direction
    write(stdout,*) 'Total number of cells in x-dir    :', g%n(1)
    write(stdout,*) 'Number of physical cells in x-dir :', dom
    write(stdout,*) 'Number of bndry cells in x-dir    :', edge
    call error('overlap_add_x','We have bndry > #of cells in x-dir, but not 1-D. Cannot handle this yet. Fatten your pizza boxes!')
  endif

  if (mpi%n(1) .eq. 1) then                                             ! no need to send anything
    if (.not.periodic(1)) return
    if (g%gn(1) >= edge) then                                           ! general case
      do iz=1,g%n(3)
       do iy=1,g%n(2)
        do ix=1,edge
          f(ix,iy,iz) = f(ix,iy,iz) + f(ix+g%gn(1),iy,iz)
          f(ix+g%gn(1),iy,iz) = f(ix,iy,iz)
        end do
       end do
      end do
    else  if (g%gn(1) .eq. 1) then                                      ! 1-D in x-direction
      do iz=1,g%n(3)
       do iy=1,g%n(2)
        do ix=1,g%n(1)-1
          f(g%n(1),iy,iz) = f(ix,iy,iz) + f(g%n(1),iy,iz)
        end do
        do ix=1,g%n(1)-1
          f(ix,iy,iz) = f(g%n(1),iy,iz)
        end do
       end do
      end do
    endif
    return
  endif

  ndn=(/ghlb,g%n(2),g%n(3)/)
  nup=(/ghub,g%n(2),g%n(3)/)
  allocate (fdns(ndn(1),ndn(2),ndn(3)))
  allocate (fups(nup(1),nup(2),nup(3)))
  allocate (fdn(ndn(1),ndn(2),ndn(3)))
  allocate (fup(nup(1),nup(2),nup(3)))

  if (.not. mpi%lb(1)) fdns = f(1:ghlb,:,:)
  if (.not. mpi%ub(1)) fups = f(ub:g%n(1),:,:)
  call comm_array(1,ndn,nup, &
    fdns, &                                                             ! ship two incomplete layers down
    fups, &                                                             ! ship one incomplete layer up
    fdn, &                                                              ! receive from above
    fup, &                                                              ! receive from below
    req(1),req(2),req(3),req(4))
  mpi_err = wait_requests(req,block)
  
  ! add from below and from above, if not on physical boundary
  if (.not. mpi%lb(1)) f(lb:lb+ghub-1,:,:) = f(lb:lb+ghub-1,:,:) + fup
  if (.not. mpi%ub(1)) f(ub-ghlb:ub-1,:,:) = f(ub-ghlb:ub-1,:,:) + fdn
  deallocate(fdn,fup,fdns,fups)

  call overlap_x(f)                                                     ! interior now OK, so do normal overlap
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE overlap_add_y(f)
  USE params, only : mpi,stdout,periodic,mpi_err,periodic
  USE grid_m,   only : g
  USE comm_arrays
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  real, allocatable, dimension(:,:,:):: fup,fdn,fups,fdns
  integer ix,iy,iz,lb,ub,ghlb,ghub,nup(3),ndn(3),req(4),edge,dom
  logical, parameter:: block=.true.

  lb=g%lb(2)
  ub=g%ub(2)
  ghlb=g%ghlb(2)
  ghub=g%ghub(2)
  dom = ub - lb
  edge = g%n(2) - dom

  if (dom < edge  .and. .not. (mpi%n(2)==1 .and. g%gn(2)==1)) then      ! [2:bndry[ cells in y-direction
    write(stdout,*) 'Total number of cells in y-dir    :', g%n(2)
    write(stdout,*) 'Number of physical cells in y-dir :', dom
    write(stdout,*) 'Number of bndry cells in y-dir    :', edge
    call error('overlap_add_y','We have bndry > #of cells in y-dir, but not 1-D. Cannot handle this yet. Fatten your pizza boxes!')
  endif

  if (mpi%n(2) .eq. 1) then                                             ! no need to send anything
    if (.not.periodic(2)) return
    if (g%gn(2) >= edge) then                                           ! general case
      do iz=1,g%n(3)
       do iy=1,edge
        do ix=1,g%n(1)
          f(ix,iy,iz) = f(ix,iy,iz) + f(ix,iy+g%gn(2),iz)
          f(ix,iy+g%gn(2),iz) = f(ix,iy,iz)
        end do
       end do
      end do
    else                                                                ! 1-D in y-direction
      do iz=1,g%n(3)
       do iy=1,g%n(2)-1
        do ix=1,g%n(1)
          f(ix,g%n(2),iz) = f(ix,iy,iz) + f(ix,g%n(2),iz)
        end do
       end do
       do iy=1,g%n(2)-1
        do ix=1,g%n(1)
          f(ix,iy,iz) = f(ix,g%n(2),iz)
        end do
       end do
      end do
    endif
    return
  endif

  ndn=(/g%n(1),ghlb,g%n(3)/)
  nup=(/g%n(1),ghub,g%n(3)/)

  allocate (fdns(ndn(1),ndn(2),ndn(3)))
  allocate (fups(nup(1),nup(2),nup(3)))
  allocate (fdn(ndn(1),ndn(2),ndn(3)))
  allocate (fup(nup(1),nup(2),nup(3)))

  if (.not. mpi%lb(2)) fdns = f(:,1 :ghlb,  :)
  if (.not. mpi%ub(2)) fups = f(:,ub:g%n(2),:)

  call comm_array(2,ndn,nup, &
    fdns, &                                                             ! ship two incomplete layers down
    fups, &                                                             ! ship one incomplete layer up
    fdn, &                                                              ! receive from above
    fup, &                                                              ! receive from below
    req(1),req(2),req(3),req(4))
  mpi_err = wait_requests(req,block)

  ! add from below and from above, if not on physical boundary
  if (.not. mpi%lb(2)) f(:,lb:lb+ghub-1,:) = f(:,lb:lb+ghub-1,:) + fup        ! add from below
  if (.not. mpi%ub(2)) f(:,ub-ghlb:ub-1,:) = f(:,ub-ghlb:ub-1,:) + fdn        ! add from above
  deallocate(fdn,fup,fdns,fups)

  call overlap_y(f)                                                     ! interior now OK, so do normal overlap
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE overlap_add_z(f)
  USE params, only : mpi,stdout,periodic,mpi_err
  USE grid_m,   only : g
  USE comm_arrays
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  real, allocatable, dimension(:,:,:):: fup,fdn,fups,fdns
  integer ix,iy,iz,lb,ub,ghlb,ghub,nup(3),ndn(3),req(4),edge,dom
  logical, parameter:: block=.true.

  lb=g%lb(3)
  ub=g%ub(3)
  ghlb=g%ghlb(3)
  ghub=g%ghub(3)
  dom = ub - lb
  edge = g%n(3) - dom

  if (dom < edge .and. .not. (mpi%n(3)==1 .and. g%gn(3)==1)) then       ! [1:bndry[ cells in z-direction
    write(stdout,*) 'Total number of cells in z-dir    :', g%n(3)
    write(stdout,*) 'Number of physical cells in z-dir :', dom
    write(stdout,*) 'Number of bndry cells in z-dir    :', edge
    call error('overlap_add_z','We have bndry > #of cells in z-dir. Cannot'// &
      ' handle this yet. Fatten your pizza boxes (and no 1-D in z-dir, please)!')
  endif

  if (mpi%n(3) .eq. 1) then
    if (.not.periodic(3)) return  
    if (g%gn(3) >= edge) then                                                ! general case
      do iz=1,edge
       do iy=1,g%n(2)
        do ix=1,g%n(1)
          f(ix,iy,iz) = f(ix,iy,iz) + f(ix,iy,iz+g%gn(3))                   ! send lb layers "up" and add them in
          f(ix,iy,iz+g%gn(3)) = f(ix,iy,iz)                                 ! copy lb layers "down" (in wrap sense)
        end do
       end do
      end do
    else                                                                  ! 1-D in z-direction
      do iz=1,g%n(3)-1
       do iy=1,g%n(2)
        do ix=1,g%n(1)
          f(ix,iy,g%n(3)) = f(ix,iy,iz) + f(ix,iy,g%n(3))
        end do
       end do
      end do
      do iz=1,g%n(3)-1
       do iy=1,g%n(2)
        do ix=1,g%n(1)
          f(ix,iy,iz) = f(ix,iy,g%n(3))
        end do
       end do
      end do
    endif
    return
  endif

  ndn=(/g%n(1),g%n(2),ghlb/)                                               ! send (nz-ub) layers down
  nup=(/g%n(1),g%n(2),ghub/)                                               ! send lb layers up

  allocate (fdn(ndn(1),ndn(2),ndn(3)))
  allocate (fup(nup(1),nup(2),nup(3)))
  allocate (fdns(ndn(1),ndn(2),ndn(3)))
  allocate (fups(nup(1),nup(2),nup(3)))

  if (.not. mpi%lb(3)) fdns = f(:,:,1 :ghlb  )
  if (.not. mpi%ub(3)) fups = f(:,:,ub:g%n(3))

  call comm_array(3,ndn,nup, &
    fdns, &                                                             ! ship one incomplete layer down
    fups, &                                                             ! ship two incomplete layers up
    fdn, &                                                              ! receive from above
    fup, &                                                              ! receive from below
    req(1),req(2),req(3),req(4))
  mpi_err = wait_requests(req,block)

  ! add from below and from above, if not on physical boundary
  if (.not. mpi%lb(3)) f(:,:,lb:lb+ghub-1) = f(:,:,lb:lb+ghub-1) + fup  ! add from below
  if (.not. mpi%ub(3)) f(:,:,ub-ghlb:ub-1) = f(:,:,ub-ghlb:ub-1) + fdn  ! add from above

  deallocate(fdn,fup,fdns,fups)

  call overlap_z(f)                                                     ! interior now OK, so do normal overlap
END SUBROUTINE
!-----------------------------------------------------------------------
! Wrapper for Broadcast of a scalar
SUBROUTINE mpi_broadcast(integer_number,root)
  USE params, only : nodes, rank, comm, mpi_err, stdout, dbg
  USE PhotonPlasmaMpi
  implicit none
  integer                :: integer_number
  integer, intent(in)    :: root
  integer                :: rroot
  integer, save          :: cnt=0
  if (dbg .gt. 4) then 
    cnt = cnt + 1
#ifndef __xlc__
    call sleep(1)
#endif
    write(stdout,'(a,i7,a,3i4)') "Rank:",rank," Cnt,Root,Int:",cnt,root,integer_number
    call flush(stdout)
  endif
  rroot = merge(root,0,root.ge.0)
  call MPI_bcast(integer_number, 1, MPI_integer, rroot, comm, mpi_err)
  if (dbg .gt. 4) then 
    cnt = cnt + 1
#ifndef __xlc__
    call sleep(1)
#endif
    write(stdout,'(a,i7,a,3i4,a)') "Rank:",rank," Cnt,Root,Int:",cnt,root,integer_number," AFTER BCAST"
    call flush(stdout)
  endif
END SUBROUTINE mpi_broadcast
!-----------------------------------------------------------------------
! Wrapper for Broadcast of a logical scalar
SUBROUTINE mpi_broadcast_logical(integer_number,root)
  USE params, only : nodes, rank, comm, mpi_err, stdout, dbg
  USE PhotonPlasmaMpi
  implicit none
  logical                :: integer_number
  integer, intent(in)    :: root
  integer                :: rroot
  integer, save          :: cnt=0
  if (dbg .gt. 4) then 
    cnt = cnt + 1
#ifndef __xlc__
    call sleep(1)
#endif
    write(stdout,'(a,i7,a,2i4,l2)') "Rank:",rank," Cnt,Root,Int:",cnt,root,integer_number
    call flush(stdout)
  endif
  rroot = merge(root,0,root.ge.0)
  call MPI_bcast(integer_number, 1, MPI_LOGICAL, rroot, comm, mpi_err)
  if (dbg .gt. 4) then 
    cnt = cnt + 1
#ifndef __xlc__
    call sleep(1)
#endif
    write(stdout,'(a,i7,a,2i4,l2,a)') "Rank:",rank," Cnt,Root,Int:",cnt,root,integer_number," AFTER BCAST"
    call flush(stdout)
  endif
END SUBROUTINE mpi_broadcast_logical
!-----------------------------------------------------------------------
! Wrapper for Broadcast of a real scalar
SUBROUTINE mpi_broadcast_real(integer_number,root)
  USE params, only : nodes, rank, comm, mpi_err, stdout, dbg
  USE PhotonPlasmaMpi
  implicit none
  real                   :: integer_number
  integer, intent(in)    :: root
  integer                :: rroot
  integer, save          :: cnt=0
  if (dbg .gt. 4) then 
    cnt = cnt + 1
#ifndef __xlc__
    call sleep(1)
#endif
    write(stdout,'(a,i7,a,2i4,e12.5)') "Rank:",rank," Cnt,Root,Real:",cnt,root,integer_number
    call flush(stdout)
  endif
  rroot = merge(root,0,root.ge.0)
  call MPI_bcast(integer_number, 1, CMPI_REAL, rroot, comm, mpi_err)
  if (dbg .gt. 4) then 
    cnt = cnt + 1
#ifndef __xlc__
    call sleep(1)
#endif
    write(stdout,'(a,i7,a,2i4,e12.5,a)') "Rank:",rank," Cnt,Root,Int:",cnt,root,integer_number," AFTER BCAST"
    call flush(stdout)
  endif
END SUBROUTINE mpi_broadcast_real
!-----------------------------------------------------------------------
SUBROUTINE flush_all
  USE params, only: stdout, nodes, rank
  implicit none
  integer irank
  do irank=0,nodes-1
    call barrier
    if (irank == rank) then
      if (stdout > 0) call flush(stdout)
    end if
  end do
END SUBROUTINE flush_all
!-----------------------------------------------------------------------
SUBROUTINE global_nr_of_particles(gnp,gmp)                              ! gather sp%np on master
  USE params,  only: nodes, rank, nspecies, comm, mpi_err
  USE species, only: sp
  USE PhotonPlasmaMpi
  implicit none
  integer, dimension(nspecies,nodes)                :: gnp,gmp
  integer, dimension(:), allocatable              :: np
  integer                                         :: recvcnt
  allocate(np(nspecies))
  np = sp%np
  recvcnt = nspecies!*nodes
  call MPI_AllGather(np, nspecies, MPI_integer, gnp, recvcnt, &
                                   MPI_integer, comm, mpi_err)
  np = sp%mp
  call MPI_AllGather(np, nspecies, MPI_integer, gmp, recvcnt, &
                                   MPI_integer, comm, mpi_err)
  deallocate(np)
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE highest_particle_id_one_species(isp)
  USE params,  only : mpi_err,comm,mpi
  USE species, only : sp
  USE PhotonPlasmaMpi
  implicit none
  integer, intent(in) :: isp
  integer(kind=8)     :: imax
  call MPI_ALLREDUCE(sp(isp)%imax,imax,1,MPI_INTEGER8,MPI_MAX,mpi%sp(isp)%comm,mpi_err)
  call assert(mpi_err.eq.0,'highest_particle_id_one_species','ALLREDUCE')
  sp(isp)%imax = imax
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE highest_particle_id
  USE params,  only : mpi_err,comm,nspecies
  USE species, only : sp
  USE PhotonPlasmaMpi
  implicit none
  integer(kind=8), dimension(nspecies) :: imax,ilmax
  ilmax = sp%imax
  call MPI_ALLREDUCE(ilmax,imax,nspecies,MPI_INTEGER8,MPI_MAX,comm,mpi_err)
  call assert(mpi_err.eq.0,'highest_particle_id','ALLREDUCE')
  sp%imax = imax
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE mpi_allreduce_logical(llogical,glogical)
  USE params,  only : mpi_err, comm
  USE PhotonPlasmaMpi
  implicit none
  logical, intent(in) :: llogical
  logical             :: glogical
  call MPI_ALLREDUCE(llogical, glogical, 1, MPI_LOGICAL, MPI_LOR, comm, mpi_err)
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE mpi_allreduce_logical_isp(llogical,glogical,isp)
  USE params,  only : mpi_err, comm, mpi
  USE PhotonPlasmaMpi
  implicit none
  integer, intent(in) :: isp
  logical, intent(in) :: llogical
  logical             :: glogical
  call MPI_ALLREDUCE(llogical, glogical, 1, MPI_LOGICAL, MPI_LOR, mpi%sp(isp)%comm, mpi_err)
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE scatter_load(node, global, isp)
  USE params,  only : nodes, mpi_err, comm, rank, mpi
  USE PhotonPlasmaMpi
  implicit none
  integer,                intent(in) :: isp
  logical,                intent(in) :: node
  logical,          dimension(nodes) :: global
  integer                            :: inode
  integer, allocatable, dimension(:) :: iglobal
  integer                            :: verbose_local=0
  allocate(iglobal(nodes))
  inode   = merge(1,0,node)
                               if (verbose_local>0) print *, rank, "gathering global count"
  call MPI_AllGather(inode, 1, MPI_integer, iglobal, 1, MPI_integer, mpi%sp(isp)%comm, mpi_err)
  call assert(mpi_err.eq.0,'scatter_load','ALLGATHER')
  global = iglobal .eq. 1
  deallocate(iglobal)
END SUBROUTINE
!-----------------------------------------------------------------------
FUNCTION sum_energy(energy)
  USE params,  only : mpi_err, comm
  USE stat,    only : energy_kind
  USE PhotonPlasmaMpi
  implicit none
  real(kind=energy_kind), intent(in) :: energy                          ! partial energy from node
  real(kind=energy_kind)             :: sum_energy                      ! energy from all nodes
  call MPI_ALLREDUCE(energy,sum_energy,1,MPI_REAL_ENERGY,MPI_SUM,comm,mpi_err)
  call assert(mpi_err.eq.0,'sum_energy','ALLREDUCE')
END FUNCTION
!-----------------------------------------------------------------------
SUBROUTINE mpi_sum_local_slice(inside,axis,llb,lub,ssl)                 ! integrate 2-D slice along axis
  USE params,  only : mpi_err, mpi
  USE PhotonPlasmaMpi
  implicit none
  logical,               intent(in) :: inside                           !> do we contribute anything ?
  integer,               intent(in) :: axis                             !> axis to sum over
  integer, dimension(2), intent(in) :: llb, lub                         !> dimensions of slice
  real,    dimension(llb(1):lub(1),llb(2):lub(2)) :: ssl                !> slice
  real, dimension(:,:), allocatable :: ssl_local
  integer                           :: area, comm, dummy
  logical                           :: master
  ! FIXME: One should use the knowledge provided by inside, to only sum on the non-zero threads
  area = (lub(1)-llb(1)+1)*(lub(2)-llb(2)+1)
  select case(axis)
  case(1)
    master = mpi%x%master; comm = mpi%x%comm
  case(2)
    master = mpi%y%master; comm = mpi%y%comm
  case(3)
    master = mpi%z%master; comm = mpi%z%comm
  end select
  if (.not. master) then
      call MPI_REDUCE(ssl,dummy,area,CMPI_REAL,MPI_SUM,0,comm,mpi_err)
  else
      allocate(ssl_local(llb(1):lub(1),llb(2):lub(2))); ssl_local = ssl
      call MPI_REDUCE(ssl_local,ssl,area,CMPI_REAL,MPI_SUM,0,comm,mpi_err)
      deallocate(ssl_local)
      !FIXME MPI_IN_PLACE does not seem to work
      !call MPI_REDUCE(MPI_IN_PLACE,ssl,area,CMPI_REAL,MPI_SUM,0,comm,mpi_err)
  endif
END SUBROUTINE mpi_sum_local_slice
!------------------------------------------------------------------------
SUBROUTINE mpi_gather_global_slice(axis,llb,lub,ssl,gn,gsl,win,gnn)
  USE params,          only : mpi_err, mpi, rank
  USE PhotonPlasmaMpi
  implicit none
  integer,               intent(in) :: axis                              !> axis to sum over
  integer, dimension(2), intent(in) :: llb, lub                          !> dimensions of slice
  real,    dimension(llb(1):lub(1),llb(2):lub(2)) :: ssl                 !> local slice chunk
  integer, dimension(2), intent(in) :: gn                                !> size of gsl array ([1,1] on slave arrays) 
  integer, dimension(2), intent(in) :: gnn                               !> global slice dims
  real,    dimension(gn(1),gn(2))   :: gsl                               !> global slice
  integer                           :: win                               !> mpi window identifier
  integer                           :: comm, iy, ox, oy, lnx, lny, offx, offy, len
  logical                           :: master
  integer(kind=MPI_ADDRESS_KIND)    :: size, pos

  select case(axis)
  case(1)
    master = mpi%yz%master; comm = mpi%yz%comm
    ox = mpi%offset(2); oy = mpi%offset(3)
  case(2)
    master = mpi%zx%master; comm = mpi%zx%comm
    ox = mpi%offset(1); oy = mpi%offset(3)
  case(3)
    master = mpi%xy%master; comm = mpi%xy%comm
    ox = mpi%offset(1); oy = mpi%offset(2)
  end select

  ! Assemble the global 2D array on the master thread using 1-sided PUT's
  !----------------------------------------------------------------------
  call MPI_TYPE_EXTENT(CMPI_REAL, len, mpi_err)                          ! get size of datatype in bytes

  size = merge(gn(1)*gn(2),0,master)                                     ! window only has non-zero length on master thread
  call MPI_WIN_CREATE(gsl, size, len, MPI_INFO_NULL, comm, win, mpi_err) ! create memory region/window for 1-sided communication

  call MPI_WIN_FENCE(MPI_MODE_NOPRECEDE, win, mpi_err)                   ! make sure window has been setup

  offx = ox+llb(1); offy = oy+llb(2)                                     ! 0-based offsets
  lnx  = lub(1)-llb(1)+1; lny = lub(2)-llb(2)+1
  do iy=offy,offy+lny-1
    pos = offx + iy*gnn(1)                                               ! position starts at (0,0)
    call MPI_PUT(ssl(:,iy-oy), lnx, CMPI_REAL, 0, pos, lnx, CMPI_REAL, win, mpi_err) ! PUT one row at a time
  enddo
END SUBROUTINE mpi_gather_global_slice
!-----------------------------------------------------------------------
SUBROUTINE mpi_release_window(win)
  USE params, only : mpi_err
  USE PhotonPlasmaMpi
  implicit none
  integer              :: win                                           !> mpi window identifier
  call MPI_WIN_FENCE(MPI_MODE_NOSUCCEED, win, mpi_err)                  !> wait for operations to finish
  call MPI_WIN_FREE(win, mpi_err)                                       !> Free window
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE write_iofield(x)
  USE params, only :nodes, rank, data_unit, trace, comm, mpi_err, & 
                    stdout, hl, mpi, master, periodic, do_validate
  USE grid_m, only :g
  USE PhotonPlasmaMpi
  implicit none
  real, intent(in), dimension(g%n(1),g%n(2),g%n(3)) :: x
  real, allocatable   :: zsl(:,:), patch(:,:)
  real(kind=4), allocatable :: buf(:,:,:)
  integer(kind=8)     :: bytes8
  integer             :: iz, ir, gsz, i(4), tag, bytes, otag, isl, nsl, giz, gn(3), ub(3)
  integer, parameter  :: buffer_size=200                                ! Size of buffer for slices in MB
  logical, save       :: first_call=.true.
  integer, save       :: ncall=10
  real(kind=8)        :: t0
  real(kind=8), external :: wallclock
!.......................................................................
  ncall = ncall - 1                                                     ! counting down towards zero
  gn = merge(g%gn, g%gn+1, periodic)                                    ! save physical boundary if non-periodic
  ub = merge(g%ub+1, g%ub, mpi%ub)                                      ! save physical boundary if non-periodic
  otag = gn(3)*mpi%n(1)*mpi%n(2)
  if (master) then
    t0=wallclock()
    nsl = buffer_size*1024**2 / ((gn(1)*gn(2))*4)                       ! nr of slices in buffer
    nsl = max(1,min(nsl, gn(3)))                                        ! no need to overdo it
    if (first_call) write(stdout,*) 'WRITE_IOFIELD: Slices per buffer :', nsl
    first_call = .false.
    isl = 0
    bytes8 = product(int(gn,kind=8))*4; bytes=int(bytes8,kind=4)
    write(data_unit) bytes                                              ! write fortran counter
    ! First get the z-slices where master is xymaster
    allocate(buf(gn(1),gn(2),0:nsl-1))
    allocate(zsl(gn(1),gn(2)))                                          ! allocate a z-slice
    do iz=1,gn(3)                                                       ! loop over slices
      do ir=0,mpi%xy%nodes-1                                            ! loop over mpi-geometry
        if (iz <= ub(3)-g%lb(3) .and. ir==0) then
          zsl(1:ub(1)-g%lb(1),1:ub(2)-g%lb(2)) = &
            x(g%lb(1):ub(1)-1,g%lb(2):ub(2)-1,iz+g%lb(3)-1)             ! own slice
        else
          tag = ir*gn(3)+iz-1                                           ! tag for this z-slice / patch
          call MPI_RECV(i,4,MPI_INTEGER,MPI_ANY_SOURCE,tag,comm,status,mpi_err)! recv info
          allocate(patch(i(3),i(4)))                                    ! make patch
          call MPI_RECV(patch,i(3)*i(4),CMPI_REAL,MPI_ANY_SOURCE,tag+otag,comm,status,mpi_err) ! recv patch
          zsl(i(1):i(1)+i(3)-1,i(2):i(2)+i(4)-1) = patch
          deallocate(patch)
        endif
      enddo
      buf(:,:,isl) = zsl
      if (isl == nsl-1) write(data_unit) buf
      isl = mod(isl + 1, nsl)
      call barrier
    enddo
    if (isl > 0) write(data_unit) buf(:,:,0:isl-1)
    write(data_unit) bytes                                              ! write fortran counter again
    if (wallclock()-t0 > 60. .and. ncall >= 0 .and. .not. do_validate) &
      write(stdout,*) 'Time used to dump field :', wallclock()-t0
  else                                                                  ! if not master send local array
    i = (/ mpi%offset(1)+g%lb(1)+1, mpi%offset(2)+g%lb(2)+1, &          ! offset in global grid
               ub(1) - g%lb(1), ub(2) - g%lb(2) /)                      ! size of patch
    allocate(zsl(i(3),i(4)))
    do giz=1,gn(3)                                                      ! loop over slices
      iz=giz-mpi%offset(3)-1
      if (iz >= g%lb(3) .and. iz < ub(3)) then
        tag = (mpi%me(2)+mpi%me(1)*mpi%n(2))*gn(3)+iz+mpi%offset(3)     ! tag for this z-slice / patch
        call MPI_SEND(i,4,MPI_INTEGER,0,tag,comm,mpi_err)               ! send info
        zsl = x(g%lb(1):ub(1)-1,g%lb(2):ub(2)-1,iz)
        call MPI_SEND(zsl,i(3)*i(4),CMPI_REAL,0,tag+otag,comm,mpi_err)  ! send patch
      endif
      call barrier
    enddo
    deallocate(zsl)
  endif
END SUBROUTINE write_iofield
!-----------------------------------------------------------------------
REAL(kind=8) FUNCTION wallclock()
  USE params, only: do_validate
  USE PhotonPlasmaMpi
  implicit none
  real(kind=8), save :: count(2)
  logical,      save :: first_call=.true.

  if (first_call) then
    first_call = .false.
    count(1) = MPI_Wtime( )
  end if

  count(2) = MPI_Wtime( )
  wallclock = count(2)-count(1)
END FUNCTION wallclock
!-----------------------------------------------------------------------
SUBROUTINE file_open (file, mode, type, fio)
  USE params, only : mpi, periodic, rank, master, nspecies, stdout, mpi_err, comm
  USE grid_m,   only : g
  USE PhotonPlasmaMpi
  USE pic_io,   only : file_io_type
  implicit none
  integer               :: mode
  character(len=*)      :: file, type
  type(file_io_type)    :: fio
  integer               :: sz(3)
  logical               :: exists
  integer(kind=mpi_offset_kind) :: pos
!
  inquire (file=file, exist=exists)
  if (.not. exists .and. iand(mode, MPI_MODE_CREATE) .eq. 0) call error("file_open", trim(file)//" does not exist")

  fio%type = trim(type)

  select case(trim(type))
  case('dump')
    fio%size = merge (g%gn, g%gn+g%n-(g%ub-g%lb), periodic)
    fio%lb = merge (1,   g%lb, mpi%lb)
    fio%ub = merge (g%n+1, g%ub, mpi%ub)
    fio%offset = mpi%offset+g%lb+merge(0,g%lb-1,periodic .or. mpi%lb)
  case('field')
    fio%size=merge(g%gn,g%gn+1,periodic)
    fio%lb=g%lb
    fio%ub=merge(g%ub+1,g%ub,mpi%ub)
    fio%offset = mpi%offset+g%lb
  case('particles')
    call MPI_FILE_OPEN (comm, file, mode, MPI_INFO_NULL, fio%handle, err)       ! Open file   
    pos = fio%position
    CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_BYTE, MPI_BYTE, 'native', MPI_INFO_NULL, err)   ! set view with correct offset
    call barrier()                                                              ! may not be needed
    return
  case default
    call error('file_open','File type '//trim(type)//' is unknown')
  end select
  if (master) then
    if (mode == MPI_MODE_RDONLY) then
      write(stdout,*) 'file_openr: dimensions expected =',fio%size
    endif
  endif
  CALL MPI_FILE_OPEN (mpi_comm_world, file, mode, MPI_INFO_NULL, fio%handle, err)    
  sz = fio%ub-fio%lb
  CALL MPI_TYPE_CREATE_SUBARRAY (3, fio%size, sz, fio%offset, MPI_ORDER_FORTRAN, MPI_REAL, fio%filetype, err)   
  CALL MPI_TYPE_COMMIT (fio%filetype, err)
  if (verbose > 0) print'(2a,3i4,i3,2(2x,3i5))','file_open:', type, mpi%me, mode, fio%size, fio%offset
END
!-----------------------------------------------------------------------
SUBROUTINE file_openw (file,type,fio)
  USE PhotonPlasmaMpi
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type) :: fio
  character(len=*) file, type
  call file_open (file, MPI_MODE_CREATE + MPI_MODE_RDWR, type, fio)
END
!-----------------------------------------------------------------------
SUBROUTINE file_openw_seq (file,pos,type,fio)
  USE params, only : master, mpi_err, comm
  USE pic_io, only : file_io_type
  USE PhotonPlasmaMpi
  implicit none
  type(file_io_type) :: fio
  integer(kind=8) pos
  character(len=*) file, type
!.......................................................................
  if (master) fio%position = pos
  call MPI_BCAST(fio%position,1,MPI_INTEGER8,0,comm,mpi_err)
  call file_open (file, MPI_MODE_CREATE + MPI_MODE_RDWR, type, fio)
END
!-----------------------------------------------------------------------
SUBROUTINE file_openr_seq (file,pos,type,fio)
  USE params, only : master, mpi_err, comm
  USE pic_io, only : file_io_type
  USE PhotonPlasmaMpi
  implicit none
  type(file_io_type) :: fio
  integer(kind=8) pos
  character(len=*) file, type
!.......................................................................
  if (master) fio%position = pos
  call MPI_BCAST(fio%position,1,MPI_INTEGER8,0,comm,mpi_err)
  call file_open (file, MPI_MODE_RDONLY, type, fio)
END
!-----------------------------------------------------------------------
SUBROUTINE file_openr (file, type, fio)
  USE PhotonPlasmaMpi
  USE pic_io, only : file_io_type
  implicit none
  type(file_io_type) :: fio
  character(len=*) file, type
  call file_open (file, MPI_MODE_RDONLY, type, fio)
END
!-----------------------------------------------------------------------
SUBROUTINE file_write_seq_particles_real (x, fio, isp)
  USE params, only : mpi, periodic, master, rank
  USE grid_m,   only : g
  USE pic_io, only : file_io_type
  USE PhotonPlasmaMpi
  implicit none
  type(file_io_type)             :: fio
  integer, intent(in)            :: isp
  real(kind=4), dimension(fio%np(isp)) :: x
  integer                        :: filetype, np(1)
  integer(kind=8)                :: count
  integer(kind=MPI_OFFSET_KIND)  :: pos
  integer(kind=MPI_ADDRESS_KIND) :: off(1)
!
  count = 4*fio%gnp(isp)
  pos = fio%position
  ! Write start counter
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, MPI_INTEGER8, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_WRITE (fio%handle, count, 1, MPI_INTEGER8, status, err)
  pos = fio%position + 8
  ! Create vector type with 64-bit offset measured in bytes
  np = fio%np(isp)
  off = fio%cnp(isp)*4_8
  filetype=0
  CALL MPI_TYPE_CREATE_HINDEXED (1, np, off, MPI_REAL, filetype, err)
  CALL MPI_TYPE_COMMIT (filetype, err)  
  ! Write data
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_REAL, filetype, 'native', MPI_INFO_NULL, err)   
  CALL MPI_FILE_WRITE_ALL(fio%handle, x, np, MPI_REAL, status, err)
  pos = pos + fio%gnp(isp)*4 
  ! Free type
  CALL MPI_TYPE_FREE (filetype, err)
  ! Write end counter
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, MPI_INTEGER8, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_WRITE (fio%handle, count, 1, MPI_INTEGER8, status, err)
  fio%position = fio%position + fio%gnp(isp)*4 + 16
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE file_write_seq_particles_int8 (x, fio, isp)
  USE params, only : mpi, periodic, master, rank
  USE grid_m,   only : g
  USE pic_io, only : file_io_type
  USE PhotonPlasmaMpi
  implicit none
  type(file_io_type)             :: fio
  integer, intent(in)            :: isp
  integer(kind=8), dimension(fio%np(isp)) :: x
  integer                        :: filetype, np(1)
  integer(kind=8)                :: count
  integer(kind=MPI_OFFSET_KIND)  :: pos
  integer(kind=MPI_ADDRESS_KIND) :: off(1)
!
  count = 8*fio%gnp(isp)
  pos = fio%position
  ! Write start counter
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, MPI_INTEGER8, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_WRITE (fio%handle, count, 1, MPI_INTEGER8, status, err)
  pos = fio%position + 8
  ! Create vector type with 64-bit offset measured in bytes
  np = fio%np(isp)
  off = fio%cnp(isp)*8_8
  filetype=0
  CALL MPI_TYPE_CREATE_HINDEXED (1, np, off, MPI_INTEGER8, filetype, err)
  CALL MPI_TYPE_COMMIT (filetype, err)  
  ! Write data
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, filetype, 'native', MPI_INFO_NULL, err)   
  CALL MPI_FILE_WRITE_ALL(fio%handle, x, np, MPI_INTEGER8, status, err)
  pos = pos + fio%gnp(isp)*8 
  ! Free type
  CALL MPI_TYPE_FREE (filetype, err)
  ! Write end counter
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, MPI_INTEGER8, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_WRITE (fio%handle, count, 1, MPI_INTEGER8, status, err)
  fio%position = fio%position + fio%gnp(isp)*8 + 16
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE file_write_seq_particles_int2 (x, fio, isp)
  USE params, only : mpi, periodic, master, rank
  USE grid_m,   only : g
  USE pic_io, only : file_io_type
  USE PhotonPlasmaMpi
  implicit none
  type(file_io_type)             :: fio
  integer, intent(in)            :: isp
  integer(kind=2), dimension(fio%np(isp)) :: x
  integer                        :: filetype, np(1)
  integer(kind=8)                :: count
  integer(kind=MPI_OFFSET_KIND)  :: pos
  integer(kind=MPI_ADDRESS_KIND) :: off(1)
!
  count = 2*fio%gnp(isp)
  pos = fio%position
  ! Write start counter
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, MPI_INTEGER8, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_WRITE (fio%handle, count, 1, MPI_INTEGER8, status, err)
  pos = fio%position + 8
  ! Create vector type with 64-bit offset measured in bytes
  np = fio%np(isp)
  off = fio%cnp(isp)*2_8
  filetype=0
  CALL MPI_TYPE_CREATE_HINDEXED (1, np, off, MPI_INTEGER2, filetype, err)
  CALL MPI_TYPE_COMMIT (filetype, err)  
  ! Write data
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER2, filetype, 'native', MPI_INFO_NULL, err)   
  CALL MPI_FILE_WRITE_ALL(fio%handle, x, np, MPI_INTEGER2, status, err)
  pos = pos + fio%gnp(isp)*2 
  ! Free type
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, MPI_INTEGER8, 'native', MPI_INFO_NULL, err)   
  ! Write end counter
  if (master) CALL MPI_FILE_WRITE (fio%handle, count, 1, MPI_INTEGER8, status, err)
  CALL MPI_TYPE_FREE (filetype, err)
  fio%position = fio%position + fio%gnp(isp)*2 + 16
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE file_write (f,rec, fio)
  USE params, only : mpi, periodic, rank
  USE grid_m, only : g
  USE pic_io, only : file_io_type
  USE PhotonPlasmaMpi
  implicit none
  real, dimension(g%n(1),g%n(2),g%n(3)), intent(in) :: f
  type(file_io_type)  :: fio
  integer, intent(in) :: rec
  real(kind=4), allocatable, dimension(:,:,:):: f1
  integer(kind=MPI_OFFSET_KIND) :: pos
!
  pos = 4*(rec-1)*product(int(fio%size,kind=8))
  allocate (f1(fio%ub(1)-fio%lb(1),fio%ub(2)-fio%lb(2),fio%ub(3)-fio%lb(3)))
  f1=f(fio%lb(1):fio%ub(1)-1,fio%lb(2):fio%ub(2)-1,fio%lb(3):fio%ub(3)-1)
  call MPI_FILE_SET_VIEW (fio%handle, pos, MPI_REAL, fio%filetype, 'native', MPI_INFO_NULL, err)   
  call MPI_FILE_WRITE_ALL(fio%handle, f1, product(fio%ub-fio%lb), MPI_REAL, status, err)
  deallocate (f1)
END
!-----------------------------------------------------------------------
SUBROUTINE file_write_seq (f, fio)
  USE params, only : mpi, periodic, master, rank
  USE grid_m, only : g
  USE pic_io, only : file_io_type
  USE PhotonPlasmaMpi
  implicit none
  type(file_io_type) :: fio
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  real(kind=4), allocatable, dimension(:,:,:):: f1
  integer                  :: count
  integer(MPI_OFFSET_KIND) :: pos
!
  pos = fio%position
  allocate (f1(fio%ub(1)-fio%lb(1),fio%ub(2)-fio%lb(2),fio%ub(3)-fio%lb(3)))
  f1=f(fio%lb(1):fio%ub(1)-1,fio%lb(2):fio%ub(2)-1,fio%lb(3):fio%ub(3)-1)
  count = 4*product(fio%size)
!
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER, fio%filetype, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_WRITE (fio%handle, count, 1, MPI_INTEGER, status, err)
  fio%position = fio%position + 4; pos = fio%position
!
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_REAL, fio%filetype, 'native', MPI_INFO_NULL, err)   
  CALL MPI_FILE_WRITE_ALL(fio%handle, f1, product(fio%ub-fio%lb), MPI_REAL, status, err)
  fio%position = fio%position + 4*product(int(fio%size,kind=8)); pos = fio%position
!
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER, fio%filetype, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_WRITE (fio%handle, count, 1, MPI_INTEGER, status, err)
  fio%position = fio%position + 4
!
  deallocate (f1)
!
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE file_read_seq_particles_real (x, fio, isp)
  USE params, only : mpi, periodic, master, rank
  USE grid_m, only : g
  USE pic_io, only : file_io_type
  USE PhotonPlasmaMpi
  implicit none
  type(file_io_type)             :: fio
  integer, intent(in)            :: isp
  real(kind=4), dimension(fio%np(isp)) :: x
  integer                        :: filetype, np(1)
  integer(kind=8)                :: count, c1, c2
  integer(kind=MPI_OFFSET_KIND)  :: pos
  integer(kind=MPI_ADDRESS_KIND) :: off(1)
!
  count = 4*fio%gnp(isp)
  pos = fio%position
  ! Read start counter
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, MPI_INTEGER8, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_READ (fio%handle, c1, 1, MPI_INTEGER8, status, err)
  pos = fio%position + 8
  ! Create vector type with 64-bit offset measured in bytes
  np = fio%np(isp)
  off = fio%cnp(isp)*4_8
  filetype=0
  CALL MPI_TYPE_CREATE_HINDEXED (1, np, off, MPI_REAL, filetype, err)
  CALL MPI_TYPE_COMMIT (filetype, err)  
  ! Read data
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_REAL, filetype, 'native', MPI_INFO_NULL, err)   
  CALL MPI_FILE_READ_ALL(fio%handle, x, np, MPI_REAL, status, err)
  pos = pos + fio%gnp(isp)*4 
  ! Free type
  CALL MPI_TYPE_FREE (filetype, err)
  ! Read end counter
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, MPI_INTEGER8, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_READ (fio%handle, c2, 1, MPI_INTEGER8, status, err)
  fio%position = fio%position + fio%gnp(isp)*4 + 16
  if (master .and. (c1 .ne. count .or. c2 .ne. count)) then
    print *, 'Expected counter    :', count
    print *, 'Found start counter :', c1
    print *, 'Found start counter :', c2
    call error('file_read_seq_particles_real','File counters do not match expected length. File corrupted')
  endif
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE file_read_seq_particles_int8 (x, fio, isp)
  USE params, only : mpi, periodic, master, rank
  USE grid_m, only : g
  USE pic_io, only : file_io_type
  USE PhotonPlasmaMpi
  implicit none
  type(file_io_type)             :: fio
  integer, intent(in)            :: isp
  integer(kind=8), dimension(fio%np(isp)) :: x
  integer                        :: filetype, np(1)
  integer(kind=8)                :: count, c1, c2
  integer(kind=MPI_OFFSET_KIND)  :: pos
  integer(kind=MPI_ADDRESS_KIND) :: off(1)
!
  count = 8*fio%gnp(isp)
  pos = fio%position
  ! Read start counter
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, MPI_INTEGER8, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_READ (fio%handle, c1, 1, MPI_INTEGER8, status, err)
  pos = fio%position + 8
  ! Create vector type with 64-bit offset measured in bytes
  np = fio%np(isp)
  off = fio%cnp(isp)*8_8
  filetype=0
  CALL MPI_TYPE_CREATE_HINDEXED (1, np, off, MPI_INTEGER8, filetype, err)
  CALL MPI_TYPE_COMMIT (filetype, err)  
  ! Read data
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, filetype, 'native', MPI_INFO_NULL, err)   
  CALL MPI_FILE_READ_ALL(fio%handle, x, np, MPI_INTEGER8, status, err)
  pos = pos + fio%gnp(isp)*8 
  ! Free type
  CALL MPI_TYPE_FREE (filetype, err)
  ! Read end counter
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, MPI_INTEGER8, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_READ (fio%handle, c2, 1, MPI_INTEGER8, status, err)
  fio%position = fio%position + fio%gnp(isp)*8 + 16
  if (master .and. (c1 .ne. count .or. c2 .ne. count)) then
    print *, 'Expected counter    :', count
    print *, 'Found start counter :', c1
    print *, 'Found start counter :', c2
    call error('file_read_seq_particles_int8','File counters do not match expected length. File corrupted')
  endif
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE file_read_seq_particles_int2 (x, fio, isp)
  USE params, only : mpi, periodic, master, rank
  USE grid_m, only : g
  USE pic_io, only : file_io_type
  USE PhotonPlasmaMpi
  implicit none
  type(file_io_type)             :: fio
  integer, intent(in)            :: isp
  integer(kind=2), dimension(fio%np(isp)) :: x
  integer                        :: filetype, np(1)
  integer(kind=8)                :: count, c1, c2
  integer(kind=MPI_OFFSET_KIND)  :: pos
  integer(kind=MPI_ADDRESS_KIND) :: off(1)
!
  count = 2*fio%gnp(isp)
  pos = fio%position
  ! Read start counter
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, MPI_INTEGER8, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_READ (fio%handle, c1, 1, MPI_INTEGER8, status, err)
  pos = fio%position + 8
  ! Create vector type with 64-bit offset measured in bytes
  np = fio%np(isp)
  off = fio%cnp(isp)*2_8
  filetype=0
  CALL MPI_TYPE_CREATE_HINDEXED (1, np, off, MPI_INTEGER2, filetype, err)
  CALL MPI_TYPE_COMMIT (filetype, err)  
  ! Read data
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER2, filetype, 'native', MPI_INFO_NULL, err)   
  CALL MPI_FILE_READ_ALL(fio%handle, x, np, MPI_INTEGER2, status, err)
  pos = pos + fio%gnp(isp)*2 
  ! Free type
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER8, MPI_INTEGER8, 'native', MPI_INFO_NULL, err)   
  ! Read end counter
  if (master) CALL MPI_FILE_READ (fio%handle, c2, 1, MPI_INTEGER8, status, err)
  CALL MPI_TYPE_FREE (filetype, err)
  fio%position = fio%position + fio%gnp(isp)*2 + 16
  if (master .and. (c1 .ne. count .or. c2 .ne. count)) then
    print *, 'Expected counter    :', count
    print *, 'Found start counter :', c1
    print *, 'Found start counter :', c2
    call error('file_read_seq_particles_int2','File counters do not match expected length. File corrupted')
  endif
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE file_read (f, rec, fio)
  USE params, only : mpi, periodic
  USE grid_m, only : g
  USE pic_io, only : file_io_type
  USE PhotonPlasmaMpi
  implicit none
  type(file_io_type) :: fio
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  real(kind=4), allocatable, dimension(:,:,:):: f1
  integer rec
  integer(kind=MPI_OFFSET_KIND) pos
!
  pos = 4*(rec-1)*product(int(fio%size,kind=8))
  allocate (f1(fio%ub(1)-fio%lb(1),fio%ub(2)-fio%lb(2),fio%ub(3)-fio%lb(3)))
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_REAL, fio%filetype, 'native', MPI_INFO_NULL, err)   
  CALL MPI_FILE_READ_ALL (fio%handle, f1, product(fio%ub-fio%lb), MPI_REAL, status, err)
  f(fio%lb(1):fio%ub(1)-1,fio%lb(2):fio%ub(2)-1,fio%lb(3):fio%ub(3)-1)=f1
  deallocate (f1)
  call overlap(f)
END
!-----------------------------------------------------------------------
SUBROUTINE file_read_seq (f, fio)
  USE params, only : mpi, periodic, master, rank
  USE grid_m, only : g
  USE pic_io, only : file_io_type
  USE PhotonPlasmaMpi
  implicit none
  type(file_io_type) :: fio
  real, dimension(g%n(1),g%n(2),g%n(3)):: f
  real(kind=4), allocatable, dimension(:,:,:):: f1
  integer                  :: count,c1,c2
  integer(MPI_OFFSET_KIND) :: pos
!
  pos = fio%position
  allocate (f1(fio%ub(1)-fio%lb(1),fio%ub(2)-fio%lb(2),fio%ub(3)-fio%lb(3)))
  count = 4*product(fio%size)
!
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER, fio%filetype, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_READ (fio%handle, c1, 1, MPI_INTEGER, status, err)
  fio%position = fio%position + 4; pos = fio%position
!
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_REAL, fio%filetype, 'native', MPI_INFO_NULL, err)   
  CALL MPI_FILE_READ_ALL(fio%handle, f1, product(fio%ub-fio%lb), MPI_REAL, status, err)
  fio%position = fio%position + 4*product(int(fio%size,kind=8)); pos = fio%position
  f(fio%lb(1):fio%ub(1)-1,fio%lb(2):fio%ub(2)-1,fio%lb(3):fio%ub(3)-1)=f1
!
  CALL MPI_FILE_SET_VIEW (fio%handle, pos, MPI_INTEGER, fio%filetype, 'native', MPI_INFO_NULL, err)   
  if (master) CALL MPI_FILE_READ (fio%handle, c2, 1, MPI_INTEGER, status, err)
  fio%position = fio%position + 4
!
  deallocate (f1)
!
  if (master .and. (c1 .ne. count .or. c2 .ne. count)) then
    print *, 'Nr of elements      :', product(fio%size)
    print *, 'Expected counter    :', count
    print *, 'Found start counter :', c1
    print *, 'Found start counter :', c2
    call error('file_read_seq','File counters do not match internal and/or expected length. File corrupted')
  endif
END SUBROUTINE
!-----------------------------------------------------------------------
SUBROUTINE file_close(fio)
  USE params, only : trace, dbg, master, stdout
  USE pic_io, only : file_io_type
  USE PhotonPlasmaMpi
  implicit none
  type(file_io_type) :: fio
  integer :: isp
  select case(trim(fio%type))
  case('dump')
    if ((dbg > 0 .or. trace) .and. master) write(stdout,*) 'file_close: Freeing IO type for file type '//trim(fio%type)
    CALL MPI_TYPE_FREE (fio%filetype, err)   
  case('field')
    if ((dbg > 0 .or. trace) .and. master) write(stdout,*) 'file_close: Freeing IO type for file type '//trim(fio%type)
    CALL MPI_TYPE_FREE (fio%filetype, err)   
  case('particles')
  case default
    call error('file_close','File type '//trim(fio%type)//' is unknown')
  end select
  CALL MPI_FILE_CLOSE(fio%handle, err)
END
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_real(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : comm, mpi_err, rank
  implicit none
  integer,            intent(in)  :: thread
  integer,            intent(in)  :: n
  real, dimension(n), intent(in)  :: x
  call MPI_SEND(x,n,CMPI_REAL,thread,rank,comm,mpi_err)
END SUBROUTINE mpi_send_real
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_integer(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : comm, mpi_err, rank
  implicit none
  integer,               intent(in)  :: thread
  integer,               intent(in)  :: n
  integer, dimension(n), intent(in)  :: x
  call MPI_SEND(x,n,MPI_INTEGER,thread,rank,comm,mpi_err)
END SUBROUTINE mpi_send_integer
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_integer8(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : comm, mpi_err, rank
  implicit none
  integer,                       intent(in)  :: thread
  integer,                       intent(in)  :: n
  integer(kind=8), dimension(n), intent(in)  :: x
  call MPI_SEND(x,n,MPI_INTEGER8,thread,rank,comm,mpi_err)
END SUBROUTINE mpi_send_integer8
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_logical(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : comm, mpi_err, rank
  implicit none
  integer,               intent(in)  :: thread
  integer,               intent(in)  :: n
  logical, dimension(n), intent(in)  :: x
  call MPI_SEND(x,n,MPI_LOGICAL,thread,rank,comm,mpi_err)
END SUBROUTINE mpi_send_logical
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_real_xy(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : mpi, mpi_err
  implicit none
  integer,            intent(in)  :: thread
  integer,            intent(in)  :: n
  real, dimension(n), intent(in)  :: x
  call MPI_SEND(x,n,CMPI_REAL,thread,mpi%xy%rank,mpi%xy%comm,mpi_err)
END SUBROUTINE mpi_send_real_xy
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_integer_xy(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : mpi, mpi_err
  implicit none
  integer,               intent(in)  :: thread
  integer,               intent(in)  :: n
  integer, dimension(n), intent(in)  :: x
  call MPI_SEND(x,n,MPI_INTEGER,thread,mpi%xy%rank,mpi%xy%comm,mpi_err)
END SUBROUTINE mpi_send_integer_xy
!-----------------------------------------------------------------------
SUBROUTINE mpi_send_integer8_xy(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : mpi, mpi_err
  implicit none
  integer,                       intent(in)  :: thread
  integer,                       intent(in)  :: n
  integer(kind=8), dimension(n), intent(in)  :: x
  call MPI_SEND(x,n,MPI_INTEGER8,thread,mpi%xy%rank,mpi%xy%comm,mpi_err)
END SUBROUTINE mpi_send_integer8_xy
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_real(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : comm, mpi_err
  implicit none
  integer,            intent(in)  :: thread
  integer,            intent(in)  :: n
  real, dimension(n)              :: x
  call MPI_RECV(x,n,CMPI_REAL,thread,thread,comm,status,mpi_err)
END SUBROUTINE mpi_recv_real
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_integer(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : comm, mpi_err
  implicit none
  integer,               intent(in)  :: thread
  integer,               intent(in)  :: n
  integer, dimension(n)              :: x
  call MPI_RECV(x,n,MPI_INTEGER,thread,thread,comm,status,mpi_err)
END SUBROUTINE mpi_recv_integer
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_integer8(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : comm, mpi_err
  implicit none
  integer,                       intent(in)  :: thread
  integer,                       intent(in)  :: n
  integer(kind=8), dimension(n)              :: x
  call MPI_RECV(x,n,MPI_INTEGER8,thread,thread,comm,status,mpi_err)
END SUBROUTINE mpi_recv_integer8
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_logical(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : comm, mpi_err
  implicit none
  integer,               intent(in)  :: thread
  integer,               intent(in)  :: n
  logical, dimension(n)              :: x
  call MPI_RECV(x,n,MPI_LOGICAL,thread,thread,comm,status,mpi_err)
END SUBROUTINE mpi_recv_logical
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_real_xy(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : mpi, mpi_err
  implicit none
  integer,            intent(in)  :: thread
  integer,            intent(in)  :: n
  real,             dimension(n)  :: x
  call MPI_RECV(x,n,CMPI_REAL,thread,thread,mpi%xy%comm,status,mpi_err)
END SUBROUTINE mpi_recv_real_xy
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_integer_xy(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : mpi, mpi_err
  implicit none
  integer,               intent(in)  :: thread
  integer,               intent(in)  :: n
  integer, dimension(n)              :: x
  call MPI_RECV(x,n,MPI_INTEGER,thread,thread,mpi%xy%comm,status,mpi_err)
END SUBROUTINE mpi_recv_integer_xy
!-----------------------------------------------------------------------
SUBROUTINE mpi_recv_integer8_xy(thread, n, x)
  USE PhotonPlasmaMPI
  USE params, only : mpi, mpi_err
  implicit none
  integer,                       intent(in)  :: thread
  integer,                       intent(in)  :: n
  integer(kind=8), dimension(n)              :: x
  call MPI_RECV(x,n,MPI_INTEGER8,thread,thread,mpi%xy%comm,status,mpi_err)
END SUBROUTINE mpi_recv_integer8_xy
!================================================================================
! stats to do with the mpi decomposition
!================================================================================
SUBROUTINE mpi_stats
  USE PhotonPlasmaMpi
  USE params,  only : datadir, mpi, master, nodes, it, time, mpi, mpi_err, nspecies, comm, mpi_stats_unit
  USE grid_m,    only : g
  USE species, only : sp
  implicit none
  integer, allocatable :: np(:), nnp(:,:)
  integer              :: nelem
  logical, save        :: first_call=.true.
  !
  integer, parameter   :: io_format=2
  !
  ! np, mp, offset, lb, ub, n, mpi%me
  nelem = nspecies*2+3*5

  allocate(np(nelem))
  np = (/ sp%np, sp%mp, mpi%offset, g%lb, g%ub, g%n, mpi%me /)
  if (master) then; allocate(nnp(nelem,nodes)); else; allocate(nnp(1,1)); endif 
  call MPI_GATHER(np, nelem, MPI_INTEGER, nnp, nelem, MPI_INTEGER, 0, comm, mpi_err)

  if (master) then
    if (first_call) then
      first_call = .false.
      open(mpi_stats_unit,file=trim(datadir)//'/mpi_stats.dat',form='unformatted',status='unknown')
      write(mpi_stats_unit) io_format
      write(mpi_stats_unit) nspecies, nodes, mpi%n, nelem
    endif
    write(mpi_stats_unit) time, it, nnp
    deallocate(nnp)
  endif  
  deallocate(np)
END SUBROUTINE mpi_stats
!================================================================================
SUBROUTINE finalize_mpi_stats
  USE params, only : master, mpi_stats_unit
  implicit none
  if (master) close(mpi_stats_unit)
END SUBROUTINE finalize_mpi_stats
!===============================================================================
LOGICAL FUNCTION any_mpi (bad)
  USE PhotonPlasmaMpi
  implicit none
  logical bad 
  integer one, sum
  one = merge (1, 0, bad)
  call MPI_Allreduce (one, sum, 1, MPI_INTEGER, MPI_SUM, mpi_comm_world, err)
  any_mpi = (sum > 0)
END FUNCTION
!===============================================================================
LOGICAL FUNCTION all_mpi (ok)
  USE PhotonPlasmaMpi
  implicit none
  logical ok 
  integer one, sum
  one = merge (0, 1, ok)
  call MPI_Allreduce (one, sum, 1, MPI_INTEGER, MPI_SUM, mpi_comm_world, err)
  all_mpi = (sum == 0)
END FUNCTION
!===============================================================================
#ifdef BGQ
! Routine that checks the place threads in the hardware layout and prints it
! to a file. This is very useful for debugging that the hardware layout was
! configured correctly on a run.
!===============================================================================
SUBROUTINE check_host_order_BGQ
  USE params, only : mpi,master,rank,nodes,comm,data_unit,mfile
  USE iso_c_binding
  implicit none
  include 'mpif.h'
  integer :: status(MPI_STATUS_SIZE)
  integer :: ir,ndims,err
  integer, allocatable, dimension(:)   :: coords, d
  integer, allocatable, dimension(:,:) :: dg
  character(len=mfile) :: fname
  interface
    function MPIX_Torus_ndims(ndims) bind(C, name="MPIX_Torus_ndims") result(err)
      use iso_c_binding
      integer(c_int)            :: err
      integer(c_int32_t)        :: ndims
    end function MPIX_Torus_ndims
  end interface
  interface
    function MPIX_rank2torus(rank,coords) bind(C, name="MPIX_Rank2torus") result(err)
      use iso_c_binding
      integer(c_int)                   :: err
      integer(c_int32_t),        value :: rank
      integer(c_int32_t), dimension(6) :: coords
    end function MPIX_rank2torus
  end interface
  err = MPIX_Torus_ndims(ndims)
  if (master .and. ndims .ne. 5) print *, 'Number of dimensions in hardware network :', ndims
  allocate(d(ndims+4),coords(ndims+1))
  err = MPIX_rank2torus(rank,coords)
  d(1:3) = mpi%me
  d(4:ndims+4) = coords
  if (master) then; allocate(dg(ndims+4,nodes)); else; allocate(dg(1,1)); endif
  call MPI_GATHER(d, ndims+4, MPI_INTEGER, dg, ndims+4, MPI_INTEGER, 0, comm, err)
  if (master) then
    fname = 'thread_layout.txt'
    open (data_unit,file=fname,status='unknown',form='formatted')
    do ir=1,nodes
     write(data_unit,'(1x,a,i7,3(a,i4),6(a,i2))') 'rank =',ir,', me=',dg(1,ir),', ',dg(2,ir),', ',dg(3,ir),', a=',dg(4,ir),', b=',dg(5,ir),' c=',dg(6,ir),' d=',dg(7,ir),' e=',dg(8,ir),', t=',dg(9,ir)
    enddo
    close(data_unit)
  endif
  call barrier()
END SUBROUTINE check_host_order_BGQ
#endif
!===============================================================================
! Mpi/mpi.f90 $Id$
! New thread parallel version of Send Particles
!===============================================================================
SUBROUTINE SendParticles_omp
  USE PhotonPlasmaMPI
  USE params,  only : nspecies,comm,mpi_err,nodes,rank,mpi,&
                      mdim,periodic,master,stdout,stdall
  USE species, only : sp,particle
  USE grid_m,    only : g
  USE debug,   only : ptrace, void=>verbose, debugit, dbg_mpi
  implicit none
  type(particle),    dimension(:), pointer     :: pa
  type(particle)                               :: pas
  type(particle),    dimension(:), allocatable :: pal1,pal2,pau1,pau2
  type idx_t
    integer                                    :: nl,nu,nw
    integer, allocatable, dimension(:)         :: l,u,w
  end type
  type(idx_t), save, dimension(:), allocatable :: idx
  integer                                      :: ip,ipl,ipu,ipw,np,isp,i,ilb,iub,&
                                                  npal,npal1,npal2,npau,npau1,npau2,npaw,&
                                                  particles_needed
  logical                                      :: ww,palb,paub,too_many_particles
  integer                                      :: r1, r2, r3, r4, r5, r6, r7, r8  ! mpi requests
  integer(kind=2)                              :: glb,gub,gn,q
  character(len=1), dimension(3)               :: cdir

  if (nodes.eq.1) return                                                  ! If uni-cpu no particle sends
  call trace_enter('Send Particles')

  cdir = (/ 'x', 'y', 'z' /)

  if (.not. allocated(idx)) then
    allocate(idx(nspecies))
    do isp=1,nspecies
      idx(isp)%nl=max(sp(isp)%np / 20,1)
      idx(isp)%nu=max(sp(isp)%np / 20,1)
      idx(isp)%nw=max(sp(isp)%np / 20,1)
      allocate(idx(isp)%l(idx(isp)%nl))
      allocate(idx(isp)%u(idx(isp)%nu))
      allocate(idx(isp)%w(idx(isp)%nw))
    enddo
  endif

  do i=1,mdim
    if (mpi%n(i) == 1) cycle                                              ! nothing to send if only 1 thread in this dir
    gn = g%gn(i)                                                          ! global size
    ilb= g%lb(i) + mpi%offset(i)
    iub= g%ub(i) + mpi%offset(i)
    glb= g%ghlb(i)
    gub= g%gn(i) - g%ghub(i)
    if (mpi%lb(i)) ilb = ilb-10                                           ! don't ship
    if (mpi%ub(i)) iub = iub+10                                           ! don't ship
    do isp=1,nspecies
      r1 = MPI_REQUEST_NULL; r2 = MPI_REQUEST_NULL; r3 = MPI_REQUEST_NULL; r4 = MPI_REQUEST_NULL ! zero requests
      r5 = MPI_REQUEST_NULL; r6 = MPI_REQUEST_NULL; r7 = MPI_REQUEST_NULL; r8 = MPI_REQUEST_NULL
      pa  => sp(isp)%particle
      np  =  sp(isp)%np
      !-------------------------------------------------------------------
      ! Count how many particles we have to trabsfer in each direction
      ! allocate index arrays for them, and then store indices
      !-------------------------------------------------------------------
      ! Use label-GOTO construct for a repeat-until loop
10    ipl=0; ipu=0; ipw=0
      if (periodic(i)) then
       if (mpi%me(i)==0) then                                                   ! first node in direction i ?
        do ip=1,np
         ww = pa(ip)%w.ne.0.
         q = mod(pa(ip)%q(i),gn); q = merge(q,q+gn,q>=0)                  ! wrap integer position
         paub = q >= iub .and. ww                                         ! crossed upper bndry?
         palb = q >= gub .and. ww                                         ! crossed lower bndry?  CHECK!!
         paub = merge(.false.,paub,palb)                                  ! cannot cross both simulataneously
         if (palb) then;     ipl = ipl + 1; idx(isp)%l(min(ipl,idx(isp)%nl))=ip; endif
         if (paub) then;     ipu = ipu + 1; idx(isp)%u(min(ipu,idx(isp)%nu))=ip; endif
         if (.not. ww) then; ipw = ipw + 1; idx(isp)%w(min(ipw,idx(isp)%nw))=ip; endif
        enddo
       else if (mpi%me(i)==mpi%n(i)-1) then                               ! last node in direction i ?
        do ip=1,np
         ww = pa(ip)%w.ne.0.
         q = mod(pa(ip)%q(i),gn); q = merge(q,q+gn,q>=0)                  ! wrap integer position
         palb = q <  ilb .and. ww                                         ! crossed lower bndry?
         paub = q <= glb .and. ww                                         ! crossed upper bndry?  CHECK!!
         palb = merge(.false.,palb,paub)                                  ! cannot cross both simulataneously
         if (palb) then;     ipl = ipl + 1; idx(isp)%l(min(ipl,idx(isp)%nl))=ip; endif
         if (paub) then;     ipu = ipu + 1; idx(isp)%u(min(ipu,idx(isp)%nu))=ip; endif
         if (.not. ww) then; ipw = ipw + 1; idx(isp)%w(min(ipw,idx(isp)%nw))=ip; endif
        enddo
       else                                                               ! interior node in direction i
        do ip=1,np
         ww = pa(ip)%w.ne.0.
         q = mod(pa(ip)%q(i),gn); q = merge(q,q+gn,q>=0)                  ! wrap integer position
         paub = q >= iub .and. ww
         palb = q <  ilb .and. ww
         if (palb) then;     ipl = ipl + 1; idx(isp)%l(min(ipl,idx(isp)%nl))=ip; endif
         if (paub) then;     ipu = ipu + 1; idx(isp)%u(min(ipu,idx(isp)%nu))=ip; endif
         if (.not. ww) then; ipw = ipw + 1; idx(isp)%w(min(ipw,idx(isp)%nw))=ip; endif
        enddo
       endif
       npal1 = ipl                                                           ! Number of particles for lower bndry
       npau1 = ipu                                                           ! Number of particles for upper bndry
       npaw  = ipw                                                           ! Number of weightless particles
      else
       do ip=1,np
        ww   = pa(ip)%w.ne.0.                                             ! Particle weightless?
        palb = pa(ip)%q(i) <  ilb .and. ww                                ! crossed lower bndry?
        paub = pa(ip)%q(i) >= iub .and. ww                                ! crossed upper bndry?
        if (palb) then;     ipl = ipl + 1; idx(isp)%l(min(ipl,idx(isp)%nl))=ip; endif
        if (paub) then;     ipu = ipu + 1; idx(isp)%u(min(ipu,idx(isp)%nu))=ip; endif
        if (.not. ww) then; ipw = ipw + 1; idx(isp)%w(min(ipw,idx(isp)%nw))=ip; endif
       enddo
       npal1 = ipl                                                           ! Number of particles for lower bndry
       npau1 = ipu                                                           ! Number of particles for upper bndry
       npaw  = ipw                                                           ! Number of weightless particles
       if (npal1 > 0 .and. mpi%lb(i)) then
         call warning_all_nodes('SendParticles-'//cdir(i), &
           'Trying to send particles downwards, even though we are at a physical lower bndry')
         npal1=0
       endif
       if (npau1 > 0 .and. mpi%ub(i)) then
         call warning_all_nodes('SendParticles-'//cdir(i), &
           'Trying to send particles upwards, even though we are at a physical upper bndry')
         npau1=0
       endif
      end if
      ! check if the counting arrays where too short. Use of GOTO is the easiest option here.
      if (idx(isp)%nl < ipl .or. idx(isp)%nu < ipu .or. idx(isp)%nw < ipw) then
        if (idx(isp)%nl < ipl) then; idx(isp)%nl=ipl; deallocate(idx(isp)%l); allocate(idx(isp)%l(idx(isp)%nl)); endif
        if (idx(isp)%nu < ipu) then; idx(isp)%nu=ipu; deallocate(idx(isp)%u); allocate(idx(isp)%u(idx(isp)%nu)); endif
        if (idx(isp)%nw < ipw) then; idx(isp)%nw=ipw; deallocate(idx(isp)%w); allocate(idx(isp)%w(idx(isp)%nw)); endif
        goto 10
      endif

      if (debugit(dbg_mpi,0) .or. verbose>0) &
       write(stdall,'(1x,a,2i3,i7,3i4,3i6,2i4,l3)') &
          'SendParticles: i,isp,rank,me,ndn,nup,nw0,dn,up,int =', &
          i,isp,rank,mpi%me,npal1,npau1,npaw,mpi%dn(i),mpi%up(i),mpi%interior(i)
      if (mpi%me(i) == 0 .and. .not. mpi%interior(i)) &                 ! lower edge. non-periodic
        npal2 = 0                                                       ! no dnward transfer
      if (mpi%me(i) > 0  .and. .not. mpi%interior(i)) &                 ! upper edge. non-periodic
        npau2 = 0                                                       ! no upward transfer
      
      if (mpi%interior(i)) then                                           ! interior node?
        call MPI_IRECV(npal2,1,MPI_INTEGER,mpi%dn(i),mpi%dn(i)+nodes+2*isp*nodes,mpi%sp(isp)%comm,r3,mpi_err)   ! recieve lower
        call MPI_IRECV(npau2,1,MPI_INTEGER,mpi%up(i),mpi%up(i)      +2*isp*nodes,mpi%sp(isp)%comm,r4,mpi_err)   ! recieve upper
        call MPI_ISEND(npal1,1,MPI_INTEGER,mpi%dn(i),rank           +2*isp*nodes,mpi%sp(isp)%comm,r1,mpi_err)   ! send lower
        call MPI_ISEND(npau1,1,MPI_INTEGER,mpi%up(i),rank+nodes     +2*isp*nodes,mpi%sp(isp)%comm,r2,mpi_err)   ! send upper
        !call MPI_WAIT(r1,status,mpi_err)
        !call MPI_WAIT(r2,status,mpi_err)
        call MPI_WAIT(r3,status,mpi_err)
        call MPI_WAIT(r4,status,mpi_err)
      else
        if (mpi%me(i) == 0) then                                          ! lower edge. non-periodic
          call MPI_IRECV(npau2,1,MPI_INTEGER,mpi%up(i),mpi%up(i) +2*isp*nodes,mpi%sp(isp)%comm,r4,mpi_err) ! recieve upper
          call MPI_ISEND(npau1,1,MPI_INTEGER,mpi%up(i),rank+nodes+2*isp*nodes,mpi%sp(isp)%comm,r2,mpi_err) ! send upper
          !call MPI_WAIT(r2,status,mpi_err)
          call MPI_WAIT(r4,status,mpi_err)
        else                                                              ! upper edge. non-periodic
          call MPI_IRECV(npal2,1,MPI_INTEGER,mpi%dn(i),mpi%dn(i)+nodes+2*isp*nodes,mpi%sp(isp)%comm,r3,mpi_err) ! recieve lower
          call MPI_ISEND(npal1,1,MPI_INTEGER,mpi%dn(i),rank           +2*isp*nodes,mpi%sp(isp)%comm,r1,mpi_err) ! send lower
          !call MPI_WAIT(r1,status,mpi_err)
          call MPI_WAIT(r3,status,mpi_err)
        endif
      endif 
      if (verbose>2) print *,'done',rank
      !if (mpi%me(i)<=1) print '(a,i2,a,i5,a,3i4,a,2i9,a,2i9)', &
      !  'I=',i,' R=',rank,' ME=',mpi%me,' SEND D,U:',npal1,npau1,' RECV D,U:',npal2,npau2
      npal = max(npal1,npal2)                                             ! parts to send dnward
      npau = max(npau1,npau2)                                             ! parts to send upward
      !-------------------------------------------------------------------
      ! Check if we have enough space to receive the particles. This could be
      ! refined, in fact only condition nr. III is strictly speaking a problem.
      !-------------------------------------------------------------------
      ! I)            total from dn > dn   +w=0  +free slots
      too_many_particles =  npal .gt. npal1+npaw+sp(isp)%mp-np
      ! II)           total from up > yp   +w=0  +free slots
      too_many_particles = (npau .gt. npau1+npaw+sp(isp)%mp-np).or.too_many_particles
      ! III)              total to recv > dn   +up   +w=0  +free slots 
      too_many_particles = (npal+npau.gt.npal1+npau1+npaw+sp(isp)%mp-np) &
                                     .or.too_many_particles
      particles_needed = max(npal - (sp(isp)%mp - (np-npal1+npaw)),0) + & ! needed from below
                         max(npau - (sp(isp)%mp - (np-npau1+npaw)),0)     ! needed from above
      call load_balance_one_species(isp,too_many_particles,particles_needed,'SendParticles-'//cdir(i)) 
      if (too_many_particles) pa => sp(isp)%particle                      ! repoint pointer, since array can be somewhere else
      if (npal>0) allocate(pal1(npal))                                         ! Lower scratch array
      if (npau>0) allocate(pau1(npau))                                         ! Upper scratch array

      !-------------------------------------------------------------------
      ! Recv particle data to the scratch arrays for the upper parts
      !-------------------------------------------------------------------
      if (npau>0) then
        allocate(pau2(npau))                                              ! Upper scratch array
        call MPI_IRECV(pau2,npau,pa_t,mpi%up(i),mpi%up(i)+2*isp*nodes,mpi%sp(isp)%comm,r8,mpi_err) ! recv upper parts
      endif
      !-------------------------------------------------------------------
      ! Recv particle data to the scratch arrays for the lower parts
      !-------------------------------------------------------------------
      if (npal>0) then                                                    ! Move parts downwards
        allocate(pal2(npal))                                              ! Lower scratch array
        call MPI_IRECV(pal2,npal,pa_t,mpi%dn(i),mpi%dn(i)+nodes+2*isp*nodes,mpi%sp(isp)%comm,r7,mpi_err) ! recv lower parts
      endif
      !-------------------------------------------------------------------
      ! Copy particle data to the scratch arrays for sending the lower parts
      !-------------------------------------------------------------------
      if (npal>0) then                                                 ! Move parts downwards
        do ip=1,npal1                                                  ! copy in physical particles
          pal1(ip)=pa(idx(isp)%l(ip))
        enddo
        do ip=1,npal-npal1                                             ! fill up with weightless particles
          if (ip <= npaw) then
            pal1(ip + npal1)=pa(idx(isp)%w(ip))
          else
            pal1(ip + npal1)=pa(np+ip-npaw)
          endif
        enddo
        call MPI_ISEND(pal1,npal,pa_t,mpi%dn(i),rank      +2*isp*nodes,mpi%sp(isp)%comm,r5,mpi_err) ! send lower parts
      endif
      !-------------------------------------------------------------------
      ! Copy particle data to the scratch arrays for sending the upper parts
      !-------------------------------------------------------------------
      if (npau>0) then                                                          ! Move parts upwards
        do ip=1,npau1                                                           ! copy in physical particles
          pau1(ip)=pa(idx(isp)%u(ip))
        enddo
        do ip=npal-npal1+1,npal-npal1+npau-npau1                                ! fill up with weightless particles
          if (ip <= npaw) then
            pau1(ip - (npal-npal1) + npau1)=pa(idx(isp)%w(ip))
          else
            pau1(ip - (npal-npal1) + npau1)=pa(np+ip-npaw)
          endif
        enddo
        call MPI_ISEND(pau1,npau,pa_t,mpi%up(i),rank+nodes+2*isp*nodes,mpi%sp(isp)%comm,r6,mpi_err) ! send lower parts
      endif
      !-------------------------------------------------------------------
      ! Wait for the receive requests to finish. When they finish copy particle data back
      !-------------------------------------------------------------------
      if (npal>0) then                                                          ! do we have parts coming from lower ?
        call MPI_WAIT(r7,status,mpi_err)                                        ! Wait for lower to finish
        do ip=1,npal1                                                           ! copy in physical particles
          pa(idx(isp)%l(ip))=pal2(ip)
        enddo
        do ip=1,npal-npal1                                                      ! fill up with weightless particles
          if (ip <= npaw) then
            pa(idx(isp)%w(ip))=pal2(ip + npal1)
          else
            pa(np+ip-npaw)=pal2(ip + npal1)
          endif
        enddo
        deallocate(pal2)
      endif
      if (npau>0) then                                                          ! do we have parts coming from upper ?
        call MPI_WAIT(r8,status,mpi_err)                                        ! Wait for upper to finish
        do ip=1,npau1                                                           ! copy in physical particles
          pa(idx(isp)%u(ip))=pau2(ip)
        enddo
        do ip=npal-npal1+1,npal-npal1+npau-npau1                                ! fill up with weightless particles
          if (ip <= npaw) then
            pa(idx(isp)%w(ip))=pau2(ip - (npal-npal1) + npau1)
          else
            pa(np+ip-npaw)=pau2(ip - (npal-npal1) + npau1)
          endif
        enddo
        deallocate(pau2)
      endif
      if (npal>0) then
        call MPI_WAIT(r5,status,mpi_err)
        deallocate(pal1)
      endif
      if (npau>0) then 
        call MPI_WAIT(r6,status,mpi_err)
        deallocate(pau1)
      endif
      call MPI_WAIT(r1,status,mpi_err)
      call MPI_WAIT(r2,status,mpi_err)

      !print *, rank, 'npal, npal1, npal2', npal, npal1, npal2
      !print *, rank, 'npau, npau1, npau2', npau, npau1, npau2
      !print *, rank, 'npaw, dnp         ', npaw, max(0,npal-npal1+npau-npau1-npaw)
      sp(isp)%np = sp(isp)%np + max(0,npal-npal1+npau-npau1-npaw)         ! Adjust if we have changed nr of particles
      if (ptrace(1).eq.isp) then
        do ip=1,sp(isp)%mp
          if (pa(ip)%i .eq. ptrace(2)) then
            ww   = pa(ip)%w.ne.0.                                         ! Particle weightless?
            palb = (pa(ip)%q(i) < ilb).and.ww                             ! crossed lower bndry?
            paub = (pa(ip)%q(i) > iub).and.ww                             ! crossed upper bndry?
            print '(a,i7.7,a,3l2,a,i1,a,i2,a,3i6,a,i8,3i6,4f6.3,i18)', &
              "Ptrace2: R=",rank," LB,UB,ERR=",palb,paub,ip>sp(isp)%np .and. ww," DIM=",i, &
              " SP=",isp, " lb,q,ub=",ilb,pa(ip)%q(i),iub," ip,q,r,w,i=",ip,pa(ip)%q,pa(ip)%r,pa(ip)%w,pa(ip)%i
          endif
        enddo
      endif
    enddo
  enddo

  call trace_exit('Send Particles')
END SUBROUTINE SendParticles_omp
!===============================================================================
