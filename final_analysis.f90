!> ============================================================================
!> Add whatever final analysis you want here.  Or else copy this file to the
!> experiment directory, and edit a specially tuned analysis for the experiment
!> ============================================================================
SUBROUTINE final_analysis
  USE params
  USE grid_m,             ONLY: g
  USE species,            ONLY: sp, fields
  implicit none
  real(8):: sum1, sum2, minv, maxv, aver, rms, e
  integer:: isp, ix, iy, iz, n
  !----------------------------------------------------------------------------
  do isp=1,nspecies
    n = 0
    maxv = 0d0
    minv = 1d30
    sum1 = 0d0
    sum2 = 0d0
    do iz=g%lb(3), g%ub(3)-1
    do iy=g%lb(2), g%ub(2)-1
    do ix=g%lb(1), g%ub(1)-1
      if (fields(ix,iy,iz,isp)%vth > 0.0) then
        e = 0.5d0*sp(isp)%mass*fields(ix,iy,iz,isp)%vth**2
        minv = min(minv,e)
        maxv = max(maxv,e)
        sum1 = sum1 + e
        sum2 = sum2 + e**2
        n = n+1
      end if
    end do
    end do
    end do
    aver = sum1/n
    rms  = sqrt(sum2/n-aver**2)
    print *, ''
    print *, '------------------------------------------------------'
    print *, 'analysis of thermal energies for species', isp
    print 1, 'number of cells =', n
    print 1, 'average thermal energy =', aver
    print 1, 'min thermal energy =', minv
    print 1, 'max thermal energy =', maxv
    print 1, 'RMS thermal energy =', rms
    print 1, 'RMS/aver =', rms/aver
   1 format(a20,1pg14.6)
  end do
END SUBROUTINE final_analysis

